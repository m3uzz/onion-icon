<?php
/**
 * This file is part of Onion Library
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionIcon
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-icon
 */

declare(strict_types=1);

namespace OnionIcon;

class material
{
    /**
     *
     * @return array
     */
    public static function icons(): array
    {
        $laIcons = [
            [
                'value' => 'material-icons 10k',
                'label' => '10k',
                'attributes' => [
                    'data-icon' => 'material-icons 10k',
                    'data-icon-value' => '10k',
                ],
            ],
            [
                'value' => 'material-icons 10mp',
                'label' => '10mp',
                'attributes' => [
                    'data-icon' => 'material-icons 10mp',
                    'data-icon-value' => '10mp',
                ],
            ],
            [
                'value' => 'material-icons 11mp',
                'label' => '11mp',
                'attributes' => [
                    'data-icon' => 'material-icons 11mp',
                    'data-icon-value' => '11mp',
                ],
            ],
            [
                'value' => 'material-icons 123',
                'label' => '123',
                'attributes' => [
                    'data-icon' => 'material-icons 123',
                    'data-icon-value' => '123',
                ],
            ],
            [
                'value' => 'material-icons 12mp',
                'label' => '12mp',
                'attributes' => [
                    'data-icon' => 'material-icons 12mp',
                    'data-icon-value' => '12mp',
                ],
            ],
            [
                'value' => 'material-icons 13mp',
                'label' => '13mp',
                'attributes' => [
                    'data-icon' => 'material-icons 13mp',
                    'data-icon-value' => '13mp',
                ],
            ],
            [
                'value' => 'material-icons 14mp',
                'label' => '14mp',
                'attributes' => [
                    'data-icon' => 'material-icons 14mp',
                    'data-icon-value' => '14mp',
                ],
            ],
            [
                'value' => 'material-icons 15mp',
                'label' => '15mp',
                'attributes' => [
                    'data-icon' => 'material-icons 15mp',
                    'data-icon-value' => '15mp',
                ],
            ],
            [
                'value' => 'material-icons 16mp',
                'label' => '16mp',
                'attributes' => [
                    'data-icon' => 'material-icons 16mp',
                    'data-icon-value' => '16mp',
                ],
            ],
            [
                'value' => 'material-icons 17mp',
                'label' => '17mp',
                'attributes' => [
                    'data-icon' => 'material-icons 17mp',
                    'data-icon-value' => '17mp',
                ],
            ],
            [
                'value' => 'material-icons 18_up_rating',
                'label' => '18_up_rating',
                'attributes' => [
                    'data-icon' => 'material-icons 18_up_rating',
                    'data-icon-value' => '18_up_rating',
                ],
            ],
            [
                'value' => 'material-icons 18mp',
                'label' => '18mp',
                'attributes' => [
                    'data-icon' => 'material-icons 18mp',
                    'data-icon-value' => '18mp',
                ],
            ],
            [
                'value' => 'material-icons 19mp',
                'label' => '19mp',
                'attributes' => [
                    'data-icon' => 'material-icons 19mp',
                    'data-icon-value' => '19mp',
                ],
            ],
            [
                'value' => 'material-icons 1k',
                'label' => '1k',
                'attributes' => [
                    'data-icon' => 'material-icons 1k',
                    'data-icon-value' => '1k',
                ],
            ],
            [
                'value' => 'material-icons 1k_plus',
                'label' => '1k_plus',
                'attributes' => [
                    'data-icon' => 'material-icons 1k_plus',
                    'data-icon-value' => '1k_plus',
                ],
            ],
            [
                'value' => 'material-icons 1x_mobiledata',
                'label' => '1x_mobiledata',
                'attributes' => [
                    'data-icon' => 'material-icons 1x_mobiledata',
                    'data-icon-value' => '1x_mobiledata',
                ],
            ],
            [
                'value' => 'material-icons 20mp',
                'label' => '20mp',
                'attributes' => [
                    'data-icon' => 'material-icons 20mp',
                    'data-icon-value' => '20mp',
                ],
            ],
            [
                'value' => 'material-icons 21mp',
                'label' => '21mp',
                'attributes' => [
                    'data-icon' => 'material-icons 21mp',
                    'data-icon-value' => '21mp',
                ],
            ],
            [
                'value' => 'material-icons 22mp',
                'label' => '22mp',
                'attributes' => [
                    'data-icon' => 'material-icons 22mp',
                    'data-icon-value' => '22mp',
                ],
            ],
            [
                'value' => 'material-icons 23mp',
                'label' => '23mp',
                'attributes' => [
                    'data-icon' => 'material-icons 23mp',
                    'data-icon-value' => '23mp',
                ],
            ],
            [
                'value' => 'material-icons 24mp',
                'label' => '24mp',
                'attributes' => [
                    'data-icon' => 'material-icons 24mp',
                    'data-icon-value' => '24mp',
                ],
            ],
            [
                'value' => 'material-icons 2k',
                'label' => '2k',
                'attributes' => [
                    'data-icon' => 'material-icons 2k',
                    'data-icon-value' => '2k',
                ],
            ],
            [
                'value' => 'material-icons 2k_plus',
                'label' => '2k_plus',
                'attributes' => [
                    'data-icon' => 'material-icons 2k_plus',
                    'data-icon-value' => '2k_plus',
                ],
            ],
            [
                'value' => 'material-icons 2mp',
                'label' => '2mp',
                'attributes' => [
                    'data-icon' => 'material-icons 2mp',
                    'data-icon-value' => '2mp',
                ],
            ],
            [
                'value' => 'material-icons 30fps',
                'label' => '30fps',
                'attributes' => [
                    'data-icon' => 'material-icons 30fps',
                    'data-icon-value' => '30fps',
                ],
            ],
            [
                'value' => 'material-icons 30fps_select',
                'label' => '30fps_select',
                'attributes' => [
                    'data-icon' => 'material-icons 30fps_select',
                    'data-icon-value' => '30fps_select',
                ],
            ],
            [
                'value' => 'material-icons 360',
                'label' => '360',
                'attributes' => [
                    'data-icon' => 'material-icons 360',
                    'data-icon-value' => '360',
                ],
            ],
            [
                'value' => 'material-icons 3d_rotation',
                'label' => '3d_rotation',
                'attributes' => [
                    'data-icon' => 'material-icons 3d_rotation',
                    'data-icon-value' => '3d_rotation',
                ],
            ],
            [
                'value' => 'material-icons 3g_mobiledata',
                'label' => '3g_mobiledata',
                'attributes' => [
                    'data-icon' => 'material-icons 3g_mobiledata',
                    'data-icon-value' => '3g_mobiledata',
                ],
            ],
            [
                'value' => 'material-icons 3k',
                'label' => '3k',
                'attributes' => [
                    'data-icon' => 'material-icons 3k',
                    'data-icon-value' => '3k',
                ],
            ],
            [
                'value' => 'material-icons 3k_plus',
                'label' => '3k_plus',
                'attributes' => [
                    'data-icon' => 'material-icons 3k_plus',
                    'data-icon-value' => '3k_plus',
                ],
            ],
            [
                'value' => 'material-icons 3mp',
                'label' => '3mp',
                'attributes' => [
                    'data-icon' => 'material-icons 3mp',
                    'data-icon-value' => '3mp',
                ],
            ],
            [
                'value' => 'material-icons 3p',
                'label' => '3p',
                'attributes' => [
                    'data-icon' => 'material-icons 3p',
                    'data-icon-value' => '3p',
                ],
            ],
            [
                'value' => 'material-icons 4g_mobiledata',
                'label' => '4g_mobiledata',
                'attributes' => [
                    'data-icon' => 'material-icons 4g_mobiledata',
                    'data-icon-value' => '4g_mobiledata',
                ],
            ],
            [
                'value' => 'material-icons 4g_plus_mobiledata',
                'label' => '4g_plus_mobiledata',
                'attributes' => [
                    'data-icon' => 'material-icons 4g_plus_mobiledata',
                    'data-icon-value' => '4g_plus_mobiledata',
                ],
            ],
            [
                'value' => 'material-icons 4k',
                'label' => '4k',
                'attributes' => [
                    'data-icon' => 'material-icons 4k',
                    'data-icon-value' => '4k',
                ],
            ],
            [
                'value' => 'material-icons 4k_plus',
                'label' => '4k_plus',
                'attributes' => [
                    'data-icon' => 'material-icons 4k_plus',
                    'data-icon-value' => '4k_plus',
                ],
            ],
            [
                'value' => 'material-icons 4mp',
                'label' => '4mp',
                'attributes' => [
                    'data-icon' => 'material-icons 4mp',
                    'data-icon-value' => '4mp',
                ],
            ],
            [
                'value' => 'material-icons 5g',
                'label' => '5g',
                'attributes' => [
                    'data-icon' => 'material-icons 5g',
                    'data-icon-value' => '5g',
                ],
            ],
            [
                'value' => 'material-icons 5k',
                'label' => '5k',
                'attributes' => [
                    'data-icon' => 'material-icons 5k',
                    'data-icon-value' => '5k',
                ],
            ],
            [
                'value' => 'material-icons 5k_plus',
                'label' => '5k_plus',
                'attributes' => [
                    'data-icon' => 'material-icons 5k_plus',
                    'data-icon-value' => '5k_plus',
                ],
            ],
            [
                'value' => 'material-icons 5mp',
                'label' => '5mp',
                'attributes' => [
                    'data-icon' => 'material-icons 5mp',
                    'data-icon-value' => '5mp',
                ],
            ],
            [
                'value' => 'material-icons 60fps',
                'label' => '60fps',
                'attributes' => [
                    'data-icon' => 'material-icons 60fps',
                    'data-icon-value' => '60fps',
                ],
            ],
            [
                'value' => 'material-icons 60fps_select',
                'label' => '60fps_select',
                'attributes' => [
                    'data-icon' => 'material-icons 60fps_select',
                    'data-icon-value' => '60fps_select',
                ],
            ],
            [
                'value' => 'material-icons 6_ft_apart',
                'label' => '6_ft_apart',
                'attributes' => [
                    'data-icon' => 'material-icons 6_ft_apart',
                    'data-icon-value' => '6_ft_apart',
                ],
            ],
            [
                'value' => 'material-icons 6k',
                'label' => '6k',
                'attributes' => [
                    'data-icon' => 'material-icons 6k',
                    'data-icon-value' => '6k',
                ],
            ],
            [
                'value' => 'material-icons 6k_plus',
                'label' => '6k_plus',
                'attributes' => [
                    'data-icon' => 'material-icons 6k_plus',
                    'data-icon-value' => '6k_plus',
                ],
            ],
            [
                'value' => 'material-icons 6mp',
                'label' => '6mp',
                'attributes' => [
                    'data-icon' => 'material-icons 6mp',
                    'data-icon-value' => '6mp',
                ],
            ],
            [
                'value' => 'material-icons 7k',
                'label' => '7k',
                'attributes' => [
                    'data-icon' => 'material-icons 7k',
                    'data-icon-value' => '7k',
                ],
            ],
            [
                'value' => 'material-icons 7k_plus',
                'label' => '7k_plus',
                'attributes' => [
                    'data-icon' => 'material-icons 7k_plus',
                    'data-icon-value' => '7k_plus',
                ],
            ],
            [
                'value' => 'material-icons 7mp',
                'label' => '7mp',
                'attributes' => [
                    'data-icon' => 'material-icons 7mp',
                    'data-icon-value' => '7mp',
                ],
            ],
            [
                'value' => 'material-icons 8k',
                'label' => '8k',
                'attributes' => [
                    'data-icon' => 'material-icons 8k',
                    'data-icon-value' => '8k',
                ],
            ],
            [
                'value' => 'material-icons 8k_plus',
                'label' => '8k_plus',
                'attributes' => [
                    'data-icon' => 'material-icons 8k_plus',
                    'data-icon-value' => '8k_plus',
                ],
            ],
            [
                'value' => 'material-icons 8mp',
                'label' => '8mp',
                'attributes' => [
                    'data-icon' => 'material-icons 8mp',
                    'data-icon-value' => '8mp',
                ],
            ],
            [
                'value' => 'material-icons 9k',
                'label' => '9k',
                'attributes' => [
                    'data-icon' => 'material-icons 9k',
                    'data-icon-value' => '9k',
                ],
            ],
            [
                'value' => 'material-icons 9k_plus',
                'label' => '9k_plus',
                'attributes' => [
                    'data-icon' => 'material-icons 9k_plus',
                    'data-icon-value' => '9k_plus',
                ],
            ],
            [
                'value' => 'material-icons 9mp',
                'label' => '9mp',
                'attributes' => [
                    'data-icon' => 'material-icons 9mp',
                    'data-icon-value' => '9mp',
                ],
            ],
            [
                'value' => 'material-icons abc',
                'label' => 'abc',
                'attributes' => [
                    'data-icon' => 'material-icons abc',
                    'data-icon-value' => 'abc',
                ],
            ],
            [
                'value' => 'material-icons ac_unit',
                'label' => 'ac_unit',
                'attributes' => [
                    'data-icon' => 'material-icons ac_unit',
                    'data-icon-value' => 'ac_unit',
                ],
            ],
            [
                'value' => 'material-icons access_alarm',
                'label' => 'access_alarm',
                'attributes' => [
                    'data-icon' => 'material-icons access_alarm',
                    'data-icon-value' => 'access_alarm',
                ],
            ],
            [
                'value' => 'material-icons access_alarms',
                'label' => 'access_alarms',
                'attributes' => [
                    'data-icon' => 'material-icons access_alarms',
                    'data-icon-value' => 'access_alarms',
                ],
            ],
            [
                'value' => 'material-icons access_time',
                'label' => 'access_time',
                'attributes' => [
                    'data-icon' => 'material-icons access_time',
                    'data-icon-value' => 'access_time',
                ],
            ],
            [
                'value' => 'material-icons access_time_filled',
                'label' => 'access_time_filled',
                'attributes' => [
                    'data-icon' => 'material-icons access_time_filled',
                    'data-icon-value' => 'access_time_filled',
                ],
            ],
            [
                'value' => 'material-icons accessibility',
                'label' => 'accessibility',
                'attributes' => [
                    'data-icon' => 'material-icons accessibility',
                    'data-icon-value' => 'accessibility',
                ],
            ],
            [
                'value' => 'material-icons accessibility_new',
                'label' => 'accessibility_new',
                'attributes' => [
                    'data-icon' => 'material-icons accessibility_new',
                    'data-icon-value' => 'accessibility_new',
                ],
            ],
            [
                'value' => 'material-icons accessible',
                'label' => 'accessible',
                'attributes' => [
                    'data-icon' => 'material-icons accessible',
                    'data-icon-value' => 'accessible',
                ],
            ],
            [
                'value' => 'material-icons accessible_forward',
                'label' => 'accessible_forward',
                'attributes' => [
                    'data-icon' => 'material-icons accessible_forward',
                    'data-icon-value' => 'accessible_forward',
                ],
            ],
            [
                'value' => 'material-icons account_balance',
                'label' => 'account_balance',
                'attributes' => [
                    'data-icon' => 'material-icons account_balance',
                    'data-icon-value' => 'account_balance',
                ],
            ],
            [
                'value' => 'material-icons account_balance_wallet',
                'label' => 'account_balance_wallet',
                'attributes' => [
                    'data-icon' => 'material-icons account_balance_wallet',
                    'data-icon-value' => 'account_balance_wallet',
                ],
            ],
            [
                'value' => 'material-icons account_box',
                'label' => 'account_box',
                'attributes' => [
                    'data-icon' => 'material-icons account_box',
                    'data-icon-value' => 'account_box',
                ],
            ],
            [
                'value' => 'material-icons account_circle',
                'label' => 'account_circle',
                'attributes' => [
                    'data-icon' => 'material-icons account_circle',
                    'data-icon-value' => 'account_circle',
                ],
            ],
            [
                'value' => 'material-icons account_tree',
                'label' => 'account_tree',
                'attributes' => [
                    'data-icon' => 'material-icons account_tree',
                    'data-icon-value' => 'account_tree',
                ],
            ],
            [
                'value' => 'material-icons ad_units',
                'label' => 'ad_units',
                'attributes' => [
                    'data-icon' => 'material-icons ad_units',
                    'data-icon-value' => 'ad_units',
                ],
            ],
            [
                'value' => 'material-icons adb',
                'label' => 'adb',
                'attributes' => [
                    'data-icon' => 'material-icons adb',
                    'data-icon-value' => 'adb',
                ],
            ],
            [
                'value' => 'material-icons add',
                'label' => 'add',
                'attributes' => [
                    'data-icon' => 'material-icons add',
                    'data-icon-value' => 'add',
                ],
            ],
            [
                'value' => 'material-icons add_a_photo',
                'label' => 'add_a_photo',
                'attributes' => [
                    'data-icon' => 'material-icons add_a_photo',
                    'data-icon-value' => 'add_a_photo',
                ],
            ],
            [
                'value' => 'material-icons add_alarm',
                'label' => 'add_alarm',
                'attributes' => [
                    'data-icon' => 'material-icons add_alarm',
                    'data-icon-value' => 'add_alarm',
                ],
            ],
            [
                'value' => 'material-icons add_alert',
                'label' => 'add_alert',
                'attributes' => [
                    'data-icon' => 'material-icons add_alert',
                    'data-icon-value' => 'add_alert',
                ],
            ],
            [
                'value' => 'material-icons add_box',
                'label' => 'add_box',
                'attributes' => [
                    'data-icon' => 'material-icons add_box',
                    'data-icon-value' => 'add_box',
                ],
            ],
            [
                'value' => 'material-icons add_business',
                'label' => 'add_business',
                'attributes' => [
                    'data-icon' => 'material-icons add_business',
                    'data-icon-value' => 'add_business',
                ],
            ],
            [
                'value' => 'material-icons add_call',
                'label' => 'add_call',
                'attributes' => [
                    'data-icon' => 'material-icons add_call',
                    'data-icon-value' => 'add_call',
                ],
            ],
            [
                'value' => 'material-icons add_card',
                'label' => 'add_card',
                'attributes' => [
                    'data-icon' => 'material-icons add_card',
                    'data-icon-value' => 'add_card',
                ],
            ],
            [
                'value' => 'material-icons add_chart',
                'label' => 'add_chart',
                'attributes' => [
                    'data-icon' => 'material-icons add_chart',
                    'data-icon-value' => 'add_chart',
                ],
            ],
            [
                'value' => 'material-icons add_circle',
                'label' => 'add_circle',
                'attributes' => [
                    'data-icon' => 'material-icons add_circle',
                    'data-icon-value' => 'add_circle',
                ],
            ],
            [
                'value' => 'material-icons add_circle_outline',
                'label' => 'add_circle_outline',
                'attributes' => [
                    'data-icon' => 'material-icons add_circle_outline',
                    'data-icon-value' => 'add_circle_outline',
                ],
            ],
            [
                'value' => 'material-icons add_comment',
                'label' => 'add_comment',
                'attributes' => [
                    'data-icon' => 'material-icons add_comment',
                    'data-icon-value' => 'add_comment',
                ],
            ],
            [
                'value' => 'material-icons add_ic_call',
                'label' => 'add_ic_call',
                'attributes' => [
                    'data-icon' => 'material-icons add_ic_call',
                    'data-icon-value' => 'add_ic_call',
                ],
            ],
            [
                'value' => 'material-icons add_link',
                'label' => 'add_link',
                'attributes' => [
                    'data-icon' => 'material-icons add_link',
                    'data-icon-value' => 'add_link',
                ],
            ],
            [
                'value' => 'material-icons add_location',
                'label' => 'add_location',
                'attributes' => [
                    'data-icon' => 'material-icons add_location',
                    'data-icon-value' => 'add_location',
                ],
            ],
            [
                'value' => 'material-icons add_location_alt',
                'label' => 'add_location_alt',
                'attributes' => [
                    'data-icon' => 'material-icons add_location_alt',
                    'data-icon-value' => 'add_location_alt',
                ],
            ],
            [
                'value' => 'material-icons add_moderator',
                'label' => 'add_moderator',
                'attributes' => [
                    'data-icon' => 'material-icons add_moderator',
                    'data-icon-value' => 'add_moderator',
                ],
            ],
            [
                'value' => 'material-icons add_photo_alternate',
                'label' => 'add_photo_alternate',
                'attributes' => [
                    'data-icon' => 'material-icons add_photo_alternate',
                    'data-icon-value' => 'add_photo_alternate',
                ],
            ],
            [
                'value' => 'material-icons add_reaction',
                'label' => 'add_reaction',
                'attributes' => [
                    'data-icon' => 'material-icons add_reaction',
                    'data-icon-value' => 'add_reaction',
                ],
            ],
            [
                'value' => 'material-icons add_road',
                'label' => 'add_road',
                'attributes' => [
                    'data-icon' => 'material-icons add_road',
                    'data-icon-value' => 'add_road',
                ],
            ],
            [
                'value' => 'material-icons add_shopping_cart',
                'label' => 'add_shopping_cart',
                'attributes' => [
                    'data-icon' => 'material-icons add_shopping_cart',
                    'data-icon-value' => 'add_shopping_cart',
                ],
            ],
            [
                'value' => 'material-icons add_task',
                'label' => 'add_task',
                'attributes' => [
                    'data-icon' => 'material-icons add_task',
                    'data-icon-value' => 'add_task',
                ],
            ],
            [
                'value' => 'material-icons add_to_drive',
                'label' => 'add_to_drive',
                'attributes' => [
                    'data-icon' => 'material-icons add_to_drive',
                    'data-icon-value' => 'add_to_drive',
                ],
            ],
            [
                'value' => 'material-icons add_to_home_screen',
                'label' => 'add_to_home_screen',
                'attributes' => [
                    'data-icon' => 'material-icons add_to_home_screen',
                    'data-icon-value' => 'add_to_home_screen',
                ],
            ],
            [
                'value' => 'material-icons add_to_photos',
                'label' => 'add_to_photos',
                'attributes' => [
                    'data-icon' => 'material-icons add_to_photos',
                    'data-icon-value' => 'add_to_photos',
                ],
            ],
            [
                'value' => 'material-icons add_to_queue',
                'label' => 'add_to_queue',
                'attributes' => [
                    'data-icon' => 'material-icons add_to_queue',
                    'data-icon-value' => 'add_to_queue',
                ],
            ],
            [
                'value' => 'material-icons addchart',
                'label' => 'addchart',
                'attributes' => [
                    'data-icon' => 'material-icons addchart',
                    'data-icon-value' => 'addchart',
                ],
            ],
            [
                'value' => 'material-icons adf_scanner',
                'label' => 'adf_scanner',
                'attributes' => [
                    'data-icon' => 'material-icons adf_scanner',
                    'data-icon-value' => 'adf_scanner',
                ],
            ],
            [
                'value' => 'material-icons adjust',
                'label' => 'adjust',
                'attributes' => [
                    'data-icon' => 'material-icons adjust',
                    'data-icon-value' => 'adjust',
                ],
            ],
            [
                'value' => 'material-icons admin_panel_settings',
                'label' => 'admin_panel_settings',
                'attributes' => [
                    'data-icon' => 'material-icons admin_panel_settings',
                    'data-icon-value' => 'admin_panel_settings',
                ],
            ],
            [
                'value' => 'material-icons adobe',
                'label' => 'adobe',
                'attributes' => [
                    'data-icon' => 'material-icons adobe',
                    'data-icon-value' => 'adobe',
                ],
            ],
            [
                'value' => 'material-icons ads_click',
                'label' => 'ads_click',
                'attributes' => [
                    'data-icon' => 'material-icons ads_click',
                    'data-icon-value' => 'ads_click',
                ],
            ],
            [
                'value' => 'material-icons agriculture',
                'label' => 'agriculture',
                'attributes' => [
                    'data-icon' => 'material-icons agriculture',
                    'data-icon-value' => 'agriculture',
                ],
            ],
            [
                'value' => 'material-icons air',
                'label' => 'air',
                'attributes' => [
                    'data-icon' => 'material-icons air',
                    'data-icon-value' => 'air',
                ],
            ],
            [
                'value' => 'material-icons airline_seat_flat',
                'label' => 'airline_seat_flat',
                'attributes' => [
                    'data-icon' => 'material-icons airline_seat_flat',
                    'data-icon-value' => 'airline_seat_flat',
                ],
            ],
            [
                'value' => 'material-icons airline_seat_flat_angled',
                'label' => 'airline_seat_flat_angled',
                'attributes' => [
                    'data-icon' => 'material-icons airline_seat_flat_angled',
                    'data-icon-value' => 'airline_seat_flat_angled',
                ],
            ],
            [
                'value' => 'material-icons airline_seat_individual_suite',
                'label' => 'airline_seat_individual_suite',
                'attributes' => [
                    'data-icon' => 'material-icons airline_seat_individual_suite',
                    'data-icon-value' => 'airline_seat_individual_suite',
                ],
            ],
            [
                'value' => 'material-icons airline_seat_legroom_extra',
                'label' => 'airline_seat_legroom_extra',
                'attributes' => [
                    'data-icon' => 'material-icons airline_seat_legroom_extra',
                    'data-icon-value' => 'airline_seat_legroom_extra',
                ],
            ],
            [
                'value' => 'material-icons airline_seat_legroom_normal',
                'label' => 'airline_seat_legroom_normal',
                'attributes' => [
                    'data-icon' => 'material-icons airline_seat_legroom_normal',
                    'data-icon-value' => 'airline_seat_legroom_normal',
                ],
            ],
            [
                'value' => 'material-icons airline_seat_legroom_reduced',
                'label' => 'airline_seat_legroom_reduced',
                'attributes' => [
                    'data-icon' => 'material-icons airline_seat_legroom_reduced',
                    'data-icon-value' => 'airline_seat_legroom_reduced',
                ],
            ],
            [
                'value' => 'material-icons airline_seat_recline_extra',
                'label' => 'airline_seat_recline_extra',
                'attributes' => [
                    'data-icon' => 'material-icons airline_seat_recline_extra',
                    'data-icon-value' => 'airline_seat_recline_extra',
                ],
            ],
            [
                'value' => 'material-icons airline_seat_recline_normal',
                'label' => 'airline_seat_recline_normal',
                'attributes' => [
                    'data-icon' => 'material-icons airline_seat_recline_normal',
                    'data-icon-value' => 'airline_seat_recline_normal',
                ],
            ],
            [
                'value' => 'material-icons airline_stops',
                'label' => 'airline_stops',
                'attributes' => [
                    'data-icon' => 'material-icons airline_stops',
                    'data-icon-value' => 'airline_stops',
                ],
            ],
            [
                'value' => 'material-icons airlines',
                'label' => 'airlines',
                'attributes' => [
                    'data-icon' => 'material-icons airlines',
                    'data-icon-value' => 'airlines',
                ],
            ],
            [
                'value' => 'material-icons airplane_ticket',
                'label' => 'airplane_ticket',
                'attributes' => [
                    'data-icon' => 'material-icons airplane_ticket',
                    'data-icon-value' => 'airplane_ticket',
                ],
            ],
            [
                'value' => 'material-icons airplanemode_active',
                'label' => 'airplanemode_active',
                'attributes' => [
                    'data-icon' => 'material-icons airplanemode_active',
                    'data-icon-value' => 'airplanemode_active',
                ],
            ],
            [
                'value' => 'material-icons airplanemode_inactive',
                'label' => 'airplanemode_inactive',
                'attributes' => [
                    'data-icon' => 'material-icons airplanemode_inactive',
                    'data-icon-value' => 'airplanemode_inactive',
                ],
            ],
            [
                'value' => 'material-icons airplanemode_off',
                'label' => 'airplanemode_off',
                'attributes' => [
                    'data-icon' => 'material-icons airplanemode_off',
                    'data-icon-value' => 'airplanemode_off',
                ],
            ],
            [
                'value' => 'material-icons airplanemode_on',
                'label' => 'airplanemode_on',
                'attributes' => [
                    'data-icon' => 'material-icons airplanemode_on',
                    'data-icon-value' => 'airplanemode_on',
                ],
            ],
            [
                'value' => 'material-icons airplay',
                'label' => 'airplay',
                'attributes' => [
                    'data-icon' => 'material-icons airplay',
                    'data-icon-value' => 'airplay',
                ],
            ],
            [
                'value' => 'material-icons airport_shuttle',
                'label' => 'airport_shuttle',
                'attributes' => [
                    'data-icon' => 'material-icons airport_shuttle',
                    'data-icon-value' => 'airport_shuttle',
                ],
            ],
            [
                'value' => 'material-icons alarm',
                'label' => 'alarm',
                'attributes' => [
                    'data-icon' => 'material-icons alarm',
                    'data-icon-value' => 'alarm',
                ],
            ],
            [
                'value' => 'material-icons alarm_add',
                'label' => 'alarm_add',
                'attributes' => [
                    'data-icon' => 'material-icons alarm_add',
                    'data-icon-value' => 'alarm_add',
                ],
            ],
            [
                'value' => 'material-icons alarm_off',
                'label' => 'alarm_off',
                'attributes' => [
                    'data-icon' => 'material-icons alarm_off',
                    'data-icon-value' => 'alarm_off',
                ],
            ],
            [
                'value' => 'material-icons alarm_on',
                'label' => 'alarm_on',
                'attributes' => [
                    'data-icon' => 'material-icons alarm_on',
                    'data-icon-value' => 'alarm_on',
                ],
            ],
            [
                'value' => 'material-icons album',
                'label' => 'album',
                'attributes' => [
                    'data-icon' => 'material-icons album',
                    'data-icon-value' => 'album',
                ],
            ],
            [
                'value' => 'material-icons align_horizontal_center',
                'label' => 'align_horizontal_center',
                'attributes' => [
                    'data-icon' => 'material-icons align_horizontal_center',
                    'data-icon-value' => 'align_horizontal_center',
                ],
            ],
            [
                'value' => 'material-icons align_horizontal_left',
                'label' => 'align_horizontal_left',
                'attributes' => [
                    'data-icon' => 'material-icons align_horizontal_left',
                    'data-icon-value' => 'align_horizontal_left',
                ],
            ],
            [
                'value' => 'material-icons align_horizontal_right',
                'label' => 'align_horizontal_right',
                'attributes' => [
                    'data-icon' => 'material-icons align_horizontal_right',
                    'data-icon-value' => 'align_horizontal_right',
                ],
            ],
            [
                'value' => 'material-icons align_vertical_bottom',
                'label' => 'align_vertical_bottom',
                'attributes' => [
                    'data-icon' => 'material-icons align_vertical_bottom',
                    'data-icon-value' => 'align_vertical_bottom',
                ],
            ],
            [
                'value' => 'material-icons align_vertical_center',
                'label' => 'align_vertical_center',
                'attributes' => [
                    'data-icon' => 'material-icons align_vertical_center',
                    'data-icon-value' => 'align_vertical_center',
                ],
            ],
            [
                'value' => 'material-icons align_vertical_top',
                'label' => 'align_vertical_top',
                'attributes' => [
                    'data-icon' => 'material-icons align_vertical_top',
                    'data-icon-value' => 'align_vertical_top',
                ],
            ],
            [
                'value' => 'material-icons all_inbox',
                'label' => 'all_inbox',
                'attributes' => [
                    'data-icon' => 'material-icons all_inbox',
                    'data-icon-value' => 'all_inbox',
                ],
            ],
            [
                'value' => 'material-icons all_inclusive',
                'label' => 'all_inclusive',
                'attributes' => [
                    'data-icon' => 'material-icons all_inclusive',
                    'data-icon-value' => 'all_inclusive',
                ],
            ],
            [
                'value' => 'material-icons all_out',
                'label' => 'all_out',
                'attributes' => [
                    'data-icon' => 'material-icons all_out',
                    'data-icon-value' => 'all_out',
                ],
            ],
            [
                'value' => 'material-icons alt_route',
                'label' => 'alt_route',
                'attributes' => [
                    'data-icon' => 'material-icons alt_route',
                    'data-icon-value' => 'alt_route',
                ],
            ],
            [
                'value' => 'material-icons alternate_email',
                'label' => 'alternate_email',
                'attributes' => [
                    'data-icon' => 'material-icons alternate_email',
                    'data-icon-value' => 'alternate_email',
                ],
            ],
            [
                'value' => 'material-icons amp_stories',
                'label' => 'amp_stories',
                'attributes' => [
                    'data-icon' => 'material-icons amp_stories',
                    'data-icon-value' => 'amp_stories',
                ],
            ],
            [
                'value' => 'material-icons analytics',
                'label' => 'analytics',
                'attributes' => [
                    'data-icon' => 'material-icons analytics',
                    'data-icon-value' => 'analytics',
                ],
            ],
            [
                'value' => 'material-icons anchor',
                'label' => 'anchor',
                'attributes' => [
                    'data-icon' => 'material-icons anchor',
                    'data-icon-value' => 'anchor',
                ],
            ],
            [
                'value' => 'material-icons android',
                'label' => 'android',
                'attributes' => [
                    'data-icon' => 'material-icons android',
                    'data-icon-value' => 'android',
                ],
            ],
            [
                'value' => 'material-icons animation',
                'label' => 'animation',
                'attributes' => [
                    'data-icon' => 'material-icons animation',
                    'data-icon-value' => 'animation',
                ],
            ],
            [
                'value' => 'material-icons announcement',
                'label' => 'announcement',
                'attributes' => [
                    'data-icon' => 'material-icons announcement',
                    'data-icon-value' => 'announcement',
                ],
            ],
            [
                'value' => 'material-icons aod',
                'label' => 'aod',
                'attributes' => [
                    'data-icon' => 'material-icons aod',
                    'data-icon-value' => 'aod',
                ],
            ],
            [
                'value' => 'material-icons apartment',
                'label' => 'apartment',
                'attributes' => [
                    'data-icon' => 'material-icons apartment',
                    'data-icon-value' => 'apartment',
                ],
            ],
            [
                'value' => 'material-icons api',
                'label' => 'api',
                'attributes' => [
                    'data-icon' => 'material-icons api',
                    'data-icon-value' => 'api',
                ],
            ],
            [
                'value' => 'material-icons app_blocking',
                'label' => 'app_blocking',
                'attributes' => [
                    'data-icon' => 'material-icons app_blocking',
                    'data-icon-value' => 'app_blocking',
                ],
            ],
            [
                'value' => 'material-icons app_registration',
                'label' => 'app_registration',
                'attributes' => [
                    'data-icon' => 'material-icons app_registration',
                    'data-icon-value' => 'app_registration',
                ],
            ],
            [
                'value' => 'material-icons app_settings_alt',
                'label' => 'app_settings_alt',
                'attributes' => [
                    'data-icon' => 'material-icons app_settings_alt',
                    'data-icon-value' => 'app_settings_alt',
                ],
            ],
            [
                'value' => 'material-icons app_shortcut',
                'label' => 'app_shortcut',
                'attributes' => [
                    'data-icon' => 'material-icons app_shortcut',
                    'data-icon-value' => 'app_shortcut',
                ],
            ],
            [
                'value' => 'material-icons apple',
                'label' => 'apple',
                'attributes' => [
                    'data-icon' => 'material-icons apple',
                    'data-icon-value' => 'apple',
                ],
            ],
            [
                'value' => 'material-icons approval',
                'label' => 'approval',
                'attributes' => [
                    'data-icon' => 'material-icons approval',
                    'data-icon-value' => 'approval',
                ],
            ],
            [
                'value' => 'material-icons apps',
                'label' => 'apps',
                'attributes' => [
                    'data-icon' => 'material-icons apps',
                    'data-icon-value' => 'apps',
                ],
            ],
            [
                'value' => 'material-icons apps_outage',
                'label' => 'apps_outage',
                'attributes' => [
                    'data-icon' => 'material-icons apps_outage',
                    'data-icon-value' => 'apps_outage',
                ],
            ],
            [
                'value' => 'material-icons architecture',
                'label' => 'architecture',
                'attributes' => [
                    'data-icon' => 'material-icons architecture',
                    'data-icon-value' => 'architecture',
                ],
            ],
            [
                'value' => 'material-icons archive',
                'label' => 'archive',
                'attributes' => [
                    'data-icon' => 'material-icons archive',
                    'data-icon-value' => 'archive',
                ],
            ],
            [
                'value' => 'material-icons area_chart',
                'label' => 'area_chart',
                'attributes' => [
                    'data-icon' => 'material-icons area_chart',
                    'data-icon-value' => 'area_chart',
                ],
            ],
            [
                'value' => 'material-icons arrow_back',
                'label' => 'arrow_back',
                'attributes' => [
                    'data-icon' => 'material-icons arrow_back',
                    'data-icon-value' => 'arrow_back',
                ],
            ],
            [
                'value' => 'material-icons arrow_back_ios',
                'label' => 'arrow_back_ios',
                'attributes' => [
                    'data-icon' => 'material-icons arrow_back_ios',
                    'data-icon-value' => 'arrow_back_ios',
                ],
            ],
            [
                'value' => 'material-icons arrow_back_ios_new',
                'label' => 'arrow_back_ios_new',
                'attributes' => [
                    'data-icon' => 'material-icons arrow_back_ios_new',
                    'data-icon-value' => 'arrow_back_ios_new',
                ],
            ],
            [
                'value' => 'material-icons arrow_circle_down',
                'label' => 'arrow_circle_down',
                'attributes' => [
                    'data-icon' => 'material-icons arrow_circle_down',
                    'data-icon-value' => 'arrow_circle_down',
                ],
            ],
            [
                'value' => 'material-icons arrow_circle_left',
                'label' => 'arrow_circle_left',
                'attributes' => [
                    'data-icon' => 'material-icons arrow_circle_left',
                    'data-icon-value' => 'arrow_circle_left',
                ],
            ],
            [
                'value' => 'material-icons arrow_circle_right',
                'label' => 'arrow_circle_right',
                'attributes' => [
                    'data-icon' => 'material-icons arrow_circle_right',
                    'data-icon-value' => 'arrow_circle_right',
                ],
            ],
            [
                'value' => 'material-icons arrow_circle_up',
                'label' => 'arrow_circle_up',
                'attributes' => [
                    'data-icon' => 'material-icons arrow_circle_up',
                    'data-icon-value' => 'arrow_circle_up',
                ],
            ],
            [
                'value' => 'material-icons arrow_downward',
                'label' => 'arrow_downward',
                'attributes' => [
                    'data-icon' => 'material-icons arrow_downward',
                    'data-icon-value' => 'arrow_downward',
                ],
            ],
            [
                'value' => 'material-icons arrow_drop_down',
                'label' => 'arrow_drop_down',
                'attributes' => [
                    'data-icon' => 'material-icons arrow_drop_down',
                    'data-icon-value' => 'arrow_drop_down',
                ],
            ],
            [
                'value' => 'material-icons arrow_drop_down_circle',
                'label' => 'arrow_drop_down_circle',
                'attributes' => [
                    'data-icon' => 'material-icons arrow_drop_down_circle',
                    'data-icon-value' => 'arrow_drop_down_circle',
                ],
            ],
            [
                'value' => 'material-icons arrow_drop_up',
                'label' => 'arrow_drop_up',
                'attributes' => [
                    'data-icon' => 'material-icons arrow_drop_up',
                    'data-icon-value' => 'arrow_drop_up',
                ],
            ],
            [
                'value' => 'material-icons arrow_forward',
                'label' => 'arrow_forward',
                'attributes' => [
                    'data-icon' => 'material-icons arrow_forward',
                    'data-icon-value' => 'arrow_forward',
                ],
            ],
            [
                'value' => 'material-icons arrow_forward_ios',
                'label' => 'arrow_forward_ios',
                'attributes' => [
                    'data-icon' => 'material-icons arrow_forward_ios',
                    'data-icon-value' => 'arrow_forward_ios',
                ],
            ],
            [
                'value' => 'material-icons arrow_left',
                'label' => 'arrow_left',
                'attributes' => [
                    'data-icon' => 'material-icons arrow_left',
                    'data-icon-value' => 'arrow_left',
                ],
            ],
            [
                'value' => 'material-icons arrow_right',
                'label' => 'arrow_right',
                'attributes' => [
                    'data-icon' => 'material-icons arrow_right',
                    'data-icon-value' => 'arrow_right',
                ],
            ],
            [
                'value' => 'material-icons arrow_right_alt',
                'label' => 'arrow_right_alt',
                'attributes' => [
                    'data-icon' => 'material-icons arrow_right_alt',
                    'data-icon-value' => 'arrow_right_alt',
                ],
            ],
            [
                'value' => 'material-icons arrow_upward',
                'label' => 'arrow_upward',
                'attributes' => [
                    'data-icon' => 'material-icons arrow_upward',
                    'data-icon-value' => 'arrow_upward',
                ],
            ],
            [
                'value' => 'material-icons art_track',
                'label' => 'art_track',
                'attributes' => [
                    'data-icon' => 'material-icons art_track',
                    'data-icon-value' => 'art_track',
                ],
            ],
            [
                'value' => 'material-icons article',
                'label' => 'article',
                'attributes' => [
                    'data-icon' => 'material-icons article',
                    'data-icon-value' => 'article',
                ],
            ],
            [
                'value' => 'material-icons aspect_ratio',
                'label' => 'aspect_ratio',
                'attributes' => [
                    'data-icon' => 'material-icons aspect_ratio',
                    'data-icon-value' => 'aspect_ratio',
                ],
            ],
            [
                'value' => 'material-icons assessment',
                'label' => 'assessment',
                'attributes' => [
                    'data-icon' => 'material-icons assessment',
                    'data-icon-value' => 'assessment',
                ],
            ],
            [
                'value' => 'material-icons assignment',
                'label' => 'assignment',
                'attributes' => [
                    'data-icon' => 'material-icons assignment',
                    'data-icon-value' => 'assignment',
                ],
            ],
            [
                'value' => 'material-icons assignment_ind',
                'label' => 'assignment_ind',
                'attributes' => [
                    'data-icon' => 'material-icons assignment_ind',
                    'data-icon-value' => 'assignment_ind',
                ],
            ],
            [
                'value' => 'material-icons assignment_late',
                'label' => 'assignment_late',
                'attributes' => [
                    'data-icon' => 'material-icons assignment_late',
                    'data-icon-value' => 'assignment_late',
                ],
            ],
            [
                'value' => 'material-icons assignment_return',
                'label' => 'assignment_return',
                'attributes' => [
                    'data-icon' => 'material-icons assignment_return',
                    'data-icon-value' => 'assignment_return',
                ],
            ],
            [
                'value' => 'material-icons assignment_returned',
                'label' => 'assignment_returned',
                'attributes' => [
                    'data-icon' => 'material-icons assignment_returned',
                    'data-icon-value' => 'assignment_returned',
                ],
            ],
            [
                'value' => 'material-icons assignment_turned_in',
                'label' => 'assignment_turned_in',
                'attributes' => [
                    'data-icon' => 'material-icons assignment_turned_in',
                    'data-icon-value' => 'assignment_turned_in',
                ],
            ],
            [
                'value' => 'material-icons assistant',
                'label' => 'assistant',
                'attributes' => [
                    'data-icon' => 'material-icons assistant',
                    'data-icon-value' => 'assistant',
                ],
            ],
            [
                'value' => 'material-icons assistant_direction',
                'label' => 'assistant_direction',
                'attributes' => [
                    'data-icon' => 'material-icons assistant_direction',
                    'data-icon-value' => 'assistant_direction',
                ],
            ],
            [
                'value' => 'material-icons assistant_navigation',
                'label' => 'assistant_navigation',
                'attributes' => [
                    'data-icon' => 'material-icons assistant_navigation',
                    'data-icon-value' => 'assistant_navigation',
                ],
            ],
            [
                'value' => 'material-icons assistant_photo',
                'label' => 'assistant_photo',
                'attributes' => [
                    'data-icon' => 'material-icons assistant_photo',
                    'data-icon-value' => 'assistant_photo',
                ],
            ],
            [
                'value' => 'material-icons assured_workload',
                'label' => 'assured_workload',
                'attributes' => [
                    'data-icon' => 'material-icons assured_workload',
                    'data-icon-value' => 'assured_workload',
                ],
            ],
            [
                'value' => 'material-icons atm',
                'label' => 'atm',
                'attributes' => [
                    'data-icon' => 'material-icons atm',
                    'data-icon-value' => 'atm',
                ],
            ],
            [
                'value' => 'material-icons attach_email',
                'label' => 'attach_email',
                'attributes' => [
                    'data-icon' => 'material-icons attach_email',
                    'data-icon-value' => 'attach_email',
                ],
            ],
            [
                'value' => 'material-icons attach_file',
                'label' => 'attach_file',
                'attributes' => [
                    'data-icon' => 'material-icons attach_file',
                    'data-icon-value' => 'attach_file',
                ],
            ],
            [
                'value' => 'material-icons attach_money',
                'label' => 'attach_money',
                'attributes' => [
                    'data-icon' => 'material-icons attach_money',
                    'data-icon-value' => 'attach_money',
                ],
            ],
            [
                'value' => 'material-icons attachment',
                'label' => 'attachment',
                'attributes' => [
                    'data-icon' => 'material-icons attachment',
                    'data-icon-value' => 'attachment',
                ],
            ],
            [
                'value' => 'material-icons attractions',
                'label' => 'attractions',
                'attributes' => [
                    'data-icon' => 'material-icons attractions',
                    'data-icon-value' => 'attractions',
                ],
            ],
            [
                'value' => 'material-icons attribution',
                'label' => 'attribution',
                'attributes' => [
                    'data-icon' => 'material-icons attribution',
                    'data-icon-value' => 'attribution',
                ],
            ],
            [
                'value' => 'material-icons audio_file',
                'label' => 'audio_file',
                'attributes' => [
                    'data-icon' => 'material-icons audio_file',
                    'data-icon-value' => 'audio_file',
                ],
            ],
            [
                'value' => 'material-icons audiotrack',
                'label' => 'audiotrack',
                'attributes' => [
                    'data-icon' => 'material-icons audiotrack',
                    'data-icon-value' => 'audiotrack',
                ],
            ],
            [
                'value' => 'material-icons auto_awesome',
                'label' => 'auto_awesome',
                'attributes' => [
                    'data-icon' => 'material-icons auto_awesome',
                    'data-icon-value' => 'auto_awesome',
                ],
            ],
            [
                'value' => 'material-icons auto_awesome_mosaic',
                'label' => 'auto_awesome_mosaic',
                'attributes' => [
                    'data-icon' => 'material-icons auto_awesome_mosaic',
                    'data-icon-value' => 'auto_awesome_mosaic',
                ],
            ],
            [
                'value' => 'material-icons auto_awesome_motion',
                'label' => 'auto_awesome_motion',
                'attributes' => [
                    'data-icon' => 'material-icons auto_awesome_motion',
                    'data-icon-value' => 'auto_awesome_motion',
                ],
            ],
            [
                'value' => 'material-icons auto_delete',
                'label' => 'auto_delete',
                'attributes' => [
                    'data-icon' => 'material-icons auto_delete',
                    'data-icon-value' => 'auto_delete',
                ],
            ],
            [
                'value' => 'material-icons auto_fix_high',
                'label' => 'auto_fix_high',
                'attributes' => [
                    'data-icon' => 'material-icons auto_fix_high',
                    'data-icon-value' => 'auto_fix_high',
                ],
            ],
            [
                'value' => 'material-icons auto_fix_normal',
                'label' => 'auto_fix_normal',
                'attributes' => [
                    'data-icon' => 'material-icons auto_fix_normal',
                    'data-icon-value' => 'auto_fix_normal',
                ],
            ],
            [
                'value' => 'material-icons auto_fix_off',
                'label' => 'auto_fix_off',
                'attributes' => [
                    'data-icon' => 'material-icons auto_fix_off',
                    'data-icon-value' => 'auto_fix_off',
                ],
            ],
            [
                'value' => 'material-icons auto_graph',
                'label' => 'auto_graph',
                'attributes' => [
                    'data-icon' => 'material-icons auto_graph',
                    'data-icon-value' => 'auto_graph',
                ],
            ],
            [
                'value' => 'material-icons auto_mode',
                'label' => 'auto_mode',
                'attributes' => [
                    'data-icon' => 'material-icons auto_mode',
                    'data-icon-value' => 'auto_mode',
                ],
            ],
            [
                'value' => 'material-icons auto_stories',
                'label' => 'auto_stories',
                'attributes' => [
                    'data-icon' => 'material-icons auto_stories',
                    'data-icon-value' => 'auto_stories',
                ],
            ],
            [
                'value' => 'material-icons autofps_select',
                'label' => 'autofps_select',
                'attributes' => [
                    'data-icon' => 'material-icons autofps_select',
                    'data-icon-value' => 'autofps_select',
                ],
            ],
            [
                'value' => 'material-icons autorenew',
                'label' => 'autorenew',
                'attributes' => [
                    'data-icon' => 'material-icons autorenew',
                    'data-icon-value' => 'autorenew',
                ],
            ],
            [
                'value' => 'material-icons av_timer',
                'label' => 'av_timer',
                'attributes' => [
                    'data-icon' => 'material-icons av_timer',
                    'data-icon-value' => 'av_timer',
                ],
            ],
            [
                'value' => 'material-icons baby_changing_station',
                'label' => 'baby_changing_station',
                'attributes' => [
                    'data-icon' => 'material-icons baby_changing_station',
                    'data-icon-value' => 'baby_changing_station',
                ],
            ],
            [
                'value' => 'material-icons back_hand',
                'label' => 'back_hand',
                'attributes' => [
                    'data-icon' => 'material-icons back_hand',
                    'data-icon-value' => 'back_hand',
                ],
            ],
            [
                'value' => 'material-icons backpack',
                'label' => 'backpack',
                'attributes' => [
                    'data-icon' => 'material-icons backpack',
                    'data-icon-value' => 'backpack',
                ],
            ],
            [
                'value' => 'material-icons backspace',
                'label' => 'backspace',
                'attributes' => [
                    'data-icon' => 'material-icons backspace',
                    'data-icon-value' => 'backspace',
                ],
            ],
            [
                'value' => 'material-icons backup',
                'label' => 'backup',
                'attributes' => [
                    'data-icon' => 'material-icons backup',
                    'data-icon-value' => 'backup',
                ],
            ],
            [
                'value' => 'material-icons backup_table',
                'label' => 'backup_table',
                'attributes' => [
                    'data-icon' => 'material-icons backup_table',
                    'data-icon-value' => 'backup_table',
                ],
            ],
            [
                'value' => 'material-icons badge',
                'label' => 'badge',
                'attributes' => [
                    'data-icon' => 'material-icons badge',
                    'data-icon-value' => 'badge',
                ],
            ],
            [
                'value' => 'material-icons bakery_dining',
                'label' => 'bakery_dining',
                'attributes' => [
                    'data-icon' => 'material-icons bakery_dining',
                    'data-icon-value' => 'bakery_dining',
                ],
            ],
            [
                'value' => 'material-icons balance',
                'label' => 'balance',
                'attributes' => [
                    'data-icon' => 'material-icons balance',
                    'data-icon-value' => 'balance',
                ],
            ],
            [
                'value' => 'material-icons balcony',
                'label' => 'balcony',
                'attributes' => [
                    'data-icon' => 'material-icons balcony',
                    'data-icon-value' => 'balcony',
                ],
            ],
            [
                'value' => 'material-icons ballot',
                'label' => 'ballot',
                'attributes' => [
                    'data-icon' => 'material-icons ballot',
                    'data-icon-value' => 'ballot',
                ],
            ],
            [
                'value' => 'material-icons bar_chart',
                'label' => 'bar_chart',
                'attributes' => [
                    'data-icon' => 'material-icons bar_chart',
                    'data-icon-value' => 'bar_chart',
                ],
            ],
            [
                'value' => 'material-icons batch_prediction',
                'label' => 'batch_prediction',
                'attributes' => [
                    'data-icon' => 'material-icons batch_prediction',
                    'data-icon-value' => 'batch_prediction',
                ],
            ],
            [
                'value' => 'material-icons bathroom',
                'label' => 'bathroom',
                'attributes' => [
                    'data-icon' => 'material-icons bathroom',
                    'data-icon-value' => 'bathroom',
                ],
            ],
            [
                'value' => 'material-icons bathtub',
                'label' => 'bathtub',
                'attributes' => [
                    'data-icon' => 'material-icons bathtub',
                    'data-icon-value' => 'bathtub',
                ],
            ],
            [
                'value' => 'material-icons battery_0_bar',
                'label' => 'battery_0_bar',
                'attributes' => [
                    'data-icon' => 'material-icons battery_0_bar',
                    'data-icon-value' => 'battery_0_bar',
                ],
            ],
            [
                'value' => 'material-icons battery_1_bar',
                'label' => 'battery_1_bar',
                'attributes' => [
                    'data-icon' => 'material-icons battery_1_bar',
                    'data-icon-value' => 'battery_1_bar',
                ],
            ],
            [
                'value' => 'material-icons battery_2_bar',
                'label' => 'battery_2_bar',
                'attributes' => [
                    'data-icon' => 'material-icons battery_2_bar',
                    'data-icon-value' => 'battery_2_bar',
                ],
            ],
            [
                'value' => 'material-icons battery_3_bar',
                'label' => 'battery_3_bar',
                'attributes' => [
                    'data-icon' => 'material-icons battery_3_bar',
                    'data-icon-value' => 'battery_3_bar',
                ],
            ],
            [
                'value' => 'material-icons battery_4_bar',
                'label' => 'battery_4_bar',
                'attributes' => [
                    'data-icon' => 'material-icons battery_4_bar',
                    'data-icon-value' => 'battery_4_bar',
                ],
            ],
            [
                'value' => 'material-icons battery_5_bar',
                'label' => 'battery_5_bar',
                'attributes' => [
                    'data-icon' => 'material-icons battery_5_bar',
                    'data-icon-value' => 'battery_5_bar',
                ],
            ],
            [
                'value' => 'material-icons battery_6_bar',
                'label' => 'battery_6_bar',
                'attributes' => [
                    'data-icon' => 'material-icons battery_6_bar',
                    'data-icon-value' => 'battery_6_bar',
                ],
            ],
            [
                'value' => 'material-icons battery_alert',
                'label' => 'battery_alert',
                'attributes' => [
                    'data-icon' => 'material-icons battery_alert',
                    'data-icon-value' => 'battery_alert',
                ],
            ],
            [
                'value' => 'material-icons battery_charging_full',
                'label' => 'battery_charging_full',
                'attributes' => [
                    'data-icon' => 'material-icons battery_charging_full',
                    'data-icon-value' => 'battery_charging_full',
                ],
            ],
            [
                'value' => 'material-icons battery_full',
                'label' => 'battery_full',
                'attributes' => [
                    'data-icon' => 'material-icons battery_full',
                    'data-icon-value' => 'battery_full',
                ],
            ],
            [
                'value' => 'material-icons battery_saver',
                'label' => 'battery_saver',
                'attributes' => [
                    'data-icon' => 'material-icons battery_saver',
                    'data-icon-value' => 'battery_saver',
                ],
            ],
            [
                'value' => 'material-icons battery_std',
                'label' => 'battery_std',
                'attributes' => [
                    'data-icon' => 'material-icons battery_std',
                    'data-icon-value' => 'battery_std',
                ],
            ],
            [
                'value' => 'material-icons battery_unknown',
                'label' => 'battery_unknown',
                'attributes' => [
                    'data-icon' => 'material-icons battery_unknown',
                    'data-icon-value' => 'battery_unknown',
                ],
            ],
            [
                'value' => 'material-icons beach_access',
                'label' => 'beach_access',
                'attributes' => [
                    'data-icon' => 'material-icons beach_access',
                    'data-icon-value' => 'beach_access',
                ],
            ],
            [
                'value' => 'material-icons bed',
                'label' => 'bed',
                'attributes' => [
                    'data-icon' => 'material-icons bed',
                    'data-icon-value' => 'bed',
                ],
            ],
            [
                'value' => 'material-icons bedroom_baby',
                'label' => 'bedroom_baby',
                'attributes' => [
                    'data-icon' => 'material-icons bedroom_baby',
                    'data-icon-value' => 'bedroom_baby',
                ],
            ],
            [
                'value' => 'material-icons bedroom_child',
                'label' => 'bedroom_child',
                'attributes' => [
                    'data-icon' => 'material-icons bedroom_child',
                    'data-icon-value' => 'bedroom_child',
                ],
            ],
            [
                'value' => 'material-icons bedroom_parent',
                'label' => 'bedroom_parent',
                'attributes' => [
                    'data-icon' => 'material-icons bedroom_parent',
                    'data-icon-value' => 'bedroom_parent',
                ],
            ],
            [
                'value' => 'material-icons bedtime',
                'label' => 'bedtime',
                'attributes' => [
                    'data-icon' => 'material-icons bedtime',
                    'data-icon-value' => 'bedtime',
                ],
            ],
            [
                'value' => 'material-icons bedtime_off',
                'label' => 'bedtime_off',
                'attributes' => [
                    'data-icon' => 'material-icons bedtime_off',
                    'data-icon-value' => 'bedtime_off',
                ],
            ],
            [
                'value' => 'material-icons beenhere',
                'label' => 'beenhere',
                'attributes' => [
                    'data-icon' => 'material-icons beenhere',
                    'data-icon-value' => 'beenhere',
                ],
            ],
            [
                'value' => 'material-icons bento',
                'label' => 'bento',
                'attributes' => [
                    'data-icon' => 'material-icons bento',
                    'data-icon-value' => 'bento',
                ],
            ],
            [
                'value' => 'material-icons bike_scooter',
                'label' => 'bike_scooter',
                'attributes' => [
                    'data-icon' => 'material-icons bike_scooter',
                    'data-icon-value' => 'bike_scooter',
                ],
            ],
            [
                'value' => 'material-icons biotech',
                'label' => 'biotech',
                'attributes' => [
                    'data-icon' => 'material-icons biotech',
                    'data-icon-value' => 'biotech',
                ],
            ],
            [
                'value' => 'material-icons blender',
                'label' => 'blender',
                'attributes' => [
                    'data-icon' => 'material-icons blender',
                    'data-icon-value' => 'blender',
                ],
            ],
            [
                'value' => 'material-icons blinds',
                'label' => 'blinds',
                'attributes' => [
                    'data-icon' => 'material-icons blinds',
                    'data-icon-value' => 'blinds',
                ],
            ],
            [
                'value' => 'material-icons blinds_closed',
                'label' => 'blinds_closed',
                'attributes' => [
                    'data-icon' => 'material-icons blinds_closed',
                    'data-icon-value' => 'blinds_closed',
                ],
            ],
            [
                'value' => 'material-icons block',
                'label' => 'block',
                'attributes' => [
                    'data-icon' => 'material-icons block',
                    'data-icon-value' => 'block',
                ],
            ],
            [
                'value' => 'material-icons block_flipped',
                'label' => 'block_flipped',
                'attributes' => [
                    'data-icon' => 'material-icons block_flipped',
                    'data-icon-value' => 'block_flipped',
                ],
            ],
            [
                'value' => 'material-icons bloodtype',
                'label' => 'bloodtype',
                'attributes' => [
                    'data-icon' => 'material-icons bloodtype',
                    'data-icon-value' => 'bloodtype',
                ],
            ],
            [
                'value' => 'material-icons bluetooth',
                'label' => 'bluetooth',
                'attributes' => [
                    'data-icon' => 'material-icons bluetooth',
                    'data-icon-value' => 'bluetooth',
                ],
            ],
            [
                'value' => 'material-icons bluetooth_audio',
                'label' => 'bluetooth_audio',
                'attributes' => [
                    'data-icon' => 'material-icons bluetooth_audio',
                    'data-icon-value' => 'bluetooth_audio',
                ],
            ],
            [
                'value' => 'material-icons bluetooth_connected',
                'label' => 'bluetooth_connected',
                'attributes' => [
                    'data-icon' => 'material-icons bluetooth_connected',
                    'data-icon-value' => 'bluetooth_connected',
                ],
            ],
            [
                'value' => 'material-icons bluetooth_disabled',
                'label' => 'bluetooth_disabled',
                'attributes' => [
                    'data-icon' => 'material-icons bluetooth_disabled',
                    'data-icon-value' => 'bluetooth_disabled',
                ],
            ],
            [
                'value' => 'material-icons bluetooth_drive',
                'label' => 'bluetooth_drive',
                'attributes' => [
                    'data-icon' => 'material-icons bluetooth_drive',
                    'data-icon-value' => 'bluetooth_drive',
                ],
            ],
            [
                'value' => 'material-icons bluetooth_searching',
                'label' => 'bluetooth_searching',
                'attributes' => [
                    'data-icon' => 'material-icons bluetooth_searching',
                    'data-icon-value' => 'bluetooth_searching',
                ],
            ],
            [
                'value' => 'material-icons blur_circular',
                'label' => 'blur_circular',
                'attributes' => [
                    'data-icon' => 'material-icons blur_circular',
                    'data-icon-value' => 'blur_circular',
                ],
            ],
            [
                'value' => 'material-icons blur_linear',
                'label' => 'blur_linear',
                'attributes' => [
                    'data-icon' => 'material-icons blur_linear',
                    'data-icon-value' => 'blur_linear',
                ],
            ],
            [
                'value' => 'material-icons blur_off',
                'label' => 'blur_off',
                'attributes' => [
                    'data-icon' => 'material-icons blur_off',
                    'data-icon-value' => 'blur_off',
                ],
            ],
            [
                'value' => 'material-icons blur_on',
                'label' => 'blur_on',
                'attributes' => [
                    'data-icon' => 'material-icons blur_on',
                    'data-icon-value' => 'blur_on',
                ],
            ],
            [
                'value' => 'material-icons bolt',
                'label' => 'bolt',
                'attributes' => [
                    'data-icon' => 'material-icons bolt',
                    'data-icon-value' => 'bolt',
                ],
            ],
            [
                'value' => 'material-icons book',
                'label' => 'book',
                'attributes' => [
                    'data-icon' => 'material-icons book',
                    'data-icon-value' => 'book',
                ],
            ],
            [
                'value' => 'material-icons book_online',
                'label' => 'book_online',
                'attributes' => [
                    'data-icon' => 'material-icons book_online',
                    'data-icon-value' => 'book_online',
                ],
            ],
            [
                'value' => 'material-icons bookmark',
                'label' => 'bookmark',
                'attributes' => [
                    'data-icon' => 'material-icons bookmark',
                    'data-icon-value' => 'bookmark',
                ],
            ],
            [
                'value' => 'material-icons bookmark_add',
                'label' => 'bookmark_add',
                'attributes' => [
                    'data-icon' => 'material-icons bookmark_add',
                    'data-icon-value' => 'bookmark_add',
                ],
            ],
            [
                'value' => 'material-icons bookmark_added',
                'label' => 'bookmark_added',
                'attributes' => [
                    'data-icon' => 'material-icons bookmark_added',
                    'data-icon-value' => 'bookmark_added',
                ],
            ],
            [
                'value' => 'material-icons bookmark_border',
                'label' => 'bookmark_border',
                'attributes' => [
                    'data-icon' => 'material-icons bookmark_border',
                    'data-icon-value' => 'bookmark_border',
                ],
            ],
            [
                'value' => 'material-icons bookmark_outline',
                'label' => 'bookmark_outline',
                'attributes' => [
                    'data-icon' => 'material-icons bookmark_outline',
                    'data-icon-value' => 'bookmark_outline',
                ],
            ],
            [
                'value' => 'material-icons bookmark_remove',
                'label' => 'bookmark_remove',
                'attributes' => [
                    'data-icon' => 'material-icons bookmark_remove',
                    'data-icon-value' => 'bookmark_remove',
                ],
            ],
            [
                'value' => 'material-icons bookmarks',
                'label' => 'bookmarks',
                'attributes' => [
                    'data-icon' => 'material-icons bookmarks',
                    'data-icon-value' => 'bookmarks',
                ],
            ],
            [
                'value' => 'material-icons border_all',
                'label' => 'border_all',
                'attributes' => [
                    'data-icon' => 'material-icons border_all',
                    'data-icon-value' => 'border_all',
                ],
            ],
            [
                'value' => 'material-icons border_bottom',
                'label' => 'border_bottom',
                'attributes' => [
                    'data-icon' => 'material-icons border_bottom',
                    'data-icon-value' => 'border_bottom',
                ],
            ],
            [
                'value' => 'material-icons border_clear',
                'label' => 'border_clear',
                'attributes' => [
                    'data-icon' => 'material-icons border_clear',
                    'data-icon-value' => 'border_clear',
                ],
            ],
            [
                'value' => 'material-icons border_color',
                'label' => 'border_color',
                'attributes' => [
                    'data-icon' => 'material-icons border_color',
                    'data-icon-value' => 'border_color',
                ],
            ],
            [
                'value' => 'material-icons border_horizontal',
                'label' => 'border_horizontal',
                'attributes' => [
                    'data-icon' => 'material-icons border_horizontal',
                    'data-icon-value' => 'border_horizontal',
                ],
            ],
            [
                'value' => 'material-icons border_inner',
                'label' => 'border_inner',
                'attributes' => [
                    'data-icon' => 'material-icons border_inner',
                    'data-icon-value' => 'border_inner',
                ],
            ],
            [
                'value' => 'material-icons border_left',
                'label' => 'border_left',
                'attributes' => [
                    'data-icon' => 'material-icons border_left',
                    'data-icon-value' => 'border_left',
                ],
            ],
            [
                'value' => 'material-icons border_outer',
                'label' => 'border_outer',
                'attributes' => [
                    'data-icon' => 'material-icons border_outer',
                    'data-icon-value' => 'border_outer',
                ],
            ],
            [
                'value' => 'material-icons border_right',
                'label' => 'border_right',
                'attributes' => [
                    'data-icon' => 'material-icons border_right',
                    'data-icon-value' => 'border_right',
                ],
            ],
            [
                'value' => 'material-icons border_style',
                'label' => 'border_style',
                'attributes' => [
                    'data-icon' => 'material-icons border_style',
                    'data-icon-value' => 'border_style',
                ],
            ],
            [
                'value' => 'material-icons border_top',
                'label' => 'border_top',
                'attributes' => [
                    'data-icon' => 'material-icons border_top',
                    'data-icon-value' => 'border_top',
                ],
            ],
            [
                'value' => 'material-icons border_vertical',
                'label' => 'border_vertical',
                'attributes' => [
                    'data-icon' => 'material-icons border_vertical',
                    'data-icon-value' => 'border_vertical',
                ],
            ],
            [
                'value' => 'material-icons boy',
                'label' => 'boy',
                'attributes' => [
                    'data-icon' => 'material-icons boy',
                    'data-icon-value' => 'boy',
                ],
            ],
            [
                'value' => 'material-icons branding_watermark',
                'label' => 'branding_watermark',
                'attributes' => [
                    'data-icon' => 'material-icons branding_watermark',
                    'data-icon-value' => 'branding_watermark',
                ],
            ],
            [
                'value' => 'material-icons breakfast_dining',
                'label' => 'breakfast_dining',
                'attributes' => [
                    'data-icon' => 'material-icons breakfast_dining',
                    'data-icon-value' => 'breakfast_dining',
                ],
            ],
            [
                'value' => 'material-icons brightness_1',
                'label' => 'brightness_1',
                'attributes' => [
                    'data-icon' => 'material-icons brightness_1',
                    'data-icon-value' => 'brightness_1',
                ],
            ],
            [
                'value' => 'material-icons brightness_2',
                'label' => 'brightness_2',
                'attributes' => [
                    'data-icon' => 'material-icons brightness_2',
                    'data-icon-value' => 'brightness_2',
                ],
            ],
            [
                'value' => 'material-icons brightness_3',
                'label' => 'brightness_3',
                'attributes' => [
                    'data-icon' => 'material-icons brightness_3',
                    'data-icon-value' => 'brightness_3',
                ],
            ],
            [
                'value' => 'material-icons brightness_4',
                'label' => 'brightness_4',
                'attributes' => [
                    'data-icon' => 'material-icons brightness_4',
                    'data-icon-value' => 'brightness_4',
                ],
            ],
            [
                'value' => 'material-icons brightness_5',
                'label' => 'brightness_5',
                'attributes' => [
                    'data-icon' => 'material-icons brightness_5',
                    'data-icon-value' => 'brightness_5',
                ],
            ],
            [
                'value' => 'material-icons brightness_6',
                'label' => 'brightness_6',
                'attributes' => [
                    'data-icon' => 'material-icons brightness_6',
                    'data-icon-value' => 'brightness_6',
                ],
            ],
            [
                'value' => 'material-icons brightness_7',
                'label' => 'brightness_7',
                'attributes' => [
                    'data-icon' => 'material-icons brightness_7',
                    'data-icon-value' => 'brightness_7',
                ],
            ],
            [
                'value' => 'material-icons brightness_auto',
                'label' => 'brightness_auto',
                'attributes' => [
                    'data-icon' => 'material-icons brightness_auto',
                    'data-icon-value' => 'brightness_auto',
                ],
            ],
            [
                'value' => 'material-icons brightness_high',
                'label' => 'brightness_high',
                'attributes' => [
                    'data-icon' => 'material-icons brightness_high',
                    'data-icon-value' => 'brightness_high',
                ],
            ],
            [
                'value' => 'material-icons brightness_low',
                'label' => 'brightness_low',
                'attributes' => [
                    'data-icon' => 'material-icons brightness_low',
                    'data-icon-value' => 'brightness_low',
                ],
            ],
            [
                'value' => 'material-icons brightness_medium',
                'label' => 'brightness_medium',
                'attributes' => [
                    'data-icon' => 'material-icons brightness_medium',
                    'data-icon-value' => 'brightness_medium',
                ],
            ],
            [
                'value' => 'material-icons broadcast_on_home',
                'label' => 'broadcast_on_home',
                'attributes' => [
                    'data-icon' => 'material-icons broadcast_on_home',
                    'data-icon-value' => 'broadcast_on_home',
                ],
            ],
            [
                'value' => 'material-icons broadcast_on_personal',
                'label' => 'broadcast_on_personal',
                'attributes' => [
                    'data-icon' => 'material-icons broadcast_on_personal',
                    'data-icon-value' => 'broadcast_on_personal',
                ],
            ],
            [
                'value' => 'material-icons broken_image',
                'label' => 'broken_image',
                'attributes' => [
                    'data-icon' => 'material-icons broken_image',
                    'data-icon-value' => 'broken_image',
                ],
            ],
            [
                'value' => 'material-icons browse_gallery',
                'label' => 'browse_gallery',
                'attributes' => [
                    'data-icon' => 'material-icons browse_gallery',
                    'data-icon-value' => 'browse_gallery',
                ],
            ],
            [
                'value' => 'material-icons browser_not_supported',
                'label' => 'browser_not_supported',
                'attributes' => [
                    'data-icon' => 'material-icons browser_not_supported',
                    'data-icon-value' => 'browser_not_supported',
                ],
            ],
            [
                'value' => 'material-icons browser_updated',
                'label' => 'browser_updated',
                'attributes' => [
                    'data-icon' => 'material-icons browser_updated',
                    'data-icon-value' => 'browser_updated',
                ],
            ],
            [
                'value' => 'material-icons brunch_dining',
                'label' => 'brunch_dining',
                'attributes' => [
                    'data-icon' => 'material-icons brunch_dining',
                    'data-icon-value' => 'brunch_dining',
                ],
            ],
            [
                'value' => 'material-icons brush',
                'label' => 'brush',
                'attributes' => [
                    'data-icon' => 'material-icons brush',
                    'data-icon-value' => 'brush',
                ],
            ],
            [
                'value' => 'material-icons bubble_chart',
                'label' => 'bubble_chart',
                'attributes' => [
                    'data-icon' => 'material-icons bubble_chart',
                    'data-icon-value' => 'bubble_chart',
                ],
            ],
            [
                'value' => 'material-icons bug_report',
                'label' => 'bug_report',
                'attributes' => [
                    'data-icon' => 'material-icons bug_report',
                    'data-icon-value' => 'bug_report',
                ],
            ],
            [
                'value' => 'material-icons build',
                'label' => 'build',
                'attributes' => [
                    'data-icon' => 'material-icons build',
                    'data-icon-value' => 'build',
                ],
            ],
            [
                'value' => 'material-icons build_circle',
                'label' => 'build_circle',
                'attributes' => [
                    'data-icon' => 'material-icons build_circle',
                    'data-icon-value' => 'build_circle',
                ],
            ],
            [
                'value' => 'material-icons bungalow',
                'label' => 'bungalow',
                'attributes' => [
                    'data-icon' => 'material-icons bungalow',
                    'data-icon-value' => 'bungalow',
                ],
            ],
            [
                'value' => 'material-icons burst_mode',
                'label' => 'burst_mode',
                'attributes' => [
                    'data-icon' => 'material-icons burst_mode',
                    'data-icon-value' => 'burst_mode',
                ],
            ],
            [
                'value' => 'material-icons bus_alert',
                'label' => 'bus_alert',
                'attributes' => [
                    'data-icon' => 'material-icons bus_alert',
                    'data-icon-value' => 'bus_alert',
                ],
            ],
            [
                'value' => 'material-icons business',
                'label' => 'business',
                'attributes' => [
                    'data-icon' => 'material-icons business',
                    'data-icon-value' => 'business',
                ],
            ],
            [
                'value' => 'material-icons business_center',
                'label' => 'business_center',
                'attributes' => [
                    'data-icon' => 'material-icons business_center',
                    'data-icon-value' => 'business_center',
                ],
            ],
            [
                'value' => 'material-icons cabin',
                'label' => 'cabin',
                'attributes' => [
                    'data-icon' => 'material-icons cabin',
                    'data-icon-value' => 'cabin',
                ],
            ],
            [
                'value' => 'material-icons cable',
                'label' => 'cable',
                'attributes' => [
                    'data-icon' => 'material-icons cable',
                    'data-icon-value' => 'cable',
                ],
            ],
            [
                'value' => 'material-icons cached',
                'label' => 'cached',
                'attributes' => [
                    'data-icon' => 'material-icons cached',
                    'data-icon-value' => 'cached',
                ],
            ],
            [
                'value' => 'material-icons cake',
                'label' => 'cake',
                'attributes' => [
                    'data-icon' => 'material-icons cake',
                    'data-icon-value' => 'cake',
                ],
            ],
            [
                'value' => 'material-icons calculate',
                'label' => 'calculate',
                'attributes' => [
                    'data-icon' => 'material-icons calculate',
                    'data-icon-value' => 'calculate',
                ],
            ],
            [
                'value' => 'material-icons calendar_month',
                'label' => 'calendar_month',
                'attributes' => [
                    'data-icon' => 'material-icons calendar_month',
                    'data-icon-value' => 'calendar_month',
                ],
            ],
            [
                'value' => 'material-icons calendar_today',
                'label' => 'calendar_today',
                'attributes' => [
                    'data-icon' => 'material-icons calendar_today',
                    'data-icon-value' => 'calendar_today',
                ],
            ],
            [
                'value' => 'material-icons calendar_view_day',
                'label' => 'calendar_view_day',
                'attributes' => [
                    'data-icon' => 'material-icons calendar_view_day',
                    'data-icon-value' => 'calendar_view_day',
                ],
            ],
            [
                'value' => 'material-icons calendar_view_month',
                'label' => 'calendar_view_month',
                'attributes' => [
                    'data-icon' => 'material-icons calendar_view_month',
                    'data-icon-value' => 'calendar_view_month',
                ],
            ],
            [
                'value' => 'material-icons calendar_view_week',
                'label' => 'calendar_view_week',
                'attributes' => [
                    'data-icon' => 'material-icons calendar_view_week',
                    'data-icon-value' => 'calendar_view_week',
                ],
            ],
            [
                'value' => 'material-icons call',
                'label' => 'call',
                'attributes' => [
                    'data-icon' => 'material-icons call',
                    'data-icon-value' => 'call',
                ],
            ],
            [
                'value' => 'material-icons call_end',
                'label' => 'call_end',
                'attributes' => [
                    'data-icon' => 'material-icons call_end',
                    'data-icon-value' => 'call_end',
                ],
            ],
            [
                'value' => 'material-icons call_made',
                'label' => 'call_made',
                'attributes' => [
                    'data-icon' => 'material-icons call_made',
                    'data-icon-value' => 'call_made',
                ],
            ],
            [
                'value' => 'material-icons call_merge',
                'label' => 'call_merge',
                'attributes' => [
                    'data-icon' => 'material-icons call_merge',
                    'data-icon-value' => 'call_merge',
                ],
            ],
            [
                'value' => 'material-icons call_missed',
                'label' => 'call_missed',
                'attributes' => [
                    'data-icon' => 'material-icons call_missed',
                    'data-icon-value' => 'call_missed',
                ],
            ],
            [
                'value' => 'material-icons call_missed_outgoing',
                'label' => 'call_missed_outgoing',
                'attributes' => [
                    'data-icon' => 'material-icons call_missed_outgoing',
                    'data-icon-value' => 'call_missed_outgoing',
                ],
            ],
            [
                'value' => 'material-icons call_received',
                'label' => 'call_received',
                'attributes' => [
                    'data-icon' => 'material-icons call_received',
                    'data-icon-value' => 'call_received',
                ],
            ],
            [
                'value' => 'material-icons call_split',
                'label' => 'call_split',
                'attributes' => [
                    'data-icon' => 'material-icons call_split',
                    'data-icon-value' => 'call_split',
                ],
            ],
            [
                'value' => 'material-icons call_to_action',
                'label' => 'call_to_action',
                'attributes' => [
                    'data-icon' => 'material-icons call_to_action',
                    'data-icon-value' => 'call_to_action',
                ],
            ],
            [
                'value' => 'material-icons camera',
                'label' => 'camera',
                'attributes' => [
                    'data-icon' => 'material-icons camera',
                    'data-icon-value' => 'camera',
                ],
            ],
            [
                'value' => 'material-icons camera_alt',
                'label' => 'camera_alt',
                'attributes' => [
                    'data-icon' => 'material-icons camera_alt',
                    'data-icon-value' => 'camera_alt',
                ],
            ],
            [
                'value' => 'material-icons camera_enhance',
                'label' => 'camera_enhance',
                'attributes' => [
                    'data-icon' => 'material-icons camera_enhance',
                    'data-icon-value' => 'camera_enhance',
                ],
            ],
            [
                'value' => 'material-icons camera_front',
                'label' => 'camera_front',
                'attributes' => [
                    'data-icon' => 'material-icons camera_front',
                    'data-icon-value' => 'camera_front',
                ],
            ],
            [
                'value' => 'material-icons camera_indoor',
                'label' => 'camera_indoor',
                'attributes' => [
                    'data-icon' => 'material-icons camera_indoor',
                    'data-icon-value' => 'camera_indoor',
                ],
            ],
            [
                'value' => 'material-icons camera_outdoor',
                'label' => 'camera_outdoor',
                'attributes' => [
                    'data-icon' => 'material-icons camera_outdoor',
                    'data-icon-value' => 'camera_outdoor',
                ],
            ],
            [
                'value' => 'material-icons camera_rear',
                'label' => 'camera_rear',
                'attributes' => [
                    'data-icon' => 'material-icons camera_rear',
                    'data-icon-value' => 'camera_rear',
                ],
            ],
            [
                'value' => 'material-icons camera_roll',
                'label' => 'camera_roll',
                'attributes' => [
                    'data-icon' => 'material-icons camera_roll',
                    'data-icon-value' => 'camera_roll',
                ],
            ],
            [
                'value' => 'material-icons cameraswitch',
                'label' => 'cameraswitch',
                'attributes' => [
                    'data-icon' => 'material-icons cameraswitch',
                    'data-icon-value' => 'cameraswitch',
                ],
            ],
            [
                'value' => 'material-icons campaign',
                'label' => 'campaign',
                'attributes' => [
                    'data-icon' => 'material-icons campaign',
                    'data-icon-value' => 'campaign',
                ],
            ],
            [
                'value' => 'material-icons cancel',
                'label' => 'cancel',
                'attributes' => [
                    'data-icon' => 'material-icons cancel',
                    'data-icon-value' => 'cancel',
                ],
            ],
            [
                'value' => 'material-icons cancel_presentation',
                'label' => 'cancel_presentation',
                'attributes' => [
                    'data-icon' => 'material-icons cancel_presentation',
                    'data-icon-value' => 'cancel_presentation',
                ],
            ],
            [
                'value' => 'material-icons cancel_schedule_send',
                'label' => 'cancel_schedule_send',
                'attributes' => [
                    'data-icon' => 'material-icons cancel_schedule_send',
                    'data-icon-value' => 'cancel_schedule_send',
                ],
            ],
            [
                'value' => 'material-icons candlestick_chart',
                'label' => 'candlestick_chart',
                'attributes' => [
                    'data-icon' => 'material-icons candlestick_chart',
                    'data-icon-value' => 'candlestick_chart',
                ],
            ],
            [
                'value' => 'material-icons car_crash',
                'label' => 'car_crash',
                'attributes' => [
                    'data-icon' => 'material-icons car_crash',
                    'data-icon-value' => 'car_crash',
                ],
            ],
            [
                'value' => 'material-icons car_rental',
                'label' => 'car_rental',
                'attributes' => [
                    'data-icon' => 'material-icons car_rental',
                    'data-icon-value' => 'car_rental',
                ],
            ],
            [
                'value' => 'material-icons car_repair',
                'label' => 'car_repair',
                'attributes' => [
                    'data-icon' => 'material-icons car_repair',
                    'data-icon-value' => 'car_repair',
                ],
            ],
            [
                'value' => 'material-icons card_giftcard',
                'label' => 'card_giftcard',
                'attributes' => [
                    'data-icon' => 'material-icons card_giftcard',
                    'data-icon-value' => 'card_giftcard',
                ],
            ],
            [
                'value' => 'material-icons card_membership',
                'label' => 'card_membership',
                'attributes' => [
                    'data-icon' => 'material-icons card_membership',
                    'data-icon-value' => 'card_membership',
                ],
            ],
            [
                'value' => 'material-icons card_travel',
                'label' => 'card_travel',
                'attributes' => [
                    'data-icon' => 'material-icons card_travel',
                    'data-icon-value' => 'card_travel',
                ],
            ],
            [
                'value' => 'material-icons carpenter',
                'label' => 'carpenter',
                'attributes' => [
                    'data-icon' => 'material-icons carpenter',
                    'data-icon-value' => 'carpenter',
                ],
            ],
            [
                'value' => 'material-icons cases',
                'label' => 'cases',
                'attributes' => [
                    'data-icon' => 'material-icons cases',
                    'data-icon-value' => 'cases',
                ],
            ],
            [
                'value' => 'material-icons casino',
                'label' => 'casino',
                'attributes' => [
                    'data-icon' => 'material-icons casino',
                    'data-icon-value' => 'casino',
                ],
            ],
            [
                'value' => 'material-icons cast',
                'label' => 'cast',
                'attributes' => [
                    'data-icon' => 'material-icons cast',
                    'data-icon-value' => 'cast',
                ],
            ],
            [
                'value' => 'material-icons cast_connected',
                'label' => 'cast_connected',
                'attributes' => [
                    'data-icon' => 'material-icons cast_connected',
                    'data-icon-value' => 'cast_connected',
                ],
            ],
            [
                'value' => 'material-icons cast_for_education',
                'label' => 'cast_for_education',
                'attributes' => [
                    'data-icon' => 'material-icons cast_for_education',
                    'data-icon-value' => 'cast_for_education',
                ],
            ],
            [
                'value' => 'material-icons castle',
                'label' => 'castle',
                'attributes' => [
                    'data-icon' => 'material-icons castle',
                    'data-icon-value' => 'castle',
                ],
            ],
            [
                'value' => 'material-icons catching_pokemon',
                'label' => 'catching_pokemon',
                'attributes' => [
                    'data-icon' => 'material-icons catching_pokemon',
                    'data-icon-value' => 'catching_pokemon',
                ],
            ],
            [
                'value' => 'material-icons category',
                'label' => 'category',
                'attributes' => [
                    'data-icon' => 'material-icons category',
                    'data-icon-value' => 'category',
                ],
            ],
            [
                'value' => 'material-icons celebration',
                'label' => 'celebration',
                'attributes' => [
                    'data-icon' => 'material-icons celebration',
                    'data-icon-value' => 'celebration',
                ],
            ],
            [
                'value' => 'material-icons cell_tower',
                'label' => 'cell_tower',
                'attributes' => [
                    'data-icon' => 'material-icons cell_tower',
                    'data-icon-value' => 'cell_tower',
                ],
            ],
            [
                'value' => 'material-icons cell_wifi',
                'label' => 'cell_wifi',
                'attributes' => [
                    'data-icon' => 'material-icons cell_wifi',
                    'data-icon-value' => 'cell_wifi',
                ],
            ],
            [
                'value' => 'material-icons center_focus_strong',
                'label' => 'center_focus_strong',
                'attributes' => [
                    'data-icon' => 'material-icons center_focus_strong',
                    'data-icon-value' => 'center_focus_strong',
                ],
            ],
            [
                'value' => 'material-icons center_focus_weak',
                'label' => 'center_focus_weak',
                'attributes' => [
                    'data-icon' => 'material-icons center_focus_weak',
                    'data-icon-value' => 'center_focus_weak',
                ],
            ],
            [
                'value' => 'material-icons chair',
                'label' => 'chair',
                'attributes' => [
                    'data-icon' => 'material-icons chair',
                    'data-icon-value' => 'chair',
                ],
            ],
            [
                'value' => 'material-icons chair_alt',
                'label' => 'chair_alt',
                'attributes' => [
                    'data-icon' => 'material-icons chair_alt',
                    'data-icon-value' => 'chair_alt',
                ],
            ],
            [
                'value' => 'material-icons chalet',
                'label' => 'chalet',
                'attributes' => [
                    'data-icon' => 'material-icons chalet',
                    'data-icon-value' => 'chalet',
                ],
            ],
            [
                'value' => 'material-icons change_circle',
                'label' => 'change_circle',
                'attributes' => [
                    'data-icon' => 'material-icons change_circle',
                    'data-icon-value' => 'change_circle',
                ],
            ],
            [
                'value' => 'material-icons change_history',
                'label' => 'change_history',
                'attributes' => [
                    'data-icon' => 'material-icons change_history',
                    'data-icon-value' => 'change_history',
                ],
            ],
            [
                'value' => 'material-icons charging_station',
                'label' => 'charging_station',
                'attributes' => [
                    'data-icon' => 'material-icons charging_station',
                    'data-icon-value' => 'charging_station',
                ],
            ],
            [
                'value' => 'material-icons chat',
                'label' => 'chat',
                'attributes' => [
                    'data-icon' => 'material-icons chat',
                    'data-icon-value' => 'chat',
                ],
            ],
            [
                'value' => 'material-icons chat_bubble',
                'label' => 'chat_bubble',
                'attributes' => [
                    'data-icon' => 'material-icons chat_bubble',
                    'data-icon-value' => 'chat_bubble',
                ],
            ],
            [
                'value' => 'material-icons chat_bubble_outline',
                'label' => 'chat_bubble_outline',
                'attributes' => [
                    'data-icon' => 'material-icons chat_bubble_outline',
                    'data-icon-value' => 'chat_bubble_outline',
                ],
            ],
            [
                'value' => 'material-icons check',
                'label' => 'check',
                'attributes' => [
                    'data-icon' => 'material-icons check',
                    'data-icon-value' => 'check',
                ],
            ],
            [
                'value' => 'material-icons check_box',
                'label' => 'check_box',
                'attributes' => [
                    'data-icon' => 'material-icons check_box',
                    'data-icon-value' => 'check_box',
                ],
            ],
            [
                'value' => 'material-icons check_box_outline_blank',
                'label' => 'check_box_outline_blank',
                'attributes' => [
                    'data-icon' => 'material-icons check_box_outline_blank',
                    'data-icon-value' => 'check_box_outline_blank',
                ],
            ],
            [
                'value' => 'material-icons check_circle',
                'label' => 'check_circle',
                'attributes' => [
                    'data-icon' => 'material-icons check_circle',
                    'data-icon-value' => 'check_circle',
                ],
            ],
            [
                'value' => 'material-icons check_circle_outline',
                'label' => 'check_circle_outline',
                'attributes' => [
                    'data-icon' => 'material-icons check_circle_outline',
                    'data-icon-value' => 'check_circle_outline',
                ],
            ],
            [
                'value' => 'material-icons checklist',
                'label' => 'checklist',
                'attributes' => [
                    'data-icon' => 'material-icons checklist',
                    'data-icon-value' => 'checklist',
                ],
            ],
            [
                'value' => 'material-icons checklist_rtl',
                'label' => 'checklist_rtl',
                'attributes' => [
                    'data-icon' => 'material-icons checklist_rtl',
                    'data-icon-value' => 'checklist_rtl',
                ],
            ],
            [
                'value' => 'material-icons checkroom',
                'label' => 'checkroom',
                'attributes' => [
                    'data-icon' => 'material-icons checkroom',
                    'data-icon-value' => 'checkroom',
                ],
            ],
            [
                'value' => 'material-icons chevron_left',
                'label' => 'chevron_left',
                'attributes' => [
                    'data-icon' => 'material-icons chevron_left',
                    'data-icon-value' => 'chevron_left',
                ],
            ],
            [
                'value' => 'material-icons chevron_right',
                'label' => 'chevron_right',
                'attributes' => [
                    'data-icon' => 'material-icons chevron_right',
                    'data-icon-value' => 'chevron_right',
                ],
            ],
            [
                'value' => 'material-icons child_care',
                'label' => 'child_care',
                'attributes' => [
                    'data-icon' => 'material-icons child_care',
                    'data-icon-value' => 'child_care',
                ],
            ],
            [
                'value' => 'material-icons child_friendly',
                'label' => 'child_friendly',
                'attributes' => [
                    'data-icon' => 'material-icons child_friendly',
                    'data-icon-value' => 'child_friendly',
                ],
            ],
            [
                'value' => 'material-icons chrome_reader_mode',
                'label' => 'chrome_reader_mode',
                'attributes' => [
                    'data-icon' => 'material-icons chrome_reader_mode',
                    'data-icon-value' => 'chrome_reader_mode',
                ],
            ],
            [
                'value' => 'material-icons church',
                'label' => 'church',
                'attributes' => [
                    'data-icon' => 'material-icons church',
                    'data-icon-value' => 'church',
                ],
            ],
            [
                'value' => 'material-icons circle',
                'label' => 'circle',
                'attributes' => [
                    'data-icon' => 'material-icons circle',
                    'data-icon-value' => 'circle',
                ],
            ],
            [
                'value' => 'material-icons circle_notifications',
                'label' => 'circle_notifications',
                'attributes' => [
                    'data-icon' => 'material-icons circle_notifications',
                    'data-icon-value' => 'circle_notifications',
                ],
            ],
            [
                'value' => 'material-icons class',
                'label' => 'class',
                'attributes' => [
                    'data-icon' => 'material-icons class',
                    'data-icon-value' => 'class',
                ],
            ],
            [
                'value' => 'material-icons clean_hands',
                'label' => 'clean_hands',
                'attributes' => [
                    'data-icon' => 'material-icons clean_hands',
                    'data-icon-value' => 'clean_hands',
                ],
            ],
            [
                'value' => 'material-icons cleaning_services',
                'label' => 'cleaning_services',
                'attributes' => [
                    'data-icon' => 'material-icons cleaning_services',
                    'data-icon-value' => 'cleaning_services',
                ],
            ],
            [
                'value' => 'material-icons clear',
                'label' => 'clear',
                'attributes' => [
                    'data-icon' => 'material-icons clear',
                    'data-icon-value' => 'clear',
                ],
            ],
            [
                'value' => 'material-icons clear_all',
                'label' => 'clear_all',
                'attributes' => [
                    'data-icon' => 'material-icons clear_all',
                    'data-icon-value' => 'clear_all',
                ],
            ],
            [
                'value' => 'material-icons close',
                'label' => 'close',
                'attributes' => [
                    'data-icon' => 'material-icons close',
                    'data-icon-value' => 'close',
                ],
            ],
            [
                'value' => 'material-icons close_fullscreen',
                'label' => 'close_fullscreen',
                'attributes' => [
                    'data-icon' => 'material-icons close_fullscreen',
                    'data-icon-value' => 'close_fullscreen',
                ],
            ],
            [
                'value' => 'material-icons closed_caption',
                'label' => 'closed_caption',
                'attributes' => [
                    'data-icon' => 'material-icons closed_caption',
                    'data-icon-value' => 'closed_caption',
                ],
            ],
            [
                'value' => 'material-icons closed_caption_disabled',
                'label' => 'closed_caption_disabled',
                'attributes' => [
                    'data-icon' => 'material-icons closed_caption_disabled',
                    'data-icon-value' => 'closed_caption_disabled',
                ],
            ],
            [
                'value' => 'material-icons closed_caption_off',
                'label' => 'closed_caption_off',
                'attributes' => [
                    'data-icon' => 'material-icons closed_caption_off',
                    'data-icon-value' => 'closed_caption_off',
                ],
            ],
            [
                'value' => 'material-icons cloud',
                'label' => 'cloud',
                'attributes' => [
                    'data-icon' => 'material-icons cloud',
                    'data-icon-value' => 'cloud',
                ],
            ],
            [
                'value' => 'material-icons cloud_circle',
                'label' => 'cloud_circle',
                'attributes' => [
                    'data-icon' => 'material-icons cloud_circle',
                    'data-icon-value' => 'cloud_circle',
                ],
            ],
            [
                'value' => 'material-icons cloud_done',
                'label' => 'cloud_done',
                'attributes' => [
                    'data-icon' => 'material-icons cloud_done',
                    'data-icon-value' => 'cloud_done',
                ],
            ],
            [
                'value' => 'material-icons cloud_download',
                'label' => 'cloud_download',
                'attributes' => [
                    'data-icon' => 'material-icons cloud_download',
                    'data-icon-value' => 'cloud_download',
                ],
            ],
            [
                'value' => 'material-icons cloud_off',
                'label' => 'cloud_off',
                'attributes' => [
                    'data-icon' => 'material-icons cloud_off',
                    'data-icon-value' => 'cloud_off',
                ],
            ],
            [
                'value' => 'material-icons cloud_queue',
                'label' => 'cloud_queue',
                'attributes' => [
                    'data-icon' => 'material-icons cloud_queue',
                    'data-icon-value' => 'cloud_queue',
                ],
            ],
            [
                'value' => 'material-icons cloud_sync',
                'label' => 'cloud_sync',
                'attributes' => [
                    'data-icon' => 'material-icons cloud_sync',
                    'data-icon-value' => 'cloud_sync',
                ],
            ],
            [
                'value' => 'material-icons cloud_upload',
                'label' => 'cloud_upload',
                'attributes' => [
                    'data-icon' => 'material-icons cloud_upload',
                    'data-icon-value' => 'cloud_upload',
                ],
            ],
            [
                'value' => 'material-icons cloudy_snowing',
                'label' => 'cloudy_snowing',
                'attributes' => [
                    'data-icon' => 'material-icons cloudy_snowing',
                    'data-icon-value' => 'cloudy_snowing',
                ],
            ],
            [
                'value' => 'material-icons co2',
                'label' => 'co2',
                'attributes' => [
                    'data-icon' => 'material-icons co2',
                    'data-icon-value' => 'co2',
                ],
            ],
            [
                'value' => 'material-icons co_present',
                'label' => 'co_present',
                'attributes' => [
                    'data-icon' => 'material-icons co_present',
                    'data-icon-value' => 'co_present',
                ],
            ],
            [
                'value' => 'material-icons code',
                'label' => 'code',
                'attributes' => [
                    'data-icon' => 'material-icons code',
                    'data-icon-value' => 'code',
                ],
            ],
            [
                'value' => 'material-icons code_off',
                'label' => 'code_off',
                'attributes' => [
                    'data-icon' => 'material-icons code_off',
                    'data-icon-value' => 'code_off',
                ],
            ],
            [
                'value' => 'material-icons coffee',
                'label' => 'coffee',
                'attributes' => [
                    'data-icon' => 'material-icons coffee',
                    'data-icon-value' => 'coffee',
                ],
            ],
            [
                'value' => 'material-icons coffee_maker',
                'label' => 'coffee_maker',
                'attributes' => [
                    'data-icon' => 'material-icons coffee_maker',
                    'data-icon-value' => 'coffee_maker',
                ],
            ],
            [
                'value' => 'material-icons collections',
                'label' => 'collections',
                'attributes' => [
                    'data-icon' => 'material-icons collections',
                    'data-icon-value' => 'collections',
                ],
            ],
            [
                'value' => 'material-icons collections_bookmark',
                'label' => 'collections_bookmark',
                'attributes' => [
                    'data-icon' => 'material-icons collections_bookmark',
                    'data-icon-value' => 'collections_bookmark',
                ],
            ],
            [
                'value' => 'material-icons color_lens',
                'label' => 'color_lens',
                'attributes' => [
                    'data-icon' => 'material-icons color_lens',
                    'data-icon-value' => 'color_lens',
                ],
            ],
            [
                'value' => 'material-icons colorize',
                'label' => 'colorize',
                'attributes' => [
                    'data-icon' => 'material-icons colorize',
                    'data-icon-value' => 'colorize',
                ],
            ],
            [
                'value' => 'material-icons comment',
                'label' => 'comment',
                'attributes' => [
                    'data-icon' => 'material-icons comment',
                    'data-icon-value' => 'comment',
                ],
            ],
            [
                'value' => 'material-icons comment_bank',
                'label' => 'comment_bank',
                'attributes' => [
                    'data-icon' => 'material-icons comment_bank',
                    'data-icon-value' => 'comment_bank',
                ],
            ],
            [
                'value' => 'material-icons comments_disabled',
                'label' => 'comments_disabled',
                'attributes' => [
                    'data-icon' => 'material-icons comments_disabled',
                    'data-icon-value' => 'comments_disabled',
                ],
            ],
            [
                'value' => 'material-icons commit',
                'label' => 'commit',
                'attributes' => [
                    'data-icon' => 'material-icons commit',
                    'data-icon-value' => 'commit',
                ],
            ],
            [
                'value' => 'material-icons commute',
                'label' => 'commute',
                'attributes' => [
                    'data-icon' => 'material-icons commute',
                    'data-icon-value' => 'commute',
                ],
            ],
            [
                'value' => 'material-icons compare',
                'label' => 'compare',
                'attributes' => [
                    'data-icon' => 'material-icons compare',
                    'data-icon-value' => 'compare',
                ],
            ],
            [
                'value' => 'material-icons compare_arrows',
                'label' => 'compare_arrows',
                'attributes' => [
                    'data-icon' => 'material-icons compare_arrows',
                    'data-icon-value' => 'compare_arrows',
                ],
            ],
            [
                'value' => 'material-icons compass_calibration',
                'label' => 'compass_calibration',
                'attributes' => [
                    'data-icon' => 'material-icons compass_calibration',
                    'data-icon-value' => 'compass_calibration',
                ],
            ],
            [
                'value' => 'material-icons compost',
                'label' => 'compost',
                'attributes' => [
                    'data-icon' => 'material-icons compost',
                    'data-icon-value' => 'compost',
                ],
            ],
            [
                'value' => 'material-icons compress',
                'label' => 'compress',
                'attributes' => [
                    'data-icon' => 'material-icons compress',
                    'data-icon-value' => 'compress',
                ],
            ],
            [
                'value' => 'material-icons computer',
                'label' => 'computer',
                'attributes' => [
                    'data-icon' => 'material-icons computer',
                    'data-icon-value' => 'computer',
                ],
            ],
            [
                'value' => 'material-icons confirmation_num',
                'label' => 'confirmation_num',
                'attributes' => [
                    'data-icon' => 'material-icons confirmation_num',
                    'data-icon-value' => 'confirmation_num',
                ],
            ],
            [
                'value' => 'material-icons confirmation_number',
                'label' => 'confirmation_number',
                'attributes' => [
                    'data-icon' => 'material-icons confirmation_number',
                    'data-icon-value' => 'confirmation_number',
                ],
            ],
            [
                'value' => 'material-icons connect_without_contact',
                'label' => 'connect_without_contact',
                'attributes' => [
                    'data-icon' => 'material-icons connect_without_contact',
                    'data-icon-value' => 'connect_without_contact',
                ],
            ],
            [
                'value' => 'material-icons connected_tv',
                'label' => 'connected_tv',
                'attributes' => [
                    'data-icon' => 'material-icons connected_tv',
                    'data-icon-value' => 'connected_tv',
                ],
            ],
            [
                'value' => 'material-icons connecting_airports',
                'label' => 'connecting_airports',
                'attributes' => [
                    'data-icon' => 'material-icons connecting_airports',
                    'data-icon-value' => 'connecting_airports',
                ],
            ],
            [
                'value' => 'material-icons construction',
                'label' => 'construction',
                'attributes' => [
                    'data-icon' => 'material-icons construction',
                    'data-icon-value' => 'construction',
                ],
            ],
            [
                'value' => 'material-icons contact_mail',
                'label' => 'contact_mail',
                'attributes' => [
                    'data-icon' => 'material-icons contact_mail',
                    'data-icon-value' => 'contact_mail',
                ],
            ],
            [
                'value' => 'material-icons contact_page',
                'label' => 'contact_page',
                'attributes' => [
                    'data-icon' => 'material-icons contact_page',
                    'data-icon-value' => 'contact_page',
                ],
            ],
            [
                'value' => 'material-icons contact_phone',
                'label' => 'contact_phone',
                'attributes' => [
                    'data-icon' => 'material-icons contact_phone',
                    'data-icon-value' => 'contact_phone',
                ],
            ],
            [
                'value' => 'material-icons contact_support',
                'label' => 'contact_support',
                'attributes' => [
                    'data-icon' => 'material-icons contact_support',
                    'data-icon-value' => 'contact_support',
                ],
            ],
            [
                'value' => 'material-icons contactless',
                'label' => 'contactless',
                'attributes' => [
                    'data-icon' => 'material-icons contactless',
                    'data-icon-value' => 'contactless',
                ],
            ],
            [
                'value' => 'material-icons contacts',
                'label' => 'contacts',
                'attributes' => [
                    'data-icon' => 'material-icons contacts',
                    'data-icon-value' => 'contacts',
                ],
            ],
            [
                'value' => 'material-icons content_copy',
                'label' => 'content_copy',
                'attributes' => [
                    'data-icon' => 'material-icons content_copy',
                    'data-icon-value' => 'content_copy',
                ],
            ],
            [
                'value' => 'material-icons content_cut',
                'label' => 'content_cut',
                'attributes' => [
                    'data-icon' => 'material-icons content_cut',
                    'data-icon-value' => 'content_cut',
                ],
            ],
            [
                'value' => 'material-icons content_paste',
                'label' => 'content_paste',
                'attributes' => [
                    'data-icon' => 'material-icons content_paste',
                    'data-icon-value' => 'content_paste',
                ],
            ],
            [
                'value' => 'material-icons content_paste_go',
                'label' => 'content_paste_go',
                'attributes' => [
                    'data-icon' => 'material-icons content_paste_go',
                    'data-icon-value' => 'content_paste_go',
                ],
            ],
            [
                'value' => 'material-icons content_paste_off',
                'label' => 'content_paste_off',
                'attributes' => [
                    'data-icon' => 'material-icons content_paste_off',
                    'data-icon-value' => 'content_paste_off',
                ],
            ],
            [
                'value' => 'material-icons content_paste_search',
                'label' => 'content_paste_search',
                'attributes' => [
                    'data-icon' => 'material-icons content_paste_search',
                    'data-icon-value' => 'content_paste_search',
                ],
            ],
            [
                'value' => 'material-icons contrast',
                'label' => 'contrast',
                'attributes' => [
                    'data-icon' => 'material-icons contrast',
                    'data-icon-value' => 'contrast',
                ],
            ],
            [
                'value' => 'material-icons control_camera',
                'label' => 'control_camera',
                'attributes' => [
                    'data-icon' => 'material-icons control_camera',
                    'data-icon-value' => 'control_camera',
                ],
            ],
            [
                'value' => 'material-icons control_point',
                'label' => 'control_point',
                'attributes' => [
                    'data-icon' => 'material-icons control_point',
                    'data-icon-value' => 'control_point',
                ],
            ],
            [
                'value' => 'material-icons control_point_duplicate',
                'label' => 'control_point_duplicate',
                'attributes' => [
                    'data-icon' => 'material-icons control_point_duplicate',
                    'data-icon-value' => 'control_point_duplicate',
                ],
            ],
            [
                'value' => 'material-icons cookie',
                'label' => 'cookie',
                'attributes' => [
                    'data-icon' => 'material-icons cookie',
                    'data-icon-value' => 'cookie',
                ],
            ],
            [
                'value' => 'material-icons copy_all',
                'label' => 'copy_all',
                'attributes' => [
                    'data-icon' => 'material-icons copy_all',
                    'data-icon-value' => 'copy_all',
                ],
            ],
            [
                'value' => 'material-icons copyright',
                'label' => 'copyright',
                'attributes' => [
                    'data-icon' => 'material-icons copyright',
                    'data-icon-value' => 'copyright',
                ],
            ],
            [
                'value' => 'material-icons coronavirus',
                'label' => 'coronavirus',
                'attributes' => [
                    'data-icon' => 'material-icons coronavirus',
                    'data-icon-value' => 'coronavirus',
                ],
            ],
            [
                'value' => 'material-icons corporate_fare',
                'label' => 'corporate_fare',
                'attributes' => [
                    'data-icon' => 'material-icons corporate_fare',
                    'data-icon-value' => 'corporate_fare',
                ],
            ],
            [
                'value' => 'material-icons cottage',
                'label' => 'cottage',
                'attributes' => [
                    'data-icon' => 'material-icons cottage',
                    'data-icon-value' => 'cottage',
                ],
            ],
            [
                'value' => 'material-icons countertops',
                'label' => 'countertops',
                'attributes' => [
                    'data-icon' => 'material-icons countertops',
                    'data-icon-value' => 'countertops',
                ],
            ],
            [
                'value' => 'material-icons create',
                'label' => 'create',
                'attributes' => [
                    'data-icon' => 'material-icons create',
                    'data-icon-value' => 'create',
                ],
            ],
            [
                'value' => 'material-icons create_new_folder',
                'label' => 'create_new_folder',
                'attributes' => [
                    'data-icon' => 'material-icons create_new_folder',
                    'data-icon-value' => 'create_new_folder',
                ],
            ],
            [
                'value' => 'material-icons credit_card',
                'label' => 'credit_card',
                'attributes' => [
                    'data-icon' => 'material-icons credit_card',
                    'data-icon-value' => 'credit_card',
                ],
            ],
            [
                'value' => 'material-icons credit_card_off',
                'label' => 'credit_card_off',
                'attributes' => [
                    'data-icon' => 'material-icons credit_card_off',
                    'data-icon-value' => 'credit_card_off',
                ],
            ],
            [
                'value' => 'material-icons credit_score',
                'label' => 'credit_score',
                'attributes' => [
                    'data-icon' => 'material-icons credit_score',
                    'data-icon-value' => 'credit_score',
                ],
            ],
            [
                'value' => 'material-icons crib',
                'label' => 'crib',
                'attributes' => [
                    'data-icon' => 'material-icons crib',
                    'data-icon-value' => 'crib',
                ],
            ],
            [
                'value' => 'material-icons crisis_alert',
                'label' => 'crisis_alert',
                'attributes' => [
                    'data-icon' => 'material-icons crisis_alert',
                    'data-icon-value' => 'crisis_alert',
                ],
            ],
            [
                'value' => 'material-icons crop',
                'label' => 'crop',
                'attributes' => [
                    'data-icon' => 'material-icons crop',
                    'data-icon-value' => 'crop',
                ],
            ],
            [
                'value' => 'material-icons crop_16_9',
                'label' => 'crop_16_9',
                'attributes' => [
                    'data-icon' => 'material-icons crop_16_9',
                    'data-icon-value' => 'crop_16_9',
                ],
            ],
            [
                'value' => 'material-icons crop_3_2',
                'label' => 'crop_3_2',
                'attributes' => [
                    'data-icon' => 'material-icons crop_3_2',
                    'data-icon-value' => 'crop_3_2',
                ],
            ],
            [
                'value' => 'material-icons crop_5_4',
                'label' => 'crop_5_4',
                'attributes' => [
                    'data-icon' => 'material-icons crop_5_4',
                    'data-icon-value' => 'crop_5_4',
                ],
            ],
            [
                'value' => 'material-icons crop_7_5',
                'label' => 'crop_7_5',
                'attributes' => [
                    'data-icon' => 'material-icons crop_7_5',
                    'data-icon-value' => 'crop_7_5',
                ],
            ],
            [
                'value' => 'material-icons crop_din',
                'label' => 'crop_din',
                'attributes' => [
                    'data-icon' => 'material-icons crop_din',
                    'data-icon-value' => 'crop_din',
                ],
            ],
            [
                'value' => 'material-icons crop_free',
                'label' => 'crop_free',
                'attributes' => [
                    'data-icon' => 'material-icons crop_free',
                    'data-icon-value' => 'crop_free',
                ],
            ],
            [
                'value' => 'material-icons crop_landscape',
                'label' => 'crop_landscape',
                'attributes' => [
                    'data-icon' => 'material-icons crop_landscape',
                    'data-icon-value' => 'crop_landscape',
                ],
            ],
            [
                'value' => 'material-icons crop_original',
                'label' => 'crop_original',
                'attributes' => [
                    'data-icon' => 'material-icons crop_original',
                    'data-icon-value' => 'crop_original',
                ],
            ],
            [
                'value' => 'material-icons crop_portrait',
                'label' => 'crop_portrait',
                'attributes' => [
                    'data-icon' => 'material-icons crop_portrait',
                    'data-icon-value' => 'crop_portrait',
                ],
            ],
            [
                'value' => 'material-icons crop_rotate',
                'label' => 'crop_rotate',
                'attributes' => [
                    'data-icon' => 'material-icons crop_rotate',
                    'data-icon-value' => 'crop_rotate',
                ],
            ],
            [
                'value' => 'material-icons crop_square',
                'label' => 'crop_square',
                'attributes' => [
                    'data-icon' => 'material-icons crop_square',
                    'data-icon-value' => 'crop_square',
                ],
            ],
            [
                'value' => 'material-icons cruelty_free',
                'label' => 'cruelty_free',
                'attributes' => [
                    'data-icon' => 'material-icons cruelty_free',
                    'data-icon-value' => 'cruelty_free',
                ],
            ],
            [
                'value' => 'material-icons css',
                'label' => 'css',
                'attributes' => [
                    'data-icon' => 'material-icons css',
                    'data-icon-value' => 'css',
                ],
            ],
            [
                'value' => 'material-icons currency_bitcoin',
                'label' => 'currency_bitcoin',
                'attributes' => [
                    'data-icon' => 'material-icons currency_bitcoin',
                    'data-icon-value' => 'currency_bitcoin',
                ],
            ],
            [
                'value' => 'material-icons currency_exchange',
                'label' => 'currency_exchange',
                'attributes' => [
                    'data-icon' => 'material-icons currency_exchange',
                    'data-icon-value' => 'currency_exchange',
                ],
            ],
            [
                'value' => 'material-icons currency_franc',
                'label' => 'currency_franc',
                'attributes' => [
                    'data-icon' => 'material-icons currency_franc',
                    'data-icon-value' => 'currency_franc',
                ],
            ],
            [
                'value' => 'material-icons currency_lira',
                'label' => 'currency_lira',
                'attributes' => [
                    'data-icon' => 'material-icons currency_lira',
                    'data-icon-value' => 'currency_lira',
                ],
            ],
            [
                'value' => 'material-icons currency_pound',
                'label' => 'currency_pound',
                'attributes' => [
                    'data-icon' => 'material-icons currency_pound',
                    'data-icon-value' => 'currency_pound',
                ],
            ],
            [
                'value' => 'material-icons currency_ruble',
                'label' => 'currency_ruble',
                'attributes' => [
                    'data-icon' => 'material-icons currency_ruble',
                    'data-icon-value' => 'currency_ruble',
                ],
            ],
            [
                'value' => 'material-icons currency_rupee',
                'label' => 'currency_rupee',
                'attributes' => [
                    'data-icon' => 'material-icons currency_rupee',
                    'data-icon-value' => 'currency_rupee',
                ],
            ],
            [
                'value' => 'material-icons currency_yen',
                'label' => 'currency_yen',
                'attributes' => [
                    'data-icon' => 'material-icons currency_yen',
                    'data-icon-value' => 'currency_yen',
                ],
            ],
            [
                'value' => 'material-icons currency_yuan',
                'label' => 'currency_yuan',
                'attributes' => [
                    'data-icon' => 'material-icons currency_yuan',
                    'data-icon-value' => 'currency_yuan',
                ],
            ],
            [
                'value' => 'material-icons curtains',
                'label' => 'curtains',
                'attributes' => [
                    'data-icon' => 'material-icons curtains',
                    'data-icon-value' => 'curtains',
                ],
            ],
            [
                'value' => 'material-icons curtains_closed',
                'label' => 'curtains_closed',
                'attributes' => [
                    'data-icon' => 'material-icons curtains_closed',
                    'data-icon-value' => 'curtains_closed',
                ],
            ],
            [
                'value' => 'material-icons cyclone',
                'label' => 'cyclone',
                'attributes' => [
                    'data-icon' => 'material-icons cyclone',
                    'data-icon-value' => 'cyclone',
                ],
            ],
            [
                'value' => 'material-icons dangerous',
                'label' => 'dangerous',
                'attributes' => [
                    'data-icon' => 'material-icons dangerous',
                    'data-icon-value' => 'dangerous',
                ],
            ],
            [
                'value' => 'material-icons dark_mode',
                'label' => 'dark_mode',
                'attributes' => [
                    'data-icon' => 'material-icons dark_mode',
                    'data-icon-value' => 'dark_mode',
                ],
            ],
            [
                'value' => 'material-icons dashboard',
                'label' => 'dashboard',
                'attributes' => [
                    'data-icon' => 'material-icons dashboard',
                    'data-icon-value' => 'dashboard',
                ],
            ],
            [
                'value' => 'material-icons dashboard_customize',
                'label' => 'dashboard_customize',
                'attributes' => [
                    'data-icon' => 'material-icons dashboard_customize',
                    'data-icon-value' => 'dashboard_customize',
                ],
            ],
            [
                'value' => 'material-icons data_array',
                'label' => 'data_array',
                'attributes' => [
                    'data-icon' => 'material-icons data_array',
                    'data-icon-value' => 'data_array',
                ],
            ],
            [
                'value' => 'material-icons data_exploration',
                'label' => 'data_exploration',
                'attributes' => [
                    'data-icon' => 'material-icons data_exploration',
                    'data-icon-value' => 'data_exploration',
                ],
            ],
            [
                'value' => 'material-icons data_object',
                'label' => 'data_object',
                'attributes' => [
                    'data-icon' => 'material-icons data_object',
                    'data-icon-value' => 'data_object',
                ],
            ],
            [
                'value' => 'material-icons data_saver_off',
                'label' => 'data_saver_off',
                'attributes' => [
                    'data-icon' => 'material-icons data_saver_off',
                    'data-icon-value' => 'data_saver_off',
                ],
            ],
            [
                'value' => 'material-icons data_saver_on',
                'label' => 'data_saver_on',
                'attributes' => [
                    'data-icon' => 'material-icons data_saver_on',
                    'data-icon-value' => 'data_saver_on',
                ],
            ],
            [
                'value' => 'material-icons data_thresholding',
                'label' => 'data_thresholding',
                'attributes' => [
                    'data-icon' => 'material-icons data_thresholding',
                    'data-icon-value' => 'data_thresholding',
                ],
            ],
            [
                'value' => 'material-icons data_usage',
                'label' => 'data_usage',
                'attributes' => [
                    'data-icon' => 'material-icons data_usage',
                    'data-icon-value' => 'data_usage',
                ],
            ],
            [
                'value' => 'material-icons date_range',
                'label' => 'date_range',
                'attributes' => [
                    'data-icon' => 'material-icons date_range',
                    'data-icon-value' => 'date_range',
                ],
            ],
            [
                'value' => 'material-icons deblur',
                'label' => 'deblur',
                'attributes' => [
                    'data-icon' => 'material-icons deblur',
                    'data-icon-value' => 'deblur',
                ],
            ],
            [
                'value' => 'material-icons deck',
                'label' => 'deck',
                'attributes' => [
                    'data-icon' => 'material-icons deck',
                    'data-icon-value' => 'deck',
                ],
            ],
            [
                'value' => 'material-icons dehaze',
                'label' => 'dehaze',
                'attributes' => [
                    'data-icon' => 'material-icons dehaze',
                    'data-icon-value' => 'dehaze',
                ],
            ],
            [
                'value' => 'material-icons delete',
                'label' => 'delete',
                'attributes' => [
                    'data-icon' => 'material-icons delete',
                    'data-icon-value' => 'delete',
                ],
            ],
            [
                'value' => 'material-icons delete_forever',
                'label' => 'delete_forever',
                'attributes' => [
                    'data-icon' => 'material-icons delete_forever',
                    'data-icon-value' => 'delete_forever',
                ],
            ],
            [
                'value' => 'material-icons delete_outline',
                'label' => 'delete_outline',
                'attributes' => [
                    'data-icon' => 'material-icons delete_outline',
                    'data-icon-value' => 'delete_outline',
                ],
            ],
            [
                'value' => 'material-icons delete_sweep',
                'label' => 'delete_sweep',
                'attributes' => [
                    'data-icon' => 'material-icons delete_sweep',
                    'data-icon-value' => 'delete_sweep',
                ],
            ],
            [
                'value' => 'material-icons delivery_dining',
                'label' => 'delivery_dining',
                'attributes' => [
                    'data-icon' => 'material-icons delivery_dining',
                    'data-icon-value' => 'delivery_dining',
                ],
            ],
            [
                'value' => 'material-icons density_large',
                'label' => 'density_large',
                'attributes' => [
                    'data-icon' => 'material-icons density_large',
                    'data-icon-value' => 'density_large',
                ],
            ],
            [
                'value' => 'material-icons density_medium',
                'label' => 'density_medium',
                'attributes' => [
                    'data-icon' => 'material-icons density_medium',
                    'data-icon-value' => 'density_medium',
                ],
            ],
            [
                'value' => 'material-icons density_small',
                'label' => 'density_small',
                'attributes' => [
                    'data-icon' => 'material-icons density_small',
                    'data-icon-value' => 'density_small',
                ],
            ],
            [
                'value' => 'material-icons departure_board',
                'label' => 'departure_board',
                'attributes' => [
                    'data-icon' => 'material-icons departure_board',
                    'data-icon-value' => 'departure_board',
                ],
            ],
            [
                'value' => 'material-icons description',
                'label' => 'description',
                'attributes' => [
                    'data-icon' => 'material-icons description',
                    'data-icon-value' => 'description',
                ],
            ],
            [
                'value' => 'material-icons deselect',
                'label' => 'deselect',
                'attributes' => [
                    'data-icon' => 'material-icons deselect',
                    'data-icon-value' => 'deselect',
                ],
            ],
            [
                'value' => 'material-icons design_services',
                'label' => 'design_services',
                'attributes' => [
                    'data-icon' => 'material-icons design_services',
                    'data-icon-value' => 'design_services',
                ],
            ],
            [
                'value' => 'material-icons desk',
                'label' => 'desk',
                'attributes' => [
                    'data-icon' => 'material-icons desk',
                    'data-icon-value' => 'desk',
                ],
            ],
            [
                'value' => 'material-icons desktop_access_disabled',
                'label' => 'desktop_access_disabled',
                'attributes' => [
                    'data-icon' => 'material-icons desktop_access_disabled',
                    'data-icon-value' => 'desktop_access_disabled',
                ],
            ],
            [
                'value' => 'material-icons desktop_mac',
                'label' => 'desktop_mac',
                'attributes' => [
                    'data-icon' => 'material-icons desktop_mac',
                    'data-icon-value' => 'desktop_mac',
                ],
            ],
            [
                'value' => 'material-icons desktop_windows',
                'label' => 'desktop_windows',
                'attributes' => [
                    'data-icon' => 'material-icons desktop_windows',
                    'data-icon-value' => 'desktop_windows',
                ],
            ],
            [
                'value' => 'material-icons details',
                'label' => 'details',
                'attributes' => [
                    'data-icon' => 'material-icons details',
                    'data-icon-value' => 'details',
                ],
            ],
            [
                'value' => 'material-icons developer_board',
                'label' => 'developer_board',
                'attributes' => [
                    'data-icon' => 'material-icons developer_board',
                    'data-icon-value' => 'developer_board',
                ],
            ],
            [
                'value' => 'material-icons developer_board_off',
                'label' => 'developer_board_off',
                'attributes' => [
                    'data-icon' => 'material-icons developer_board_off',
                    'data-icon-value' => 'developer_board_off',
                ],
            ],
            [
                'value' => 'material-icons developer_mode',
                'label' => 'developer_mode',
                'attributes' => [
                    'data-icon' => 'material-icons developer_mode',
                    'data-icon-value' => 'developer_mode',
                ],
            ],
            [
                'value' => 'material-icons device_hub',
                'label' => 'device_hub',
                'attributes' => [
                    'data-icon' => 'material-icons device_hub',
                    'data-icon-value' => 'device_hub',
                ],
            ],
            [
                'value' => 'material-icons device_thermostat',
                'label' => 'device_thermostat',
                'attributes' => [
                    'data-icon' => 'material-icons device_thermostat',
                    'data-icon-value' => 'device_thermostat',
                ],
            ],
            [
                'value' => 'material-icons device_unknown',
                'label' => 'device_unknown',
                'attributes' => [
                    'data-icon' => 'material-icons device_unknown',
                    'data-icon-value' => 'device_unknown',
                ],
            ],
            [
                'value' => 'material-icons devices',
                'label' => 'devices',
                'attributes' => [
                    'data-icon' => 'material-icons devices',
                    'data-icon-value' => 'devices',
                ],
            ],
            [
                'value' => 'material-icons devices_fold',
                'label' => 'devices_fold',
                'attributes' => [
                    'data-icon' => 'material-icons devices_fold',
                    'data-icon-value' => 'devices_fold',
                ],
            ],
            [
                'value' => 'material-icons devices_other',
                'label' => 'devices_other',
                'attributes' => [
                    'data-icon' => 'material-icons devices_other',
                    'data-icon-value' => 'devices_other',
                ],
            ],
            [
                'value' => 'material-icons dialer_sip',
                'label' => 'dialer_sip',
                'attributes' => [
                    'data-icon' => 'material-icons dialer_sip',
                    'data-icon-value' => 'dialer_sip',
                ],
            ],
            [
                'value' => 'material-icons dialpad',
                'label' => 'dialpad',
                'attributes' => [
                    'data-icon' => 'material-icons dialpad',
                    'data-icon-value' => 'dialpad',
                ],
            ],
            [
                'value' => 'material-icons diamond',
                'label' => 'diamond',
                'attributes' => [
                    'data-icon' => 'material-icons diamond',
                    'data-icon-value' => 'diamond',
                ],
            ],
            [
                'value' => 'material-icons difference',
                'label' => 'difference',
                'attributes' => [
                    'data-icon' => 'material-icons difference',
                    'data-icon-value' => 'difference',
                ],
            ],
            [
                'value' => 'material-icons dining',
                'label' => 'dining',
                'attributes' => [
                    'data-icon' => 'material-icons dining',
                    'data-icon-value' => 'dining',
                ],
            ],
            [
                'value' => 'material-icons dinner_dining',
                'label' => 'dinner_dining',
                'attributes' => [
                    'data-icon' => 'material-icons dinner_dining',
                    'data-icon-value' => 'dinner_dining',
                ],
            ],
            [
                'value' => 'material-icons directions',
                'label' => 'directions',
                'attributes' => [
                    'data-icon' => 'material-icons directions',
                    'data-icon-value' => 'directions',
                ],
            ],
            [
                'value' => 'material-icons directions_bike',
                'label' => 'directions_bike',
                'attributes' => [
                    'data-icon' => 'material-icons directions_bike',
                    'data-icon-value' => 'directions_bike',
                ],
            ],
            [
                'value' => 'material-icons directions_boat',
                'label' => 'directions_boat',
                'attributes' => [
                    'data-icon' => 'material-icons directions_boat',
                    'data-icon-value' => 'directions_boat',
                ],
            ],
            [
                'value' => 'material-icons directions_boat_filled',
                'label' => 'directions_boat_filled',
                'attributes' => [
                    'data-icon' => 'material-icons directions_boat_filled',
                    'data-icon-value' => 'directions_boat_filled',
                ],
            ],
            [
                'value' => 'material-icons directions_bus',
                'label' => 'directions_bus',
                'attributes' => [
                    'data-icon' => 'material-icons directions_bus',
                    'data-icon-value' => 'directions_bus',
                ],
            ],
            [
                'value' => 'material-icons directions_bus_filled',
                'label' => 'directions_bus_filled',
                'attributes' => [
                    'data-icon' => 'material-icons directions_bus_filled',
                    'data-icon-value' => 'directions_bus_filled',
                ],
            ],
            [
                'value' => 'material-icons directions_car',
                'label' => 'directions_car',
                'attributes' => [
                    'data-icon' => 'material-icons directions_car',
                    'data-icon-value' => 'directions_car',
                ],
            ],
            [
                'value' => 'material-icons directions_car_filled',
                'label' => 'directions_car_filled',
                'attributes' => [
                    'data-icon' => 'material-icons directions_car_filled',
                    'data-icon-value' => 'directions_car_filled',
                ],
            ],
            [
                'value' => 'material-icons directions_ferry',
                'label' => 'directions_ferry',
                'attributes' => [
                    'data-icon' => 'material-icons directions_ferry',
                    'data-icon-value' => 'directions_ferry',
                ],
            ],
            [
                'value' => 'material-icons directions_off',
                'label' => 'directions_off',
                'attributes' => [
                    'data-icon' => 'material-icons directions_off',
                    'data-icon-value' => 'directions_off',
                ],
            ],
            [
                'value' => 'material-icons directions_railway',
                'label' => 'directions_railway',
                'attributes' => [
                    'data-icon' => 'material-icons directions_railway',
                    'data-icon-value' => 'directions_railway',
                ],
            ],
            [
                'value' => 'material-icons directions_railway_filled',
                'label' => 'directions_railway_filled',
                'attributes' => [
                    'data-icon' => 'material-icons directions_railway_filled',
                    'data-icon-value' => 'directions_railway_filled',
                ],
            ],
            [
                'value' => 'material-icons directions_run',
                'label' => 'directions_run',
                'attributes' => [
                    'data-icon' => 'material-icons directions_run',
                    'data-icon-value' => 'directions_run',
                ],
            ],
            [
                'value' => 'material-icons directions_subway',
                'label' => 'directions_subway',
                'attributes' => [
                    'data-icon' => 'material-icons directions_subway',
                    'data-icon-value' => 'directions_subway',
                ],
            ],
            [
                'value' => 'material-icons directions_subway_filled',
                'label' => 'directions_subway_filled',
                'attributes' => [
                    'data-icon' => 'material-icons directions_subway_filled',
                    'data-icon-value' => 'directions_subway_filled',
                ],
            ],
            [
                'value' => 'material-icons directions_train',
                'label' => 'directions_train',
                'attributes' => [
                    'data-icon' => 'material-icons directions_train',
                    'data-icon-value' => 'directions_train',
                ],
            ],
            [
                'value' => 'material-icons directions_transit',
                'label' => 'directions_transit',
                'attributes' => [
                    'data-icon' => 'material-icons directions_transit',
                    'data-icon-value' => 'directions_transit',
                ],
            ],
            [
                'value' => 'material-icons directions_transit_filled',
                'label' => 'directions_transit_filled',
                'attributes' => [
                    'data-icon' => 'material-icons directions_transit_filled',
                    'data-icon-value' => 'directions_transit_filled',
                ],
            ],
            [
                'value' => 'material-icons directions_walk',
                'label' => 'directions_walk',
                'attributes' => [
                    'data-icon' => 'material-icons directions_walk',
                    'data-icon-value' => 'directions_walk',
                ],
            ],
            [
                'value' => 'material-icons dirty_lens',
                'label' => 'dirty_lens',
                'attributes' => [
                    'data-icon' => 'material-icons dirty_lens',
                    'data-icon-value' => 'dirty_lens',
                ],
            ],
            [
                'value' => 'material-icons disabled_by_default',
                'label' => 'disabled_by_default',
                'attributes' => [
                    'data-icon' => 'material-icons disabled_by_default',
                    'data-icon-value' => 'disabled_by_default',
                ],
            ],
            [
                'value' => 'material-icons disabled_visible',
                'label' => 'disabled_visible',
                'attributes' => [
                    'data-icon' => 'material-icons disabled_visible',
                    'data-icon-value' => 'disabled_visible',
                ],
            ],
            [
                'value' => 'material-icons disc_full',
                'label' => 'disc_full',
                'attributes' => [
                    'data-icon' => 'material-icons disc_full',
                    'data-icon-value' => 'disc_full',
                ],
            ],
            [
                'value' => 'material-icons discord',
                'label' => 'discord',
                'attributes' => [
                    'data-icon' => 'material-icons discord',
                    'data-icon-value' => 'discord',
                ],
            ],
            [
                'value' => 'material-icons discount',
                'label' => 'discount',
                'attributes' => [
                    'data-icon' => 'material-icons discount',
                    'data-icon-value' => 'discount',
                ],
            ],
            [
                'value' => 'material-icons display_settings',
                'label' => 'display_settings',
                'attributes' => [
                    'data-icon' => 'material-icons display_settings',
                    'data-icon-value' => 'display_settings',
                ],
            ],
            [
                'value' => 'material-icons dnd_forwardslash',
                'label' => 'dnd_forwardslash',
                'attributes' => [
                    'data-icon' => 'material-icons dnd_forwardslash',
                    'data-icon-value' => 'dnd_forwardslash',
                ],
            ],
            [
                'value' => 'material-icons dns',
                'label' => 'dns',
                'attributes' => [
                    'data-icon' => 'material-icons dns',
                    'data-icon-value' => 'dns',
                ],
            ],
            [
                'value' => 'material-icons do_disturb',
                'label' => 'do_disturb',
                'attributes' => [
                    'data-icon' => 'material-icons do_disturb',
                    'data-icon-value' => 'do_disturb',
                ],
            ],
            [
                'value' => 'material-icons do_disturb_alt',
                'label' => 'do_disturb_alt',
                'attributes' => [
                    'data-icon' => 'material-icons do_disturb_alt',
                    'data-icon-value' => 'do_disturb_alt',
                ],
            ],
            [
                'value' => 'material-icons do_disturb_off',
                'label' => 'do_disturb_off',
                'attributes' => [
                    'data-icon' => 'material-icons do_disturb_off',
                    'data-icon-value' => 'do_disturb_off',
                ],
            ],
            [
                'value' => 'material-icons do_disturb_on',
                'label' => 'do_disturb_on',
                'attributes' => [
                    'data-icon' => 'material-icons do_disturb_on',
                    'data-icon-value' => 'do_disturb_on',
                ],
            ],
            [
                'value' => 'material-icons do_not_disturb',
                'label' => 'do_not_disturb',
                'attributes' => [
                    'data-icon' => 'material-icons do_not_disturb',
                    'data-icon-value' => 'do_not_disturb',
                ],
            ],
            [
                'value' => 'material-icons do_not_disturb_alt',
                'label' => 'do_not_disturb_alt',
                'attributes' => [
                    'data-icon' => 'material-icons do_not_disturb_alt',
                    'data-icon-value' => 'do_not_disturb_alt',
                ],
            ],
            [
                'value' => 'material-icons do_not_disturb_off',
                'label' => 'do_not_disturb_off',
                'attributes' => [
                    'data-icon' => 'material-icons do_not_disturb_off',
                    'data-icon-value' => 'do_not_disturb_off',
                ],
            ],
            [
                'value' => 'material-icons do_not_disturb_on',
                'label' => 'do_not_disturb_on',
                'attributes' => [
                    'data-icon' => 'material-icons do_not_disturb_on',
                    'data-icon-value' => 'do_not_disturb_on',
                ],
            ],
            [
                'value' => 'material-icons do_not_disturb_on_total_silence',
                'label' => 'do_not_disturb_on_total_silence',
                'attributes' => [
                    'data-icon' => 'material-icons do_not_disturb_on_total_silence',
                    'data-icon-value' => 'do_not_disturb_on_total_silence',
                ],
            ],
            [
                'value' => 'material-icons do_not_step',
                'label' => 'do_not_step',
                'attributes' => [
                    'data-icon' => 'material-icons do_not_step',
                    'data-icon-value' => 'do_not_step',
                ],
            ],
            [
                'value' => 'material-icons do_not_touch',
                'label' => 'do_not_touch',
                'attributes' => [
                    'data-icon' => 'material-icons do_not_touch',
                    'data-icon-value' => 'do_not_touch',
                ],
            ],
            [
                'value' => 'material-icons dock',
                'label' => 'dock',
                'attributes' => [
                    'data-icon' => 'material-icons dock',
                    'data-icon-value' => 'dock',
                ],
            ],
            [
                'value' => 'material-icons document_scanner',
                'label' => 'document_scanner',
                'attributes' => [
                    'data-icon' => 'material-icons document_scanner',
                    'data-icon-value' => 'document_scanner',
                ],
            ],
            [
                'value' => 'material-icons domain',
                'label' => 'domain',
                'attributes' => [
                    'data-icon' => 'material-icons domain',
                    'data-icon-value' => 'domain',
                ],
            ],
            [
                'value' => 'material-icons domain_add',
                'label' => 'domain_add',
                'attributes' => [
                    'data-icon' => 'material-icons domain_add',
                    'data-icon-value' => 'domain_add',
                ],
            ],
            [
                'value' => 'material-icons domain_disabled',
                'label' => 'domain_disabled',
                'attributes' => [
                    'data-icon' => 'material-icons domain_disabled',
                    'data-icon-value' => 'domain_disabled',
                ],
            ],
            [
                'value' => 'material-icons domain_verification',
                'label' => 'domain_verification',
                'attributes' => [
                    'data-icon' => 'material-icons domain_verification',
                    'data-icon-value' => 'domain_verification',
                ],
            ],
            [
                'value' => 'material-icons done',
                'label' => 'done',
                'attributes' => [
                    'data-icon' => 'material-icons done',
                    'data-icon-value' => 'done',
                ],
            ],
            [
                'value' => 'material-icons done_all',
                'label' => 'done_all',
                'attributes' => [
                    'data-icon' => 'material-icons done_all',
                    'data-icon-value' => 'done_all',
                ],
            ],
            [
                'value' => 'material-icons done_outline',
                'label' => 'done_outline',
                'attributes' => [
                    'data-icon' => 'material-icons done_outline',
                    'data-icon-value' => 'done_outline',
                ],
            ],
            [
                'value' => 'material-icons donut_large',
                'label' => 'donut_large',
                'attributes' => [
                    'data-icon' => 'material-icons donut_large',
                    'data-icon-value' => 'donut_large',
                ],
            ],
            [
                'value' => 'material-icons donut_small',
                'label' => 'donut_small',
                'attributes' => [
                    'data-icon' => 'material-icons donut_small',
                    'data-icon-value' => 'donut_small',
                ],
            ],
            [
                'value' => 'material-icons door_back',
                'label' => 'door_back',
                'attributes' => [
                    'data-icon' => 'material-icons door_back',
                    'data-icon-value' => 'door_back',
                ],
            ],
            [
                'value' => 'material-icons door_front',
                'label' => 'door_front',
                'attributes' => [
                    'data-icon' => 'material-icons door_front',
                    'data-icon-value' => 'door_front',
                ],
            ],
            [
                'value' => 'material-icons door_sliding',
                'label' => 'door_sliding',
                'attributes' => [
                    'data-icon' => 'material-icons door_sliding',
                    'data-icon-value' => 'door_sliding',
                ],
            ],
            [
                'value' => 'material-icons doorbell',
                'label' => 'doorbell',
                'attributes' => [
                    'data-icon' => 'material-icons doorbell',
                    'data-icon-value' => 'doorbell',
                ],
            ],
            [
                'value' => 'material-icons double_arrow',
                'label' => 'double_arrow',
                'attributes' => [
                    'data-icon' => 'material-icons double_arrow',
                    'data-icon-value' => 'double_arrow',
                ],
            ],
            [
                'value' => 'material-icons downhill_skiing',
                'label' => 'downhill_skiing',
                'attributes' => [
                    'data-icon' => 'material-icons downhill_skiing',
                    'data-icon-value' => 'downhill_skiing',
                ],
            ],
            [
                'value' => 'material-icons download',
                'label' => 'download',
                'attributes' => [
                    'data-icon' => 'material-icons download',
                    'data-icon-value' => 'download',
                ],
            ],
            [
                'value' => 'material-icons download_done',
                'label' => 'download_done',
                'attributes' => [
                    'data-icon' => 'material-icons download_done',
                    'data-icon-value' => 'download_done',
                ],
            ],
            [
                'value' => 'material-icons download_for_offline',
                'label' => 'download_for_offline',
                'attributes' => [
                    'data-icon' => 'material-icons download_for_offline',
                    'data-icon-value' => 'download_for_offline',
                ],
            ],
            [
                'value' => 'material-icons downloading',
                'label' => 'downloading',
                'attributes' => [
                    'data-icon' => 'material-icons downloading',
                    'data-icon-value' => 'downloading',
                ],
            ],
            [
                'value' => 'material-icons drafts',
                'label' => 'drafts',
                'attributes' => [
                    'data-icon' => 'material-icons drafts',
                    'data-icon-value' => 'drafts',
                ],
            ],
            [
                'value' => 'material-icons drag_handle',
                'label' => 'drag_handle',
                'attributes' => [
                    'data-icon' => 'material-icons drag_handle',
                    'data-icon-value' => 'drag_handle',
                ],
            ],
            [
                'value' => 'material-icons drag_indicator',
                'label' => 'drag_indicator',
                'attributes' => [
                    'data-icon' => 'material-icons drag_indicator',
                    'data-icon-value' => 'drag_indicator',
                ],
            ],
            [
                'value' => 'material-icons draw',
                'label' => 'draw',
                'attributes' => [
                    'data-icon' => 'material-icons draw',
                    'data-icon-value' => 'draw',
                ],
            ],
            [
                'value' => 'material-icons drive_eta',
                'label' => 'drive_eta',
                'attributes' => [
                    'data-icon' => 'material-icons drive_eta',
                    'data-icon-value' => 'drive_eta',
                ],
            ],
            [
                'value' => 'material-icons drive_file_move',
                'label' => 'drive_file_move',
                'attributes' => [
                    'data-icon' => 'material-icons drive_file_move',
                    'data-icon-value' => 'drive_file_move',
                ],
            ],
            [
                'value' => 'material-icons drive_file_move_outline',
                'label' => 'drive_file_move_outline',
                'attributes' => [
                    'data-icon' => 'material-icons drive_file_move_outline',
                    'data-icon-value' => 'drive_file_move_outline',
                ],
            ],
            [
                'value' => 'material-icons drive_file_move_rtl',
                'label' => 'drive_file_move_rtl',
                'attributes' => [
                    'data-icon' => 'material-icons drive_file_move_rtl',
                    'data-icon-value' => 'drive_file_move_rtl',
                ],
            ],
            [
                'value' => 'material-icons drive_file_rename_outline',
                'label' => 'drive_file_rename_outline',
                'attributes' => [
                    'data-icon' => 'material-icons drive_file_rename_outline',
                    'data-icon-value' => 'drive_file_rename_outline',
                ],
            ],
            [
                'value' => 'material-icons drive_folder_upload',
                'label' => 'drive_folder_upload',
                'attributes' => [
                    'data-icon' => 'material-icons drive_folder_upload',
                    'data-icon-value' => 'drive_folder_upload',
                ],
            ],
            [
                'value' => 'material-icons dry',
                'label' => 'dry',
                'attributes' => [
                    'data-icon' => 'material-icons dry',
                    'data-icon-value' => 'dry',
                ],
            ],
            [
                'value' => 'material-icons dry_cleaning',
                'label' => 'dry_cleaning',
                'attributes' => [
                    'data-icon' => 'material-icons dry_cleaning',
                    'data-icon-value' => 'dry_cleaning',
                ],
            ],
            [
                'value' => 'material-icons duo',
                'label' => 'duo',
                'attributes' => [
                    'data-icon' => 'material-icons duo',
                    'data-icon-value' => 'duo',
                ],
            ],
            [
                'value' => 'material-icons dvr',
                'label' => 'dvr',
                'attributes' => [
                    'data-icon' => 'material-icons dvr',
                    'data-icon-value' => 'dvr',
                ],
            ],
            [
                'value' => 'material-icons dynamic_feed',
                'label' => 'dynamic_feed',
                'attributes' => [
                    'data-icon' => 'material-icons dynamic_feed',
                    'data-icon-value' => 'dynamic_feed',
                ],
            ],
            [
                'value' => 'material-icons dynamic_form',
                'label' => 'dynamic_form',
                'attributes' => [
                    'data-icon' => 'material-icons dynamic_form',
                    'data-icon-value' => 'dynamic_form',
                ],
            ],
            [
                'value' => 'material-icons e_mobiledata',
                'label' => 'e_mobiledata',
                'attributes' => [
                    'data-icon' => 'material-icons e_mobiledata',
                    'data-icon-value' => 'e_mobiledata',
                ],
            ],
            [
                'value' => 'material-icons earbuds',
                'label' => 'earbuds',
                'attributes' => [
                    'data-icon' => 'material-icons earbuds',
                    'data-icon-value' => 'earbuds',
                ],
            ],
            [
                'value' => 'material-icons earbuds_battery',
                'label' => 'earbuds_battery',
                'attributes' => [
                    'data-icon' => 'material-icons earbuds_battery',
                    'data-icon-value' => 'earbuds_battery',
                ],
            ],
            [
                'value' => 'material-icons east',
                'label' => 'east',
                'attributes' => [
                    'data-icon' => 'material-icons east',
                    'data-icon-value' => 'east',
                ],
            ],
            [
                'value' => 'material-icons eco',
                'label' => 'eco',
                'attributes' => [
                    'data-icon' => 'material-icons eco',
                    'data-icon-value' => 'eco',
                ],
            ],
            [
                'value' => 'material-icons edgesensor_high',
                'label' => 'edgesensor_high',
                'attributes' => [
                    'data-icon' => 'material-icons edgesensor_high',
                    'data-icon-value' => 'edgesensor_high',
                ],
            ],
            [
                'value' => 'material-icons edgesensor_low',
                'label' => 'edgesensor_low',
                'attributes' => [
                    'data-icon' => 'material-icons edgesensor_low',
                    'data-icon-value' => 'edgesensor_low',
                ],
            ],
            [
                'value' => 'material-icons edit',
                'label' => 'edit',
                'attributes' => [
                    'data-icon' => 'material-icons edit',
                    'data-icon-value' => 'edit',
                ],
            ],
            [
                'value' => 'material-icons edit_attributes',
                'label' => 'edit_attributes',
                'attributes' => [
                    'data-icon' => 'material-icons edit_attributes',
                    'data-icon-value' => 'edit_attributes',
                ],
            ],
            [
                'value' => 'material-icons edit_calendar',
                'label' => 'edit_calendar',
                'attributes' => [
                    'data-icon' => 'material-icons edit_calendar',
                    'data-icon-value' => 'edit_calendar',
                ],
            ],
            [
                'value' => 'material-icons edit_location',
                'label' => 'edit_location',
                'attributes' => [
                    'data-icon' => 'material-icons edit_location',
                    'data-icon-value' => 'edit_location',
                ],
            ],
            [
                'value' => 'material-icons edit_location_alt',
                'label' => 'edit_location_alt',
                'attributes' => [
                    'data-icon' => 'material-icons edit_location_alt',
                    'data-icon-value' => 'edit_location_alt',
                ],
            ],
            [
                'value' => 'material-icons edit_note',
                'label' => 'edit_note',
                'attributes' => [
                    'data-icon' => 'material-icons edit_note',
                    'data-icon-value' => 'edit_note',
                ],
            ],
            [
                'value' => 'material-icons edit_notifications',
                'label' => 'edit_notifications',
                'attributes' => [
                    'data-icon' => 'material-icons edit_notifications',
                    'data-icon-value' => 'edit_notifications',
                ],
            ],
            [
                'value' => 'material-icons edit_off',
                'label' => 'edit_off',
                'attributes' => [
                    'data-icon' => 'material-icons edit_off',
                    'data-icon-value' => 'edit_off',
                ],
            ],
            [
                'value' => 'material-icons edit_road',
                'label' => 'edit_road',
                'attributes' => [
                    'data-icon' => 'material-icons edit_road',
                    'data-icon-value' => 'edit_road',
                ],
            ],
            [
                'value' => 'material-icons egg',
                'label' => 'egg',
                'attributes' => [
                    'data-icon' => 'material-icons egg',
                    'data-icon-value' => 'egg',
                ],
            ],
            [
                'value' => 'material-icons egg_alt',
                'label' => 'egg_alt',
                'attributes' => [
                    'data-icon' => 'material-icons egg_alt',
                    'data-icon-value' => 'egg_alt',
                ],
            ],
            [
                'value' => 'material-icons eject',
                'label' => 'eject',
                'attributes' => [
                    'data-icon' => 'material-icons eject',
                    'data-icon-value' => 'eject',
                ],
            ],
            [
                'value' => 'material-icons elderly',
                'label' => 'elderly',
                'attributes' => [
                    'data-icon' => 'material-icons elderly',
                    'data-icon-value' => 'elderly',
                ],
            ],
            [
                'value' => 'material-icons elderly_woman',
                'label' => 'elderly_woman',
                'attributes' => [
                    'data-icon' => 'material-icons elderly_woman',
                    'data-icon-value' => 'elderly_woman',
                ],
            ],
            [
                'value' => 'material-icons electric_bike',
                'label' => 'electric_bike',
                'attributes' => [
                    'data-icon' => 'material-icons electric_bike',
                    'data-icon-value' => 'electric_bike',
                ],
            ],
            [
                'value' => 'material-icons electric_bolt',
                'label' => 'electric_bolt',
                'attributes' => [
                    'data-icon' => 'material-icons electric_bolt',
                    'data-icon-value' => 'electric_bolt',
                ],
            ],
            [
                'value' => 'material-icons electric_car',
                'label' => 'electric_car',
                'attributes' => [
                    'data-icon' => 'material-icons electric_car',
                    'data-icon-value' => 'electric_car',
                ],
            ],
            [
                'value' => 'material-icons electric_meter',
                'label' => 'electric_meter',
                'attributes' => [
                    'data-icon' => 'material-icons electric_meter',
                    'data-icon-value' => 'electric_meter',
                ],
            ],
            [
                'value' => 'material-icons electric_moped',
                'label' => 'electric_moped',
                'attributes' => [
                    'data-icon' => 'material-icons electric_moped',
                    'data-icon-value' => 'electric_moped',
                ],
            ],
            [
                'value' => 'material-icons electric_rickshaw',
                'label' => 'electric_rickshaw',
                'attributes' => [
                    'data-icon' => 'material-icons electric_rickshaw',
                    'data-icon-value' => 'electric_rickshaw',
                ],
            ],
            [
                'value' => 'material-icons electric_scooter',
                'label' => 'electric_scooter',
                'attributes' => [
                    'data-icon' => 'material-icons electric_scooter',
                    'data-icon-value' => 'electric_scooter',
                ],
            ],
            [
                'value' => 'material-icons electrical_services',
                'label' => 'electrical_services',
                'attributes' => [
                    'data-icon' => 'material-icons electrical_services',
                    'data-icon-value' => 'electrical_services',
                ],
            ],
            [
                'value' => 'material-icons elevator',
                'label' => 'elevator',
                'attributes' => [
                    'data-icon' => 'material-icons elevator',
                    'data-icon-value' => 'elevator',
                ],
            ],
            [
                'value' => 'material-icons email',
                'label' => 'email',
                'attributes' => [
                    'data-icon' => 'material-icons email',
                    'data-icon-value' => 'email',
                ],
            ],
            [
                'value' => 'material-icons emergency',
                'label' => 'emergency',
                'attributes' => [
                    'data-icon' => 'material-icons emergency',
                    'data-icon-value' => 'emergency',
                ],
            ],
            [
                'value' => 'material-icons emergency_recording',
                'label' => 'emergency_recording',
                'attributes' => [
                    'data-icon' => 'material-icons emergency_recording',
                    'data-icon-value' => 'emergency_recording',
                ],
            ],
            [
                'value' => 'material-icons emergency_share',
                'label' => 'emergency_share',
                'attributes' => [
                    'data-icon' => 'material-icons emergency_share',
                    'data-icon-value' => 'emergency_share',
                ],
            ],
            [
                'value' => 'material-icons emoji_emotions',
                'label' => 'emoji_emotions',
                'attributes' => [
                    'data-icon' => 'material-icons emoji_emotions',
                    'data-icon-value' => 'emoji_emotions',
                ],
            ],
            [
                'value' => 'material-icons emoji_events',
                'label' => 'emoji_events',
                'attributes' => [
                    'data-icon' => 'material-icons emoji_events',
                    'data-icon-value' => 'emoji_events',
                ],
            ],
            [
                'value' => 'material-icons emoji_flags',
                'label' => 'emoji_flags',
                'attributes' => [
                    'data-icon' => 'material-icons emoji_flags',
                    'data-icon-value' => 'emoji_flags',
                ],
            ],
            [
                'value' => 'material-icons emoji_food_beverage',
                'label' => 'emoji_food_beverage',
                'attributes' => [
                    'data-icon' => 'material-icons emoji_food_beverage',
                    'data-icon-value' => 'emoji_food_beverage',
                ],
            ],
            [
                'value' => 'material-icons emoji_nature',
                'label' => 'emoji_nature',
                'attributes' => [
                    'data-icon' => 'material-icons emoji_nature',
                    'data-icon-value' => 'emoji_nature',
                ],
            ],
            [
                'value' => 'material-icons emoji_objects',
                'label' => 'emoji_objects',
                'attributes' => [
                    'data-icon' => 'material-icons emoji_objects',
                    'data-icon-value' => 'emoji_objects',
                ],
            ],
            [
                'value' => 'material-icons emoji_people',
                'label' => 'emoji_people',
                'attributes' => [
                    'data-icon' => 'material-icons emoji_people',
                    'data-icon-value' => 'emoji_people',
                ],
            ],
            [
                'value' => 'material-icons emoji_symbols',
                'label' => 'emoji_symbols',
                'attributes' => [
                    'data-icon' => 'material-icons emoji_symbols',
                    'data-icon-value' => 'emoji_symbols',
                ],
            ],
            [
                'value' => 'material-icons emoji_transportation',
                'label' => 'emoji_transportation',
                'attributes' => [
                    'data-icon' => 'material-icons emoji_transportation',
                    'data-icon-value' => 'emoji_transportation',
                ],
            ],
            [
                'value' => 'material-icons energy_savings_leaf',
                'label' => 'energy_savings_leaf',
                'attributes' => [
                    'data-icon' => 'material-icons energy_savings_leaf',
                    'data-icon-value' => 'energy_savings_leaf',
                ],
            ],
            [
                'value' => 'material-icons engineering',
                'label' => 'engineering',
                'attributes' => [
                    'data-icon' => 'material-icons engineering',
                    'data-icon-value' => 'engineering',
                ],
            ],
            [
                'value' => 'material-icons enhance_photo_translate',
                'label' => 'enhance_photo_translate',
                'attributes' => [
                    'data-icon' => 'material-icons enhance_photo_translate',
                    'data-icon-value' => 'enhance_photo_translate',
                ],
            ],
            [
                'value' => 'material-icons enhanced_encryption',
                'label' => 'enhanced_encryption',
                'attributes' => [
                    'data-icon' => 'material-icons enhanced_encryption',
                    'data-icon-value' => 'enhanced_encryption',
                ],
            ],
            [
                'value' => 'material-icons equalizer',
                'label' => 'equalizer',
                'attributes' => [
                    'data-icon' => 'material-icons equalizer',
                    'data-icon-value' => 'equalizer',
                ],
            ],
            [
                'value' => 'material-icons error',
                'label' => 'error',
                'attributes' => [
                    'data-icon' => 'material-icons error',
                    'data-icon-value' => 'error',
                ],
            ],
            [
                'value' => 'material-icons error_outline',
                'label' => 'error_outline',
                'attributes' => [
                    'data-icon' => 'material-icons error_outline',
                    'data-icon-value' => 'error_outline',
                ],
            ],
            [
                'value' => 'material-icons escalator',
                'label' => 'escalator',
                'attributes' => [
                    'data-icon' => 'material-icons escalator',
                    'data-icon-value' => 'escalator',
                ],
            ],
            [
                'value' => 'material-icons escalator_warning',
                'label' => 'escalator_warning',
                'attributes' => [
                    'data-icon' => 'material-icons escalator_warning',
                    'data-icon-value' => 'escalator_warning',
                ],
            ],
            [
                'value' => 'material-icons euro',
                'label' => 'euro',
                'attributes' => [
                    'data-icon' => 'material-icons euro',
                    'data-icon-value' => 'euro',
                ],
            ],
            [
                'value' => 'material-icons euro_symbol',
                'label' => 'euro_symbol',
                'attributes' => [
                    'data-icon' => 'material-icons euro_symbol',
                    'data-icon-value' => 'euro_symbol',
                ],
            ],
            [
                'value' => 'material-icons ev_station',
                'label' => 'ev_station',
                'attributes' => [
                    'data-icon' => 'material-icons ev_station',
                    'data-icon-value' => 'ev_station',
                ],
            ],
            [
                'value' => 'material-icons event',
                'label' => 'event',
                'attributes' => [
                    'data-icon' => 'material-icons event',
                    'data-icon-value' => 'event',
                ],
            ],
            [
                'value' => 'material-icons event_available',
                'label' => 'event_available',
                'attributes' => [
                    'data-icon' => 'material-icons event_available',
                    'data-icon-value' => 'event_available',
                ],
            ],
            [
                'value' => 'material-icons event_busy',
                'label' => 'event_busy',
                'attributes' => [
                    'data-icon' => 'material-icons event_busy',
                    'data-icon-value' => 'event_busy',
                ],
            ],
            [
                'value' => 'material-icons event_note',
                'label' => 'event_note',
                'attributes' => [
                    'data-icon' => 'material-icons event_note',
                    'data-icon-value' => 'event_note',
                ],
            ],
            [
                'value' => 'material-icons event_repeat',
                'label' => 'event_repeat',
                'attributes' => [
                    'data-icon' => 'material-icons event_repeat',
                    'data-icon-value' => 'event_repeat',
                ],
            ],
            [
                'value' => 'material-icons event_seat',
                'label' => 'event_seat',
                'attributes' => [
                    'data-icon' => 'material-icons event_seat',
                    'data-icon-value' => 'event_seat',
                ],
            ],
            [
                'value' => 'material-icons exit_to_app',
                'label' => 'exit_to_app',
                'attributes' => [
                    'data-icon' => 'material-icons exit_to_app',
                    'data-icon-value' => 'exit_to_app',
                ],
            ],
            [
                'value' => 'material-icons expand',
                'label' => 'expand',
                'attributes' => [
                    'data-icon' => 'material-icons expand',
                    'data-icon-value' => 'expand',
                ],
            ],
            [
                'value' => 'material-icons expand_circle_down',
                'label' => 'expand_circle_down',
                'attributes' => [
                    'data-icon' => 'material-icons expand_circle_down',
                    'data-icon-value' => 'expand_circle_down',
                ],
            ],
            [
                'value' => 'material-icons expand_less',
                'label' => 'expand_less',
                'attributes' => [
                    'data-icon' => 'material-icons expand_less',
                    'data-icon-value' => 'expand_less',
                ],
            ],
            [
                'value' => 'material-icons expand_more',
                'label' => 'expand_more',
                'attributes' => [
                    'data-icon' => 'material-icons expand_more',
                    'data-icon-value' => 'expand_more',
                ],
            ],
            [
                'value' => 'material-icons explicit',
                'label' => 'explicit',
                'attributes' => [
                    'data-icon' => 'material-icons explicit',
                    'data-icon-value' => 'explicit',
                ],
            ],
            [
                'value' => 'material-icons explore',
                'label' => 'explore',
                'attributes' => [
                    'data-icon' => 'material-icons explore',
                    'data-icon-value' => 'explore',
                ],
            ],
            [
                'value' => 'material-icons explore_off',
                'label' => 'explore_off',
                'attributes' => [
                    'data-icon' => 'material-icons explore_off',
                    'data-icon-value' => 'explore_off',
                ],
            ],
            [
                'value' => 'material-icons exposure',
                'label' => 'exposure',
                'attributes' => [
                    'data-icon' => 'material-icons exposure',
                    'data-icon-value' => 'exposure',
                ],
            ],
            [
                'value' => 'material-icons exposure_minus_1',
                'label' => 'exposure_minus_1',
                'attributes' => [
                    'data-icon' => 'material-icons exposure_minus_1',
                    'data-icon-value' => 'exposure_minus_1',
                ],
            ],
            [
                'value' => 'material-icons exposure_minus_2',
                'label' => 'exposure_minus_2',
                'attributes' => [
                    'data-icon' => 'material-icons exposure_minus_2',
                    'data-icon-value' => 'exposure_minus_2',
                ],
            ],
            [
                'value' => 'material-icons exposure_neg_1',
                'label' => 'exposure_neg_1',
                'attributes' => [
                    'data-icon' => 'material-icons exposure_neg_1',
                    'data-icon-value' => 'exposure_neg_1',
                ],
            ],
            [
                'value' => 'material-icons exposure_neg_2',
                'label' => 'exposure_neg_2',
                'attributes' => [
                    'data-icon' => 'material-icons exposure_neg_2',
                    'data-icon-value' => 'exposure_neg_2',
                ],
            ],
            [
                'value' => 'material-icons exposure_plus_1',
                'label' => 'exposure_plus_1',
                'attributes' => [
                    'data-icon' => 'material-icons exposure_plus_1',
                    'data-icon-value' => 'exposure_plus_1',
                ],
            ],
            [
                'value' => 'material-icons exposure_plus_2',
                'label' => 'exposure_plus_2',
                'attributes' => [
                    'data-icon' => 'material-icons exposure_plus_2',
                    'data-icon-value' => 'exposure_plus_2',
                ],
            ],
            [
                'value' => 'material-icons exposure_zero',
                'label' => 'exposure_zero',
                'attributes' => [
                    'data-icon' => 'material-icons exposure_zero',
                    'data-icon-value' => 'exposure_zero',
                ],
            ],
            [
                'value' => 'material-icons extension',
                'label' => 'extension',
                'attributes' => [
                    'data-icon' => 'material-icons extension',
                    'data-icon-value' => 'extension',
                ],
            ],
            [
                'value' => 'material-icons extension_off',
                'label' => 'extension_off',
                'attributes' => [
                    'data-icon' => 'material-icons extension_off',
                    'data-icon-value' => 'extension_off',
                ],
            ],
            [
                'value' => 'material-icons face',
                'label' => 'face',
                'attributes' => [
                    'data-icon' => 'material-icons face',
                    'data-icon-value' => 'face',
                ],
            ],
            [
                'value' => 'material-icons face_retouching_natural',
                'label' => 'face_retouching_natural',
                'attributes' => [
                    'data-icon' => 'material-icons face_retouching_natural',
                    'data-icon-value' => 'face_retouching_natural',
                ],
            ],
            [
                'value' => 'material-icons face_retouching_off',
                'label' => 'face_retouching_off',
                'attributes' => [
                    'data-icon' => 'material-icons face_retouching_off',
                    'data-icon-value' => 'face_retouching_off',
                ],
            ],
            [
                'value' => 'material-icons facebook',
                'label' => 'facebook',
                'attributes' => [
                    'data-icon' => 'material-icons facebook',
                    'data-icon-value' => 'facebook',
                ],
            ],
            [
                'value' => 'material-icons fact_check',
                'label' => 'fact_check',
                'attributes' => [
                    'data-icon' => 'material-icons fact_check',
                    'data-icon-value' => 'fact_check',
                ],
            ],
            [
                'value' => 'material-icons factory',
                'label' => 'factory',
                'attributes' => [
                    'data-icon' => 'material-icons factory',
                    'data-icon-value' => 'factory',
                ],
            ],
            [
                'value' => 'material-icons family_restroom',
                'label' => 'family_restroom',
                'attributes' => [
                    'data-icon' => 'material-icons family_restroom',
                    'data-icon-value' => 'family_restroom',
                ],
            ],
            [
                'value' => 'material-icons fast_forward',
                'label' => 'fast_forward',
                'attributes' => [
                    'data-icon' => 'material-icons fast_forward',
                    'data-icon-value' => 'fast_forward',
                ],
            ],
            [
                'value' => 'material-icons fast_rewind',
                'label' => 'fast_rewind',
                'attributes' => [
                    'data-icon' => 'material-icons fast_rewind',
                    'data-icon-value' => 'fast_rewind',
                ],
            ],
            [
                'value' => 'material-icons fastfood',
                'label' => 'fastfood',
                'attributes' => [
                    'data-icon' => 'material-icons fastfood',
                    'data-icon-value' => 'fastfood',
                ],
            ],
            [
                'value' => 'material-icons favorite',
                'label' => 'favorite',
                'attributes' => [
                    'data-icon' => 'material-icons favorite',
                    'data-icon-value' => 'favorite',
                ],
            ],
            [
                'value' => 'material-icons favorite_border',
                'label' => 'favorite_border',
                'attributes' => [
                    'data-icon' => 'material-icons favorite_border',
                    'data-icon-value' => 'favorite_border',
                ],
            ],
            [
                'value' => 'material-icons favorite_outline',
                'label' => 'favorite_outline',
                'attributes' => [
                    'data-icon' => 'material-icons favorite_outline',
                    'data-icon-value' => 'favorite_outline',
                ],
            ],
            [
                'value' => 'material-icons fax',
                'label' => 'fax',
                'attributes' => [
                    'data-icon' => 'material-icons fax',
                    'data-icon-value' => 'fax',
                ],
            ],
            [
                'value' => 'material-icons featured_play_list',
                'label' => 'featured_play_list',
                'attributes' => [
                    'data-icon' => 'material-icons featured_play_list',
                    'data-icon-value' => 'featured_play_list',
                ],
            ],
            [
                'value' => 'material-icons featured_video',
                'label' => 'featured_video',
                'attributes' => [
                    'data-icon' => 'material-icons featured_video',
                    'data-icon-value' => 'featured_video',
                ],
            ],
            [
                'value' => 'material-icons feed',
                'label' => 'feed',
                'attributes' => [
                    'data-icon' => 'material-icons feed',
                    'data-icon-value' => 'feed',
                ],
            ],
            [
                'value' => 'material-icons feedback',
                'label' => 'feedback',
                'attributes' => [
                    'data-icon' => 'material-icons feedback',
                    'data-icon-value' => 'feedback',
                ],
            ],
            [
                'value' => 'material-icons female',
                'label' => 'female',
                'attributes' => [
                    'data-icon' => 'material-icons female',
                    'data-icon-value' => 'female',
                ],
            ],
            [
                'value' => 'material-icons fence',
                'label' => 'fence',
                'attributes' => [
                    'data-icon' => 'material-icons fence',
                    'data-icon-value' => 'fence',
                ],
            ],
            [
                'value' => 'material-icons festival',
                'label' => 'festival',
                'attributes' => [
                    'data-icon' => 'material-icons festival',
                    'data-icon-value' => 'festival',
                ],
            ],
            [
                'value' => 'material-icons fiber_dvr',
                'label' => 'fiber_dvr',
                'attributes' => [
                    'data-icon' => 'material-icons fiber_dvr',
                    'data-icon-value' => 'fiber_dvr',
                ],
            ],
            [
                'value' => 'material-icons fiber_manual_record',
                'label' => 'fiber_manual_record',
                'attributes' => [
                    'data-icon' => 'material-icons fiber_manual_record',
                    'data-icon-value' => 'fiber_manual_record',
                ],
            ],
            [
                'value' => 'material-icons fiber_new',
                'label' => 'fiber_new',
                'attributes' => [
                    'data-icon' => 'material-icons fiber_new',
                    'data-icon-value' => 'fiber_new',
                ],
            ],
            [
                'value' => 'material-icons fiber_pin',
                'label' => 'fiber_pin',
                'attributes' => [
                    'data-icon' => 'material-icons fiber_pin',
                    'data-icon-value' => 'fiber_pin',
                ],
            ],
            [
                'value' => 'material-icons fiber_smart_record',
                'label' => 'fiber_smart_record',
                'attributes' => [
                    'data-icon' => 'material-icons fiber_smart_record',
                    'data-icon-value' => 'fiber_smart_record',
                ],
            ],
            [
                'value' => 'material-icons file_copy',
                'label' => 'file_copy',
                'attributes' => [
                    'data-icon' => 'material-icons file_copy',
                    'data-icon-value' => 'file_copy',
                ],
            ],
            [
                'value' => 'material-icons file_download',
                'label' => 'file_download',
                'attributes' => [
                    'data-icon' => 'material-icons file_download',
                    'data-icon-value' => 'file_download',
                ],
            ],
            [
                'value' => 'material-icons file_download_done',
                'label' => 'file_download_done',
                'attributes' => [
                    'data-icon' => 'material-icons file_download_done',
                    'data-icon-value' => 'file_download_done',
                ],
            ],
            [
                'value' => 'material-icons file_download_off',
                'label' => 'file_download_off',
                'attributes' => [
                    'data-icon' => 'material-icons file_download_off',
                    'data-icon-value' => 'file_download_off',
                ],
            ],
            [
                'value' => 'material-icons file_open',
                'label' => 'file_open',
                'attributes' => [
                    'data-icon' => 'material-icons file_open',
                    'data-icon-value' => 'file_open',
                ],
            ],
            [
                'value' => 'material-icons file_present',
                'label' => 'file_present',
                'attributes' => [
                    'data-icon' => 'material-icons file_present',
                    'data-icon-value' => 'file_present',
                ],
            ],
            [
                'value' => 'material-icons file_upload',
                'label' => 'file_upload',
                'attributes' => [
                    'data-icon' => 'material-icons file_upload',
                    'data-icon-value' => 'file_upload',
                ],
            ],
            [
                'value' => 'material-icons filter',
                'label' => 'filter',
                'attributes' => [
                    'data-icon' => 'material-icons filter',
                    'data-icon-value' => 'filter',
                ],
            ],
            [
                'value' => 'material-icons filter_1',
                'label' => 'filter_1',
                'attributes' => [
                    'data-icon' => 'material-icons filter_1',
                    'data-icon-value' => 'filter_1',
                ],
            ],
            [
                'value' => 'material-icons filter_2',
                'label' => 'filter_2',
                'attributes' => [
                    'data-icon' => 'material-icons filter_2',
                    'data-icon-value' => 'filter_2',
                ],
            ],
            [
                'value' => 'material-icons filter_3',
                'label' => 'filter_3',
                'attributes' => [
                    'data-icon' => 'material-icons filter_3',
                    'data-icon-value' => 'filter_3',
                ],
            ],
            [
                'value' => 'material-icons filter_4',
                'label' => 'filter_4',
                'attributes' => [
                    'data-icon' => 'material-icons filter_4',
                    'data-icon-value' => 'filter_4',
                ],
            ],
            [
                'value' => 'material-icons filter_5',
                'label' => 'filter_5',
                'attributes' => [
                    'data-icon' => 'material-icons filter_5',
                    'data-icon-value' => 'filter_5',
                ],
            ],
            [
                'value' => 'material-icons filter_6',
                'label' => 'filter_6',
                'attributes' => [
                    'data-icon' => 'material-icons filter_6',
                    'data-icon-value' => 'filter_6',
                ],
            ],
            [
                'value' => 'material-icons filter_7',
                'label' => 'filter_7',
                'attributes' => [
                    'data-icon' => 'material-icons filter_7',
                    'data-icon-value' => 'filter_7',
                ],
            ],
            [
                'value' => 'material-icons filter_8',
                'label' => 'filter_8',
                'attributes' => [
                    'data-icon' => 'material-icons filter_8',
                    'data-icon-value' => 'filter_8',
                ],
            ],
            [
                'value' => 'material-icons filter_9',
                'label' => 'filter_9',
                'attributes' => [
                    'data-icon' => 'material-icons filter_9',
                    'data-icon-value' => 'filter_9',
                ],
            ],
            [
                'value' => 'material-icons filter_9_plus',
                'label' => 'filter_9_plus',
                'attributes' => [
                    'data-icon' => 'material-icons filter_9_plus',
                    'data-icon-value' => 'filter_9_plus',
                ],
            ],
            [
                'value' => 'material-icons filter_alt',
                'label' => 'filter_alt',
                'attributes' => [
                    'data-icon' => 'material-icons filter_alt',
                    'data-icon-value' => 'filter_alt',
                ],
            ],
            [
                'value' => 'material-icons filter_alt_off',
                'label' => 'filter_alt_off',
                'attributes' => [
                    'data-icon' => 'material-icons filter_alt_off',
                    'data-icon-value' => 'filter_alt_off',
                ],
            ],
            [
                'value' => 'material-icons filter_b_and_w',
                'label' => 'filter_b_and_w',
                'attributes' => [
                    'data-icon' => 'material-icons filter_b_and_w',
                    'data-icon-value' => 'filter_b_and_w',
                ],
            ],
            [
                'value' => 'material-icons filter_center_focus',
                'label' => 'filter_center_focus',
                'attributes' => [
                    'data-icon' => 'material-icons filter_center_focus',
                    'data-icon-value' => 'filter_center_focus',
                ],
            ],
            [
                'value' => 'material-icons filter_drama',
                'label' => 'filter_drama',
                'attributes' => [
                    'data-icon' => 'material-icons filter_drama',
                    'data-icon-value' => 'filter_drama',
                ],
            ],
            [
                'value' => 'material-icons filter_frames',
                'label' => 'filter_frames',
                'attributes' => [
                    'data-icon' => 'material-icons filter_frames',
                    'data-icon-value' => 'filter_frames',
                ],
            ],
            [
                'value' => 'material-icons filter_hdr',
                'label' => 'filter_hdr',
                'attributes' => [
                    'data-icon' => 'material-icons filter_hdr',
                    'data-icon-value' => 'filter_hdr',
                ],
            ],
            [
                'value' => 'material-icons filter_list',
                'label' => 'filter_list',
                'attributes' => [
                    'data-icon' => 'material-icons filter_list',
                    'data-icon-value' => 'filter_list',
                ],
            ],
            [
                'value' => 'material-icons filter_list_alt',
                'label' => 'filter_list_alt',
                'attributes' => [
                    'data-icon' => 'material-icons filter_list_alt',
                    'data-icon-value' => 'filter_list_alt',
                ],
            ],
            [
                'value' => 'material-icons filter_list_off',
                'label' => 'filter_list_off',
                'attributes' => [
                    'data-icon' => 'material-icons filter_list_off',
                    'data-icon-value' => 'filter_list_off',
                ],
            ],
            [
                'value' => 'material-icons filter_none',
                'label' => 'filter_none',
                'attributes' => [
                    'data-icon' => 'material-icons filter_none',
                    'data-icon-value' => 'filter_none',
                ],
            ],
            [
                'value' => 'material-icons filter_tilt_shift',
                'label' => 'filter_tilt_shift',
                'attributes' => [
                    'data-icon' => 'material-icons filter_tilt_shift',
                    'data-icon-value' => 'filter_tilt_shift',
                ],
            ],
            [
                'value' => 'material-icons filter_vintage',
                'label' => 'filter_vintage',
                'attributes' => [
                    'data-icon' => 'material-icons filter_vintage',
                    'data-icon-value' => 'filter_vintage',
                ],
            ],
            [
                'value' => 'material-icons find_in_page',
                'label' => 'find_in_page',
                'attributes' => [
                    'data-icon' => 'material-icons find_in_page',
                    'data-icon-value' => 'find_in_page',
                ],
            ],
            [
                'value' => 'material-icons find_replace',
                'label' => 'find_replace',
                'attributes' => [
                    'data-icon' => 'material-icons find_replace',
                    'data-icon-value' => 'find_replace',
                ],
            ],
            [
                'value' => 'material-icons fingerprint',
                'label' => 'fingerprint',
                'attributes' => [
                    'data-icon' => 'material-icons fingerprint',
                    'data-icon-value' => 'fingerprint',
                ],
            ],
            [
                'value' => 'material-icons fire_extinguisher',
                'label' => 'fire_extinguisher',
                'attributes' => [
                    'data-icon' => 'material-icons fire_extinguisher',
                    'data-icon-value' => 'fire_extinguisher',
                ],
            ],
            [
                'value' => 'material-icons fire_hydrant',
                'label' => 'fire_hydrant',
                'attributes' => [
                    'data-icon' => 'material-icons fire_hydrant',
                    'data-icon-value' => 'fire_hydrant',
                ],
            ],
            [
                'value' => 'material-icons fireplace',
                'label' => 'fireplace',
                'attributes' => [
                    'data-icon' => 'material-icons fireplace',
                    'data-icon-value' => 'fireplace',
                ],
            ],
            [
                'value' => 'material-icons first_page',
                'label' => 'first_page',
                'attributes' => [
                    'data-icon' => 'material-icons first_page',
                    'data-icon-value' => 'first_page',
                ],
            ],
            [
                'value' => 'material-icons fit_screen',
                'label' => 'fit_screen',
                'attributes' => [
                    'data-icon' => 'material-icons fit_screen',
                    'data-icon-value' => 'fit_screen',
                ],
            ],
            [
                'value' => 'material-icons fitbit',
                'label' => 'fitbit',
                'attributes' => [
                    'data-icon' => 'material-icons fitbit',
                    'data-icon-value' => 'fitbit',
                ],
            ],
            [
                'value' => 'material-icons fitness_center',
                'label' => 'fitness_center',
                'attributes' => [
                    'data-icon' => 'material-icons fitness_center',
                    'data-icon-value' => 'fitness_center',
                ],
            ],
            [
                'value' => 'material-icons flag',
                'label' => 'flag',
                'attributes' => [
                    'data-icon' => 'material-icons flag',
                    'data-icon-value' => 'flag',
                ],
            ],
            [
                'value' => 'material-icons flag_circle',
                'label' => 'flag_circle',
                'attributes' => [
                    'data-icon' => 'material-icons flag_circle',
                    'data-icon-value' => 'flag_circle',
                ],
            ],
            [
                'value' => 'material-icons flaky',
                'label' => 'flaky',
                'attributes' => [
                    'data-icon' => 'material-icons flaky',
                    'data-icon-value' => 'flaky',
                ],
            ],
            [
                'value' => 'material-icons flare',
                'label' => 'flare',
                'attributes' => [
                    'data-icon' => 'material-icons flare',
                    'data-icon-value' => 'flare',
                ],
            ],
            [
                'value' => 'material-icons flash_auto',
                'label' => 'flash_auto',
                'attributes' => [
                    'data-icon' => 'material-icons flash_auto',
                    'data-icon-value' => 'flash_auto',
                ],
            ],
            [
                'value' => 'material-icons flash_off',
                'label' => 'flash_off',
                'attributes' => [
                    'data-icon' => 'material-icons flash_off',
                    'data-icon-value' => 'flash_off',
                ],
            ],
            [
                'value' => 'material-icons flash_on',
                'label' => 'flash_on',
                'attributes' => [
                    'data-icon' => 'material-icons flash_on',
                    'data-icon-value' => 'flash_on',
                ],
            ],
            [
                'value' => 'material-icons flashlight_off',
                'label' => 'flashlight_off',
                'attributes' => [
                    'data-icon' => 'material-icons flashlight_off',
                    'data-icon-value' => 'flashlight_off',
                ],
            ],
            [
                'value' => 'material-icons flashlight_on',
                'label' => 'flashlight_on',
                'attributes' => [
                    'data-icon' => 'material-icons flashlight_on',
                    'data-icon-value' => 'flashlight_on',
                ],
            ],
            [
                'value' => 'material-icons flatware',
                'label' => 'flatware',
                'attributes' => [
                    'data-icon' => 'material-icons flatware',
                    'data-icon-value' => 'flatware',
                ],
            ],
            [
                'value' => 'material-icons flight',
                'label' => 'flight',
                'attributes' => [
                    'data-icon' => 'material-icons flight',
                    'data-icon-value' => 'flight',
                ],
            ],
            [
                'value' => 'material-icons flight_class',
                'label' => 'flight_class',
                'attributes' => [
                    'data-icon' => 'material-icons flight_class',
                    'data-icon-value' => 'flight_class',
                ],
            ],
            [
                'value' => 'material-icons flight_land',
                'label' => 'flight_land',
                'attributes' => [
                    'data-icon' => 'material-icons flight_land',
                    'data-icon-value' => 'flight_land',
                ],
            ],
            [
                'value' => 'material-icons flight_takeoff',
                'label' => 'flight_takeoff',
                'attributes' => [
                    'data-icon' => 'material-icons flight_takeoff',
                    'data-icon-value' => 'flight_takeoff',
                ],
            ],
            [
                'value' => 'material-icons flip',
                'label' => 'flip',
                'attributes' => [
                    'data-icon' => 'material-icons flip',
                    'data-icon-value' => 'flip',
                ],
            ],
            [
                'value' => 'material-icons flip_camera_android',
                'label' => 'flip_camera_android',
                'attributes' => [
                    'data-icon' => 'material-icons flip_camera_android',
                    'data-icon-value' => 'flip_camera_android',
                ],
            ],
            [
                'value' => 'material-icons flip_camera_ios',
                'label' => 'flip_camera_ios',
                'attributes' => [
                    'data-icon' => 'material-icons flip_camera_ios',
                    'data-icon-value' => 'flip_camera_ios',
                ],
            ],
            [
                'value' => 'material-icons flip_to_back',
                'label' => 'flip_to_back',
                'attributes' => [
                    'data-icon' => 'material-icons flip_to_back',
                    'data-icon-value' => 'flip_to_back',
                ],
            ],
            [
                'value' => 'material-icons flip_to_front',
                'label' => 'flip_to_front',
                'attributes' => [
                    'data-icon' => 'material-icons flip_to_front',
                    'data-icon-value' => 'flip_to_front',
                ],
            ],
            [
                'value' => 'material-icons flood',
                'label' => 'flood',
                'attributes' => [
                    'data-icon' => 'material-icons flood',
                    'data-icon-value' => 'flood',
                ],
            ],
            [
                'value' => 'material-icons flourescent',
                'label' => 'flourescent',
                'attributes' => [
                    'data-icon' => 'material-icons flourescent',
                    'data-icon-value' => 'flourescent',
                ],
            ],
            [
                'value' => 'material-icons flutter_dash',
                'label' => 'flutter_dash',
                'attributes' => [
                    'data-icon' => 'material-icons flutter_dash',
                    'data-icon-value' => 'flutter_dash',
                ],
            ],
            [
                'value' => 'material-icons fmd_bad',
                'label' => 'fmd_bad',
                'attributes' => [
                    'data-icon' => 'material-icons fmd_bad',
                    'data-icon-value' => 'fmd_bad',
                ],
            ],
            [
                'value' => 'material-icons fmd_good',
                'label' => 'fmd_good',
                'attributes' => [
                    'data-icon' => 'material-icons fmd_good',
                    'data-icon-value' => 'fmd_good',
                ],
            ],
            [
                'value' => 'material-icons foggy',
                'label' => 'foggy',
                'attributes' => [
                    'data-icon' => 'material-icons foggy',
                    'data-icon-value' => 'foggy',
                ],
            ],
            [
                'value' => 'material-icons folder',
                'label' => 'folder',
                'attributes' => [
                    'data-icon' => 'material-icons folder',
                    'data-icon-value' => 'folder',
                ],
            ],
            [
                'value' => 'material-icons folder_copy',
                'label' => 'folder_copy',
                'attributes' => [
                    'data-icon' => 'material-icons folder_copy',
                    'data-icon-value' => 'folder_copy',
                ],
            ],
            [
                'value' => 'material-icons folder_delete',
                'label' => 'folder_delete',
                'attributes' => [
                    'data-icon' => 'material-icons folder_delete',
                    'data-icon-value' => 'folder_delete',
                ],
            ],
            [
                'value' => 'material-icons folder_off',
                'label' => 'folder_off',
                'attributes' => [
                    'data-icon' => 'material-icons folder_off',
                    'data-icon-value' => 'folder_off',
                ],
            ],
            [
                'value' => 'material-icons folder_open',
                'label' => 'folder_open',
                'attributes' => [
                    'data-icon' => 'material-icons folder_open',
                    'data-icon-value' => 'folder_open',
                ],
            ],
            [
                'value' => 'material-icons folder_shared',
                'label' => 'folder_shared',
                'attributes' => [
                    'data-icon' => 'material-icons folder_shared',
                    'data-icon-value' => 'folder_shared',
                ],
            ],
            [
                'value' => 'material-icons folder_special',
                'label' => 'folder_special',
                'attributes' => [
                    'data-icon' => 'material-icons folder_special',
                    'data-icon-value' => 'folder_special',
                ],
            ],
            [
                'value' => 'material-icons folder_zip',
                'label' => 'folder_zip',
                'attributes' => [
                    'data-icon' => 'material-icons folder_zip',
                    'data-icon-value' => 'folder_zip',
                ],
            ],
            [
                'value' => 'material-icons follow_the_signs',
                'label' => 'follow_the_signs',
                'attributes' => [
                    'data-icon' => 'material-icons follow_the_signs',
                    'data-icon-value' => 'follow_the_signs',
                ],
            ],
            [
                'value' => 'material-icons font_download',
                'label' => 'font_download',
                'attributes' => [
                    'data-icon' => 'material-icons font_download',
                    'data-icon-value' => 'font_download',
                ],
            ],
            [
                'value' => 'material-icons font_download_off',
                'label' => 'font_download_off',
                'attributes' => [
                    'data-icon' => 'material-icons font_download_off',
                    'data-icon-value' => 'font_download_off',
                ],
            ],
            [
                'value' => 'material-icons food_bank',
                'label' => 'food_bank',
                'attributes' => [
                    'data-icon' => 'material-icons food_bank',
                    'data-icon-value' => 'food_bank',
                ],
            ],
            [
                'value' => 'material-icons forest',
                'label' => 'forest',
                'attributes' => [
                    'data-icon' => 'material-icons forest',
                    'data-icon-value' => 'forest',
                ],
            ],
            [
                'value' => 'material-icons fork_left',
                'label' => 'fork_left',
                'attributes' => [
                    'data-icon' => 'material-icons fork_left',
                    'data-icon-value' => 'fork_left',
                ],
            ],
            [
                'value' => 'material-icons fork_right',
                'label' => 'fork_right',
                'attributes' => [
                    'data-icon' => 'material-icons fork_right',
                    'data-icon-value' => 'fork_right',
                ],
            ],
            [
                'value' => 'material-icons format_align_center',
                'label' => 'format_align_center',
                'attributes' => [
                    'data-icon' => 'material-icons format_align_center',
                    'data-icon-value' => 'format_align_center',
                ],
            ],
            [
                'value' => 'material-icons format_align_justify',
                'label' => 'format_align_justify',
                'attributes' => [
                    'data-icon' => 'material-icons format_align_justify',
                    'data-icon-value' => 'format_align_justify',
                ],
            ],
            [
                'value' => 'material-icons format_align_left',
                'label' => 'format_align_left',
                'attributes' => [
                    'data-icon' => 'material-icons format_align_left',
                    'data-icon-value' => 'format_align_left',
                ],
            ],
            [
                'value' => 'material-icons format_align_right',
                'label' => 'format_align_right',
                'attributes' => [
                    'data-icon' => 'material-icons format_align_right',
                    'data-icon-value' => 'format_align_right',
                ],
            ],
            [
                'value' => 'material-icons format_bold',
                'label' => 'format_bold',
                'attributes' => [
                    'data-icon' => 'material-icons format_bold',
                    'data-icon-value' => 'format_bold',
                ],
            ],
            [
                'value' => 'material-icons format_clear',
                'label' => 'format_clear',
                'attributes' => [
                    'data-icon' => 'material-icons format_clear',
                    'data-icon-value' => 'format_clear',
                ],
            ],
            [
                'value' => 'material-icons format_color_fill',
                'label' => 'format_color_fill',
                'attributes' => [
                    'data-icon' => 'material-icons format_color_fill',
                    'data-icon-value' => 'format_color_fill',
                ],
            ],
            [
                'value' => 'material-icons format_color_reset',
                'label' => 'format_color_reset',
                'attributes' => [
                    'data-icon' => 'material-icons format_color_reset',
                    'data-icon-value' => 'format_color_reset',
                ],
            ],
            [
                'value' => 'material-icons format_color_text',
                'label' => 'format_color_text',
                'attributes' => [
                    'data-icon' => 'material-icons format_color_text',
                    'data-icon-value' => 'format_color_text',
                ],
            ],
            [
                'value' => 'material-icons format_indent_decrease',
                'label' => 'format_indent_decrease',
                'attributes' => [
                    'data-icon' => 'material-icons format_indent_decrease',
                    'data-icon-value' => 'format_indent_decrease',
                ],
            ],
            [
                'value' => 'material-icons format_indent_increase',
                'label' => 'format_indent_increase',
                'attributes' => [
                    'data-icon' => 'material-icons format_indent_increase',
                    'data-icon-value' => 'format_indent_increase',
                ],
            ],
            [
                'value' => 'material-icons format_italic',
                'label' => 'format_italic',
                'attributes' => [
                    'data-icon' => 'material-icons format_italic',
                    'data-icon-value' => 'format_italic',
                ],
            ],
            [
                'value' => 'material-icons format_line_spacing',
                'label' => 'format_line_spacing',
                'attributes' => [
                    'data-icon' => 'material-icons format_line_spacing',
                    'data-icon-value' => 'format_line_spacing',
                ],
            ],
            [
                'value' => 'material-icons format_list_bulleted',
                'label' => 'format_list_bulleted',
                'attributes' => [
                    'data-icon' => 'material-icons format_list_bulleted',
                    'data-icon-value' => 'format_list_bulleted',
                ],
            ],
            [
                'value' => 'material-icons format_list_numbered',
                'label' => 'format_list_numbered',
                'attributes' => [
                    'data-icon' => 'material-icons format_list_numbered',
                    'data-icon-value' => 'format_list_numbered',
                ],
            ],
            [
                'value' => 'material-icons format_list_numbered_rtl',
                'label' => 'format_list_numbered_rtl',
                'attributes' => [
                    'data-icon' => 'material-icons format_list_numbered_rtl',
                    'data-icon-value' => 'format_list_numbered_rtl',
                ],
            ],
            [
                'value' => 'material-icons format_overline',
                'label' => 'format_overline',
                'attributes' => [
                    'data-icon' => 'material-icons format_overline',
                    'data-icon-value' => 'format_overline',
                ],
            ],
            [
                'value' => 'material-icons format_paint',
                'label' => 'format_paint',
                'attributes' => [
                    'data-icon' => 'material-icons format_paint',
                    'data-icon-value' => 'format_paint',
                ],
            ],
            [
                'value' => 'material-icons format_quote',
                'label' => 'format_quote',
                'attributes' => [
                    'data-icon' => 'material-icons format_quote',
                    'data-icon-value' => 'format_quote',
                ],
            ],
            [
                'value' => 'material-icons format_shapes',
                'label' => 'format_shapes',
                'attributes' => [
                    'data-icon' => 'material-icons format_shapes',
                    'data-icon-value' => 'format_shapes',
                ],
            ],
            [
                'value' => 'material-icons format_size',
                'label' => 'format_size',
                'attributes' => [
                    'data-icon' => 'material-icons format_size',
                    'data-icon-value' => 'format_size',
                ],
            ],
            [
                'value' => 'material-icons format_strikethrough',
                'label' => 'format_strikethrough',
                'attributes' => [
                    'data-icon' => 'material-icons format_strikethrough',
                    'data-icon-value' => 'format_strikethrough',
                ],
            ],
            [
                'value' => 'material-icons format_textdirection_l_to_r',
                'label' => 'format_textdirection_l_to_r',
                'attributes' => [
                    'data-icon' => 'material-icons format_textdirection_l_to_r',
                    'data-icon-value' => 'format_textdirection_l_to_r',
                ],
            ],
            [
                'value' => 'material-icons format_textdirection_r_to_l',
                'label' => 'format_textdirection_r_to_l',
                'attributes' => [
                    'data-icon' => 'material-icons format_textdirection_r_to_l',
                    'data-icon-value' => 'format_textdirection_r_to_l',
                ],
            ],
            [
                'value' => 'material-icons format_underline',
                'label' => 'format_underline',
                'attributes' => [
                    'data-icon' => 'material-icons format_underline',
                    'data-icon-value' => 'format_underline',
                ],
            ],
            [
                'value' => 'material-icons format_underlined',
                'label' => 'format_underlined',
                'attributes' => [
                    'data-icon' => 'material-icons format_underlined',
                    'data-icon-value' => 'format_underlined',
                ],
            ],
            [
                'value' => 'material-icons fort',
                'label' => 'fort',
                'attributes' => [
                    'data-icon' => 'material-icons fort',
                    'data-icon-value' => 'fort',
                ],
            ],
            [
                'value' => 'material-icons forum',
                'label' => 'forum',
                'attributes' => [
                    'data-icon' => 'material-icons forum',
                    'data-icon-value' => 'forum',
                ],
            ],
            [
                'value' => 'material-icons forward',
                'label' => 'forward',
                'attributes' => [
                    'data-icon' => 'material-icons forward',
                    'data-icon-value' => 'forward',
                ],
            ],
            [
                'value' => 'material-icons forward_10',
                'label' => 'forward_10',
                'attributes' => [
                    'data-icon' => 'material-icons forward_10',
                    'data-icon-value' => 'forward_10',
                ],
            ],
            [
                'value' => 'material-icons forward_30',
                'label' => 'forward_30',
                'attributes' => [
                    'data-icon' => 'material-icons forward_30',
                    'data-icon-value' => 'forward_30',
                ],
            ],
            [
                'value' => 'material-icons forward_5',
                'label' => 'forward_5',
                'attributes' => [
                    'data-icon' => 'material-icons forward_5',
                    'data-icon-value' => 'forward_5',
                ],
            ],
            [
                'value' => 'material-icons forward_to_inbox',
                'label' => 'forward_to_inbox',
                'attributes' => [
                    'data-icon' => 'material-icons forward_to_inbox',
                    'data-icon-value' => 'forward_to_inbox',
                ],
            ],
            [
                'value' => 'material-icons foundation',
                'label' => 'foundation',
                'attributes' => [
                    'data-icon' => 'material-icons foundation',
                    'data-icon-value' => 'foundation',
                ],
            ],
            [
                'value' => 'material-icons free_breakfast',
                'label' => 'free_breakfast',
                'attributes' => [
                    'data-icon' => 'material-icons free_breakfast',
                    'data-icon-value' => 'free_breakfast',
                ],
            ],
            [
                'value' => 'material-icons free_cancellation',
                'label' => 'free_cancellation',
                'attributes' => [
                    'data-icon' => 'material-icons free_cancellation',
                    'data-icon-value' => 'free_cancellation',
                ],
            ],
            [
                'value' => 'material-icons front_hand',
                'label' => 'front_hand',
                'attributes' => [
                    'data-icon' => 'material-icons front_hand',
                    'data-icon-value' => 'front_hand',
                ],
            ],
            [
                'value' => 'material-icons fullscreen',
                'label' => 'fullscreen',
                'attributes' => [
                    'data-icon' => 'material-icons fullscreen',
                    'data-icon-value' => 'fullscreen',
                ],
            ],
            [
                'value' => 'material-icons fullscreen_exit',
                'label' => 'fullscreen_exit',
                'attributes' => [
                    'data-icon' => 'material-icons fullscreen_exit',
                    'data-icon-value' => 'fullscreen_exit',
                ],
            ],
            [
                'value' => 'material-icons functions',
                'label' => 'functions',
                'attributes' => [
                    'data-icon' => 'material-icons functions',
                    'data-icon-value' => 'functions',
                ],
            ],
            [
                'value' => 'material-icons g_mobiledata',
                'label' => 'g_mobiledata',
                'attributes' => [
                    'data-icon' => 'material-icons g_mobiledata',
                    'data-icon-value' => 'g_mobiledata',
                ],
            ],
            [
                'value' => 'material-icons g_translate',
                'label' => 'g_translate',
                'attributes' => [
                    'data-icon' => 'material-icons g_translate',
                    'data-icon-value' => 'g_translate',
                ],
            ],
            [
                'value' => 'material-icons gamepad',
                'label' => 'gamepad',
                'attributes' => [
                    'data-icon' => 'material-icons gamepad',
                    'data-icon-value' => 'gamepad',
                ],
            ],
            [
                'value' => 'material-icons games',
                'label' => 'games',
                'attributes' => [
                    'data-icon' => 'material-icons games',
                    'data-icon-value' => 'games',
                ],
            ],
            [
                'value' => 'material-icons garage',
                'label' => 'garage',
                'attributes' => [
                    'data-icon' => 'material-icons garage',
                    'data-icon-value' => 'garage',
                ],
            ],
            [
                'value' => 'material-icons gas_meter',
                'label' => 'gas_meter',
                'attributes' => [
                    'data-icon' => 'material-icons gas_meter',
                    'data-icon-value' => 'gas_meter',
                ],
            ],
            [
                'value' => 'material-icons gavel',
                'label' => 'gavel',
                'attributes' => [
                    'data-icon' => 'material-icons gavel',
                    'data-icon-value' => 'gavel',
                ],
            ],
            [
                'value' => 'material-icons generating_tokens',
                'label' => 'generating_tokens',
                'attributes' => [
                    'data-icon' => 'material-icons generating_tokens',
                    'data-icon-value' => 'generating_tokens',
                ],
            ],
            [
                'value' => 'material-icons gesture',
                'label' => 'gesture',
                'attributes' => [
                    'data-icon' => 'material-icons gesture',
                    'data-icon-value' => 'gesture',
                ],
            ],
            [
                'value' => 'material-icons get_app',
                'label' => 'get_app',
                'attributes' => [
                    'data-icon' => 'material-icons get_app',
                    'data-icon-value' => 'get_app',
                ],
            ],
            [
                'value' => 'material-icons gif',
                'label' => 'gif',
                'attributes' => [
                    'data-icon' => 'material-icons gif',
                    'data-icon-value' => 'gif',
                ],
            ],
            [
                'value' => 'material-icons gif_box',
                'label' => 'gif_box',
                'attributes' => [
                    'data-icon' => 'material-icons gif_box',
                    'data-icon-value' => 'gif_box',
                ],
            ],
            [
                'value' => 'material-icons girl',
                'label' => 'girl',
                'attributes' => [
                    'data-icon' => 'material-icons girl',
                    'data-icon-value' => 'girl',
                ],
            ],
            [
                'value' => 'material-icons gite',
                'label' => 'gite',
                'attributes' => [
                    'data-icon' => 'material-icons gite',
                    'data-icon-value' => 'gite',
                ],
            ],
            [
                'value' => 'material-icons goat',
                'label' => 'goat',
                'attributes' => [
                    'data-icon' => 'material-icons goat',
                    'data-icon-value' => 'goat',
                ],
            ],
            [
                'value' => 'material-icons golf_course',
                'label' => 'golf_course',
                'attributes' => [
                    'data-icon' => 'material-icons golf_course',
                    'data-icon-value' => 'golf_course',
                ],
            ],
            [
                'value' => 'material-icons gpp_bad',
                'label' => 'gpp_bad',
                'attributes' => [
                    'data-icon' => 'material-icons gpp_bad',
                    'data-icon-value' => 'gpp_bad',
                ],
            ],
            [
                'value' => 'material-icons gpp_good',
                'label' => 'gpp_good',
                'attributes' => [
                    'data-icon' => 'material-icons gpp_good',
                    'data-icon-value' => 'gpp_good',
                ],
            ],
            [
                'value' => 'material-icons gpp_maybe',
                'label' => 'gpp_maybe',
                'attributes' => [
                    'data-icon' => 'material-icons gpp_maybe',
                    'data-icon-value' => 'gpp_maybe',
                ],
            ],
            [
                'value' => 'material-icons gps_fixed',
                'label' => 'gps_fixed',
                'attributes' => [
                    'data-icon' => 'material-icons gps_fixed',
                    'data-icon-value' => 'gps_fixed',
                ],
            ],
            [
                'value' => 'material-icons gps_not_fixed',
                'label' => 'gps_not_fixed',
                'attributes' => [
                    'data-icon' => 'material-icons gps_not_fixed',
                    'data-icon-value' => 'gps_not_fixed',
                ],
            ],
            [
                'value' => 'material-icons gps_off',
                'label' => 'gps_off',
                'attributes' => [
                    'data-icon' => 'material-icons gps_off',
                    'data-icon-value' => 'gps_off',
                ],
            ],
            [
                'value' => 'material-icons grade',
                'label' => 'grade',
                'attributes' => [
                    'data-icon' => 'material-icons grade',
                    'data-icon-value' => 'grade',
                ],
            ],
            [
                'value' => 'material-icons gradient',
                'label' => 'gradient',
                'attributes' => [
                    'data-icon' => 'material-icons gradient',
                    'data-icon-value' => 'gradient',
                ],
            ],
            [
                'value' => 'material-icons grading',
                'label' => 'grading',
                'attributes' => [
                    'data-icon' => 'material-icons grading',
                    'data-icon-value' => 'grading',
                ],
            ],
            [
                'value' => 'material-icons grain',
                'label' => 'grain',
                'attributes' => [
                    'data-icon' => 'material-icons grain',
                    'data-icon-value' => 'grain',
                ],
            ],
            [
                'value' => 'material-icons graphic_eq',
                'label' => 'graphic_eq',
                'attributes' => [
                    'data-icon' => 'material-icons graphic_eq',
                    'data-icon-value' => 'graphic_eq',
                ],
            ],
            [
                'value' => 'material-icons grass',
                'label' => 'grass',
                'attributes' => [
                    'data-icon' => 'material-icons grass',
                    'data-icon-value' => 'grass',
                ],
            ],
            [
                'value' => 'material-icons grid_3x3',
                'label' => 'grid_3x3',
                'attributes' => [
                    'data-icon' => 'material-icons grid_3x3',
                    'data-icon-value' => 'grid_3x3',
                ],
            ],
            [
                'value' => 'material-icons grid_4x4',
                'label' => 'grid_4x4',
                'attributes' => [
                    'data-icon' => 'material-icons grid_4x4',
                    'data-icon-value' => 'grid_4x4',
                ],
            ],
            [
                'value' => 'material-icons grid_goldenratio',
                'label' => 'grid_goldenratio',
                'attributes' => [
                    'data-icon' => 'material-icons grid_goldenratio',
                    'data-icon-value' => 'grid_goldenratio',
                ],
            ],
            [
                'value' => 'material-icons grid_off',
                'label' => 'grid_off',
                'attributes' => [
                    'data-icon' => 'material-icons grid_off',
                    'data-icon-value' => 'grid_off',
                ],
            ],
            [
                'value' => 'material-icons grid_on',
                'label' => 'grid_on',
                'attributes' => [
                    'data-icon' => 'material-icons grid_on',
                    'data-icon-value' => 'grid_on',
                ],
            ],
            [
                'value' => 'material-icons grid_view',
                'label' => 'grid_view',
                'attributes' => [
                    'data-icon' => 'material-icons grid_view',
                    'data-icon-value' => 'grid_view',
                ],
            ],
            [
                'value' => 'material-icons group',
                'label' => 'group',
                'attributes' => [
                    'data-icon' => 'material-icons group',
                    'data-icon-value' => 'group',
                ],
            ],
            [
                'value' => 'material-icons group_add',
                'label' => 'group_add',
                'attributes' => [
                    'data-icon' => 'material-icons group_add',
                    'data-icon-value' => 'group_add',
                ],
            ],
            [
                'value' => 'material-icons group_off',
                'label' => 'group_off',
                'attributes' => [
                    'data-icon' => 'material-icons group_off',
                    'data-icon-value' => 'group_off',
                ],
            ],
            [
                'value' => 'material-icons group_remove',
                'label' => 'group_remove',
                'attributes' => [
                    'data-icon' => 'material-icons group_remove',
                    'data-icon-value' => 'group_remove',
                ],
            ],
            [
                'value' => 'material-icons group_work',
                'label' => 'group_work',
                'attributes' => [
                    'data-icon' => 'material-icons group_work',
                    'data-icon-value' => 'group_work',
                ],
            ],
            [
                'value' => 'material-icons groups',
                'label' => 'groups',
                'attributes' => [
                    'data-icon' => 'material-icons groups',
                    'data-icon-value' => 'groups',
                ],
            ],
            [
                'value' => 'material-icons h_mobiledata',
                'label' => 'h_mobiledata',
                'attributes' => [
                    'data-icon' => 'material-icons h_mobiledata',
                    'data-icon-value' => 'h_mobiledata',
                ],
            ],
            [
                'value' => 'material-icons h_plus_mobiledata',
                'label' => 'h_plus_mobiledata',
                'attributes' => [
                    'data-icon' => 'material-icons h_plus_mobiledata',
                    'data-icon-value' => 'h_plus_mobiledata',
                ],
            ],
            [
                'value' => 'material-icons hail',
                'label' => 'hail',
                'attributes' => [
                    'data-icon' => 'material-icons hail',
                    'data-icon-value' => 'hail',
                ],
            ],
            [
                'value' => 'material-icons handshake',
                'label' => 'handshake',
                'attributes' => [
                    'data-icon' => 'material-icons handshake',
                    'data-icon-value' => 'handshake',
                ],
            ],
            [
                'value' => 'material-icons handyman',
                'label' => 'handyman',
                'attributes' => [
                    'data-icon' => 'material-icons handyman',
                    'data-icon-value' => 'handyman',
                ],
            ],
            [
                'value' => 'material-icons hardware',
                'label' => 'hardware',
                'attributes' => [
                    'data-icon' => 'material-icons hardware',
                    'data-icon-value' => 'hardware',
                ],
            ],
            [
                'value' => 'material-icons hd',
                'label' => 'hd',
                'attributes' => [
                    'data-icon' => 'material-icons hd',
                    'data-icon-value' => 'hd',
                ],
            ],
            [
                'value' => 'material-icons hdr_auto',
                'label' => 'hdr_auto',
                'attributes' => [
                    'data-icon' => 'material-icons hdr_auto',
                    'data-icon-value' => 'hdr_auto',
                ],
            ],
            [
                'value' => 'material-icons hdr_auto_select',
                'label' => 'hdr_auto_select',
                'attributes' => [
                    'data-icon' => 'material-icons hdr_auto_select',
                    'data-icon-value' => 'hdr_auto_select',
                ],
            ],
            [
                'value' => 'material-icons hdr_enhanced_select',
                'label' => 'hdr_enhanced_select',
                'attributes' => [
                    'data-icon' => 'material-icons hdr_enhanced_select',
                    'data-icon-value' => 'hdr_enhanced_select',
                ],
            ],
            [
                'value' => 'material-icons hdr_off',
                'label' => 'hdr_off',
                'attributes' => [
                    'data-icon' => 'material-icons hdr_off',
                    'data-icon-value' => 'hdr_off',
                ],
            ],
            [
                'value' => 'material-icons hdr_off_select',
                'label' => 'hdr_off_select',
                'attributes' => [
                    'data-icon' => 'material-icons hdr_off_select',
                    'data-icon-value' => 'hdr_off_select',
                ],
            ],
            [
                'value' => 'material-icons hdr_on',
                'label' => 'hdr_on',
                'attributes' => [
                    'data-icon' => 'material-icons hdr_on',
                    'data-icon-value' => 'hdr_on',
                ],
            ],
            [
                'value' => 'material-icons hdr_on_select',
                'label' => 'hdr_on_select',
                'attributes' => [
                    'data-icon' => 'material-icons hdr_on_select',
                    'data-icon-value' => 'hdr_on_select',
                ],
            ],
            [
                'value' => 'material-icons hdr_plus',
                'label' => 'hdr_plus',
                'attributes' => [
                    'data-icon' => 'material-icons hdr_plus',
                    'data-icon-value' => 'hdr_plus',
                ],
            ],
            [
                'value' => 'material-icons hdr_strong',
                'label' => 'hdr_strong',
                'attributes' => [
                    'data-icon' => 'material-icons hdr_strong',
                    'data-icon-value' => 'hdr_strong',
                ],
            ],
            [
                'value' => 'material-icons hdr_weak',
                'label' => 'hdr_weak',
                'attributes' => [
                    'data-icon' => 'material-icons hdr_weak',
                    'data-icon-value' => 'hdr_weak',
                ],
            ],
            [
                'value' => 'material-icons headphones',
                'label' => 'headphones',
                'attributes' => [
                    'data-icon' => 'material-icons headphones',
                    'data-icon-value' => 'headphones',
                ],
            ],
            [
                'value' => 'material-icons headphones_battery',
                'label' => 'headphones_battery',
                'attributes' => [
                    'data-icon' => 'material-icons headphones_battery',
                    'data-icon-value' => 'headphones_battery',
                ],
            ],
            [
                'value' => 'material-icons headset',
                'label' => 'headset',
                'attributes' => [
                    'data-icon' => 'material-icons headset',
                    'data-icon-value' => 'headset',
                ],
            ],
            [
                'value' => 'material-icons headset_mic',
                'label' => 'headset_mic',
                'attributes' => [
                    'data-icon' => 'material-icons headset_mic',
                    'data-icon-value' => 'headset_mic',
                ],
            ],
            [
                'value' => 'material-icons headset_off',
                'label' => 'headset_off',
                'attributes' => [
                    'data-icon' => 'material-icons headset_off',
                    'data-icon-value' => 'headset_off',
                ],
            ],
            [
                'value' => 'material-icons healing',
                'label' => 'healing',
                'attributes' => [
                    'data-icon' => 'material-icons healing',
                    'data-icon-value' => 'healing',
                ],
            ],
            [
                'value' => 'material-icons health_and_safety',
                'label' => 'health_and_safety',
                'attributes' => [
                    'data-icon' => 'material-icons health_and_safety',
                    'data-icon-value' => 'health_and_safety',
                ],
            ],
            [
                'value' => 'material-icons hearing',
                'label' => 'hearing',
                'attributes' => [
                    'data-icon' => 'material-icons hearing',
                    'data-icon-value' => 'hearing',
                ],
            ],
            [
                'value' => 'material-icons hearing_disabled',
                'label' => 'hearing_disabled',
                'attributes' => [
                    'data-icon' => 'material-icons hearing_disabled',
                    'data-icon-value' => 'hearing_disabled',
                ],
            ],
            [
                'value' => 'material-icons heart_broken',
                'label' => 'heart_broken',
                'attributes' => [
                    'data-icon' => 'material-icons heart_broken',
                    'data-icon-value' => 'heart_broken',
                ],
            ],
            [
                'value' => 'material-icons heat_pump',
                'label' => 'heat_pump',
                'attributes' => [
                    'data-icon' => 'material-icons heat_pump',
                    'data-icon-value' => 'heat_pump',
                ],
            ],
            [
                'value' => 'material-icons height',
                'label' => 'height',
                'attributes' => [
                    'data-icon' => 'material-icons height',
                    'data-icon-value' => 'height',
                ],
            ],
            [
                'value' => 'material-icons help',
                'label' => 'help',
                'attributes' => [
                    'data-icon' => 'material-icons help',
                    'data-icon-value' => 'help',
                ],
            ],
            [
                'value' => 'material-icons help_center',
                'label' => 'help_center',
                'attributes' => [
                    'data-icon' => 'material-icons help_center',
                    'data-icon-value' => 'help_center',
                ],
            ],
            [
                'value' => 'material-icons help_outline',
                'label' => 'help_outline',
                'attributes' => [
                    'data-icon' => 'material-icons help_outline',
                    'data-icon-value' => 'help_outline',
                ],
            ],
            [
                'value' => 'material-icons hevc',
                'label' => 'hevc',
                'attributes' => [
                    'data-icon' => 'material-icons hevc',
                    'data-icon-value' => 'hevc',
                ],
            ],
            [
                'value' => 'material-icons hexagon',
                'label' => 'hexagon',
                'attributes' => [
                    'data-icon' => 'material-icons hexagon',
                    'data-icon-value' => 'hexagon',
                ],
            ],
            [
                'value' => 'material-icons hide_image',
                'label' => 'hide_image',
                'attributes' => [
                    'data-icon' => 'material-icons hide_image',
                    'data-icon-value' => 'hide_image',
                ],
            ],
            [
                'value' => 'material-icons hide_source',
                'label' => 'hide_source',
                'attributes' => [
                    'data-icon' => 'material-icons hide_source',
                    'data-icon-value' => 'hide_source',
                ],
            ],
            [
                'value' => 'material-icons high_quality',
                'label' => 'high_quality',
                'attributes' => [
                    'data-icon' => 'material-icons high_quality',
                    'data-icon-value' => 'high_quality',
                ],
            ],
            [
                'value' => 'material-icons highlight',
                'label' => 'highlight',
                'attributes' => [
                    'data-icon' => 'material-icons highlight',
                    'data-icon-value' => 'highlight',
                ],
            ],
            [
                'value' => 'material-icons highlight_alt',
                'label' => 'highlight_alt',
                'attributes' => [
                    'data-icon' => 'material-icons highlight_alt',
                    'data-icon-value' => 'highlight_alt',
                ],
            ],
            [
                'value' => 'material-icons highlight_off',
                'label' => 'highlight_off',
                'attributes' => [
                    'data-icon' => 'material-icons highlight_off',
                    'data-icon-value' => 'highlight_off',
                ],
            ],
            [
                'value' => 'material-icons highlight_remove',
                'label' => 'highlight_remove',
                'attributes' => [
                    'data-icon' => 'material-icons highlight_remove',
                    'data-icon-value' => 'highlight_remove',
                ],
            ],
            [
                'value' => 'material-icons hiking',
                'label' => 'hiking',
                'attributes' => [
                    'data-icon' => 'material-icons hiking',
                    'data-icon-value' => 'hiking',
                ],
            ],
            [
                'value' => 'material-icons history',
                'label' => 'history',
                'attributes' => [
                    'data-icon' => 'material-icons history',
                    'data-icon-value' => 'history',
                ],
            ],
            [
                'value' => 'material-icons history_edu',
                'label' => 'history_edu',
                'attributes' => [
                    'data-icon' => 'material-icons history_edu',
                    'data-icon-value' => 'history_edu',
                ],
            ],
            [
                'value' => 'material-icons history_toggle_off',
                'label' => 'history_toggle_off',
                'attributes' => [
                    'data-icon' => 'material-icons history_toggle_off',
                    'data-icon-value' => 'history_toggle_off',
                ],
            ],
            [
                'value' => 'material-icons hive',
                'label' => 'hive',
                'attributes' => [
                    'data-icon' => 'material-icons hive',
                    'data-icon-value' => 'hive',
                ],
            ],
            [
                'value' => 'material-icons hls',
                'label' => 'hls',
                'attributes' => [
                    'data-icon' => 'material-icons hls',
                    'data-icon-value' => 'hls',
                ],
            ],
            [
                'value' => 'material-icons hls_off',
                'label' => 'hls_off',
                'attributes' => [
                    'data-icon' => 'material-icons hls_off',
                    'data-icon-value' => 'hls_off',
                ],
            ],
            [
                'value' => 'material-icons holiday_village',
                'label' => 'holiday_village',
                'attributes' => [
                    'data-icon' => 'material-icons holiday_village',
                    'data-icon-value' => 'holiday_village',
                ],
            ],
            [
                'value' => 'material-icons home',
                'label' => 'home',
                'attributes' => [
                    'data-icon' => 'material-icons home',
                    'data-icon-value' => 'home',
                ],
            ],
            [
                'value' => 'material-icons home_filled',
                'label' => 'home_filled',
                'attributes' => [
                    'data-icon' => 'material-icons home_filled',
                    'data-icon-value' => 'home_filled',
                ],
            ],
            [
                'value' => 'material-icons home_max',
                'label' => 'home_max',
                'attributes' => [
                    'data-icon' => 'material-icons home_max',
                    'data-icon-value' => 'home_max',
                ],
            ],
            [
                'value' => 'material-icons home_mini',
                'label' => 'home_mini',
                'attributes' => [
                    'data-icon' => 'material-icons home_mini',
                    'data-icon-value' => 'home_mini',
                ],
            ],
            [
                'value' => 'material-icons home_repair_service',
                'label' => 'home_repair_service',
                'attributes' => [
                    'data-icon' => 'material-icons home_repair_service',
                    'data-icon-value' => 'home_repair_service',
                ],
            ],
            [
                'value' => 'material-icons home_work',
                'label' => 'home_work',
                'attributes' => [
                    'data-icon' => 'material-icons home_work',
                    'data-icon-value' => 'home_work',
                ],
            ],
            [
                'value' => 'material-icons horizontal_distribute',
                'label' => 'horizontal_distribute',
                'attributes' => [
                    'data-icon' => 'material-icons horizontal_distribute',
                    'data-icon-value' => 'horizontal_distribute',
                ],
            ],
            [
                'value' => 'material-icons horizontal_rule',
                'label' => 'horizontal_rule',
                'attributes' => [
                    'data-icon' => 'material-icons horizontal_rule',
                    'data-icon-value' => 'horizontal_rule',
                ],
            ],
            [
                'value' => 'material-icons horizontal_split',
                'label' => 'horizontal_split',
                'attributes' => [
                    'data-icon' => 'material-icons horizontal_split',
                    'data-icon-value' => 'horizontal_split',
                ],
            ],
            [
                'value' => 'material-icons hot_tub',
                'label' => 'hot_tub',
                'attributes' => [
                    'data-icon' => 'material-icons hot_tub',
                    'data-icon-value' => 'hot_tub',
                ],
            ],
            [
                'value' => 'material-icons hotel',
                'label' => 'hotel',
                'attributes' => [
                    'data-icon' => 'material-icons hotel',
                    'data-icon-value' => 'hotel',
                ],
            ],
            [
                'value' => 'material-icons hotel_class',
                'label' => 'hotel_class',
                'attributes' => [
                    'data-icon' => 'material-icons hotel_class',
                    'data-icon-value' => 'hotel_class',
                ],
            ],
            [
                'value' => 'material-icons hourglass_bottom',
                'label' => 'hourglass_bottom',
                'attributes' => [
                    'data-icon' => 'material-icons hourglass_bottom',
                    'data-icon-value' => 'hourglass_bottom',
                ],
            ],
            [
                'value' => 'material-icons hourglass_disabled',
                'label' => 'hourglass_disabled',
                'attributes' => [
                    'data-icon' => 'material-icons hourglass_disabled',
                    'data-icon-value' => 'hourglass_disabled',
                ],
            ],
            [
                'value' => 'material-icons hourglass_empty',
                'label' => 'hourglass_empty',
                'attributes' => [
                    'data-icon' => 'material-icons hourglass_empty',
                    'data-icon-value' => 'hourglass_empty',
                ],
            ],
            [
                'value' => 'material-icons hourglass_full',
                'label' => 'hourglass_full',
                'attributes' => [
                    'data-icon' => 'material-icons hourglass_full',
                    'data-icon-value' => 'hourglass_full',
                ],
            ],
            [
                'value' => 'material-icons hourglass_top',
                'label' => 'hourglass_top',
                'attributes' => [
                    'data-icon' => 'material-icons hourglass_top',
                    'data-icon-value' => 'hourglass_top',
                ],
            ],
            [
                'value' => 'material-icons house',
                'label' => 'house',
                'attributes' => [
                    'data-icon' => 'material-icons house',
                    'data-icon-value' => 'house',
                ],
            ],
            [
                'value' => 'material-icons house_siding',
                'label' => 'house_siding',
                'attributes' => [
                    'data-icon' => 'material-icons house_siding',
                    'data-icon-value' => 'house_siding',
                ],
            ],
            [
                'value' => 'material-icons houseboat',
                'label' => 'houseboat',
                'attributes' => [
                    'data-icon' => 'material-icons houseboat',
                    'data-icon-value' => 'houseboat',
                ],
            ],
            [
                'value' => 'material-icons how_to_reg',
                'label' => 'how_to_reg',
                'attributes' => [
                    'data-icon' => 'material-icons how_to_reg',
                    'data-icon-value' => 'how_to_reg',
                ],
            ],
            [
                'value' => 'material-icons how_to_vote',
                'label' => 'how_to_vote',
                'attributes' => [
                    'data-icon' => 'material-icons how_to_vote',
                    'data-icon-value' => 'how_to_vote',
                ],
            ],
            [
                'value' => 'material-icons html',
                'label' => 'html',
                'attributes' => [
                    'data-icon' => 'material-icons html',
                    'data-icon-value' => 'html',
                ],
            ],
            [
                'value' => 'material-icons http',
                'label' => 'http',
                'attributes' => [
                    'data-icon' => 'material-icons http',
                    'data-icon-value' => 'http',
                ],
            ],
            [
                'value' => 'material-icons https',
                'label' => 'https',
                'attributes' => [
                    'data-icon' => 'material-icons https',
                    'data-icon-value' => 'https',
                ],
            ],
            [
                'value' => 'material-icons hub',
                'label' => 'hub',
                'attributes' => [
                    'data-icon' => 'material-icons hub',
                    'data-icon-value' => 'hub',
                ],
            ],
            [
                'value' => 'material-icons hvac',
                'label' => 'hvac',
                'attributes' => [
                    'data-icon' => 'material-icons hvac',
                    'data-icon-value' => 'hvac',
                ],
            ],
            [
                'value' => 'material-icons ice_skating',
                'label' => 'ice_skating',
                'attributes' => [
                    'data-icon' => 'material-icons ice_skating',
                    'data-icon-value' => 'ice_skating',
                ],
            ],
            [
                'value' => 'material-icons icecream',
                'label' => 'icecream',
                'attributes' => [
                    'data-icon' => 'material-icons icecream',
                    'data-icon-value' => 'icecream',
                ],
            ],
            [
                'value' => 'material-icons image',
                'label' => 'image',
                'attributes' => [
                    'data-icon' => 'material-icons image',
                    'data-icon-value' => 'image',
                ],
            ],
            [
                'value' => 'material-icons image_aspect_ratio',
                'label' => 'image_aspect_ratio',
                'attributes' => [
                    'data-icon' => 'material-icons image_aspect_ratio',
                    'data-icon-value' => 'image_aspect_ratio',
                ],
            ],
            [
                'value' => 'material-icons image_not_supported',
                'label' => 'image_not_supported',
                'attributes' => [
                    'data-icon' => 'material-icons image_not_supported',
                    'data-icon-value' => 'image_not_supported',
                ],
            ],
            [
                'value' => 'material-icons image_search',
                'label' => 'image_search',
                'attributes' => [
                    'data-icon' => 'material-icons image_search',
                    'data-icon-value' => 'image_search',
                ],
            ],
            [
                'value' => 'material-icons imagesearch_roller',
                'label' => 'imagesearch_roller',
                'attributes' => [
                    'data-icon' => 'material-icons imagesearch_roller',
                    'data-icon-value' => 'imagesearch_roller',
                ],
            ],
            [
                'value' => 'material-icons import_contacts',
                'label' => 'import_contacts',
                'attributes' => [
                    'data-icon' => 'material-icons import_contacts',
                    'data-icon-value' => 'import_contacts',
                ],
            ],
            [
                'value' => 'material-icons import_export',
                'label' => 'import_export',
                'attributes' => [
                    'data-icon' => 'material-icons import_export',
                    'data-icon-value' => 'import_export',
                ],
            ],
            [
                'value' => 'material-icons important_devices',
                'label' => 'important_devices',
                'attributes' => [
                    'data-icon' => 'material-icons important_devices',
                    'data-icon-value' => 'important_devices',
                ],
            ],
            [
                'value' => 'material-icons inbox',
                'label' => 'inbox',
                'attributes' => [
                    'data-icon' => 'material-icons inbox',
                    'data-icon-value' => 'inbox',
                ],
            ],
            [
                'value' => 'material-icons incomplete_circle',
                'label' => 'incomplete_circle',
                'attributes' => [
                    'data-icon' => 'material-icons incomplete_circle',
                    'data-icon-value' => 'incomplete_circle',
                ],
            ],
            [
                'value' => 'material-icons indeterminate_check_box',
                'label' => 'indeterminate_check_box',
                'attributes' => [
                    'data-icon' => 'material-icons indeterminate_check_box',
                    'data-icon-value' => 'indeterminate_check_box',
                ],
            ],
            [
                'value' => 'material-icons info',
                'label' => 'info',
                'attributes' => [
                    'data-icon' => 'material-icons info',
                    'data-icon-value' => 'info',
                ],
            ],
            [
                'value' => 'material-icons info_outline',
                'label' => 'info_outline',
                'attributes' => [
                    'data-icon' => 'material-icons info_outline',
                    'data-icon-value' => 'info_outline',
                ],
            ],
            [
                'value' => 'material-icons input',
                'label' => 'input',
                'attributes' => [
                    'data-icon' => 'material-icons input',
                    'data-icon-value' => 'input',
                ],
            ],
            [
                'value' => 'material-icons insert_chart',
                'label' => 'insert_chart',
                'attributes' => [
                    'data-icon' => 'material-icons insert_chart',
                    'data-icon-value' => 'insert_chart',
                ],
            ],
            [
                'value' => 'material-icons insert_chart_outlined',
                'label' => 'insert_chart_outlined',
                'attributes' => [
                    'data-icon' => 'material-icons insert_chart_outlined',
                    'data-icon-value' => 'insert_chart_outlined',
                ],
            ],
            [
                'value' => 'material-icons insert_comment',
                'label' => 'insert_comment',
                'attributes' => [
                    'data-icon' => 'material-icons insert_comment',
                    'data-icon-value' => 'insert_comment',
                ],
            ],
            [
                'value' => 'material-icons insert_drive_file',
                'label' => 'insert_drive_file',
                'attributes' => [
                    'data-icon' => 'material-icons insert_drive_file',
                    'data-icon-value' => 'insert_drive_file',
                ],
            ],
            [
                'value' => 'material-icons insert_emoticon',
                'label' => 'insert_emoticon',
                'attributes' => [
                    'data-icon' => 'material-icons insert_emoticon',
                    'data-icon-value' => 'insert_emoticon',
                ],
            ],
            [
                'value' => 'material-icons insert_invitation',
                'label' => 'insert_invitation',
                'attributes' => [
                    'data-icon' => 'material-icons insert_invitation',
                    'data-icon-value' => 'insert_invitation',
                ],
            ],
            [
                'value' => 'material-icons insert_link',
                'label' => 'insert_link',
                'attributes' => [
                    'data-icon' => 'material-icons insert_link',
                    'data-icon-value' => 'insert_link',
                ],
            ],
            [
                'value' => 'material-icons insert_page_break',
                'label' => 'insert_page_break',
                'attributes' => [
                    'data-icon' => 'material-icons insert_page_break',
                    'data-icon-value' => 'insert_page_break',
                ],
            ],
            [
                'value' => 'material-icons insert_photo',
                'label' => 'insert_photo',
                'attributes' => [
                    'data-icon' => 'material-icons insert_photo',
                    'data-icon-value' => 'insert_photo',
                ],
            ],
            [
                'value' => 'material-icons insights',
                'label' => 'insights',
                'attributes' => [
                    'data-icon' => 'material-icons insights',
                    'data-icon-value' => 'insights',
                ],
            ],
            [
                'value' => 'material-icons install_desktop',
                'label' => 'install_desktop',
                'attributes' => [
                    'data-icon' => 'material-icons install_desktop',
                    'data-icon-value' => 'install_desktop',
                ],
            ],
            [
                'value' => 'material-icons install_mobile',
                'label' => 'install_mobile',
                'attributes' => [
                    'data-icon' => 'material-icons install_mobile',
                    'data-icon-value' => 'install_mobile',
                ],
            ],
            [
                'value' => 'material-icons integration_instructions',
                'label' => 'integration_instructions',
                'attributes' => [
                    'data-icon' => 'material-icons integration_instructions',
                    'data-icon-value' => 'integration_instructions',
                ],
            ],
            [
                'value' => 'material-icons interests',
                'label' => 'interests',
                'attributes' => [
                    'data-icon' => 'material-icons interests',
                    'data-icon-value' => 'interests',
                ],
            ],
            [
                'value' => 'material-icons interpreter_mode',
                'label' => 'interpreter_mode',
                'attributes' => [
                    'data-icon' => 'material-icons interpreter_mode',
                    'data-icon-value' => 'interpreter_mode',
                ],
            ],
            [
                'value' => 'material-icons inventory',
                'label' => 'inventory',
                'attributes' => [
                    'data-icon' => 'material-icons inventory',
                    'data-icon-value' => 'inventory',
                ],
            ],
            [
                'value' => 'material-icons inventory_2',
                'label' => 'inventory_2',
                'attributes' => [
                    'data-icon' => 'material-icons inventory_2',
                    'data-icon-value' => 'inventory_2',
                ],
            ],
            [
                'value' => 'material-icons invert_colors',
                'label' => 'invert_colors',
                'attributes' => [
                    'data-icon' => 'material-icons invert_colors',
                    'data-icon-value' => 'invert_colors',
                ],
            ],
            [
                'value' => 'material-icons invert_colors_off',
                'label' => 'invert_colors_off',
                'attributes' => [
                    'data-icon' => 'material-icons invert_colors_off',
                    'data-icon-value' => 'invert_colors_off',
                ],
            ],
            [
                'value' => 'material-icons invert_colors_on',
                'label' => 'invert_colors_on',
                'attributes' => [
                    'data-icon' => 'material-icons invert_colors_on',
                    'data-icon-value' => 'invert_colors_on',
                ],
            ],
            [
                'value' => 'material-icons ios_share',
                'label' => 'ios_share',
                'attributes' => [
                    'data-icon' => 'material-icons ios_share',
                    'data-icon-value' => 'ios_share',
                ],
            ],
            [
                'value' => 'material-icons iron',
                'label' => 'iron',
                'attributes' => [
                    'data-icon' => 'material-icons iron',
                    'data-icon-value' => 'iron',
                ],
            ],
            [
                'value' => 'material-icons iso',
                'label' => 'iso',
                'attributes' => [
                    'data-icon' => 'material-icons iso',
                    'data-icon-value' => 'iso',
                ],
            ],
            [
                'value' => 'material-icons javascript',
                'label' => 'javascript',
                'attributes' => [
                    'data-icon' => 'material-icons javascript',
                    'data-icon-value' => 'javascript',
                ],
            ],
            [
                'value' => 'material-icons join_full',
                'label' => 'join_full',
                'attributes' => [
                    'data-icon' => 'material-icons join_full',
                    'data-icon-value' => 'join_full',
                ],
            ],
            [
                'value' => 'material-icons join_inner',
                'label' => 'join_inner',
                'attributes' => [
                    'data-icon' => 'material-icons join_inner',
                    'data-icon-value' => 'join_inner',
                ],
            ],
            [
                'value' => 'material-icons join_left',
                'label' => 'join_left',
                'attributes' => [
                    'data-icon' => 'material-icons join_left',
                    'data-icon-value' => 'join_left',
                ],
            ],
            [
                'value' => 'material-icons join_right',
                'label' => 'join_right',
                'attributes' => [
                    'data-icon' => 'material-icons join_right',
                    'data-icon-value' => 'join_right',
                ],
            ],
            [
                'value' => 'material-icons kayaking',
                'label' => 'kayaking',
                'attributes' => [
                    'data-icon' => 'material-icons kayaking',
                    'data-icon-value' => 'kayaking',
                ],
            ],
            [
                'value' => 'material-icons kebab_dining',
                'label' => 'kebab_dining',
                'attributes' => [
                    'data-icon' => 'material-icons kebab_dining',
                    'data-icon-value' => 'kebab_dining',
                ],
            ],
            [
                'value' => 'material-icons key',
                'label' => 'key',
                'attributes' => [
                    'data-icon' => 'material-icons key',
                    'data-icon-value' => 'key',
                ],
            ],
            [
                'value' => 'material-icons key_off',
                'label' => 'key_off',
                'attributes' => [
                    'data-icon' => 'material-icons key_off',
                    'data-icon-value' => 'key_off',
                ],
            ],
            [
                'value' => 'material-icons keyboard',
                'label' => 'keyboard',
                'attributes' => [
                    'data-icon' => 'material-icons keyboard',
                    'data-icon-value' => 'keyboard',
                ],
            ],
            [
                'value' => 'material-icons keyboard_alt',
                'label' => 'keyboard_alt',
                'attributes' => [
                    'data-icon' => 'material-icons keyboard_alt',
                    'data-icon-value' => 'keyboard_alt',
                ],
            ],
            [
                'value' => 'material-icons keyboard_arrow_down',
                'label' => 'keyboard_arrow_down',
                'attributes' => [
                    'data-icon' => 'material-icons keyboard_arrow_down',
                    'data-icon-value' => 'keyboard_arrow_down',
                ],
            ],
            [
                'value' => 'material-icons keyboard_arrow_left',
                'label' => 'keyboard_arrow_left',
                'attributes' => [
                    'data-icon' => 'material-icons keyboard_arrow_left',
                    'data-icon-value' => 'keyboard_arrow_left',
                ],
            ],
            [
                'value' => 'material-icons keyboard_arrow_right',
                'label' => 'keyboard_arrow_right',
                'attributes' => [
                    'data-icon' => 'material-icons keyboard_arrow_right',
                    'data-icon-value' => 'keyboard_arrow_right',
                ],
            ],
            [
                'value' => 'material-icons keyboard_arrow_up',
                'label' => 'keyboard_arrow_up',
                'attributes' => [
                    'data-icon' => 'material-icons keyboard_arrow_up',
                    'data-icon-value' => 'keyboard_arrow_up',
                ],
            ],
            [
                'value' => 'material-icons keyboard_backspace',
                'label' => 'keyboard_backspace',
                'attributes' => [
                    'data-icon' => 'material-icons keyboard_backspace',
                    'data-icon-value' => 'keyboard_backspace',
                ],
            ],
            [
                'value' => 'material-icons keyboard_capslock',
                'label' => 'keyboard_capslock',
                'attributes' => [
                    'data-icon' => 'material-icons keyboard_capslock',
                    'data-icon-value' => 'keyboard_capslock',
                ],
            ],
            [
                'value' => 'material-icons keyboard_command',
                'label' => 'keyboard_command',
                'attributes' => [
                    'data-icon' => 'material-icons keyboard_command',
                    'data-icon-value' => 'keyboard_command',
                ],
            ],
            [
                'value' => 'material-icons keyboard_command_key',
                'label' => 'keyboard_command_key',
                'attributes' => [
                    'data-icon' => 'material-icons keyboard_command_key',
                    'data-icon-value' => 'keyboard_command_key',
                ],
            ],
            [
                'value' => 'material-icons keyboard_control',
                'label' => 'keyboard_control',
                'attributes' => [
                    'data-icon' => 'material-icons keyboard_control',
                    'data-icon-value' => 'keyboard_control',
                ],
            ],
            [
                'value' => 'material-icons keyboard_control_key',
                'label' => 'keyboard_control_key',
                'attributes' => [
                    'data-icon' => 'material-icons keyboard_control_key',
                    'data-icon-value' => 'keyboard_control_key',
                ],
            ],
            [
                'value' => 'material-icons keyboard_double_arrow_down',
                'label' => 'keyboard_double_arrow_down',
                'attributes' => [
                    'data-icon' => 'material-icons keyboard_double_arrow_down',
                    'data-icon-value' => 'keyboard_double_arrow_down',
                ],
            ],
            [
                'value' => 'material-icons keyboard_double_arrow_left',
                'label' => 'keyboard_double_arrow_left',
                'attributes' => [
                    'data-icon' => 'material-icons keyboard_double_arrow_left',
                    'data-icon-value' => 'keyboard_double_arrow_left',
                ],
            ],
            [
                'value' => 'material-icons keyboard_double_arrow_right',
                'label' => 'keyboard_double_arrow_right',
                'attributes' => [
                    'data-icon' => 'material-icons keyboard_double_arrow_right',
                    'data-icon-value' => 'keyboard_double_arrow_right',
                ],
            ],
            [
                'value' => 'material-icons keyboard_double_arrow_up',
                'label' => 'keyboard_double_arrow_up',
                'attributes' => [
                    'data-icon' => 'material-icons keyboard_double_arrow_up',
                    'data-icon-value' => 'keyboard_double_arrow_up',
                ],
            ],
            [
                'value' => 'material-icons keyboard_hide',
                'label' => 'keyboard_hide',
                'attributes' => [
                    'data-icon' => 'material-icons keyboard_hide',
                    'data-icon-value' => 'keyboard_hide',
                ],
            ],
            [
                'value' => 'material-icons keyboard_option',
                'label' => 'keyboard_option',
                'attributes' => [
                    'data-icon' => 'material-icons keyboard_option',
                    'data-icon-value' => 'keyboard_option',
                ],
            ],
            [
                'value' => 'material-icons keyboard_option_key',
                'label' => 'keyboard_option_key',
                'attributes' => [
                    'data-icon' => 'material-icons keyboard_option_key',
                    'data-icon-value' => 'keyboard_option_key',
                ],
            ],
            [
                'value' => 'material-icons keyboard_return',
                'label' => 'keyboard_return',
                'attributes' => [
                    'data-icon' => 'material-icons keyboard_return',
                    'data-icon-value' => 'keyboard_return',
                ],
            ],
            [
                'value' => 'material-icons keyboard_tab',
                'label' => 'keyboard_tab',
                'attributes' => [
                    'data-icon' => 'material-icons keyboard_tab',
                    'data-icon-value' => 'keyboard_tab',
                ],
            ],
            [
                'value' => 'material-icons keyboard_voice',
                'label' => 'keyboard_voice',
                'attributes' => [
                    'data-icon' => 'material-icons keyboard_voice',
                    'data-icon-value' => 'keyboard_voice',
                ],
            ],
            [
                'value' => 'material-icons king_bed',
                'label' => 'king_bed',
                'attributes' => [
                    'data-icon' => 'material-icons king_bed',
                    'data-icon-value' => 'king_bed',
                ],
            ],
            [
                'value' => 'material-icons kitchen',
                'label' => 'kitchen',
                'attributes' => [
                    'data-icon' => 'material-icons kitchen',
                    'data-icon-value' => 'kitchen',
                ],
            ],
            [
                'value' => 'material-icons kitesurfing',
                'label' => 'kitesurfing',
                'attributes' => [
                    'data-icon' => 'material-icons kitesurfing',
                    'data-icon-value' => 'kitesurfing',
                ],
            ],
            [
                'value' => 'material-icons label',
                'label' => 'label',
                'attributes' => [
                    'data-icon' => 'material-icons label',
                    'data-icon-value' => 'label',
                ],
            ],
            [
                'value' => 'material-icons label_important',
                'label' => 'label_important',
                'attributes' => [
                    'data-icon' => 'material-icons label_important',
                    'data-icon-value' => 'label_important',
                ],
            ],
            [
                'value' => 'material-icons label_important_outline',
                'label' => 'label_important_outline',
                'attributes' => [
                    'data-icon' => 'material-icons label_important_outline',
                    'data-icon-value' => 'label_important_outline',
                ],
            ],
            [
                'value' => 'material-icons label_off',
                'label' => 'label_off',
                'attributes' => [
                    'data-icon' => 'material-icons label_off',
                    'data-icon-value' => 'label_off',
                ],
            ],
            [
                'value' => 'material-icons label_outline',
                'label' => 'label_outline',
                'attributes' => [
                    'data-icon' => 'material-icons label_outline',
                    'data-icon-value' => 'label_outline',
                ],
            ],
            [
                'value' => 'material-icons lan',
                'label' => 'lan',
                'attributes' => [
                    'data-icon' => 'material-icons lan',
                    'data-icon-value' => 'lan',
                ],
            ],
            [
                'value' => 'material-icons landscape',
                'label' => 'landscape',
                'attributes' => [
                    'data-icon' => 'material-icons landscape',
                    'data-icon-value' => 'landscape',
                ],
            ],
            [
                'value' => 'material-icons landslide',
                'label' => 'landslide',
                'attributes' => [
                    'data-icon' => 'material-icons landslide',
                    'data-icon-value' => 'landslide',
                ],
            ],
            [
                'value' => 'material-icons language',
                'label' => 'language',
                'attributes' => [
                    'data-icon' => 'material-icons language',
                    'data-icon-value' => 'language',
                ],
            ],
            [
                'value' => 'material-icons laptop',
                'label' => 'laptop',
                'attributes' => [
                    'data-icon' => 'material-icons laptop',
                    'data-icon-value' => 'laptop',
                ],
            ],
            [
                'value' => 'material-icons laptop_chromebook',
                'label' => 'laptop_chromebook',
                'attributes' => [
                    'data-icon' => 'material-icons laptop_chromebook',
                    'data-icon-value' => 'laptop_chromebook',
                ],
            ],
            [
                'value' => 'material-icons laptop_mac',
                'label' => 'laptop_mac',
                'attributes' => [
                    'data-icon' => 'material-icons laptop_mac',
                    'data-icon-value' => 'laptop_mac',
                ],
            ],
            [
                'value' => 'material-icons laptop_windows',
                'label' => 'laptop_windows',
                'attributes' => [
                    'data-icon' => 'material-icons laptop_windows',
                    'data-icon-value' => 'laptop_windows',
                ],
            ],
            [
                'value' => 'material-icons last_page',
                'label' => 'last_page',
                'attributes' => [
                    'data-icon' => 'material-icons last_page',
                    'data-icon-value' => 'last_page',
                ],
            ],
            [
                'value' => 'material-icons launch',
                'label' => 'launch',
                'attributes' => [
                    'data-icon' => 'material-icons launch',
                    'data-icon-value' => 'launch',
                ],
            ],
            [
                'value' => 'material-icons layers',
                'label' => 'layers',
                'attributes' => [
                    'data-icon' => 'material-icons layers',
                    'data-icon-value' => 'layers',
                ],
            ],
            [
                'value' => 'material-icons layers_clear',
                'label' => 'layers_clear',
                'attributes' => [
                    'data-icon' => 'material-icons layers_clear',
                    'data-icon-value' => 'layers_clear',
                ],
            ],
            [
                'value' => 'material-icons leaderboard',
                'label' => 'leaderboard',
                'attributes' => [
                    'data-icon' => 'material-icons leaderboard',
                    'data-icon-value' => 'leaderboard',
                ],
            ],
            [
                'value' => 'material-icons leak_add',
                'label' => 'leak_add',
                'attributes' => [
                    'data-icon' => 'material-icons leak_add',
                    'data-icon-value' => 'leak_add',
                ],
            ],
            [
                'value' => 'material-icons leak_remove',
                'label' => 'leak_remove',
                'attributes' => [
                    'data-icon' => 'material-icons leak_remove',
                    'data-icon-value' => 'leak_remove',
                ],
            ],
            [
                'value' => 'material-icons leave_bags_at_home',
                'label' => 'leave_bags_at_home',
                'attributes' => [
                    'data-icon' => 'material-icons leave_bags_at_home',
                    'data-icon-value' => 'leave_bags_at_home',
                ],
            ],
            [
                'value' => 'material-icons legend_toggle',
                'label' => 'legend_toggle',
                'attributes' => [
                    'data-icon' => 'material-icons legend_toggle',
                    'data-icon-value' => 'legend_toggle',
                ],
            ],
            [
                'value' => 'material-icons lens',
                'label' => 'lens',
                'attributes' => [
                    'data-icon' => 'material-icons lens',
                    'data-icon-value' => 'lens',
                ],
            ],
            [
                'value' => 'material-icons lens_blur',
                'label' => 'lens_blur',
                'attributes' => [
                    'data-icon' => 'material-icons lens_blur',
                    'data-icon-value' => 'lens_blur',
                ],
            ],
            [
                'value' => 'material-icons library_add',
                'label' => 'library_add',
                'attributes' => [
                    'data-icon' => 'material-icons library_add',
                    'data-icon-value' => 'library_add',
                ],
            ],
            [
                'value' => 'material-icons library_add_check',
                'label' => 'library_add_check',
                'attributes' => [
                    'data-icon' => 'material-icons library_add_check',
                    'data-icon-value' => 'library_add_check',
                ],
            ],
            [
                'value' => 'material-icons library_books',
                'label' => 'library_books',
                'attributes' => [
                    'data-icon' => 'material-icons library_books',
                    'data-icon-value' => 'library_books',
                ],
            ],
            [
                'value' => 'material-icons library_music',
                'label' => 'library_music',
                'attributes' => [
                    'data-icon' => 'material-icons library_music',
                    'data-icon-value' => 'library_music',
                ],
            ],
            [
                'value' => 'material-icons light',
                'label' => 'light',
                'attributes' => [
                    'data-icon' => 'material-icons light',
                    'data-icon-value' => 'light',
                ],
            ],
            [
                'value' => 'material-icons light_mode',
                'label' => 'light_mode',
                'attributes' => [
                    'data-icon' => 'material-icons light_mode',
                    'data-icon-value' => 'light_mode',
                ],
            ],
            [
                'value' => 'material-icons lightbulb',
                'label' => 'lightbulb',
                'attributes' => [
                    'data-icon' => 'material-icons lightbulb',
                    'data-icon-value' => 'lightbulb',
                ],
            ],
            [
                'value' => 'material-icons lightbulb_circle',
                'label' => 'lightbulb_circle',
                'attributes' => [
                    'data-icon' => 'material-icons lightbulb_circle',
                    'data-icon-value' => 'lightbulb_circle',
                ],
            ],
            [
                'value' => 'material-icons lightbulb_outline',
                'label' => 'lightbulb_outline',
                'attributes' => [
                    'data-icon' => 'material-icons lightbulb_outline',
                    'data-icon-value' => 'lightbulb_outline',
                ],
            ],
            [
                'value' => 'material-icons line_axis',
                'label' => 'line_axis',
                'attributes' => [
                    'data-icon' => 'material-icons line_axis',
                    'data-icon-value' => 'line_axis',
                ],
            ],
            [
                'value' => 'material-icons line_style',
                'label' => 'line_style',
                'attributes' => [
                    'data-icon' => 'material-icons line_style',
                    'data-icon-value' => 'line_style',
                ],
            ],
            [
                'value' => 'material-icons line_weight',
                'label' => 'line_weight',
                'attributes' => [
                    'data-icon' => 'material-icons line_weight',
                    'data-icon-value' => 'line_weight',
                ],
            ],
            [
                'value' => 'material-icons linear_scale',
                'label' => 'linear_scale',
                'attributes' => [
                    'data-icon' => 'material-icons linear_scale',
                    'data-icon-value' => 'linear_scale',
                ],
            ],
            [
                'value' => 'material-icons link',
                'label' => 'link',
                'attributes' => [
                    'data-icon' => 'material-icons link',
                    'data-icon-value' => 'link',
                ],
            ],
            [
                'value' => 'material-icons link_off',
                'label' => 'link_off',
                'attributes' => [
                    'data-icon' => 'material-icons link_off',
                    'data-icon-value' => 'link_off',
                ],
            ],
            [
                'value' => 'material-icons linked_camera',
                'label' => 'linked_camera',
                'attributes' => [
                    'data-icon' => 'material-icons linked_camera',
                    'data-icon-value' => 'linked_camera',
                ],
            ],
            [
                'value' => 'material-icons liquor',
                'label' => 'liquor',
                'attributes' => [
                    'data-icon' => 'material-icons liquor',
                    'data-icon-value' => 'liquor',
                ],
            ],
            [
                'value' => 'material-icons list',
                'label' => 'list',
                'attributes' => [
                    'data-icon' => 'material-icons list',
                    'data-icon-value' => 'list',
                ],
            ],
            [
                'value' => 'material-icons list_alt',
                'label' => 'list_alt',
                'attributes' => [
                    'data-icon' => 'material-icons list_alt',
                    'data-icon-value' => 'list_alt',
                ],
            ],
            [
                'value' => 'material-icons live_help',
                'label' => 'live_help',
                'attributes' => [
                    'data-icon' => 'material-icons live_help',
                    'data-icon-value' => 'live_help',
                ],
            ],
            [
                'value' => 'material-icons live_tv',
                'label' => 'live_tv',
                'attributes' => [
                    'data-icon' => 'material-icons live_tv',
                    'data-icon-value' => 'live_tv',
                ],
            ],
            [
                'value' => 'material-icons living',
                'label' => 'living',
                'attributes' => [
                    'data-icon' => 'material-icons living',
                    'data-icon-value' => 'living',
                ],
            ],
            [
                'value' => 'material-icons local_activity',
                'label' => 'local_activity',
                'attributes' => [
                    'data-icon' => 'material-icons local_activity',
                    'data-icon-value' => 'local_activity',
                ],
            ],
            [
                'value' => 'material-icons local_airport',
                'label' => 'local_airport',
                'attributes' => [
                    'data-icon' => 'material-icons local_airport',
                    'data-icon-value' => 'local_airport',
                ],
            ],
            [
                'value' => 'material-icons local_atm',
                'label' => 'local_atm',
                'attributes' => [
                    'data-icon' => 'material-icons local_atm',
                    'data-icon-value' => 'local_atm',
                ],
            ],
            [
                'value' => 'material-icons local_attraction',
                'label' => 'local_attraction',
                'attributes' => [
                    'data-icon' => 'material-icons local_attraction',
                    'data-icon-value' => 'local_attraction',
                ],
            ],
            [
                'value' => 'material-icons local_bar',
                'label' => 'local_bar',
                'attributes' => [
                    'data-icon' => 'material-icons local_bar',
                    'data-icon-value' => 'local_bar',
                ],
            ],
            [
                'value' => 'material-icons local_cafe',
                'label' => 'local_cafe',
                'attributes' => [
                    'data-icon' => 'material-icons local_cafe',
                    'data-icon-value' => 'local_cafe',
                ],
            ],
            [
                'value' => 'material-icons local_car_wash',
                'label' => 'local_car_wash',
                'attributes' => [
                    'data-icon' => 'material-icons local_car_wash',
                    'data-icon-value' => 'local_car_wash',
                ],
            ],
            [
                'value' => 'material-icons local_convenience_store',
                'label' => 'local_convenience_store',
                'attributes' => [
                    'data-icon' => 'material-icons local_convenience_store',
                    'data-icon-value' => 'local_convenience_store',
                ],
            ],
            [
                'value' => 'material-icons local_dining',
                'label' => 'local_dining',
                'attributes' => [
                    'data-icon' => 'material-icons local_dining',
                    'data-icon-value' => 'local_dining',
                ],
            ],
            [
                'value' => 'material-icons local_drink',
                'label' => 'local_drink',
                'attributes' => [
                    'data-icon' => 'material-icons local_drink',
                    'data-icon-value' => 'local_drink',
                ],
            ],
            [
                'value' => 'material-icons local_fire_department',
                'label' => 'local_fire_department',
                'attributes' => [
                    'data-icon' => 'material-icons local_fire_department',
                    'data-icon-value' => 'local_fire_department',
                ],
            ],
            [
                'value' => 'material-icons local_florist',
                'label' => 'local_florist',
                'attributes' => [
                    'data-icon' => 'material-icons local_florist',
                    'data-icon-value' => 'local_florist',
                ],
            ],
            [
                'value' => 'material-icons local_gas_station',
                'label' => 'local_gas_station',
                'attributes' => [
                    'data-icon' => 'material-icons local_gas_station',
                    'data-icon-value' => 'local_gas_station',
                ],
            ],
            [
                'value' => 'material-icons local_grocery_store',
                'label' => 'local_grocery_store',
                'attributes' => [
                    'data-icon' => 'material-icons local_grocery_store',
                    'data-icon-value' => 'local_grocery_store',
                ],
            ],
            [
                'value' => 'material-icons local_hospital',
                'label' => 'local_hospital',
                'attributes' => [
                    'data-icon' => 'material-icons local_hospital',
                    'data-icon-value' => 'local_hospital',
                ],
            ],
            [
                'value' => 'material-icons local_hotel',
                'label' => 'local_hotel',
                'attributes' => [
                    'data-icon' => 'material-icons local_hotel',
                    'data-icon-value' => 'local_hotel',
                ],
            ],
            [
                'value' => 'material-icons local_laundry_service',
                'label' => 'local_laundry_service',
                'attributes' => [
                    'data-icon' => 'material-icons local_laundry_service',
                    'data-icon-value' => 'local_laundry_service',
                ],
            ],
            [
                'value' => 'material-icons local_library',
                'label' => 'local_library',
                'attributes' => [
                    'data-icon' => 'material-icons local_library',
                    'data-icon-value' => 'local_library',
                ],
            ],
            [
                'value' => 'material-icons local_mall',
                'label' => 'local_mall',
                'attributes' => [
                    'data-icon' => 'material-icons local_mall',
                    'data-icon-value' => 'local_mall',
                ],
            ],
            [
                'value' => 'material-icons local_movies',
                'label' => 'local_movies',
                'attributes' => [
                    'data-icon' => 'material-icons local_movies',
                    'data-icon-value' => 'local_movies',
                ],
            ],
            [
                'value' => 'material-icons local_offer',
                'label' => 'local_offer',
                'attributes' => [
                    'data-icon' => 'material-icons local_offer',
                    'data-icon-value' => 'local_offer',
                ],
            ],
            [
                'value' => 'material-icons local_parking',
                'label' => 'local_parking',
                'attributes' => [
                    'data-icon' => 'material-icons local_parking',
                    'data-icon-value' => 'local_parking',
                ],
            ],
            [
                'value' => 'material-icons local_pharmacy',
                'label' => 'local_pharmacy',
                'attributes' => [
                    'data-icon' => 'material-icons local_pharmacy',
                    'data-icon-value' => 'local_pharmacy',
                ],
            ],
            [
                'value' => 'material-icons local_phone',
                'label' => 'local_phone',
                'attributes' => [
                    'data-icon' => 'material-icons local_phone',
                    'data-icon-value' => 'local_phone',
                ],
            ],
            [
                'value' => 'material-icons local_pizza',
                'label' => 'local_pizza',
                'attributes' => [
                    'data-icon' => 'material-icons local_pizza',
                    'data-icon-value' => 'local_pizza',
                ],
            ],
            [
                'value' => 'material-icons local_play',
                'label' => 'local_play',
                'attributes' => [
                    'data-icon' => 'material-icons local_play',
                    'data-icon-value' => 'local_play',
                ],
            ],
            [
                'value' => 'material-icons local_police',
                'label' => 'local_police',
                'attributes' => [
                    'data-icon' => 'material-icons local_police',
                    'data-icon-value' => 'local_police',
                ],
            ],
            [
                'value' => 'material-icons local_post_office',
                'label' => 'local_post_office',
                'attributes' => [
                    'data-icon' => 'material-icons local_post_office',
                    'data-icon-value' => 'local_post_office',
                ],
            ],
            [
                'value' => 'material-icons local_print_shop',
                'label' => 'local_print_shop',
                'attributes' => [
                    'data-icon' => 'material-icons local_print_shop',
                    'data-icon-value' => 'local_print_shop',
                ],
            ],
            [
                'value' => 'material-icons local_printshop',
                'label' => 'local_printshop',
                'attributes' => [
                    'data-icon' => 'material-icons local_printshop',
                    'data-icon-value' => 'local_printshop',
                ],
            ],
            [
                'value' => 'material-icons local_restaurant',
                'label' => 'local_restaurant',
                'attributes' => [
                    'data-icon' => 'material-icons local_restaurant',
                    'data-icon-value' => 'local_restaurant',
                ],
            ],
            [
                'value' => 'material-icons local_see',
                'label' => 'local_see',
                'attributes' => [
                    'data-icon' => 'material-icons local_see',
                    'data-icon-value' => 'local_see',
                ],
            ],
            [
                'value' => 'material-icons local_shipping',
                'label' => 'local_shipping',
                'attributes' => [
                    'data-icon' => 'material-icons local_shipping',
                    'data-icon-value' => 'local_shipping',
                ],
            ],
            [
                'value' => 'material-icons local_taxi',
                'label' => 'local_taxi',
                'attributes' => [
                    'data-icon' => 'material-icons local_taxi',
                    'data-icon-value' => 'local_taxi',
                ],
            ],
            [
                'value' => 'material-icons location_city',
                'label' => 'location_city',
                'attributes' => [
                    'data-icon' => 'material-icons location_city',
                    'data-icon-value' => 'location_city',
                ],
            ],
            [
                'value' => 'material-icons location_disabled',
                'label' => 'location_disabled',
                'attributes' => [
                    'data-icon' => 'material-icons location_disabled',
                    'data-icon-value' => 'location_disabled',
                ],
            ],
            [
                'value' => 'material-icons location_history',
                'label' => 'location_history',
                'attributes' => [
                    'data-icon' => 'material-icons location_history',
                    'data-icon-value' => 'location_history',
                ],
            ],
            [
                'value' => 'material-icons location_off',
                'label' => 'location_off',
                'attributes' => [
                    'data-icon' => 'material-icons location_off',
                    'data-icon-value' => 'location_off',
                ],
            ],
            [
                'value' => 'material-icons location_on',
                'label' => 'location_on',
                'attributes' => [
                    'data-icon' => 'material-icons location_on',
                    'data-icon-value' => 'location_on',
                ],
            ],
            [
                'value' => 'material-icons location_pin',
                'label' => 'location_pin',
                'attributes' => [
                    'data-icon' => 'material-icons location_pin',
                    'data-icon-value' => 'location_pin',
                ],
            ],
            [
                'value' => 'material-icons location_searching',
                'label' => 'location_searching',
                'attributes' => [
                    'data-icon' => 'material-icons location_searching',
                    'data-icon-value' => 'location_searching',
                ],
            ],
            [
                'value' => 'material-icons lock',
                'label' => 'lock',
                'attributes' => [
                    'data-icon' => 'material-icons lock',
                    'data-icon-value' => 'lock',
                ],
            ],
            [
                'value' => 'material-icons lock_clock',
                'label' => 'lock_clock',
                'attributes' => [
                    'data-icon' => 'material-icons lock_clock',
                    'data-icon-value' => 'lock_clock',
                ],
            ],
            [
                'value' => 'material-icons lock_open',
                'label' => 'lock_open',
                'attributes' => [
                    'data-icon' => 'material-icons lock_open',
                    'data-icon-value' => 'lock_open',
                ],
            ],
            [
                'value' => 'material-icons lock_outline',
                'label' => 'lock_outline',
                'attributes' => [
                    'data-icon' => 'material-icons lock_outline',
                    'data-icon-value' => 'lock_outline',
                ],
            ],
            [
                'value' => 'material-icons lock_person',
                'label' => 'lock_person',
                'attributes' => [
                    'data-icon' => 'material-icons lock_person',
                    'data-icon-value' => 'lock_person',
                ],
            ],
            [
                'value' => 'material-icons lock_reset',
                'label' => 'lock_reset',
                'attributes' => [
                    'data-icon' => 'material-icons lock_reset',
                    'data-icon-value' => 'lock_reset',
                ],
            ],
            [
                'value' => 'material-icons login',
                'label' => 'login',
                'attributes' => [
                    'data-icon' => 'material-icons login',
                    'data-icon-value' => 'login',
                ],
            ],
            [
                'value' => 'material-icons logo_dev',
                'label' => 'logo_dev',
                'attributes' => [
                    'data-icon' => 'material-icons logo_dev',
                    'data-icon-value' => 'logo_dev',
                ],
            ],
            [
                'value' => 'material-icons logout',
                'label' => 'logout',
                'attributes' => [
                    'data-icon' => 'material-icons logout',
                    'data-icon-value' => 'logout',
                ],
            ],
            [
                'value' => 'material-icons looks',
                'label' => 'looks',
                'attributes' => [
                    'data-icon' => 'material-icons looks',
                    'data-icon-value' => 'looks',
                ],
            ],
            [
                'value' => 'material-icons looks_3',
                'label' => 'looks_3',
                'attributes' => [
                    'data-icon' => 'material-icons looks_3',
                    'data-icon-value' => 'looks_3',
                ],
            ],
            [
                'value' => 'material-icons looks_4',
                'label' => 'looks_4',
                'attributes' => [
                    'data-icon' => 'material-icons looks_4',
                    'data-icon-value' => 'looks_4',
                ],
            ],
            [
                'value' => 'material-icons looks_5',
                'label' => 'looks_5',
                'attributes' => [
                    'data-icon' => 'material-icons looks_5',
                    'data-icon-value' => 'looks_5',
                ],
            ],
            [
                'value' => 'material-icons looks_6',
                'label' => 'looks_6',
                'attributes' => [
                    'data-icon' => 'material-icons looks_6',
                    'data-icon-value' => 'looks_6',
                ],
            ],
            [
                'value' => 'material-icons looks_one',
                'label' => 'looks_one',
                'attributes' => [
                    'data-icon' => 'material-icons looks_one',
                    'data-icon-value' => 'looks_one',
                ],
            ],
            [
                'value' => 'material-icons looks_two',
                'label' => 'looks_two',
                'attributes' => [
                    'data-icon' => 'material-icons looks_two',
                    'data-icon-value' => 'looks_two',
                ],
            ],
            [
                'value' => 'material-icons loop',
                'label' => 'loop',
                'attributes' => [
                    'data-icon' => 'material-icons loop',
                    'data-icon-value' => 'loop',
                ],
            ],
            [
                'value' => 'material-icons loupe',
                'label' => 'loupe',
                'attributes' => [
                    'data-icon' => 'material-icons loupe',
                    'data-icon-value' => 'loupe',
                ],
            ],
            [
                'value' => 'material-icons low_priority',
                'label' => 'low_priority',
                'attributes' => [
                    'data-icon' => 'material-icons low_priority',
                    'data-icon-value' => 'low_priority',
                ],
            ],
            [
                'value' => 'material-icons loyalty',
                'label' => 'loyalty',
                'attributes' => [
                    'data-icon' => 'material-icons loyalty',
                    'data-icon-value' => 'loyalty',
                ],
            ],
            [
                'value' => 'material-icons lte_mobiledata',
                'label' => 'lte_mobiledata',
                'attributes' => [
                    'data-icon' => 'material-icons lte_mobiledata',
                    'data-icon-value' => 'lte_mobiledata',
                ],
            ],
            [
                'value' => 'material-icons lte_plus_mobiledata',
                'label' => 'lte_plus_mobiledata',
                'attributes' => [
                    'data-icon' => 'material-icons lte_plus_mobiledata',
                    'data-icon-value' => 'lte_plus_mobiledata',
                ],
            ],
            [
                'value' => 'material-icons luggage',
                'label' => 'luggage',
                'attributes' => [
                    'data-icon' => 'material-icons luggage',
                    'data-icon-value' => 'luggage',
                ],
            ],
            [
                'value' => 'material-icons lunch_dining',
                'label' => 'lunch_dining',
                'attributes' => [
                    'data-icon' => 'material-icons lunch_dining',
                    'data-icon-value' => 'lunch_dining',
                ],
            ],
            [
                'value' => 'material-icons lyrics',
                'label' => 'lyrics',
                'attributes' => [
                    'data-icon' => 'material-icons lyrics',
                    'data-icon-value' => 'lyrics',
                ],
            ],
            [
                'value' => 'material-icons mail',
                'label' => 'mail',
                'attributes' => [
                    'data-icon' => 'material-icons mail',
                    'data-icon-value' => 'mail',
                ],
            ],
            [
                'value' => 'material-icons mail_lock',
                'label' => 'mail_lock',
                'attributes' => [
                    'data-icon' => 'material-icons mail_lock',
                    'data-icon-value' => 'mail_lock',
                ],
            ],
            [
                'value' => 'material-icons mail_outline',
                'label' => 'mail_outline',
                'attributes' => [
                    'data-icon' => 'material-icons mail_outline',
                    'data-icon-value' => 'mail_outline',
                ],
            ],
            [
                'value' => 'material-icons male',
                'label' => 'male',
                'attributes' => [
                    'data-icon' => 'material-icons male',
                    'data-icon-value' => 'male',
                ],
            ],
            [
                'value' => 'material-icons man',
                'label' => 'man',
                'attributes' => [
                    'data-icon' => 'material-icons man',
                    'data-icon-value' => 'man',
                ],
            ],
            [
                'value' => 'material-icons manage_accounts',
                'label' => 'manage_accounts',
                'attributes' => [
                    'data-icon' => 'material-icons manage_accounts',
                    'data-icon-value' => 'manage_accounts',
                ],
            ],
            [
                'value' => 'material-icons manage_history',
                'label' => 'manage_history',
                'attributes' => [
                    'data-icon' => 'material-icons manage_history',
                    'data-icon-value' => 'manage_history',
                ],
            ],
            [
                'value' => 'material-icons manage_search',
                'label' => 'manage_search',
                'attributes' => [
                    'data-icon' => 'material-icons manage_search',
                    'data-icon-value' => 'manage_search',
                ],
            ],
            [
                'value' => 'material-icons map',
                'label' => 'map',
                'attributes' => [
                    'data-icon' => 'material-icons map',
                    'data-icon-value' => 'map',
                ],
            ],
            [
                'value' => 'material-icons maps_home_work',
                'label' => 'maps_home_work',
                'attributes' => [
                    'data-icon' => 'material-icons maps_home_work',
                    'data-icon-value' => 'maps_home_work',
                ],
            ],
            [
                'value' => 'material-icons maps_ugc',
                'label' => 'maps_ugc',
                'attributes' => [
                    'data-icon' => 'material-icons maps_ugc',
                    'data-icon-value' => 'maps_ugc',
                ],
            ],
            [
                'value' => 'material-icons margin',
                'label' => 'margin',
                'attributes' => [
                    'data-icon' => 'material-icons margin',
                    'data-icon-value' => 'margin',
                ],
            ],
            [
                'value' => 'material-icons mark_as_unread',
                'label' => 'mark_as_unread',
                'attributes' => [
                    'data-icon' => 'material-icons mark_as_unread',
                    'data-icon-value' => 'mark_as_unread',
                ],
            ],
            [
                'value' => 'material-icons mark_chat_read',
                'label' => 'mark_chat_read',
                'attributes' => [
                    'data-icon' => 'material-icons mark_chat_read',
                    'data-icon-value' => 'mark_chat_read',
                ],
            ],
            [
                'value' => 'material-icons mark_chat_unread',
                'label' => 'mark_chat_unread',
                'attributes' => [
                    'data-icon' => 'material-icons mark_chat_unread',
                    'data-icon-value' => 'mark_chat_unread',
                ],
            ],
            [
                'value' => 'material-icons mark_email_read',
                'label' => 'mark_email_read',
                'attributes' => [
                    'data-icon' => 'material-icons mark_email_read',
                    'data-icon-value' => 'mark_email_read',
                ],
            ],
            [
                'value' => 'material-icons mark_email_unread',
                'label' => 'mark_email_unread',
                'attributes' => [
                    'data-icon' => 'material-icons mark_email_unread',
                    'data-icon-value' => 'mark_email_unread',
                ],
            ],
            [
                'value' => 'material-icons mark_unread_chat_alt',
                'label' => 'mark_unread_chat_alt',
                'attributes' => [
                    'data-icon' => 'material-icons mark_unread_chat_alt',
                    'data-icon-value' => 'mark_unread_chat_alt',
                ],
            ],
            [
                'value' => 'material-icons markunread',
                'label' => 'markunread',
                'attributes' => [
                    'data-icon' => 'material-icons markunread',
                    'data-icon-value' => 'markunread',
                ],
            ],
            [
                'value' => 'material-icons markunread_mailbox',
                'label' => 'markunread_mailbox',
                'attributes' => [
                    'data-icon' => 'material-icons markunread_mailbox',
                    'data-icon-value' => 'markunread_mailbox',
                ],
            ],
            [
                'value' => 'material-icons masks',
                'label' => 'masks',
                'attributes' => [
                    'data-icon' => 'material-icons masks',
                    'data-icon-value' => 'masks',
                ],
            ],
            [
                'value' => 'material-icons maximize',
                'label' => 'maximize',
                'attributes' => [
                    'data-icon' => 'material-icons maximize',
                    'data-icon-value' => 'maximize',
                ],
            ],
            [
                'value' => 'material-icons media_bluetooth_off',
                'label' => 'media_bluetooth_off',
                'attributes' => [
                    'data-icon' => 'material-icons media_bluetooth_off',
                    'data-icon-value' => 'media_bluetooth_off',
                ],
            ],
            [
                'value' => 'material-icons media_bluetooth_on',
                'label' => 'media_bluetooth_on',
                'attributes' => [
                    'data-icon' => 'material-icons media_bluetooth_on',
                    'data-icon-value' => 'media_bluetooth_on',
                ],
            ],
            [
                'value' => 'material-icons mediation',
                'label' => 'mediation',
                'attributes' => [
                    'data-icon' => 'material-icons mediation',
                    'data-icon-value' => 'mediation',
                ],
            ],
            [
                'value' => 'material-icons medical_information',
                'label' => 'medical_information',
                'attributes' => [
                    'data-icon' => 'material-icons medical_information',
                    'data-icon-value' => 'medical_information',
                ],
            ],
            [
                'value' => 'material-icons medical_services',
                'label' => 'medical_services',
                'attributes' => [
                    'data-icon' => 'material-icons medical_services',
                    'data-icon-value' => 'medical_services',
                ],
            ],
            [
                'value' => 'material-icons medication',
                'label' => 'medication',
                'attributes' => [
                    'data-icon' => 'material-icons medication',
                    'data-icon-value' => 'medication',
                ],
            ],
            [
                'value' => 'material-icons medication_liquid',
                'label' => 'medication_liquid',
                'attributes' => [
                    'data-icon' => 'material-icons medication_liquid',
                    'data-icon-value' => 'medication_liquid',
                ],
            ],
            [
                'value' => 'material-icons meeting_room',
                'label' => 'meeting_room',
                'attributes' => [
                    'data-icon' => 'material-icons meeting_room',
                    'data-icon-value' => 'meeting_room',
                ],
            ],
            [
                'value' => 'material-icons memory',
                'label' => 'memory',
                'attributes' => [
                    'data-icon' => 'material-icons memory',
                    'data-icon-value' => 'memory',
                ],
            ],
            [
                'value' => 'material-icons menu',
                'label' => 'menu',
                'attributes' => [
                    'data-icon' => 'material-icons menu',
                    'data-icon-value' => 'menu',
                ],
            ],
            [
                'value' => 'material-icons menu_book',
                'label' => 'menu_book',
                'attributes' => [
                    'data-icon' => 'material-icons menu_book',
                    'data-icon-value' => 'menu_book',
                ],
            ],
            [
                'value' => 'material-icons menu_open',
                'label' => 'menu_open',
                'attributes' => [
                    'data-icon' => 'material-icons menu_open',
                    'data-icon-value' => 'menu_open',
                ],
            ],
            [
                'value' => 'material-icons merge',
                'label' => 'merge',
                'attributes' => [
                    'data-icon' => 'material-icons merge',
                    'data-icon-value' => 'merge',
                ],
            ],
            [
                'value' => 'material-icons merge_type',
                'label' => 'merge_type',
                'attributes' => [
                    'data-icon' => 'material-icons merge_type',
                    'data-icon-value' => 'merge_type',
                ],
            ],
            [
                'value' => 'material-icons message',
                'label' => 'message',
                'attributes' => [
                    'data-icon' => 'material-icons message',
                    'data-icon-value' => 'message',
                ],
            ],
            [
                'value' => 'material-icons messenger',
                'label' => 'messenger',
                'attributes' => [
                    'data-icon' => 'material-icons messenger',
                    'data-icon-value' => 'messenger',
                ],
            ],
            [
                'value' => 'material-icons messenger_outline',
                'label' => 'messenger_outline',
                'attributes' => [
                    'data-icon' => 'material-icons messenger_outline',
                    'data-icon-value' => 'messenger_outline',
                ],
            ],
            [
                'value' => 'material-icons mic',
                'label' => 'mic',
                'attributes' => [
                    'data-icon' => 'material-icons mic',
                    'data-icon-value' => 'mic',
                ],
            ],
            [
                'value' => 'material-icons mic_external_off',
                'label' => 'mic_external_off',
                'attributes' => [
                    'data-icon' => 'material-icons mic_external_off',
                    'data-icon-value' => 'mic_external_off',
                ],
            ],
            [
                'value' => 'material-icons mic_external_on',
                'label' => 'mic_external_on',
                'attributes' => [
                    'data-icon' => 'material-icons mic_external_on',
                    'data-icon-value' => 'mic_external_on',
                ],
            ],
            [
                'value' => 'material-icons mic_none',
                'label' => 'mic_none',
                'attributes' => [
                    'data-icon' => 'material-icons mic_none',
                    'data-icon-value' => 'mic_none',
                ],
            ],
            [
                'value' => 'material-icons mic_off',
                'label' => 'mic_off',
                'attributes' => [
                    'data-icon' => 'material-icons mic_off',
                    'data-icon-value' => 'mic_off',
                ],
            ],
            [
                'value' => 'material-icons microwave',
                'label' => 'microwave',
                'attributes' => [
                    'data-icon' => 'material-icons microwave',
                    'data-icon-value' => 'microwave',
                ],
            ],
            [
                'value' => 'material-icons military_tech',
                'label' => 'military_tech',
                'attributes' => [
                    'data-icon' => 'material-icons military_tech',
                    'data-icon-value' => 'military_tech',
                ],
            ],
            [
                'value' => 'material-icons minimize',
                'label' => 'minimize',
                'attributes' => [
                    'data-icon' => 'material-icons minimize',
                    'data-icon-value' => 'minimize',
                ],
            ],
            [
                'value' => 'material-icons minor_crash',
                'label' => 'minor_crash',
                'attributes' => [
                    'data-icon' => 'material-icons minor_crash',
                    'data-icon-value' => 'minor_crash',
                ],
            ],
            [
                'value' => 'material-icons miscellaneous_services',
                'label' => 'miscellaneous_services',
                'attributes' => [
                    'data-icon' => 'material-icons miscellaneous_services',
                    'data-icon-value' => 'miscellaneous_services',
                ],
            ],
            [
                'value' => 'material-icons missed_video_call',
                'label' => 'missed_video_call',
                'attributes' => [
                    'data-icon' => 'material-icons missed_video_call',
                    'data-icon-value' => 'missed_video_call',
                ],
            ],
            [
                'value' => 'material-icons mms',
                'label' => 'mms',
                'attributes' => [
                    'data-icon' => 'material-icons mms',
                    'data-icon-value' => 'mms',
                ],
            ],
            [
                'value' => 'material-icons mobile_friendly',
                'label' => 'mobile_friendly',
                'attributes' => [
                    'data-icon' => 'material-icons mobile_friendly',
                    'data-icon-value' => 'mobile_friendly',
                ],
            ],
            [
                'value' => 'material-icons mobile_off',
                'label' => 'mobile_off',
                'attributes' => [
                    'data-icon' => 'material-icons mobile_off',
                    'data-icon-value' => 'mobile_off',
                ],
            ],
            [
                'value' => 'material-icons mobile_screen_share',
                'label' => 'mobile_screen_share',
                'attributes' => [
                    'data-icon' => 'material-icons mobile_screen_share',
                    'data-icon-value' => 'mobile_screen_share',
                ],
            ],
            [
                'value' => 'material-icons mobiledata_off',
                'label' => 'mobiledata_off',
                'attributes' => [
                    'data-icon' => 'material-icons mobiledata_off',
                    'data-icon-value' => 'mobiledata_off',
                ],
            ],
            [
                'value' => 'material-icons mode',
                'label' => 'mode',
                'attributes' => [
                    'data-icon' => 'material-icons mode',
                    'data-icon-value' => 'mode',
                ],
            ],
            [
                'value' => 'material-icons mode_comment',
                'label' => 'mode_comment',
                'attributes' => [
                    'data-icon' => 'material-icons mode_comment',
                    'data-icon-value' => 'mode_comment',
                ],
            ],
            [
                'value' => 'material-icons mode_edit',
                'label' => 'mode_edit',
                'attributes' => [
                    'data-icon' => 'material-icons mode_edit',
                    'data-icon-value' => 'mode_edit',
                ],
            ],
            [
                'value' => 'material-icons mode_edit_outline',
                'label' => 'mode_edit_outline',
                'attributes' => [
                    'data-icon' => 'material-icons mode_edit_outline',
                    'data-icon-value' => 'mode_edit_outline',
                ],
            ],
            [
                'value' => 'material-icons mode_fan_off',
                'label' => 'mode_fan_off',
                'attributes' => [
                    'data-icon' => 'material-icons mode_fan_off',
                    'data-icon-value' => 'mode_fan_off',
                ],
            ],
            [
                'value' => 'material-icons mode_night',
                'label' => 'mode_night',
                'attributes' => [
                    'data-icon' => 'material-icons mode_night',
                    'data-icon-value' => 'mode_night',
                ],
            ],
            [
                'value' => 'material-icons mode_of_travel',
                'label' => 'mode_of_travel',
                'attributes' => [
                    'data-icon' => 'material-icons mode_of_travel',
                    'data-icon-value' => 'mode_of_travel',
                ],
            ],
            [
                'value' => 'material-icons mode_standby',
                'label' => 'mode_standby',
                'attributes' => [
                    'data-icon' => 'material-icons mode_standby',
                    'data-icon-value' => 'mode_standby',
                ],
            ],
            [
                'value' => 'material-icons model_training',
                'label' => 'model_training',
                'attributes' => [
                    'data-icon' => 'material-icons model_training',
                    'data-icon-value' => 'model_training',
                ],
            ],
            [
                'value' => 'material-icons monetization_on',
                'label' => 'monetization_on',
                'attributes' => [
                    'data-icon' => 'material-icons monetization_on',
                    'data-icon-value' => 'monetization_on',
                ],
            ],
            [
                'value' => 'material-icons money',
                'label' => 'money',
                'attributes' => [
                    'data-icon' => 'material-icons money',
                    'data-icon-value' => 'money',
                ],
            ],
            [
                'value' => 'material-icons money_off',
                'label' => 'money_off',
                'attributes' => [
                    'data-icon' => 'material-icons money_off',
                    'data-icon-value' => 'money_off',
                ],
            ],
            [
                'value' => 'material-icons money_off_csred',
                'label' => 'money_off_csred',
                'attributes' => [
                    'data-icon' => 'material-icons money_off_csred',
                    'data-icon-value' => 'money_off_csred',
                ],
            ],
            [
                'value' => 'material-icons monitor',
                'label' => 'monitor',
                'attributes' => [
                    'data-icon' => 'material-icons monitor',
                    'data-icon-value' => 'monitor',
                ],
            ],
            [
                'value' => 'material-icons monitor_heart',
                'label' => 'monitor_heart',
                'attributes' => [
                    'data-icon' => 'material-icons monitor_heart',
                    'data-icon-value' => 'monitor_heart',
                ],
            ],
            [
                'value' => 'material-icons monitor_weight',
                'label' => 'monitor_weight',
                'attributes' => [
                    'data-icon' => 'material-icons monitor_weight',
                    'data-icon-value' => 'monitor_weight',
                ],
            ],
            [
                'value' => 'material-icons monochrome_photos',
                'label' => 'monochrome_photos',
                'attributes' => [
                    'data-icon' => 'material-icons monochrome_photos',
                    'data-icon-value' => 'monochrome_photos',
                ],
            ],
            [
                'value' => 'material-icons mood',
                'label' => 'mood',
                'attributes' => [
                    'data-icon' => 'material-icons mood',
                    'data-icon-value' => 'mood',
                ],
            ],
            [
                'value' => 'material-icons mood_bad',
                'label' => 'mood_bad',
                'attributes' => [
                    'data-icon' => 'material-icons mood_bad',
                    'data-icon-value' => 'mood_bad',
                ],
            ],
            [
                'value' => 'material-icons moped',
                'label' => 'moped',
                'attributes' => [
                    'data-icon' => 'material-icons moped',
                    'data-icon-value' => 'moped',
                ],
            ],
            [
                'value' => 'material-icons more',
                'label' => 'more',
                'attributes' => [
                    'data-icon' => 'material-icons more',
                    'data-icon-value' => 'more',
                ],
            ],
            [
                'value' => 'material-icons more_horiz',
                'label' => 'more_horiz',
                'attributes' => [
                    'data-icon' => 'material-icons more_horiz',
                    'data-icon-value' => 'more_horiz',
                ],
            ],
            [
                'value' => 'material-icons more_time',
                'label' => 'more_time',
                'attributes' => [
                    'data-icon' => 'material-icons more_time',
                    'data-icon-value' => 'more_time',
                ],
            ],
            [
                'value' => 'material-icons more_vert',
                'label' => 'more_vert',
                'attributes' => [
                    'data-icon' => 'material-icons more_vert',
                    'data-icon-value' => 'more_vert',
                ],
            ],
            [
                'value' => 'material-icons mosque',
                'label' => 'mosque',
                'attributes' => [
                    'data-icon' => 'material-icons mosque',
                    'data-icon-value' => 'mosque',
                ],
            ],
            [
                'value' => 'material-icons motion_photos_auto',
                'label' => 'motion_photos_auto',
                'attributes' => [
                    'data-icon' => 'material-icons motion_photos_auto',
                    'data-icon-value' => 'motion_photos_auto',
                ],
            ],
            [
                'value' => 'material-icons motion_photos_off',
                'label' => 'motion_photos_off',
                'attributes' => [
                    'data-icon' => 'material-icons motion_photos_off',
                    'data-icon-value' => 'motion_photos_off',
                ],
            ],
            [
                'value' => 'material-icons motion_photos_on',
                'label' => 'motion_photos_on',
                'attributes' => [
                    'data-icon' => 'material-icons motion_photos_on',
                    'data-icon-value' => 'motion_photos_on',
                ],
            ],
            [
                'value' => 'material-icons motion_photos_pause',
                'label' => 'motion_photos_pause',
                'attributes' => [
                    'data-icon' => 'material-icons motion_photos_pause',
                    'data-icon-value' => 'motion_photos_pause',
                ],
            ],
            [
                'value' => 'material-icons motion_photos_paused',
                'label' => 'motion_photos_paused',
                'attributes' => [
                    'data-icon' => 'material-icons motion_photos_paused',
                    'data-icon-value' => 'motion_photos_paused',
                ],
            ],
            [
                'value' => 'material-icons motorcycle',
                'label' => 'motorcycle',
                'attributes' => [
                    'data-icon' => 'material-icons motorcycle',
                    'data-icon-value' => 'motorcycle',
                ],
            ],
            [
                'value' => 'material-icons mouse',
                'label' => 'mouse',
                'attributes' => [
                    'data-icon' => 'material-icons mouse',
                    'data-icon-value' => 'mouse',
                ],
            ],
            [
                'value' => 'material-icons move_down',
                'label' => 'move_down',
                'attributes' => [
                    'data-icon' => 'material-icons move_down',
                    'data-icon-value' => 'move_down',
                ],
            ],
            [
                'value' => 'material-icons move_to_inbox',
                'label' => 'move_to_inbox',
                'attributes' => [
                    'data-icon' => 'material-icons move_to_inbox',
                    'data-icon-value' => 'move_to_inbox',
                ],
            ],
            [
                'value' => 'material-icons move_up',
                'label' => 'move_up',
                'attributes' => [
                    'data-icon' => 'material-icons move_up',
                    'data-icon-value' => 'move_up',
                ],
            ],
            [
                'value' => 'material-icons movie',
                'label' => 'movie',
                'attributes' => [
                    'data-icon' => 'material-icons movie',
                    'data-icon-value' => 'movie',
                ],
            ],
            [
                'value' => 'material-icons movie_creation',
                'label' => 'movie_creation',
                'attributes' => [
                    'data-icon' => 'material-icons movie_creation',
                    'data-icon-value' => 'movie_creation',
                ],
            ],
            [
                'value' => 'material-icons movie_filter',
                'label' => 'movie_filter',
                'attributes' => [
                    'data-icon' => 'material-icons movie_filter',
                    'data-icon-value' => 'movie_filter',
                ],
            ],
            [
                'value' => 'material-icons moving',
                'label' => 'moving',
                'attributes' => [
                    'data-icon' => 'material-icons moving',
                    'data-icon-value' => 'moving',
                ],
            ],
            [
                'value' => 'material-icons mp',
                'label' => 'mp',
                'attributes' => [
                    'data-icon' => 'material-icons mp',
                    'data-icon-value' => 'mp',
                ],
            ],
            [
                'value' => 'material-icons multiline_chart',
                'label' => 'multiline_chart',
                'attributes' => [
                    'data-icon' => 'material-icons multiline_chart',
                    'data-icon-value' => 'multiline_chart',
                ],
            ],
            [
                'value' => 'material-icons multiple_stop',
                'label' => 'multiple_stop',
                'attributes' => [
                    'data-icon' => 'material-icons multiple_stop',
                    'data-icon-value' => 'multiple_stop',
                ],
            ],
            [
                'value' => 'material-icons multitrack_audio',
                'label' => 'multitrack_audio',
                'attributes' => [
                    'data-icon' => 'material-icons multitrack_audio',
                    'data-icon-value' => 'multitrack_audio',
                ],
            ],
            [
                'value' => 'material-icons museum',
                'label' => 'museum',
                'attributes' => [
                    'data-icon' => 'material-icons museum',
                    'data-icon-value' => 'museum',
                ],
            ],
            [
                'value' => 'material-icons music_note',
                'label' => 'music_note',
                'attributes' => [
                    'data-icon' => 'material-icons music_note',
                    'data-icon-value' => 'music_note',
                ],
            ],
            [
                'value' => 'material-icons music_off',
                'label' => 'music_off',
                'attributes' => [
                    'data-icon' => 'material-icons music_off',
                    'data-icon-value' => 'music_off',
                ],
            ],
            [
                'value' => 'material-icons music_video',
                'label' => 'music_video',
                'attributes' => [
                    'data-icon' => 'material-icons music_video',
                    'data-icon-value' => 'music_video',
                ],
            ],
            [
                'value' => 'material-icons my_library_add',
                'label' => 'my_library_add',
                'attributes' => [
                    'data-icon' => 'material-icons my_library_add',
                    'data-icon-value' => 'my_library_add',
                ],
            ],
            [
                'value' => 'material-icons my_library_books',
                'label' => 'my_library_books',
                'attributes' => [
                    'data-icon' => 'material-icons my_library_books',
                    'data-icon-value' => 'my_library_books',
                ],
            ],
            [
                'value' => 'material-icons my_library_music',
                'label' => 'my_library_music',
                'attributes' => [
                    'data-icon' => 'material-icons my_library_music',
                    'data-icon-value' => 'my_library_music',
                ],
            ],
            [
                'value' => 'material-icons my_location',
                'label' => 'my_location',
                'attributes' => [
                    'data-icon' => 'material-icons my_location',
                    'data-icon-value' => 'my_location',
                ],
            ],
            [
                'value' => 'material-icons nat',
                'label' => 'nat',
                'attributes' => [
                    'data-icon' => 'material-icons nat',
                    'data-icon-value' => 'nat',
                ],
            ],
            [
                'value' => 'material-icons nature',
                'label' => 'nature',
                'attributes' => [
                    'data-icon' => 'material-icons nature',
                    'data-icon-value' => 'nature',
                ],
            ],
            [
                'value' => 'material-icons nature_people',
                'label' => 'nature_people',
                'attributes' => [
                    'data-icon' => 'material-icons nature_people',
                    'data-icon-value' => 'nature_people',
                ],
            ],
            [
                'value' => 'material-icons navigate_before',
                'label' => 'navigate_before',
                'attributes' => [
                    'data-icon' => 'material-icons navigate_before',
                    'data-icon-value' => 'navigate_before',
                ],
            ],
            [
                'value' => 'material-icons navigate_next',
                'label' => 'navigate_next',
                'attributes' => [
                    'data-icon' => 'material-icons navigate_next',
                    'data-icon-value' => 'navigate_next',
                ],
            ],
            [
                'value' => 'material-icons navigation',
                'label' => 'navigation',
                'attributes' => [
                    'data-icon' => 'material-icons navigation',
                    'data-icon-value' => 'navigation',
                ],
            ],
            [
                'value' => 'material-icons near_me',
                'label' => 'near_me',
                'attributes' => [
                    'data-icon' => 'material-icons near_me',
                    'data-icon-value' => 'near_me',
                ],
            ],
            [
                'value' => 'material-icons near_me_disabled',
                'label' => 'near_me_disabled',
                'attributes' => [
                    'data-icon' => 'material-icons near_me_disabled',
                    'data-icon-value' => 'near_me_disabled',
                ],
            ],
            [
                'value' => 'material-icons nearby_error',
                'label' => 'nearby_error',
                'attributes' => [
                    'data-icon' => 'material-icons nearby_error',
                    'data-icon-value' => 'nearby_error',
                ],
            ],
            [
                'value' => 'material-icons nearby_off',
                'label' => 'nearby_off',
                'attributes' => [
                    'data-icon' => 'material-icons nearby_off',
                    'data-icon-value' => 'nearby_off',
                ],
            ],
            [
                'value' => 'material-icons nest_cam_wired_stand',
                'label' => 'nest_cam_wired_stand',
                'attributes' => [
                    'data-icon' => 'material-icons nest_cam_wired_stand',
                    'data-icon-value' => 'nest_cam_wired_stand',
                ],
            ],
            [
                'value' => 'material-icons network_cell',
                'label' => 'network_cell',
                'attributes' => [
                    'data-icon' => 'material-icons network_cell',
                    'data-icon-value' => 'network_cell',
                ],
            ],
            [
                'value' => 'material-icons network_check',
                'label' => 'network_check',
                'attributes' => [
                    'data-icon' => 'material-icons network_check',
                    'data-icon-value' => 'network_check',
                ],
            ],
            [
                'value' => 'material-icons network_locked',
                'label' => 'network_locked',
                'attributes' => [
                    'data-icon' => 'material-icons network_locked',
                    'data-icon-value' => 'network_locked',
                ],
            ],
            [
                'value' => 'material-icons network_ping',
                'label' => 'network_ping',
                'attributes' => [
                    'data-icon' => 'material-icons network_ping',
                    'data-icon-value' => 'network_ping',
                ],
            ],
            [
                'value' => 'material-icons network_wifi',
                'label' => 'network_wifi',
                'attributes' => [
                    'data-icon' => 'material-icons network_wifi',
                    'data-icon-value' => 'network_wifi',
                ],
            ],
            [
                'value' => 'material-icons network_wifi_1_bar',
                'label' => 'network_wifi_1_bar',
                'attributes' => [
                    'data-icon' => 'material-icons network_wifi_1_bar',
                    'data-icon-value' => 'network_wifi_1_bar',
                ],
            ],
            [
                'value' => 'material-icons network_wifi_2_bar',
                'label' => 'network_wifi_2_bar',
                'attributes' => [
                    'data-icon' => 'material-icons network_wifi_2_bar',
                    'data-icon-value' => 'network_wifi_2_bar',
                ],
            ],
            [
                'value' => 'material-icons network_wifi_3_bar',
                'label' => 'network_wifi_3_bar',
                'attributes' => [
                    'data-icon' => 'material-icons network_wifi_3_bar',
                    'data-icon-value' => 'network_wifi_3_bar',
                ],
            ],
            [
                'value' => 'material-icons new_label',
                'label' => 'new_label',
                'attributes' => [
                    'data-icon' => 'material-icons new_label',
                    'data-icon-value' => 'new_label',
                ],
            ],
            [
                'value' => 'material-icons new_releases',
                'label' => 'new_releases',
                'attributes' => [
                    'data-icon' => 'material-icons new_releases',
                    'data-icon-value' => 'new_releases',
                ],
            ],
            [
                'value' => 'material-icons newspaper',
                'label' => 'newspaper',
                'attributes' => [
                    'data-icon' => 'material-icons newspaper',
                    'data-icon-value' => 'newspaper',
                ],
            ],
            [
                'value' => 'material-icons next_plan',
                'label' => 'next_plan',
                'attributes' => [
                    'data-icon' => 'material-icons next_plan',
                    'data-icon-value' => 'next_plan',
                ],
            ],
            [
                'value' => 'material-icons next_week',
                'label' => 'next_week',
                'attributes' => [
                    'data-icon' => 'material-icons next_week',
                    'data-icon-value' => 'next_week',
                ],
            ],
            [
                'value' => 'material-icons nfc',
                'label' => 'nfc',
                'attributes' => [
                    'data-icon' => 'material-icons nfc',
                    'data-icon-value' => 'nfc',
                ],
            ],
            [
                'value' => 'material-icons night_shelter',
                'label' => 'night_shelter',
                'attributes' => [
                    'data-icon' => 'material-icons night_shelter',
                    'data-icon-value' => 'night_shelter',
                ],
            ],
            [
                'value' => 'material-icons nightlife',
                'label' => 'nightlife',
                'attributes' => [
                    'data-icon' => 'material-icons nightlife',
                    'data-icon-value' => 'nightlife',
                ],
            ],
            [
                'value' => 'material-icons nightlight',
                'label' => 'nightlight',
                'attributes' => [
                    'data-icon' => 'material-icons nightlight',
                    'data-icon-value' => 'nightlight',
                ],
            ],
            [
                'value' => 'material-icons nightlight_round',
                'label' => 'nightlight_round',
                'attributes' => [
                    'data-icon' => 'material-icons nightlight_round',
                    'data-icon-value' => 'nightlight_round',
                ],
            ],
            [
                'value' => 'material-icons nights_stay',
                'label' => 'nights_stay',
                'attributes' => [
                    'data-icon' => 'material-icons nights_stay',
                    'data-icon-value' => 'nights_stay',
                ],
            ],
            [
                'value' => 'material-icons no_accounts',
                'label' => 'no_accounts',
                'attributes' => [
                    'data-icon' => 'material-icons no_accounts',
                    'data-icon-value' => 'no_accounts',
                ],
            ],
            [
                'value' => 'material-icons no_adult_content',
                'label' => 'no_adult_content',
                'attributes' => [
                    'data-icon' => 'material-icons no_adult_content',
                    'data-icon-value' => 'no_adult_content',
                ],
            ],
            [
                'value' => 'material-icons no_backpack',
                'label' => 'no_backpack',
                'attributes' => [
                    'data-icon' => 'material-icons no_backpack',
                    'data-icon-value' => 'no_backpack',
                ],
            ],
            [
                'value' => 'material-icons no_cell',
                'label' => 'no_cell',
                'attributes' => [
                    'data-icon' => 'material-icons no_cell',
                    'data-icon-value' => 'no_cell',
                ],
            ],
            [
                'value' => 'material-icons no_crash',
                'label' => 'no_crash',
                'attributes' => [
                    'data-icon' => 'material-icons no_crash',
                    'data-icon-value' => 'no_crash',
                ],
            ],
            [
                'value' => 'material-icons no_drinks',
                'label' => 'no_drinks',
                'attributes' => [
                    'data-icon' => 'material-icons no_drinks',
                    'data-icon-value' => 'no_drinks',
                ],
            ],
            [
                'value' => 'material-icons no_encryption',
                'label' => 'no_encryption',
                'attributes' => [
                    'data-icon' => 'material-icons no_encryption',
                    'data-icon-value' => 'no_encryption',
                ],
            ],
            [
                'value' => 'material-icons no_encryption_gmailerrorred',
                'label' => 'no_encryption_gmailerrorred',
                'attributes' => [
                    'data-icon' => 'material-icons no_encryption_gmailerrorred',
                    'data-icon-value' => 'no_encryption_gmailerrorred',
                ],
            ],
            [
                'value' => 'material-icons no_flash',
                'label' => 'no_flash',
                'attributes' => [
                    'data-icon' => 'material-icons no_flash',
                    'data-icon-value' => 'no_flash',
                ],
            ],
            [
                'value' => 'material-icons no_food',
                'label' => 'no_food',
                'attributes' => [
                    'data-icon' => 'material-icons no_food',
                    'data-icon-value' => 'no_food',
                ],
            ],
            [
                'value' => 'material-icons no_luggage',
                'label' => 'no_luggage',
                'attributes' => [
                    'data-icon' => 'material-icons no_luggage',
                    'data-icon-value' => 'no_luggage',
                ],
            ],
            [
                'value' => 'material-icons no_meals',
                'label' => 'no_meals',
                'attributes' => [
                    'data-icon' => 'material-icons no_meals',
                    'data-icon-value' => 'no_meals',
                ],
            ],
            [
                'value' => 'material-icons no_meals_ouline',
                'label' => 'no_meals_ouline',
                'attributes' => [
                    'data-icon' => 'material-icons no_meals_ouline',
                    'data-icon-value' => 'no_meals_ouline',
                ],
            ],
            [
                'value' => 'material-icons no_meeting_room',
                'label' => 'no_meeting_room',
                'attributes' => [
                    'data-icon' => 'material-icons no_meeting_room',
                    'data-icon-value' => 'no_meeting_room',
                ],
            ],
            [
                'value' => 'material-icons no_photography',
                'label' => 'no_photography',
                'attributes' => [
                    'data-icon' => 'material-icons no_photography',
                    'data-icon-value' => 'no_photography',
                ],
            ],
            [
                'value' => 'material-icons no_sim',
                'label' => 'no_sim',
                'attributes' => [
                    'data-icon' => 'material-icons no_sim',
                    'data-icon-value' => 'no_sim',
                ],
            ],
            [
                'value' => 'material-icons no_stroller',
                'label' => 'no_stroller',
                'attributes' => [
                    'data-icon' => 'material-icons no_stroller',
                    'data-icon-value' => 'no_stroller',
                ],
            ],
            [
                'value' => 'material-icons no_transfer',
                'label' => 'no_transfer',
                'attributes' => [
                    'data-icon' => 'material-icons no_transfer',
                    'data-icon-value' => 'no_transfer',
                ],
            ],
            [
                'value' => 'material-icons noise_aware',
                'label' => 'noise_aware',
                'attributes' => [
                    'data-icon' => 'material-icons noise_aware',
                    'data-icon-value' => 'noise_aware',
                ],
            ],
            [
                'value' => 'material-icons noise_control_off',
                'label' => 'noise_control_off',
                'attributes' => [
                    'data-icon' => 'material-icons noise_control_off',
                    'data-icon-value' => 'noise_control_off',
                ],
            ],
            [
                'value' => 'material-icons nordic_walking',
                'label' => 'nordic_walking',
                'attributes' => [
                    'data-icon' => 'material-icons nordic_walking',
                    'data-icon-value' => 'nordic_walking',
                ],
            ],
            [
                'value' => 'material-icons north',
                'label' => 'north',
                'attributes' => [
                    'data-icon' => 'material-icons north',
                    'data-icon-value' => 'north',
                ],
            ],
            [
                'value' => 'material-icons north_east',
                'label' => 'north_east',
                'attributes' => [
                    'data-icon' => 'material-icons north_east',
                    'data-icon-value' => 'north_east',
                ],
            ],
            [
                'value' => 'material-icons north_west',
                'label' => 'north_west',
                'attributes' => [
                    'data-icon' => 'material-icons north_west',
                    'data-icon-value' => 'north_west',
                ],
            ],
            [
                'value' => 'material-icons not_accessible',
                'label' => 'not_accessible',
                'attributes' => [
                    'data-icon' => 'material-icons not_accessible',
                    'data-icon-value' => 'not_accessible',
                ],
            ],
            [
                'value' => 'material-icons not_interested',
                'label' => 'not_interested',
                'attributes' => [
                    'data-icon' => 'material-icons not_interested',
                    'data-icon-value' => 'not_interested',
                ],
            ],
            [
                'value' => 'material-icons not_listed_location',
                'label' => 'not_listed_location',
                'attributes' => [
                    'data-icon' => 'material-icons not_listed_location',
                    'data-icon-value' => 'not_listed_location',
                ],
            ],
            [
                'value' => 'material-icons not_started',
                'label' => 'not_started',
                'attributes' => [
                    'data-icon' => 'material-icons not_started',
                    'data-icon-value' => 'not_started',
                ],
            ],
            [
                'value' => 'material-icons note',
                'label' => 'note',
                'attributes' => [
                    'data-icon' => 'material-icons note',
                    'data-icon-value' => 'note',
                ],
            ],
            [
                'value' => 'material-icons note_add',
                'label' => 'note_add',
                'attributes' => [
                    'data-icon' => 'material-icons note_add',
                    'data-icon-value' => 'note_add',
                ],
            ],
            [
                'value' => 'material-icons note_alt',
                'label' => 'note_alt',
                'attributes' => [
                    'data-icon' => 'material-icons note_alt',
                    'data-icon-value' => 'note_alt',
                ],
            ],
            [
                'value' => 'material-icons notes',
                'label' => 'notes',
                'attributes' => [
                    'data-icon' => 'material-icons notes',
                    'data-icon-value' => 'notes',
                ],
            ],
            [
                'value' => 'material-icons notification_add',
                'label' => 'notification_add',
                'attributes' => [
                    'data-icon' => 'material-icons notification_add',
                    'data-icon-value' => 'notification_add',
                ],
            ],
            [
                'value' => 'material-icons notification_important',
                'label' => 'notification_important',
                'attributes' => [
                    'data-icon' => 'material-icons notification_important',
                    'data-icon-value' => 'notification_important',
                ],
            ],
            [
                'value' => 'material-icons notifications',
                'label' => 'notifications',
                'attributes' => [
                    'data-icon' => 'material-icons notifications',
                    'data-icon-value' => 'notifications',
                ],
            ],
            [
                'value' => 'material-icons notifications_active',
                'label' => 'notifications_active',
                'attributes' => [
                    'data-icon' => 'material-icons notifications_active',
                    'data-icon-value' => 'notifications_active',
                ],
            ],
            [
                'value' => 'material-icons notifications_none',
                'label' => 'notifications_none',
                'attributes' => [
                    'data-icon' => 'material-icons notifications_none',
                    'data-icon-value' => 'notifications_none',
                ],
            ],
            [
                'value' => 'material-icons notifications_off',
                'label' => 'notifications_off',
                'attributes' => [
                    'data-icon' => 'material-icons notifications_off',
                    'data-icon-value' => 'notifications_off',
                ],
            ],
            [
                'value' => 'material-icons notifications_on',
                'label' => 'notifications_on',
                'attributes' => [
                    'data-icon' => 'material-icons notifications_on',
                    'data-icon-value' => 'notifications_on',
                ],
            ],
            [
                'value' => 'material-icons notifications_paused',
                'label' => 'notifications_paused',
                'attributes' => [
                    'data-icon' => 'material-icons notifications_paused',
                    'data-icon-value' => 'notifications_paused',
                ],
            ],
            [
                'value' => 'material-icons now_wallpaper',
                'label' => 'now_wallpaper',
                'attributes' => [
                    'data-icon' => 'material-icons now_wallpaper',
                    'data-icon-value' => 'now_wallpaper',
                ],
            ],
            [
                'value' => 'material-icons now_widgets',
                'label' => 'now_widgets',
                'attributes' => [
                    'data-icon' => 'material-icons now_widgets',
                    'data-icon-value' => 'now_widgets',
                ],
            ],
            [
                'value' => 'material-icons numbers',
                'label' => 'numbers',
                'attributes' => [
                    'data-icon' => 'material-icons numbers',
                    'data-icon-value' => 'numbers',
                ],
            ],
            [
                'value' => 'material-icons offline_bolt',
                'label' => 'offline_bolt',
                'attributes' => [
                    'data-icon' => 'material-icons offline_bolt',
                    'data-icon-value' => 'offline_bolt',
                ],
            ],
            [
                'value' => 'material-icons offline_pin',
                'label' => 'offline_pin',
                'attributes' => [
                    'data-icon' => 'material-icons offline_pin',
                    'data-icon-value' => 'offline_pin',
                ],
            ],
            [
                'value' => 'material-icons offline_share',
                'label' => 'offline_share',
                'attributes' => [
                    'data-icon' => 'material-icons offline_share',
                    'data-icon-value' => 'offline_share',
                ],
            ],
            [
                'value' => 'material-icons oil_barrel',
                'label' => 'oil_barrel',
                'attributes' => [
                    'data-icon' => 'material-icons oil_barrel',
                    'data-icon-value' => 'oil_barrel',
                ],
            ],
            [
                'value' => 'material-icons on_device_training',
                'label' => 'on_device_training',
                'attributes' => [
                    'data-icon' => 'material-icons on_device_training',
                    'data-icon-value' => 'on_device_training',
                ],
            ],
            [
                'value' => 'material-icons ondemand_video',
                'label' => 'ondemand_video',
                'attributes' => [
                    'data-icon' => 'material-icons ondemand_video',
                    'data-icon-value' => 'ondemand_video',
                ],
            ],
            [
                'value' => 'material-icons online_prediction',
                'label' => 'online_prediction',
                'attributes' => [
                    'data-icon' => 'material-icons online_prediction',
                    'data-icon-value' => 'online_prediction',
                ],
            ],
            [
                'value' => 'material-icons opacity',
                'label' => 'opacity',
                'attributes' => [
                    'data-icon' => 'material-icons opacity',
                    'data-icon-value' => 'opacity',
                ],
            ],
            [
                'value' => 'material-icons open_in_browser',
                'label' => 'open_in_browser',
                'attributes' => [
                    'data-icon' => 'material-icons open_in_browser',
                    'data-icon-value' => 'open_in_browser',
                ],
            ],
            [
                'value' => 'material-icons open_in_full',
                'label' => 'open_in_full',
                'attributes' => [
                    'data-icon' => 'material-icons open_in_full',
                    'data-icon-value' => 'open_in_full',
                ],
            ],
            [
                'value' => 'material-icons open_in_new',
                'label' => 'open_in_new',
                'attributes' => [
                    'data-icon' => 'material-icons open_in_new',
                    'data-icon-value' => 'open_in_new',
                ],
            ],
            [
                'value' => 'material-icons open_in_new_off',
                'label' => 'open_in_new_off',
                'attributes' => [
                    'data-icon' => 'material-icons open_in_new_off',
                    'data-icon-value' => 'open_in_new_off',
                ],
            ],
            [
                'value' => 'material-icons open_with',
                'label' => 'open_with',
                'attributes' => [
                    'data-icon' => 'material-icons open_with',
                    'data-icon-value' => 'open_with',
                ],
            ],
            [
                'value' => 'material-icons other_houses',
                'label' => 'other_houses',
                'attributes' => [
                    'data-icon' => 'material-icons other_houses',
                    'data-icon-value' => 'other_houses',
                ],
            ],
            [
                'value' => 'material-icons outbond',
                'label' => 'outbond',
                'attributes' => [
                    'data-icon' => 'material-icons outbond',
                    'data-icon-value' => 'outbond',
                ],
            ],
            [
                'value' => 'material-icons outbound',
                'label' => 'outbound',
                'attributes' => [
                    'data-icon' => 'material-icons outbound',
                    'data-icon-value' => 'outbound',
                ],
            ],
            [
                'value' => 'material-icons outbox',
                'label' => 'outbox',
                'attributes' => [
                    'data-icon' => 'material-icons outbox',
                    'data-icon-value' => 'outbox',
                ],
            ],
            [
                'value' => 'material-icons outdoor_grill',
                'label' => 'outdoor_grill',
                'attributes' => [
                    'data-icon' => 'material-icons outdoor_grill',
                    'data-icon-value' => 'outdoor_grill',
                ],
            ],
            [
                'value' => 'material-icons outgoing_mail',
                'label' => 'outgoing_mail',
                'attributes' => [
                    'data-icon' => 'material-icons outgoing_mail',
                    'data-icon-value' => 'outgoing_mail',
                ],
            ],
            [
                'value' => 'material-icons outlet',
                'label' => 'outlet',
                'attributes' => [
                    'data-icon' => 'material-icons outlet',
                    'data-icon-value' => 'outlet',
                ],
            ],
            [
                'value' => 'material-icons outlined_flag',
                'label' => 'outlined_flag',
                'attributes' => [
                    'data-icon' => 'material-icons outlined_flag',
                    'data-icon-value' => 'outlined_flag',
                ],
            ],
            [
                'value' => 'material-icons output',
                'label' => 'output',
                'attributes' => [
                    'data-icon' => 'material-icons output',
                    'data-icon-value' => 'output',
                ],
            ],
            [
                'value' => 'material-icons padding',
                'label' => 'padding',
                'attributes' => [
                    'data-icon' => 'material-icons padding',
                    'data-icon-value' => 'padding',
                ],
            ],
            [
                'value' => 'material-icons pages',
                'label' => 'pages',
                'attributes' => [
                    'data-icon' => 'material-icons pages',
                    'data-icon-value' => 'pages',
                ],
            ],
            [
                'value' => 'material-icons pageview',
                'label' => 'pageview',
                'attributes' => [
                    'data-icon' => 'material-icons pageview',
                    'data-icon-value' => 'pageview',
                ],
            ],
            [
                'value' => 'material-icons paid',
                'label' => 'paid',
                'attributes' => [
                    'data-icon' => 'material-icons paid',
                    'data-icon-value' => 'paid',
                ],
            ],
            [
                'value' => 'material-icons palette',
                'label' => 'palette',
                'attributes' => [
                    'data-icon' => 'material-icons palette',
                    'data-icon-value' => 'palette',
                ],
            ],
            [
                'value' => 'material-icons pan_tool',
                'label' => 'pan_tool',
                'attributes' => [
                    'data-icon' => 'material-icons pan_tool',
                    'data-icon-value' => 'pan_tool',
                ],
            ],
            [
                'value' => 'material-icons pan_tool_alt',
                'label' => 'pan_tool_alt',
                'attributes' => [
                    'data-icon' => 'material-icons pan_tool_alt',
                    'data-icon-value' => 'pan_tool_alt',
                ],
            ],
            [
                'value' => 'material-icons panorama',
                'label' => 'panorama',
                'attributes' => [
                    'data-icon' => 'material-icons panorama',
                    'data-icon-value' => 'panorama',
                ],
            ],
            [
                'value' => 'material-icons panorama_fish_eye',
                'label' => 'panorama_fish_eye',
                'attributes' => [
                    'data-icon' => 'material-icons panorama_fish_eye',
                    'data-icon-value' => 'panorama_fish_eye',
                ],
            ],
            [
                'value' => 'material-icons panorama_fisheye',
                'label' => 'panorama_fisheye',
                'attributes' => [
                    'data-icon' => 'material-icons panorama_fisheye',
                    'data-icon-value' => 'panorama_fisheye',
                ],
            ],
            [
                'value' => 'material-icons panorama_horizontal',
                'label' => 'panorama_horizontal',
                'attributes' => [
                    'data-icon' => 'material-icons panorama_horizontal',
                    'data-icon-value' => 'panorama_horizontal',
                ],
            ],
            [
                'value' => 'material-icons panorama_horizontal_select',
                'label' => 'panorama_horizontal_select',
                'attributes' => [
                    'data-icon' => 'material-icons panorama_horizontal_select',
                    'data-icon-value' => 'panorama_horizontal_select',
                ],
            ],
            [
                'value' => 'material-icons panorama_photosphere',
                'label' => 'panorama_photosphere',
                'attributes' => [
                    'data-icon' => 'material-icons panorama_photosphere',
                    'data-icon-value' => 'panorama_photosphere',
                ],
            ],
            [
                'value' => 'material-icons panorama_photosphere_select',
                'label' => 'panorama_photosphere_select',
                'attributes' => [
                    'data-icon' => 'material-icons panorama_photosphere_select',
                    'data-icon-value' => 'panorama_photosphere_select',
                ],
            ],
            [
                'value' => 'material-icons panorama_vertical',
                'label' => 'panorama_vertical',
                'attributes' => [
                    'data-icon' => 'material-icons panorama_vertical',
                    'data-icon-value' => 'panorama_vertical',
                ],
            ],
            [
                'value' => 'material-icons panorama_vertical_select',
                'label' => 'panorama_vertical_select',
                'attributes' => [
                    'data-icon' => 'material-icons panorama_vertical_select',
                    'data-icon-value' => 'panorama_vertical_select',
                ],
            ],
            [
                'value' => 'material-icons panorama_wide_angle',
                'label' => 'panorama_wide_angle',
                'attributes' => [
                    'data-icon' => 'material-icons panorama_wide_angle',
                    'data-icon-value' => 'panorama_wide_angle',
                ],
            ],
            [
                'value' => 'material-icons panorama_wide_angle_select',
                'label' => 'panorama_wide_angle_select',
                'attributes' => [
                    'data-icon' => 'material-icons panorama_wide_angle_select',
                    'data-icon-value' => 'panorama_wide_angle_select',
                ],
            ],
            [
                'value' => 'material-icons paragliding',
                'label' => 'paragliding',
                'attributes' => [
                    'data-icon' => 'material-icons paragliding',
                    'data-icon-value' => 'paragliding',
                ],
            ],
            [
                'value' => 'material-icons park',
                'label' => 'park',
                'attributes' => [
                    'data-icon' => 'material-icons park',
                    'data-icon-value' => 'park',
                ],
            ],
            [
                'value' => 'material-icons party_mode',
                'label' => 'party_mode',
                'attributes' => [
                    'data-icon' => 'material-icons party_mode',
                    'data-icon-value' => 'party_mode',
                ],
            ],
            [
                'value' => 'material-icons password',
                'label' => 'password',
                'attributes' => [
                    'data-icon' => 'material-icons password',
                    'data-icon-value' => 'password',
                ],
            ],
            [
                'value' => 'material-icons pattern',
                'label' => 'pattern',
                'attributes' => [
                    'data-icon' => 'material-icons pattern',
                    'data-icon-value' => 'pattern',
                ],
            ],
            [
                'value' => 'material-icons pause',
                'label' => 'pause',
                'attributes' => [
                    'data-icon' => 'material-icons pause',
                    'data-icon-value' => 'pause',
                ],
            ],
            [
                'value' => 'material-icons pause_circle',
                'label' => 'pause_circle',
                'attributes' => [
                    'data-icon' => 'material-icons pause_circle',
                    'data-icon-value' => 'pause_circle',
                ],
            ],
            [
                'value' => 'material-icons pause_circle_filled',
                'label' => 'pause_circle_filled',
                'attributes' => [
                    'data-icon' => 'material-icons pause_circle_filled',
                    'data-icon-value' => 'pause_circle_filled',
                ],
            ],
            [
                'value' => 'material-icons pause_circle_outline',
                'label' => 'pause_circle_outline',
                'attributes' => [
                    'data-icon' => 'material-icons pause_circle_outline',
                    'data-icon-value' => 'pause_circle_outline',
                ],
            ],
            [
                'value' => 'material-icons pause_presentation',
                'label' => 'pause_presentation',
                'attributes' => [
                    'data-icon' => 'material-icons pause_presentation',
                    'data-icon-value' => 'pause_presentation',
                ],
            ],
            [
                'value' => 'material-icons payment',
                'label' => 'payment',
                'attributes' => [
                    'data-icon' => 'material-icons payment',
                    'data-icon-value' => 'payment',
                ],
            ],
            [
                'value' => 'material-icons payments',
                'label' => 'payments',
                'attributes' => [
                    'data-icon' => 'material-icons payments',
                    'data-icon-value' => 'payments',
                ],
            ],
            [
                'value' => 'material-icons paypal',
                'label' => 'paypal',
                'attributes' => [
                    'data-icon' => 'material-icons paypal',
                    'data-icon-value' => 'paypal',
                ],
            ],
            [
                'value' => 'material-icons pedal_bike',
                'label' => 'pedal_bike',
                'attributes' => [
                    'data-icon' => 'material-icons pedal_bike',
                    'data-icon-value' => 'pedal_bike',
                ],
            ],
            [
                'value' => 'material-icons pending',
                'label' => 'pending',
                'attributes' => [
                    'data-icon' => 'material-icons pending',
                    'data-icon-value' => 'pending',
                ],
            ],
            [
                'value' => 'material-icons pending_actions',
                'label' => 'pending_actions',
                'attributes' => [
                    'data-icon' => 'material-icons pending_actions',
                    'data-icon-value' => 'pending_actions',
                ],
            ],
            [
                'value' => 'material-icons pentagon',
                'label' => 'pentagon',
                'attributes' => [
                    'data-icon' => 'material-icons pentagon',
                    'data-icon-value' => 'pentagon',
                ],
            ],
            [
                'value' => 'material-icons people',
                'label' => 'people',
                'attributes' => [
                    'data-icon' => 'material-icons people',
                    'data-icon-value' => 'people',
                ],
            ],
            [
                'value' => 'material-icons people_alt',
                'label' => 'people_alt',
                'attributes' => [
                    'data-icon' => 'material-icons people_alt',
                    'data-icon-value' => 'people_alt',
                ],
            ],
            [
                'value' => 'material-icons people_outline',
                'label' => 'people_outline',
                'attributes' => [
                    'data-icon' => 'material-icons people_outline',
                    'data-icon-value' => 'people_outline',
                ],
            ],
            [
                'value' => 'material-icons percent',
                'label' => 'percent',
                'attributes' => [
                    'data-icon' => 'material-icons percent',
                    'data-icon-value' => 'percent',
                ],
            ],
            [
                'value' => 'material-icons perm_camera_mic',
                'label' => 'perm_camera_mic',
                'attributes' => [
                    'data-icon' => 'material-icons perm_camera_mic',
                    'data-icon-value' => 'perm_camera_mic',
                ],
            ],
            [
                'value' => 'material-icons perm_contact_cal',
                'label' => 'perm_contact_cal',
                'attributes' => [
                    'data-icon' => 'material-icons perm_contact_cal',
                    'data-icon-value' => 'perm_contact_cal',
                ],
            ],
            [
                'value' => 'material-icons perm_contact_calendar',
                'label' => 'perm_contact_calendar',
                'attributes' => [
                    'data-icon' => 'material-icons perm_contact_calendar',
                    'data-icon-value' => 'perm_contact_calendar',
                ],
            ],
            [
                'value' => 'material-icons perm_data_setting',
                'label' => 'perm_data_setting',
                'attributes' => [
                    'data-icon' => 'material-icons perm_data_setting',
                    'data-icon-value' => 'perm_data_setting',
                ],
            ],
            [
                'value' => 'material-icons perm_device_info',
                'label' => 'perm_device_info',
                'attributes' => [
                    'data-icon' => 'material-icons perm_device_info',
                    'data-icon-value' => 'perm_device_info',
                ],
            ],
            [
                'value' => 'material-icons perm_device_information',
                'label' => 'perm_device_information',
                'attributes' => [
                    'data-icon' => 'material-icons perm_device_information',
                    'data-icon-value' => 'perm_device_information',
                ],
            ],
            [
                'value' => 'material-icons perm_identity',
                'label' => 'perm_identity',
                'attributes' => [
                    'data-icon' => 'material-icons perm_identity',
                    'data-icon-value' => 'perm_identity',
                ],
            ],
            [
                'value' => 'material-icons perm_media',
                'label' => 'perm_media',
                'attributes' => [
                    'data-icon' => 'material-icons perm_media',
                    'data-icon-value' => 'perm_media',
                ],
            ],
            [
                'value' => 'material-icons perm_phone_msg',
                'label' => 'perm_phone_msg',
                'attributes' => [
                    'data-icon' => 'material-icons perm_phone_msg',
                    'data-icon-value' => 'perm_phone_msg',
                ],
            ],
            [
                'value' => 'material-icons perm_scan_wifi',
                'label' => 'perm_scan_wifi',
                'attributes' => [
                    'data-icon' => 'material-icons perm_scan_wifi',
                    'data-icon-value' => 'perm_scan_wifi',
                ],
            ],
            [
                'value' => 'material-icons person',
                'label' => 'person',
                'attributes' => [
                    'data-icon' => 'material-icons person',
                    'data-icon-value' => 'person',
                ],
            ],
            [
                'value' => 'material-icons person_add',
                'label' => 'person_add',
                'attributes' => [
                    'data-icon' => 'material-icons person_add',
                    'data-icon-value' => 'person_add',
                ],
            ],
            [
                'value' => 'material-icons person_add_alt',
                'label' => 'person_add_alt',
                'attributes' => [
                    'data-icon' => 'material-icons person_add_alt',
                    'data-icon-value' => 'person_add_alt',
                ],
            ],
            [
                'value' => 'material-icons person_add_alt_1',
                'label' => 'person_add_alt_1',
                'attributes' => [
                    'data-icon' => 'material-icons person_add_alt_1',
                    'data-icon-value' => 'person_add_alt_1',
                ],
            ],
            [
                'value' => 'material-icons person_add_disabled',
                'label' => 'person_add_disabled',
                'attributes' => [
                    'data-icon' => 'material-icons person_add_disabled',
                    'data-icon-value' => 'person_add_disabled',
                ],
            ],
            [
                'value' => 'material-icons person_off',
                'label' => 'person_off',
                'attributes' => [
                    'data-icon' => 'material-icons person_off',
                    'data-icon-value' => 'person_off',
                ],
            ],
            [
                'value' => 'material-icons person_outline',
                'label' => 'person_outline',
                'attributes' => [
                    'data-icon' => 'material-icons person_outline',
                    'data-icon-value' => 'person_outline',
                ],
            ],
            [
                'value' => 'material-icons person_pin',
                'label' => 'person_pin',
                'attributes' => [
                    'data-icon' => 'material-icons person_pin',
                    'data-icon-value' => 'person_pin',
                ],
            ],
            [
                'value' => 'material-icons person_pin_circle',
                'label' => 'person_pin_circle',
                'attributes' => [
                    'data-icon' => 'material-icons person_pin_circle',
                    'data-icon-value' => 'person_pin_circle',
                ],
            ],
            [
                'value' => 'material-icons person_remove',
                'label' => 'person_remove',
                'attributes' => [
                    'data-icon' => 'material-icons person_remove',
                    'data-icon-value' => 'person_remove',
                ],
            ],
            [
                'value' => 'material-icons person_remove_alt_1',
                'label' => 'person_remove_alt_1',
                'attributes' => [
                    'data-icon' => 'material-icons person_remove_alt_1',
                    'data-icon-value' => 'person_remove_alt_1',
                ],
            ],
            [
                'value' => 'material-icons person_search',
                'label' => 'person_search',
                'attributes' => [
                    'data-icon' => 'material-icons person_search',
                    'data-icon-value' => 'person_search',
                ],
            ],
            [
                'value' => 'material-icons personal_injury',
                'label' => 'personal_injury',
                'attributes' => [
                    'data-icon' => 'material-icons personal_injury',
                    'data-icon-value' => 'personal_injury',
                ],
            ],
            [
                'value' => 'material-icons personal_video',
                'label' => 'personal_video',
                'attributes' => [
                    'data-icon' => 'material-icons personal_video',
                    'data-icon-value' => 'personal_video',
                ],
            ],
            [
                'value' => 'material-icons pest_control',
                'label' => 'pest_control',
                'attributes' => [
                    'data-icon' => 'material-icons pest_control',
                    'data-icon-value' => 'pest_control',
                ],
            ],
            [
                'value' => 'material-icons pest_control_rodent',
                'label' => 'pest_control_rodent',
                'attributes' => [
                    'data-icon' => 'material-icons pest_control_rodent',
                    'data-icon-value' => 'pest_control_rodent',
                ],
            ],
            [
                'value' => 'material-icons pets',
                'label' => 'pets',
                'attributes' => [
                    'data-icon' => 'material-icons pets',
                    'data-icon-value' => 'pets',
                ],
            ],
            [
                'value' => 'material-icons phishing',
                'label' => 'phishing',
                'attributes' => [
                    'data-icon' => 'material-icons phishing',
                    'data-icon-value' => 'phishing',
                ],
            ],
            [
                'value' => 'material-icons phone',
                'label' => 'phone',
                'attributes' => [
                    'data-icon' => 'material-icons phone',
                    'data-icon-value' => 'phone',
                ],
            ],
            [
                'value' => 'material-icons phone_android',
                'label' => 'phone_android',
                'attributes' => [
                    'data-icon' => 'material-icons phone_android',
                    'data-icon-value' => 'phone_android',
                ],
            ],
            [
                'value' => 'material-icons phone_bluetooth_speaker',
                'label' => 'phone_bluetooth_speaker',
                'attributes' => [
                    'data-icon' => 'material-icons phone_bluetooth_speaker',
                    'data-icon-value' => 'phone_bluetooth_speaker',
                ],
            ],
            [
                'value' => 'material-icons phone_callback',
                'label' => 'phone_callback',
                'attributes' => [
                    'data-icon' => 'material-icons phone_callback',
                    'data-icon-value' => 'phone_callback',
                ],
            ],
            [
                'value' => 'material-icons phone_disabled',
                'label' => 'phone_disabled',
                'attributes' => [
                    'data-icon' => 'material-icons phone_disabled',
                    'data-icon-value' => 'phone_disabled',
                ],
            ],
            [
                'value' => 'material-icons phone_enabled',
                'label' => 'phone_enabled',
                'attributes' => [
                    'data-icon' => 'material-icons phone_enabled',
                    'data-icon-value' => 'phone_enabled',
                ],
            ],
            [
                'value' => 'material-icons phone_forwarded',
                'label' => 'phone_forwarded',
                'attributes' => [
                    'data-icon' => 'material-icons phone_forwarded',
                    'data-icon-value' => 'phone_forwarded',
                ],
            ],
            [
                'value' => 'material-icons phone_in_talk',
                'label' => 'phone_in_talk',
                'attributes' => [
                    'data-icon' => 'material-icons phone_in_talk',
                    'data-icon-value' => 'phone_in_talk',
                ],
            ],
            [
                'value' => 'material-icons phone_iphone',
                'label' => 'phone_iphone',
                'attributes' => [
                    'data-icon' => 'material-icons phone_iphone',
                    'data-icon-value' => 'phone_iphone',
                ],
            ],
            [
                'value' => 'material-icons phone_locked',
                'label' => 'phone_locked',
                'attributes' => [
                    'data-icon' => 'material-icons phone_locked',
                    'data-icon-value' => 'phone_locked',
                ],
            ],
            [
                'value' => 'material-icons phone_missed',
                'label' => 'phone_missed',
                'attributes' => [
                    'data-icon' => 'material-icons phone_missed',
                    'data-icon-value' => 'phone_missed',
                ],
            ],
            [
                'value' => 'material-icons phone_paused',
                'label' => 'phone_paused',
                'attributes' => [
                    'data-icon' => 'material-icons phone_paused',
                    'data-icon-value' => 'phone_paused',
                ],
            ],
            [
                'value' => 'material-icons phonelink',
                'label' => 'phonelink',
                'attributes' => [
                    'data-icon' => 'material-icons phonelink',
                    'data-icon-value' => 'phonelink',
                ],
            ],
            [
                'value' => 'material-icons phonelink_erase',
                'label' => 'phonelink_erase',
                'attributes' => [
                    'data-icon' => 'material-icons phonelink_erase',
                    'data-icon-value' => 'phonelink_erase',
                ],
            ],
            [
                'value' => 'material-icons phonelink_lock',
                'label' => 'phonelink_lock',
                'attributes' => [
                    'data-icon' => 'material-icons phonelink_lock',
                    'data-icon-value' => 'phonelink_lock',
                ],
            ],
            [
                'value' => 'material-icons phonelink_off',
                'label' => 'phonelink_off',
                'attributes' => [
                    'data-icon' => 'material-icons phonelink_off',
                    'data-icon-value' => 'phonelink_off',
                ],
            ],
            [
                'value' => 'material-icons phonelink_ring',
                'label' => 'phonelink_ring',
                'attributes' => [
                    'data-icon' => 'material-icons phonelink_ring',
                    'data-icon-value' => 'phonelink_ring',
                ],
            ],
            [
                'value' => 'material-icons phonelink_setup',
                'label' => 'phonelink_setup',
                'attributes' => [
                    'data-icon' => 'material-icons phonelink_setup',
                    'data-icon-value' => 'phonelink_setup',
                ],
            ],
            [
                'value' => 'material-icons photo',
                'label' => 'photo',
                'attributes' => [
                    'data-icon' => 'material-icons photo',
                    'data-icon-value' => 'photo',
                ],
            ],
            [
                'value' => 'material-icons photo_album',
                'label' => 'photo_album',
                'attributes' => [
                    'data-icon' => 'material-icons photo_album',
                    'data-icon-value' => 'photo_album',
                ],
            ],
            [
                'value' => 'material-icons photo_camera',
                'label' => 'photo_camera',
                'attributes' => [
                    'data-icon' => 'material-icons photo_camera',
                    'data-icon-value' => 'photo_camera',
                ],
            ],
            [
                'value' => 'material-icons photo_camera_back',
                'label' => 'photo_camera_back',
                'attributes' => [
                    'data-icon' => 'material-icons photo_camera_back',
                    'data-icon-value' => 'photo_camera_back',
                ],
            ],
            [
                'value' => 'material-icons photo_camera_front',
                'label' => 'photo_camera_front',
                'attributes' => [
                    'data-icon' => 'material-icons photo_camera_front',
                    'data-icon-value' => 'photo_camera_front',
                ],
            ],
            [
                'value' => 'material-icons photo_filter',
                'label' => 'photo_filter',
                'attributes' => [
                    'data-icon' => 'material-icons photo_filter',
                    'data-icon-value' => 'photo_filter',
                ],
            ],
            [
                'value' => 'material-icons photo_library',
                'label' => 'photo_library',
                'attributes' => [
                    'data-icon' => 'material-icons photo_library',
                    'data-icon-value' => 'photo_library',
                ],
            ],
            [
                'value' => 'material-icons photo_size_select_actual',
                'label' => 'photo_size_select_actual',
                'attributes' => [
                    'data-icon' => 'material-icons photo_size_select_actual',
                    'data-icon-value' => 'photo_size_select_actual',
                ],
            ],
            [
                'value' => 'material-icons photo_size_select_large',
                'label' => 'photo_size_select_large',
                'attributes' => [
                    'data-icon' => 'material-icons photo_size_select_large',
                    'data-icon-value' => 'photo_size_select_large',
                ],
            ],
            [
                'value' => 'material-icons photo_size_select_small',
                'label' => 'photo_size_select_small',
                'attributes' => [
                    'data-icon' => 'material-icons photo_size_select_small',
                    'data-icon-value' => 'photo_size_select_small',
                ],
            ],
            [
                'value' => 'material-icons php',
                'label' => 'php',
                'attributes' => [
                    'data-icon' => 'material-icons php',
                    'data-icon-value' => 'php',
                ],
            ],
            [
                'value' => 'material-icons piano',
                'label' => 'piano',
                'attributes' => [
                    'data-icon' => 'material-icons piano',
                    'data-icon-value' => 'piano',
                ],
            ],
            [
                'value' => 'material-icons piano_off',
                'label' => 'piano_off',
                'attributes' => [
                    'data-icon' => 'material-icons piano_off',
                    'data-icon-value' => 'piano_off',
                ],
            ],
            [
                'value' => 'material-icons picture_as_pdf',
                'label' => 'picture_as_pdf',
                'attributes' => [
                    'data-icon' => 'material-icons picture_as_pdf',
                    'data-icon-value' => 'picture_as_pdf',
                ],
            ],
            [
                'value' => 'material-icons picture_in_picture',
                'label' => 'picture_in_picture',
                'attributes' => [
                    'data-icon' => 'material-icons picture_in_picture',
                    'data-icon-value' => 'picture_in_picture',
                ],
            ],
            [
                'value' => 'material-icons picture_in_picture_alt',
                'label' => 'picture_in_picture_alt',
                'attributes' => [
                    'data-icon' => 'material-icons picture_in_picture_alt',
                    'data-icon-value' => 'picture_in_picture_alt',
                ],
            ],
            [
                'value' => 'material-icons pie_chart',
                'label' => 'pie_chart',
                'attributes' => [
                    'data-icon' => 'material-icons pie_chart',
                    'data-icon-value' => 'pie_chart',
                ],
            ],
            [
                'value' => 'material-icons pie_chart_outline',
                'label' => 'pie_chart_outline',
                'attributes' => [
                    'data-icon' => 'material-icons pie_chart_outline',
                    'data-icon-value' => 'pie_chart_outline',
                ],
            ],
            [
                'value' => 'material-icons pie_chart_outlined',
                'label' => 'pie_chart_outlined',
                'attributes' => [
                    'data-icon' => 'material-icons pie_chart_outlined',
                    'data-icon-value' => 'pie_chart_outlined',
                ],
            ],
            [
                'value' => 'material-icons pin',
                'label' => 'pin',
                'attributes' => [
                    'data-icon' => 'material-icons pin',
                    'data-icon-value' => 'pin',
                ],
            ],
            [
                'value' => 'material-icons pin_drop',
                'label' => 'pin_drop',
                'attributes' => [
                    'data-icon' => 'material-icons pin_drop',
                    'data-icon-value' => 'pin_drop',
                ],
            ],
            [
                'value' => 'material-icons pin_end',
                'label' => 'pin_end',
                'attributes' => [
                    'data-icon' => 'material-icons pin_end',
                    'data-icon-value' => 'pin_end',
                ],
            ],
            [
                'value' => 'material-icons pin_invoke',
                'label' => 'pin_invoke',
                'attributes' => [
                    'data-icon' => 'material-icons pin_invoke',
                    'data-icon-value' => 'pin_invoke',
                ],
            ],
            [
                'value' => 'material-icons pinch',
                'label' => 'pinch',
                'attributes' => [
                    'data-icon' => 'material-icons pinch',
                    'data-icon-value' => 'pinch',
                ],
            ],
            [
                'value' => 'material-icons pivot_table_chart',
                'label' => 'pivot_table_chart',
                'attributes' => [
                    'data-icon' => 'material-icons pivot_table_chart',
                    'data-icon-value' => 'pivot_table_chart',
                ],
            ],
            [
                'value' => 'material-icons pix',
                'label' => 'pix',
                'attributes' => [
                    'data-icon' => 'material-icons pix',
                    'data-icon-value' => 'pix',
                ],
            ],
            [
                'value' => 'material-icons place',
                'label' => 'place',
                'attributes' => [
                    'data-icon' => 'material-icons place',
                    'data-icon-value' => 'place',
                ],
            ],
            [
                'value' => 'material-icons plagiarism',
                'label' => 'plagiarism',
                'attributes' => [
                    'data-icon' => 'material-icons plagiarism',
                    'data-icon-value' => 'plagiarism',
                ],
            ],
            [
                'value' => 'material-icons play_arrow',
                'label' => 'play_arrow',
                'attributes' => [
                    'data-icon' => 'material-icons play_arrow',
                    'data-icon-value' => 'play_arrow',
                ],
            ],
            [
                'value' => 'material-icons play_circle',
                'label' => 'play_circle',
                'attributes' => [
                    'data-icon' => 'material-icons play_circle',
                    'data-icon-value' => 'play_circle',
                ],
            ],
            [
                'value' => 'material-icons play_circle_fill',
                'label' => 'play_circle_fill',
                'attributes' => [
                    'data-icon' => 'material-icons play_circle_fill',
                    'data-icon-value' => 'play_circle_fill',
                ],
            ],
            [
                'value' => 'material-icons play_circle_filled',
                'label' => 'play_circle_filled',
                'attributes' => [
                    'data-icon' => 'material-icons play_circle_filled',
                    'data-icon-value' => 'play_circle_filled',
                ],
            ],
            [
                'value' => 'material-icons play_circle_outline',
                'label' => 'play_circle_outline',
                'attributes' => [
                    'data-icon' => 'material-icons play_circle_outline',
                    'data-icon-value' => 'play_circle_outline',
                ],
            ],
            [
                'value' => 'material-icons play_disabled',
                'label' => 'play_disabled',
                'attributes' => [
                    'data-icon' => 'material-icons play_disabled',
                    'data-icon-value' => 'play_disabled',
                ],
            ],
            [
                'value' => 'material-icons play_for_work',
                'label' => 'play_for_work',
                'attributes' => [
                    'data-icon' => 'material-icons play_for_work',
                    'data-icon-value' => 'play_for_work',
                ],
            ],
            [
                'value' => 'material-icons play_lesson',
                'label' => 'play_lesson',
                'attributes' => [
                    'data-icon' => 'material-icons play_lesson',
                    'data-icon-value' => 'play_lesson',
                ],
            ],
            [
                'value' => 'material-icons playlist_add',
                'label' => 'playlist_add',
                'attributes' => [
                    'data-icon' => 'material-icons playlist_add',
                    'data-icon-value' => 'playlist_add',
                ],
            ],
            [
                'value' => 'material-icons playlist_add_check',
                'label' => 'playlist_add_check',
                'attributes' => [
                    'data-icon' => 'material-icons playlist_add_check',
                    'data-icon-value' => 'playlist_add_check',
                ],
            ],
            [
                'value' => 'material-icons playlist_add_check_circle',
                'label' => 'playlist_add_check_circle',
                'attributes' => [
                    'data-icon' => 'material-icons playlist_add_check_circle',
                    'data-icon-value' => 'playlist_add_check_circle',
                ],
            ],
            [
                'value' => 'material-icons playlist_add_circle',
                'label' => 'playlist_add_circle',
                'attributes' => [
                    'data-icon' => 'material-icons playlist_add_circle',
                    'data-icon-value' => 'playlist_add_circle',
                ],
            ],
            [
                'value' => 'material-icons playlist_play',
                'label' => 'playlist_play',
                'attributes' => [
                    'data-icon' => 'material-icons playlist_play',
                    'data-icon-value' => 'playlist_play',
                ],
            ],
            [
                'value' => 'material-icons playlist_remove',
                'label' => 'playlist_remove',
                'attributes' => [
                    'data-icon' => 'material-icons playlist_remove',
                    'data-icon-value' => 'playlist_remove',
                ],
            ],
            [
                'value' => 'material-icons plumbing',
                'label' => 'plumbing',
                'attributes' => [
                    'data-icon' => 'material-icons plumbing',
                    'data-icon-value' => 'plumbing',
                ],
            ],
            [
                'value' => 'material-icons plus_one',
                'label' => 'plus_one',
                'attributes' => [
                    'data-icon' => 'material-icons plus_one',
                    'data-icon-value' => 'plus_one',
                ],
            ],
            [
                'value' => 'material-icons podcasts',
                'label' => 'podcasts',
                'attributes' => [
                    'data-icon' => 'material-icons podcasts',
                    'data-icon-value' => 'podcasts',
                ],
            ],
            [
                'value' => 'material-icons point_of_sale',
                'label' => 'point_of_sale',
                'attributes' => [
                    'data-icon' => 'material-icons point_of_sale',
                    'data-icon-value' => 'point_of_sale',
                ],
            ],
            [
                'value' => 'material-icons policy',
                'label' => 'policy',
                'attributes' => [
                    'data-icon' => 'material-icons policy',
                    'data-icon-value' => 'policy',
                ],
            ],
            [
                'value' => 'material-icons poll',
                'label' => 'poll',
                'attributes' => [
                    'data-icon' => 'material-icons poll',
                    'data-icon-value' => 'poll',
                ],
            ],
            [
                'value' => 'material-icons polyline',
                'label' => 'polyline',
                'attributes' => [
                    'data-icon' => 'material-icons polyline',
                    'data-icon-value' => 'polyline',
                ],
            ],
            [
                'value' => 'material-icons polymer',
                'label' => 'polymer',
                'attributes' => [
                    'data-icon' => 'material-icons polymer',
                    'data-icon-value' => 'polymer',
                ],
            ],
            [
                'value' => 'material-icons pool',
                'label' => 'pool',
                'attributes' => [
                    'data-icon' => 'material-icons pool',
                    'data-icon-value' => 'pool',
                ],
            ],
            [
                'value' => 'material-icons portable_wifi_off',
                'label' => 'portable_wifi_off',
                'attributes' => [
                    'data-icon' => 'material-icons portable_wifi_off',
                    'data-icon-value' => 'portable_wifi_off',
                ],
            ],
            [
                'value' => 'material-icons portrait',
                'label' => 'portrait',
                'attributes' => [
                    'data-icon' => 'material-icons portrait',
                    'data-icon-value' => 'portrait',
                ],
            ],
            [
                'value' => 'material-icons post_add',
                'label' => 'post_add',
                'attributes' => [
                    'data-icon' => 'material-icons post_add',
                    'data-icon-value' => 'post_add',
                ],
            ],
            [
                'value' => 'material-icons power',
                'label' => 'power',
                'attributes' => [
                    'data-icon' => 'material-icons power',
                    'data-icon-value' => 'power',
                ],
            ],
            [
                'value' => 'material-icons power_input',
                'label' => 'power_input',
                'attributes' => [
                    'data-icon' => 'material-icons power_input',
                    'data-icon-value' => 'power_input',
                ],
            ],
            [
                'value' => 'material-icons power_off',
                'label' => 'power_off',
                'attributes' => [
                    'data-icon' => 'material-icons power_off',
                    'data-icon-value' => 'power_off',
                ],
            ],
            [
                'value' => 'material-icons power_settings_new',
                'label' => 'power_settings_new',
                'attributes' => [
                    'data-icon' => 'material-icons power_settings_new',
                    'data-icon-value' => 'power_settings_new',
                ],
            ],
            [
                'value' => 'material-icons precision_manufacturing',
                'label' => 'precision_manufacturing',
                'attributes' => [
                    'data-icon' => 'material-icons precision_manufacturing',
                    'data-icon-value' => 'precision_manufacturing',
                ],
            ],
            [
                'value' => 'material-icons pregnant_woman',
                'label' => 'pregnant_woman',
                'attributes' => [
                    'data-icon' => 'material-icons pregnant_woman',
                    'data-icon-value' => 'pregnant_woman',
                ],
            ],
            [
                'value' => 'material-icons present_to_all',
                'label' => 'present_to_all',
                'attributes' => [
                    'data-icon' => 'material-icons present_to_all',
                    'data-icon-value' => 'present_to_all',
                ],
            ],
            [
                'value' => 'material-icons preview',
                'label' => 'preview',
                'attributes' => [
                    'data-icon' => 'material-icons preview',
                    'data-icon-value' => 'preview',
                ],
            ],
            [
                'value' => 'material-icons price_change',
                'label' => 'price_change',
                'attributes' => [
                    'data-icon' => 'material-icons price_change',
                    'data-icon-value' => 'price_change',
                ],
            ],
            [
                'value' => 'material-icons price_check',
                'label' => 'price_check',
                'attributes' => [
                    'data-icon' => 'material-icons price_check',
                    'data-icon-value' => 'price_check',
                ],
            ],
            [
                'value' => 'material-icons print',
                'label' => 'print',
                'attributes' => [
                    'data-icon' => 'material-icons print',
                    'data-icon-value' => 'print',
                ],
            ],
            [
                'value' => 'material-icons print_disabled',
                'label' => 'print_disabled',
                'attributes' => [
                    'data-icon' => 'material-icons print_disabled',
                    'data-icon-value' => 'print_disabled',
                ],
            ],
            [
                'value' => 'material-icons priority_high',
                'label' => 'priority_high',
                'attributes' => [
                    'data-icon' => 'material-icons priority_high',
                    'data-icon-value' => 'priority_high',
                ],
            ],
            [
                'value' => 'material-icons privacy_tip',
                'label' => 'privacy_tip',
                'attributes' => [
                    'data-icon' => 'material-icons privacy_tip',
                    'data-icon-value' => 'privacy_tip',
                ],
            ],
            [
                'value' => 'material-icons private_connectivity',
                'label' => 'private_connectivity',
                'attributes' => [
                    'data-icon' => 'material-icons private_connectivity',
                    'data-icon-value' => 'private_connectivity',
                ],
            ],
            [
                'value' => 'material-icons production_quantity_limits',
                'label' => 'production_quantity_limits',
                'attributes' => [
                    'data-icon' => 'material-icons production_quantity_limits',
                    'data-icon-value' => 'production_quantity_limits',
                ],
            ],
            [
                'value' => 'material-icons propane',
                'label' => 'propane',
                'attributes' => [
                    'data-icon' => 'material-icons propane',
                    'data-icon-value' => 'propane',
                ],
            ],
            [
                'value' => 'material-icons propane_tank',
                'label' => 'propane_tank',
                'attributes' => [
                    'data-icon' => 'material-icons propane_tank',
                    'data-icon-value' => 'propane_tank',
                ],
            ],
            [
                'value' => 'material-icons psychology',
                'label' => 'psychology',
                'attributes' => [
                    'data-icon' => 'material-icons psychology',
                    'data-icon-value' => 'psychology',
                ],
            ],
            [
                'value' => 'material-icons public',
                'label' => 'public',
                'attributes' => [
                    'data-icon' => 'material-icons public',
                    'data-icon-value' => 'public',
                ],
            ],
            [
                'value' => 'material-icons public_off',
                'label' => 'public_off',
                'attributes' => [
                    'data-icon' => 'material-icons public_off',
                    'data-icon-value' => 'public_off',
                ],
            ],
            [
                'value' => 'material-icons publish',
                'label' => 'publish',
                'attributes' => [
                    'data-icon' => 'material-icons publish',
                    'data-icon-value' => 'publish',
                ],
            ],
            [
                'value' => 'material-icons published_with_changes',
                'label' => 'published_with_changes',
                'attributes' => [
                    'data-icon' => 'material-icons published_with_changes',
                    'data-icon-value' => 'published_with_changes',
                ],
            ],
            [
                'value' => 'material-icons punch_clock',
                'label' => 'punch_clock',
                'attributes' => [
                    'data-icon' => 'material-icons punch_clock',
                    'data-icon-value' => 'punch_clock',
                ],
            ],
            [
                'value' => 'material-icons push_pin',
                'label' => 'push_pin',
                'attributes' => [
                    'data-icon' => 'material-icons push_pin',
                    'data-icon-value' => 'push_pin',
                ],
            ],
            [
                'value' => 'material-icons qr_code',
                'label' => 'qr_code',
                'attributes' => [
                    'data-icon' => 'material-icons qr_code',
                    'data-icon-value' => 'qr_code',
                ],
            ],
            [
                'value' => 'material-icons qr_code_2',
                'label' => 'qr_code_2',
                'attributes' => [
                    'data-icon' => 'material-icons qr_code_2',
                    'data-icon-value' => 'qr_code_2',
                ],
            ],
            [
                'value' => 'material-icons qr_code_scanner',
                'label' => 'qr_code_scanner',
                'attributes' => [
                    'data-icon' => 'material-icons qr_code_scanner',
                    'data-icon-value' => 'qr_code_scanner',
                ],
            ],
            [
                'value' => 'material-icons query_builder',
                'label' => 'query_builder',
                'attributes' => [
                    'data-icon' => 'material-icons query_builder',
                    'data-icon-value' => 'query_builder',
                ],
            ],
            [
                'value' => 'material-icons query_stats',
                'label' => 'query_stats',
                'attributes' => [
                    'data-icon' => 'material-icons query_stats',
                    'data-icon-value' => 'query_stats',
                ],
            ],
            [
                'value' => 'material-icons question_answer',
                'label' => 'question_answer',
                'attributes' => [
                    'data-icon' => 'material-icons question_answer',
                    'data-icon-value' => 'question_answer',
                ],
            ],
            [
                'value' => 'material-icons question_mark',
                'label' => 'question_mark',
                'attributes' => [
                    'data-icon' => 'material-icons question_mark',
                    'data-icon-value' => 'question_mark',
                ],
            ],
            [
                'value' => 'material-icons queue',
                'label' => 'queue',
                'attributes' => [
                    'data-icon' => 'material-icons queue',
                    'data-icon-value' => 'queue',
                ],
            ],
            [
                'value' => 'material-icons queue_music',
                'label' => 'queue_music',
                'attributes' => [
                    'data-icon' => 'material-icons queue_music',
                    'data-icon-value' => 'queue_music',
                ],
            ],
            [
                'value' => 'material-icons queue_play_next',
                'label' => 'queue_play_next',
                'attributes' => [
                    'data-icon' => 'material-icons queue_play_next',
                    'data-icon-value' => 'queue_play_next',
                ],
            ],
            [
                'value' => 'material-icons quick_contacts_dialer',
                'label' => 'quick_contacts_dialer',
                'attributes' => [
                    'data-icon' => 'material-icons quick_contacts_dialer',
                    'data-icon-value' => 'quick_contacts_dialer',
                ],
            ],
            [
                'value' => 'material-icons quick_contacts_mail',
                'label' => 'quick_contacts_mail',
                'attributes' => [
                    'data-icon' => 'material-icons quick_contacts_mail',
                    'data-icon-value' => 'quick_contacts_mail',
                ],
            ],
            [
                'value' => 'material-icons quickreply',
                'label' => 'quickreply',
                'attributes' => [
                    'data-icon' => 'material-icons quickreply',
                    'data-icon-value' => 'quickreply',
                ],
            ],
            [
                'value' => 'material-icons quiz',
                'label' => 'quiz',
                'attributes' => [
                    'data-icon' => 'material-icons quiz',
                    'data-icon-value' => 'quiz',
                ],
            ],
            [
                'value' => 'material-icons quora',
                'label' => 'quora',
                'attributes' => [
                    'data-icon' => 'material-icons quora',
                    'data-icon-value' => 'quora',
                ],
            ],
            [
                'value' => 'material-icons r_mobiledata',
                'label' => 'r_mobiledata',
                'attributes' => [
                    'data-icon' => 'material-icons r_mobiledata',
                    'data-icon-value' => 'r_mobiledata',
                ],
            ],
            [
                'value' => 'material-icons radar',
                'label' => 'radar',
                'attributes' => [
                    'data-icon' => 'material-icons radar',
                    'data-icon-value' => 'radar',
                ],
            ],
            [
                'value' => 'material-icons radio',
                'label' => 'radio',
                'attributes' => [
                    'data-icon' => 'material-icons radio',
                    'data-icon-value' => 'radio',
                ],
            ],
            [
                'value' => 'material-icons radio_button_checked',
                'label' => 'radio_button_checked',
                'attributes' => [
                    'data-icon' => 'material-icons radio_button_checked',
                    'data-icon-value' => 'radio_button_checked',
                ],
            ],
            [
                'value' => 'material-icons radio_button_off',
                'label' => 'radio_button_off',
                'attributes' => [
                    'data-icon' => 'material-icons radio_button_off',
                    'data-icon-value' => 'radio_button_off',
                ],
            ],
            [
                'value' => 'material-icons radio_button_on',
                'label' => 'radio_button_on',
                'attributes' => [
                    'data-icon' => 'material-icons radio_button_on',
                    'data-icon-value' => 'radio_button_on',
                ],
            ],
            [
                'value' => 'material-icons radio_button_unchecked',
                'label' => 'radio_button_unchecked',
                'attributes' => [
                    'data-icon' => 'material-icons radio_button_unchecked',
                    'data-icon-value' => 'radio_button_unchecked',
                ],
            ],
            [
                'value' => 'material-icons railway_alert',
                'label' => 'railway_alert',
                'attributes' => [
                    'data-icon' => 'material-icons railway_alert',
                    'data-icon-value' => 'railway_alert',
                ],
            ],
            [
                'value' => 'material-icons ramen_dining',
                'label' => 'ramen_dining',
                'attributes' => [
                    'data-icon' => 'material-icons ramen_dining',
                    'data-icon-value' => 'ramen_dining',
                ],
            ],
            [
                'value' => 'material-icons ramp_left',
                'label' => 'ramp_left',
                'attributes' => [
                    'data-icon' => 'material-icons ramp_left',
                    'data-icon-value' => 'ramp_left',
                ],
            ],
            [
                'value' => 'material-icons ramp_right',
                'label' => 'ramp_right',
                'attributes' => [
                    'data-icon' => 'material-icons ramp_right',
                    'data-icon-value' => 'ramp_right',
                ],
            ],
            [
                'value' => 'material-icons rate_review',
                'label' => 'rate_review',
                'attributes' => [
                    'data-icon' => 'material-icons rate_review',
                    'data-icon-value' => 'rate_review',
                ],
            ],
            [
                'value' => 'material-icons raw_off',
                'label' => 'raw_off',
                'attributes' => [
                    'data-icon' => 'material-icons raw_off',
                    'data-icon-value' => 'raw_off',
                ],
            ],
            [
                'value' => 'material-icons raw_on',
                'label' => 'raw_on',
                'attributes' => [
                    'data-icon' => 'material-icons raw_on',
                    'data-icon-value' => 'raw_on',
                ],
            ],
            [
                'value' => 'material-icons read_more',
                'label' => 'read_more',
                'attributes' => [
                    'data-icon' => 'material-icons read_more',
                    'data-icon-value' => 'read_more',
                ],
            ],
            [
                'value' => 'material-icons real_estate_agent',
                'label' => 'real_estate_agent',
                'attributes' => [
                    'data-icon' => 'material-icons real_estate_agent',
                    'data-icon-value' => 'real_estate_agent',
                ],
            ],
            [
                'value' => 'material-icons receipt',
                'label' => 'receipt',
                'attributes' => [
                    'data-icon' => 'material-icons receipt',
                    'data-icon-value' => 'receipt',
                ],
            ],
            [
                'value' => 'material-icons receipt_long',
                'label' => 'receipt_long',
                'attributes' => [
                    'data-icon' => 'material-icons receipt_long',
                    'data-icon-value' => 'receipt_long',
                ],
            ],
            [
                'value' => 'material-icons recent_actors',
                'label' => 'recent_actors',
                'attributes' => [
                    'data-icon' => 'material-icons recent_actors',
                    'data-icon-value' => 'recent_actors',
                ],
            ],
            [
                'value' => 'material-icons recommend',
                'label' => 'recommend',
                'attributes' => [
                    'data-icon' => 'material-icons recommend',
                    'data-icon-value' => 'recommend',
                ],
            ],
            [
                'value' => 'material-icons record_voice_over',
                'label' => 'record_voice_over',
                'attributes' => [
                    'data-icon' => 'material-icons record_voice_over',
                    'data-icon-value' => 'record_voice_over',
                ],
            ],
            [
                'value' => 'material-icons rectangle',
                'label' => 'rectangle',
                'attributes' => [
                    'data-icon' => 'material-icons rectangle',
                    'data-icon-value' => 'rectangle',
                ],
            ],
            [
                'value' => 'material-icons recycling',
                'label' => 'recycling',
                'attributes' => [
                    'data-icon' => 'material-icons recycling',
                    'data-icon-value' => 'recycling',
                ],
            ],
            [
                'value' => 'material-icons reddit',
                'label' => 'reddit',
                'attributes' => [
                    'data-icon' => 'material-icons reddit',
                    'data-icon-value' => 'reddit',
                ],
            ],
            [
                'value' => 'material-icons redeem',
                'label' => 'redeem',
                'attributes' => [
                    'data-icon' => 'material-icons redeem',
                    'data-icon-value' => 'redeem',
                ],
            ],
            [
                'value' => 'material-icons redo',
                'label' => 'redo',
                'attributes' => [
                    'data-icon' => 'material-icons redo',
                    'data-icon-value' => 'redo',
                ],
            ],
            [
                'value' => 'material-icons reduce_capacity',
                'label' => 'reduce_capacity',
                'attributes' => [
                    'data-icon' => 'material-icons reduce_capacity',
                    'data-icon-value' => 'reduce_capacity',
                ],
            ],
            [
                'value' => 'material-icons refresh',
                'label' => 'refresh',
                'attributes' => [
                    'data-icon' => 'material-icons refresh',
                    'data-icon-value' => 'refresh',
                ],
            ],
            [
                'value' => 'material-icons remember_me',
                'label' => 'remember_me',
                'attributes' => [
                    'data-icon' => 'material-icons remember_me',
                    'data-icon-value' => 'remember_me',
                ],
            ],
            [
                'value' => 'material-icons remove',
                'label' => 'remove',
                'attributes' => [
                    'data-icon' => 'material-icons remove',
                    'data-icon-value' => 'remove',
                ],
            ],
            [
                'value' => 'material-icons remove_circle',
                'label' => 'remove_circle',
                'attributes' => [
                    'data-icon' => 'material-icons remove_circle',
                    'data-icon-value' => 'remove_circle',
                ],
            ],
            [
                'value' => 'material-icons remove_circle_outline',
                'label' => 'remove_circle_outline',
                'attributes' => [
                    'data-icon' => 'material-icons remove_circle_outline',
                    'data-icon-value' => 'remove_circle_outline',
                ],
            ],
            [
                'value' => 'material-icons remove_done',
                'label' => 'remove_done',
                'attributes' => [
                    'data-icon' => 'material-icons remove_done',
                    'data-icon-value' => 'remove_done',
                ],
            ],
            [
                'value' => 'material-icons remove_from_queue',
                'label' => 'remove_from_queue',
                'attributes' => [
                    'data-icon' => 'material-icons remove_from_queue',
                    'data-icon-value' => 'remove_from_queue',
                ],
            ],
            [
                'value' => 'material-icons remove_moderator',
                'label' => 'remove_moderator',
                'attributes' => [
                    'data-icon' => 'material-icons remove_moderator',
                    'data-icon-value' => 'remove_moderator',
                ],
            ],
            [
                'value' => 'material-icons remove_red_eye',
                'label' => 'remove_red_eye',
                'attributes' => [
                    'data-icon' => 'material-icons remove_red_eye',
                    'data-icon-value' => 'remove_red_eye',
                ],
            ],
            [
                'value' => 'material-icons remove_road',
                'label' => 'remove_road',
                'attributes' => [
                    'data-icon' => 'material-icons remove_road',
                    'data-icon-value' => 'remove_road',
                ],
            ],
            [
                'value' => 'material-icons remove_shopping_cart',
                'label' => 'remove_shopping_cart',
                'attributes' => [
                    'data-icon' => 'material-icons remove_shopping_cart',
                    'data-icon-value' => 'remove_shopping_cart',
                ],
            ],
            [
                'value' => 'material-icons reorder',
                'label' => 'reorder',
                'attributes' => [
                    'data-icon' => 'material-icons reorder',
                    'data-icon-value' => 'reorder',
                ],
            ],
            [
                'value' => 'material-icons repeat',
                'label' => 'repeat',
                'attributes' => [
                    'data-icon' => 'material-icons repeat',
                    'data-icon-value' => 'repeat',
                ],
            ],
            [
                'value' => 'material-icons repeat_on',
                'label' => 'repeat_on',
                'attributes' => [
                    'data-icon' => 'material-icons repeat_on',
                    'data-icon-value' => 'repeat_on',
                ],
            ],
            [
                'value' => 'material-icons repeat_one',
                'label' => 'repeat_one',
                'attributes' => [
                    'data-icon' => 'material-icons repeat_one',
                    'data-icon-value' => 'repeat_one',
                ],
            ],
            [
                'value' => 'material-icons repeat_one_on',
                'label' => 'repeat_one_on',
                'attributes' => [
                    'data-icon' => 'material-icons repeat_one_on',
                    'data-icon-value' => 'repeat_one_on',
                ],
            ],
            [
                'value' => 'material-icons replay',
                'label' => 'replay',
                'attributes' => [
                    'data-icon' => 'material-icons replay',
                    'data-icon-value' => 'replay',
                ],
            ],
            [
                'value' => 'material-icons replay_10',
                'label' => 'replay_10',
                'attributes' => [
                    'data-icon' => 'material-icons replay_10',
                    'data-icon-value' => 'replay_10',
                ],
            ],
            [
                'value' => 'material-icons replay_30',
                'label' => 'replay_30',
                'attributes' => [
                    'data-icon' => 'material-icons replay_30',
                    'data-icon-value' => 'replay_30',
                ],
            ],
            [
                'value' => 'material-icons replay_5',
                'label' => 'replay_5',
                'attributes' => [
                    'data-icon' => 'material-icons replay_5',
                    'data-icon-value' => 'replay_5',
                ],
            ],
            [
                'value' => 'material-icons replay_circle_filled',
                'label' => 'replay_circle_filled',
                'attributes' => [
                    'data-icon' => 'material-icons replay_circle_filled',
                    'data-icon-value' => 'replay_circle_filled',
                ],
            ],
            [
                'value' => 'material-icons reply',
                'label' => 'reply',
                'attributes' => [
                    'data-icon' => 'material-icons reply',
                    'data-icon-value' => 'reply',
                ],
            ],
            [
                'value' => 'material-icons reply_all',
                'label' => 'reply_all',
                'attributes' => [
                    'data-icon' => 'material-icons reply_all',
                    'data-icon-value' => 'reply_all',
                ],
            ],
            [
                'value' => 'material-icons report',
                'label' => 'report',
                'attributes' => [
                    'data-icon' => 'material-icons report',
                    'data-icon-value' => 'report',
                ],
            ],
            [
                'value' => 'material-icons report_gmailerrorred',
                'label' => 'report_gmailerrorred',
                'attributes' => [
                    'data-icon' => 'material-icons report_gmailerrorred',
                    'data-icon-value' => 'report_gmailerrorred',
                ],
            ],
            [
                'value' => 'material-icons report_off',
                'label' => 'report_off',
                'attributes' => [
                    'data-icon' => 'material-icons report_off',
                    'data-icon-value' => 'report_off',
                ],
            ],
            [
                'value' => 'material-icons report_problem',
                'label' => 'report_problem',
                'attributes' => [
                    'data-icon' => 'material-icons report_problem',
                    'data-icon-value' => 'report_problem',
                ],
            ],
            [
                'value' => 'material-icons request_page',
                'label' => 'request_page',
                'attributes' => [
                    'data-icon' => 'material-icons request_page',
                    'data-icon-value' => 'request_page',
                ],
            ],
            [
                'value' => 'material-icons request_quote',
                'label' => 'request_quote',
                'attributes' => [
                    'data-icon' => 'material-icons request_quote',
                    'data-icon-value' => 'request_quote',
                ],
            ],
            [
                'value' => 'material-icons reset_tv',
                'label' => 'reset_tv',
                'attributes' => [
                    'data-icon' => 'material-icons reset_tv',
                    'data-icon-value' => 'reset_tv',
                ],
            ],
            [
                'value' => 'material-icons restart_alt',
                'label' => 'restart_alt',
                'attributes' => [
                    'data-icon' => 'material-icons restart_alt',
                    'data-icon-value' => 'restart_alt',
                ],
            ],
            [
                'value' => 'material-icons restaurant',
                'label' => 'restaurant',
                'attributes' => [
                    'data-icon' => 'material-icons restaurant',
                    'data-icon-value' => 'restaurant',
                ],
            ],
            [
                'value' => 'material-icons restaurant_menu',
                'label' => 'restaurant_menu',
                'attributes' => [
                    'data-icon' => 'material-icons restaurant_menu',
                    'data-icon-value' => 'restaurant_menu',
                ],
            ],
            [
                'value' => 'material-icons restore',
                'label' => 'restore',
                'attributes' => [
                    'data-icon' => 'material-icons restore',
                    'data-icon-value' => 'restore',
                ],
            ],
            [
                'value' => 'material-icons restore_from_trash',
                'label' => 'restore_from_trash',
                'attributes' => [
                    'data-icon' => 'material-icons restore_from_trash',
                    'data-icon-value' => 'restore_from_trash',
                ],
            ],
            [
                'value' => 'material-icons restore_page',
                'label' => 'restore_page',
                'attributes' => [
                    'data-icon' => 'material-icons restore_page',
                    'data-icon-value' => 'restore_page',
                ],
            ],
            [
                'value' => 'material-icons reviews',
                'label' => 'reviews',
                'attributes' => [
                    'data-icon' => 'material-icons reviews',
                    'data-icon-value' => 'reviews',
                ],
            ],
            [
                'value' => 'material-icons rice_bowl',
                'label' => 'rice_bowl',
                'attributes' => [
                    'data-icon' => 'material-icons rice_bowl',
                    'data-icon-value' => 'rice_bowl',
                ],
            ],
            [
                'value' => 'material-icons ring_volume',
                'label' => 'ring_volume',
                'attributes' => [
                    'data-icon' => 'material-icons ring_volume',
                    'data-icon-value' => 'ring_volume',
                ],
            ],
            [
                'value' => 'material-icons rocket',
                'label' => 'rocket',
                'attributes' => [
                    'data-icon' => 'material-icons rocket',
                    'data-icon-value' => 'rocket',
                ],
            ],
            [
                'value' => 'material-icons rocket_launch',
                'label' => 'rocket_launch',
                'attributes' => [
                    'data-icon' => 'material-icons rocket_launch',
                    'data-icon-value' => 'rocket_launch',
                ],
            ],
            [
                'value' => 'material-icons roller_shades',
                'label' => 'roller_shades',
                'attributes' => [
                    'data-icon' => 'material-icons roller_shades',
                    'data-icon-value' => 'roller_shades',
                ],
            ],
            [
                'value' => 'material-icons roller_shades_closed',
                'label' => 'roller_shades_closed',
                'attributes' => [
                    'data-icon' => 'material-icons roller_shades_closed',
                    'data-icon-value' => 'roller_shades_closed',
                ],
            ],
            [
                'value' => 'material-icons roller_skating',
                'label' => 'roller_skating',
                'attributes' => [
                    'data-icon' => 'material-icons roller_skating',
                    'data-icon-value' => 'roller_skating',
                ],
            ],
            [
                'value' => 'material-icons roofing',
                'label' => 'roofing',
                'attributes' => [
                    'data-icon' => 'material-icons roofing',
                    'data-icon-value' => 'roofing',
                ],
            ],
            [
                'value' => 'material-icons room',
                'label' => 'room',
                'attributes' => [
                    'data-icon' => 'material-icons room',
                    'data-icon-value' => 'room',
                ],
            ],
            [
                'value' => 'material-icons room_preferences',
                'label' => 'room_preferences',
                'attributes' => [
                    'data-icon' => 'material-icons room_preferences',
                    'data-icon-value' => 'room_preferences',
                ],
            ],
            [
                'value' => 'material-icons room_service',
                'label' => 'room_service',
                'attributes' => [
                    'data-icon' => 'material-icons room_service',
                    'data-icon-value' => 'room_service',
                ],
            ],
            [
                'value' => 'material-icons rotate_90_degrees_ccw',
                'label' => 'rotate_90_degrees_ccw',
                'attributes' => [
                    'data-icon' => 'material-icons rotate_90_degrees_ccw',
                    'data-icon-value' => 'rotate_90_degrees_ccw',
                ],
            ],
            [
                'value' => 'material-icons rotate_90_degrees_cw',
                'label' => 'rotate_90_degrees_cw',
                'attributes' => [
                    'data-icon' => 'material-icons rotate_90_degrees_cw',
                    'data-icon-value' => 'rotate_90_degrees_cw',
                ],
            ],
            [
                'value' => 'material-icons rotate_left',
                'label' => 'rotate_left',
                'attributes' => [
                    'data-icon' => 'material-icons rotate_left',
                    'data-icon-value' => 'rotate_left',
                ],
            ],
            [
                'value' => 'material-icons rotate_right',
                'label' => 'rotate_right',
                'attributes' => [
                    'data-icon' => 'material-icons rotate_right',
                    'data-icon-value' => 'rotate_right',
                ],
            ],
            [
                'value' => 'material-icons roundabout_left',
                'label' => 'roundabout_left',
                'attributes' => [
                    'data-icon' => 'material-icons roundabout_left',
                    'data-icon-value' => 'roundabout_left',
                ],
            ],
            [
                'value' => 'material-icons roundabout_right',
                'label' => 'roundabout_right',
                'attributes' => [
                    'data-icon' => 'material-icons roundabout_right',
                    'data-icon-value' => 'roundabout_right',
                ],
            ],
            [
                'value' => 'material-icons rounded_corner',
                'label' => 'rounded_corner',
                'attributes' => [
                    'data-icon' => 'material-icons rounded_corner',
                    'data-icon-value' => 'rounded_corner',
                ],
            ],
            [
                'value' => 'material-icons route',
                'label' => 'route',
                'attributes' => [
                    'data-icon' => 'material-icons route',
                    'data-icon-value' => 'route',
                ],
            ],
            [
                'value' => 'material-icons router',
                'label' => 'router',
                'attributes' => [
                    'data-icon' => 'material-icons router',
                    'data-icon-value' => 'router',
                ],
            ],
            [
                'value' => 'material-icons rowing',
                'label' => 'rowing',
                'attributes' => [
                    'data-icon' => 'material-icons rowing',
                    'data-icon-value' => 'rowing',
                ],
            ],
            [
                'value' => 'material-icons rss_feed',
                'label' => 'rss_feed',
                'attributes' => [
                    'data-icon' => 'material-icons rss_feed',
                    'data-icon-value' => 'rss_feed',
                ],
            ],
            [
                'value' => 'material-icons rsvp',
                'label' => 'rsvp',
                'attributes' => [
                    'data-icon' => 'material-icons rsvp',
                    'data-icon-value' => 'rsvp',
                ],
            ],
            [
                'value' => 'material-icons rtt',
                'label' => 'rtt',
                'attributes' => [
                    'data-icon' => 'material-icons rtt',
                    'data-icon-value' => 'rtt',
                ],
            ],
            [
                'value' => 'material-icons rule',
                'label' => 'rule',
                'attributes' => [
                    'data-icon' => 'material-icons rule',
                    'data-icon-value' => 'rule',
                ],
            ],
            [
                'value' => 'material-icons rule_folder',
                'label' => 'rule_folder',
                'attributes' => [
                    'data-icon' => 'material-icons rule_folder',
                    'data-icon-value' => 'rule_folder',
                ],
            ],
            [
                'value' => 'material-icons run_circle',
                'label' => 'run_circle',
                'attributes' => [
                    'data-icon' => 'material-icons run_circle',
                    'data-icon-value' => 'run_circle',
                ],
            ],
            [
                'value' => 'material-icons running_with_errors',
                'label' => 'running_with_errors',
                'attributes' => [
                    'data-icon' => 'material-icons running_with_errors',
                    'data-icon-value' => 'running_with_errors',
                ],
            ],
            [
                'value' => 'material-icons rv_hookup',
                'label' => 'rv_hookup',
                'attributes' => [
                    'data-icon' => 'material-icons rv_hookup',
                    'data-icon-value' => 'rv_hookup',
                ],
            ],
            [
                'value' => 'material-icons safety_check',
                'label' => 'safety_check',
                'attributes' => [
                    'data-icon' => 'material-icons safety_check',
                    'data-icon-value' => 'safety_check',
                ],
            ],
            [
                'value' => 'material-icons safety_divider',
                'label' => 'safety_divider',
                'attributes' => [
                    'data-icon' => 'material-icons safety_divider',
                    'data-icon-value' => 'safety_divider',
                ],
            ],
            [
                'value' => 'material-icons sailing',
                'label' => 'sailing',
                'attributes' => [
                    'data-icon' => 'material-icons sailing',
                    'data-icon-value' => 'sailing',
                ],
            ],
            [
                'value' => 'material-icons sanitizer',
                'label' => 'sanitizer',
                'attributes' => [
                    'data-icon' => 'material-icons sanitizer',
                    'data-icon-value' => 'sanitizer',
                ],
            ],
            [
                'value' => 'material-icons satellite',
                'label' => 'satellite',
                'attributes' => [
                    'data-icon' => 'material-icons satellite',
                    'data-icon-value' => 'satellite',
                ],
            ],
            [
                'value' => 'material-icons satellite_alt',
                'label' => 'satellite_alt',
                'attributes' => [
                    'data-icon' => 'material-icons satellite_alt',
                    'data-icon-value' => 'satellite_alt',
                ],
            ],
            [
                'value' => 'material-icons save',
                'label' => 'save',
                'attributes' => [
                    'data-icon' => 'material-icons save',
                    'data-icon-value' => 'save',
                ],
            ],
            [
                'value' => 'material-icons save_alt',
                'label' => 'save_alt',
                'attributes' => [
                    'data-icon' => 'material-icons save_alt',
                    'data-icon-value' => 'save_alt',
                ],
            ],
            [
                'value' => 'material-icons save_as',
                'label' => 'save_as',
                'attributes' => [
                    'data-icon' => 'material-icons save_as',
                    'data-icon-value' => 'save_as',
                ],
            ],
            [
                'value' => 'material-icons saved_search',
                'label' => 'saved_search',
                'attributes' => [
                    'data-icon' => 'material-icons saved_search',
                    'data-icon-value' => 'saved_search',
                ],
            ],
            [
                'value' => 'material-icons savings',
                'label' => 'savings',
                'attributes' => [
                    'data-icon' => 'material-icons savings',
                    'data-icon-value' => 'savings',
                ],
            ],
            [
                'value' => 'material-icons scale',
                'label' => 'scale',
                'attributes' => [
                    'data-icon' => 'material-icons scale',
                    'data-icon-value' => 'scale',
                ],
            ],
            [
                'value' => 'material-icons scanner',
                'label' => 'scanner',
                'attributes' => [
                    'data-icon' => 'material-icons scanner',
                    'data-icon-value' => 'scanner',
                ],
            ],
            [
                'value' => 'material-icons scatter_plot',
                'label' => 'scatter_plot',
                'attributes' => [
                    'data-icon' => 'material-icons scatter_plot',
                    'data-icon-value' => 'scatter_plot',
                ],
            ],
            [
                'value' => 'material-icons schedule',
                'label' => 'schedule',
                'attributes' => [
                    'data-icon' => 'material-icons schedule',
                    'data-icon-value' => 'schedule',
                ],
            ],
            [
                'value' => 'material-icons schedule_send',
                'label' => 'schedule_send',
                'attributes' => [
                    'data-icon' => 'material-icons schedule_send',
                    'data-icon-value' => 'schedule_send',
                ],
            ],
            [
                'value' => 'material-icons schema',
                'label' => 'schema',
                'attributes' => [
                    'data-icon' => 'material-icons schema',
                    'data-icon-value' => 'schema',
                ],
            ],
            [
                'value' => 'material-icons school',
                'label' => 'school',
                'attributes' => [
                    'data-icon' => 'material-icons school',
                    'data-icon-value' => 'school',
                ],
            ],
            [
                'value' => 'material-icons science',
                'label' => 'science',
                'attributes' => [
                    'data-icon' => 'material-icons science',
                    'data-icon-value' => 'science',
                ],
            ],
            [
                'value' => 'material-icons score',
                'label' => 'score',
                'attributes' => [
                    'data-icon' => 'material-icons score',
                    'data-icon-value' => 'score',
                ],
            ],
            [
                'value' => 'material-icons scoreboard',
                'label' => 'scoreboard',
                'attributes' => [
                    'data-icon' => 'material-icons scoreboard',
                    'data-icon-value' => 'scoreboard',
                ],
            ],
            [
                'value' => 'material-icons screen_lock_landscape',
                'label' => 'screen_lock_landscape',
                'attributes' => [
                    'data-icon' => 'material-icons screen_lock_landscape',
                    'data-icon-value' => 'screen_lock_landscape',
                ],
            ],
            [
                'value' => 'material-icons screen_lock_portrait',
                'label' => 'screen_lock_portrait',
                'attributes' => [
                    'data-icon' => 'material-icons screen_lock_portrait',
                    'data-icon-value' => 'screen_lock_portrait',
                ],
            ],
            [
                'value' => 'material-icons screen_lock_rotation',
                'label' => 'screen_lock_rotation',
                'attributes' => [
                    'data-icon' => 'material-icons screen_lock_rotation',
                    'data-icon-value' => 'screen_lock_rotation',
                ],
            ],
            [
                'value' => 'material-icons screen_rotation',
                'label' => 'screen_rotation',
                'attributes' => [
                    'data-icon' => 'material-icons screen_rotation',
                    'data-icon-value' => 'screen_rotation',
                ],
            ],
            [
                'value' => 'material-icons screen_rotation_alt',
                'label' => 'screen_rotation_alt',
                'attributes' => [
                    'data-icon' => 'material-icons screen_rotation_alt',
                    'data-icon-value' => 'screen_rotation_alt',
                ],
            ],
            [
                'value' => 'material-icons screen_search_desktop',
                'label' => 'screen_search_desktop',
                'attributes' => [
                    'data-icon' => 'material-icons screen_search_desktop',
                    'data-icon-value' => 'screen_search_desktop',
                ],
            ],
            [
                'value' => 'material-icons screen_share',
                'label' => 'screen_share',
                'attributes' => [
                    'data-icon' => 'material-icons screen_share',
                    'data-icon-value' => 'screen_share',
                ],
            ],
            [
                'value' => 'material-icons screenshot',
                'label' => 'screenshot',
                'attributes' => [
                    'data-icon' => 'material-icons screenshot',
                    'data-icon-value' => 'screenshot',
                ],
            ],
            [
                'value' => 'material-icons screenshot_monitor',
                'label' => 'screenshot_monitor',
                'attributes' => [
                    'data-icon' => 'material-icons screenshot_monitor',
                    'data-icon-value' => 'screenshot_monitor',
                ],
            ],
            [
                'value' => 'material-icons scuba_diving',
                'label' => 'scuba_diving',
                'attributes' => [
                    'data-icon' => 'material-icons scuba_diving',
                    'data-icon-value' => 'scuba_diving',
                ],
            ],
            [
                'value' => 'material-icons sd',
                'label' => 'sd',
                'attributes' => [
                    'data-icon' => 'material-icons sd',
                    'data-icon-value' => 'sd',
                ],
            ],
            [
                'value' => 'material-icons sd_card',
                'label' => 'sd_card',
                'attributes' => [
                    'data-icon' => 'material-icons sd_card',
                    'data-icon-value' => 'sd_card',
                ],
            ],
            [
                'value' => 'material-icons sd_card_alert',
                'label' => 'sd_card_alert',
                'attributes' => [
                    'data-icon' => 'material-icons sd_card_alert',
                    'data-icon-value' => 'sd_card_alert',
                ],
            ],
            [
                'value' => 'material-icons sd_storage',
                'label' => 'sd_storage',
                'attributes' => [
                    'data-icon' => 'material-icons sd_storage',
                    'data-icon-value' => 'sd_storage',
                ],
            ],
            [
                'value' => 'material-icons search',
                'label' => 'search',
                'attributes' => [
                    'data-icon' => 'material-icons search',
                    'data-icon-value' => 'search',
                ],
            ],
            [
                'value' => 'material-icons search_off',
                'label' => 'search_off',
                'attributes' => [
                    'data-icon' => 'material-icons search_off',
                    'data-icon-value' => 'search_off',
                ],
            ],
            [
                'value' => 'material-icons security',
                'label' => 'security',
                'attributes' => [
                    'data-icon' => 'material-icons security',
                    'data-icon-value' => 'security',
                ],
            ],
            [
                'value' => 'material-icons security_update',
                'label' => 'security_update',
                'attributes' => [
                    'data-icon' => 'material-icons security_update',
                    'data-icon-value' => 'security_update',
                ],
            ],
            [
                'value' => 'material-icons security_update_good',
                'label' => 'security_update_good',
                'attributes' => [
                    'data-icon' => 'material-icons security_update_good',
                    'data-icon-value' => 'security_update_good',
                ],
            ],
            [
                'value' => 'material-icons security_update_warning',
                'label' => 'security_update_warning',
                'attributes' => [
                    'data-icon' => 'material-icons security_update_warning',
                    'data-icon-value' => 'security_update_warning',
                ],
            ],
            [
                'value' => 'material-icons segment',
                'label' => 'segment',
                'attributes' => [
                    'data-icon' => 'material-icons segment',
                    'data-icon-value' => 'segment',
                ],
            ],
            [
                'value' => 'material-icons select_all',
                'label' => 'select_all',
                'attributes' => [
                    'data-icon' => 'material-icons select_all',
                    'data-icon-value' => 'select_all',
                ],
            ],
            [
                'value' => 'material-icons self_improvement',
                'label' => 'self_improvement',
                'attributes' => [
                    'data-icon' => 'material-icons self_improvement',
                    'data-icon-value' => 'self_improvement',
                ],
            ],
            [
                'value' => 'material-icons sell',
                'label' => 'sell',
                'attributes' => [
                    'data-icon' => 'material-icons sell',
                    'data-icon-value' => 'sell',
                ],
            ],
            [
                'value' => 'material-icons send',
                'label' => 'send',
                'attributes' => [
                    'data-icon' => 'material-icons send',
                    'data-icon-value' => 'send',
                ],
            ],
            [
                'value' => 'material-icons send_and_archive',
                'label' => 'send_and_archive',
                'attributes' => [
                    'data-icon' => 'material-icons send_and_archive',
                    'data-icon-value' => 'send_and_archive',
                ],
            ],
            [
                'value' => 'material-icons send_time_extension',
                'label' => 'send_time_extension',
                'attributes' => [
                    'data-icon' => 'material-icons send_time_extension',
                    'data-icon-value' => 'send_time_extension',
                ],
            ],
            [
                'value' => 'material-icons send_to_mobile',
                'label' => 'send_to_mobile',
                'attributes' => [
                    'data-icon' => 'material-icons send_to_mobile',
                    'data-icon-value' => 'send_to_mobile',
                ],
            ],
            [
                'value' => 'material-icons sensor_door',
                'label' => 'sensor_door',
                'attributes' => [
                    'data-icon' => 'material-icons sensor_door',
                    'data-icon-value' => 'sensor_door',
                ],
            ],
            [
                'value' => 'material-icons sensor_occupied',
                'label' => 'sensor_occupied',
                'attributes' => [
                    'data-icon' => 'material-icons sensor_occupied',
                    'data-icon-value' => 'sensor_occupied',
                ],
            ],
            [
                'value' => 'material-icons sensor_window',
                'label' => 'sensor_window',
                'attributes' => [
                    'data-icon' => 'material-icons sensor_window',
                    'data-icon-value' => 'sensor_window',
                ],
            ],
            [
                'value' => 'material-icons sensors',
                'label' => 'sensors',
                'attributes' => [
                    'data-icon' => 'material-icons sensors',
                    'data-icon-value' => 'sensors',
                ],
            ],
            [
                'value' => 'material-icons sensors_off',
                'label' => 'sensors_off',
                'attributes' => [
                    'data-icon' => 'material-icons sensors_off',
                    'data-icon-value' => 'sensors_off',
                ],
            ],
            [
                'value' => 'material-icons sentiment_dissatisfied',
                'label' => 'sentiment_dissatisfied',
                'attributes' => [
                    'data-icon' => 'material-icons sentiment_dissatisfied',
                    'data-icon-value' => 'sentiment_dissatisfied',
                ],
            ],
            [
                'value' => 'material-icons sentiment_neutral',
                'label' => 'sentiment_neutral',
                'attributes' => [
                    'data-icon' => 'material-icons sentiment_neutral',
                    'data-icon-value' => 'sentiment_neutral',
                ],
            ],
            [
                'value' => 'material-icons sentiment_satisfied',
                'label' => 'sentiment_satisfied',
                'attributes' => [
                    'data-icon' => 'material-icons sentiment_satisfied',
                    'data-icon-value' => 'sentiment_satisfied',
                ],
            ],
            [
                'value' => 'material-icons sentiment_satisfied_alt',
                'label' => 'sentiment_satisfied_alt',
                'attributes' => [
                    'data-icon' => 'material-icons sentiment_satisfied_alt',
                    'data-icon-value' => 'sentiment_satisfied_alt',
                ],
            ],
            [
                'value' => 'material-icons sentiment_very_dissatisfied',
                'label' => 'sentiment_very_dissatisfied',
                'attributes' => [
                    'data-icon' => 'material-icons sentiment_very_dissatisfied',
                    'data-icon-value' => 'sentiment_very_dissatisfied',
                ],
            ],
            [
                'value' => 'material-icons sentiment_very_satisfied',
                'label' => 'sentiment_very_satisfied',
                'attributes' => [
                    'data-icon' => 'material-icons sentiment_very_satisfied',
                    'data-icon-value' => 'sentiment_very_satisfied',
                ],
            ],
            [
                'value' => 'material-icons set_meal',
                'label' => 'set_meal',
                'attributes' => [
                    'data-icon' => 'material-icons set_meal',
                    'data-icon-value' => 'set_meal',
                ],
            ],
            [
                'value' => 'material-icons settings',
                'label' => 'settings',
                'attributes' => [
                    'data-icon' => 'material-icons settings',
                    'data-icon-value' => 'settings',
                ],
            ],
            [
                'value' => 'material-icons settings_accessibility',
                'label' => 'settings_accessibility',
                'attributes' => [
                    'data-icon' => 'material-icons settings_accessibility',
                    'data-icon-value' => 'settings_accessibility',
                ],
            ],
            [
                'value' => 'material-icons settings_applications',
                'label' => 'settings_applications',
                'attributes' => [
                    'data-icon' => 'material-icons settings_applications',
                    'data-icon-value' => 'settings_applications',
                ],
            ],
            [
                'value' => 'material-icons settings_backup_restore',
                'label' => 'settings_backup_restore',
                'attributes' => [
                    'data-icon' => 'material-icons settings_backup_restore',
                    'data-icon-value' => 'settings_backup_restore',
                ],
            ],
            [
                'value' => 'material-icons settings_bluetooth',
                'label' => 'settings_bluetooth',
                'attributes' => [
                    'data-icon' => 'material-icons settings_bluetooth',
                    'data-icon-value' => 'settings_bluetooth',
                ],
            ],
            [
                'value' => 'material-icons settings_brightness',
                'label' => 'settings_brightness',
                'attributes' => [
                    'data-icon' => 'material-icons settings_brightness',
                    'data-icon-value' => 'settings_brightness',
                ],
            ],
            [
                'value' => 'material-icons settings_cell',
                'label' => 'settings_cell',
                'attributes' => [
                    'data-icon' => 'material-icons settings_cell',
                    'data-icon-value' => 'settings_cell',
                ],
            ],
            [
                'value' => 'material-icons settings_display',
                'label' => 'settings_display',
                'attributes' => [
                    'data-icon' => 'material-icons settings_display',
                    'data-icon-value' => 'settings_display',
                ],
            ],
            [
                'value' => 'material-icons settings_ethernet',
                'label' => 'settings_ethernet',
                'attributes' => [
                    'data-icon' => 'material-icons settings_ethernet',
                    'data-icon-value' => 'settings_ethernet',
                ],
            ],
            [
                'value' => 'material-icons settings_input_antenna',
                'label' => 'settings_input_antenna',
                'attributes' => [
                    'data-icon' => 'material-icons settings_input_antenna',
                    'data-icon-value' => 'settings_input_antenna',
                ],
            ],
            [
                'value' => 'material-icons settings_input_component',
                'label' => 'settings_input_component',
                'attributes' => [
                    'data-icon' => 'material-icons settings_input_component',
                    'data-icon-value' => 'settings_input_component',
                ],
            ],
            [
                'value' => 'material-icons settings_input_composite',
                'label' => 'settings_input_composite',
                'attributes' => [
                    'data-icon' => 'material-icons settings_input_composite',
                    'data-icon-value' => 'settings_input_composite',
                ],
            ],
            [
                'value' => 'material-icons settings_input_hdmi',
                'label' => 'settings_input_hdmi',
                'attributes' => [
                    'data-icon' => 'material-icons settings_input_hdmi',
                    'data-icon-value' => 'settings_input_hdmi',
                ],
            ],
            [
                'value' => 'material-icons settings_input_svideo',
                'label' => 'settings_input_svideo',
                'attributes' => [
                    'data-icon' => 'material-icons settings_input_svideo',
                    'data-icon-value' => 'settings_input_svideo',
                ],
            ],
            [
                'value' => 'material-icons settings_overscan',
                'label' => 'settings_overscan',
                'attributes' => [
                    'data-icon' => 'material-icons settings_overscan',
                    'data-icon-value' => 'settings_overscan',
                ],
            ],
            [
                'value' => 'material-icons settings_phone',
                'label' => 'settings_phone',
                'attributes' => [
                    'data-icon' => 'material-icons settings_phone',
                    'data-icon-value' => 'settings_phone',
                ],
            ],
            [
                'value' => 'material-icons settings_power',
                'label' => 'settings_power',
                'attributes' => [
                    'data-icon' => 'material-icons settings_power',
                    'data-icon-value' => 'settings_power',
                ],
            ],
            [
                'value' => 'material-icons settings_remote',
                'label' => 'settings_remote',
                'attributes' => [
                    'data-icon' => 'material-icons settings_remote',
                    'data-icon-value' => 'settings_remote',
                ],
            ],
            [
                'value' => 'material-icons settings_suggest',
                'label' => 'settings_suggest',
                'attributes' => [
                    'data-icon' => 'material-icons settings_suggest',
                    'data-icon-value' => 'settings_suggest',
                ],
            ],
            [
                'value' => 'material-icons settings_system_daydream',
                'label' => 'settings_system_daydream',
                'attributes' => [
                    'data-icon' => 'material-icons settings_system_daydream',
                    'data-icon-value' => 'settings_system_daydream',
                ],
            ],
            [
                'value' => 'material-icons settings_voice',
                'label' => 'settings_voice',
                'attributes' => [
                    'data-icon' => 'material-icons settings_voice',
                    'data-icon-value' => 'settings_voice',
                ],
            ],
            [
                'value' => 'material-icons severe_cold',
                'label' => 'severe_cold',
                'attributes' => [
                    'data-icon' => 'material-icons severe_cold',
                    'data-icon-value' => 'severe_cold',
                ],
            ],
            [
                'value' => 'material-icons share',
                'label' => 'share',
                'attributes' => [
                    'data-icon' => 'material-icons share',
                    'data-icon-value' => 'share',
                ],
            ],
            [
                'value' => 'material-icons share_arrival_time',
                'label' => 'share_arrival_time',
                'attributes' => [
                    'data-icon' => 'material-icons share_arrival_time',
                    'data-icon-value' => 'share_arrival_time',
                ],
            ],
            [
                'value' => 'material-icons share_location',
                'label' => 'share_location',
                'attributes' => [
                    'data-icon' => 'material-icons share_location',
                    'data-icon-value' => 'share_location',
                ],
            ],
            [
                'value' => 'material-icons shield',
                'label' => 'shield',
                'attributes' => [
                    'data-icon' => 'material-icons shield',
                    'data-icon-value' => 'shield',
                ],
            ],
            [
                'value' => 'material-icons shield_moon',
                'label' => 'shield_moon',
                'attributes' => [
                    'data-icon' => 'material-icons shield_moon',
                    'data-icon-value' => 'shield_moon',
                ],
            ],
            [
                'value' => 'material-icons shop',
                'label' => 'shop',
                'attributes' => [
                    'data-icon' => 'material-icons shop',
                    'data-icon-value' => 'shop',
                ],
            ],
            [
                'value' => 'material-icons shop_2',
                'label' => 'shop_2',
                'attributes' => [
                    'data-icon' => 'material-icons shop_2',
                    'data-icon-value' => 'shop_2',
                ],
            ],
            [
                'value' => 'material-icons shop_two',
                'label' => 'shop_two',
                'attributes' => [
                    'data-icon' => 'material-icons shop_two',
                    'data-icon-value' => 'shop_two',
                ],
            ],
            [
                'value' => 'material-icons shopify',
                'label' => 'shopify',
                'attributes' => [
                    'data-icon' => 'material-icons shopify',
                    'data-icon-value' => 'shopify',
                ],
            ],
            [
                'value' => 'material-icons shopping_bag',
                'label' => 'shopping_bag',
                'attributes' => [
                    'data-icon' => 'material-icons shopping_bag',
                    'data-icon-value' => 'shopping_bag',
                ],
            ],
            [
                'value' => 'material-icons shopping_basket',
                'label' => 'shopping_basket',
                'attributes' => [
                    'data-icon' => 'material-icons shopping_basket',
                    'data-icon-value' => 'shopping_basket',
                ],
            ],
            [
                'value' => 'material-icons shopping_cart',
                'label' => 'shopping_cart',
                'attributes' => [
                    'data-icon' => 'material-icons shopping_cart',
                    'data-icon-value' => 'shopping_cart',
                ],
            ],
            [
                'value' => 'material-icons shopping_cart_checkout',
                'label' => 'shopping_cart_checkout',
                'attributes' => [
                    'data-icon' => 'material-icons shopping_cart_checkout',
                    'data-icon-value' => 'shopping_cart_checkout',
                ],
            ],
            [
                'value' => 'material-icons short_text',
                'label' => 'short_text',
                'attributes' => [
                    'data-icon' => 'material-icons short_text',
                    'data-icon-value' => 'short_text',
                ],
            ],
            [
                'value' => 'material-icons shortcut',
                'label' => 'shortcut',
                'attributes' => [
                    'data-icon' => 'material-icons shortcut',
                    'data-icon-value' => 'shortcut',
                ],
            ],
            [
                'value' => 'material-icons show_chart',
                'label' => 'show_chart',
                'attributes' => [
                    'data-icon' => 'material-icons show_chart',
                    'data-icon-value' => 'show_chart',
                ],
            ],
            [
                'value' => 'material-icons shower',
                'label' => 'shower',
                'attributes' => [
                    'data-icon' => 'material-icons shower',
                    'data-icon-value' => 'shower',
                ],
            ],
            [
                'value' => 'material-icons shuffle',
                'label' => 'shuffle',
                'attributes' => [
                    'data-icon' => 'material-icons shuffle',
                    'data-icon-value' => 'shuffle',
                ],
            ],
            [
                'value' => 'material-icons shuffle_on',
                'label' => 'shuffle_on',
                'attributes' => [
                    'data-icon' => 'material-icons shuffle_on',
                    'data-icon-value' => 'shuffle_on',
                ],
            ],
            [
                'value' => 'material-icons shutter_speed',
                'label' => 'shutter_speed',
                'attributes' => [
                    'data-icon' => 'material-icons shutter_speed',
                    'data-icon-value' => 'shutter_speed',
                ],
            ],
            [
                'value' => 'material-icons sick',
                'label' => 'sick',
                'attributes' => [
                    'data-icon' => 'material-icons sick',
                    'data-icon-value' => 'sick',
                ],
            ],
            [
                'value' => 'material-icons sign_language',
                'label' => 'sign_language',
                'attributes' => [
                    'data-icon' => 'material-icons sign_language',
                    'data-icon-value' => 'sign_language',
                ],
            ],
            [
                'value' => 'material-icons signal_cellular_0_bar',
                'label' => 'signal_cellular_0_bar',
                'attributes' => [
                    'data-icon' => 'material-icons signal_cellular_0_bar',
                    'data-icon-value' => 'signal_cellular_0_bar',
                ],
            ],
            [
                'value' => 'material-icons signal_cellular_4_bar',
                'label' => 'signal_cellular_4_bar',
                'attributes' => [
                    'data-icon' => 'material-icons signal_cellular_4_bar',
                    'data-icon-value' => 'signal_cellular_4_bar',
                ],
            ],
            [
                'value' => 'material-icons signal_cellular_alt',
                'label' => 'signal_cellular_alt',
                'attributes' => [
                    'data-icon' => 'material-icons signal_cellular_alt',
                    'data-icon-value' => 'signal_cellular_alt',
                ],
            ],
            [
                'value' => 'material-icons signal_cellular_alt_1_bar',
                'label' => 'signal_cellular_alt_1_bar',
                'attributes' => [
                    'data-icon' => 'material-icons signal_cellular_alt_1_bar',
                    'data-icon-value' => 'signal_cellular_alt_1_bar',
                ],
            ],
            [
                'value' => 'material-icons signal_cellular_alt_2_bar',
                'label' => 'signal_cellular_alt_2_bar',
                'attributes' => [
                    'data-icon' => 'material-icons signal_cellular_alt_2_bar',
                    'data-icon-value' => 'signal_cellular_alt_2_bar',
                ],
            ],
            [
                'value' => 'material-icons signal_cellular_connected_no_internet_0_bar',
                'label' => 'signal_cellular_connected_no_internet_0_bar',
                'attributes' => [
                    'data-icon' => 'material-icons signal_cellular_connected_no_internet_0_bar',
                    'data-icon-value' => 'signal_cellular_connected_no_internet_0_bar',
                ],
            ],
            [
                'value' => 'material-icons signal_cellular_connected_no_internet_4_bar',
                'label' => 'signal_cellular_connected_no_internet_4_bar',
                'attributes' => [
                    'data-icon' => 'material-icons signal_cellular_connected_no_internet_4_bar',
                    'data-icon-value' => 'signal_cellular_connected_no_internet_4_bar',
                ],
            ],
            [
                'value' => 'material-icons signal_cellular_no_sim',
                'label' => 'signal_cellular_no_sim',
                'attributes' => [
                    'data-icon' => 'material-icons signal_cellular_no_sim',
                    'data-icon-value' => 'signal_cellular_no_sim',
                ],
            ],
            [
                'value' => 'material-icons signal_cellular_nodata',
                'label' => 'signal_cellular_nodata',
                'attributes' => [
                    'data-icon' => 'material-icons signal_cellular_nodata',
                    'data-icon-value' => 'signal_cellular_nodata',
                ],
            ],
            [
                'value' => 'material-icons signal_cellular_null',
                'label' => 'signal_cellular_null',
                'attributes' => [
                    'data-icon' => 'material-icons signal_cellular_null',
                    'data-icon-value' => 'signal_cellular_null',
                ],
            ],
            [
                'value' => 'material-icons signal_cellular_off',
                'label' => 'signal_cellular_off',
                'attributes' => [
                    'data-icon' => 'material-icons signal_cellular_off',
                    'data-icon-value' => 'signal_cellular_off',
                ],
            ],
            [
                'value' => 'material-icons signal_wifi_0_bar',
                'label' => 'signal_wifi_0_bar',
                'attributes' => [
                    'data-icon' => 'material-icons signal_wifi_0_bar',
                    'data-icon-value' => 'signal_wifi_0_bar',
                ],
            ],
            [
                'value' => 'material-icons signal_wifi_4_bar',
                'label' => 'signal_wifi_4_bar',
                'attributes' => [
                    'data-icon' => 'material-icons signal_wifi_4_bar',
                    'data-icon-value' => 'signal_wifi_4_bar',
                ],
            ],
            [
                'value' => 'material-icons signal_wifi_4_bar_lock',
                'label' => 'signal_wifi_4_bar_lock',
                'attributes' => [
                    'data-icon' => 'material-icons signal_wifi_4_bar_lock',
                    'data-icon-value' => 'signal_wifi_4_bar_lock',
                ],
            ],
            [
                'value' => 'material-icons signal_wifi_bad',
                'label' => 'signal_wifi_bad',
                'attributes' => [
                    'data-icon' => 'material-icons signal_wifi_bad',
                    'data-icon-value' => 'signal_wifi_bad',
                ],
            ],
            [
                'value' => 'material-icons signal_wifi_connected_no_internet_4',
                'label' => 'signal_wifi_connected_no_internet_4',
                'attributes' => [
                    'data-icon' => 'material-icons signal_wifi_connected_no_internet_4',
                    'data-icon-value' => 'signal_wifi_connected_no_internet_4',
                ],
            ],
            [
                'value' => 'material-icons signal_wifi_off',
                'label' => 'signal_wifi_off',
                'attributes' => [
                    'data-icon' => 'material-icons signal_wifi_off',
                    'data-icon-value' => 'signal_wifi_off',
                ],
            ],
            [
                'value' => 'material-icons signal_wifi_statusbar_4_bar',
                'label' => 'signal_wifi_statusbar_4_bar',
                'attributes' => [
                    'data-icon' => 'material-icons signal_wifi_statusbar_4_bar',
                    'data-icon-value' => 'signal_wifi_statusbar_4_bar',
                ],
            ],
            [
                'value' => 'material-icons signal_wifi_statusbar_connected_no_internet_4',
                'label' => 'signal_wifi_statusbar_connected_no_internet_4',
                'attributes' => [
                    'data-icon' => 'material-icons signal_wifi_statusbar_connected_no_internet_4',
                    'data-icon-value' => 'signal_wifi_statusbar_connected_no_internet_4',
                ],
            ],
            [
                'value' => 'material-icons signal_wifi_statusbar_null',
                'label' => 'signal_wifi_statusbar_null',
                'attributes' => [
                    'data-icon' => 'material-icons signal_wifi_statusbar_null',
                    'data-icon-value' => 'signal_wifi_statusbar_null',
                ],
            ],
            [
                'value' => 'material-icons signpost',
                'label' => 'signpost',
                'attributes' => [
                    'data-icon' => 'material-icons signpost',
                    'data-icon-value' => 'signpost',
                ],
            ],
            [
                'value' => 'material-icons sim_card',
                'label' => 'sim_card',
                'attributes' => [
                    'data-icon' => 'material-icons sim_card',
                    'data-icon-value' => 'sim_card',
                ],
            ],
            [
                'value' => 'material-icons sim_card_alert',
                'label' => 'sim_card_alert',
                'attributes' => [
                    'data-icon' => 'material-icons sim_card_alert',
                    'data-icon-value' => 'sim_card_alert',
                ],
            ],
            [
                'value' => 'material-icons sim_card_download',
                'label' => 'sim_card_download',
                'attributes' => [
                    'data-icon' => 'material-icons sim_card_download',
                    'data-icon-value' => 'sim_card_download',
                ],
            ],
            [
                'value' => 'material-icons single_bed',
                'label' => 'single_bed',
                'attributes' => [
                    'data-icon' => 'material-icons single_bed',
                    'data-icon-value' => 'single_bed',
                ],
            ],
            [
                'value' => 'material-icons sip',
                'label' => 'sip',
                'attributes' => [
                    'data-icon' => 'material-icons sip',
                    'data-icon-value' => 'sip',
                ],
            ],
            [
                'value' => 'material-icons skateboarding',
                'label' => 'skateboarding',
                'attributes' => [
                    'data-icon' => 'material-icons skateboarding',
                    'data-icon-value' => 'skateboarding',
                ],
            ],
            [
                'value' => 'material-icons skip_next',
                'label' => 'skip_next',
                'attributes' => [
                    'data-icon' => 'material-icons skip_next',
                    'data-icon-value' => 'skip_next',
                ],
            ],
            [
                'value' => 'material-icons skip_previous',
                'label' => 'skip_previous',
                'attributes' => [
                    'data-icon' => 'material-icons skip_previous',
                    'data-icon-value' => 'skip_previous',
                ],
            ],
            [
                'value' => 'material-icons sledding',
                'label' => 'sledding',
                'attributes' => [
                    'data-icon' => 'material-icons sledding',
                    'data-icon-value' => 'sledding',
                ],
            ],
            [
                'value' => 'material-icons slideshow',
                'label' => 'slideshow',
                'attributes' => [
                    'data-icon' => 'material-icons slideshow',
                    'data-icon-value' => 'slideshow',
                ],
            ],
            [
                'value' => 'material-icons slow_motion_video',
                'label' => 'slow_motion_video',
                'attributes' => [
                    'data-icon' => 'material-icons slow_motion_video',
                    'data-icon-value' => 'slow_motion_video',
                ],
            ],
            [
                'value' => 'material-icons smart_button',
                'label' => 'smart_button',
                'attributes' => [
                    'data-icon' => 'material-icons smart_button',
                    'data-icon-value' => 'smart_button',
                ],
            ],
            [
                'value' => 'material-icons smart_display',
                'label' => 'smart_display',
                'attributes' => [
                    'data-icon' => 'material-icons smart_display',
                    'data-icon-value' => 'smart_display',
                ],
            ],
            [
                'value' => 'material-icons smart_screen',
                'label' => 'smart_screen',
                'attributes' => [
                    'data-icon' => 'material-icons smart_screen',
                    'data-icon-value' => 'smart_screen',
                ],
            ],
            [
                'value' => 'material-icons smart_toy',
                'label' => 'smart_toy',
                'attributes' => [
                    'data-icon' => 'material-icons smart_toy',
                    'data-icon-value' => 'smart_toy',
                ],
            ],
            [
                'value' => 'material-icons smartphone',
                'label' => 'smartphone',
                'attributes' => [
                    'data-icon' => 'material-icons smartphone',
                    'data-icon-value' => 'smartphone',
                ],
            ],
            [
                'value' => 'material-icons smoke_free',
                'label' => 'smoke_free',
                'attributes' => [
                    'data-icon' => 'material-icons smoke_free',
                    'data-icon-value' => 'smoke_free',
                ],
            ],
            [
                'value' => 'material-icons smoking_rooms',
                'label' => 'smoking_rooms',
                'attributes' => [
                    'data-icon' => 'material-icons smoking_rooms',
                    'data-icon-value' => 'smoking_rooms',
                ],
            ],
            [
                'value' => 'material-icons sms',
                'label' => 'sms',
                'attributes' => [
                    'data-icon' => 'material-icons sms',
                    'data-icon-value' => 'sms',
                ],
            ],
            [
                'value' => 'material-icons sms_failed',
                'label' => 'sms_failed',
                'attributes' => [
                    'data-icon' => 'material-icons sms_failed',
                    'data-icon-value' => 'sms_failed',
                ],
            ],
            [
                'value' => 'material-icons snapchat',
                'label' => 'snapchat',
                'attributes' => [
                    'data-icon' => 'material-icons snapchat',
                    'data-icon-value' => 'snapchat',
                ],
            ],
            [
                'value' => 'material-icons snippet_folder',
                'label' => 'snippet_folder',
                'attributes' => [
                    'data-icon' => 'material-icons snippet_folder',
                    'data-icon-value' => 'snippet_folder',
                ],
            ],
            [
                'value' => 'material-icons snooze',
                'label' => 'snooze',
                'attributes' => [
                    'data-icon' => 'material-icons snooze',
                    'data-icon-value' => 'snooze',
                ],
            ],
            [
                'value' => 'material-icons snowboarding',
                'label' => 'snowboarding',
                'attributes' => [
                    'data-icon' => 'material-icons snowboarding',
                    'data-icon-value' => 'snowboarding',
                ],
            ],
            [
                'value' => 'material-icons snowing',
                'label' => 'snowing',
                'attributes' => [
                    'data-icon' => 'material-icons snowing',
                    'data-icon-value' => 'snowing',
                ],
            ],
            [
                'value' => 'material-icons snowmobile',
                'label' => 'snowmobile',
                'attributes' => [
                    'data-icon' => 'material-icons snowmobile',
                    'data-icon-value' => 'snowmobile',
                ],
            ],
            [
                'value' => 'material-icons snowshoeing',
                'label' => 'snowshoeing',
                'attributes' => [
                    'data-icon' => 'material-icons snowshoeing',
                    'data-icon-value' => 'snowshoeing',
                ],
            ],
            [
                'value' => 'material-icons soap',
                'label' => 'soap',
                'attributes' => [
                    'data-icon' => 'material-icons soap',
                    'data-icon-value' => 'soap',
                ],
            ],
            [
                'value' => 'material-icons social_distance',
                'label' => 'social_distance',
                'attributes' => [
                    'data-icon' => 'material-icons social_distance',
                    'data-icon-value' => 'social_distance',
                ],
            ],
            [
                'value' => 'material-icons solar_power',
                'label' => 'solar_power',
                'attributes' => [
                    'data-icon' => 'material-icons solar_power',
                    'data-icon-value' => 'solar_power',
                ],
            ],
            [
                'value' => 'material-icons sort',
                'label' => 'sort',
                'attributes' => [
                    'data-icon' => 'material-icons sort',
                    'data-icon-value' => 'sort',
                ],
            ],
            [
                'value' => 'material-icons sort_by_alpha',
                'label' => 'sort_by_alpha',
                'attributes' => [
                    'data-icon' => 'material-icons sort_by_alpha',
                    'data-icon-value' => 'sort_by_alpha',
                ],
            ],
            [
                'value' => 'material-icons sos',
                'label' => 'sos',
                'attributes' => [
                    'data-icon' => 'material-icons sos',
                    'data-icon-value' => 'sos',
                ],
            ],
            [
                'value' => 'material-icons soup_kitchen',
                'label' => 'soup_kitchen',
                'attributes' => [
                    'data-icon' => 'material-icons soup_kitchen',
                    'data-icon-value' => 'soup_kitchen',
                ],
            ],
            [
                'value' => 'material-icons source',
                'label' => 'source',
                'attributes' => [
                    'data-icon' => 'material-icons source',
                    'data-icon-value' => 'source',
                ],
            ],
            [
                'value' => 'material-icons south',
                'label' => 'south',
                'attributes' => [
                    'data-icon' => 'material-icons south',
                    'data-icon-value' => 'south',
                ],
            ],
            [
                'value' => 'material-icons south_america',
                'label' => 'south_america',
                'attributes' => [
                    'data-icon' => 'material-icons south_america',
                    'data-icon-value' => 'south_america',
                ],
            ],
            [
                'value' => 'material-icons south_east',
                'label' => 'south_east',
                'attributes' => [
                    'data-icon' => 'material-icons south_east',
                    'data-icon-value' => 'south_east',
                ],
            ],
            [
                'value' => 'material-icons south_west',
                'label' => 'south_west',
                'attributes' => [
                    'data-icon' => 'material-icons south_west',
                    'data-icon-value' => 'south_west',
                ],
            ],
            [
                'value' => 'material-icons spa',
                'label' => 'spa',
                'attributes' => [
                    'data-icon' => 'material-icons spa',
                    'data-icon-value' => 'spa',
                ],
            ],
            [
                'value' => 'material-icons space_bar',
                'label' => 'space_bar',
                'attributes' => [
                    'data-icon' => 'material-icons space_bar',
                    'data-icon-value' => 'space_bar',
                ],
            ],
            [
                'value' => 'material-icons space_dashboard',
                'label' => 'space_dashboard',
                'attributes' => [
                    'data-icon' => 'material-icons space_dashboard',
                    'data-icon-value' => 'space_dashboard',
                ],
            ],
            [
                'value' => 'material-icons spatial_audio',
                'label' => 'spatial_audio',
                'attributes' => [
                    'data-icon' => 'material-icons spatial_audio',
                    'data-icon-value' => 'spatial_audio',
                ],
            ],
            [
                'value' => 'material-icons spatial_audio_off',
                'label' => 'spatial_audio_off',
                'attributes' => [
                    'data-icon' => 'material-icons spatial_audio_off',
                    'data-icon-value' => 'spatial_audio_off',
                ],
            ],
            [
                'value' => 'material-icons spatial_tracking',
                'label' => 'spatial_tracking',
                'attributes' => [
                    'data-icon' => 'material-icons spatial_tracking',
                    'data-icon-value' => 'spatial_tracking',
                ],
            ],
            [
                'value' => 'material-icons speaker',
                'label' => 'speaker',
                'attributes' => [
                    'data-icon' => 'material-icons speaker',
                    'data-icon-value' => 'speaker',
                ],
            ],
            [
                'value' => 'material-icons speaker_group',
                'label' => 'speaker_group',
                'attributes' => [
                    'data-icon' => 'material-icons speaker_group',
                    'data-icon-value' => 'speaker_group',
                ],
            ],
            [
                'value' => 'material-icons speaker_notes',
                'label' => 'speaker_notes',
                'attributes' => [
                    'data-icon' => 'material-icons speaker_notes',
                    'data-icon-value' => 'speaker_notes',
                ],
            ],
            [
                'value' => 'material-icons speaker_notes_off',
                'label' => 'speaker_notes_off',
                'attributes' => [
                    'data-icon' => 'material-icons speaker_notes_off',
                    'data-icon-value' => 'speaker_notes_off',
                ],
            ],
            [
                'value' => 'material-icons speaker_phone',
                'label' => 'speaker_phone',
                'attributes' => [
                    'data-icon' => 'material-icons speaker_phone',
                    'data-icon-value' => 'speaker_phone',
                ],
            ],
            [
                'value' => 'material-icons speed',
                'label' => 'speed',
                'attributes' => [
                    'data-icon' => 'material-icons speed',
                    'data-icon-value' => 'speed',
                ],
            ],
            [
                'value' => 'material-icons spellcheck',
                'label' => 'spellcheck',
                'attributes' => [
                    'data-icon' => 'material-icons spellcheck',
                    'data-icon-value' => 'spellcheck',
                ],
            ],
            [
                'value' => 'material-icons splitscreen',
                'label' => 'splitscreen',
                'attributes' => [
                    'data-icon' => 'material-icons splitscreen',
                    'data-icon-value' => 'splitscreen',
                ],
            ],
            [
                'value' => 'material-icons spoke',
                'label' => 'spoke',
                'attributes' => [
                    'data-icon' => 'material-icons spoke',
                    'data-icon-value' => 'spoke',
                ],
            ],
            [
                'value' => 'material-icons sports',
                'label' => 'sports',
                'attributes' => [
                    'data-icon' => 'material-icons sports',
                    'data-icon-value' => 'sports',
                ],
            ],
            [
                'value' => 'material-icons sports_bar',
                'label' => 'sports_bar',
                'attributes' => [
                    'data-icon' => 'material-icons sports_bar',
                    'data-icon-value' => 'sports_bar',
                ],
            ],
            [
                'value' => 'material-icons sports_baseball',
                'label' => 'sports_baseball',
                'attributes' => [
                    'data-icon' => 'material-icons sports_baseball',
                    'data-icon-value' => 'sports_baseball',
                ],
            ],
            [
                'value' => 'material-icons sports_basketball',
                'label' => 'sports_basketball',
                'attributes' => [
                    'data-icon' => 'material-icons sports_basketball',
                    'data-icon-value' => 'sports_basketball',
                ],
            ],
            [
                'value' => 'material-icons sports_cricket',
                'label' => 'sports_cricket',
                'attributes' => [
                    'data-icon' => 'material-icons sports_cricket',
                    'data-icon-value' => 'sports_cricket',
                ],
            ],
            [
                'value' => 'material-icons sports_esports',
                'label' => 'sports_esports',
                'attributes' => [
                    'data-icon' => 'material-icons sports_esports',
                    'data-icon-value' => 'sports_esports',
                ],
            ],
            [
                'value' => 'material-icons sports_football',
                'label' => 'sports_football',
                'attributes' => [
                    'data-icon' => 'material-icons sports_football',
                    'data-icon-value' => 'sports_football',
                ],
            ],
            [
                'value' => 'material-icons sports_golf',
                'label' => 'sports_golf',
                'attributes' => [
                    'data-icon' => 'material-icons sports_golf',
                    'data-icon-value' => 'sports_golf',
                ],
            ],
            [
                'value' => 'material-icons sports_gymnastics',
                'label' => 'sports_gymnastics',
                'attributes' => [
                    'data-icon' => 'material-icons sports_gymnastics',
                    'data-icon-value' => 'sports_gymnastics',
                ],
            ],
            [
                'value' => 'material-icons sports_handball',
                'label' => 'sports_handball',
                'attributes' => [
                    'data-icon' => 'material-icons sports_handball',
                    'data-icon-value' => 'sports_handball',
                ],
            ],
            [
                'value' => 'material-icons sports_hockey',
                'label' => 'sports_hockey',
                'attributes' => [
                    'data-icon' => 'material-icons sports_hockey',
                    'data-icon-value' => 'sports_hockey',
                ],
            ],
            [
                'value' => 'material-icons sports_kabaddi',
                'label' => 'sports_kabaddi',
                'attributes' => [
                    'data-icon' => 'material-icons sports_kabaddi',
                    'data-icon-value' => 'sports_kabaddi',
                ],
            ],
            [
                'value' => 'material-icons sports_martial_arts',
                'label' => 'sports_martial_arts',
                'attributes' => [
                    'data-icon' => 'material-icons sports_martial_arts',
                    'data-icon-value' => 'sports_martial_arts',
                ],
            ],
            [
                'value' => 'material-icons sports_mma',
                'label' => 'sports_mma',
                'attributes' => [
                    'data-icon' => 'material-icons sports_mma',
                    'data-icon-value' => 'sports_mma',
                ],
            ],
            [
                'value' => 'material-icons sports_motorsports',
                'label' => 'sports_motorsports',
                'attributes' => [
                    'data-icon' => 'material-icons sports_motorsports',
                    'data-icon-value' => 'sports_motorsports',
                ],
            ],
            [
                'value' => 'material-icons sports_rugby',
                'label' => 'sports_rugby',
                'attributes' => [
                    'data-icon' => 'material-icons sports_rugby',
                    'data-icon-value' => 'sports_rugby',
                ],
            ],
            [
                'value' => 'material-icons sports_score',
                'label' => 'sports_score',
                'attributes' => [
                    'data-icon' => 'material-icons sports_score',
                    'data-icon-value' => 'sports_score',
                ],
            ],
            [
                'value' => 'material-icons sports_soccer',
                'label' => 'sports_soccer',
                'attributes' => [
                    'data-icon' => 'material-icons sports_soccer',
                    'data-icon-value' => 'sports_soccer',
                ],
            ],
            [
                'value' => 'material-icons sports_tennis',
                'label' => 'sports_tennis',
                'attributes' => [
                    'data-icon' => 'material-icons sports_tennis',
                    'data-icon-value' => 'sports_tennis',
                ],
            ],
            [
                'value' => 'material-icons sports_volleyball',
                'label' => 'sports_volleyball',
                'attributes' => [
                    'data-icon' => 'material-icons sports_volleyball',
                    'data-icon-value' => 'sports_volleyball',
                ],
            ],
            [
                'value' => 'material-icons square',
                'label' => 'square',
                'attributes' => [
                    'data-icon' => 'material-icons square',
                    'data-icon-value' => 'square',
                ],
            ],
            [
                'value' => 'material-icons square_foot',
                'label' => 'square_foot',
                'attributes' => [
                    'data-icon' => 'material-icons square_foot',
                    'data-icon-value' => 'square_foot',
                ],
            ],
            [
                'value' => 'material-icons ssid_chart',
                'label' => 'ssid_chart',
                'attributes' => [
                    'data-icon' => 'material-icons ssid_chart',
                    'data-icon-value' => 'ssid_chart',
                ],
            ],
            [
                'value' => 'material-icons stacked_bar_chart',
                'label' => 'stacked_bar_chart',
                'attributes' => [
                    'data-icon' => 'material-icons stacked_bar_chart',
                    'data-icon-value' => 'stacked_bar_chart',
                ],
            ],
            [
                'value' => 'material-icons stacked_line_chart',
                'label' => 'stacked_line_chart',
                'attributes' => [
                    'data-icon' => 'material-icons stacked_line_chart',
                    'data-icon-value' => 'stacked_line_chart',
                ],
            ],
            [
                'value' => 'material-icons stadium',
                'label' => 'stadium',
                'attributes' => [
                    'data-icon' => 'material-icons stadium',
                    'data-icon-value' => 'stadium',
                ],
            ],
            [
                'value' => 'material-icons stairs',
                'label' => 'stairs',
                'attributes' => [
                    'data-icon' => 'material-icons stairs',
                    'data-icon-value' => 'stairs',
                ],
            ],
            [
                'value' => 'material-icons star',
                'label' => 'star',
                'attributes' => [
                    'data-icon' => 'material-icons star',
                    'data-icon-value' => 'star',
                ],
            ],
            [
                'value' => 'material-icons star_border',
                'label' => 'star_border',
                'attributes' => [
                    'data-icon' => 'material-icons star_border',
                    'data-icon-value' => 'star_border',
                ],
            ],
            [
                'value' => 'material-icons star_border_purple500',
                'label' => 'star_border_purple500',
                'attributes' => [
                    'data-icon' => 'material-icons star_border_purple500',
                    'data-icon-value' => 'star_border_purple500',
                ],
            ],
            [
                'value' => 'material-icons star_half',
                'label' => 'star_half',
                'attributes' => [
                    'data-icon' => 'material-icons star_half',
                    'data-icon-value' => 'star_half',
                ],
            ],
            [
                'value' => 'material-icons star_outline',
                'label' => 'star_outline',
                'attributes' => [
                    'data-icon' => 'material-icons star_outline',
                    'data-icon-value' => 'star_outline',
                ],
            ],
            [
                'value' => 'material-icons star_purple500',
                'label' => 'star_purple500',
                'attributes' => [
                    'data-icon' => 'material-icons star_purple500',
                    'data-icon-value' => 'star_purple500',
                ],
            ],
            [
                'value' => 'material-icons star_rate',
                'label' => 'star_rate',
                'attributes' => [
                    'data-icon' => 'material-icons star_rate',
                    'data-icon-value' => 'star_rate',
                ],
            ],
            [
                'value' => 'material-icons stars',
                'label' => 'stars',
                'attributes' => [
                    'data-icon' => 'material-icons stars',
                    'data-icon-value' => 'stars',
                ],
            ],
            [
                'value' => 'material-icons start',
                'label' => 'start',
                'attributes' => [
                    'data-icon' => 'material-icons start',
                    'data-icon-value' => 'start',
                ],
            ],
            [
                'value' => 'material-icons stay_current_landscape',
                'label' => 'stay_current_landscape',
                'attributes' => [
                    'data-icon' => 'material-icons stay_current_landscape',
                    'data-icon-value' => 'stay_current_landscape',
                ],
            ],
            [
                'value' => 'material-icons stay_current_portrait',
                'label' => 'stay_current_portrait',
                'attributes' => [
                    'data-icon' => 'material-icons stay_current_portrait',
                    'data-icon-value' => 'stay_current_portrait',
                ],
            ],
            [
                'value' => 'material-icons stay_primary_landscape',
                'label' => 'stay_primary_landscape',
                'attributes' => [
                    'data-icon' => 'material-icons stay_primary_landscape',
                    'data-icon-value' => 'stay_primary_landscape',
                ],
            ],
            [
                'value' => 'material-icons stay_primary_portrait',
                'label' => 'stay_primary_portrait',
                'attributes' => [
                    'data-icon' => 'material-icons stay_primary_portrait',
                    'data-icon-value' => 'stay_primary_portrait',
                ],
            ],
            [
                'value' => 'material-icons sticky_note_2',
                'label' => 'sticky_note_2',
                'attributes' => [
                    'data-icon' => 'material-icons sticky_note_2',
                    'data-icon-value' => 'sticky_note_2',
                ],
            ],
            [
                'value' => 'material-icons stop',
                'label' => 'stop',
                'attributes' => [
                    'data-icon' => 'material-icons stop',
                    'data-icon-value' => 'stop',
                ],
            ],
            [
                'value' => 'material-icons stop_circle',
                'label' => 'stop_circle',
                'attributes' => [
                    'data-icon' => 'material-icons stop_circle',
                    'data-icon-value' => 'stop_circle',
                ],
            ],
            [
                'value' => 'material-icons stop_screen_share',
                'label' => 'stop_screen_share',
                'attributes' => [
                    'data-icon' => 'material-icons stop_screen_share',
                    'data-icon-value' => 'stop_screen_share',
                ],
            ],
            [
                'value' => 'material-icons storage',
                'label' => 'storage',
                'attributes' => [
                    'data-icon' => 'material-icons storage',
                    'data-icon-value' => 'storage',
                ],
            ],
            [
                'value' => 'material-icons store',
                'label' => 'store',
                'attributes' => [
                    'data-icon' => 'material-icons store',
                    'data-icon-value' => 'store',
                ],
            ],
            [
                'value' => 'material-icons store_mall_directory',
                'label' => 'store_mall_directory',
                'attributes' => [
                    'data-icon' => 'material-icons store_mall_directory',
                    'data-icon-value' => 'store_mall_directory',
                ],
            ],
            [
                'value' => 'material-icons storefront',
                'label' => 'storefront',
                'attributes' => [
                    'data-icon' => 'material-icons storefront',
                    'data-icon-value' => 'storefront',
                ],
            ],
            [
                'value' => 'material-icons storm',
                'label' => 'storm',
                'attributes' => [
                    'data-icon' => 'material-icons storm',
                    'data-icon-value' => 'storm',
                ],
            ],
            [
                'value' => 'material-icons straight',
                'label' => 'straight',
                'attributes' => [
                    'data-icon' => 'material-icons straight',
                    'data-icon-value' => 'straight',
                ],
            ],
            [
                'value' => 'material-icons straighten',
                'label' => 'straighten',
                'attributes' => [
                    'data-icon' => 'material-icons straighten',
                    'data-icon-value' => 'straighten',
                ],
            ],
            [
                'value' => 'material-icons stream',
                'label' => 'stream',
                'attributes' => [
                    'data-icon' => 'material-icons stream',
                    'data-icon-value' => 'stream',
                ],
            ],
            [
                'value' => 'material-icons streetview',
                'label' => 'streetview',
                'attributes' => [
                    'data-icon' => 'material-icons streetview',
                    'data-icon-value' => 'streetview',
                ],
            ],
            [
                'value' => 'material-icons strikethrough_s',
                'label' => 'strikethrough_s',
                'attributes' => [
                    'data-icon' => 'material-icons strikethrough_s',
                    'data-icon-value' => 'strikethrough_s',
                ],
            ],
            [
                'value' => 'material-icons stroller',
                'label' => 'stroller',
                'attributes' => [
                    'data-icon' => 'material-icons stroller',
                    'data-icon-value' => 'stroller',
                ],
            ],
            [
                'value' => 'material-icons style',
                'label' => 'style',
                'attributes' => [
                    'data-icon' => 'material-icons style',
                    'data-icon-value' => 'style',
                ],
            ],
            [
                'value' => 'material-icons subdirectory_arrow_left',
                'label' => 'subdirectory_arrow_left',
                'attributes' => [
                    'data-icon' => 'material-icons subdirectory_arrow_left',
                    'data-icon-value' => 'subdirectory_arrow_left',
                ],
            ],
            [
                'value' => 'material-icons subdirectory_arrow_right',
                'label' => 'subdirectory_arrow_right',
                'attributes' => [
                    'data-icon' => 'material-icons subdirectory_arrow_right',
                    'data-icon-value' => 'subdirectory_arrow_right',
                ],
            ],
            [
                'value' => 'material-icons subject',
                'label' => 'subject',
                'attributes' => [
                    'data-icon' => 'material-icons subject',
                    'data-icon-value' => 'subject',
                ],
            ],
            [
                'value' => 'material-icons subscript',
                'label' => 'subscript',
                'attributes' => [
                    'data-icon' => 'material-icons subscript',
                    'data-icon-value' => 'subscript',
                ],
            ],
            [
                'value' => 'material-icons subscriptions',
                'label' => 'subscriptions',
                'attributes' => [
                    'data-icon' => 'material-icons subscriptions',
                    'data-icon-value' => 'subscriptions',
                ],
            ],
            [
                'value' => 'material-icons subtitles',
                'label' => 'subtitles',
                'attributes' => [
                    'data-icon' => 'material-icons subtitles',
                    'data-icon-value' => 'subtitles',
                ],
            ],
            [
                'value' => 'material-icons subtitles_off',
                'label' => 'subtitles_off',
                'attributes' => [
                    'data-icon' => 'material-icons subtitles_off',
                    'data-icon-value' => 'subtitles_off',
                ],
            ],
            [
                'value' => 'material-icons subway',
                'label' => 'subway',
                'attributes' => [
                    'data-icon' => 'material-icons subway',
                    'data-icon-value' => 'subway',
                ],
            ],
            [
                'value' => 'material-icons summarize',
                'label' => 'summarize',
                'attributes' => [
                    'data-icon' => 'material-icons summarize',
                    'data-icon-value' => 'summarize',
                ],
            ],
            [
                'value' => 'material-icons sunny',
                'label' => 'sunny',
                'attributes' => [
                    'data-icon' => 'material-icons sunny',
                    'data-icon-value' => 'sunny',
                ],
            ],
            [
                'value' => 'material-icons sunny_snowing',
                'label' => 'sunny_snowing',
                'attributes' => [
                    'data-icon' => 'material-icons sunny_snowing',
                    'data-icon-value' => 'sunny_snowing',
                ],
            ],
            [
                'value' => 'material-icons superscript',
                'label' => 'superscript',
                'attributes' => [
                    'data-icon' => 'material-icons superscript',
                    'data-icon-value' => 'superscript',
                ],
            ],
            [
                'value' => 'material-icons supervised_user_circle',
                'label' => 'supervised_user_circle',
                'attributes' => [
                    'data-icon' => 'material-icons supervised_user_circle',
                    'data-icon-value' => 'supervised_user_circle',
                ],
            ],
            [
                'value' => 'material-icons supervisor_account',
                'label' => 'supervisor_account',
                'attributes' => [
                    'data-icon' => 'material-icons supervisor_account',
                    'data-icon-value' => 'supervisor_account',
                ],
            ],
            [
                'value' => 'material-icons support',
                'label' => 'support',
                'attributes' => [
                    'data-icon' => 'material-icons support',
                    'data-icon-value' => 'support',
                ],
            ],
            [
                'value' => 'material-icons support_agent',
                'label' => 'support_agent',
                'attributes' => [
                    'data-icon' => 'material-icons support_agent',
                    'data-icon-value' => 'support_agent',
                ],
            ],
            [
                'value' => 'material-icons surfing',
                'label' => 'surfing',
                'attributes' => [
                    'data-icon' => 'material-icons surfing',
                    'data-icon-value' => 'surfing',
                ],
            ],
            [
                'value' => 'material-icons surround_sound',
                'label' => 'surround_sound',
                'attributes' => [
                    'data-icon' => 'material-icons surround_sound',
                    'data-icon-value' => 'surround_sound',
                ],
            ],
            [
                'value' => 'material-icons swap_calls',
                'label' => 'swap_calls',
                'attributes' => [
                    'data-icon' => 'material-icons swap_calls',
                    'data-icon-value' => 'swap_calls',
                ],
            ],
            [
                'value' => 'material-icons swap_horiz',
                'label' => 'swap_horiz',
                'attributes' => [
                    'data-icon' => 'material-icons swap_horiz',
                    'data-icon-value' => 'swap_horiz',
                ],
            ],
            [
                'value' => 'material-icons swap_horizontal_circle',
                'label' => 'swap_horizontal_circle',
                'attributes' => [
                    'data-icon' => 'material-icons swap_horizontal_circle',
                    'data-icon-value' => 'swap_horizontal_circle',
                ],
            ],
            [
                'value' => 'material-icons swap_vert',
                'label' => 'swap_vert',
                'attributes' => [
                    'data-icon' => 'material-icons swap_vert',
                    'data-icon-value' => 'swap_vert',
                ],
            ],
            [
                'value' => 'material-icons swap_vert_circle',
                'label' => 'swap_vert_circle',
                'attributes' => [
                    'data-icon' => 'material-icons swap_vert_circle',
                    'data-icon-value' => 'swap_vert_circle',
                ],
            ],
            [
                'value' => 'material-icons swap_vertical_circle',
                'label' => 'swap_vertical_circle',
                'attributes' => [
                    'data-icon' => 'material-icons swap_vertical_circle',
                    'data-icon-value' => 'swap_vertical_circle',
                ],
            ],
            [
                'value' => 'material-icons swipe',
                'label' => 'swipe',
                'attributes' => [
                    'data-icon' => 'material-icons swipe',
                    'data-icon-value' => 'swipe',
                ],
            ],
            [
                'value' => 'material-icons swipe_down',
                'label' => 'swipe_down',
                'attributes' => [
                    'data-icon' => 'material-icons swipe_down',
                    'data-icon-value' => 'swipe_down',
                ],
            ],
            [
                'value' => 'material-icons swipe_down_alt',
                'label' => 'swipe_down_alt',
                'attributes' => [
                    'data-icon' => 'material-icons swipe_down_alt',
                    'data-icon-value' => 'swipe_down_alt',
                ],
            ],
            [
                'value' => 'material-icons swipe_left',
                'label' => 'swipe_left',
                'attributes' => [
                    'data-icon' => 'material-icons swipe_left',
                    'data-icon-value' => 'swipe_left',
                ],
            ],
            [
                'value' => 'material-icons swipe_left_alt',
                'label' => 'swipe_left_alt',
                'attributes' => [
                    'data-icon' => 'material-icons swipe_left_alt',
                    'data-icon-value' => 'swipe_left_alt',
                ],
            ],
            [
                'value' => 'material-icons swipe_right',
                'label' => 'swipe_right',
                'attributes' => [
                    'data-icon' => 'material-icons swipe_right',
                    'data-icon-value' => 'swipe_right',
                ],
            ],
            [
                'value' => 'material-icons swipe_right_alt',
                'label' => 'swipe_right_alt',
                'attributes' => [
                    'data-icon' => 'material-icons swipe_right_alt',
                    'data-icon-value' => 'swipe_right_alt',
                ],
            ],
            [
                'value' => 'material-icons swipe_up',
                'label' => 'swipe_up',
                'attributes' => [
                    'data-icon' => 'material-icons swipe_up',
                    'data-icon-value' => 'swipe_up',
                ],
            ],
            [
                'value' => 'material-icons swipe_up_alt',
                'label' => 'swipe_up_alt',
                'attributes' => [
                    'data-icon' => 'material-icons swipe_up_alt',
                    'data-icon-value' => 'swipe_up_alt',
                ],
            ],
            [
                'value' => 'material-icons swipe_vertical',
                'label' => 'swipe_vertical',
                'attributes' => [
                    'data-icon' => 'material-icons swipe_vertical',
                    'data-icon-value' => 'swipe_vertical',
                ],
            ],
            [
                'value' => 'material-icons switch_access_shortcut',
                'label' => 'switch_access_shortcut',
                'attributes' => [
                    'data-icon' => 'material-icons switch_access_shortcut',
                    'data-icon-value' => 'switch_access_shortcut',
                ],
            ],
            [
                'value' => 'material-icons switch_access_shortcut_add',
                'label' => 'switch_access_shortcut_add',
                'attributes' => [
                    'data-icon' => 'material-icons switch_access_shortcut_add',
                    'data-icon-value' => 'switch_access_shortcut_add',
                ],
            ],
            [
                'value' => 'material-icons switch_account',
                'label' => 'switch_account',
                'attributes' => [
                    'data-icon' => 'material-icons switch_account',
                    'data-icon-value' => 'switch_account',
                ],
            ],
            [
                'value' => 'material-icons switch_camera',
                'label' => 'switch_camera',
                'attributes' => [
                    'data-icon' => 'material-icons switch_camera',
                    'data-icon-value' => 'switch_camera',
                ],
            ],
            [
                'value' => 'material-icons switch_left',
                'label' => 'switch_left',
                'attributes' => [
                    'data-icon' => 'material-icons switch_left',
                    'data-icon-value' => 'switch_left',
                ],
            ],
            [
                'value' => 'material-icons switch_right',
                'label' => 'switch_right',
                'attributes' => [
                    'data-icon' => 'material-icons switch_right',
                    'data-icon-value' => 'switch_right',
                ],
            ],
            [
                'value' => 'material-icons switch_video',
                'label' => 'switch_video',
                'attributes' => [
                    'data-icon' => 'material-icons switch_video',
                    'data-icon-value' => 'switch_video',
                ],
            ],
            [
                'value' => 'material-icons synagogue',
                'label' => 'synagogue',
                'attributes' => [
                    'data-icon' => 'material-icons synagogue',
                    'data-icon-value' => 'synagogue',
                ],
            ],
            [
                'value' => 'material-icons sync',
                'label' => 'sync',
                'attributes' => [
                    'data-icon' => 'material-icons sync',
                    'data-icon-value' => 'sync',
                ],
            ],
            [
                'value' => 'material-icons sync_alt',
                'label' => 'sync_alt',
                'attributes' => [
                    'data-icon' => 'material-icons sync_alt',
                    'data-icon-value' => 'sync_alt',
                ],
            ],
            [
                'value' => 'material-icons sync_disabled',
                'label' => 'sync_disabled',
                'attributes' => [
                    'data-icon' => 'material-icons sync_disabled',
                    'data-icon-value' => 'sync_disabled',
                ],
            ],
            [
                'value' => 'material-icons sync_lock',
                'label' => 'sync_lock',
                'attributes' => [
                    'data-icon' => 'material-icons sync_lock',
                    'data-icon-value' => 'sync_lock',
                ],
            ],
            [
                'value' => 'material-icons sync_problem',
                'label' => 'sync_problem',
                'attributes' => [
                    'data-icon' => 'material-icons sync_problem',
                    'data-icon-value' => 'sync_problem',
                ],
            ],
            [
                'value' => 'material-icons system_security_update',
                'label' => 'system_security_update',
                'attributes' => [
                    'data-icon' => 'material-icons system_security_update',
                    'data-icon-value' => 'system_security_update',
                ],
            ],
            [
                'value' => 'material-icons system_security_update_good',
                'label' => 'system_security_update_good',
                'attributes' => [
                    'data-icon' => 'material-icons system_security_update_good',
                    'data-icon-value' => 'system_security_update_good',
                ],
            ],
            [
                'value' => 'material-icons system_security_update_warning',
                'label' => 'system_security_update_warning',
                'attributes' => [
                    'data-icon' => 'material-icons system_security_update_warning',
                    'data-icon-value' => 'system_security_update_warning',
                ],
            ],
            [
                'value' => 'material-icons system_update',
                'label' => 'system_update',
                'attributes' => [
                    'data-icon' => 'material-icons system_update',
                    'data-icon-value' => 'system_update',
                ],
            ],
            [
                'value' => 'material-icons system_update_alt',
                'label' => 'system_update_alt',
                'attributes' => [
                    'data-icon' => 'material-icons system_update_alt',
                    'data-icon-value' => 'system_update_alt',
                ],
            ],
            [
                'value' => 'material-icons system_update_tv',
                'label' => 'system_update_tv',
                'attributes' => [
                    'data-icon' => 'material-icons system_update_tv',
                    'data-icon-value' => 'system_update_tv',
                ],
            ],
            [
                'value' => 'material-icons tab',
                'label' => 'tab',
                'attributes' => [
                    'data-icon' => 'material-icons tab',
                    'data-icon-value' => 'tab',
                ],
            ],
            [
                'value' => 'material-icons tab_unselected',
                'label' => 'tab_unselected',
                'attributes' => [
                    'data-icon' => 'material-icons tab_unselected',
                    'data-icon-value' => 'tab_unselected',
                ],
            ],
            [
                'value' => 'material-icons table_bar',
                'label' => 'table_bar',
                'attributes' => [
                    'data-icon' => 'material-icons table_bar',
                    'data-icon-value' => 'table_bar',
                ],
            ],
            [
                'value' => 'material-icons table_chart',
                'label' => 'table_chart',
                'attributes' => [
                    'data-icon' => 'material-icons table_chart',
                    'data-icon-value' => 'table_chart',
                ],
            ],
            [
                'value' => 'material-icons table_restaurant',
                'label' => 'table_restaurant',
                'attributes' => [
                    'data-icon' => 'material-icons table_restaurant',
                    'data-icon-value' => 'table_restaurant',
                ],
            ],
            [
                'value' => 'material-icons table_rows',
                'label' => 'table_rows',
                'attributes' => [
                    'data-icon' => 'material-icons table_rows',
                    'data-icon-value' => 'table_rows',
                ],
            ],
            [
                'value' => 'material-icons table_view',
                'label' => 'table_view',
                'attributes' => [
                    'data-icon' => 'material-icons table_view',
                    'data-icon-value' => 'table_view',
                ],
            ],
            [
                'value' => 'material-icons tablet',
                'label' => 'tablet',
                'attributes' => [
                    'data-icon' => 'material-icons tablet',
                    'data-icon-value' => 'tablet',
                ],
            ],
            [
                'value' => 'material-icons tablet_android',
                'label' => 'tablet_android',
                'attributes' => [
                    'data-icon' => 'material-icons tablet_android',
                    'data-icon-value' => 'tablet_android',
                ],
            ],
            [
                'value' => 'material-icons tablet_mac',
                'label' => 'tablet_mac',
                'attributes' => [
                    'data-icon' => 'material-icons tablet_mac',
                    'data-icon-value' => 'tablet_mac',
                ],
            ],
            [
                'value' => 'material-icons tag',
                'label' => 'tag',
                'attributes' => [
                    'data-icon' => 'material-icons tag',
                    'data-icon-value' => 'tag',
                ],
            ],
            [
                'value' => 'material-icons tag_faces',
                'label' => 'tag_faces',
                'attributes' => [
                    'data-icon' => 'material-icons tag_faces',
                    'data-icon-value' => 'tag_faces',
                ],
            ],
            [
                'value' => 'material-icons takeout_dining',
                'label' => 'takeout_dining',
                'attributes' => [
                    'data-icon' => 'material-icons takeout_dining',
                    'data-icon-value' => 'takeout_dining',
                ],
            ],
            [
                'value' => 'material-icons tap_and_play',
                'label' => 'tap_and_play',
                'attributes' => [
                    'data-icon' => 'material-icons tap_and_play',
                    'data-icon-value' => 'tap_and_play',
                ],
            ],
            [
                'value' => 'material-icons tapas',
                'label' => 'tapas',
                'attributes' => [
                    'data-icon' => 'material-icons tapas',
                    'data-icon-value' => 'tapas',
                ],
            ],
            [
                'value' => 'material-icons task',
                'label' => 'task',
                'attributes' => [
                    'data-icon' => 'material-icons task',
                    'data-icon-value' => 'task',
                ],
            ],
            [
                'value' => 'material-icons task_alt',
                'label' => 'task_alt',
                'attributes' => [
                    'data-icon' => 'material-icons task_alt',
                    'data-icon-value' => 'task_alt',
                ],
            ],
            [
                'value' => 'material-icons taxi_alert',
                'label' => 'taxi_alert',
                'attributes' => [
                    'data-icon' => 'material-icons taxi_alert',
                    'data-icon-value' => 'taxi_alert',
                ],
            ],
            [
                'value' => 'material-icons telegram',
                'label' => 'telegram',
                'attributes' => [
                    'data-icon' => 'material-icons telegram',
                    'data-icon-value' => 'telegram',
                ],
            ],
            [
                'value' => 'material-icons temple_buddhist',
                'label' => 'temple_buddhist',
                'attributes' => [
                    'data-icon' => 'material-icons temple_buddhist',
                    'data-icon-value' => 'temple_buddhist',
                ],
            ],
            [
                'value' => 'material-icons temple_hindu',
                'label' => 'temple_hindu',
                'attributes' => [
                    'data-icon' => 'material-icons temple_hindu',
                    'data-icon-value' => 'temple_hindu',
                ],
            ],
            [
                'value' => 'material-icons terminal',
                'label' => 'terminal',
                'attributes' => [
                    'data-icon' => 'material-icons terminal',
                    'data-icon-value' => 'terminal',
                ],
            ],
            [
                'value' => 'material-icons terrain',
                'label' => 'terrain',
                'attributes' => [
                    'data-icon' => 'material-icons terrain',
                    'data-icon-value' => 'terrain',
                ],
            ],
            [
                'value' => 'material-icons text_decrease',
                'label' => 'text_decrease',
                'attributes' => [
                    'data-icon' => 'material-icons text_decrease',
                    'data-icon-value' => 'text_decrease',
                ],
            ],
            [
                'value' => 'material-icons text_fields',
                'label' => 'text_fields',
                'attributes' => [
                    'data-icon' => 'material-icons text_fields',
                    'data-icon-value' => 'text_fields',
                ],
            ],
            [
                'value' => 'material-icons text_format',
                'label' => 'text_format',
                'attributes' => [
                    'data-icon' => 'material-icons text_format',
                    'data-icon-value' => 'text_format',
                ],
            ],
            [
                'value' => 'material-icons text_increase',
                'label' => 'text_increase',
                'attributes' => [
                    'data-icon' => 'material-icons text_increase',
                    'data-icon-value' => 'text_increase',
                ],
            ],
            [
                'value' => 'material-icons text_rotate_up',
                'label' => 'text_rotate_up',
                'attributes' => [
                    'data-icon' => 'material-icons text_rotate_up',
                    'data-icon-value' => 'text_rotate_up',
                ],
            ],
            [
                'value' => 'material-icons text_rotate_vertical',
                'label' => 'text_rotate_vertical',
                'attributes' => [
                    'data-icon' => 'material-icons text_rotate_vertical',
                    'data-icon-value' => 'text_rotate_vertical',
                ],
            ],
            [
                'value' => 'material-icons text_rotation_angledown',
                'label' => 'text_rotation_angledown',
                'attributes' => [
                    'data-icon' => 'material-icons text_rotation_angledown',
                    'data-icon-value' => 'text_rotation_angledown',
                ],
            ],
            [
                'value' => 'material-icons text_rotation_angleup',
                'label' => 'text_rotation_angleup',
                'attributes' => [
                    'data-icon' => 'material-icons text_rotation_angleup',
                    'data-icon-value' => 'text_rotation_angleup',
                ],
            ],
            [
                'value' => 'material-icons text_rotation_down',
                'label' => 'text_rotation_down',
                'attributes' => [
                    'data-icon' => 'material-icons text_rotation_down',
                    'data-icon-value' => 'text_rotation_down',
                ],
            ],
            [
                'value' => 'material-icons text_rotation_none',
                'label' => 'text_rotation_none',
                'attributes' => [
                    'data-icon' => 'material-icons text_rotation_none',
                    'data-icon-value' => 'text_rotation_none',
                ],
            ],
            [
                'value' => 'material-icons text_snippet',
                'label' => 'text_snippet',
                'attributes' => [
                    'data-icon' => 'material-icons text_snippet',
                    'data-icon-value' => 'text_snippet',
                ],
            ],
            [
                'value' => 'material-icons textsms',
                'label' => 'textsms',
                'attributes' => [
                    'data-icon' => 'material-icons textsms',
                    'data-icon-value' => 'textsms',
                ],
            ],
            [
                'value' => 'material-icons texture',
                'label' => 'texture',
                'attributes' => [
                    'data-icon' => 'material-icons texture',
                    'data-icon-value' => 'texture',
                ],
            ],
            [
                'value' => 'material-icons theater_comedy',
                'label' => 'theater_comedy',
                'attributes' => [
                    'data-icon' => 'material-icons theater_comedy',
                    'data-icon-value' => 'theater_comedy',
                ],
            ],
            [
                'value' => 'material-icons theaters',
                'label' => 'theaters',
                'attributes' => [
                    'data-icon' => 'material-icons theaters',
                    'data-icon-value' => 'theaters',
                ],
            ],
            [
                'value' => 'material-icons thermostat',
                'label' => 'thermostat',
                'attributes' => [
                    'data-icon' => 'material-icons thermostat',
                    'data-icon-value' => 'thermostat',
                ],
            ],
            [
                'value' => 'material-icons thermostat_auto',
                'label' => 'thermostat_auto',
                'attributes' => [
                    'data-icon' => 'material-icons thermostat_auto',
                    'data-icon-value' => 'thermostat_auto',
                ],
            ],
            [
                'value' => 'material-icons thumb_down',
                'label' => 'thumb_down',
                'attributes' => [
                    'data-icon' => 'material-icons thumb_down',
                    'data-icon-value' => 'thumb_down',
                ],
            ],
            [
                'value' => 'material-icons thumb_down_alt',
                'label' => 'thumb_down_alt',
                'attributes' => [
                    'data-icon' => 'material-icons thumb_down_alt',
                    'data-icon-value' => 'thumb_down_alt',
                ],
            ],
            [
                'value' => 'material-icons thumb_down_off_alt',
                'label' => 'thumb_down_off_alt',
                'attributes' => [
                    'data-icon' => 'material-icons thumb_down_off_alt',
                    'data-icon-value' => 'thumb_down_off_alt',
                ],
            ],
            [
                'value' => 'material-icons thumb_up',
                'label' => 'thumb_up',
                'attributes' => [
                    'data-icon' => 'material-icons thumb_up',
                    'data-icon-value' => 'thumb_up',
                ],
            ],
            [
                'value' => 'material-icons thumb_up_alt',
                'label' => 'thumb_up_alt',
                'attributes' => [
                    'data-icon' => 'material-icons thumb_up_alt',
                    'data-icon-value' => 'thumb_up_alt',
                ],
            ],
            [
                'value' => 'material-icons thumb_up_off_alt',
                'label' => 'thumb_up_off_alt',
                'attributes' => [
                    'data-icon' => 'material-icons thumb_up_off_alt',
                    'data-icon-value' => 'thumb_up_off_alt',
                ],
            ],
            [
                'value' => 'material-icons thumbs_up_down',
                'label' => 'thumbs_up_down',
                'attributes' => [
                    'data-icon' => 'material-icons thumbs_up_down',
                    'data-icon-value' => 'thumbs_up_down',
                ],
            ],
            [
                'value' => 'material-icons thunderstorm',
                'label' => 'thunderstorm',
                'attributes' => [
                    'data-icon' => 'material-icons thunderstorm',
                    'data-icon-value' => 'thunderstorm',
                ],
            ],
            [
                'value' => 'material-icons tiktok',
                'label' => 'tiktok',
                'attributes' => [
                    'data-icon' => 'material-icons tiktok',
                    'data-icon-value' => 'tiktok',
                ],
            ],
            [
                'value' => 'material-icons time_to_leave',
                'label' => 'time_to_leave',
                'attributes' => [
                    'data-icon' => 'material-icons time_to_leave',
                    'data-icon-value' => 'time_to_leave',
                ],
            ],
            [
                'value' => 'material-icons timelapse',
                'label' => 'timelapse',
                'attributes' => [
                    'data-icon' => 'material-icons timelapse',
                    'data-icon-value' => 'timelapse',
                ],
            ],
            [
                'value' => 'material-icons timeline',
                'label' => 'timeline',
                'attributes' => [
                    'data-icon' => 'material-icons timeline',
                    'data-icon-value' => 'timeline',
                ],
            ],
            [
                'value' => 'material-icons timer',
                'label' => 'timer',
                'attributes' => [
                    'data-icon' => 'material-icons timer',
                    'data-icon-value' => 'timer',
                ],
            ],
            [
                'value' => 'material-icons timer_10',
                'label' => 'timer_10',
                'attributes' => [
                    'data-icon' => 'material-icons timer_10',
                    'data-icon-value' => 'timer_10',
                ],
            ],
            [
                'value' => 'material-icons timer_10_select',
                'label' => 'timer_10_select',
                'attributes' => [
                    'data-icon' => 'material-icons timer_10_select',
                    'data-icon-value' => 'timer_10_select',
                ],
            ],
            [
                'value' => 'material-icons timer_3',
                'label' => 'timer_3',
                'attributes' => [
                    'data-icon' => 'material-icons timer_3',
                    'data-icon-value' => 'timer_3',
                ],
            ],
            [
                'value' => 'material-icons timer_3_select',
                'label' => 'timer_3_select',
                'attributes' => [
                    'data-icon' => 'material-icons timer_3_select',
                    'data-icon-value' => 'timer_3_select',
                ],
            ],
            [
                'value' => 'material-icons timer_off',
                'label' => 'timer_off',
                'attributes' => [
                    'data-icon' => 'material-icons timer_off',
                    'data-icon-value' => 'timer_off',
                ],
            ],
            [
                'value' => 'material-icons tips_and_updates',
                'label' => 'tips_and_updates',
                'attributes' => [
                    'data-icon' => 'material-icons tips_and_updates',
                    'data-icon-value' => 'tips_and_updates',
                ],
            ],
            [
                'value' => 'material-icons tire_repair',
                'label' => 'tire_repair',
                'attributes' => [
                    'data-icon' => 'material-icons tire_repair',
                    'data-icon-value' => 'tire_repair',
                ],
            ],
            [
                'value' => 'material-icons title',
                'label' => 'title',
                'attributes' => [
                    'data-icon' => 'material-icons title',
                    'data-icon-value' => 'title',
                ],
            ],
            [
                'value' => 'material-icons toc',
                'label' => 'toc',
                'attributes' => [
                    'data-icon' => 'material-icons toc',
                    'data-icon-value' => 'toc',
                ],
            ],
            [
                'value' => 'material-icons today',
                'label' => 'today',
                'attributes' => [
                    'data-icon' => 'material-icons today',
                    'data-icon-value' => 'today',
                ],
            ],
            [
                'value' => 'material-icons toggle_off',
                'label' => 'toggle_off',
                'attributes' => [
                    'data-icon' => 'material-icons toggle_off',
                    'data-icon-value' => 'toggle_off',
                ],
            ],
            [
                'value' => 'material-icons toggle_on',
                'label' => 'toggle_on',
                'attributes' => [
                    'data-icon' => 'material-icons toggle_on',
                    'data-icon-value' => 'toggle_on',
                ],
            ],
            [
                'value' => 'material-icons token',
                'label' => 'token',
                'attributes' => [
                    'data-icon' => 'material-icons token',
                    'data-icon-value' => 'token',
                ],
            ],
            [
                'value' => 'material-icons toll',
                'label' => 'toll',
                'attributes' => [
                    'data-icon' => 'material-icons toll',
                    'data-icon-value' => 'toll',
                ],
            ],
            [
                'value' => 'material-icons tonality',
                'label' => 'tonality',
                'attributes' => [
                    'data-icon' => 'material-icons tonality',
                    'data-icon-value' => 'tonality',
                ],
            ],
            [
                'value' => 'material-icons topic',
                'label' => 'topic',
                'attributes' => [
                    'data-icon' => 'material-icons topic',
                    'data-icon-value' => 'topic',
                ],
            ],
            [
                'value' => 'material-icons tornado',
                'label' => 'tornado',
                'attributes' => [
                    'data-icon' => 'material-icons tornado',
                    'data-icon-value' => 'tornado',
                ],
            ],
            [
                'value' => 'material-icons touch_app',
                'label' => 'touch_app',
                'attributes' => [
                    'data-icon' => 'material-icons touch_app',
                    'data-icon-value' => 'touch_app',
                ],
            ],
            [
                'value' => 'material-icons tour',
                'label' => 'tour',
                'attributes' => [
                    'data-icon' => 'material-icons tour',
                    'data-icon-value' => 'tour',
                ],
            ],
            [
                'value' => 'material-icons toys',
                'label' => 'toys',
                'attributes' => [
                    'data-icon' => 'material-icons toys',
                    'data-icon-value' => 'toys',
                ],
            ],
            [
                'value' => 'material-icons track_changes',
                'label' => 'track_changes',
                'attributes' => [
                    'data-icon' => 'material-icons track_changes',
                    'data-icon-value' => 'track_changes',
                ],
            ],
            [
                'value' => 'material-icons traffic',
                'label' => 'traffic',
                'attributes' => [
                    'data-icon' => 'material-icons traffic',
                    'data-icon-value' => 'traffic',
                ],
            ],
            [
                'value' => 'material-icons train',
                'label' => 'train',
                'attributes' => [
                    'data-icon' => 'material-icons train',
                    'data-icon-value' => 'train',
                ],
            ],
            [
                'value' => 'material-icons tram',
                'label' => 'tram',
                'attributes' => [
                    'data-icon' => 'material-icons tram',
                    'data-icon-value' => 'tram',
                ],
            ],
            [
                'value' => 'material-icons transfer_within_a_station',
                'label' => 'transfer_within_a_station',
                'attributes' => [
                    'data-icon' => 'material-icons transfer_within_a_station',
                    'data-icon-value' => 'transfer_within_a_station',
                ],
            ],
            [
                'value' => 'material-icons transform',
                'label' => 'transform',
                'attributes' => [
                    'data-icon' => 'material-icons transform',
                    'data-icon-value' => 'transform',
                ],
            ],
            [
                'value' => 'material-icons transgender',
                'label' => 'transgender',
                'attributes' => [
                    'data-icon' => 'material-icons transgender',
                    'data-icon-value' => 'transgender',
                ],
            ],
            [
                'value' => 'material-icons transit_enterexit',
                'label' => 'transit_enterexit',
                'attributes' => [
                    'data-icon' => 'material-icons transit_enterexit',
                    'data-icon-value' => 'transit_enterexit',
                ],
            ],
            [
                'value' => 'material-icons translate',
                'label' => 'translate',
                'attributes' => [
                    'data-icon' => 'material-icons translate',
                    'data-icon-value' => 'translate',
                ],
            ],
            [
                'value' => 'material-icons travel_explore',
                'label' => 'travel_explore',
                'attributes' => [
                    'data-icon' => 'material-icons travel_explore',
                    'data-icon-value' => 'travel_explore',
                ],
            ],
            [
                'value' => 'material-icons trending_down',
                'label' => 'trending_down',
                'attributes' => [
                    'data-icon' => 'material-icons trending_down',
                    'data-icon-value' => 'trending_down',
                ],
            ],
            [
                'value' => 'material-icons trending_flat',
                'label' => 'trending_flat',
                'attributes' => [
                    'data-icon' => 'material-icons trending_flat',
                    'data-icon-value' => 'trending_flat',
                ],
            ],
            [
                'value' => 'material-icons trending_neutral',
                'label' => 'trending_neutral',
                'attributes' => [
                    'data-icon' => 'material-icons trending_neutral',
                    'data-icon-value' => 'trending_neutral',
                ],
            ],
            [
                'value' => 'material-icons trending_up',
                'label' => 'trending_up',
                'attributes' => [
                    'data-icon' => 'material-icons trending_up',
                    'data-icon-value' => 'trending_up',
                ],
            ],
            [
                'value' => 'material-icons trip_origin',
                'label' => 'trip_origin',
                'attributes' => [
                    'data-icon' => 'material-icons trip_origin',
                    'data-icon-value' => 'trip_origin',
                ],
            ],
            [
                'value' => 'material-icons troubleshoot',
                'label' => 'troubleshoot',
                'attributes' => [
                    'data-icon' => 'material-icons troubleshoot',
                    'data-icon-value' => 'troubleshoot',
                ],
            ],
            [
                'value' => 'material-icons try',
                'label' => 'try',
                'attributes' => [
                    'data-icon' => 'material-icons try',
                    'data-icon-value' => 'try',
                ],
            ],
            [
                'value' => 'material-icons tsunami',
                'label' => 'tsunami',
                'attributes' => [
                    'data-icon' => 'material-icons tsunami',
                    'data-icon-value' => 'tsunami',
                ],
            ],
            [
                'value' => 'material-icons tty',
                'label' => 'tty',
                'attributes' => [
                    'data-icon' => 'material-icons tty',
                    'data-icon-value' => 'tty',
                ],
            ],
            [
                'value' => 'material-icons tune',
                'label' => 'tune',
                'attributes' => [
                    'data-icon' => 'material-icons tune',
                    'data-icon-value' => 'tune',
                ],
            ],
            [
                'value' => 'material-icons tungsten',
                'label' => 'tungsten',
                'attributes' => [
                    'data-icon' => 'material-icons tungsten',
                    'data-icon-value' => 'tungsten',
                ],
            ],
            [
                'value' => 'material-icons turn_left',
                'label' => 'turn_left',
                'attributes' => [
                    'data-icon' => 'material-icons turn_left',
                    'data-icon-value' => 'turn_left',
                ],
            ],
            [
                'value' => 'material-icons turn_right',
                'label' => 'turn_right',
                'attributes' => [
                    'data-icon' => 'material-icons turn_right',
                    'data-icon-value' => 'turn_right',
                ],
            ],
            [
                'value' => 'material-icons turn_sharp_left',
                'label' => 'turn_sharp_left',
                'attributes' => [
                    'data-icon' => 'material-icons turn_sharp_left',
                    'data-icon-value' => 'turn_sharp_left',
                ],
            ],
            [
                'value' => 'material-icons turn_sharp_right',
                'label' => 'turn_sharp_right',
                'attributes' => [
                    'data-icon' => 'material-icons turn_sharp_right',
                    'data-icon-value' => 'turn_sharp_right',
                ],
            ],
            [
                'value' => 'material-icons turn_slight_left',
                'label' => 'turn_slight_left',
                'attributes' => [
                    'data-icon' => 'material-icons turn_slight_left',
                    'data-icon-value' => 'turn_slight_left',
                ],
            ],
            [
                'value' => 'material-icons turn_slight_right',
                'label' => 'turn_slight_right',
                'attributes' => [
                    'data-icon' => 'material-icons turn_slight_right',
                    'data-icon-value' => 'turn_slight_right',
                ],
            ],
            [
                'value' => 'material-icons turned_in',
                'label' => 'turned_in',
                'attributes' => [
                    'data-icon' => 'material-icons turned_in',
                    'data-icon-value' => 'turned_in',
                ],
            ],
            [
                'value' => 'material-icons turned_in_not',
                'label' => 'turned_in_not',
                'attributes' => [
                    'data-icon' => 'material-icons turned_in_not',
                    'data-icon-value' => 'turned_in_not',
                ],
            ],
            [
                'value' => 'material-icons tv',
                'label' => 'tv',
                'attributes' => [
                    'data-icon' => 'material-icons tv',
                    'data-icon-value' => 'tv',
                ],
            ],
            [
                'value' => 'material-icons tv_off',
                'label' => 'tv_off',
                'attributes' => [
                    'data-icon' => 'material-icons tv_off',
                    'data-icon-value' => 'tv_off',
                ],
            ],
            [
                'value' => 'material-icons two_wheeler',
                'label' => 'two_wheeler',
                'attributes' => [
                    'data-icon' => 'material-icons two_wheeler',
                    'data-icon-value' => 'two_wheeler',
                ],
            ],
            [
                'value' => 'material-icons u_turn_left',
                'label' => 'u_turn_left',
                'attributes' => [
                    'data-icon' => 'material-icons u_turn_left',
                    'data-icon-value' => 'u_turn_left',
                ],
            ],
            [
                'value' => 'material-icons u_turn_right',
                'label' => 'u_turn_right',
                'attributes' => [
                    'data-icon' => 'material-icons u_turn_right',
                    'data-icon-value' => 'u_turn_right',
                ],
            ],
            [
                'value' => 'material-icons umbrella',
                'label' => 'umbrella',
                'attributes' => [
                    'data-icon' => 'material-icons umbrella',
                    'data-icon-value' => 'umbrella',
                ],
            ],
            [
                'value' => 'material-icons unarchive',
                'label' => 'unarchive',
                'attributes' => [
                    'data-icon' => 'material-icons unarchive',
                    'data-icon-value' => 'unarchive',
                ],
            ],
            [
                'value' => 'material-icons undo',
                'label' => 'undo',
                'attributes' => [
                    'data-icon' => 'material-icons undo',
                    'data-icon-value' => 'undo',
                ],
            ],
            [
                'value' => 'material-icons unfold_less',
                'label' => 'unfold_less',
                'attributes' => [
                    'data-icon' => 'material-icons unfold_less',
                    'data-icon-value' => 'unfold_less',
                ],
            ],
            [
                'value' => 'material-icons unfold_more',
                'label' => 'unfold_more',
                'attributes' => [
                    'data-icon' => 'material-icons unfold_more',
                    'data-icon-value' => 'unfold_more',
                ],
            ],
            [
                'value' => 'material-icons unpublished',
                'label' => 'unpublished',
                'attributes' => [
                    'data-icon' => 'material-icons unpublished',
                    'data-icon-value' => 'unpublished',
                ],
            ],
            [
                'value' => 'material-icons unsubscribe',
                'label' => 'unsubscribe',
                'attributes' => [
                    'data-icon' => 'material-icons unsubscribe',
                    'data-icon-value' => 'unsubscribe',
                ],
            ],
            [
                'value' => 'material-icons upcoming',
                'label' => 'upcoming',
                'attributes' => [
                    'data-icon' => 'material-icons upcoming',
                    'data-icon-value' => 'upcoming',
                ],
            ],
            [
                'value' => 'material-icons update',
                'label' => 'update',
                'attributes' => [
                    'data-icon' => 'material-icons update',
                    'data-icon-value' => 'update',
                ],
            ],
            [
                'value' => 'material-icons update_disabled',
                'label' => 'update_disabled',
                'attributes' => [
                    'data-icon' => 'material-icons update_disabled',
                    'data-icon-value' => 'update_disabled',
                ],
            ],
            [
                'value' => 'material-icons upgrade',
                'label' => 'upgrade',
                'attributes' => [
                    'data-icon' => 'material-icons upgrade',
                    'data-icon-value' => 'upgrade',
                ],
            ],
            [
                'value' => 'material-icons upload',
                'label' => 'upload',
                'attributes' => [
                    'data-icon' => 'material-icons upload',
                    'data-icon-value' => 'upload',
                ],
            ],
            [
                'value' => 'material-icons upload_file',
                'label' => 'upload_file',
                'attributes' => [
                    'data-icon' => 'material-icons upload_file',
                    'data-icon-value' => 'upload_file',
                ],
            ],
            [
                'value' => 'material-icons usb',
                'label' => 'usb',
                'attributes' => [
                    'data-icon' => 'material-icons usb',
                    'data-icon-value' => 'usb',
                ],
            ],
            [
                'value' => 'material-icons usb_off',
                'label' => 'usb_off',
                'attributes' => [
                    'data-icon' => 'material-icons usb_off',
                    'data-icon-value' => 'usb_off',
                ],
            ],
            [
                'value' => 'material-icons vaccines',
                'label' => 'vaccines',
                'attributes' => [
                    'data-icon' => 'material-icons vaccines',
                    'data-icon-value' => 'vaccines',
                ],
            ],
            [
                'value' => 'material-icons vape_free',
                'label' => 'vape_free',
                'attributes' => [
                    'data-icon' => 'material-icons vape_free',
                    'data-icon-value' => 'vape_free',
                ],
            ],
            [
                'value' => 'material-icons vaping_rooms',
                'label' => 'vaping_rooms',
                'attributes' => [
                    'data-icon' => 'material-icons vaping_rooms',
                    'data-icon-value' => 'vaping_rooms',
                ],
            ],
            [
                'value' => 'material-icons verified',
                'label' => 'verified',
                'attributes' => [
                    'data-icon' => 'material-icons verified',
                    'data-icon-value' => 'verified',
                ],
            ],
            [
                'value' => 'material-icons verified_user',
                'label' => 'verified_user',
                'attributes' => [
                    'data-icon' => 'material-icons verified_user',
                    'data-icon-value' => 'verified_user',
                ],
            ],
            [
                'value' => 'material-icons vertical_align_bottom',
                'label' => 'vertical_align_bottom',
                'attributes' => [
                    'data-icon' => 'material-icons vertical_align_bottom',
                    'data-icon-value' => 'vertical_align_bottom',
                ],
            ],
            [
                'value' => 'material-icons vertical_align_center',
                'label' => 'vertical_align_center',
                'attributes' => [
                    'data-icon' => 'material-icons vertical_align_center',
                    'data-icon-value' => 'vertical_align_center',
                ],
            ],
            [
                'value' => 'material-icons vertical_align_top',
                'label' => 'vertical_align_top',
                'attributes' => [
                    'data-icon' => 'material-icons vertical_align_top',
                    'data-icon-value' => 'vertical_align_top',
                ],
            ],
            [
                'value' => 'material-icons vertical_distribute',
                'label' => 'vertical_distribute',
                'attributes' => [
                    'data-icon' => 'material-icons vertical_distribute',
                    'data-icon-value' => 'vertical_distribute',
                ],
            ],
            [
                'value' => 'material-icons vertical_shades',
                'label' => 'vertical_shades',
                'attributes' => [
                    'data-icon' => 'material-icons vertical_shades',
                    'data-icon-value' => 'vertical_shades',
                ],
            ],
            [
                'value' => 'material-icons vertical_shades_closed',
                'label' => 'vertical_shades_closed',
                'attributes' => [
                    'data-icon' => 'material-icons vertical_shades_closed',
                    'data-icon-value' => 'vertical_shades_closed',
                ],
            ],
            [
                'value' => 'material-icons vertical_split',
                'label' => 'vertical_split',
                'attributes' => [
                    'data-icon' => 'material-icons vertical_split',
                    'data-icon-value' => 'vertical_split',
                ],
            ],
            [
                'value' => 'material-icons vibration',
                'label' => 'vibration',
                'attributes' => [
                    'data-icon' => 'material-icons vibration',
                    'data-icon-value' => 'vibration',
                ],
            ],
            [
                'value' => 'material-icons video_call',
                'label' => 'video_call',
                'attributes' => [
                    'data-icon' => 'material-icons video_call',
                    'data-icon-value' => 'video_call',
                ],
            ],
            [
                'value' => 'material-icons video_camera_back',
                'label' => 'video_camera_back',
                'attributes' => [
                    'data-icon' => 'material-icons video_camera_back',
                    'data-icon-value' => 'video_camera_back',
                ],
            ],
            [
                'value' => 'material-icons video_camera_front',
                'label' => 'video_camera_front',
                'attributes' => [
                    'data-icon' => 'material-icons video_camera_front',
                    'data-icon-value' => 'video_camera_front',
                ],
            ],
            [
                'value' => 'material-icons video_collection',
                'label' => 'video_collection',
                'attributes' => [
                    'data-icon' => 'material-icons video_collection',
                    'data-icon-value' => 'video_collection',
                ],
            ],
            [
                'value' => 'material-icons video_file',
                'label' => 'video_file',
                'attributes' => [
                    'data-icon' => 'material-icons video_file',
                    'data-icon-value' => 'video_file',
                ],
            ],
            [
                'value' => 'material-icons video_label',
                'label' => 'video_label',
                'attributes' => [
                    'data-icon' => 'material-icons video_label',
                    'data-icon-value' => 'video_label',
                ],
            ],
            [
                'value' => 'material-icons video_library',
                'label' => 'video_library',
                'attributes' => [
                    'data-icon' => 'material-icons video_library',
                    'data-icon-value' => 'video_library',
                ],
            ],
            [
                'value' => 'material-icons video_settings',
                'label' => 'video_settings',
                'attributes' => [
                    'data-icon' => 'material-icons video_settings',
                    'data-icon-value' => 'video_settings',
                ],
            ],
            [
                'value' => 'material-icons video_stable',
                'label' => 'video_stable',
                'attributes' => [
                    'data-icon' => 'material-icons video_stable',
                    'data-icon-value' => 'video_stable',
                ],
            ],
            [
                'value' => 'material-icons videocam',
                'label' => 'videocam',
                'attributes' => [
                    'data-icon' => 'material-icons videocam',
                    'data-icon-value' => 'videocam',
                ],
            ],
            [
                'value' => 'material-icons videocam_off',
                'label' => 'videocam_off',
                'attributes' => [
                    'data-icon' => 'material-icons videocam_off',
                    'data-icon-value' => 'videocam_off',
                ],
            ],
            [
                'value' => 'material-icons videogame_asset',
                'label' => 'videogame_asset',
                'attributes' => [
                    'data-icon' => 'material-icons videogame_asset',
                    'data-icon-value' => 'videogame_asset',
                ],
            ],
            [
                'value' => 'material-icons videogame_asset_off',
                'label' => 'videogame_asset_off',
                'attributes' => [
                    'data-icon' => 'material-icons videogame_asset_off',
                    'data-icon-value' => 'videogame_asset_off',
                ],
            ],
            [
                'value' => 'material-icons view_agenda',
                'label' => 'view_agenda',
                'attributes' => [
                    'data-icon' => 'material-icons view_agenda',
                    'data-icon-value' => 'view_agenda',
                ],
            ],
            [
                'value' => 'material-icons view_array',
                'label' => 'view_array',
                'attributes' => [
                    'data-icon' => 'material-icons view_array',
                    'data-icon-value' => 'view_array',
                ],
            ],
            [
                'value' => 'material-icons view_carousel',
                'label' => 'view_carousel',
                'attributes' => [
                    'data-icon' => 'material-icons view_carousel',
                    'data-icon-value' => 'view_carousel',
                ],
            ],
            [
                'value' => 'material-icons view_column',
                'label' => 'view_column',
                'attributes' => [
                    'data-icon' => 'material-icons view_column',
                    'data-icon-value' => 'view_column',
                ],
            ],
            [
                'value' => 'material-icons view_comfortable',
                'label' => 'view_comfortable',
                'attributes' => [
                    'data-icon' => 'material-icons view_comfortable',
                    'data-icon-value' => 'view_comfortable',
                ],
            ],
            [
                'value' => 'material-icons view_comfy',
                'label' => 'view_comfy',
                'attributes' => [
                    'data-icon' => 'material-icons view_comfy',
                    'data-icon-value' => 'view_comfy',
                ],
            ],
            [
                'value' => 'material-icons view_comfy_alt',
                'label' => 'view_comfy_alt',
                'attributes' => [
                    'data-icon' => 'material-icons view_comfy_alt',
                    'data-icon-value' => 'view_comfy_alt',
                ],
            ],
            [
                'value' => 'material-icons view_compact',
                'label' => 'view_compact',
                'attributes' => [
                    'data-icon' => 'material-icons view_compact',
                    'data-icon-value' => 'view_compact',
                ],
            ],
            [
                'value' => 'material-icons view_compact_alt',
                'label' => 'view_compact_alt',
                'attributes' => [
                    'data-icon' => 'material-icons view_compact_alt',
                    'data-icon-value' => 'view_compact_alt',
                ],
            ],
            [
                'value' => 'material-icons view_cozy',
                'label' => 'view_cozy',
                'attributes' => [
                    'data-icon' => 'material-icons view_cozy',
                    'data-icon-value' => 'view_cozy',
                ],
            ],
            [
                'value' => 'material-icons view_day',
                'label' => 'view_day',
                'attributes' => [
                    'data-icon' => 'material-icons view_day',
                    'data-icon-value' => 'view_day',
                ],
            ],
            [
                'value' => 'material-icons view_headline',
                'label' => 'view_headline',
                'attributes' => [
                    'data-icon' => 'material-icons view_headline',
                    'data-icon-value' => 'view_headline',
                ],
            ],
            [
                'value' => 'material-icons view_in_ar',
                'label' => 'view_in_ar',
                'attributes' => [
                    'data-icon' => 'material-icons view_in_ar',
                    'data-icon-value' => 'view_in_ar',
                ],
            ],
            [
                'value' => 'material-icons view_kanban',
                'label' => 'view_kanban',
                'attributes' => [
                    'data-icon' => 'material-icons view_kanban',
                    'data-icon-value' => 'view_kanban',
                ],
            ],
            [
                'value' => 'material-icons view_list',
                'label' => 'view_list',
                'attributes' => [
                    'data-icon' => 'material-icons view_list',
                    'data-icon-value' => 'view_list',
                ],
            ],
            [
                'value' => 'material-icons view_module',
                'label' => 'view_module',
                'attributes' => [
                    'data-icon' => 'material-icons view_module',
                    'data-icon-value' => 'view_module',
                ],
            ],
            [
                'value' => 'material-icons view_quilt',
                'label' => 'view_quilt',
                'attributes' => [
                    'data-icon' => 'material-icons view_quilt',
                    'data-icon-value' => 'view_quilt',
                ],
            ],
            [
                'value' => 'material-icons view_sidebar',
                'label' => 'view_sidebar',
                'attributes' => [
                    'data-icon' => 'material-icons view_sidebar',
                    'data-icon-value' => 'view_sidebar',
                ],
            ],
            [
                'value' => 'material-icons view_stream',
                'label' => 'view_stream',
                'attributes' => [
                    'data-icon' => 'material-icons view_stream',
                    'data-icon-value' => 'view_stream',
                ],
            ],
            [
                'value' => 'material-icons view_timeline',
                'label' => 'view_timeline',
                'attributes' => [
                    'data-icon' => 'material-icons view_timeline',
                    'data-icon-value' => 'view_timeline',
                ],
            ],
            [
                'value' => 'material-icons view_week',
                'label' => 'view_week',
                'attributes' => [
                    'data-icon' => 'material-icons view_week',
                    'data-icon-value' => 'view_week',
                ],
            ],
            [
                'value' => 'material-icons vignette',
                'label' => 'vignette',
                'attributes' => [
                    'data-icon' => 'material-icons vignette',
                    'data-icon-value' => 'vignette',
                ],
            ],
            [
                'value' => 'material-icons villa',
                'label' => 'villa',
                'attributes' => [
                    'data-icon' => 'material-icons villa',
                    'data-icon-value' => 'villa',
                ],
            ],
            [
                'value' => 'material-icons visibility',
                'label' => 'visibility',
                'attributes' => [
                    'data-icon' => 'material-icons visibility',
                    'data-icon-value' => 'visibility',
                ],
            ],
            [
                'value' => 'material-icons visibility_off',
                'label' => 'visibility_off',
                'attributes' => [
                    'data-icon' => 'material-icons visibility_off',
                    'data-icon-value' => 'visibility_off',
                ],
            ],
            [
                'value' => 'material-icons voice_chat',
                'label' => 'voice_chat',
                'attributes' => [
                    'data-icon' => 'material-icons voice_chat',
                    'data-icon-value' => 'voice_chat',
                ],
            ],
            [
                'value' => 'material-icons voice_over_off',
                'label' => 'voice_over_off',
                'attributes' => [
                    'data-icon' => 'material-icons voice_over_off',
                    'data-icon-value' => 'voice_over_off',
                ],
            ],
            [
                'value' => 'material-icons voicemail',
                'label' => 'voicemail',
                'attributes' => [
                    'data-icon' => 'material-icons voicemail',
                    'data-icon-value' => 'voicemail',
                ],
            ],
            [
                'value' => 'material-icons volcano',
                'label' => 'volcano',
                'attributes' => [
                    'data-icon' => 'material-icons volcano',
                    'data-icon-value' => 'volcano',
                ],
            ],
            [
                'value' => 'material-icons volume_down',
                'label' => 'volume_down',
                'attributes' => [
                    'data-icon' => 'material-icons volume_down',
                    'data-icon-value' => 'volume_down',
                ],
            ],
            [
                'value' => 'material-icons volume_down_alt',
                'label' => 'volume_down_alt',
                'attributes' => [
                    'data-icon' => 'material-icons volume_down_alt',
                    'data-icon-value' => 'volume_down_alt',
                ],
            ],
            [
                'value' => 'material-icons volume_mute',
                'label' => 'volume_mute',
                'attributes' => [
                    'data-icon' => 'material-icons volume_mute',
                    'data-icon-value' => 'volume_mute',
                ],
            ],
            [
                'value' => 'material-icons volume_off',
                'label' => 'volume_off',
                'attributes' => [
                    'data-icon' => 'material-icons volume_off',
                    'data-icon-value' => 'volume_off',
                ],
            ],
            [
                'value' => 'material-icons volume_up',
                'label' => 'volume_up',
                'attributes' => [
                    'data-icon' => 'material-icons volume_up',
                    'data-icon-value' => 'volume_up',
                ],
            ],
            [
                'value' => 'material-icons volunteer_activism',
                'label' => 'volunteer_activism',
                'attributes' => [
                    'data-icon' => 'material-icons volunteer_activism',
                    'data-icon-value' => 'volunteer_activism',
                ],
            ],
            [
                'value' => 'material-icons vpn_key',
                'label' => 'vpn_key',
                'attributes' => [
                    'data-icon' => 'material-icons vpn_key',
                    'data-icon-value' => 'vpn_key',
                ],
            ],
            [
                'value' => 'material-icons vpn_key_off',
                'label' => 'vpn_key_off',
                'attributes' => [
                    'data-icon' => 'material-icons vpn_key_off',
                    'data-icon-value' => 'vpn_key_off',
                ],
            ],
            [
                'value' => 'material-icons vpn_lock',
                'label' => 'vpn_lock',
                'attributes' => [
                    'data-icon' => 'material-icons vpn_lock',
                    'data-icon-value' => 'vpn_lock',
                ],
            ],
            [
                'value' => 'material-icons vrpano',
                'label' => 'vrpano',
                'attributes' => [
                    'data-icon' => 'material-icons vrpano',
                    'data-icon-value' => 'vrpano',
                ],
            ],
            [
                'value' => 'material-icons wallet',
                'label' => 'wallet',
                'attributes' => [
                    'data-icon' => 'material-icons wallet',
                    'data-icon-value' => 'wallet',
                ],
            ],
            [
                'value' => 'material-icons wallet_giftcard',
                'label' => 'wallet_giftcard',
                'attributes' => [
                    'data-icon' => 'material-icons wallet_giftcard',
                    'data-icon-value' => 'wallet_giftcard',
                ],
            ],
            [
                'value' => 'material-icons wallet_membership',
                'label' => 'wallet_membership',
                'attributes' => [
                    'data-icon' => 'material-icons wallet_membership',
                    'data-icon-value' => 'wallet_membership',
                ],
            ],
            [
                'value' => 'material-icons wallet_travel',
                'label' => 'wallet_travel',
                'attributes' => [
                    'data-icon' => 'material-icons wallet_travel',
                    'data-icon-value' => 'wallet_travel',
                ],
            ],
            [
                'value' => 'material-icons wallpaper',
                'label' => 'wallpaper',
                'attributes' => [
                    'data-icon' => 'material-icons wallpaper',
                    'data-icon-value' => 'wallpaper',
                ],
            ],
            [
                'value' => 'material-icons warehouse',
                'label' => 'warehouse',
                'attributes' => [
                    'data-icon' => 'material-icons warehouse',
                    'data-icon-value' => 'warehouse',
                ],
            ],
            [
                'value' => 'material-icons warning',
                'label' => 'warning',
                'attributes' => [
                    'data-icon' => 'material-icons warning',
                    'data-icon-value' => 'warning',
                ],
            ],
            [
                'value' => 'material-icons warning_amber',
                'label' => 'warning_amber',
                'attributes' => [
                    'data-icon' => 'material-icons warning_amber',
                    'data-icon-value' => 'warning_amber',
                ],
            ],
            [
                'value' => 'material-icons wash',
                'label' => 'wash',
                'attributes' => [
                    'data-icon' => 'material-icons wash',
                    'data-icon-value' => 'wash',
                ],
            ],
            [
                'value' => 'material-icons watch',
                'label' => 'watch',
                'attributes' => [
                    'data-icon' => 'material-icons watch',
                    'data-icon-value' => 'watch',
                ],
            ],
            [
                'value' => 'material-icons watch_later',
                'label' => 'watch_later',
                'attributes' => [
                    'data-icon' => 'material-icons watch_later',
                    'data-icon-value' => 'watch_later',
                ],
            ],
            [
                'value' => 'material-icons watch_off',
                'label' => 'watch_off',
                'attributes' => [
                    'data-icon' => 'material-icons watch_off',
                    'data-icon-value' => 'watch_off',
                ],
            ],
            [
                'value' => 'material-icons water',
                'label' => 'water',
                'attributes' => [
                    'data-icon' => 'material-icons water',
                    'data-icon-value' => 'water',
                ],
            ],
            [
                'value' => 'material-icons water_damage',
                'label' => 'water_damage',
                'attributes' => [
                    'data-icon' => 'material-icons water_damage',
                    'data-icon-value' => 'water_damage',
                ],
            ],
            [
                'value' => 'material-icons water_drop',
                'label' => 'water_drop',
                'attributes' => [
                    'data-icon' => 'material-icons water_drop',
                    'data-icon-value' => 'water_drop',
                ],
            ],
            [
                'value' => 'material-icons waterfall_chart',
                'label' => 'waterfall_chart',
                'attributes' => [
                    'data-icon' => 'material-icons waterfall_chart',
                    'data-icon-value' => 'waterfall_chart',
                ],
            ],
            [
                'value' => 'material-icons waves',
                'label' => 'waves',
                'attributes' => [
                    'data-icon' => 'material-icons waves',
                    'data-icon-value' => 'waves',
                ],
            ],
            [
                'value' => 'material-icons waving_hand',
                'label' => 'waving_hand',
                'attributes' => [
                    'data-icon' => 'material-icons waving_hand',
                    'data-icon-value' => 'waving_hand',
                ],
            ],
            [
                'value' => 'material-icons wb_auto',
                'label' => 'wb_auto',
                'attributes' => [
                    'data-icon' => 'material-icons wb_auto',
                    'data-icon-value' => 'wb_auto',
                ],
            ],
            [
                'value' => 'material-icons wb_cloudy',
                'label' => 'wb_cloudy',
                'attributes' => [
                    'data-icon' => 'material-icons wb_cloudy',
                    'data-icon-value' => 'wb_cloudy',
                ],
            ],
            [
                'value' => 'material-icons wb_incandescent',
                'label' => 'wb_incandescent',
                'attributes' => [
                    'data-icon' => 'material-icons wb_incandescent',
                    'data-icon-value' => 'wb_incandescent',
                ],
            ],
            [
                'value' => 'material-icons wb_iridescent',
                'label' => 'wb_iridescent',
                'attributes' => [
                    'data-icon' => 'material-icons wb_iridescent',
                    'data-icon-value' => 'wb_iridescent',
                ],
            ],
            [
                'value' => 'material-icons wb_shade',
                'label' => 'wb_shade',
                'attributes' => [
                    'data-icon' => 'material-icons wb_shade',
                    'data-icon-value' => 'wb_shade',
                ],
            ],
            [
                'value' => 'material-icons wb_sunny',
                'label' => 'wb_sunny',
                'attributes' => [
                    'data-icon' => 'material-icons wb_sunny',
                    'data-icon-value' => 'wb_sunny',
                ],
            ],
            [
                'value' => 'material-icons wb_twighlight',
                'label' => 'wb_twighlight',
                'attributes' => [
                    'data-icon' => 'material-icons wb_twighlight',
                    'data-icon-value' => 'wb_twighlight',
                ],
            ],
            [
                'value' => 'material-icons wb_twilight',
                'label' => 'wb_twilight',
                'attributes' => [
                    'data-icon' => 'material-icons wb_twilight',
                    'data-icon-value' => 'wb_twilight',
                ],
            ],
            [
                'value' => 'material-icons wc',
                'label' => 'wc',
                'attributes' => [
                    'data-icon' => 'material-icons wc',
                    'data-icon-value' => 'wc',
                ],
            ],
            [
                'value' => 'material-icons web',
                'label' => 'web',
                'attributes' => [
                    'data-icon' => 'material-icons web',
                    'data-icon-value' => 'web',
                ],
            ],
            [
                'value' => 'material-icons web_asset',
                'label' => 'web_asset',
                'attributes' => [
                    'data-icon' => 'material-icons web_asset',
                    'data-icon-value' => 'web_asset',
                ],
            ],
            [
                'value' => 'material-icons web_asset_off',
                'label' => 'web_asset_off',
                'attributes' => [
                    'data-icon' => 'material-icons web_asset_off',
                    'data-icon-value' => 'web_asset_off',
                ],
            ],
            [
                'value' => 'material-icons web_stories',
                'label' => 'web_stories',
                'attributes' => [
                    'data-icon' => 'material-icons web_stories',
                    'data-icon-value' => 'web_stories',
                ],
            ],
            [
                'value' => 'material-icons webhook',
                'label' => 'webhook',
                'attributes' => [
                    'data-icon' => 'material-icons webhook',
                    'data-icon-value' => 'webhook',
                ],
            ],
            [
                'value' => 'material-icons wechat',
                'label' => 'wechat',
                'attributes' => [
                    'data-icon' => 'material-icons wechat',
                    'data-icon-value' => 'wechat',
                ],
            ],
            [
                'value' => 'material-icons weekend',
                'label' => 'weekend',
                'attributes' => [
                    'data-icon' => 'material-icons weekend',
                    'data-icon-value' => 'weekend',
                ],
            ],
            [
                'value' => 'material-icons west',
                'label' => 'west',
                'attributes' => [
                    'data-icon' => 'material-icons west',
                    'data-icon-value' => 'west',
                ],
            ],
            [
                'value' => 'material-icons whatsapp',
                'label' => 'whatsapp',
                'attributes' => [
                    'data-icon' => 'material-icons whatsapp',
                    'data-icon-value' => 'whatsapp',
                ],
            ],
            [
                'value' => 'material-icons whatshot',
                'label' => 'whatshot',
                'attributes' => [
                    'data-icon' => 'material-icons whatshot',
                    'data-icon-value' => 'whatshot',
                ],
            ],
            [
                'value' => 'material-icons wheelchair_pickup',
                'label' => 'wheelchair_pickup',
                'attributes' => [
                    'data-icon' => 'material-icons wheelchair_pickup',
                    'data-icon-value' => 'wheelchair_pickup',
                ],
            ],
            [
                'value' => 'material-icons where_to_vote',
                'label' => 'where_to_vote',
                'attributes' => [
                    'data-icon' => 'material-icons where_to_vote',
                    'data-icon-value' => 'where_to_vote',
                ],
            ],
            [
                'value' => 'material-icons widgets',
                'label' => 'widgets',
                'attributes' => [
                    'data-icon' => 'material-icons widgets',
                    'data-icon-value' => 'widgets',
                ],
            ],
            [
                'value' => 'material-icons width_full',
                'label' => 'width_full',
                'attributes' => [
                    'data-icon' => 'material-icons width_full',
                    'data-icon-value' => 'width_full',
                ],
            ],
            [
                'value' => 'material-icons width_normal',
                'label' => 'width_normal',
                'attributes' => [
                    'data-icon' => 'material-icons width_normal',
                    'data-icon-value' => 'width_normal',
                ],
            ],
            [
                'value' => 'material-icons width_wide',
                'label' => 'width_wide',
                'attributes' => [
                    'data-icon' => 'material-icons width_wide',
                    'data-icon-value' => 'width_wide',
                ],
            ],
            [
                'value' => 'material-icons wifi',
                'label' => 'wifi',
                'attributes' => [
                    'data-icon' => 'material-icons wifi',
                    'data-icon-value' => 'wifi',
                ],
            ],
            [
                'value' => 'material-icons wifi_1_bar',
                'label' => 'wifi_1_bar',
                'attributes' => [
                    'data-icon' => 'material-icons wifi_1_bar',
                    'data-icon-value' => 'wifi_1_bar',
                ],
            ],
            [
                'value' => 'material-icons wifi_2_bar',
                'label' => 'wifi_2_bar',
                'attributes' => [
                    'data-icon' => 'material-icons wifi_2_bar',
                    'data-icon-value' => 'wifi_2_bar',
                ],
            ],
            [
                'value' => 'material-icons wifi_calling',
                'label' => 'wifi_calling',
                'attributes' => [
                    'data-icon' => 'material-icons wifi_calling',
                    'data-icon-value' => 'wifi_calling',
                ],
            ],
            [
                'value' => 'material-icons wifi_calling_3',
                'label' => 'wifi_calling_3',
                'attributes' => [
                    'data-icon' => 'material-icons wifi_calling_3',
                    'data-icon-value' => 'wifi_calling_3',
                ],
            ],
            [
                'value' => 'material-icons wifi_channel',
                'label' => 'wifi_channel',
                'attributes' => [
                    'data-icon' => 'material-icons wifi_channel',
                    'data-icon-value' => 'wifi_channel',
                ],
            ],
            [
                'value' => 'material-icons wifi_find',
                'label' => 'wifi_find',
                'attributes' => [
                    'data-icon' => 'material-icons wifi_find',
                    'data-icon-value' => 'wifi_find',
                ],
            ],
            [
                'value' => 'material-icons wifi_lock',
                'label' => 'wifi_lock',
                'attributes' => [
                    'data-icon' => 'material-icons wifi_lock',
                    'data-icon-value' => 'wifi_lock',
                ],
            ],
            [
                'value' => 'material-icons wifi_off',
                'label' => 'wifi_off',
                'attributes' => [
                    'data-icon' => 'material-icons wifi_off',
                    'data-icon-value' => 'wifi_off',
                ],
            ],
            [
                'value' => 'material-icons wifi_password',
                'label' => 'wifi_password',
                'attributes' => [
                    'data-icon' => 'material-icons wifi_password',
                    'data-icon-value' => 'wifi_password',
                ],
            ],
            [
                'value' => 'material-icons wifi_protected_setup',
                'label' => 'wifi_protected_setup',
                'attributes' => [
                    'data-icon' => 'material-icons wifi_protected_setup',
                    'data-icon-value' => 'wifi_protected_setup',
                ],
            ],
            [
                'value' => 'material-icons wifi_tethering',
                'label' => 'wifi_tethering',
                'attributes' => [
                    'data-icon' => 'material-icons wifi_tethering',
                    'data-icon-value' => 'wifi_tethering',
                ],
            ],
            [
                'value' => 'material-icons wifi_tethering_error',
                'label' => 'wifi_tethering_error',
                'attributes' => [
                    'data-icon' => 'material-icons wifi_tethering_error',
                    'data-icon-value' => 'wifi_tethering_error',
                ],
            ],
            [
                'value' => 'material-icons wifi_tethering_error_rounded',
                'label' => 'wifi_tethering_error_rounded',
                'attributes' => [
                    'data-icon' => 'material-icons wifi_tethering_error_rounded',
                    'data-icon-value' => 'wifi_tethering_error_rounded',
                ],
            ],
            [
                'value' => 'material-icons wifi_tethering_off',
                'label' => 'wifi_tethering_off',
                'attributes' => [
                    'data-icon' => 'material-icons wifi_tethering_off',
                    'data-icon-value' => 'wifi_tethering_off',
                ],
            ],
            [
                'value' => 'material-icons wind_power',
                'label' => 'wind_power',
                'attributes' => [
                    'data-icon' => 'material-icons wind_power',
                    'data-icon-value' => 'wind_power',
                ],
            ],
            [
                'value' => 'material-icons window',
                'label' => 'window',
                'attributes' => [
                    'data-icon' => 'material-icons window',
                    'data-icon-value' => 'window',
                ],
            ],
            [
                'value' => 'material-icons wine_bar',
                'label' => 'wine_bar',
                'attributes' => [
                    'data-icon' => 'material-icons wine_bar',
                    'data-icon-value' => 'wine_bar',
                ],
            ],
            [
                'value' => 'material-icons woman',
                'label' => 'woman',
                'attributes' => [
                    'data-icon' => 'material-icons woman',
                    'data-icon-value' => 'woman',
                ],
            ],
            [
                'value' => 'material-icons woo_commerce',
                'label' => 'woo_commerce',
                'attributes' => [
                    'data-icon' => 'material-icons woo_commerce',
                    'data-icon-value' => 'woo_commerce',
                ],
            ],
            [
                'value' => 'material-icons wordpress',
                'label' => 'wordpress',
                'attributes' => [
                    'data-icon' => 'material-icons wordpress',
                    'data-icon-value' => 'wordpress',
                ],
            ],
            [
                'value' => 'material-icons work',
                'label' => 'work',
                'attributes' => [
                    'data-icon' => 'material-icons work',
                    'data-icon-value' => 'work',
                ],
            ],
            [
                'value' => 'material-icons work_history',
                'label' => 'work_history',
                'attributes' => [
                    'data-icon' => 'material-icons work_history',
                    'data-icon-value' => 'work_history',
                ],
            ],
            [
                'value' => 'material-icons work_off',
                'label' => 'work_off',
                'attributes' => [
                    'data-icon' => 'material-icons work_off',
                    'data-icon-value' => 'work_off',
                ],
            ],
            [
                'value' => 'material-icons work_outline',
                'label' => 'work_outline',
                'attributes' => [
                    'data-icon' => 'material-icons work_outline',
                    'data-icon-value' => 'work_outline',
                ],
            ],
            [
                'value' => 'material-icons workspace_premium',
                'label' => 'workspace_premium',
                'attributes' => [
                    'data-icon' => 'material-icons workspace_premium',
                    'data-icon-value' => 'workspace_premium',
                ],
            ],
            [
                'value' => 'material-icons workspaces',
                'label' => 'workspaces',
                'attributes' => [
                    'data-icon' => 'material-icons workspaces',
                    'data-icon-value' => 'workspaces',
                ],
            ],
            [
                'value' => 'material-icons workspaces_filled',
                'label' => 'workspaces_filled',
                'attributes' => [
                    'data-icon' => 'material-icons workspaces_filled',
                    'data-icon-value' => 'workspaces_filled',
                ],
            ],
            [
                'value' => 'material-icons workspaces_outline',
                'label' => 'workspaces_outline',
                'attributes' => [
                    'data-icon' => 'material-icons workspaces_outline',
                    'data-icon-value' => 'workspaces_outline',
                ],
            ],
            [
                'value' => 'material-icons wrap_text',
                'label' => 'wrap_text',
                'attributes' => [
                    'data-icon' => 'material-icons wrap_text',
                    'data-icon-value' => 'wrap_text',
                ],
            ],
            [
                'value' => 'material-icons wrong_location',
                'label' => 'wrong_location',
                'attributes' => [
                    'data-icon' => 'material-icons wrong_location',
                    'data-icon-value' => 'wrong_location',
                ],
            ],
            [
                'value' => 'material-icons wysiwyg',
                'label' => 'wysiwyg',
                'attributes' => [
                    'data-icon' => 'material-icons wysiwyg',
                    'data-icon-value' => 'wysiwyg',
                ],
            ],
            [
                'value' => 'material-icons yard',
                'label' => 'yard',
                'attributes' => [
                    'data-icon' => 'material-icons yard',
                    'data-icon-value' => 'yard',
                ],
            ],
            [
                'value' => 'material-icons youtube_searched_for',
                'label' => 'youtube_searched_for',
                'attributes' => [
                    'data-icon' => 'material-icons youtube_searched_for',
                    'data-icon-value' => 'youtube_searched_for',
                ],
            ],
            [
                'value' => 'material-icons zoom_in',
                'label' => 'zoom_in',
                'attributes' => [
                    'data-icon' => 'material-icons zoom_in',
                    'data-icon-value' => 'zoom_in',
                ],
            ],
            [
                'value' => 'material-icons zoom_in_map',
                'label' => 'zoom_in_map',
                'attributes' => [
                    'data-icon' => 'material-icons zoom_in_map',
                    'data-icon-value' => 'zoom_in_map',
                ],
            ],
            [
                'value' => 'material-icons zoom_out',
                'label' => 'zoom_out',
                'attributes' => [
                    'data-icon' => 'material-icons zoom_out',
                    'data-icon-value' => 'zoom_out',
                ],
            ],
            [
                'value' => 'material-icons zoom_out_map',
                'label' => 'zoom_out_map',
                'attributes' => [
                    'data-icon' => 'material-icons zoom_out_map',
                    'data-icon-value' => 'zoom_out_map',
                ],
            ],
        ];

        return $laIcons;
    }
}

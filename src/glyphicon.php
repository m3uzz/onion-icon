<?php
/**
 * This file is part of Onion Library
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionIcon
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-icon
 */
declare (strict_types = 1);

namespace OnionIcon;

class glyphicon
{
	/**
	 * 
	 * @return array
	 */
	public static function icons () : array
	{
        $laIcons = [
            [
                'value' => 'glyphicon glyphicon-adjust',
                'label' => 'adjust',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-adjust',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-alert',
                'label' => 'alert',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-alert',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-align-center',
                'label' => 'align-center',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-align-center',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-align-justify',
                'label' => 'align-justify',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-align-justify',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-align-left',
                'label' => 'align-left',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-align-left',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-align-right',
                'label' => 'align-right',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-align-right',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-apple',
                'label' => 'apple',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-apple',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-arrow-down',
                'label' => 'arrow-down',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-arrow-down',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-arrow-left',
                'label' => 'arrow-left',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-arrow-left',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-arrow-right',
                'label' => 'arrow-right',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-arrow-right',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-arrow-up',
                'label' => 'arrow-up',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-arrow-up',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-asterisk',
                'label' => 'asterisk',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-asterisk',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-baby-formula',
                'label' => 'baby-formula',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-baby-formula',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-backward',
                'label' => 'backward',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-backward',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-ban-circle',
                'label' => 'ban-circle',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-ban-circle',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-barcode',
                'label' => 'barcode',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-barcode',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-bed',
                'label' => 'bed',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-bed',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-bell',
                'label' => 'bell',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-bell',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-bishop',
                'label' => 'bishop',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-bishop',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-bitcoin',
                'label' => 'bitcoin',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-bitcoin',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-blackboard',
                'label' => 'blackboard',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-blackboard',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-bold',
                'label' => 'bold',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-bold',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-book',
                'label' => 'book',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-book',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-bookmark',
                'label' => 'bookmark',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-bookmark',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-briefcase',
                'label' => 'briefcase',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-briefcase',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-btc',
                'label' => 'btc',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-btc',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-bullhorn',
                'label' => 'bullhorn',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-bullhorn',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-calendar',
                'label' => 'calendar',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-calendar',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-camera',
                'label' => 'camera',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-camera',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-cd',
                'label' => 'cd',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-cd',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-certificate',
                'label' => 'certificate',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-certificate',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-check',
                'label' => 'check',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-check',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-chevron-down',
                'label' => 'chevron-down',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-chevron-down',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-chevron-left',
                'label' => 'chevron-left',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-chevron-left',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-chevron-right',
                'label' => 'chevron-right',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-chevron-right',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-chevron-up',
                'label' => 'chevron-up',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-chevron-up',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-circle-arrow-down',
                'label' => 'circle-arrow-down',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-circle-arrow-down',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-circle-arrow-left',
                'label' => 'circle-arrow-left',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-circle-arrow-left',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-circle-arrow-right',
                'label' => 'circle-arrow-right',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-circle-arrow-right',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-circle-arrow-up',
                'label' => 'circle-arrow-up',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-circle-arrow-up',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-cloud',
                'label' => 'cloud',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-cloud',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-cloud-download',
                'label' => 'cloud-download',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-cloud-download',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-cloud-upload',
                'label' => 'cloud-upload',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-cloud-upload',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-cog',
                'label' => 'cog',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-cog',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-collapse-down',
                'label' => 'collapse-down',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-collapse-down',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-collapse-up',
                'label' => 'collapse-up',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-collapse-up',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-comment',
                'label' => 'comment',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-comment',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-compressed',
                'label' => 'compressed',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-compressed',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-console',
                'label' => 'console',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-console',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-copy',
                'label' => 'copy',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-copy',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-copyright-mark',
                'label' => 'copyright-mark',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-copyright-mark',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-credit-card',
                'label' => 'credit-card',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-credit-card',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-cutlery',
                'label' => 'cutlery',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-cutlery',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-dashboard',
                'label' => 'dashboard',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-dashboard',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-download',
                'label' => 'download',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-download',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-download-alt',
                'label' => 'download-alt',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-download-alt',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-duplicate',
                'label' => 'duplicate',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-duplicate',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-earphone',
                'label' => 'earphone',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-earphone',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-edit',
                'label' => 'edit',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-edit',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-education',
                'label' => 'education',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-education',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-eject',
                'label' => 'eject',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-eject',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-envelope',
                'label' => 'envelope',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-envelope',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-equalizer',
                'label' => 'equalizer',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-equalizer',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-erase',
                'label' => 'erase',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-erase',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-eur',
                'label' => 'eur',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-eur',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-euro',
                'label' => 'euro',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-euro',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-exclamation-sign',
                'label' => 'exclamation-sign',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-exclamation-sign',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-expand',
                'label' => 'expand',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-expand',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-export',
                'label' => 'export',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-export',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-eye-close',
                'label' => 'eye-close',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-eye-close',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-eye-open',
                'label' => 'eye-open',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-eye-open',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-facetime-video',
                'label' => 'facetime-video',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-facetime-video',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-fast-backward',
                'label' => 'fast-backward',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-fast-backward',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-fast-forward',
                'label' => 'fast-forward',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-fast-forward',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-file',
                'label' => 'file',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-file',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-film',
                'label' => 'film',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-film',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-filter',
                'label' => 'filter',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-filter',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-fire',
                'label' => 'fire',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-fire',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-flag',
                'label' => 'flag',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-flag',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-flash',
                'label' => 'flash',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-flash',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-floppy-disk',
                'label' => 'floppy-disk',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-floppy-disk',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-floppy-open',
                'label' => 'floppy-open',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-floppy-open',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-floppy-remove',
                'label' => 'floppy-remove',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-floppy-remove',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-floppy-save',
                'label' => 'floppy-save',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-floppy-save',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-floppy-saved',
                'label' => 'floppy-saved',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-floppy-saved',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-folder-close',
                'label' => 'folder-close',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-folder-close',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-folder-open',
                'label' => 'folder-open',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-folder-open',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-font',
                'label' => 'font',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-font',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-forward',
                'label' => 'forward',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-forward',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-fullscreen',
                'label' => 'fullscreen',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-fullscreen',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-gbp',
                'label' => 'gbp',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-gbp',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-gift',
                'label' => 'gift',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-gift',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-glass',
                'label' => 'glass',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-glass',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-globe',
                'label' => 'globe',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-globe',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-grain',
                'label' => 'grain',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-grain',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-hand-down',
                'label' => 'hand-down',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-hand-down',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-hand-left',
                'label' => 'hand-left',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-hand-left',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-hand-right',
                'label' => 'hand-right',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-hand-right',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-hand-up',
                'label' => 'hand-up',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-hand-up',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-hd-video',
                'label' => 'hd-video',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-hd-video',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-hdd',
                'label' => 'hdd',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-hdd',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-header',
                'label' => 'header',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-header',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-headphones',
                'label' => 'headphones',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-headphones',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-heart',
                'label' => 'heart',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-heart',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-heart-empty',
                'label' => 'heart-empty',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-heart-empty',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-home',
                'label' => 'home',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-home',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-hourglass',
                'label' => 'hourglass',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-hourglass',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-ice-lolly',
                'label' => 'ice-lolly',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-ice-lolly',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-ice-lolly-tasted',
                'label' => 'ice-lolly-tasted',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-ice-lolly-tasted',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-import',
                'label' => 'import',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-import',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-inbox',
                'label' => 'inbox',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-inbox',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-indent-left',
                'label' => 'indent-left',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-indent-left',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-indent-right',
                'label' => 'indent-right',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-indent-right',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-info-sign',
                'label' => 'info-sign',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-info-sign',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-italic',
                'label' => 'italic',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-italic',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-jpy',
                'label' => 'jpy',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-jpy',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-king',
                'label' => 'king',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-king',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-knight',
                'label' => 'knight',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-knight',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-lamp',
                'label' => 'lamp',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-lamp',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-leaf',
                'label' => 'leaf',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-leaf',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-level-up',
                'label' => 'level-up',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-level-up',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-link',
                'label' => 'link',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-link',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-list',
                'label' => 'list',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-list',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-list-alt',
                'label' => 'list-alt',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-list-alt',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-lock',
                'label' => 'lock',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-lock',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-log-in',
                'label' => 'log-in',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-log-in',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-log-out',
                'label' => 'log-out',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-log-out',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-magnet',
                'label' => 'magnet',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-magnet',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-map-marker',
                'label' => 'map-marker',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-map-marker',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-menu-down',
                'label' => 'menu-down',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-menu-down',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-menu-hamburger',
                'label' => 'menu-hamburger',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-menu-hamburger',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-menu-left',
                'label' => 'menu-left',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-menu-left',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-menu-right',
                'label' => 'menu-right',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-menu-right',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-menu-up',
                'label' => 'menu-up',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-menu-up',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-minus',
                'label' => 'minus',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-minus',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-minus-sign',
                'label' => 'minus-sign',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-minus-sign',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-modal-window',
                'label' => 'modal-window',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-modal-window',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-move',
                'label' => 'move',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-move',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-music',
                'label' => 'music',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-music',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-new-window',
                'label' => 'new-window',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-new-window',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-object-align-bottom',
                'label' => 'object-align-bottom',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-object-align-bottom',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-object-align-horizontal',
                'label' => 'object-align-horizontal',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-object-align-horizontal',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-object-align-left',
                'label' => 'object-align-left',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-object-align-left',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-object-align-right',
                'label' => 'object-align-right',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-object-align-right',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-object-align-top',
                'label' => 'object-align-top',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-object-align-top',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-object-align-vertical',
                'label' => 'object-align-vertical',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-object-align-vertical',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-off',
                'label' => 'off',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-off',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-oil',
                'label' => 'oil',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-oil',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-ok',
                'label' => 'ok',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-ok',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-ok-circle',
                'label' => 'ok-circle',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-ok-circle',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-ok-sign',
                'label' => 'ok-sign',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-ok-sign',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-open',
                'label' => 'open',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-open',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-open-file',
                'label' => 'open-file',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-open-file',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-option-horizontal',
                'label' => 'option-horizontal',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-option-horizontal',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-option-vertical',
                'label' => 'option-vertical',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-option-vertical',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-paperclip',
                'label' => 'paperclip',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-paperclip',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-paste',
                'label' => 'paste',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-paste',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-pause',
                'label' => 'pause',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-pause',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-pawn',
                'label' => 'pawn',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-pawn',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-pencil',
                'label' => 'pencil',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-pencil',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-phone',
                'label' => 'phone',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-phone',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-phone-alt',
                'label' => 'phone-alt',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-phone-alt',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-picture',
                'label' => 'picture',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-picture',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-piggy-bank',
                'label' => 'piggy-bank',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-piggy-bank',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-plane',
                'label' => 'plane',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-plane',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-play',
                'label' => 'play',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-play',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-play-circle',
                'label' => 'play-circle',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-play-circle',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-plus',
                'label' => 'plus',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-plus',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-plus-sign',
                'label' => 'plus-sign',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-plus-sign',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-print',
                'label' => 'print',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-print',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-pushpin',
                'label' => 'pushpin',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-pushpin',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-qrcode',
                'label' => 'qrcode',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-qrcode',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-queen',
                'label' => 'queen',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-queen',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-question-sign',
                'label' => 'question-sign',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-question-sign',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-random',
                'label' => 'random',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-random',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-record',
                'label' => 'record',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-record',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-refresh',
                'label' => 'refresh',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-refresh',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-registration-mark',
                'label' => 'registration-mark',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-registration-mark',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-remove',
                'label' => 'remove',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-remove',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-remove-circle',
                'label' => 'remove-circle',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-remove-circle',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-remove-sign',
                'label' => 'remove-sign',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-remove-sign',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-repeat',
                'label' => 'repeat',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-repeat',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-resize-full',
                'label' => 'resize-full',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-resize-full',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-resize-horizontal',
                'label' => 'resize-horizontal',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-resize-horizontal',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-resize-small',
                'label' => 'resize-small',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-resize-small',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-resize-vertical',
                'label' => 'resize-vertical',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-resize-vertical',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-retweet',
                'label' => 'retweet',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-retweet',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-road',
                'label' => 'road',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-road',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-rub',
                'label' => 'rub',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-rub',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-ruble',
                'label' => 'ruble',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-ruble',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-save',
                'label' => 'save',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-save',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-save-file',
                'label' => 'save-file',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-save-file',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-saved',
                'label' => 'saved',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-saved',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-scale',
                'label' => 'scale',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-scale',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-scissors',
                'label' => 'scissors',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-scissors',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-screenshot',
                'label' => 'screenshot',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-screenshot',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-sd-video',
                'label' => 'sd-video',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-sd-video',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-search',
                'label' => 'search',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-search',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-send',
                'label' => 'send',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-send',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-share',
                'label' => 'share',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-share',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-share-alt',
                'label' => 'share-alt',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-share-alt',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-shopping-cart',
                'label' => 'shopping-cart',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-shopping-cart',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-signal',
                'label' => 'signal',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-signal',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-sort',
                'label' => 'sort',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-sort',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-sort-by-alphabet',
                'label' => 'sort-by-alphabet',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-sort-by-alphabet',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-sort-by-alphabet-alt',
                'label' => 'sort-by-alphabet-alt',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-sort-by-alphabet-alt',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-sort-by-attributes',
                'label' => 'sort-by-attributes',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-sort-by-attributes',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-sort-by-attributes-alt',
                'label' => 'sort-by-attributes-alt',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-sort-by-attributes-alt',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-sort-by-order',
                'label' => 'sort-by-order',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-sort-by-order',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-sort-by-order-alt',
                'label' => 'sort-by-order-alt',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-sort-by-order-alt',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-sound-5-1',
                'label' => 'sound-5-1',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-sound-5-1',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-sound-6-1',
                'label' => 'sound-6-1',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-sound-6-1',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-sound-7-1',
                'label' => 'sound-7-1',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-sound-7-1',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-sound-dolby',
                'label' => 'sound-dolby',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-sound-dolby',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-sound-stereo',
                'label' => 'sound-stereo',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-sound-stereo',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-star',
                'label' => 'star',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-star',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-star-empty',
                'label' => 'star-empty',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-star-empty',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-stats',
                'label' => 'stats',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-stats',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-step-backward',
                'label' => 'step-backward',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-step-backward',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-step-forward',
                'label' => 'step-forward',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-step-forward',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-stop',
                'label' => 'stop',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-stop',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-subscript',
                'label' => 'subscript',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-subscript',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-subtitles',
                'label' => 'subtitles',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-subtitles',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-sunglasses',
                'label' => 'sunglasses',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-sunglasses',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-superscript',
                'label' => 'superscript',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-superscript',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-tag',
                'label' => 'tag',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-tag',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-tags',
                'label' => 'tags',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-tags',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-tasks',
                'label' => 'tasks',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-tasks',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-tent',
                'label' => 'tent',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-tent',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-text-background',
                'label' => 'text-background',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-text-background',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-text-color',
                'label' => 'text-color',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-text-color',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-text-height',
                'label' => 'text-height',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-text-height',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-text-size',
                'label' => 'text-size',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-text-size',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-text-width',
                'label' => 'text-width',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-text-width',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-th',
                'label' => 'th',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-th',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-th-large',
                'label' => 'th-large',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-th-large',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-th-list',
                'label' => 'th-list',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-th-list',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-thumbs-down',
                'label' => 'thumbs-down',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-thumbs-down',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-thumbs-up',
                'label' => 'thumbs-up',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-thumbs-up',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-time',
                'label' => 'time',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-time',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-tint',
                'label' => 'tint',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-tint',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-tower',
                'label' => 'tower',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-tower',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-transfer',
                'label' => 'transfer',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-transfer',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-trash',
                'label' => 'trash',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-trash',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-tree-conifer',
                'label' => 'tree-conifer',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-tree-conifer',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-tree-deciduous',
                'label' => 'tree-deciduous',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-tree-deciduous',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-triangle-bottom',
                'label' => 'triangle-bottom',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-triangle-bottom',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-triangle-left',
                'label' => 'triangle-left',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-triangle-left',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-triangle-right',
                'label' => 'triangle-right',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-triangle-right',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-triangle-top',
                'label' => 'triangle-top',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-triangle-top',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-unchecked',
                'label' => 'unchecked',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-unchecked',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-upload',
                'label' => 'upload',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-upload',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-usd',
                'label' => 'usd',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-usd',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-user',
                'label' => 'user',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-user',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-volume-down',
                'label' => 'volume-down',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-volume-down',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-volume-off',
                'label' => 'volume-off',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-volume-off',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-volume-up',
                'label' => 'volume-up',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-volume-up',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-warning-sign',
                'label' => 'warning-sign',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-warning-sign',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-wrench',
                'label' => 'wrench',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-wrench',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-xbt',
                'label' => 'xbt',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-xbt',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-yen',
                'label' => 'yen',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-yen',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-zoom-in',
                'label' => 'zoom-in',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-zoom-in',
                ],
            ],
            [
                'value' => 'glyphicon glyphicon-zoom-out',
                'label' => 'zoom-out',
                'attributes' => [
                    'data-icon' => 'glyphicon glyphicon-zoom-out',
                ],
            ],
        ];
        
        return $laIcons;	
    }
}
<?php
/**
 * This file is part of Onion Library
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionIcon
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-icon
 */
declare (strict_types = 1);

namespace OnionIcon;
use OnionLib\ArrayObject;
use OnionLib\Debug;


class IconFont 
{
	/**
	 *
	 * @param string|null $psIcon
	 * @param bool $pbWithCaret
	 * @param string $psClass
     * @param string|null $psStyle
	 * @return string
	 */
    public static function renderIcon (?string $psIcon = null, $pbWithCaret = false, string $psClass = '', ?string $psStyle = null) : string
	{
		$lsCaret = '';
		$lsStyle = '';
		$lsClass = '';
		
		if ($pbWithCaret)
		{
			$lsCaret = '<b class="caret"></b>';
		}
		
		if (!is_null($psStyle))
		{
		    $lsStyle = 'style="' . $psStyle. '"';
		}
				
		if (!empty($psIcon))
		{
			if (strstr($psIcon, 'glyphicon'))
			{
			    return '<i class="mr-1 ' . $psIcon . ' ' . $psClass . '" ' . $lsStyle . '></i>' . $lsCaret . ' ';
			}
			elseif (strstr($psIcon, 'material-icons'))
			{
				$laIcon = explode(" ", $psIcon);
				$lsIcon = array_pop($laIcon);
				$lsClass = implode(" ", $laIcon);
				
				return '<i class="mr-1 ' . $lsClass . ' ' . $psClass . '" ' . $lsStyle . '>' . $lsIcon . '</i>' . $lsCaret . ' ';
			}
            elseif (strstr($psIcon, 'bi'))
			{
			    return '<i class="mr-1 ' . $psIcon . ' ' . $psClass . '" ' . $lsStyle . '></i>' . $lsCaret . ' ';
			}
            elseif (strstr($psIcon, 'fa'))
			{
			    return '<i class="mr-1 ' . $psIcon . ' ' . $psClass . '" ' . $lsStyle . '></i>' . $lsCaret . ' ';
			}
		}
		
		return '';
	}
	

	/**
	 *
     * @param string $psType
	 * @return array
	 */
	public static function getIcons (string $psType = 'all') : array
    {
	    switch ($psType)
	    {
            case 'material':
	            return material::icons();
                break;
            case 'fontawesome':
	            return awesome::icons();
	           break;
            case 'bootstrap':
	            return bootstrap::icons();
	            break;
            case 'glyphicon':
	            return glyphicon::icons();
	            break;
            default:
                $laM = material::icons();   
                $laF = awesome::icons();
                $laB = bootstrap::icons();
                $laG = glyphicon::icons();
                
                $laIcons = array_merge($laM, $laF, $laB, $laG);
                
                return $laIcons;
	    }
	}    


    /**
     * 
     */
    public static function generateMaterial () : void
    {
        $laIcons = include('materialicons.php');

        $laReturn = [];

        foreach ($laIcons as $lsIcon)
        {
            $laReturn[] = [
                'value' => 'material-icons ' . $lsIcon,
                'label' => $lsIcon,
                'attributes' => [
                    'data-icon' => 'material-icons ' . $lsIcon,
                    'data-icon-value' => $lsIcon
                ]
            ];
        }

        Debug::displayd(ArrayObject::arrayToString($laReturn, "\t", true));
    }


    /**
     * 
     */
    public static function generateIcons () : void
    {
        $laIcons = include('bootstrapicons.php');

        $laReturn = [];

        foreach ($laIcons as $lsIcon)
        {
            $laReturn[] = [
                'value' => 'bi bi-' . $lsIcon,
                'label' => $lsIcon,
                'attributes' => [
                    'data-icon' => 'bi bi-' . $lsIcon,
                ]
            ];
        }

        Debug::displayd(ArrayObject::arrayToString($laReturn, "\t", true));
    }
    
    
    /**
     * 
     */
    public static function generateAwesome () : void
    {
        $lsJson = include('awesomeicons.php');
        $laIcons = json_decode($lsJson, true);

        $laReturn = [];

        foreach ($laIcons as $lsName => $laIcon)
        {
            if (is_array($laIcon['styles']))
            {
                foreach ($laIcon['styles'] as $lsSt)
                {
                    $lsStyle = 'fas';

                    switch ($lsSt)
                    {
                        case 'brands':
                            $lsStyle = 'fab';
                            break;
                        case 'solid':
                            $lsStyle = 'fas';
                            break;
                        case 'regular':
                            $lsStyle = 'far';
                            break;
                        case 'duotone':
                            $lsStyle = 'fad';
                            break;
                        case 'light':
                            $lsStyle = 'fal';
                            break;
                    }
        
                    $laReturn[] = [
                        'value' => "{$lsStyle} fa-{$lsName}",
                        'label' => $laIcon['label'],
                        'attributes' => [
                            'data-icon' => "{$lsStyle} fa-{$lsName}"
                        ]
                    ];       
                }
            }
        }

        Debug::displayd(ArrayObject::arrayToString($laReturn, "\t", true));
    }
}
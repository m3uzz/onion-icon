<?php
/**
 * This file is part of Onion Library
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionIcon
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-icon
 */

declare(strict_types=1);

namespace OnionIcon;

class bootstrap
{
    /**
     *
     * @return array
     */
    public static function icons(): array
    {
        $laIcons = [
            '0' => [
                'value' => 'bi bi-123',
                'label' => '123',
                'attributes' => [
                    'data-icon' => 'bi bi-123',
                ],
            ],
            [
                'value' => 'bi bi-alarm-fill',
                'label' => 'alarm-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-alarm-fill',
                ],
            ],
            [
                'value' => 'bi bi-alarm',
                'label' => 'alarm',
                'attributes' => [
                    'data-icon' => 'bi bi-alarm',
                ],
            ],
            [
                'value' => 'bi bi-align-bottom',
                'label' => 'align-bottom',
                'attributes' => [
                    'data-icon' => 'bi bi-align-bottom',
                ],
            ],
            [
                'value' => 'bi bi-align-center',
                'label' => 'align-center',
                'attributes' => [
                    'data-icon' => 'bi bi-align-center',
                ],
            ],
            [
                'value' => 'bi bi-align-end',
                'label' => 'align-end',
                'attributes' => [
                    'data-icon' => 'bi bi-align-end',
                ],
            ],
            [
                'value' => 'bi bi-align-middle',
                'label' => 'align-middle',
                'attributes' => [
                    'data-icon' => 'bi bi-align-middle',
                ],
            ],
            [
                'value' => 'bi bi-align-start',
                'label' => 'align-start',
                'attributes' => [
                    'data-icon' => 'bi bi-align-start',
                ],
            ],
            [
                'value' => 'bi bi-align-top',
                'label' => 'align-top',
                'attributes' => [
                    'data-icon' => 'bi bi-align-top',
                ],
            ],
            [
                'value' => 'bi bi-alt',
                'label' => 'alt',
                'attributes' => [
                    'data-icon' => 'bi bi-alt',
                ],
            ],
            [
                'value' => 'bi bi-app-indicator',
                'label' => 'app-indicator',
                'attributes' => [
                    'data-icon' => 'bi bi-app-indicator',
                ],
            ],
            [
                'value' => 'bi bi-app',
                'label' => 'app',
                'attributes' => [
                    'data-icon' => 'bi bi-app',
                ],
            ],
            [
                'value' => 'bi bi-archive-fill',
                'label' => 'archive-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-archive-fill',
                ],
            ],
            [
                'value' => 'bi bi-archive',
                'label' => 'archive',
                'attributes' => [
                    'data-icon' => 'bi bi-archive',
                ],
            ],
            [
                'value' => 'bi bi-arrow-90deg-down',
                'label' => 'arrow-90deg-down',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-90deg-down',
                ],
            ],
            [
                'value' => 'bi bi-arrow-90deg-left',
                'label' => 'arrow-90deg-left',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-90deg-left',
                ],
            ],
            [
                'value' => 'bi bi-arrow-90deg-right',
                'label' => 'arrow-90deg-right',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-90deg-right',
                ],
            ],
            [
                'value' => 'bi bi-arrow-90deg-up',
                'label' => 'arrow-90deg-up',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-90deg-up',
                ],
            ],
            [
                'value' => 'bi bi-arrow-bar-down',
                'label' => 'arrow-bar-down',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-bar-down',
                ],
            ],
            [
                'value' => 'bi bi-arrow-bar-left',
                'label' => 'arrow-bar-left',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-bar-left',
                ],
            ],
            [
                'value' => 'bi bi-arrow-bar-right',
                'label' => 'arrow-bar-right',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-bar-right',
                ],
            ],
            [
                'value' => 'bi bi-arrow-bar-up',
                'label' => 'arrow-bar-up',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-bar-up',
                ],
            ],
            [
                'value' => 'bi bi-arrow-clockwise',
                'label' => 'arrow-clockwise',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-clockwise',
                ],
            ],
            [
                'value' => 'bi bi-arrow-counterclockwise',
                'label' => 'arrow-counterclockwise',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-counterclockwise',
                ],
            ],
            [
                'value' => 'bi bi-arrow-down-circle-fill',
                'label' => 'arrow-down-circle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-down-circle-fill',
                ],
            ],
            [
                'value' => 'bi bi-arrow-down-circle',
                'label' => 'arrow-down-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-down-circle',
                ],
            ],
            [
                'value' => 'bi bi-arrow-down-left-circle-fill',
                'label' => 'arrow-down-left-circle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-down-left-circle-fill',
                ],
            ],
            [
                'value' => 'bi bi-arrow-down-left-circle',
                'label' => 'arrow-down-left-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-down-left-circle',
                ],
            ],
            [
                'value' => 'bi bi-arrow-down-left-square-fill',
                'label' => 'arrow-down-left-square-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-down-left-square-fill',
                ],
            ],
            [
                'value' => 'bi bi-arrow-down-left-square',
                'label' => 'arrow-down-left-square',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-down-left-square',
                ],
            ],
            [
                'value' => 'bi bi-arrow-down-left',
                'label' => 'arrow-down-left',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-down-left',
                ],
            ],
            [
                'value' => 'bi bi-arrow-down-right-circle-fill',
                'label' => 'arrow-down-right-circle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-down-right-circle-fill',
                ],
            ],
            [
                'value' => 'bi bi-arrow-down-right-circle',
                'label' => 'arrow-down-right-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-down-right-circle',
                ],
            ],
            [
                'value' => 'bi bi-arrow-down-right-square-fill',
                'label' => 'arrow-down-right-square-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-down-right-square-fill',
                ],
            ],
            [
                'value' => 'bi bi-arrow-down-right-square',
                'label' => 'arrow-down-right-square',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-down-right-square',
                ],
            ],
            [
                'value' => 'bi bi-arrow-down-right',
                'label' => 'arrow-down-right',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-down-right',
                ],
            ],
            [
                'value' => 'bi bi-arrow-down-short',
                'label' => 'arrow-down-short',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-down-short',
                ],
            ],
            [
                'value' => 'bi bi-arrow-down-square-fill',
                'label' => 'arrow-down-square-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-down-square-fill',
                ],
            ],
            [
                'value' => 'bi bi-arrow-down-square',
                'label' => 'arrow-down-square',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-down-square',
                ],
            ],
            [
                'value' => 'bi bi-arrow-down-up',
                'label' => 'arrow-down-up',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-down-up',
                ],
            ],
            [
                'value' => 'bi bi-arrow-down',
                'label' => 'arrow-down',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-down',
                ],
            ],
            [
                'value' => 'bi bi-arrow-left-circle-fill',
                'label' => 'arrow-left-circle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-left-circle-fill',
                ],
            ],
            [
                'value' => 'bi bi-arrow-left-circle',
                'label' => 'arrow-left-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-left-circle',
                ],
            ],
            [
                'value' => 'bi bi-arrow-left-right',
                'label' => 'arrow-left-right',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-left-right',
                ],
            ],
            [
                'value' => 'bi bi-arrow-left-short',
                'label' => 'arrow-left-short',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-left-short',
                ],
            ],
            [
                'value' => 'bi bi-arrow-left-square-fill',
                'label' => 'arrow-left-square-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-left-square-fill',
                ],
            ],
            [
                'value' => 'bi bi-arrow-left-square',
                'label' => 'arrow-left-square',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-left-square',
                ],
            ],
            [
                'value' => 'bi bi-arrow-left',
                'label' => 'arrow-left',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-left',
                ],
            ],
            [
                'value' => 'bi bi-arrow-repeat',
                'label' => 'arrow-repeat',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-repeat',
                ],
            ],
            [
                'value' => 'bi bi-arrow-return-left',
                'label' => 'arrow-return-left',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-return-left',
                ],
            ],
            [
                'value' => 'bi bi-arrow-return-right',
                'label' => 'arrow-return-right',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-return-right',
                ],
            ],
            [
                'value' => 'bi bi-arrow-right-circle-fill',
                'label' => 'arrow-right-circle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-right-circle-fill',
                ],
            ],
            [
                'value' => 'bi bi-arrow-right-circle',
                'label' => 'arrow-right-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-right-circle',
                ],
            ],
            [
                'value' => 'bi bi-arrow-right-short',
                'label' => 'arrow-right-short',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-right-short',
                ],
            ],
            [
                'value' => 'bi bi-arrow-right-square-fill',
                'label' => 'arrow-right-square-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-right-square-fill',
                ],
            ],
            [
                'value' => 'bi bi-arrow-right-square',
                'label' => 'arrow-right-square',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-right-square',
                ],
            ],
            [
                'value' => 'bi bi-arrow-right',
                'label' => 'arrow-right',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-right',
                ],
            ],
            [
                'value' => 'bi bi-arrow-up-circle-fill',
                'label' => 'arrow-up-circle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-up-circle-fill',
                ],
            ],
            [
                'value' => 'bi bi-arrow-up-circle',
                'label' => 'arrow-up-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-up-circle',
                ],
            ],
            [
                'value' => 'bi bi-arrow-up-left-circle-fill',
                'label' => 'arrow-up-left-circle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-up-left-circle-fill',
                ],
            ],
            [
                'value' => 'bi bi-arrow-up-left-circle',
                'label' => 'arrow-up-left-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-up-left-circle',
                ],
            ],
            [
                'value' => 'bi bi-arrow-up-left-square-fill',
                'label' => 'arrow-up-left-square-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-up-left-square-fill',
                ],
            ],
            [
                'value' => 'bi bi-arrow-up-left-square',
                'label' => 'arrow-up-left-square',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-up-left-square',
                ],
            ],
            [
                'value' => 'bi bi-arrow-up-left',
                'label' => 'arrow-up-left',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-up-left',
                ],
            ],
            [
                'value' => 'bi bi-arrow-up-right-circle-fill',
                'label' => 'arrow-up-right-circle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-up-right-circle-fill',
                ],
            ],
            [
                'value' => 'bi bi-arrow-up-right-circle',
                'label' => 'arrow-up-right-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-up-right-circle',
                ],
            ],
            [
                'value' => 'bi bi-arrow-up-right-square-fill',
                'label' => 'arrow-up-right-square-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-up-right-square-fill',
                ],
            ],
            [
                'value' => 'bi bi-arrow-up-right-square',
                'label' => 'arrow-up-right-square',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-up-right-square',
                ],
            ],
            [
                'value' => 'bi bi-arrow-up-right',
                'label' => 'arrow-up-right',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-up-right',
                ],
            ],
            [
                'value' => 'bi bi-arrow-up-short',
                'label' => 'arrow-up-short',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-up-short',
                ],
            ],
            [
                'value' => 'bi bi-arrow-up-square-fill',
                'label' => 'arrow-up-square-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-up-square-fill',
                ],
            ],
            [
                'value' => 'bi bi-arrow-up-square',
                'label' => 'arrow-up-square',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-up-square',
                ],
            ],
            [
                'value' => 'bi bi-arrow-up',
                'label' => 'arrow-up',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-up',
                ],
            ],
            [
                'value' => 'bi bi-arrows-angle-contract',
                'label' => 'arrows-angle-contract',
                'attributes' => [
                    'data-icon' => 'bi bi-arrows-angle-contract',
                ],
            ],
            [
                'value' => 'bi bi-arrows-angle-expand',
                'label' => 'arrows-angle-expand',
                'attributes' => [
                    'data-icon' => 'bi bi-arrows-angle-expand',
                ],
            ],
            [
                'value' => 'bi bi-arrows-collapse',
                'label' => 'arrows-collapse',
                'attributes' => [
                    'data-icon' => 'bi bi-arrows-collapse',
                ],
            ],
            [
                'value' => 'bi bi-arrows-expand',
                'label' => 'arrows-expand',
                'attributes' => [
                    'data-icon' => 'bi bi-arrows-expand',
                ],
            ],
            [
                'value' => 'bi bi-arrows-fullscreen',
                'label' => 'arrows-fullscreen',
                'attributes' => [
                    'data-icon' => 'bi bi-arrows-fullscreen',
                ],
            ],
            [
                'value' => 'bi bi-arrows-move',
                'label' => 'arrows-move',
                'attributes' => [
                    'data-icon' => 'bi bi-arrows-move',
                ],
            ],
            [
                'value' => 'bi bi-aspect-ratio-fill',
                'label' => 'aspect-ratio-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-aspect-ratio-fill',
                ],
            ],
            [
                'value' => 'bi bi-aspect-ratio',
                'label' => 'aspect-ratio',
                'attributes' => [
                    'data-icon' => 'bi bi-aspect-ratio',
                ],
            ],
            [
                'value' => 'bi bi-asterisk',
                'label' => 'asterisk',
                'attributes' => [
                    'data-icon' => 'bi bi-asterisk',
                ],
            ],
            [
                'value' => 'bi bi-at',
                'label' => 'at',
                'attributes' => [
                    'data-icon' => 'bi bi-at',
                ],
            ],
            [
                'value' => 'bi bi-award-fill',
                'label' => 'award-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-award-fill',
                ],
            ],
            [
                'value' => 'bi bi-award',
                'label' => 'award',
                'attributes' => [
                    'data-icon' => 'bi bi-award',
                ],
            ],
            [
                'value' => 'bi bi-back',
                'label' => 'back',
                'attributes' => [
                    'data-icon' => 'bi bi-back',
                ],
            ],
            [
                'value' => 'bi bi-backspace-fill',
                'label' => 'backspace-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-backspace-fill',
                ],
            ],
            [
                'value' => 'bi bi-backspace-reverse-fill',
                'label' => 'backspace-reverse-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-backspace-reverse-fill',
                ],
            ],
            [
                'value' => 'bi bi-backspace-reverse',
                'label' => 'backspace-reverse',
                'attributes' => [
                    'data-icon' => 'bi bi-backspace-reverse',
                ],
            ],
            [
                'value' => 'bi bi-backspace',
                'label' => 'backspace',
                'attributes' => [
                    'data-icon' => 'bi bi-backspace',
                ],
            ],
            [
                'value' => 'bi bi-badge-3d-fill',
                'label' => 'badge-3d-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-badge-3d-fill',
                ],
            ],
            [
                'value' => 'bi bi-badge-3d',
                'label' => 'badge-3d',
                'attributes' => [
                    'data-icon' => 'bi bi-badge-3d',
                ],
            ],
            [
                'value' => 'bi bi-badge-4k-fill',
                'label' => 'badge-4k-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-badge-4k-fill',
                ],
            ],
            [
                'value' => 'bi bi-badge-4k',
                'label' => 'badge-4k',
                'attributes' => [
                    'data-icon' => 'bi bi-badge-4k',
                ],
            ],
            [
                'value' => 'bi bi-badge-8k-fill',
                'label' => 'badge-8k-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-badge-8k-fill',
                ],
            ],
            [
                'value' => 'bi bi-badge-8k',
                'label' => 'badge-8k',
                'attributes' => [
                    'data-icon' => 'bi bi-badge-8k',
                ],
            ],
            [
                'value' => 'bi bi-badge-ad-fill',
                'label' => 'badge-ad-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-badge-ad-fill',
                ],
            ],
            [
                'value' => 'bi bi-badge-ad',
                'label' => 'badge-ad',
                'attributes' => [
                    'data-icon' => 'bi bi-badge-ad',
                ],
            ],
            [
                'value' => 'bi bi-badge-ar-fill',
                'label' => 'badge-ar-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-badge-ar-fill',
                ],
            ],
            [
                'value' => 'bi bi-badge-ar',
                'label' => 'badge-ar',
                'attributes' => [
                    'data-icon' => 'bi bi-badge-ar',
                ],
            ],
            [
                'value' => 'bi bi-badge-cc-fill',
                'label' => 'badge-cc-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-badge-cc-fill',
                ],
            ],
            [
                'value' => 'bi bi-badge-cc',
                'label' => 'badge-cc',
                'attributes' => [
                    'data-icon' => 'bi bi-badge-cc',
                ],
            ],
            [
                'value' => 'bi bi-badge-hd-fill',
                'label' => 'badge-hd-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-badge-hd-fill',
                ],
            ],
            [
                'value' => 'bi bi-badge-hd',
                'label' => 'badge-hd',
                'attributes' => [
                    'data-icon' => 'bi bi-badge-hd',
                ],
            ],
            [
                'value' => 'bi bi-badge-tm-fill',
                'label' => 'badge-tm-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-badge-tm-fill',
                ],
            ],
            [
                'value' => 'bi bi-badge-tm',
                'label' => 'badge-tm',
                'attributes' => [
                    'data-icon' => 'bi bi-badge-tm',
                ],
            ],
            [
                'value' => 'bi bi-badge-vo-fill',
                'label' => 'badge-vo-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-badge-vo-fill',
                ],
            ],
            [
                'value' => 'bi bi-badge-vo',
                'label' => 'badge-vo',
                'attributes' => [
                    'data-icon' => 'bi bi-badge-vo',
                ],
            ],
            [
                'value' => 'bi bi-badge-vr-fill',
                'label' => 'badge-vr-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-badge-vr-fill',
                ],
            ],
            [
                'value' => 'bi bi-badge-vr',
                'label' => 'badge-vr',
                'attributes' => [
                    'data-icon' => 'bi bi-badge-vr',
                ],
            ],
            [
                'value' => 'bi bi-badge-wc-fill',
                'label' => 'badge-wc-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-badge-wc-fill',
                ],
            ],
            [
                'value' => 'bi bi-badge-wc',
                'label' => 'badge-wc',
                'attributes' => [
                    'data-icon' => 'bi bi-badge-wc',
                ],
            ],
            [
                'value' => 'bi bi-bag-check-fill',
                'label' => 'bag-check-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-bag-check-fill',
                ],
            ],
            [
                'value' => 'bi bi-bag-check',
                'label' => 'bag-check',
                'attributes' => [
                    'data-icon' => 'bi bi-bag-check',
                ],
            ],
            [
                'value' => 'bi bi-bag-dash-fill',
                'label' => 'bag-dash-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-bag-dash-fill',
                ],
            ],
            [
                'value' => 'bi bi-bag-dash',
                'label' => 'bag-dash',
                'attributes' => [
                    'data-icon' => 'bi bi-bag-dash',
                ],
            ],
            [
                'value' => 'bi bi-bag-fill',
                'label' => 'bag-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-bag-fill',
                ],
            ],
            [
                'value' => 'bi bi-bag-plus-fill',
                'label' => 'bag-plus-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-bag-plus-fill',
                ],
            ],
            [
                'value' => 'bi bi-bag-plus',
                'label' => 'bag-plus',
                'attributes' => [
                    'data-icon' => 'bi bi-bag-plus',
                ],
            ],
            [
                'value' => 'bi bi-bag-x-fill',
                'label' => 'bag-x-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-bag-x-fill',
                ],
            ],
            [
                'value' => 'bi bi-bag-x',
                'label' => 'bag-x',
                'attributes' => [
                    'data-icon' => 'bi bi-bag-x',
                ],
            ],
            [
                'value' => 'bi bi-bag',
                'label' => 'bag',
                'attributes' => [
                    'data-icon' => 'bi bi-bag',
                ],
            ],
            [
                'value' => 'bi bi-bar-chart-fill',
                'label' => 'bar-chart-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-bar-chart-fill',
                ],
            ],
            [
                'value' => 'bi bi-bar-chart-line-fill',
                'label' => 'bar-chart-line-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-bar-chart-line-fill',
                ],
            ],
            [
                'value' => 'bi bi-bar-chart-line',
                'label' => 'bar-chart-line',
                'attributes' => [
                    'data-icon' => 'bi bi-bar-chart-line',
                ],
            ],
            [
                'value' => 'bi bi-bar-chart-steps',
                'label' => 'bar-chart-steps',
                'attributes' => [
                    'data-icon' => 'bi bi-bar-chart-steps',
                ],
            ],
            [
                'value' => 'bi bi-bar-chart',
                'label' => 'bar-chart',
                'attributes' => [
                    'data-icon' => 'bi bi-bar-chart',
                ],
            ],
            [
                'value' => 'bi bi-basket-fill',
                'label' => 'basket-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-basket-fill',
                ],
            ],
            [
                'value' => 'bi bi-basket',
                'label' => 'basket',
                'attributes' => [
                    'data-icon' => 'bi bi-basket',
                ],
            ],
            [
                'value' => 'bi bi-basket2-fill',
                'label' => 'basket2-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-basket2-fill',
                ],
            ],
            [
                'value' => 'bi bi-basket2',
                'label' => 'basket2',
                'attributes' => [
                    'data-icon' => 'bi bi-basket2',
                ],
            ],
            [
                'value' => 'bi bi-basket3-fill',
                'label' => 'basket3-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-basket3-fill',
                ],
            ],
            [
                'value' => 'bi bi-basket3',
                'label' => 'basket3',
                'attributes' => [
                    'data-icon' => 'bi bi-basket3',
                ],
            ],
            [
                'value' => 'bi bi-battery-charging',
                'label' => 'battery-charging',
                'attributes' => [
                    'data-icon' => 'bi bi-battery-charging',
                ],
            ],
            [
                'value' => 'bi bi-battery-full',
                'label' => 'battery-full',
                'attributes' => [
                    'data-icon' => 'bi bi-battery-full',
                ],
            ],
            [
                'value' => 'bi bi-battery-half',
                'label' => 'battery-half',
                'attributes' => [
                    'data-icon' => 'bi bi-battery-half',
                ],
            ],
            [
                'value' => 'bi bi-battery',
                'label' => 'battery',
                'attributes' => [
                    'data-icon' => 'bi bi-battery',
                ],
            ],
            [
                'value' => 'bi bi-bell-fill',
                'label' => 'bell-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-bell-fill',
                ],
            ],
            [
                'value' => 'bi bi-bell',
                'label' => 'bell',
                'attributes' => [
                    'data-icon' => 'bi bi-bell',
                ],
            ],
            [
                'value' => 'bi bi-bezier',
                'label' => 'bezier',
                'attributes' => [
                    'data-icon' => 'bi bi-bezier',
                ],
            ],
            [
                'value' => 'bi bi-bezier2',
                'label' => 'bezier2',
                'attributes' => [
                    'data-icon' => 'bi bi-bezier2',
                ],
            ],
            [
                'value' => 'bi bi-bicycle',
                'label' => 'bicycle',
                'attributes' => [
                    'data-icon' => 'bi bi-bicycle',
                ],
            ],
            [
                'value' => 'bi bi-binoculars-fill',
                'label' => 'binoculars-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-binoculars-fill',
                ],
            ],
            [
                'value' => 'bi bi-binoculars',
                'label' => 'binoculars',
                'attributes' => [
                    'data-icon' => 'bi bi-binoculars',
                ],
            ],
            [
                'value' => 'bi bi-blockquote-left',
                'label' => 'blockquote-left',
                'attributes' => [
                    'data-icon' => 'bi bi-blockquote-left',
                ],
            ],
            [
                'value' => 'bi bi-blockquote-right',
                'label' => 'blockquote-right',
                'attributes' => [
                    'data-icon' => 'bi bi-blockquote-right',
                ],
            ],
            [
                'value' => 'bi bi-book-fill',
                'label' => 'book-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-book-fill',
                ],
            ],
            [
                'value' => 'bi bi-book-half',
                'label' => 'book-half',
                'attributes' => [
                    'data-icon' => 'bi bi-book-half',
                ],
            ],
            [
                'value' => 'bi bi-book',
                'label' => 'book',
                'attributes' => [
                    'data-icon' => 'bi bi-book',
                ],
            ],
            [
                'value' => 'bi bi-bookmark-check-fill',
                'label' => 'bookmark-check-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-bookmark-check-fill',
                ],
            ],
            [
                'value' => 'bi bi-bookmark-check',
                'label' => 'bookmark-check',
                'attributes' => [
                    'data-icon' => 'bi bi-bookmark-check',
                ],
            ],
            [
                'value' => 'bi bi-bookmark-dash-fill',
                'label' => 'bookmark-dash-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-bookmark-dash-fill',
                ],
            ],
            [
                'value' => 'bi bi-bookmark-dash',
                'label' => 'bookmark-dash',
                'attributes' => [
                    'data-icon' => 'bi bi-bookmark-dash',
                ],
            ],
            [
                'value' => 'bi bi-bookmark-fill',
                'label' => 'bookmark-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-bookmark-fill',
                ],
            ],
            [
                'value' => 'bi bi-bookmark-heart-fill',
                'label' => 'bookmark-heart-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-bookmark-heart-fill',
                ],
            ],
            [
                'value' => 'bi bi-bookmark-heart',
                'label' => 'bookmark-heart',
                'attributes' => [
                    'data-icon' => 'bi bi-bookmark-heart',
                ],
            ],
            [
                'value' => 'bi bi-bookmark-plus-fill',
                'label' => 'bookmark-plus-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-bookmark-plus-fill',
                ],
            ],
            [
                'value' => 'bi bi-bookmark-plus',
                'label' => 'bookmark-plus',
                'attributes' => [
                    'data-icon' => 'bi bi-bookmark-plus',
                ],
            ],
            [
                'value' => 'bi bi-bookmark-star-fill',
                'label' => 'bookmark-star-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-bookmark-star-fill',
                ],
            ],
            [
                'value' => 'bi bi-bookmark-star',
                'label' => 'bookmark-star',
                'attributes' => [
                    'data-icon' => 'bi bi-bookmark-star',
                ],
            ],
            [
                'value' => 'bi bi-bookmark-x-fill',
                'label' => 'bookmark-x-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-bookmark-x-fill',
                ],
            ],
            [
                'value' => 'bi bi-bookmark-x',
                'label' => 'bookmark-x',
                'attributes' => [
                    'data-icon' => 'bi bi-bookmark-x',
                ],
            ],
            [
                'value' => 'bi bi-bookmark',
                'label' => 'bookmark',
                'attributes' => [
                    'data-icon' => 'bi bi-bookmark',
                ],
            ],
            [
                'value' => 'bi bi-bookmarks-fill',
                'label' => 'bookmarks-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-bookmarks-fill',
                ],
            ],
            [
                'value' => 'bi bi-bookmarks',
                'label' => 'bookmarks',
                'attributes' => [
                    'data-icon' => 'bi bi-bookmarks',
                ],
            ],
            [
                'value' => 'bi bi-bookshelf',
                'label' => 'bookshelf',
                'attributes' => [
                    'data-icon' => 'bi bi-bookshelf',
                ],
            ],
            [
                'value' => 'bi bi-bootstrap-fill',
                'label' => 'bootstrap-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-bootstrap-fill',
                ],
            ],
            [
                'value' => 'bi bi-bootstrap-reboot',
                'label' => 'bootstrap-reboot',
                'attributes' => [
                    'data-icon' => 'bi bi-bootstrap-reboot',
                ],
            ],
            [
                'value' => 'bi bi-bootstrap',
                'label' => 'bootstrap',
                'attributes' => [
                    'data-icon' => 'bi bi-bootstrap',
                ],
            ],
            [
                'value' => 'bi bi-border-all',
                'label' => 'border-all',
                'attributes' => [
                    'data-icon' => 'bi bi-border-all',
                ],
            ],
            [
                'value' => 'bi bi-border-bottom',
                'label' => 'border-bottom',
                'attributes' => [
                    'data-icon' => 'bi bi-border-bottom',
                ],
            ],
            [
                'value' => 'bi bi-border-center',
                'label' => 'border-center',
                'attributes' => [
                    'data-icon' => 'bi bi-border-center',
                ],
            ],
            [
                'value' => 'bi bi-border-inner',
                'label' => 'border-inner',
                'attributes' => [
                    'data-icon' => 'bi bi-border-inner',
                ],
            ],
            [
                'value' => 'bi bi-border-left',
                'label' => 'border-left',
                'attributes' => [
                    'data-icon' => 'bi bi-border-left',
                ],
            ],
            [
                'value' => 'bi bi-border-middle',
                'label' => 'border-middle',
                'attributes' => [
                    'data-icon' => 'bi bi-border-middle',
                ],
            ],
            [
                'value' => 'bi bi-border-outer',
                'label' => 'border-outer',
                'attributes' => [
                    'data-icon' => 'bi bi-border-outer',
                ],
            ],
            [
                'value' => 'bi bi-border-right',
                'label' => 'border-right',
                'attributes' => [
                    'data-icon' => 'bi bi-border-right',
                ],
            ],
            [
                'value' => 'bi bi-border-style',
                'label' => 'border-style',
                'attributes' => [
                    'data-icon' => 'bi bi-border-style',
                ],
            ],
            [
                'value' => 'bi bi-border-top',
                'label' => 'border-top',
                'attributes' => [
                    'data-icon' => 'bi bi-border-top',
                ],
            ],
            [
                'value' => 'bi bi-border-width',
                'label' => 'border-width',
                'attributes' => [
                    'data-icon' => 'bi bi-border-width',
                ],
            ],
            [
                'value' => 'bi bi-border',
                'label' => 'border',
                'attributes' => [
                    'data-icon' => 'bi bi-border',
                ],
            ],
            [
                'value' => 'bi bi-bounding-box-circles',
                'label' => 'bounding-box-circles',
                'attributes' => [
                    'data-icon' => 'bi bi-bounding-box-circles',
                ],
            ],
            [
                'value' => 'bi bi-bounding-box',
                'label' => 'bounding-box',
                'attributes' => [
                    'data-icon' => 'bi bi-bounding-box',
                ],
            ],
            [
                'value' => 'bi bi-box-arrow-down-left',
                'label' => 'box-arrow-down-left',
                'attributes' => [
                    'data-icon' => 'bi bi-box-arrow-down-left',
                ],
            ],
            [
                'value' => 'bi bi-box-arrow-down-right',
                'label' => 'box-arrow-down-right',
                'attributes' => [
                    'data-icon' => 'bi bi-box-arrow-down-right',
                ],
            ],
            [
                'value' => 'bi bi-box-arrow-down',
                'label' => 'box-arrow-down',
                'attributes' => [
                    'data-icon' => 'bi bi-box-arrow-down',
                ],
            ],
            [
                'value' => 'bi bi-box-arrow-in-down-left',
                'label' => 'box-arrow-in-down-left',
                'attributes' => [
                    'data-icon' => 'bi bi-box-arrow-in-down-left',
                ],
            ],
            [
                'value' => 'bi bi-box-arrow-in-down-right',
                'label' => 'box-arrow-in-down-right',
                'attributes' => [
                    'data-icon' => 'bi bi-box-arrow-in-down-right',
                ],
            ],
            [
                'value' => 'bi bi-box-arrow-in-down',
                'label' => 'box-arrow-in-down',
                'attributes' => [
                    'data-icon' => 'bi bi-box-arrow-in-down',
                ],
            ],
            [
                'value' => 'bi bi-box-arrow-in-left',
                'label' => 'box-arrow-in-left',
                'attributes' => [
                    'data-icon' => 'bi bi-box-arrow-in-left',
                ],
            ],
            [
                'value' => 'bi bi-box-arrow-in-right',
                'label' => 'box-arrow-in-right',
                'attributes' => [
                    'data-icon' => 'bi bi-box-arrow-in-right',
                ],
            ],
            [
                'value' => 'bi bi-box-arrow-in-up-left',
                'label' => 'box-arrow-in-up-left',
                'attributes' => [
                    'data-icon' => 'bi bi-box-arrow-in-up-left',
                ],
            ],
            [
                'value' => 'bi bi-box-arrow-in-up-right',
                'label' => 'box-arrow-in-up-right',
                'attributes' => [
                    'data-icon' => 'bi bi-box-arrow-in-up-right',
                ],
            ],
            [
                'value' => 'bi bi-box-arrow-in-up',
                'label' => 'box-arrow-in-up',
                'attributes' => [
                    'data-icon' => 'bi bi-box-arrow-in-up',
                ],
            ],
            [
                'value' => 'bi bi-box-arrow-left',
                'label' => 'box-arrow-left',
                'attributes' => [
                    'data-icon' => 'bi bi-box-arrow-left',
                ],
            ],
            [
                'value' => 'bi bi-box-arrow-right',
                'label' => 'box-arrow-right',
                'attributes' => [
                    'data-icon' => 'bi bi-box-arrow-right',
                ],
            ],
            [
                'value' => 'bi bi-box-arrow-up-left',
                'label' => 'box-arrow-up-left',
                'attributes' => [
                    'data-icon' => 'bi bi-box-arrow-up-left',
                ],
            ],
            [
                'value' => 'bi bi-box-arrow-up-right',
                'label' => 'box-arrow-up-right',
                'attributes' => [
                    'data-icon' => 'bi bi-box-arrow-up-right',
                ],
            ],
            [
                'value' => 'bi bi-box-arrow-up',
                'label' => 'box-arrow-up',
                'attributes' => [
                    'data-icon' => 'bi bi-box-arrow-up',
                ],
            ],
            [
                'value' => 'bi bi-box-seam',
                'label' => 'box-seam',
                'attributes' => [
                    'data-icon' => 'bi bi-box-seam',
                ],
            ],
            [
                'value' => 'bi bi-box',
                'label' => 'box',
                'attributes' => [
                    'data-icon' => 'bi bi-box',
                ],
            ],
            [
                'value' => 'bi bi-braces',
                'label' => 'braces',
                'attributes' => [
                    'data-icon' => 'bi bi-braces',
                ],
            ],
            [
                'value' => 'bi bi-bricks',
                'label' => 'bricks',
                'attributes' => [
                    'data-icon' => 'bi bi-bricks',
                ],
            ],
            [
                'value' => 'bi bi-briefcase-fill',
                'label' => 'briefcase-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-briefcase-fill',
                ],
            ],
            [
                'value' => 'bi bi-briefcase',
                'label' => 'briefcase',
                'attributes' => [
                    'data-icon' => 'bi bi-briefcase',
                ],
            ],
            [
                'value' => 'bi bi-brightness-alt-high-fill',
                'label' => 'brightness-alt-high-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-brightness-alt-high-fill',
                ],
            ],
            [
                'value' => 'bi bi-brightness-alt-high',
                'label' => 'brightness-alt-high',
                'attributes' => [
                    'data-icon' => 'bi bi-brightness-alt-high',
                ],
            ],
            [
                'value' => 'bi bi-brightness-alt-low-fill',
                'label' => 'brightness-alt-low-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-brightness-alt-low-fill',
                ],
            ],
            [
                'value' => 'bi bi-brightness-alt-low',
                'label' => 'brightness-alt-low',
                'attributes' => [
                    'data-icon' => 'bi bi-brightness-alt-low',
                ],
            ],
            [
                'value' => 'bi bi-brightness-high-fill',
                'label' => 'brightness-high-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-brightness-high-fill',
                ],
            ],
            [
                'value' => 'bi bi-brightness-high',
                'label' => 'brightness-high',
                'attributes' => [
                    'data-icon' => 'bi bi-brightness-high',
                ],
            ],
            [
                'value' => 'bi bi-brightness-low-fill',
                'label' => 'brightness-low-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-brightness-low-fill',
                ],
            ],
            [
                'value' => 'bi bi-brightness-low',
                'label' => 'brightness-low',
                'attributes' => [
                    'data-icon' => 'bi bi-brightness-low',
                ],
            ],
            [
                'value' => 'bi bi-broadcast-pin',
                'label' => 'broadcast-pin',
                'attributes' => [
                    'data-icon' => 'bi bi-broadcast-pin',
                ],
            ],
            [
                'value' => 'bi bi-broadcast',
                'label' => 'broadcast',
                'attributes' => [
                    'data-icon' => 'bi bi-broadcast',
                ],
            ],
            [
                'value' => 'bi bi-brush-fill',
                'label' => 'brush-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-brush-fill',
                ],
            ],
            [
                'value' => 'bi bi-brush',
                'label' => 'brush',
                'attributes' => [
                    'data-icon' => 'bi bi-brush',
                ],
            ],
            [
                'value' => 'bi bi-bucket-fill',
                'label' => 'bucket-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-bucket-fill',
                ],
            ],
            [
                'value' => 'bi bi-bucket',
                'label' => 'bucket',
                'attributes' => [
                    'data-icon' => 'bi bi-bucket',
                ],
            ],
            [
                'value' => 'bi bi-bug-fill',
                'label' => 'bug-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-bug-fill',
                ],
            ],
            [
                'value' => 'bi bi-bug',
                'label' => 'bug',
                'attributes' => [
                    'data-icon' => 'bi bi-bug',
                ],
            ],
            [
                'value' => 'bi bi-building',
                'label' => 'building',
                'attributes' => [
                    'data-icon' => 'bi bi-building',
                ],
            ],
            [
                'value' => 'bi bi-bullseye',
                'label' => 'bullseye',
                'attributes' => [
                    'data-icon' => 'bi bi-bullseye',
                ],
            ],
            [
                'value' => 'bi bi-calculator-fill',
                'label' => 'calculator-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calculator-fill',
                ],
            ],
            [
                'value' => 'bi bi-calculator',
                'label' => 'calculator',
                'attributes' => [
                    'data-icon' => 'bi bi-calculator',
                ],
            ],
            [
                'value' => 'bi bi-calendar-check-fill',
                'label' => 'calendar-check-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar-check-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar-check',
                'label' => 'calendar-check',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar-check',
                ],
            ],
            [
                'value' => 'bi bi-calendar-date-fill',
                'label' => 'calendar-date-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar-date-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar-date',
                'label' => 'calendar-date',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar-date',
                ],
            ],
            [
                'value' => 'bi bi-calendar-day-fill',
                'label' => 'calendar-day-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar-day-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar-day',
                'label' => 'calendar-day',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar-day',
                ],
            ],
            [
                'value' => 'bi bi-calendar-event-fill',
                'label' => 'calendar-event-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar-event-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar-event',
                'label' => 'calendar-event',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar-event',
                ],
            ],
            [
                'value' => 'bi bi-calendar-fill',
                'label' => 'calendar-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar-minus-fill',
                'label' => 'calendar-minus-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar-minus-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar-minus',
                'label' => 'calendar-minus',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar-minus',
                ],
            ],
            [
                'value' => 'bi bi-calendar-month-fill',
                'label' => 'calendar-month-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar-month-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar-month',
                'label' => 'calendar-month',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar-month',
                ],
            ],
            [
                'value' => 'bi bi-calendar-plus-fill',
                'label' => 'calendar-plus-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar-plus-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar-plus',
                'label' => 'calendar-plus',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar-plus',
                ],
            ],
            [
                'value' => 'bi bi-calendar-range-fill',
                'label' => 'calendar-range-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar-range-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar-range',
                'label' => 'calendar-range',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar-range',
                ],
            ],
            [
                'value' => 'bi bi-calendar-week-fill',
                'label' => 'calendar-week-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar-week-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar-week',
                'label' => 'calendar-week',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar-week',
                ],
            ],
            [
                'value' => 'bi bi-calendar-x-fill',
                'label' => 'calendar-x-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar-x-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar-x',
                'label' => 'calendar-x',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar-x',
                ],
            ],
            [
                'value' => 'bi bi-calendar',
                'label' => 'calendar',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar',
                ],
            ],
            [
                'value' => 'bi bi-calendar2-check-fill',
                'label' => 'calendar2-check-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar2-check-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar2-check',
                'label' => 'calendar2-check',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar2-check',
                ],
            ],
            [
                'value' => 'bi bi-calendar2-date-fill',
                'label' => 'calendar2-date-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar2-date-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar2-date',
                'label' => 'calendar2-date',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar2-date',
                ],
            ],
            [
                'value' => 'bi bi-calendar2-day-fill',
                'label' => 'calendar2-day-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar2-day-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar2-day',
                'label' => 'calendar2-day',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar2-day',
                ],
            ],
            [
                'value' => 'bi bi-calendar2-event-fill',
                'label' => 'calendar2-event-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar2-event-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar2-event',
                'label' => 'calendar2-event',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar2-event',
                ],
            ],
            [
                'value' => 'bi bi-calendar2-fill',
                'label' => 'calendar2-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar2-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar2-minus-fill',
                'label' => 'calendar2-minus-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar2-minus-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar2-minus',
                'label' => 'calendar2-minus',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar2-minus',
                ],
            ],
            [
                'value' => 'bi bi-calendar2-month-fill',
                'label' => 'calendar2-month-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar2-month-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar2-month',
                'label' => 'calendar2-month',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar2-month',
                ],
            ],
            [
                'value' => 'bi bi-calendar2-plus-fill',
                'label' => 'calendar2-plus-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar2-plus-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar2-plus',
                'label' => 'calendar2-plus',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar2-plus',
                ],
            ],
            [
                'value' => 'bi bi-calendar2-range-fill',
                'label' => 'calendar2-range-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar2-range-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar2-range',
                'label' => 'calendar2-range',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar2-range',
                ],
            ],
            [
                'value' => 'bi bi-calendar2-week-fill',
                'label' => 'calendar2-week-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar2-week-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar2-week',
                'label' => 'calendar2-week',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar2-week',
                ],
            ],
            [
                'value' => 'bi bi-calendar2-x-fill',
                'label' => 'calendar2-x-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar2-x-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar2-x',
                'label' => 'calendar2-x',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar2-x',
                ],
            ],
            [
                'value' => 'bi bi-calendar2',
                'label' => 'calendar2',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar2',
                ],
            ],
            [
                'value' => 'bi bi-calendar3-event-fill',
                'label' => 'calendar3-event-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar3-event-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar3-event',
                'label' => 'calendar3-event',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar3-event',
                ],
            ],
            [
                'value' => 'bi bi-calendar3-fill',
                'label' => 'calendar3-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar3-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar3-range-fill',
                'label' => 'calendar3-range-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar3-range-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar3-range',
                'label' => 'calendar3-range',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar3-range',
                ],
            ],
            [
                'value' => 'bi bi-calendar3-week-fill',
                'label' => 'calendar3-week-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar3-week-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar3-week',
                'label' => 'calendar3-week',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar3-week',
                ],
            ],
            [
                'value' => 'bi bi-calendar3',
                'label' => 'calendar3',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar3',
                ],
            ],
            [
                'value' => 'bi bi-calendar4-event',
                'label' => 'calendar4-event',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar4-event',
                ],
            ],
            [
                'value' => 'bi bi-calendar4-range',
                'label' => 'calendar4-range',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar4-range',
                ],
            ],
            [
                'value' => 'bi bi-calendar4-week',
                'label' => 'calendar4-week',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar4-week',
                ],
            ],
            [
                'value' => 'bi bi-calendar4',
                'label' => 'calendar4',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar4',
                ],
            ],
            [
                'value' => 'bi bi-camera-fill',
                'label' => 'camera-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-camera-fill',
                ],
            ],
            [
                'value' => 'bi bi-camera-reels-fill',
                'label' => 'camera-reels-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-camera-reels-fill',
                ],
            ],
            [
                'value' => 'bi bi-camera-reels',
                'label' => 'camera-reels',
                'attributes' => [
                    'data-icon' => 'bi bi-camera-reels',
                ],
            ],
            [
                'value' => 'bi bi-camera-video-fill',
                'label' => 'camera-video-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-camera-video-fill',
                ],
            ],
            [
                'value' => 'bi bi-camera-video-off-fill',
                'label' => 'camera-video-off-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-camera-video-off-fill',
                ],
            ],
            [
                'value' => 'bi bi-camera-video-off',
                'label' => 'camera-video-off',
                'attributes' => [
                    'data-icon' => 'bi bi-camera-video-off',
                ],
            ],
            [
                'value' => 'bi bi-camera-video',
                'label' => 'camera-video',
                'attributes' => [
                    'data-icon' => 'bi bi-camera-video',
                ],
            ],
            [
                'value' => 'bi bi-camera',
                'label' => 'camera',
                'attributes' => [
                    'data-icon' => 'bi bi-camera',
                ],
            ],
            [
                'value' => 'bi bi-camera2',
                'label' => 'camera2',
                'attributes' => [
                    'data-icon' => 'bi bi-camera2',
                ],
            ],
            [
                'value' => 'bi bi-capslock-fill',
                'label' => 'capslock-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-capslock-fill',
                ],
            ],
            [
                'value' => 'bi bi-capslock',
                'label' => 'capslock',
                'attributes' => [
                    'data-icon' => 'bi bi-capslock',
                ],
            ],
            [
                'value' => 'bi bi-card-checklist',
                'label' => 'card-checklist',
                'attributes' => [
                    'data-icon' => 'bi bi-card-checklist',
                ],
            ],
            [
                'value' => 'bi bi-card-heading',
                'label' => 'card-heading',
                'attributes' => [
                    'data-icon' => 'bi bi-card-heading',
                ],
            ],
            [
                'value' => 'bi bi-card-image',
                'label' => 'card-image',
                'attributes' => [
                    'data-icon' => 'bi bi-card-image',
                ],
            ],
            [
                'value' => 'bi bi-card-list',
                'label' => 'card-list',
                'attributes' => [
                    'data-icon' => 'bi bi-card-list',
                ],
            ],
            [
                'value' => 'bi bi-card-text',
                'label' => 'card-text',
                'attributes' => [
                    'data-icon' => 'bi bi-card-text',
                ],
            ],
            [
                'value' => 'bi bi-caret-down-fill',
                'label' => 'caret-down-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-caret-down-fill',
                ],
            ],
            [
                'value' => 'bi bi-caret-down-square-fill',
                'label' => 'caret-down-square-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-caret-down-square-fill',
                ],
            ],
            [
                'value' => 'bi bi-caret-down-square',
                'label' => 'caret-down-square',
                'attributes' => [
                    'data-icon' => 'bi bi-caret-down-square',
                ],
            ],
            [
                'value' => 'bi bi-caret-down',
                'label' => 'caret-down',
                'attributes' => [
                    'data-icon' => 'bi bi-caret-down',
                ],
            ],
            [
                'value' => 'bi bi-caret-left-fill',
                'label' => 'caret-left-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-caret-left-fill',
                ],
            ],
            [
                'value' => 'bi bi-caret-left-square-fill',
                'label' => 'caret-left-square-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-caret-left-square-fill',
                ],
            ],
            [
                'value' => 'bi bi-caret-left-square',
                'label' => 'caret-left-square',
                'attributes' => [
                    'data-icon' => 'bi bi-caret-left-square',
                ],
            ],
            [
                'value' => 'bi bi-caret-left',
                'label' => 'caret-left',
                'attributes' => [
                    'data-icon' => 'bi bi-caret-left',
                ],
            ],
            [
                'value' => 'bi bi-caret-right-fill',
                'label' => 'caret-right-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-caret-right-fill',
                ],
            ],
            [
                'value' => 'bi bi-caret-right-square-fill',
                'label' => 'caret-right-square-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-caret-right-square-fill',
                ],
            ],
            [
                'value' => 'bi bi-caret-right-square',
                'label' => 'caret-right-square',
                'attributes' => [
                    'data-icon' => 'bi bi-caret-right-square',
                ],
            ],
            [
                'value' => 'bi bi-caret-right',
                'label' => 'caret-right',
                'attributes' => [
                    'data-icon' => 'bi bi-caret-right',
                ],
            ],
            [
                'value' => 'bi bi-caret-up-fill',
                'label' => 'caret-up-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-caret-up-fill',
                ],
            ],
            [
                'value' => 'bi bi-caret-up-square-fill',
                'label' => 'caret-up-square-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-caret-up-square-fill',
                ],
            ],
            [
                'value' => 'bi bi-caret-up-square',
                'label' => 'caret-up-square',
                'attributes' => [
                    'data-icon' => 'bi bi-caret-up-square',
                ],
            ],
            [
                'value' => 'bi bi-caret-up',
                'label' => 'caret-up',
                'attributes' => [
                    'data-icon' => 'bi bi-caret-up',
                ],
            ],
            [
                'value' => 'bi bi-cart-check-fill',
                'label' => 'cart-check-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cart-check-fill',
                ],
            ],
            [
                'value' => 'bi bi-cart-check',
                'label' => 'cart-check',
                'attributes' => [
                    'data-icon' => 'bi bi-cart-check',
                ],
            ],
            [
                'value' => 'bi bi-cart-dash-fill',
                'label' => 'cart-dash-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cart-dash-fill',
                ],
            ],
            [
                'value' => 'bi bi-cart-dash',
                'label' => 'cart-dash',
                'attributes' => [
                    'data-icon' => 'bi bi-cart-dash',
                ],
            ],
            [
                'value' => 'bi bi-cart-fill',
                'label' => 'cart-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cart-fill',
                ],
            ],
            [
                'value' => 'bi bi-cart-plus-fill',
                'label' => 'cart-plus-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cart-plus-fill',
                ],
            ],
            [
                'value' => 'bi bi-cart-plus',
                'label' => 'cart-plus',
                'attributes' => [
                    'data-icon' => 'bi bi-cart-plus',
                ],
            ],
            [
                'value' => 'bi bi-cart-x-fill',
                'label' => 'cart-x-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cart-x-fill',
                ],
            ],
            [
                'value' => 'bi bi-cart-x',
                'label' => 'cart-x',
                'attributes' => [
                    'data-icon' => 'bi bi-cart-x',
                ],
            ],
            [
                'value' => 'bi bi-cart',
                'label' => 'cart',
                'attributes' => [
                    'data-icon' => 'bi bi-cart',
                ],
            ],
            [
                'value' => 'bi bi-cart2',
                'label' => 'cart2',
                'attributes' => [
                    'data-icon' => 'bi bi-cart2',
                ],
            ],
            [
                'value' => 'bi bi-cart3',
                'label' => 'cart3',
                'attributes' => [
                    'data-icon' => 'bi bi-cart3',
                ],
            ],
            [
                'value' => 'bi bi-cart4',
                'label' => 'cart4',
                'attributes' => [
                    'data-icon' => 'bi bi-cart4',
                ],
            ],
            [
                'value' => 'bi bi-cash-stack',
                'label' => 'cash-stack',
                'attributes' => [
                    'data-icon' => 'bi bi-cash-stack',
                ],
            ],
            [
                'value' => 'bi bi-cash',
                'label' => 'cash',
                'attributes' => [
                    'data-icon' => 'bi bi-cash',
                ],
            ],
            [
                'value' => 'bi bi-cast',
                'label' => 'cast',
                'attributes' => [
                    'data-icon' => 'bi bi-cast',
                ],
            ],
            [
                'value' => 'bi bi-chat-dots-fill',
                'label' => 'chat-dots-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-dots-fill',
                ],
            ],
            [
                'value' => 'bi bi-chat-dots',
                'label' => 'chat-dots',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-dots',
                ],
            ],
            [
                'value' => 'bi bi-chat-fill',
                'label' => 'chat-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-fill',
                ],
            ],
            [
                'value' => 'bi bi-chat-left-dots-fill',
                'label' => 'chat-left-dots-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-left-dots-fill',
                ],
            ],
            [
                'value' => 'bi bi-chat-left-dots',
                'label' => 'chat-left-dots',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-left-dots',
                ],
            ],
            [
                'value' => 'bi bi-chat-left-fill',
                'label' => 'chat-left-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-left-fill',
                ],
            ],
            [
                'value' => 'bi bi-chat-left-quote-fill',
                'label' => 'chat-left-quote-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-left-quote-fill',
                ],
            ],
            [
                'value' => 'bi bi-chat-left-quote',
                'label' => 'chat-left-quote',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-left-quote',
                ],
            ],
            [
                'value' => 'bi bi-chat-left-text-fill',
                'label' => 'chat-left-text-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-left-text-fill',
                ],
            ],
            [
                'value' => 'bi bi-chat-left-text',
                'label' => 'chat-left-text',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-left-text',
                ],
            ],
            [
                'value' => 'bi bi-chat-left',
                'label' => 'chat-left',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-left',
                ],
            ],
            [
                'value' => 'bi bi-chat-quote-fill',
                'label' => 'chat-quote-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-quote-fill',
                ],
            ],
            [
                'value' => 'bi bi-chat-quote',
                'label' => 'chat-quote',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-quote',
                ],
            ],
            [
                'value' => 'bi bi-chat-right-dots-fill',
                'label' => 'chat-right-dots-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-right-dots-fill',
                ],
            ],
            [
                'value' => 'bi bi-chat-right-dots',
                'label' => 'chat-right-dots',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-right-dots',
                ],
            ],
            [
                'value' => 'bi bi-chat-right-fill',
                'label' => 'chat-right-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-right-fill',
                ],
            ],
            [
                'value' => 'bi bi-chat-right-quote-fill',
                'label' => 'chat-right-quote-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-right-quote-fill',
                ],
            ],
            [
                'value' => 'bi bi-chat-right-quote',
                'label' => 'chat-right-quote',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-right-quote',
                ],
            ],
            [
                'value' => 'bi bi-chat-right-text-fill',
                'label' => 'chat-right-text-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-right-text-fill',
                ],
            ],
            [
                'value' => 'bi bi-chat-right-text',
                'label' => 'chat-right-text',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-right-text',
                ],
            ],
            [
                'value' => 'bi bi-chat-right',
                'label' => 'chat-right',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-right',
                ],
            ],
            [
                'value' => 'bi bi-chat-square-dots-fill',
                'label' => 'chat-square-dots-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-square-dots-fill',
                ],
            ],
            [
                'value' => 'bi bi-chat-square-dots',
                'label' => 'chat-square-dots',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-square-dots',
                ],
            ],
            [
                'value' => 'bi bi-chat-square-fill',
                'label' => 'chat-square-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-square-fill',
                ],
            ],
            [
                'value' => 'bi bi-chat-square-quote-fill',
                'label' => 'chat-square-quote-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-square-quote-fill',
                ],
            ],
            [
                'value' => 'bi bi-chat-square-quote',
                'label' => 'chat-square-quote',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-square-quote',
                ],
            ],
            [
                'value' => 'bi bi-chat-square-text-fill',
                'label' => 'chat-square-text-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-square-text-fill',
                ],
            ],
            [
                'value' => 'bi bi-chat-square-text',
                'label' => 'chat-square-text',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-square-text',
                ],
            ],
            [
                'value' => 'bi bi-chat-square',
                'label' => 'chat-square',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-square',
                ],
            ],
            [
                'value' => 'bi bi-chat-text-fill',
                'label' => 'chat-text-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-text-fill',
                ],
            ],
            [
                'value' => 'bi bi-chat-text',
                'label' => 'chat-text',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-text',
                ],
            ],
            [
                'value' => 'bi bi-chat',
                'label' => 'chat',
                'attributes' => [
                    'data-icon' => 'bi bi-chat',
                ],
            ],
            [
                'value' => 'bi bi-check-all',
                'label' => 'check-all',
                'attributes' => [
                    'data-icon' => 'bi bi-check-all',
                ],
            ],
            [
                'value' => 'bi bi-check-circle-fill',
                'label' => 'check-circle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-check-circle-fill',
                ],
            ],
            [
                'value' => 'bi bi-check-circle',
                'label' => 'check-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-check-circle',
                ],
            ],
            [
                'value' => 'bi bi-check-square-fill',
                'label' => 'check-square-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-check-square-fill',
                ],
            ],
            [
                'value' => 'bi bi-check-square',
                'label' => 'check-square',
                'attributes' => [
                    'data-icon' => 'bi bi-check-square',
                ],
            ],
            [
                'value' => 'bi bi-check',
                'label' => 'check',
                'attributes' => [
                    'data-icon' => 'bi bi-check',
                ],
            ],
            [
                'value' => 'bi bi-check2-all',
                'label' => 'check2-all',
                'attributes' => [
                    'data-icon' => 'bi bi-check2-all',
                ],
            ],
            [
                'value' => 'bi bi-check2-circle',
                'label' => 'check2-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-check2-circle',
                ],
            ],
            [
                'value' => 'bi bi-check2-square',
                'label' => 'check2-square',
                'attributes' => [
                    'data-icon' => 'bi bi-check2-square',
                ],
            ],
            [
                'value' => 'bi bi-check2',
                'label' => 'check2',
                'attributes' => [
                    'data-icon' => 'bi bi-check2',
                ],
            ],
            [
                'value' => 'bi bi-chevron-bar-contract',
                'label' => 'chevron-bar-contract',
                'attributes' => [
                    'data-icon' => 'bi bi-chevron-bar-contract',
                ],
            ],
            [
                'value' => 'bi bi-chevron-bar-down',
                'label' => 'chevron-bar-down',
                'attributes' => [
                    'data-icon' => 'bi bi-chevron-bar-down',
                ],
            ],
            [
                'value' => 'bi bi-chevron-bar-expand',
                'label' => 'chevron-bar-expand',
                'attributes' => [
                    'data-icon' => 'bi bi-chevron-bar-expand',
                ],
            ],
            [
                'value' => 'bi bi-chevron-bar-left',
                'label' => 'chevron-bar-left',
                'attributes' => [
                    'data-icon' => 'bi bi-chevron-bar-left',
                ],
            ],
            [
                'value' => 'bi bi-chevron-bar-right',
                'label' => 'chevron-bar-right',
                'attributes' => [
                    'data-icon' => 'bi bi-chevron-bar-right',
                ],
            ],
            [
                'value' => 'bi bi-chevron-bar-up',
                'label' => 'chevron-bar-up',
                'attributes' => [
                    'data-icon' => 'bi bi-chevron-bar-up',
                ],
            ],
            [
                'value' => 'bi bi-chevron-compact-down',
                'label' => 'chevron-compact-down',
                'attributes' => [
                    'data-icon' => 'bi bi-chevron-compact-down',
                ],
            ],
            [
                'value' => 'bi bi-chevron-compact-left',
                'label' => 'chevron-compact-left',
                'attributes' => [
                    'data-icon' => 'bi bi-chevron-compact-left',
                ],
            ],
            [
                'value' => 'bi bi-chevron-compact-right',
                'label' => 'chevron-compact-right',
                'attributes' => [
                    'data-icon' => 'bi bi-chevron-compact-right',
                ],
            ],
            [
                'value' => 'bi bi-chevron-compact-up',
                'label' => 'chevron-compact-up',
                'attributes' => [
                    'data-icon' => 'bi bi-chevron-compact-up',
                ],
            ],
            [
                'value' => 'bi bi-chevron-contract',
                'label' => 'chevron-contract',
                'attributes' => [
                    'data-icon' => 'bi bi-chevron-contract',
                ],
            ],
            [
                'value' => 'bi bi-chevron-double-down',
                'label' => 'chevron-double-down',
                'attributes' => [
                    'data-icon' => 'bi bi-chevron-double-down',
                ],
            ],
            [
                'value' => 'bi bi-chevron-double-left',
                'label' => 'chevron-double-left',
                'attributes' => [
                    'data-icon' => 'bi bi-chevron-double-left',
                ],
            ],
            [
                'value' => 'bi bi-chevron-double-right',
                'label' => 'chevron-double-right',
                'attributes' => [
                    'data-icon' => 'bi bi-chevron-double-right',
                ],
            ],
            [
                'value' => 'bi bi-chevron-double-up',
                'label' => 'chevron-double-up',
                'attributes' => [
                    'data-icon' => 'bi bi-chevron-double-up',
                ],
            ],
            [
                'value' => 'bi bi-chevron-down',
                'label' => 'chevron-down',
                'attributes' => [
                    'data-icon' => 'bi bi-chevron-down',
                ],
            ],
            [
                'value' => 'bi bi-chevron-expand',
                'label' => 'chevron-expand',
                'attributes' => [
                    'data-icon' => 'bi bi-chevron-expand',
                ],
            ],
            [
                'value' => 'bi bi-chevron-left',
                'label' => 'chevron-left',
                'attributes' => [
                    'data-icon' => 'bi bi-chevron-left',
                ],
            ],
            [
                'value' => 'bi bi-chevron-right',
                'label' => 'chevron-right',
                'attributes' => [
                    'data-icon' => 'bi bi-chevron-right',
                ],
            ],
            [
                'value' => 'bi bi-chevron-up',
                'label' => 'chevron-up',
                'attributes' => [
                    'data-icon' => 'bi bi-chevron-up',
                ],
            ],
            [
                'value' => 'bi bi-circle-fill',
                'label' => 'circle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-circle-fill',
                ],
            ],
            [
                'value' => 'bi bi-circle-half',
                'label' => 'circle-half',
                'attributes' => [
                    'data-icon' => 'bi bi-circle-half',
                ],
            ],
            [
                'value' => 'bi bi-circle-square',
                'label' => 'circle-square',
                'attributes' => [
                    'data-icon' => 'bi bi-circle-square',
                ],
            ],
            [
                'value' => 'bi bi-circle',
                'label' => 'circle',
                'attributes' => [
                    'data-icon' => 'bi bi-circle',
                ],
            ],
            [
                'value' => 'bi bi-clipboard-check',
                'label' => 'clipboard-check',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard-check',
                ],
            ],
            [
                'value' => 'bi bi-clipboard-data',
                'label' => 'clipboard-data',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard-data',
                ],
            ],
            [
                'value' => 'bi bi-clipboard-minus',
                'label' => 'clipboard-minus',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard-minus',
                ],
            ],
            [
                'value' => 'bi bi-clipboard-plus',
                'label' => 'clipboard-plus',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard-plus',
                ],
            ],
            [
                'value' => 'bi bi-clipboard-x',
                'label' => 'clipboard-x',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard-x',
                ],
            ],
            [
                'value' => 'bi bi-clipboard',
                'label' => 'clipboard',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard',
                ],
            ],
            [
                'value' => 'bi bi-clock-fill',
                'label' => 'clock-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-clock-fill',
                ],
            ],
            [
                'value' => 'bi bi-clock-history',
                'label' => 'clock-history',
                'attributes' => [
                    'data-icon' => 'bi bi-clock-history',
                ],
            ],
            [
                'value' => 'bi bi-clock',
                'label' => 'clock',
                'attributes' => [
                    'data-icon' => 'bi bi-clock',
                ],
            ],
            [
                'value' => 'bi bi-cloud-arrow-down-fill',
                'label' => 'cloud-arrow-down-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-arrow-down-fill',
                ],
            ],
            [
                'value' => 'bi bi-cloud-arrow-down',
                'label' => 'cloud-arrow-down',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-arrow-down',
                ],
            ],
            [
                'value' => 'bi bi-cloud-arrow-up-fill',
                'label' => 'cloud-arrow-up-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-arrow-up-fill',
                ],
            ],
            [
                'value' => 'bi bi-cloud-arrow-up',
                'label' => 'cloud-arrow-up',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-arrow-up',
                ],
            ],
            [
                'value' => 'bi bi-cloud-check-fill',
                'label' => 'cloud-check-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-check-fill',
                ],
            ],
            [
                'value' => 'bi bi-cloud-check',
                'label' => 'cloud-check',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-check',
                ],
            ],
            [
                'value' => 'bi bi-cloud-download-fill',
                'label' => 'cloud-download-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-download-fill',
                ],
            ],
            [
                'value' => 'bi bi-cloud-download',
                'label' => 'cloud-download',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-download',
                ],
            ],
            [
                'value' => 'bi bi-cloud-drizzle-fill',
                'label' => 'cloud-drizzle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-drizzle-fill',
                ],
            ],
            [
                'value' => 'bi bi-cloud-drizzle',
                'label' => 'cloud-drizzle',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-drizzle',
                ],
            ],
            [
                'value' => 'bi bi-cloud-fill',
                'label' => 'cloud-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-fill',
                ],
            ],
            [
                'value' => 'bi bi-cloud-fog-fill',
                'label' => 'cloud-fog-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-fog-fill',
                ],
            ],
            [
                'value' => 'bi bi-cloud-fog',
                'label' => 'cloud-fog',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-fog',
                ],
            ],
            [
                'value' => 'bi bi-cloud-fog2-fill',
                'label' => 'cloud-fog2-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-fog2-fill',
                ],
            ],
            [
                'value' => 'bi bi-cloud-fog2',
                'label' => 'cloud-fog2',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-fog2',
                ],
            ],
            [
                'value' => 'bi bi-cloud-hail-fill',
                'label' => 'cloud-hail-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-hail-fill',
                ],
            ],
            [
                'value' => 'bi bi-cloud-hail',
                'label' => 'cloud-hail',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-hail',
                ],
            ],
            [
                'value' => 'bi bi-cloud-haze-1',
                'label' => 'cloud-haze-1',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-haze-1',
                ],
            ],
            [
                'value' => 'bi bi-cloud-haze-fill',
                'label' => 'cloud-haze-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-haze-fill',
                ],
            ],
            [
                'value' => 'bi bi-cloud-haze',
                'label' => 'cloud-haze',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-haze',
                ],
            ],
            [
                'value' => 'bi bi-cloud-haze2-fill',
                'label' => 'cloud-haze2-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-haze2-fill',
                ],
            ],
            [
                'value' => 'bi bi-cloud-lightning-fill',
                'label' => 'cloud-lightning-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-lightning-fill',
                ],
            ],
            [
                'value' => 'bi bi-cloud-lightning-rain-fill',
                'label' => 'cloud-lightning-rain-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-lightning-rain-fill',
                ],
            ],
            [
                'value' => 'bi bi-cloud-lightning-rain',
                'label' => 'cloud-lightning-rain',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-lightning-rain',
                ],
            ],
            [
                'value' => 'bi bi-cloud-lightning',
                'label' => 'cloud-lightning',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-lightning',
                ],
            ],
            [
                'value' => 'bi bi-cloud-minus-fill',
                'label' => 'cloud-minus-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-minus-fill',
                ],
            ],
            [
                'value' => 'bi bi-cloud-minus',
                'label' => 'cloud-minus',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-minus',
                ],
            ],
            [
                'value' => 'bi bi-cloud-moon-fill',
                'label' => 'cloud-moon-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-moon-fill',
                ],
            ],
            [
                'value' => 'bi bi-cloud-moon',
                'label' => 'cloud-moon',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-moon',
                ],
            ],
            [
                'value' => 'bi bi-cloud-plus-fill',
                'label' => 'cloud-plus-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-plus-fill',
                ],
            ],
            [
                'value' => 'bi bi-cloud-plus',
                'label' => 'cloud-plus',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-plus',
                ],
            ],
            [
                'value' => 'bi bi-cloud-rain-fill',
                'label' => 'cloud-rain-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-rain-fill',
                ],
            ],
            [
                'value' => 'bi bi-cloud-rain-heavy-fill',
                'label' => 'cloud-rain-heavy-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-rain-heavy-fill',
                ],
            ],
            [
                'value' => 'bi bi-cloud-rain-heavy',
                'label' => 'cloud-rain-heavy',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-rain-heavy',
                ],
            ],
            [
                'value' => 'bi bi-cloud-rain',
                'label' => 'cloud-rain',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-rain',
                ],
            ],
            [
                'value' => 'bi bi-cloud-slash-fill',
                'label' => 'cloud-slash-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-slash-fill',
                ],
            ],
            [
                'value' => 'bi bi-cloud-slash',
                'label' => 'cloud-slash',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-slash',
                ],
            ],
            [
                'value' => 'bi bi-cloud-sleet-fill',
                'label' => 'cloud-sleet-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-sleet-fill',
                ],
            ],
            [
                'value' => 'bi bi-cloud-sleet',
                'label' => 'cloud-sleet',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-sleet',
                ],
            ],
            [
                'value' => 'bi bi-cloud-snow-fill',
                'label' => 'cloud-snow-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-snow-fill',
                ],
            ],
            [
                'value' => 'bi bi-cloud-snow',
                'label' => 'cloud-snow',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-snow',
                ],
            ],
            [
                'value' => 'bi bi-cloud-sun-fill',
                'label' => 'cloud-sun-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-sun-fill',
                ],
            ],
            [
                'value' => 'bi bi-cloud-sun',
                'label' => 'cloud-sun',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-sun',
                ],
            ],
            [
                'value' => 'bi bi-cloud-upload-fill',
                'label' => 'cloud-upload-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-upload-fill',
                ],
            ],
            [
                'value' => 'bi bi-cloud-upload',
                'label' => 'cloud-upload',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-upload',
                ],
            ],
            [
                'value' => 'bi bi-cloud',
                'label' => 'cloud',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud',
                ],
            ],
            [
                'value' => 'bi bi-clouds-fill',
                'label' => 'clouds-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-clouds-fill',
                ],
            ],
            [
                'value' => 'bi bi-clouds',
                'label' => 'clouds',
                'attributes' => [
                    'data-icon' => 'bi bi-clouds',
                ],
            ],
            [
                'value' => 'bi bi-cloudy-fill',
                'label' => 'cloudy-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cloudy-fill',
                ],
            ],
            [
                'value' => 'bi bi-cloudy',
                'label' => 'cloudy',
                'attributes' => [
                    'data-icon' => 'bi bi-cloudy',
                ],
            ],
            [
                'value' => 'bi bi-code-slash',
                'label' => 'code-slash',
                'attributes' => [
                    'data-icon' => 'bi bi-code-slash',
                ],
            ],
            [
                'value' => 'bi bi-code-square',
                'label' => 'code-square',
                'attributes' => [
                    'data-icon' => 'bi bi-code-square',
                ],
            ],
            [
                'value' => 'bi bi-code',
                'label' => 'code',
                'attributes' => [
                    'data-icon' => 'bi bi-code',
                ],
            ],
            [
                'value' => 'bi bi-collection-fill',
                'label' => 'collection-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-collection-fill',
                ],
            ],
            [
                'value' => 'bi bi-collection-play-fill',
                'label' => 'collection-play-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-collection-play-fill',
                ],
            ],
            [
                'value' => 'bi bi-collection-play',
                'label' => 'collection-play',
                'attributes' => [
                    'data-icon' => 'bi bi-collection-play',
                ],
            ],
            [
                'value' => 'bi bi-collection',
                'label' => 'collection',
                'attributes' => [
                    'data-icon' => 'bi bi-collection',
                ],
            ],
            [
                'value' => 'bi bi-columns-gap',
                'label' => 'columns-gap',
                'attributes' => [
                    'data-icon' => 'bi bi-columns-gap',
                ],
            ],
            [
                'value' => 'bi bi-columns',
                'label' => 'columns',
                'attributes' => [
                    'data-icon' => 'bi bi-columns',
                ],
            ],
            [
                'value' => 'bi bi-command',
                'label' => 'command',
                'attributes' => [
                    'data-icon' => 'bi bi-command',
                ],
            ],
            [
                'value' => 'bi bi-compass-fill',
                'label' => 'compass-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-compass-fill',
                ],
            ],
            [
                'value' => 'bi bi-compass',
                'label' => 'compass',
                'attributes' => [
                    'data-icon' => 'bi bi-compass',
                ],
            ],
            [
                'value' => 'bi bi-cone-striped',
                'label' => 'cone-striped',
                'attributes' => [
                    'data-icon' => 'bi bi-cone-striped',
                ],
            ],
            [
                'value' => 'bi bi-cone',
                'label' => 'cone',
                'attributes' => [
                    'data-icon' => 'bi bi-cone',
                ],
            ],
            [
                'value' => 'bi bi-controller',
                'label' => 'controller',
                'attributes' => [
                    'data-icon' => 'bi bi-controller',
                ],
            ],
            [
                'value' => 'bi bi-cpu-fill',
                'label' => 'cpu-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cpu-fill',
                ],
            ],
            [
                'value' => 'bi bi-cpu',
                'label' => 'cpu',
                'attributes' => [
                    'data-icon' => 'bi bi-cpu',
                ],
            ],
            [
                'value' => 'bi bi-credit-card-2-back-fill',
                'label' => 'credit-card-2-back-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-credit-card-2-back-fill',
                ],
            ],
            [
                'value' => 'bi bi-credit-card-2-back',
                'label' => 'credit-card-2-back',
                'attributes' => [
                    'data-icon' => 'bi bi-credit-card-2-back',
                ],
            ],
            [
                'value' => 'bi bi-credit-card-2-front-fill',
                'label' => 'credit-card-2-front-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-credit-card-2-front-fill',
                ],
            ],
            [
                'value' => 'bi bi-credit-card-2-front',
                'label' => 'credit-card-2-front',
                'attributes' => [
                    'data-icon' => 'bi bi-credit-card-2-front',
                ],
            ],
            [
                'value' => 'bi bi-credit-card-fill',
                'label' => 'credit-card-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-credit-card-fill',
                ],
            ],
            [
                'value' => 'bi bi-credit-card',
                'label' => 'credit-card',
                'attributes' => [
                    'data-icon' => 'bi bi-credit-card',
                ],
            ],
            [
                'value' => 'bi bi-crop',
                'label' => 'crop',
                'attributes' => [
                    'data-icon' => 'bi bi-crop',
                ],
            ],
            [
                'value' => 'bi bi-cup-fill',
                'label' => 'cup-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cup-fill',
                ],
            ],
            [
                'value' => 'bi bi-cup-straw',
                'label' => 'cup-straw',
                'attributes' => [
                    'data-icon' => 'bi bi-cup-straw',
                ],
            ],
            [
                'value' => 'bi bi-cup',
                'label' => 'cup',
                'attributes' => [
                    'data-icon' => 'bi bi-cup',
                ],
            ],
            [
                'value' => 'bi bi-cursor-fill',
                'label' => 'cursor-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-cursor-fill',
                ],
            ],
            [
                'value' => 'bi bi-cursor-text',
                'label' => 'cursor-text',
                'attributes' => [
                    'data-icon' => 'bi bi-cursor-text',
                ],
            ],
            [
                'value' => 'bi bi-cursor',
                'label' => 'cursor',
                'attributes' => [
                    'data-icon' => 'bi bi-cursor',
                ],
            ],
            [
                'value' => 'bi bi-dash-circle-dotted',
                'label' => 'dash-circle-dotted',
                'attributes' => [
                    'data-icon' => 'bi bi-dash-circle-dotted',
                ],
            ],
            [
                'value' => 'bi bi-dash-circle-fill',
                'label' => 'dash-circle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-dash-circle-fill',
                ],
            ],
            [
                'value' => 'bi bi-dash-circle',
                'label' => 'dash-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-dash-circle',
                ],
            ],
            [
                'value' => 'bi bi-dash-square-dotted',
                'label' => 'dash-square-dotted',
                'attributes' => [
                    'data-icon' => 'bi bi-dash-square-dotted',
                ],
            ],
            [
                'value' => 'bi bi-dash-square-fill',
                'label' => 'dash-square-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-dash-square-fill',
                ],
            ],
            [
                'value' => 'bi bi-dash-square',
                'label' => 'dash-square',
                'attributes' => [
                    'data-icon' => 'bi bi-dash-square',
                ],
            ],
            [
                'value' => 'bi bi-dash',
                'label' => 'dash',
                'attributes' => [
                    'data-icon' => 'bi bi-dash',
                ],
            ],
            [
                'value' => 'bi bi-diagram-2-fill',
                'label' => 'diagram-2-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-diagram-2-fill',
                ],
            ],
            [
                'value' => 'bi bi-diagram-2',
                'label' => 'diagram-2',
                'attributes' => [
                    'data-icon' => 'bi bi-diagram-2',
                ],
            ],
            [
                'value' => 'bi bi-diagram-3-fill',
                'label' => 'diagram-3-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-diagram-3-fill',
                ],
            ],
            [
                'value' => 'bi bi-diagram-3',
                'label' => 'diagram-3',
                'attributes' => [
                    'data-icon' => 'bi bi-diagram-3',
                ],
            ],
            [
                'value' => 'bi bi-diamond-fill',
                'label' => 'diamond-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-diamond-fill',
                ],
            ],
            [
                'value' => 'bi bi-diamond-half',
                'label' => 'diamond-half',
                'attributes' => [
                    'data-icon' => 'bi bi-diamond-half',
                ],
            ],
            [
                'value' => 'bi bi-diamond',
                'label' => 'diamond',
                'attributes' => [
                    'data-icon' => 'bi bi-diamond',
                ],
            ],
            [
                'value' => 'bi bi-dice-1-fill',
                'label' => 'dice-1-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-dice-1-fill',
                ],
            ],
            [
                'value' => 'bi bi-dice-1',
                'label' => 'dice-1',
                'attributes' => [
                    'data-icon' => 'bi bi-dice-1',
                ],
            ],
            [
                'value' => 'bi bi-dice-2-fill',
                'label' => 'dice-2-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-dice-2-fill',
                ],
            ],
            [
                'value' => 'bi bi-dice-2',
                'label' => 'dice-2',
                'attributes' => [
                    'data-icon' => 'bi bi-dice-2',
                ],
            ],
            [
                'value' => 'bi bi-dice-3-fill',
                'label' => 'dice-3-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-dice-3-fill',
                ],
            ],
            [
                'value' => 'bi bi-dice-3',
                'label' => 'dice-3',
                'attributes' => [
                    'data-icon' => 'bi bi-dice-3',
                ],
            ],
            [
                'value' => 'bi bi-dice-4-fill',
                'label' => 'dice-4-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-dice-4-fill',
                ],
            ],
            [
                'value' => 'bi bi-dice-4',
                'label' => 'dice-4',
                'attributes' => [
                    'data-icon' => 'bi bi-dice-4',
                ],
            ],
            [
                'value' => 'bi bi-dice-5-fill',
                'label' => 'dice-5-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-dice-5-fill',
                ],
            ],
            [
                'value' => 'bi bi-dice-5',
                'label' => 'dice-5',
                'attributes' => [
                    'data-icon' => 'bi bi-dice-5',
                ],
            ],
            [
                'value' => 'bi bi-dice-6-fill',
                'label' => 'dice-6-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-dice-6-fill',
                ],
            ],
            [
                'value' => 'bi bi-dice-6',
                'label' => 'dice-6',
                'attributes' => [
                    'data-icon' => 'bi bi-dice-6',
                ],
            ],
            [
                'value' => 'bi bi-disc-fill',
                'label' => 'disc-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-disc-fill',
                ],
            ],
            [
                'value' => 'bi bi-disc',
                'label' => 'disc',
                'attributes' => [
                    'data-icon' => 'bi bi-disc',
                ],
            ],
            [
                'value' => 'bi bi-discord',
                'label' => 'discord',
                'attributes' => [
                    'data-icon' => 'bi bi-discord',
                ],
            ],
            [
                'value' => 'bi bi-display-fill',
                'label' => 'display-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-display-fill',
                ],
            ],
            [
                'value' => 'bi bi-display',
                'label' => 'display',
                'attributes' => [
                    'data-icon' => 'bi bi-display',
                ],
            ],
            [
                'value' => 'bi bi-distribute-horizontal',
                'label' => 'distribute-horizontal',
                'attributes' => [
                    'data-icon' => 'bi bi-distribute-horizontal',
                ],
            ],
            [
                'value' => 'bi bi-distribute-vertical',
                'label' => 'distribute-vertical',
                'attributes' => [
                    'data-icon' => 'bi bi-distribute-vertical',
                ],
            ],
            [
                'value' => 'bi bi-door-closed-fill',
                'label' => 'door-closed-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-door-closed-fill',
                ],
            ],
            [
                'value' => 'bi bi-door-closed',
                'label' => 'door-closed',
                'attributes' => [
                    'data-icon' => 'bi bi-door-closed',
                ],
            ],
            [
                'value' => 'bi bi-door-open-fill',
                'label' => 'door-open-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-door-open-fill',
                ],
            ],
            [
                'value' => 'bi bi-door-open',
                'label' => 'door-open',
                'attributes' => [
                    'data-icon' => 'bi bi-door-open',
                ],
            ],
            [
                'value' => 'bi bi-dot',
                'label' => 'dot',
                'attributes' => [
                    'data-icon' => 'bi bi-dot',
                ],
            ],
            [
                'value' => 'bi bi-download',
                'label' => 'download',
                'attributes' => [
                    'data-icon' => 'bi bi-download',
                ],
            ],
            [
                'value' => 'bi bi-droplet-fill',
                'label' => 'droplet-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-droplet-fill',
                ],
            ],
            [
                'value' => 'bi bi-droplet-half',
                'label' => 'droplet-half',
                'attributes' => [
                    'data-icon' => 'bi bi-droplet-half',
                ],
            ],
            [
                'value' => 'bi bi-droplet',
                'label' => 'droplet',
                'attributes' => [
                    'data-icon' => 'bi bi-droplet',
                ],
            ],
            [
                'value' => 'bi bi-earbuds',
                'label' => 'earbuds',
                'attributes' => [
                    'data-icon' => 'bi bi-earbuds',
                ],
            ],
            [
                'value' => 'bi bi-easel-fill',
                'label' => 'easel-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-easel-fill',
                ],
            ],
            [
                'value' => 'bi bi-easel',
                'label' => 'easel',
                'attributes' => [
                    'data-icon' => 'bi bi-easel',
                ],
            ],
            [
                'value' => 'bi bi-egg-fill',
                'label' => 'egg-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-egg-fill',
                ],
            ],
            [
                'value' => 'bi bi-egg-fried',
                'label' => 'egg-fried',
                'attributes' => [
                    'data-icon' => 'bi bi-egg-fried',
                ],
            ],
            [
                'value' => 'bi bi-egg',
                'label' => 'egg',
                'attributes' => [
                    'data-icon' => 'bi bi-egg',
                ],
            ],
            [
                'value' => 'bi bi-eject-fill',
                'label' => 'eject-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-eject-fill',
                ],
            ],
            [
                'value' => 'bi bi-eject',
                'label' => 'eject',
                'attributes' => [
                    'data-icon' => 'bi bi-eject',
                ],
            ],
            [
                'value' => 'bi bi-emoji-angry-fill',
                'label' => 'emoji-angry-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-emoji-angry-fill',
                ],
            ],
            [
                'value' => 'bi bi-emoji-angry',
                'label' => 'emoji-angry',
                'attributes' => [
                    'data-icon' => 'bi bi-emoji-angry',
                ],
            ],
            [
                'value' => 'bi bi-emoji-dizzy-fill',
                'label' => 'emoji-dizzy-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-emoji-dizzy-fill',
                ],
            ],
            [
                'value' => 'bi bi-emoji-dizzy',
                'label' => 'emoji-dizzy',
                'attributes' => [
                    'data-icon' => 'bi bi-emoji-dizzy',
                ],
            ],
            [
                'value' => 'bi bi-emoji-expressionless-fill',
                'label' => 'emoji-expressionless-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-emoji-expressionless-fill',
                ],
            ],
            [
                'value' => 'bi bi-emoji-expressionless',
                'label' => 'emoji-expressionless',
                'attributes' => [
                    'data-icon' => 'bi bi-emoji-expressionless',
                ],
            ],
            [
                'value' => 'bi bi-emoji-frown-fill',
                'label' => 'emoji-frown-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-emoji-frown-fill',
                ],
            ],
            [
                'value' => 'bi bi-emoji-frown',
                'label' => 'emoji-frown',
                'attributes' => [
                    'data-icon' => 'bi bi-emoji-frown',
                ],
            ],
            [
                'value' => 'bi bi-emoji-heart-eyes-fill',
                'label' => 'emoji-heart-eyes-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-emoji-heart-eyes-fill',
                ],
            ],
            [
                'value' => 'bi bi-emoji-heart-eyes',
                'label' => 'emoji-heart-eyes',
                'attributes' => [
                    'data-icon' => 'bi bi-emoji-heart-eyes',
                ],
            ],
            [
                'value' => 'bi bi-emoji-laughing-fill',
                'label' => 'emoji-laughing-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-emoji-laughing-fill',
                ],
            ],
            [
                'value' => 'bi bi-emoji-laughing',
                'label' => 'emoji-laughing',
                'attributes' => [
                    'data-icon' => 'bi bi-emoji-laughing',
                ],
            ],
            [
                'value' => 'bi bi-emoji-neutral-fill',
                'label' => 'emoji-neutral-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-emoji-neutral-fill',
                ],
            ],
            [
                'value' => 'bi bi-emoji-neutral',
                'label' => 'emoji-neutral',
                'attributes' => [
                    'data-icon' => 'bi bi-emoji-neutral',
                ],
            ],
            [
                'value' => 'bi bi-emoji-smile-fill',
                'label' => 'emoji-smile-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-emoji-smile-fill',
                ],
            ],
            [
                'value' => 'bi bi-emoji-smile-upside-down-fill',
                'label' => 'emoji-smile-upside-down-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-emoji-smile-upside-down-fill',
                ],
            ],
            [
                'value' => 'bi bi-emoji-smile-upside-down',
                'label' => 'emoji-smile-upside-down',
                'attributes' => [
                    'data-icon' => 'bi bi-emoji-smile-upside-down',
                ],
            ],
            [
                'value' => 'bi bi-emoji-smile',
                'label' => 'emoji-smile',
                'attributes' => [
                    'data-icon' => 'bi bi-emoji-smile',
                ],
            ],
            [
                'value' => 'bi bi-emoji-sunglasses-fill',
                'label' => 'emoji-sunglasses-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-emoji-sunglasses-fill',
                ],
            ],
            [
                'value' => 'bi bi-emoji-sunglasses',
                'label' => 'emoji-sunglasses',
                'attributes' => [
                    'data-icon' => 'bi bi-emoji-sunglasses',
                ],
            ],
            [
                'value' => 'bi bi-emoji-wink-fill',
                'label' => 'emoji-wink-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-emoji-wink-fill',
                ],
            ],
            [
                'value' => 'bi bi-emoji-wink',
                'label' => 'emoji-wink',
                'attributes' => [
                    'data-icon' => 'bi bi-emoji-wink',
                ],
            ],
            [
                'value' => 'bi bi-envelope-fill',
                'label' => 'envelope-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-fill',
                ],
            ],
            [
                'value' => 'bi bi-envelope-open-fill',
                'label' => 'envelope-open-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-open-fill',
                ],
            ],
            [
                'value' => 'bi bi-envelope-open',
                'label' => 'envelope-open',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-open',
                ],
            ],
            [
                'value' => 'bi bi-envelope',
                'label' => 'envelope',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope',
                ],
            ],
            [
                'value' => 'bi bi-eraser-fill',
                'label' => 'eraser-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-eraser-fill',
                ],
            ],
            [
                'value' => 'bi bi-eraser',
                'label' => 'eraser',
                'attributes' => [
                    'data-icon' => 'bi bi-eraser',
                ],
            ],
            [
                'value' => 'bi bi-exclamation-circle-fill',
                'label' => 'exclamation-circle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-exclamation-circle-fill',
                ],
            ],
            [
                'value' => 'bi bi-exclamation-circle',
                'label' => 'exclamation-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-exclamation-circle',
                ],
            ],
            [
                'value' => 'bi bi-exclamation-diamond-fill',
                'label' => 'exclamation-diamond-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-exclamation-diamond-fill',
                ],
            ],
            [
                'value' => 'bi bi-exclamation-diamond',
                'label' => 'exclamation-diamond',
                'attributes' => [
                    'data-icon' => 'bi bi-exclamation-diamond',
                ],
            ],
            [
                'value' => 'bi bi-exclamation-octagon-fill',
                'label' => 'exclamation-octagon-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-exclamation-octagon-fill',
                ],
            ],
            [
                'value' => 'bi bi-exclamation-octagon',
                'label' => 'exclamation-octagon',
                'attributes' => [
                    'data-icon' => 'bi bi-exclamation-octagon',
                ],
            ],
            [
                'value' => 'bi bi-exclamation-square-fill',
                'label' => 'exclamation-square-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-exclamation-square-fill',
                ],
            ],
            [
                'value' => 'bi bi-exclamation-square',
                'label' => 'exclamation-square',
                'attributes' => [
                    'data-icon' => 'bi bi-exclamation-square',
                ],
            ],
            [
                'value' => 'bi bi-exclamation-triangle-fill',
                'label' => 'exclamation-triangle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-exclamation-triangle-fill',
                ],
            ],
            [
                'value' => 'bi bi-exclamation-triangle',
                'label' => 'exclamation-triangle',
                'attributes' => [
                    'data-icon' => 'bi bi-exclamation-triangle',
                ],
            ],
            [
                'value' => 'bi bi-exclamation',
                'label' => 'exclamation',
                'attributes' => [
                    'data-icon' => 'bi bi-exclamation',
                ],
            ],
            [
                'value' => 'bi bi-exclude',
                'label' => 'exclude',
                'attributes' => [
                    'data-icon' => 'bi bi-exclude',
                ],
            ],
            [
                'value' => 'bi bi-eye-fill',
                'label' => 'eye-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-eye-fill',
                ],
            ],
            [
                'value' => 'bi bi-eye-slash-fill',
                'label' => 'eye-slash-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-eye-slash-fill',
                ],
            ],
            [
                'value' => 'bi bi-eye-slash',
                'label' => 'eye-slash',
                'attributes' => [
                    'data-icon' => 'bi bi-eye-slash',
                ],
            ],
            [
                'value' => 'bi bi-eye',
                'label' => 'eye',
                'attributes' => [
                    'data-icon' => 'bi bi-eye',
                ],
            ],
            [
                'value' => 'bi bi-eyedropper',
                'label' => 'eyedropper',
                'attributes' => [
                    'data-icon' => 'bi bi-eyedropper',
                ],
            ],
            [
                'value' => 'bi bi-eyeglasses',
                'label' => 'eyeglasses',
                'attributes' => [
                    'data-icon' => 'bi bi-eyeglasses',
                ],
            ],
            [
                'value' => 'bi bi-facebook',
                'label' => 'facebook',
                'attributes' => [
                    'data-icon' => 'bi bi-facebook',
                ],
            ],
            [
                'value' => 'bi bi-file-arrow-down-fill',
                'label' => 'file-arrow-down-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-arrow-down-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-arrow-down',
                'label' => 'file-arrow-down',
                'attributes' => [
                    'data-icon' => 'bi bi-file-arrow-down',
                ],
            ],
            [
                'value' => 'bi bi-file-arrow-up-fill',
                'label' => 'file-arrow-up-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-arrow-up-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-arrow-up',
                'label' => 'file-arrow-up',
                'attributes' => [
                    'data-icon' => 'bi bi-file-arrow-up',
                ],
            ],
            [
                'value' => 'bi bi-file-bar-graph-fill',
                'label' => 'file-bar-graph-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-bar-graph-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-bar-graph',
                'label' => 'file-bar-graph',
                'attributes' => [
                    'data-icon' => 'bi bi-file-bar-graph',
                ],
            ],
            [
                'value' => 'bi bi-file-binary-fill',
                'label' => 'file-binary-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-binary-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-binary',
                'label' => 'file-binary',
                'attributes' => [
                    'data-icon' => 'bi bi-file-binary',
                ],
            ],
            [
                'value' => 'bi bi-file-break-fill',
                'label' => 'file-break-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-break-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-break',
                'label' => 'file-break',
                'attributes' => [
                    'data-icon' => 'bi bi-file-break',
                ],
            ],
            [
                'value' => 'bi bi-file-check-fill',
                'label' => 'file-check-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-check-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-check',
                'label' => 'file-check',
                'attributes' => [
                    'data-icon' => 'bi bi-file-check',
                ],
            ],
            [
                'value' => 'bi bi-file-code-fill',
                'label' => 'file-code-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-code-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-code',
                'label' => 'file-code',
                'attributes' => [
                    'data-icon' => 'bi bi-file-code',
                ],
            ],
            [
                'value' => 'bi bi-file-diff-fill',
                'label' => 'file-diff-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-diff-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-diff',
                'label' => 'file-diff',
                'attributes' => [
                    'data-icon' => 'bi bi-file-diff',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-arrow-down-fill',
                'label' => 'file-earmark-arrow-down-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-arrow-down-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-arrow-down',
                'label' => 'file-earmark-arrow-down',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-arrow-down',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-arrow-up-fill',
                'label' => 'file-earmark-arrow-up-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-arrow-up-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-arrow-up',
                'label' => 'file-earmark-arrow-up',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-arrow-up',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-bar-graph-fill',
                'label' => 'file-earmark-bar-graph-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-bar-graph-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-bar-graph',
                'label' => 'file-earmark-bar-graph',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-bar-graph',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-binary-fill',
                'label' => 'file-earmark-binary-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-binary-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-binary',
                'label' => 'file-earmark-binary',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-binary',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-break-fill',
                'label' => 'file-earmark-break-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-break-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-break',
                'label' => 'file-earmark-break',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-break',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-check-fill',
                'label' => 'file-earmark-check-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-check-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-check',
                'label' => 'file-earmark-check',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-check',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-code-fill',
                'label' => 'file-earmark-code-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-code-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-code',
                'label' => 'file-earmark-code',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-code',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-diff-fill',
                'label' => 'file-earmark-diff-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-diff-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-diff',
                'label' => 'file-earmark-diff',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-diff',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-easel-fill',
                'label' => 'file-earmark-easel-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-easel-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-easel',
                'label' => 'file-earmark-easel',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-easel',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-excel-fill',
                'label' => 'file-earmark-excel-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-excel-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-excel',
                'label' => 'file-earmark-excel',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-excel',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-fill',
                'label' => 'file-earmark-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-font-fill',
                'label' => 'file-earmark-font-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-font-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-font',
                'label' => 'file-earmark-font',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-font',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-image-fill',
                'label' => 'file-earmark-image-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-image-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-image',
                'label' => 'file-earmark-image',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-image',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-lock-fill',
                'label' => 'file-earmark-lock-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-lock-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-lock',
                'label' => 'file-earmark-lock',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-lock',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-lock2-fill',
                'label' => 'file-earmark-lock2-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-lock2-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-lock2',
                'label' => 'file-earmark-lock2',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-lock2',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-medical-fill',
                'label' => 'file-earmark-medical-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-medical-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-medical',
                'label' => 'file-earmark-medical',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-medical',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-minus-fill',
                'label' => 'file-earmark-minus-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-minus-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-minus',
                'label' => 'file-earmark-minus',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-minus',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-music-fill',
                'label' => 'file-earmark-music-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-music-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-music',
                'label' => 'file-earmark-music',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-music',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-person-fill',
                'label' => 'file-earmark-person-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-person-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-person',
                'label' => 'file-earmark-person',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-person',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-play-fill',
                'label' => 'file-earmark-play-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-play-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-play',
                'label' => 'file-earmark-play',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-play',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-plus-fill',
                'label' => 'file-earmark-plus-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-plus-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-plus',
                'label' => 'file-earmark-plus',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-plus',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-post-fill',
                'label' => 'file-earmark-post-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-post-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-post',
                'label' => 'file-earmark-post',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-post',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-ppt-fill',
                'label' => 'file-earmark-ppt-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-ppt-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-ppt',
                'label' => 'file-earmark-ppt',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-ppt',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-richtext-fill',
                'label' => 'file-earmark-richtext-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-richtext-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-richtext',
                'label' => 'file-earmark-richtext',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-richtext',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-ruled-fill',
                'label' => 'file-earmark-ruled-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-ruled-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-ruled',
                'label' => 'file-earmark-ruled',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-ruled',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-slides-fill',
                'label' => 'file-earmark-slides-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-slides-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-slides',
                'label' => 'file-earmark-slides',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-slides',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-spreadsheet-fill',
                'label' => 'file-earmark-spreadsheet-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-spreadsheet-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-spreadsheet',
                'label' => 'file-earmark-spreadsheet',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-spreadsheet',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-text-fill',
                'label' => 'file-earmark-text-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-text-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-text',
                'label' => 'file-earmark-text',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-text',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-word-fill',
                'label' => 'file-earmark-word-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-word-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-word',
                'label' => 'file-earmark-word',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-word',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-x-fill',
                'label' => 'file-earmark-x-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-x-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-x',
                'label' => 'file-earmark-x',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-x',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-zip-fill',
                'label' => 'file-earmark-zip-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-zip-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-zip',
                'label' => 'file-earmark-zip',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-zip',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark',
                'label' => 'file-earmark',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark',
                ],
            ],
            [
                'value' => 'bi bi-file-easel-fill',
                'label' => 'file-easel-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-easel-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-easel',
                'label' => 'file-easel',
                'attributes' => [
                    'data-icon' => 'bi bi-file-easel',
                ],
            ],
            [
                'value' => 'bi bi-file-excel-fill',
                'label' => 'file-excel-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-excel-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-excel',
                'label' => 'file-excel',
                'attributes' => [
                    'data-icon' => 'bi bi-file-excel',
                ],
            ],
            [
                'value' => 'bi bi-file-fill',
                'label' => 'file-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-font-fill',
                'label' => 'file-font-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-font-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-font',
                'label' => 'file-font',
                'attributes' => [
                    'data-icon' => 'bi bi-file-font',
                ],
            ],
            [
                'value' => 'bi bi-file-image-fill',
                'label' => 'file-image-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-image-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-image',
                'label' => 'file-image',
                'attributes' => [
                    'data-icon' => 'bi bi-file-image',
                ],
            ],
            [
                'value' => 'bi bi-file-lock-fill',
                'label' => 'file-lock-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-lock-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-lock',
                'label' => 'file-lock',
                'attributes' => [
                    'data-icon' => 'bi bi-file-lock',
                ],
            ],
            [
                'value' => 'bi bi-file-lock2-fill',
                'label' => 'file-lock2-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-lock2-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-lock2',
                'label' => 'file-lock2',
                'attributes' => [
                    'data-icon' => 'bi bi-file-lock2',
                ],
            ],
            [
                'value' => 'bi bi-file-medical-fill',
                'label' => 'file-medical-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-medical-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-medical',
                'label' => 'file-medical',
                'attributes' => [
                    'data-icon' => 'bi bi-file-medical',
                ],
            ],
            [
                'value' => 'bi bi-file-minus-fill',
                'label' => 'file-minus-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-minus-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-minus',
                'label' => 'file-minus',
                'attributes' => [
                    'data-icon' => 'bi bi-file-minus',
                ],
            ],
            [
                'value' => 'bi bi-file-music-fill',
                'label' => 'file-music-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-music-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-music',
                'label' => 'file-music',
                'attributes' => [
                    'data-icon' => 'bi bi-file-music',
                ],
            ],
            [
                'value' => 'bi bi-file-person-fill',
                'label' => 'file-person-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-person-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-person',
                'label' => 'file-person',
                'attributes' => [
                    'data-icon' => 'bi bi-file-person',
                ],
            ],
            [
                'value' => 'bi bi-file-play-fill',
                'label' => 'file-play-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-play-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-play',
                'label' => 'file-play',
                'attributes' => [
                    'data-icon' => 'bi bi-file-play',
                ],
            ],
            [
                'value' => 'bi bi-file-plus-fill',
                'label' => 'file-plus-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-plus-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-plus',
                'label' => 'file-plus',
                'attributes' => [
                    'data-icon' => 'bi bi-file-plus',
                ],
            ],
            [
                'value' => 'bi bi-file-post-fill',
                'label' => 'file-post-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-post-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-post',
                'label' => 'file-post',
                'attributes' => [
                    'data-icon' => 'bi bi-file-post',
                ],
            ],
            [
                'value' => 'bi bi-file-ppt-fill',
                'label' => 'file-ppt-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-ppt-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-ppt',
                'label' => 'file-ppt',
                'attributes' => [
                    'data-icon' => 'bi bi-file-ppt',
                ],
            ],
            [
                'value' => 'bi bi-file-richtext-fill',
                'label' => 'file-richtext-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-richtext-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-richtext',
                'label' => 'file-richtext',
                'attributes' => [
                    'data-icon' => 'bi bi-file-richtext',
                ],
            ],
            [
                'value' => 'bi bi-file-ruled-fill',
                'label' => 'file-ruled-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-ruled-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-ruled',
                'label' => 'file-ruled',
                'attributes' => [
                    'data-icon' => 'bi bi-file-ruled',
                ],
            ],
            [
                'value' => 'bi bi-file-slides-fill',
                'label' => 'file-slides-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-slides-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-slides',
                'label' => 'file-slides',
                'attributes' => [
                    'data-icon' => 'bi bi-file-slides',
                ],
            ],
            [
                'value' => 'bi bi-file-spreadsheet-fill',
                'label' => 'file-spreadsheet-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-spreadsheet-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-spreadsheet',
                'label' => 'file-spreadsheet',
                'attributes' => [
                    'data-icon' => 'bi bi-file-spreadsheet',
                ],
            ],
            [
                'value' => 'bi bi-file-text-fill',
                'label' => 'file-text-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-text-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-text',
                'label' => 'file-text',
                'attributes' => [
                    'data-icon' => 'bi bi-file-text',
                ],
            ],
            [
                'value' => 'bi bi-file-word-fill',
                'label' => 'file-word-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-word-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-word',
                'label' => 'file-word',
                'attributes' => [
                    'data-icon' => 'bi bi-file-word',
                ],
            ],
            [
                'value' => 'bi bi-file-x-fill',
                'label' => 'file-x-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-x-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-x',
                'label' => 'file-x',
                'attributes' => [
                    'data-icon' => 'bi bi-file-x',
                ],
            ],
            [
                'value' => 'bi bi-file-zip-fill',
                'label' => 'file-zip-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-zip-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-zip',
                'label' => 'file-zip',
                'attributes' => [
                    'data-icon' => 'bi bi-file-zip',
                ],
            ],
            [
                'value' => 'bi bi-file',
                'label' => 'file',
                'attributes' => [
                    'data-icon' => 'bi bi-file',
                ],
            ],
            [
                'value' => 'bi bi-files-alt',
                'label' => 'files-alt',
                'attributes' => [
                    'data-icon' => 'bi bi-files-alt',
                ],
            ],
            [
                'value' => 'bi bi-files',
                'label' => 'files',
                'attributes' => [
                    'data-icon' => 'bi bi-files',
                ],
            ],
            [
                'value' => 'bi bi-film',
                'label' => 'film',
                'attributes' => [
                    'data-icon' => 'bi bi-film',
                ],
            ],
            [
                'value' => 'bi bi-filter-circle-fill',
                'label' => 'filter-circle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-filter-circle-fill',
                ],
            ],
            [
                'value' => 'bi bi-filter-circle',
                'label' => 'filter-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-filter-circle',
                ],
            ],
            [
                'value' => 'bi bi-filter-left',
                'label' => 'filter-left',
                'attributes' => [
                    'data-icon' => 'bi bi-filter-left',
                ],
            ],
            [
                'value' => 'bi bi-filter-right',
                'label' => 'filter-right',
                'attributes' => [
                    'data-icon' => 'bi bi-filter-right',
                ],
            ],
            [
                'value' => 'bi bi-filter-square-fill',
                'label' => 'filter-square-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-filter-square-fill',
                ],
            ],
            [
                'value' => 'bi bi-filter-square',
                'label' => 'filter-square',
                'attributes' => [
                    'data-icon' => 'bi bi-filter-square',
                ],
            ],
            [
                'value' => 'bi bi-filter',
                'label' => 'filter',
                'attributes' => [
                    'data-icon' => 'bi bi-filter',
                ],
            ],
            [
                'value' => 'bi bi-flag-fill',
                'label' => 'flag-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-flag-fill',
                ],
            ],
            [
                'value' => 'bi bi-flag',
                'label' => 'flag',
                'attributes' => [
                    'data-icon' => 'bi bi-flag',
                ],
            ],
            [
                'value' => 'bi bi-flower1',
                'label' => 'flower1',
                'attributes' => [
                    'data-icon' => 'bi bi-flower1',
                ],
            ],
            [
                'value' => 'bi bi-flower2',
                'label' => 'flower2',
                'attributes' => [
                    'data-icon' => 'bi bi-flower2',
                ],
            ],
            [
                'value' => 'bi bi-flower3',
                'label' => 'flower3',
                'attributes' => [
                    'data-icon' => 'bi bi-flower3',
                ],
            ],
            [
                'value' => 'bi bi-folder-check',
                'label' => 'folder-check',
                'attributes' => [
                    'data-icon' => 'bi bi-folder-check',
                ],
            ],
            [
                'value' => 'bi bi-folder-fill',
                'label' => 'folder-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-folder-fill',
                ],
            ],
            [
                'value' => 'bi bi-folder-minus',
                'label' => 'folder-minus',
                'attributes' => [
                    'data-icon' => 'bi bi-folder-minus',
                ],
            ],
            [
                'value' => 'bi bi-folder-plus',
                'label' => 'folder-plus',
                'attributes' => [
                    'data-icon' => 'bi bi-folder-plus',
                ],
            ],
            [
                'value' => 'bi bi-folder-symlink-fill',
                'label' => 'folder-symlink-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-folder-symlink-fill',
                ],
            ],
            [
                'value' => 'bi bi-folder-symlink',
                'label' => 'folder-symlink',
                'attributes' => [
                    'data-icon' => 'bi bi-folder-symlink',
                ],
            ],
            [
                'value' => 'bi bi-folder-x',
                'label' => 'folder-x',
                'attributes' => [
                    'data-icon' => 'bi bi-folder-x',
                ],
            ],
            [
                'value' => 'bi bi-folder',
                'label' => 'folder',
                'attributes' => [
                    'data-icon' => 'bi bi-folder',
                ],
            ],
            [
                'value' => 'bi bi-folder2-open',
                'label' => 'folder2-open',
                'attributes' => [
                    'data-icon' => 'bi bi-folder2-open',
                ],
            ],
            [
                'value' => 'bi bi-folder2',
                'label' => 'folder2',
                'attributes' => [
                    'data-icon' => 'bi bi-folder2',
                ],
            ],
            [
                'value' => 'bi bi-fonts',
                'label' => 'fonts',
                'attributes' => [
                    'data-icon' => 'bi bi-fonts',
                ],
            ],
            [
                'value' => 'bi bi-forward-fill',
                'label' => 'forward-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-forward-fill',
                ],
            ],
            [
                'value' => 'bi bi-forward',
                'label' => 'forward',
                'attributes' => [
                    'data-icon' => 'bi bi-forward',
                ],
            ],
            [
                'value' => 'bi bi-front',
                'label' => 'front',
                'attributes' => [
                    'data-icon' => 'bi bi-front',
                ],
            ],
            [
                'value' => 'bi bi-fullscreen-exit',
                'label' => 'fullscreen-exit',
                'attributes' => [
                    'data-icon' => 'bi bi-fullscreen-exit',
                ],
            ],
            [
                'value' => 'bi bi-fullscreen',
                'label' => 'fullscreen',
                'attributes' => [
                    'data-icon' => 'bi bi-fullscreen',
                ],
            ],
            [
                'value' => 'bi bi-funnel-fill',
                'label' => 'funnel-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-funnel-fill',
                ],
            ],
            [
                'value' => 'bi bi-funnel',
                'label' => 'funnel',
                'attributes' => [
                    'data-icon' => 'bi bi-funnel',
                ],
            ],
            [
                'value' => 'bi bi-gear-fill',
                'label' => 'gear-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-gear-fill',
                ],
            ],
            [
                'value' => 'bi bi-gear-wide-connected',
                'label' => 'gear-wide-connected',
                'attributes' => [
                    'data-icon' => 'bi bi-gear-wide-connected',
                ],
            ],
            [
                'value' => 'bi bi-gear-wide',
                'label' => 'gear-wide',
                'attributes' => [
                    'data-icon' => 'bi bi-gear-wide',
                ],
            ],
            [
                'value' => 'bi bi-gear',
                'label' => 'gear',
                'attributes' => [
                    'data-icon' => 'bi bi-gear',
                ],
            ],
            [
                'value' => 'bi bi-gem',
                'label' => 'gem',
                'attributes' => [
                    'data-icon' => 'bi bi-gem',
                ],
            ],
            [
                'value' => 'bi bi-geo-alt-fill',
                'label' => 'geo-alt-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-geo-alt-fill',
                ],
            ],
            [
                'value' => 'bi bi-geo-alt',
                'label' => 'geo-alt',
                'attributes' => [
                    'data-icon' => 'bi bi-geo-alt',
                ],
            ],
            [
                'value' => 'bi bi-geo-fill',
                'label' => 'geo-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-geo-fill',
                ],
            ],
            [
                'value' => 'bi bi-geo',
                'label' => 'geo',
                'attributes' => [
                    'data-icon' => 'bi bi-geo',
                ],
            ],
            [
                'value' => 'bi bi-gift-fill',
                'label' => 'gift-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-gift-fill',
                ],
            ],
            [
                'value' => 'bi bi-gift',
                'label' => 'gift',
                'attributes' => [
                    'data-icon' => 'bi bi-gift',
                ],
            ],
            [
                'value' => 'bi bi-github',
                'label' => 'github',
                'attributes' => [
                    'data-icon' => 'bi bi-github',
                ],
            ],
            [
                'value' => 'bi bi-globe',
                'label' => 'globe',
                'attributes' => [
                    'data-icon' => 'bi bi-globe',
                ],
            ],
            [
                'value' => 'bi bi-globe2',
                'label' => 'globe2',
                'attributes' => [
                    'data-icon' => 'bi bi-globe2',
                ],
            ],
            [
                'value' => 'bi bi-google',
                'label' => 'google',
                'attributes' => [
                    'data-icon' => 'bi bi-google',
                ],
            ],
            [
                'value' => 'bi bi-graph-down',
                'label' => 'graph-down',
                'attributes' => [
                    'data-icon' => 'bi bi-graph-down',
                ],
            ],
            [
                'value' => 'bi bi-graph-up',
                'label' => 'graph-up',
                'attributes' => [
                    'data-icon' => 'bi bi-graph-up',
                ],
            ],
            [
                'value' => 'bi bi-grid-1x2-fill',
                'label' => 'grid-1x2-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-grid-1x2-fill',
                ],
            ],
            [
                'value' => 'bi bi-grid-1x2',
                'label' => 'grid-1x2',
                'attributes' => [
                    'data-icon' => 'bi bi-grid-1x2',
                ],
            ],
            [
                'value' => 'bi bi-grid-3x2-gap-fill',
                'label' => 'grid-3x2-gap-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-grid-3x2-gap-fill',
                ],
            ],
            [
                'value' => 'bi bi-grid-3x2-gap',
                'label' => 'grid-3x2-gap',
                'attributes' => [
                    'data-icon' => 'bi bi-grid-3x2-gap',
                ],
            ],
            [
                'value' => 'bi bi-grid-3x2',
                'label' => 'grid-3x2',
                'attributes' => [
                    'data-icon' => 'bi bi-grid-3x2',
                ],
            ],
            [
                'value' => 'bi bi-grid-3x3-gap-fill',
                'label' => 'grid-3x3-gap-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-grid-3x3-gap-fill',
                ],
            ],
            [
                'value' => 'bi bi-grid-3x3-gap',
                'label' => 'grid-3x3-gap',
                'attributes' => [
                    'data-icon' => 'bi bi-grid-3x3-gap',
                ],
            ],
            [
                'value' => 'bi bi-grid-3x3',
                'label' => 'grid-3x3',
                'attributes' => [
                    'data-icon' => 'bi bi-grid-3x3',
                ],
            ],
            [
                'value' => 'bi bi-grid-fill',
                'label' => 'grid-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-grid-fill',
                ],
            ],
            [
                'value' => 'bi bi-grid',
                'label' => 'grid',
                'attributes' => [
                    'data-icon' => 'bi bi-grid',
                ],
            ],
            [
                'value' => 'bi bi-grip-horizontal',
                'label' => 'grip-horizontal',
                'attributes' => [
                    'data-icon' => 'bi bi-grip-horizontal',
                ],
            ],
            [
                'value' => 'bi bi-grip-vertical',
                'label' => 'grip-vertical',
                'attributes' => [
                    'data-icon' => 'bi bi-grip-vertical',
                ],
            ],
            [
                'value' => 'bi bi-hammer',
                'label' => 'hammer',
                'attributes' => [
                    'data-icon' => 'bi bi-hammer',
                ],
            ],
            [
                'value' => 'bi bi-hand-index-fill',
                'label' => 'hand-index-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-hand-index-fill',
                ],
            ],
            [
                'value' => 'bi bi-hand-index-thumb-fill',
                'label' => 'hand-index-thumb-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-hand-index-thumb-fill',
                ],
            ],
            [
                'value' => 'bi bi-hand-index-thumb',
                'label' => 'hand-index-thumb',
                'attributes' => [
                    'data-icon' => 'bi bi-hand-index-thumb',
                ],
            ],
            [
                'value' => 'bi bi-hand-index',
                'label' => 'hand-index',
                'attributes' => [
                    'data-icon' => 'bi bi-hand-index',
                ],
            ],
            [
                'value' => 'bi bi-hand-thumbs-down-fill',
                'label' => 'hand-thumbs-down-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-hand-thumbs-down-fill',
                ],
            ],
            [
                'value' => 'bi bi-hand-thumbs-down',
                'label' => 'hand-thumbs-down',
                'attributes' => [
                    'data-icon' => 'bi bi-hand-thumbs-down',
                ],
            ],
            [
                'value' => 'bi bi-hand-thumbs-up-fill',
                'label' => 'hand-thumbs-up-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-hand-thumbs-up-fill',
                ],
            ],
            [
                'value' => 'bi bi-hand-thumbs-up',
                'label' => 'hand-thumbs-up',
                'attributes' => [
                    'data-icon' => 'bi bi-hand-thumbs-up',
                ],
            ],
            [
                'value' => 'bi bi-handbag-fill',
                'label' => 'handbag-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-handbag-fill',
                ],
            ],
            [
                'value' => 'bi bi-handbag',
                'label' => 'handbag',
                'attributes' => [
                    'data-icon' => 'bi bi-handbag',
                ],
            ],
            [
                'value' => 'bi bi-hash',
                'label' => 'hash',
                'attributes' => [
                    'data-icon' => 'bi bi-hash',
                ],
            ],
            [
                'value' => 'bi bi-hdd-fill',
                'label' => 'hdd-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-hdd-fill',
                ],
            ],
            [
                'value' => 'bi bi-hdd-network-fill',
                'label' => 'hdd-network-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-hdd-network-fill',
                ],
            ],
            [
                'value' => 'bi bi-hdd-network',
                'label' => 'hdd-network',
                'attributes' => [
                    'data-icon' => 'bi bi-hdd-network',
                ],
            ],
            [
                'value' => 'bi bi-hdd-rack-fill',
                'label' => 'hdd-rack-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-hdd-rack-fill',
                ],
            ],
            [
                'value' => 'bi bi-hdd-rack',
                'label' => 'hdd-rack',
                'attributes' => [
                    'data-icon' => 'bi bi-hdd-rack',
                ],
            ],
            [
                'value' => 'bi bi-hdd-stack-fill',
                'label' => 'hdd-stack-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-hdd-stack-fill',
                ],
            ],
            [
                'value' => 'bi bi-hdd-stack',
                'label' => 'hdd-stack',
                'attributes' => [
                    'data-icon' => 'bi bi-hdd-stack',
                ],
            ],
            [
                'value' => 'bi bi-hdd',
                'label' => 'hdd',
                'attributes' => [
                    'data-icon' => 'bi bi-hdd',
                ],
            ],
            [
                'value' => 'bi bi-headphones',
                'label' => 'headphones',
                'attributes' => [
                    'data-icon' => 'bi bi-headphones',
                ],
            ],
            [
                'value' => 'bi bi-headset',
                'label' => 'headset',
                'attributes' => [
                    'data-icon' => 'bi bi-headset',
                ],
            ],
            [
                'value' => 'bi bi-heart-fill',
                'label' => 'heart-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-heart-fill',
                ],
            ],
            [
                'value' => 'bi bi-heart-half',
                'label' => 'heart-half',
                'attributes' => [
                    'data-icon' => 'bi bi-heart-half',
                ],
            ],
            [
                'value' => 'bi bi-heart',
                'label' => 'heart',
                'attributes' => [
                    'data-icon' => 'bi bi-heart',
                ],
            ],
            [
                'value' => 'bi bi-heptagon-fill',
                'label' => 'heptagon-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-heptagon-fill',
                ],
            ],
            [
                'value' => 'bi bi-heptagon-half',
                'label' => 'heptagon-half',
                'attributes' => [
                    'data-icon' => 'bi bi-heptagon-half',
                ],
            ],
            [
                'value' => 'bi bi-heptagon',
                'label' => 'heptagon',
                'attributes' => [
                    'data-icon' => 'bi bi-heptagon',
                ],
            ],
            [
                'value' => 'bi bi-hexagon-fill',
                'label' => 'hexagon-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-hexagon-fill',
                ],
            ],
            [
                'value' => 'bi bi-hexagon-half',
                'label' => 'hexagon-half',
                'attributes' => [
                    'data-icon' => 'bi bi-hexagon-half',
                ],
            ],
            [
                'value' => 'bi bi-hexagon',
                'label' => 'hexagon',
                'attributes' => [
                    'data-icon' => 'bi bi-hexagon',
                ],
            ],
            [
                'value' => 'bi bi-hourglass-bottom',
                'label' => 'hourglass-bottom',
                'attributes' => [
                    'data-icon' => 'bi bi-hourglass-bottom',
                ],
            ],
            [
                'value' => 'bi bi-hourglass-split',
                'label' => 'hourglass-split',
                'attributes' => [
                    'data-icon' => 'bi bi-hourglass-split',
                ],
            ],
            [
                'value' => 'bi bi-hourglass-top',
                'label' => 'hourglass-top',
                'attributes' => [
                    'data-icon' => 'bi bi-hourglass-top',
                ],
            ],
            [
                'value' => 'bi bi-hourglass',
                'label' => 'hourglass',
                'attributes' => [
                    'data-icon' => 'bi bi-hourglass',
                ],
            ],
            [
                'value' => 'bi bi-house-door-fill',
                'label' => 'house-door-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-house-door-fill',
                ],
            ],
            [
                'value' => 'bi bi-house-door',
                'label' => 'house-door',
                'attributes' => [
                    'data-icon' => 'bi bi-house-door',
                ],
            ],
            [
                'value' => 'bi bi-house-fill',
                'label' => 'house-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-house-fill',
                ],
            ],
            [
                'value' => 'bi bi-house',
                'label' => 'house',
                'attributes' => [
                    'data-icon' => 'bi bi-house',
                ],
            ],
            [
                'value' => 'bi bi-hr',
                'label' => 'hr',
                'attributes' => [
                    'data-icon' => 'bi bi-hr',
                ],
            ],
            [
                'value' => 'bi bi-hurricane',
                'label' => 'hurricane',
                'attributes' => [
                    'data-icon' => 'bi bi-hurricane',
                ],
            ],
            [
                'value' => 'bi bi-image-alt',
                'label' => 'image-alt',
                'attributes' => [
                    'data-icon' => 'bi bi-image-alt',
                ],
            ],
            [
                'value' => 'bi bi-image-fill',
                'label' => 'image-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-image-fill',
                ],
            ],
            [
                'value' => 'bi bi-image',
                'label' => 'image',
                'attributes' => [
                    'data-icon' => 'bi bi-image',
                ],
            ],
            [
                'value' => 'bi bi-images',
                'label' => 'images',
                'attributes' => [
                    'data-icon' => 'bi bi-images',
                ],
            ],
            [
                'value' => 'bi bi-inbox-fill',
                'label' => 'inbox-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-inbox-fill',
                ],
            ],
            [
                'value' => 'bi bi-inbox',
                'label' => 'inbox',
                'attributes' => [
                    'data-icon' => 'bi bi-inbox',
                ],
            ],
            [
                'value' => 'bi bi-inboxes-fill',
                'label' => 'inboxes-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-inboxes-fill',
                ],
            ],
            [
                'value' => 'bi bi-inboxes',
                'label' => 'inboxes',
                'attributes' => [
                    'data-icon' => 'bi bi-inboxes',
                ],
            ],
            [
                'value' => 'bi bi-info-circle-fill',
                'label' => 'info-circle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-info-circle-fill',
                ],
            ],
            [
                'value' => 'bi bi-info-circle',
                'label' => 'info-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-info-circle',
                ],
            ],
            [
                'value' => 'bi bi-info-square-fill',
                'label' => 'info-square-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-info-square-fill',
                ],
            ],
            [
                'value' => 'bi bi-info-square',
                'label' => 'info-square',
                'attributes' => [
                    'data-icon' => 'bi bi-info-square',
                ],
            ],
            [
                'value' => 'bi bi-info',
                'label' => 'info',
                'attributes' => [
                    'data-icon' => 'bi bi-info',
                ],
            ],
            [
                'value' => 'bi bi-input-cursor-text',
                'label' => 'input-cursor-text',
                'attributes' => [
                    'data-icon' => 'bi bi-input-cursor-text',
                ],
            ],
            [
                'value' => 'bi bi-input-cursor',
                'label' => 'input-cursor',
                'attributes' => [
                    'data-icon' => 'bi bi-input-cursor',
                ],
            ],
            [
                'value' => 'bi bi-instagram',
                'label' => 'instagram',
                'attributes' => [
                    'data-icon' => 'bi bi-instagram',
                ],
            ],
            [
                'value' => 'bi bi-intersect',
                'label' => 'intersect',
                'attributes' => [
                    'data-icon' => 'bi bi-intersect',
                ],
            ],
            [
                'value' => 'bi bi-journal-album',
                'label' => 'journal-album',
                'attributes' => [
                    'data-icon' => 'bi bi-journal-album',
                ],
            ],
            [
                'value' => 'bi bi-journal-arrow-down',
                'label' => 'journal-arrow-down',
                'attributes' => [
                    'data-icon' => 'bi bi-journal-arrow-down',
                ],
            ],
            [
                'value' => 'bi bi-journal-arrow-up',
                'label' => 'journal-arrow-up',
                'attributes' => [
                    'data-icon' => 'bi bi-journal-arrow-up',
                ],
            ],
            [
                'value' => 'bi bi-journal-bookmark-fill',
                'label' => 'journal-bookmark-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-journal-bookmark-fill',
                ],
            ],
            [
                'value' => 'bi bi-journal-bookmark',
                'label' => 'journal-bookmark',
                'attributes' => [
                    'data-icon' => 'bi bi-journal-bookmark',
                ],
            ],
            [
                'value' => 'bi bi-journal-check',
                'label' => 'journal-check',
                'attributes' => [
                    'data-icon' => 'bi bi-journal-check',
                ],
            ],
            [
                'value' => 'bi bi-journal-code',
                'label' => 'journal-code',
                'attributes' => [
                    'data-icon' => 'bi bi-journal-code',
                ],
            ],
            [
                'value' => 'bi bi-journal-medical',
                'label' => 'journal-medical',
                'attributes' => [
                    'data-icon' => 'bi bi-journal-medical',
                ],
            ],
            [
                'value' => 'bi bi-journal-minus',
                'label' => 'journal-minus',
                'attributes' => [
                    'data-icon' => 'bi bi-journal-minus',
                ],
            ],
            [
                'value' => 'bi bi-journal-plus',
                'label' => 'journal-plus',
                'attributes' => [
                    'data-icon' => 'bi bi-journal-plus',
                ],
            ],
            [
                'value' => 'bi bi-journal-richtext',
                'label' => 'journal-richtext',
                'attributes' => [
                    'data-icon' => 'bi bi-journal-richtext',
                ],
            ],
            [
                'value' => 'bi bi-journal-text',
                'label' => 'journal-text',
                'attributes' => [
                    'data-icon' => 'bi bi-journal-text',
                ],
            ],
            [
                'value' => 'bi bi-journal-x',
                'label' => 'journal-x',
                'attributes' => [
                    'data-icon' => 'bi bi-journal-x',
                ],
            ],
            [
                'value' => 'bi bi-journal',
                'label' => 'journal',
                'attributes' => [
                    'data-icon' => 'bi bi-journal',
                ],
            ],
            [
                'value' => 'bi bi-journals',
                'label' => 'journals',
                'attributes' => [
                    'data-icon' => 'bi bi-journals',
                ],
            ],
            [
                'value' => 'bi bi-joystick',
                'label' => 'joystick',
                'attributes' => [
                    'data-icon' => 'bi bi-joystick',
                ],
            ],
            [
                'value' => 'bi bi-justify-left',
                'label' => 'justify-left',
                'attributes' => [
                    'data-icon' => 'bi bi-justify-left',
                ],
            ],
            [
                'value' => 'bi bi-justify-right',
                'label' => 'justify-right',
                'attributes' => [
                    'data-icon' => 'bi bi-justify-right',
                ],
            ],
            [
                'value' => 'bi bi-justify',
                'label' => 'justify',
                'attributes' => [
                    'data-icon' => 'bi bi-justify',
                ],
            ],
            [
                'value' => 'bi bi-kanban-fill',
                'label' => 'kanban-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-kanban-fill',
                ],
            ],
            [
                'value' => 'bi bi-kanban',
                'label' => 'kanban',
                'attributes' => [
                    'data-icon' => 'bi bi-kanban',
                ],
            ],
            [
                'value' => 'bi bi-key-fill',
                'label' => 'key-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-key-fill',
                ],
            ],
            [
                'value' => 'bi bi-key',
                'label' => 'key',
                'attributes' => [
                    'data-icon' => 'bi bi-key',
                ],
            ],
            [
                'value' => 'bi bi-keyboard-fill',
                'label' => 'keyboard-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-keyboard-fill',
                ],
            ],
            [
                'value' => 'bi bi-keyboard',
                'label' => 'keyboard',
                'attributes' => [
                    'data-icon' => 'bi bi-keyboard',
                ],
            ],
            [
                'value' => 'bi bi-ladder',
                'label' => 'ladder',
                'attributes' => [
                    'data-icon' => 'bi bi-ladder',
                ],
            ],
            [
                'value' => 'bi bi-lamp-fill',
                'label' => 'lamp-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-lamp-fill',
                ],
            ],
            [
                'value' => 'bi bi-lamp',
                'label' => 'lamp',
                'attributes' => [
                    'data-icon' => 'bi bi-lamp',
                ],
            ],
            [
                'value' => 'bi bi-laptop-fill',
                'label' => 'laptop-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-laptop-fill',
                ],
            ],
            [
                'value' => 'bi bi-laptop',
                'label' => 'laptop',
                'attributes' => [
                    'data-icon' => 'bi bi-laptop',
                ],
            ],
            [
                'value' => 'bi bi-layer-backward',
                'label' => 'layer-backward',
                'attributes' => [
                    'data-icon' => 'bi bi-layer-backward',
                ],
            ],
            [
                'value' => 'bi bi-layer-forward',
                'label' => 'layer-forward',
                'attributes' => [
                    'data-icon' => 'bi bi-layer-forward',
                ],
            ],
            [
                'value' => 'bi bi-layers-fill',
                'label' => 'layers-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-layers-fill',
                ],
            ],
            [
                'value' => 'bi bi-layers-half',
                'label' => 'layers-half',
                'attributes' => [
                    'data-icon' => 'bi bi-layers-half',
                ],
            ],
            [
                'value' => 'bi bi-layers',
                'label' => 'layers',
                'attributes' => [
                    'data-icon' => 'bi bi-layers',
                ],
            ],
            [
                'value' => 'bi bi-layout-sidebar-inset-reverse',
                'label' => 'layout-sidebar-inset-reverse',
                'attributes' => [
                    'data-icon' => 'bi bi-layout-sidebar-inset-reverse',
                ],
            ],
            [
                'value' => 'bi bi-layout-sidebar-inset',
                'label' => 'layout-sidebar-inset',
                'attributes' => [
                    'data-icon' => 'bi bi-layout-sidebar-inset',
                ],
            ],
            [
                'value' => 'bi bi-layout-sidebar-reverse',
                'label' => 'layout-sidebar-reverse',
                'attributes' => [
                    'data-icon' => 'bi bi-layout-sidebar-reverse',
                ],
            ],
            [
                'value' => 'bi bi-layout-sidebar',
                'label' => 'layout-sidebar',
                'attributes' => [
                    'data-icon' => 'bi bi-layout-sidebar',
                ],
            ],
            [
                'value' => 'bi bi-layout-split',
                'label' => 'layout-split',
                'attributes' => [
                    'data-icon' => 'bi bi-layout-split',
                ],
            ],
            [
                'value' => 'bi bi-layout-text-sidebar-reverse',
                'label' => 'layout-text-sidebar-reverse',
                'attributes' => [
                    'data-icon' => 'bi bi-layout-text-sidebar-reverse',
                ],
            ],
            [
                'value' => 'bi bi-layout-text-sidebar',
                'label' => 'layout-text-sidebar',
                'attributes' => [
                    'data-icon' => 'bi bi-layout-text-sidebar',
                ],
            ],
            [
                'value' => 'bi bi-layout-text-window-reverse',
                'label' => 'layout-text-window-reverse',
                'attributes' => [
                    'data-icon' => 'bi bi-layout-text-window-reverse',
                ],
            ],
            [
                'value' => 'bi bi-layout-text-window',
                'label' => 'layout-text-window',
                'attributes' => [
                    'data-icon' => 'bi bi-layout-text-window',
                ],
            ],
            [
                'value' => 'bi bi-layout-three-columns',
                'label' => 'layout-three-columns',
                'attributes' => [
                    'data-icon' => 'bi bi-layout-three-columns',
                ],
            ],
            [
                'value' => 'bi bi-layout-wtf',
                'label' => 'layout-wtf',
                'attributes' => [
                    'data-icon' => 'bi bi-layout-wtf',
                ],
            ],
            [
                'value' => 'bi bi-life-preserver',
                'label' => 'life-preserver',
                'attributes' => [
                    'data-icon' => 'bi bi-life-preserver',
                ],
            ],
            [
                'value' => 'bi bi-lightbulb-fill',
                'label' => 'lightbulb-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-lightbulb-fill',
                ],
            ],
            [
                'value' => 'bi bi-lightbulb-off-fill',
                'label' => 'lightbulb-off-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-lightbulb-off-fill',
                ],
            ],
            [
                'value' => 'bi bi-lightbulb-off',
                'label' => 'lightbulb-off',
                'attributes' => [
                    'data-icon' => 'bi bi-lightbulb-off',
                ],
            ],
            [
                'value' => 'bi bi-lightbulb',
                'label' => 'lightbulb',
                'attributes' => [
                    'data-icon' => 'bi bi-lightbulb',
                ],
            ],
            [
                'value' => 'bi bi-lightning-charge-fill',
                'label' => 'lightning-charge-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-lightning-charge-fill',
                ],
            ],
            [
                'value' => 'bi bi-lightning-charge',
                'label' => 'lightning-charge',
                'attributes' => [
                    'data-icon' => 'bi bi-lightning-charge',
                ],
            ],
            [
                'value' => 'bi bi-lightning-fill',
                'label' => 'lightning-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-lightning-fill',
                ],
            ],
            [
                'value' => 'bi bi-lightning',
                'label' => 'lightning',
                'attributes' => [
                    'data-icon' => 'bi bi-lightning',
                ],
            ],
            [
                'value' => 'bi bi-link-45deg',
                'label' => 'link-45deg',
                'attributes' => [
                    'data-icon' => 'bi bi-link-45deg',
                ],
            ],
            [
                'value' => 'bi bi-link',
                'label' => 'link',
                'attributes' => [
                    'data-icon' => 'bi bi-link',
                ],
            ],
            [
                'value' => 'bi bi-linkedin',
                'label' => 'linkedin',
                'attributes' => [
                    'data-icon' => 'bi bi-linkedin',
                ],
            ],
            [
                'value' => 'bi bi-list-check',
                'label' => 'list-check',
                'attributes' => [
                    'data-icon' => 'bi bi-list-check',
                ],
            ],
            [
                'value' => 'bi bi-list-nested',
                'label' => 'list-nested',
                'attributes' => [
                    'data-icon' => 'bi bi-list-nested',
                ],
            ],
            [
                'value' => 'bi bi-list-ol',
                'label' => 'list-ol',
                'attributes' => [
                    'data-icon' => 'bi bi-list-ol',
                ],
            ],
            [
                'value' => 'bi bi-list-stars',
                'label' => 'list-stars',
                'attributes' => [
                    'data-icon' => 'bi bi-list-stars',
                ],
            ],
            [
                'value' => 'bi bi-list-task',
                'label' => 'list-task',
                'attributes' => [
                    'data-icon' => 'bi bi-list-task',
                ],
            ],
            [
                'value' => 'bi bi-list-ul',
                'label' => 'list-ul',
                'attributes' => [
                    'data-icon' => 'bi bi-list-ul',
                ],
            ],
            [
                'value' => 'bi bi-list',
                'label' => 'list',
                'attributes' => [
                    'data-icon' => 'bi bi-list',
                ],
            ],
            [
                'value' => 'bi bi-lock-fill',
                'label' => 'lock-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-lock-fill',
                ],
            ],
            [
                'value' => 'bi bi-lock',
                'label' => 'lock',
                'attributes' => [
                    'data-icon' => 'bi bi-lock',
                ],
            ],
            [
                'value' => 'bi bi-mailbox',
                'label' => 'mailbox',
                'attributes' => [
                    'data-icon' => 'bi bi-mailbox',
                ],
            ],
            [
                'value' => 'bi bi-mailbox2',
                'label' => 'mailbox2',
                'attributes' => [
                    'data-icon' => 'bi bi-mailbox2',
                ],
            ],
            [
                'value' => 'bi bi-map-fill',
                'label' => 'map-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-map-fill',
                ],
            ],
            [
                'value' => 'bi bi-map',
                'label' => 'map',
                'attributes' => [
                    'data-icon' => 'bi bi-map',
                ],
            ],
            [
                'value' => 'bi bi-markdown-fill',
                'label' => 'markdown-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-markdown-fill',
                ],
            ],
            [
                'value' => 'bi bi-markdown',
                'label' => 'markdown',
                'attributes' => [
                    'data-icon' => 'bi bi-markdown',
                ],
            ],
            [
                'value' => 'bi bi-mask',
                'label' => 'mask',
                'attributes' => [
                    'data-icon' => 'bi bi-mask',
                ],
            ],
            [
                'value' => 'bi bi-megaphone-fill',
                'label' => 'megaphone-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-megaphone-fill',
                ],
            ],
            [
                'value' => 'bi bi-megaphone',
                'label' => 'megaphone',
                'attributes' => [
                    'data-icon' => 'bi bi-megaphone',
                ],
            ],
            [
                'value' => 'bi bi-menu-app-fill',
                'label' => 'menu-app-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-menu-app-fill',
                ],
            ],
            [
                'value' => 'bi bi-menu-app',
                'label' => 'menu-app',
                'attributes' => [
                    'data-icon' => 'bi bi-menu-app',
                ],
            ],
            [
                'value' => 'bi bi-menu-button-fill',
                'label' => 'menu-button-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-menu-button-fill',
                ],
            ],
            [
                'value' => 'bi bi-menu-button-wide-fill',
                'label' => 'menu-button-wide-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-menu-button-wide-fill',
                ],
            ],
            [
                'value' => 'bi bi-menu-button-wide',
                'label' => 'menu-button-wide',
                'attributes' => [
                    'data-icon' => 'bi bi-menu-button-wide',
                ],
            ],
            [
                'value' => 'bi bi-menu-button',
                'label' => 'menu-button',
                'attributes' => [
                    'data-icon' => 'bi bi-menu-button',
                ],
            ],
            [
                'value' => 'bi bi-menu-down',
                'label' => 'menu-down',
                'attributes' => [
                    'data-icon' => 'bi bi-menu-down',
                ],
            ],
            [
                'value' => 'bi bi-menu-up',
                'label' => 'menu-up',
                'attributes' => [
                    'data-icon' => 'bi bi-menu-up',
                ],
            ],
            [
                'value' => 'bi bi-mic-fill',
                'label' => 'mic-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-mic-fill',
                ],
            ],
            [
                'value' => 'bi bi-mic-mute-fill',
                'label' => 'mic-mute-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-mic-mute-fill',
                ],
            ],
            [
                'value' => 'bi bi-mic-mute',
                'label' => 'mic-mute',
                'attributes' => [
                    'data-icon' => 'bi bi-mic-mute',
                ],
            ],
            [
                'value' => 'bi bi-mic',
                'label' => 'mic',
                'attributes' => [
                    'data-icon' => 'bi bi-mic',
                ],
            ],
            [
                'value' => 'bi bi-minecart-loaded',
                'label' => 'minecart-loaded',
                'attributes' => [
                    'data-icon' => 'bi bi-minecart-loaded',
                ],
            ],
            [
                'value' => 'bi bi-minecart',
                'label' => 'minecart',
                'attributes' => [
                    'data-icon' => 'bi bi-minecart',
                ],
            ],
            [
                'value' => 'bi bi-moisture',
                'label' => 'moisture',
                'attributes' => [
                    'data-icon' => 'bi bi-moisture',
                ],
            ],
            [
                'value' => 'bi bi-moon-fill',
                'label' => 'moon-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-moon-fill',
                ],
            ],
            [
                'value' => 'bi bi-moon-stars-fill',
                'label' => 'moon-stars-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-moon-stars-fill',
                ],
            ],
            [
                'value' => 'bi bi-moon-stars',
                'label' => 'moon-stars',
                'attributes' => [
                    'data-icon' => 'bi bi-moon-stars',
                ],
            ],
            [
                'value' => 'bi bi-moon',
                'label' => 'moon',
                'attributes' => [
                    'data-icon' => 'bi bi-moon',
                ],
            ],
            [
                'value' => 'bi bi-mouse-fill',
                'label' => 'mouse-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-mouse-fill',
                ],
            ],
            [
                'value' => 'bi bi-mouse',
                'label' => 'mouse',
                'attributes' => [
                    'data-icon' => 'bi bi-mouse',
                ],
            ],
            [
                'value' => 'bi bi-mouse2-fill',
                'label' => 'mouse2-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-mouse2-fill',
                ],
            ],
            [
                'value' => 'bi bi-mouse2',
                'label' => 'mouse2',
                'attributes' => [
                    'data-icon' => 'bi bi-mouse2',
                ],
            ],
            [
                'value' => 'bi bi-mouse3-fill',
                'label' => 'mouse3-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-mouse3-fill',
                ],
            ],
            [
                'value' => 'bi bi-mouse3',
                'label' => 'mouse3',
                'attributes' => [
                    'data-icon' => 'bi bi-mouse3',
                ],
            ],
            [
                'value' => 'bi bi-music-note-beamed',
                'label' => 'music-note-beamed',
                'attributes' => [
                    'data-icon' => 'bi bi-music-note-beamed',
                ],
            ],
            [
                'value' => 'bi bi-music-note-list',
                'label' => 'music-note-list',
                'attributes' => [
                    'data-icon' => 'bi bi-music-note-list',
                ],
            ],
            [
                'value' => 'bi bi-music-note',
                'label' => 'music-note',
                'attributes' => [
                    'data-icon' => 'bi bi-music-note',
                ],
            ],
            [
                'value' => 'bi bi-music-player-fill',
                'label' => 'music-player-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-music-player-fill',
                ],
            ],
            [
                'value' => 'bi bi-music-player',
                'label' => 'music-player',
                'attributes' => [
                    'data-icon' => 'bi bi-music-player',
                ],
            ],
            [
                'value' => 'bi bi-newspaper',
                'label' => 'newspaper',
                'attributes' => [
                    'data-icon' => 'bi bi-newspaper',
                ],
            ],
            [
                'value' => 'bi bi-node-minus-fill',
                'label' => 'node-minus-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-node-minus-fill',
                ],
            ],
            [
                'value' => 'bi bi-node-minus',
                'label' => 'node-minus',
                'attributes' => [
                    'data-icon' => 'bi bi-node-minus',
                ],
            ],
            [
                'value' => 'bi bi-node-plus-fill',
                'label' => 'node-plus-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-node-plus-fill',
                ],
            ],
            [
                'value' => 'bi bi-node-plus',
                'label' => 'node-plus',
                'attributes' => [
                    'data-icon' => 'bi bi-node-plus',
                ],
            ],
            [
                'value' => 'bi bi-nut-fill',
                'label' => 'nut-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-nut-fill',
                ],
            ],
            [
                'value' => 'bi bi-nut',
                'label' => 'nut',
                'attributes' => [
                    'data-icon' => 'bi bi-nut',
                ],
            ],
            [
                'value' => 'bi bi-octagon-fill',
                'label' => 'octagon-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-octagon-fill',
                ],
            ],
            [
                'value' => 'bi bi-octagon-half',
                'label' => 'octagon-half',
                'attributes' => [
                    'data-icon' => 'bi bi-octagon-half',
                ],
            ],
            [
                'value' => 'bi bi-octagon',
                'label' => 'octagon',
                'attributes' => [
                    'data-icon' => 'bi bi-octagon',
                ],
            ],
            [
                'value' => 'bi bi-option',
                'label' => 'option',
                'attributes' => [
                    'data-icon' => 'bi bi-option',
                ],
            ],
            [
                'value' => 'bi bi-outlet',
                'label' => 'outlet',
                'attributes' => [
                    'data-icon' => 'bi bi-outlet',
                ],
            ],
            [
                'value' => 'bi bi-paint-bucket',
                'label' => 'paint-bucket',
                'attributes' => [
                    'data-icon' => 'bi bi-paint-bucket',
                ],
            ],
            [
                'value' => 'bi bi-palette-fill',
                'label' => 'palette-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-palette-fill',
                ],
            ],
            [
                'value' => 'bi bi-palette',
                'label' => 'palette',
                'attributes' => [
                    'data-icon' => 'bi bi-palette',
                ],
            ],
            [
                'value' => 'bi bi-palette2',
                'label' => 'palette2',
                'attributes' => [
                    'data-icon' => 'bi bi-palette2',
                ],
            ],
            [
                'value' => 'bi bi-paperclip',
                'label' => 'paperclip',
                'attributes' => [
                    'data-icon' => 'bi bi-paperclip',
                ],
            ],
            [
                'value' => 'bi bi-paragraph',
                'label' => 'paragraph',
                'attributes' => [
                    'data-icon' => 'bi bi-paragraph',
                ],
            ],
            [
                'value' => 'bi bi-patch-check-fill',
                'label' => 'patch-check-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-patch-check-fill',
                ],
            ],
            [
                'value' => 'bi bi-patch-check',
                'label' => 'patch-check',
                'attributes' => [
                    'data-icon' => 'bi bi-patch-check',
                ],
            ],
            [
                'value' => 'bi bi-patch-exclamation-fill',
                'label' => 'patch-exclamation-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-patch-exclamation-fill',
                ],
            ],
            [
                'value' => 'bi bi-patch-exclamation',
                'label' => 'patch-exclamation',
                'attributes' => [
                    'data-icon' => 'bi bi-patch-exclamation',
                ],
            ],
            [
                'value' => 'bi bi-patch-minus-fill',
                'label' => 'patch-minus-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-patch-minus-fill',
                ],
            ],
            [
                'value' => 'bi bi-patch-minus',
                'label' => 'patch-minus',
                'attributes' => [
                    'data-icon' => 'bi bi-patch-minus',
                ],
            ],
            [
                'value' => 'bi bi-patch-plus-fill',
                'label' => 'patch-plus-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-patch-plus-fill',
                ],
            ],
            [
                'value' => 'bi bi-patch-plus',
                'label' => 'patch-plus',
                'attributes' => [
                    'data-icon' => 'bi bi-patch-plus',
                ],
            ],
            [
                'value' => 'bi bi-patch-question-fill',
                'label' => 'patch-question-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-patch-question-fill',
                ],
            ],
            [
                'value' => 'bi bi-patch-question',
                'label' => 'patch-question',
                'attributes' => [
                    'data-icon' => 'bi bi-patch-question',
                ],
            ],
            [
                'value' => 'bi bi-pause-btn-fill',
                'label' => 'pause-btn-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-pause-btn-fill',
                ],
            ],
            [
                'value' => 'bi bi-pause-btn',
                'label' => 'pause-btn',
                'attributes' => [
                    'data-icon' => 'bi bi-pause-btn',
                ],
            ],
            [
                'value' => 'bi bi-pause-circle-fill',
                'label' => 'pause-circle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-pause-circle-fill',
                ],
            ],
            [
                'value' => 'bi bi-pause-circle',
                'label' => 'pause-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-pause-circle',
                ],
            ],
            [
                'value' => 'bi bi-pause-fill',
                'label' => 'pause-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-pause-fill',
                ],
            ],
            [
                'value' => 'bi bi-pause',
                'label' => 'pause',
                'attributes' => [
                    'data-icon' => 'bi bi-pause',
                ],
            ],
            [
                'value' => 'bi bi-peace-fill',
                'label' => 'peace-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-peace-fill',
                ],
            ],
            [
                'value' => 'bi bi-peace',
                'label' => 'peace',
                'attributes' => [
                    'data-icon' => 'bi bi-peace',
                ],
            ],
            [
                'value' => 'bi bi-pen-fill',
                'label' => 'pen-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-pen-fill',
                ],
            ],
            [
                'value' => 'bi bi-pen',
                'label' => 'pen',
                'attributes' => [
                    'data-icon' => 'bi bi-pen',
                ],
            ],
            [
                'value' => 'bi bi-pencil-fill',
                'label' => 'pencil-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-pencil-fill',
                ],
            ],
            [
                'value' => 'bi bi-pencil-square',
                'label' => 'pencil-square',
                'attributes' => [
                    'data-icon' => 'bi bi-pencil-square',
                ],
            ],
            [
                'value' => 'bi bi-pencil',
                'label' => 'pencil',
                'attributes' => [
                    'data-icon' => 'bi bi-pencil',
                ],
            ],
            [
                'value' => 'bi bi-pentagon-fill',
                'label' => 'pentagon-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-pentagon-fill',
                ],
            ],
            [
                'value' => 'bi bi-pentagon-half',
                'label' => 'pentagon-half',
                'attributes' => [
                    'data-icon' => 'bi bi-pentagon-half',
                ],
            ],
            [
                'value' => 'bi bi-pentagon',
                'label' => 'pentagon',
                'attributes' => [
                    'data-icon' => 'bi bi-pentagon',
                ],
            ],
            [
                'value' => 'bi bi-people-fill',
                'label' => 'people-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-people-fill',
                ],
            ],
            [
                'value' => 'bi bi-people',
                'label' => 'people',
                'attributes' => [
                    'data-icon' => 'bi bi-people',
                ],
            ],
            [
                'value' => 'bi bi-percent',
                'label' => 'percent',
                'attributes' => [
                    'data-icon' => 'bi bi-percent',
                ],
            ],
            [
                'value' => 'bi bi-person-badge-fill',
                'label' => 'person-badge-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-person-badge-fill',
                ],
            ],
            [
                'value' => 'bi bi-person-badge',
                'label' => 'person-badge',
                'attributes' => [
                    'data-icon' => 'bi bi-person-badge',
                ],
            ],
            [
                'value' => 'bi bi-person-bounding-box',
                'label' => 'person-bounding-box',
                'attributes' => [
                    'data-icon' => 'bi bi-person-bounding-box',
                ],
            ],
            [
                'value' => 'bi bi-person-check-fill',
                'label' => 'person-check-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-person-check-fill',
                ],
            ],
            [
                'value' => 'bi bi-person-check',
                'label' => 'person-check',
                'attributes' => [
                    'data-icon' => 'bi bi-person-check',
                ],
            ],
            [
                'value' => 'bi bi-person-circle',
                'label' => 'person-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-person-circle',
                ],
            ],
            [
                'value' => 'bi bi-person-dash-fill',
                'label' => 'person-dash-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-person-dash-fill',
                ],
            ],
            [
                'value' => 'bi bi-person-dash',
                'label' => 'person-dash',
                'attributes' => [
                    'data-icon' => 'bi bi-person-dash',
                ],
            ],
            [
                'value' => 'bi bi-person-fill',
                'label' => 'person-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-person-fill',
                ],
            ],
            [
                'value' => 'bi bi-person-lines-fill',
                'label' => 'person-lines-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-person-lines-fill',
                ],
            ],
            [
                'value' => 'bi bi-person-plus-fill',
                'label' => 'person-plus-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-person-plus-fill',
                ],
            ],
            [
                'value' => 'bi bi-person-plus',
                'label' => 'person-plus',
                'attributes' => [
                    'data-icon' => 'bi bi-person-plus',
                ],
            ],
            [
                'value' => 'bi bi-person-square',
                'label' => 'person-square',
                'attributes' => [
                    'data-icon' => 'bi bi-person-square',
                ],
            ],
            [
                'value' => 'bi bi-person-x-fill',
                'label' => 'person-x-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-person-x-fill',
                ],
            ],
            [
                'value' => 'bi bi-person-x',
                'label' => 'person-x',
                'attributes' => [
                    'data-icon' => 'bi bi-person-x',
                ],
            ],
            [
                'value' => 'bi bi-person',
                'label' => 'person',
                'attributes' => [
                    'data-icon' => 'bi bi-person',
                ],
            ],
            [
                'value' => 'bi bi-phone-fill',
                'label' => 'phone-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-phone-fill',
                ],
            ],
            [
                'value' => 'bi bi-phone-landscape-fill',
                'label' => 'phone-landscape-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-phone-landscape-fill',
                ],
            ],
            [
                'value' => 'bi bi-phone-landscape',
                'label' => 'phone-landscape',
                'attributes' => [
                    'data-icon' => 'bi bi-phone-landscape',
                ],
            ],
            [
                'value' => 'bi bi-phone-vibrate-fill',
                'label' => 'phone-vibrate-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-phone-vibrate-fill',
                ],
            ],
            [
                'value' => 'bi bi-phone-vibrate',
                'label' => 'phone-vibrate',
                'attributes' => [
                    'data-icon' => 'bi bi-phone-vibrate',
                ],
            ],
            [
                'value' => 'bi bi-phone',
                'label' => 'phone',
                'attributes' => [
                    'data-icon' => 'bi bi-phone',
                ],
            ],
            [
                'value' => 'bi bi-pie-chart-fill',
                'label' => 'pie-chart-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-pie-chart-fill',
                ],
            ],
            [
                'value' => 'bi bi-pie-chart',
                'label' => 'pie-chart',
                'attributes' => [
                    'data-icon' => 'bi bi-pie-chart',
                ],
            ],
            [
                'value' => 'bi bi-pin-angle-fill',
                'label' => 'pin-angle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-pin-angle-fill',
                ],
            ],
            [
                'value' => 'bi bi-pin-angle',
                'label' => 'pin-angle',
                'attributes' => [
                    'data-icon' => 'bi bi-pin-angle',
                ],
            ],
            [
                'value' => 'bi bi-pin-fill',
                'label' => 'pin-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-pin-fill',
                ],
            ],
            [
                'value' => 'bi bi-pin',
                'label' => 'pin',
                'attributes' => [
                    'data-icon' => 'bi bi-pin',
                ],
            ],
            [
                'value' => 'bi bi-pip-fill',
                'label' => 'pip-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-pip-fill',
                ],
            ],
            [
                'value' => 'bi bi-pip',
                'label' => 'pip',
                'attributes' => [
                    'data-icon' => 'bi bi-pip',
                ],
            ],
            [
                'value' => 'bi bi-play-btn-fill',
                'label' => 'play-btn-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-play-btn-fill',
                ],
            ],
            [
                'value' => 'bi bi-play-btn',
                'label' => 'play-btn',
                'attributes' => [
                    'data-icon' => 'bi bi-play-btn',
                ],
            ],
            [
                'value' => 'bi bi-play-circle-fill',
                'label' => 'play-circle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-play-circle-fill',
                ],
            ],
            [
                'value' => 'bi bi-play-circle',
                'label' => 'play-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-play-circle',
                ],
            ],
            [
                'value' => 'bi bi-play-fill',
                'label' => 'play-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-play-fill',
                ],
            ],
            [
                'value' => 'bi bi-play',
                'label' => 'play',
                'attributes' => [
                    'data-icon' => 'bi bi-play',
                ],
            ],
            [
                'value' => 'bi bi-plug-fill',
                'label' => 'plug-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-plug-fill',
                ],
            ],
            [
                'value' => 'bi bi-plug',
                'label' => 'plug',
                'attributes' => [
                    'data-icon' => 'bi bi-plug',
                ],
            ],
            [
                'value' => 'bi bi-plus-circle-dotted',
                'label' => 'plus-circle-dotted',
                'attributes' => [
                    'data-icon' => 'bi bi-plus-circle-dotted',
                ],
            ],
            [
                'value' => 'bi bi-plus-circle-fill',
                'label' => 'plus-circle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-plus-circle-fill',
                ],
            ],
            [
                'value' => 'bi bi-plus-circle',
                'label' => 'plus-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-plus-circle',
                ],
            ],
            [
                'value' => 'bi bi-plus-square-dotted',
                'label' => 'plus-square-dotted',
                'attributes' => [
                    'data-icon' => 'bi bi-plus-square-dotted',
                ],
            ],
            [
                'value' => 'bi bi-plus-square-fill',
                'label' => 'plus-square-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-plus-square-fill',
                ],
            ],
            [
                'value' => 'bi bi-plus-square',
                'label' => 'plus-square',
                'attributes' => [
                    'data-icon' => 'bi bi-plus-square',
                ],
            ],
            [
                'value' => 'bi bi-plus',
                'label' => 'plus',
                'attributes' => [
                    'data-icon' => 'bi bi-plus',
                ],
            ],
            [
                'value' => 'bi bi-power',
                'label' => 'power',
                'attributes' => [
                    'data-icon' => 'bi bi-power',
                ],
            ],
            [
                'value' => 'bi bi-printer-fill',
                'label' => 'printer-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-printer-fill',
                ],
            ],
            [
                'value' => 'bi bi-printer',
                'label' => 'printer',
                'attributes' => [
                    'data-icon' => 'bi bi-printer',
                ],
            ],
            [
                'value' => 'bi bi-puzzle-fill',
                'label' => 'puzzle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-puzzle-fill',
                ],
            ],
            [
                'value' => 'bi bi-puzzle',
                'label' => 'puzzle',
                'attributes' => [
                    'data-icon' => 'bi bi-puzzle',
                ],
            ],
            [
                'value' => 'bi bi-question-circle-fill',
                'label' => 'question-circle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-question-circle-fill',
                ],
            ],
            [
                'value' => 'bi bi-question-circle',
                'label' => 'question-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-question-circle',
                ],
            ],
            [
                'value' => 'bi bi-question-diamond-fill',
                'label' => 'question-diamond-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-question-diamond-fill',
                ],
            ],
            [
                'value' => 'bi bi-question-diamond',
                'label' => 'question-diamond',
                'attributes' => [
                    'data-icon' => 'bi bi-question-diamond',
                ],
            ],
            [
                'value' => 'bi bi-question-octagon-fill',
                'label' => 'question-octagon-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-question-octagon-fill',
                ],
            ],
            [
                'value' => 'bi bi-question-octagon',
                'label' => 'question-octagon',
                'attributes' => [
                    'data-icon' => 'bi bi-question-octagon',
                ],
            ],
            [
                'value' => 'bi bi-question-square-fill',
                'label' => 'question-square-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-question-square-fill',
                ],
            ],
            [
                'value' => 'bi bi-question-square',
                'label' => 'question-square',
                'attributes' => [
                    'data-icon' => 'bi bi-question-square',
                ],
            ],
            [
                'value' => 'bi bi-question',
                'label' => 'question',
                'attributes' => [
                    'data-icon' => 'bi bi-question',
                ],
            ],
            [
                'value' => 'bi bi-rainbow',
                'label' => 'rainbow',
                'attributes' => [
                    'data-icon' => 'bi bi-rainbow',
                ],
            ],
            [
                'value' => 'bi bi-receipt-cutoff',
                'label' => 'receipt-cutoff',
                'attributes' => [
                    'data-icon' => 'bi bi-receipt-cutoff',
                ],
            ],
            [
                'value' => 'bi bi-receipt',
                'label' => 'receipt',
                'attributes' => [
                    'data-icon' => 'bi bi-receipt',
                ],
            ],
            [
                'value' => 'bi bi-reception-0',
                'label' => 'reception-0',
                'attributes' => [
                    'data-icon' => 'bi bi-reception-0',
                ],
            ],
            [
                'value' => 'bi bi-reception-1',
                'label' => 'reception-1',
                'attributes' => [
                    'data-icon' => 'bi bi-reception-1',
                ],
            ],
            [
                'value' => 'bi bi-reception-2',
                'label' => 'reception-2',
                'attributes' => [
                    'data-icon' => 'bi bi-reception-2',
                ],
            ],
            [
                'value' => 'bi bi-reception-3',
                'label' => 'reception-3',
                'attributes' => [
                    'data-icon' => 'bi bi-reception-3',
                ],
            ],
            [
                'value' => 'bi bi-reception-4',
                'label' => 'reception-4',
                'attributes' => [
                    'data-icon' => 'bi bi-reception-4',
                ],
            ],
            [
                'value' => 'bi bi-record-btn-fill',
                'label' => 'record-btn-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-record-btn-fill',
                ],
            ],
            [
                'value' => 'bi bi-record-btn',
                'label' => 'record-btn',
                'attributes' => [
                    'data-icon' => 'bi bi-record-btn',
                ],
            ],
            [
                'value' => 'bi bi-record-circle-fill',
                'label' => 'record-circle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-record-circle-fill',
                ],
            ],
            [
                'value' => 'bi bi-record-circle',
                'label' => 'record-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-record-circle',
                ],
            ],
            [
                'value' => 'bi bi-record-fill',
                'label' => 'record-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-record-fill',
                ],
            ],
            [
                'value' => 'bi bi-record',
                'label' => 'record',
                'attributes' => [
                    'data-icon' => 'bi bi-record',
                ],
            ],
            [
                'value' => 'bi bi-record2-fill',
                'label' => 'record2-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-record2-fill',
                ],
            ],
            [
                'value' => 'bi bi-record2',
                'label' => 'record2',
                'attributes' => [
                    'data-icon' => 'bi bi-record2',
                ],
            ],
            [
                'value' => 'bi bi-reply-all-fill',
                'label' => 'reply-all-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-reply-all-fill',
                ],
            ],
            [
                'value' => 'bi bi-reply-all',
                'label' => 'reply-all',
                'attributes' => [
                    'data-icon' => 'bi bi-reply-all',
                ],
            ],
            [
                'value' => 'bi bi-reply-fill',
                'label' => 'reply-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-reply-fill',
                ],
            ],
            [
                'value' => 'bi bi-reply',
                'label' => 'reply',
                'attributes' => [
                    'data-icon' => 'bi bi-reply',
                ],
            ],
            [
                'value' => 'bi bi-rss-fill',
                'label' => 'rss-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-rss-fill',
                ],
            ],
            [
                'value' => 'bi bi-rss',
                'label' => 'rss',
                'attributes' => [
                    'data-icon' => 'bi bi-rss',
                ],
            ],
            [
                'value' => 'bi bi-rulers',
                'label' => 'rulers',
                'attributes' => [
                    'data-icon' => 'bi bi-rulers',
                ],
            ],
            [
                'value' => 'bi bi-save-fill',
                'label' => 'save-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-save-fill',
                ],
            ],
            [
                'value' => 'bi bi-save',
                'label' => 'save',
                'attributes' => [
                    'data-icon' => 'bi bi-save',
                ],
            ],
            [
                'value' => 'bi bi-save2-fill',
                'label' => 'save2-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-save2-fill',
                ],
            ],
            [
                'value' => 'bi bi-save2',
                'label' => 'save2',
                'attributes' => [
                    'data-icon' => 'bi bi-save2',
                ],
            ],
            [
                'value' => 'bi bi-scissors',
                'label' => 'scissors',
                'attributes' => [
                    'data-icon' => 'bi bi-scissors',
                ],
            ],
            [
                'value' => 'bi bi-screwdriver',
                'label' => 'screwdriver',
                'attributes' => [
                    'data-icon' => 'bi bi-screwdriver',
                ],
            ],
            [
                'value' => 'bi bi-search',
                'label' => 'search',
                'attributes' => [
                    'data-icon' => 'bi bi-search',
                ],
            ],
            [
                'value' => 'bi bi-segmented-nav',
                'label' => 'segmented-nav',
                'attributes' => [
                    'data-icon' => 'bi bi-segmented-nav',
                ],
            ],
            [
                'value' => 'bi bi-server',
                'label' => 'server',
                'attributes' => [
                    'data-icon' => 'bi bi-server',
                ],
            ],
            [
                'value' => 'bi bi-share-fill',
                'label' => 'share-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-share-fill',
                ],
            ],
            [
                'value' => 'bi bi-share',
                'label' => 'share',
                'attributes' => [
                    'data-icon' => 'bi bi-share',
                ],
            ],
            [
                'value' => 'bi bi-shield-check',
                'label' => 'shield-check',
                'attributes' => [
                    'data-icon' => 'bi bi-shield-check',
                ],
            ],
            [
                'value' => 'bi bi-shield-exclamation',
                'label' => 'shield-exclamation',
                'attributes' => [
                    'data-icon' => 'bi bi-shield-exclamation',
                ],
            ],
            [
                'value' => 'bi bi-shield-fill-check',
                'label' => 'shield-fill-check',
                'attributes' => [
                    'data-icon' => 'bi bi-shield-fill-check',
                ],
            ],
            [
                'value' => 'bi bi-shield-fill-exclamation',
                'label' => 'shield-fill-exclamation',
                'attributes' => [
                    'data-icon' => 'bi bi-shield-fill-exclamation',
                ],
            ],
            [
                'value' => 'bi bi-shield-fill-minus',
                'label' => 'shield-fill-minus',
                'attributes' => [
                    'data-icon' => 'bi bi-shield-fill-minus',
                ],
            ],
            [
                'value' => 'bi bi-shield-fill-plus',
                'label' => 'shield-fill-plus',
                'attributes' => [
                    'data-icon' => 'bi bi-shield-fill-plus',
                ],
            ],
            [
                'value' => 'bi bi-shield-fill-x',
                'label' => 'shield-fill-x',
                'attributes' => [
                    'data-icon' => 'bi bi-shield-fill-x',
                ],
            ],
            [
                'value' => 'bi bi-shield-fill',
                'label' => 'shield-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-shield-fill',
                ],
            ],
            [
                'value' => 'bi bi-shield-lock-fill',
                'label' => 'shield-lock-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-shield-lock-fill',
                ],
            ],
            [
                'value' => 'bi bi-shield-lock',
                'label' => 'shield-lock',
                'attributes' => [
                    'data-icon' => 'bi bi-shield-lock',
                ],
            ],
            [
                'value' => 'bi bi-shield-minus',
                'label' => 'shield-minus',
                'attributes' => [
                    'data-icon' => 'bi bi-shield-minus',
                ],
            ],
            [
                'value' => 'bi bi-shield-plus',
                'label' => 'shield-plus',
                'attributes' => [
                    'data-icon' => 'bi bi-shield-plus',
                ],
            ],
            [
                'value' => 'bi bi-shield-shaded',
                'label' => 'shield-shaded',
                'attributes' => [
                    'data-icon' => 'bi bi-shield-shaded',
                ],
            ],
            [
                'value' => 'bi bi-shield-slash-fill',
                'label' => 'shield-slash-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-shield-slash-fill',
                ],
            ],
            [
                'value' => 'bi bi-shield-slash',
                'label' => 'shield-slash',
                'attributes' => [
                    'data-icon' => 'bi bi-shield-slash',
                ],
            ],
            [
                'value' => 'bi bi-shield-x',
                'label' => 'shield-x',
                'attributes' => [
                    'data-icon' => 'bi bi-shield-x',
                ],
            ],
            [
                'value' => 'bi bi-shield',
                'label' => 'shield',
                'attributes' => [
                    'data-icon' => 'bi bi-shield',
                ],
            ],
            [
                'value' => 'bi bi-shift-fill',
                'label' => 'shift-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-shift-fill',
                ],
            ],
            [
                'value' => 'bi bi-shift',
                'label' => 'shift',
                'attributes' => [
                    'data-icon' => 'bi bi-shift',
                ],
            ],
            [
                'value' => 'bi bi-shop-window',
                'label' => 'shop-window',
                'attributes' => [
                    'data-icon' => 'bi bi-shop-window',
                ],
            ],
            [
                'value' => 'bi bi-shop',
                'label' => 'shop',
                'attributes' => [
                    'data-icon' => 'bi bi-shop',
                ],
            ],
            [
                'value' => 'bi bi-shuffle',
                'label' => 'shuffle',
                'attributes' => [
                    'data-icon' => 'bi bi-shuffle',
                ],
            ],
            [
                'value' => 'bi bi-signpost-2-fill',
                'label' => 'signpost-2-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-signpost-2-fill',
                ],
            ],
            [
                'value' => 'bi bi-signpost-2',
                'label' => 'signpost-2',
                'attributes' => [
                    'data-icon' => 'bi bi-signpost-2',
                ],
            ],
            [
                'value' => 'bi bi-signpost-fill',
                'label' => 'signpost-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-signpost-fill',
                ],
            ],
            [
                'value' => 'bi bi-signpost-split-fill',
                'label' => 'signpost-split-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-signpost-split-fill',
                ],
            ],
            [
                'value' => 'bi bi-signpost-split',
                'label' => 'signpost-split',
                'attributes' => [
                    'data-icon' => 'bi bi-signpost-split',
                ],
            ],
            [
                'value' => 'bi bi-signpost',
                'label' => 'signpost',
                'attributes' => [
                    'data-icon' => 'bi bi-signpost',
                ],
            ],
            [
                'value' => 'bi bi-sim-fill',
                'label' => 'sim-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-sim-fill',
                ],
            ],
            [
                'value' => 'bi bi-sim',
                'label' => 'sim',
                'attributes' => [
                    'data-icon' => 'bi bi-sim',
                ],
            ],
            [
                'value' => 'bi bi-skip-backward-btn-fill',
                'label' => 'skip-backward-btn-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-skip-backward-btn-fill',
                ],
            ],
            [
                'value' => 'bi bi-skip-backward-btn',
                'label' => 'skip-backward-btn',
                'attributes' => [
                    'data-icon' => 'bi bi-skip-backward-btn',
                ],
            ],
            [
                'value' => 'bi bi-skip-backward-circle-fill',
                'label' => 'skip-backward-circle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-skip-backward-circle-fill',
                ],
            ],
            [
                'value' => 'bi bi-skip-backward-circle',
                'label' => 'skip-backward-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-skip-backward-circle',
                ],
            ],
            [
                'value' => 'bi bi-skip-backward-fill',
                'label' => 'skip-backward-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-skip-backward-fill',
                ],
            ],
            [
                'value' => 'bi bi-skip-backward',
                'label' => 'skip-backward',
                'attributes' => [
                    'data-icon' => 'bi bi-skip-backward',
                ],
            ],
            [
                'value' => 'bi bi-skip-end-btn-fill',
                'label' => 'skip-end-btn-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-skip-end-btn-fill',
                ],
            ],
            [
                'value' => 'bi bi-skip-end-btn',
                'label' => 'skip-end-btn',
                'attributes' => [
                    'data-icon' => 'bi bi-skip-end-btn',
                ],
            ],
            [
                'value' => 'bi bi-skip-end-circle-fill',
                'label' => 'skip-end-circle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-skip-end-circle-fill',
                ],
            ],
            [
                'value' => 'bi bi-skip-end-circle',
                'label' => 'skip-end-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-skip-end-circle',
                ],
            ],
            [
                'value' => 'bi bi-skip-end-fill',
                'label' => 'skip-end-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-skip-end-fill',
                ],
            ],
            [
                'value' => 'bi bi-skip-end',
                'label' => 'skip-end',
                'attributes' => [
                    'data-icon' => 'bi bi-skip-end',
                ],
            ],
            [
                'value' => 'bi bi-skip-forward-btn-fill',
                'label' => 'skip-forward-btn-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-skip-forward-btn-fill',
                ],
            ],
            [
                'value' => 'bi bi-skip-forward-btn',
                'label' => 'skip-forward-btn',
                'attributes' => [
                    'data-icon' => 'bi bi-skip-forward-btn',
                ],
            ],
            [
                'value' => 'bi bi-skip-forward-circle-fill',
                'label' => 'skip-forward-circle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-skip-forward-circle-fill',
                ],
            ],
            [
                'value' => 'bi bi-skip-forward-circle',
                'label' => 'skip-forward-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-skip-forward-circle',
                ],
            ],
            [
                'value' => 'bi bi-skip-forward-fill',
                'label' => 'skip-forward-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-skip-forward-fill',
                ],
            ],
            [
                'value' => 'bi bi-skip-forward',
                'label' => 'skip-forward',
                'attributes' => [
                    'data-icon' => 'bi bi-skip-forward',
                ],
            ],
            [
                'value' => 'bi bi-skip-start-btn-fill',
                'label' => 'skip-start-btn-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-skip-start-btn-fill',
                ],
            ],
            [
                'value' => 'bi bi-skip-start-btn',
                'label' => 'skip-start-btn',
                'attributes' => [
                    'data-icon' => 'bi bi-skip-start-btn',
                ],
            ],
            [
                'value' => 'bi bi-skip-start-circle-fill',
                'label' => 'skip-start-circle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-skip-start-circle-fill',
                ],
            ],
            [
                'value' => 'bi bi-skip-start-circle',
                'label' => 'skip-start-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-skip-start-circle',
                ],
            ],
            [
                'value' => 'bi bi-skip-start-fill',
                'label' => 'skip-start-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-skip-start-fill',
                ],
            ],
            [
                'value' => 'bi bi-skip-start',
                'label' => 'skip-start',
                'attributes' => [
                    'data-icon' => 'bi bi-skip-start',
                ],
            ],
            [
                'value' => 'bi bi-slack',
                'label' => 'slack',
                'attributes' => [
                    'data-icon' => 'bi bi-slack',
                ],
            ],
            [
                'value' => 'bi bi-slash-circle-fill',
                'label' => 'slash-circle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-slash-circle-fill',
                ],
            ],
            [
                'value' => 'bi bi-slash-circle',
                'label' => 'slash-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-slash-circle',
                ],
            ],
            [
                'value' => 'bi bi-slash-square-fill',
                'label' => 'slash-square-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-slash-square-fill',
                ],
            ],
            [
                'value' => 'bi bi-slash-square',
                'label' => 'slash-square',
                'attributes' => [
                    'data-icon' => 'bi bi-slash-square',
                ],
            ],
            [
                'value' => 'bi bi-slash',
                'label' => 'slash',
                'attributes' => [
                    'data-icon' => 'bi bi-slash',
                ],
            ],
            [
                'value' => 'bi bi-sliders',
                'label' => 'sliders',
                'attributes' => [
                    'data-icon' => 'bi bi-sliders',
                ],
            ],
            [
                'value' => 'bi bi-smartwatch',
                'label' => 'smartwatch',
                'attributes' => [
                    'data-icon' => 'bi bi-smartwatch',
                ],
            ],
            [
                'value' => 'bi bi-snow',
                'label' => 'snow',
                'attributes' => [
                    'data-icon' => 'bi bi-snow',
                ],
            ],
            [
                'value' => 'bi bi-snow2',
                'label' => 'snow2',
                'attributes' => [
                    'data-icon' => 'bi bi-snow2',
                ],
            ],
            [
                'value' => 'bi bi-snow3',
                'label' => 'snow3',
                'attributes' => [
                    'data-icon' => 'bi bi-snow3',
                ],
            ],
            [
                'value' => 'bi bi-sort-alpha-down-alt',
                'label' => 'sort-alpha-down-alt',
                'attributes' => [
                    'data-icon' => 'bi bi-sort-alpha-down-alt',
                ],
            ],
            [
                'value' => 'bi bi-sort-alpha-down',
                'label' => 'sort-alpha-down',
                'attributes' => [
                    'data-icon' => 'bi bi-sort-alpha-down',
                ],
            ],
            [
                'value' => 'bi bi-sort-alpha-up-alt',
                'label' => 'sort-alpha-up-alt',
                'attributes' => [
                    'data-icon' => 'bi bi-sort-alpha-up-alt',
                ],
            ],
            [
                'value' => 'bi bi-sort-alpha-up',
                'label' => 'sort-alpha-up',
                'attributes' => [
                    'data-icon' => 'bi bi-sort-alpha-up',
                ],
            ],
            [
                'value' => 'bi bi-sort-down-alt',
                'label' => 'sort-down-alt',
                'attributes' => [
                    'data-icon' => 'bi bi-sort-down-alt',
                ],
            ],
            [
                'value' => 'bi bi-sort-down',
                'label' => 'sort-down',
                'attributes' => [
                    'data-icon' => 'bi bi-sort-down',
                ],
            ],
            [
                'value' => 'bi bi-sort-numeric-down-alt',
                'label' => 'sort-numeric-down-alt',
                'attributes' => [
                    'data-icon' => 'bi bi-sort-numeric-down-alt',
                ],
            ],
            [
                'value' => 'bi bi-sort-numeric-down',
                'label' => 'sort-numeric-down',
                'attributes' => [
                    'data-icon' => 'bi bi-sort-numeric-down',
                ],
            ],
            [
                'value' => 'bi bi-sort-numeric-up-alt',
                'label' => 'sort-numeric-up-alt',
                'attributes' => [
                    'data-icon' => 'bi bi-sort-numeric-up-alt',
                ],
            ],
            [
                'value' => 'bi bi-sort-numeric-up',
                'label' => 'sort-numeric-up',
                'attributes' => [
                    'data-icon' => 'bi bi-sort-numeric-up',
                ],
            ],
            [
                'value' => 'bi bi-sort-up-alt',
                'label' => 'sort-up-alt',
                'attributes' => [
                    'data-icon' => 'bi bi-sort-up-alt',
                ],
            ],
            [
                'value' => 'bi bi-sort-up',
                'label' => 'sort-up',
                'attributes' => [
                    'data-icon' => 'bi bi-sort-up',
                ],
            ],
            [
                'value' => 'bi bi-soundwave',
                'label' => 'soundwave',
                'attributes' => [
                    'data-icon' => 'bi bi-soundwave',
                ],
            ],
            [
                'value' => 'bi bi-speaker-fill',
                'label' => 'speaker-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-speaker-fill',
                ],
            ],
            [
                'value' => 'bi bi-speaker',
                'label' => 'speaker',
                'attributes' => [
                    'data-icon' => 'bi bi-speaker',
                ],
            ],
            [
                'value' => 'bi bi-speedometer',
                'label' => 'speedometer',
                'attributes' => [
                    'data-icon' => 'bi bi-speedometer',
                ],
            ],
            [
                'value' => 'bi bi-speedometer2',
                'label' => 'speedometer2',
                'attributes' => [
                    'data-icon' => 'bi bi-speedometer2',
                ],
            ],
            [
                'value' => 'bi bi-spellcheck',
                'label' => 'spellcheck',
                'attributes' => [
                    'data-icon' => 'bi bi-spellcheck',
                ],
            ],
            [
                'value' => 'bi bi-square-fill',
                'label' => 'square-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-square-fill',
                ],
            ],
            [
                'value' => 'bi bi-square-half',
                'label' => 'square-half',
                'attributes' => [
                    'data-icon' => 'bi bi-square-half',
                ],
            ],
            [
                'value' => 'bi bi-square',
                'label' => 'square',
                'attributes' => [
                    'data-icon' => 'bi bi-square',
                ],
            ],
            [
                'value' => 'bi bi-stack',
                'label' => 'stack',
                'attributes' => [
                    'data-icon' => 'bi bi-stack',
                ],
            ],
            [
                'value' => 'bi bi-star-fill',
                'label' => 'star-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-star-fill',
                ],
            ],
            [
                'value' => 'bi bi-star-half',
                'label' => 'star-half',
                'attributes' => [
                    'data-icon' => 'bi bi-star-half',
                ],
            ],
            [
                'value' => 'bi bi-star',
                'label' => 'star',
                'attributes' => [
                    'data-icon' => 'bi bi-star',
                ],
            ],
            [
                'value' => 'bi bi-stars',
                'label' => 'stars',
                'attributes' => [
                    'data-icon' => 'bi bi-stars',
                ],
            ],
            [
                'value' => 'bi bi-stickies-fill',
                'label' => 'stickies-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-stickies-fill',
                ],
            ],
            [
                'value' => 'bi bi-stickies',
                'label' => 'stickies',
                'attributes' => [
                    'data-icon' => 'bi bi-stickies',
                ],
            ],
            [
                'value' => 'bi bi-sticky-fill',
                'label' => 'sticky-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-sticky-fill',
                ],
            ],
            [
                'value' => 'bi bi-sticky',
                'label' => 'sticky',
                'attributes' => [
                    'data-icon' => 'bi bi-sticky',
                ],
            ],
            [
                'value' => 'bi bi-stop-btn-fill',
                'label' => 'stop-btn-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-stop-btn-fill',
                ],
            ],
            [
                'value' => 'bi bi-stop-btn',
                'label' => 'stop-btn',
                'attributes' => [
                    'data-icon' => 'bi bi-stop-btn',
                ],
            ],
            [
                'value' => 'bi bi-stop-circle-fill',
                'label' => 'stop-circle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-stop-circle-fill',
                ],
            ],
            [
                'value' => 'bi bi-stop-circle',
                'label' => 'stop-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-stop-circle',
                ],
            ],
            [
                'value' => 'bi bi-stop-fill',
                'label' => 'stop-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-stop-fill',
                ],
            ],
            [
                'value' => 'bi bi-stop',
                'label' => 'stop',
                'attributes' => [
                    'data-icon' => 'bi bi-stop',
                ],
            ],
            [
                'value' => 'bi bi-stoplights-fill',
                'label' => 'stoplights-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-stoplights-fill',
                ],
            ],
            [
                'value' => 'bi bi-stoplights',
                'label' => 'stoplights',
                'attributes' => [
                    'data-icon' => 'bi bi-stoplights',
                ],
            ],
            [
                'value' => 'bi bi-stopwatch-fill',
                'label' => 'stopwatch-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-stopwatch-fill',
                ],
            ],
            [
                'value' => 'bi bi-stopwatch',
                'label' => 'stopwatch',
                'attributes' => [
                    'data-icon' => 'bi bi-stopwatch',
                ],
            ],
            [
                'value' => 'bi bi-subtract',
                'label' => 'subtract',
                'attributes' => [
                    'data-icon' => 'bi bi-subtract',
                ],
            ],
            [
                'value' => 'bi bi-suit-club-fill',
                'label' => 'suit-club-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-suit-club-fill',
                ],
            ],
            [
                'value' => 'bi bi-suit-club',
                'label' => 'suit-club',
                'attributes' => [
                    'data-icon' => 'bi bi-suit-club',
                ],
            ],
            [
                'value' => 'bi bi-suit-diamond-fill',
                'label' => 'suit-diamond-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-suit-diamond-fill',
                ],
            ],
            [
                'value' => 'bi bi-suit-diamond',
                'label' => 'suit-diamond',
                'attributes' => [
                    'data-icon' => 'bi bi-suit-diamond',
                ],
            ],
            [
                'value' => 'bi bi-suit-heart-fill',
                'label' => 'suit-heart-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-suit-heart-fill',
                ],
            ],
            [
                'value' => 'bi bi-suit-heart',
                'label' => 'suit-heart',
                'attributes' => [
                    'data-icon' => 'bi bi-suit-heart',
                ],
            ],
            [
                'value' => 'bi bi-suit-spade-fill',
                'label' => 'suit-spade-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-suit-spade-fill',
                ],
            ],
            [
                'value' => 'bi bi-suit-spade',
                'label' => 'suit-spade',
                'attributes' => [
                    'data-icon' => 'bi bi-suit-spade',
                ],
            ],
            [
                'value' => 'bi bi-sun-fill',
                'label' => 'sun-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-sun-fill',
                ],
            ],
            [
                'value' => 'bi bi-sun',
                'label' => 'sun',
                'attributes' => [
                    'data-icon' => 'bi bi-sun',
                ],
            ],
            [
                'value' => 'bi bi-sunglasses',
                'label' => 'sunglasses',
                'attributes' => [
                    'data-icon' => 'bi bi-sunglasses',
                ],
            ],
            [
                'value' => 'bi bi-sunrise-fill',
                'label' => 'sunrise-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-sunrise-fill',
                ],
            ],
            [
                'value' => 'bi bi-sunrise',
                'label' => 'sunrise',
                'attributes' => [
                    'data-icon' => 'bi bi-sunrise',
                ],
            ],
            [
                'value' => 'bi bi-sunset-fill',
                'label' => 'sunset-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-sunset-fill',
                ],
            ],
            [
                'value' => 'bi bi-sunset',
                'label' => 'sunset',
                'attributes' => [
                    'data-icon' => 'bi bi-sunset',
                ],
            ],
            [
                'value' => 'bi bi-symmetry-horizontal',
                'label' => 'symmetry-horizontal',
                'attributes' => [
                    'data-icon' => 'bi bi-symmetry-horizontal',
                ],
            ],
            [
                'value' => 'bi bi-symmetry-vertical',
                'label' => 'symmetry-vertical',
                'attributes' => [
                    'data-icon' => 'bi bi-symmetry-vertical',
                ],
            ],
            [
                'value' => 'bi bi-table',
                'label' => 'table',
                'attributes' => [
                    'data-icon' => 'bi bi-table',
                ],
            ],
            [
                'value' => 'bi bi-tablet-fill',
                'label' => 'tablet-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-tablet-fill',
                ],
            ],
            [
                'value' => 'bi bi-tablet-landscape-fill',
                'label' => 'tablet-landscape-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-tablet-landscape-fill',
                ],
            ],
            [
                'value' => 'bi bi-tablet-landscape',
                'label' => 'tablet-landscape',
                'attributes' => [
                    'data-icon' => 'bi bi-tablet-landscape',
                ],
            ],
            [
                'value' => 'bi bi-tablet',
                'label' => 'tablet',
                'attributes' => [
                    'data-icon' => 'bi bi-tablet',
                ],
            ],
            [
                'value' => 'bi bi-tag-fill',
                'label' => 'tag-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-tag-fill',
                ],
            ],
            [
                'value' => 'bi bi-tag',
                'label' => 'tag',
                'attributes' => [
                    'data-icon' => 'bi bi-tag',
                ],
            ],
            [
                'value' => 'bi bi-tags-fill',
                'label' => 'tags-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-tags-fill',
                ],
            ],
            [
                'value' => 'bi bi-tags',
                'label' => 'tags',
                'attributes' => [
                    'data-icon' => 'bi bi-tags',
                ],
            ],
            [
                'value' => 'bi bi-telegram',
                'label' => 'telegram',
                'attributes' => [
                    'data-icon' => 'bi bi-telegram',
                ],
            ],
            [
                'value' => 'bi bi-telephone-fill',
                'label' => 'telephone-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-telephone-fill',
                ],
            ],
            [
                'value' => 'bi bi-telephone-forward-fill',
                'label' => 'telephone-forward-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-telephone-forward-fill',
                ],
            ],
            [
                'value' => 'bi bi-telephone-forward',
                'label' => 'telephone-forward',
                'attributes' => [
                    'data-icon' => 'bi bi-telephone-forward',
                ],
            ],
            [
                'value' => 'bi bi-telephone-inbound-fill',
                'label' => 'telephone-inbound-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-telephone-inbound-fill',
                ],
            ],
            [
                'value' => 'bi bi-telephone-inbound',
                'label' => 'telephone-inbound',
                'attributes' => [
                    'data-icon' => 'bi bi-telephone-inbound',
                ],
            ],
            [
                'value' => 'bi bi-telephone-minus-fill',
                'label' => 'telephone-minus-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-telephone-minus-fill',
                ],
            ],
            [
                'value' => 'bi bi-telephone-minus',
                'label' => 'telephone-minus',
                'attributes' => [
                    'data-icon' => 'bi bi-telephone-minus',
                ],
            ],
            [
                'value' => 'bi bi-telephone-outbound-fill',
                'label' => 'telephone-outbound-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-telephone-outbound-fill',
                ],
            ],
            [
                'value' => 'bi bi-telephone-outbound',
                'label' => 'telephone-outbound',
                'attributes' => [
                    'data-icon' => 'bi bi-telephone-outbound',
                ],
            ],
            [
                'value' => 'bi bi-telephone-plus-fill',
                'label' => 'telephone-plus-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-telephone-plus-fill',
                ],
            ],
            [
                'value' => 'bi bi-telephone-plus',
                'label' => 'telephone-plus',
                'attributes' => [
                    'data-icon' => 'bi bi-telephone-plus',
                ],
            ],
            [
                'value' => 'bi bi-telephone-x-fill',
                'label' => 'telephone-x-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-telephone-x-fill',
                ],
            ],
            [
                'value' => 'bi bi-telephone-x',
                'label' => 'telephone-x',
                'attributes' => [
                    'data-icon' => 'bi bi-telephone-x',
                ],
            ],
            [
                'value' => 'bi bi-telephone',
                'label' => 'telephone',
                'attributes' => [
                    'data-icon' => 'bi bi-telephone',
                ],
            ],
            [
                'value' => 'bi bi-terminal-fill',
                'label' => 'terminal-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-terminal-fill',
                ],
            ],
            [
                'value' => 'bi bi-terminal',
                'label' => 'terminal',
                'attributes' => [
                    'data-icon' => 'bi bi-terminal',
                ],
            ],
            [
                'value' => 'bi bi-text-center',
                'label' => 'text-center',
                'attributes' => [
                    'data-icon' => 'bi bi-text-center',
                ],
            ],
            [
                'value' => 'bi bi-text-indent-left',
                'label' => 'text-indent-left',
                'attributes' => [
                    'data-icon' => 'bi bi-text-indent-left',
                ],
            ],
            [
                'value' => 'bi bi-text-indent-right',
                'label' => 'text-indent-right',
                'attributes' => [
                    'data-icon' => 'bi bi-text-indent-right',
                ],
            ],
            [
                'value' => 'bi bi-text-left',
                'label' => 'text-left',
                'attributes' => [
                    'data-icon' => 'bi bi-text-left',
                ],
            ],
            [
                'value' => 'bi bi-text-paragraph',
                'label' => 'text-paragraph',
                'attributes' => [
                    'data-icon' => 'bi bi-text-paragraph',
                ],
            ],
            [
                'value' => 'bi bi-text-right',
                'label' => 'text-right',
                'attributes' => [
                    'data-icon' => 'bi bi-text-right',
                ],
            ],
            [
                'value' => 'bi bi-textarea-resize',
                'label' => 'textarea-resize',
                'attributes' => [
                    'data-icon' => 'bi bi-textarea-resize',
                ],
            ],
            [
                'value' => 'bi bi-textarea-t',
                'label' => 'textarea-t',
                'attributes' => [
                    'data-icon' => 'bi bi-textarea-t',
                ],
            ],
            [
                'value' => 'bi bi-textarea',
                'label' => 'textarea',
                'attributes' => [
                    'data-icon' => 'bi bi-textarea',
                ],
            ],
            [
                'value' => 'bi bi-thermometer-half',
                'label' => 'thermometer-half',
                'attributes' => [
                    'data-icon' => 'bi bi-thermometer-half',
                ],
            ],
            [
                'value' => 'bi bi-thermometer-high',
                'label' => 'thermometer-high',
                'attributes' => [
                    'data-icon' => 'bi bi-thermometer-high',
                ],
            ],
            [
                'value' => 'bi bi-thermometer-low',
                'label' => 'thermometer-low',
                'attributes' => [
                    'data-icon' => 'bi bi-thermometer-low',
                ],
            ],
            [
                'value' => 'bi bi-thermometer-snow',
                'label' => 'thermometer-snow',
                'attributes' => [
                    'data-icon' => 'bi bi-thermometer-snow',
                ],
            ],
            [
                'value' => 'bi bi-thermometer-sun',
                'label' => 'thermometer-sun',
                'attributes' => [
                    'data-icon' => 'bi bi-thermometer-sun',
                ],
            ],
            [
                'value' => 'bi bi-thermometer',
                'label' => 'thermometer',
                'attributes' => [
                    'data-icon' => 'bi bi-thermometer',
                ],
            ],
            [
                'value' => 'bi bi-three-dots-vertical',
                'label' => 'three-dots-vertical',
                'attributes' => [
                    'data-icon' => 'bi bi-three-dots-vertical',
                ],
            ],
            [
                'value' => 'bi bi-three-dots',
                'label' => 'three-dots',
                'attributes' => [
                    'data-icon' => 'bi bi-three-dots',
                ],
            ],
            [
                'value' => 'bi bi-toggle-off',
                'label' => 'toggle-off',
                'attributes' => [
                    'data-icon' => 'bi bi-toggle-off',
                ],
            ],
            [
                'value' => 'bi bi-toggle-on',
                'label' => 'toggle-on',
                'attributes' => [
                    'data-icon' => 'bi bi-toggle-on',
                ],
            ],
            [
                'value' => 'bi bi-toggle2-off',
                'label' => 'toggle2-off',
                'attributes' => [
                    'data-icon' => 'bi bi-toggle2-off',
                ],
            ],
            [
                'value' => 'bi bi-toggle2-on',
                'label' => 'toggle2-on',
                'attributes' => [
                    'data-icon' => 'bi bi-toggle2-on',
                ],
            ],
            [
                'value' => 'bi bi-toggles',
                'label' => 'toggles',
                'attributes' => [
                    'data-icon' => 'bi bi-toggles',
                ],
            ],
            [
                'value' => 'bi bi-toggles2',
                'label' => 'toggles2',
                'attributes' => [
                    'data-icon' => 'bi bi-toggles2',
                ],
            ],
            [
                'value' => 'bi bi-tools',
                'label' => 'tools',
                'attributes' => [
                    'data-icon' => 'bi bi-tools',
                ],
            ],
            [
                'value' => 'bi bi-tornado',
                'label' => 'tornado',
                'attributes' => [
                    'data-icon' => 'bi bi-tornado',
                ],
            ],
            [
                'value' => 'bi bi-trash-fill',
                'label' => 'trash-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-trash-fill',
                ],
            ],
            [
                'value' => 'bi bi-trash',
                'label' => 'trash',
                'attributes' => [
                    'data-icon' => 'bi bi-trash',
                ],
            ],
            [
                'value' => 'bi bi-trash2-fill',
                'label' => 'trash2-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-trash2-fill',
                ],
            ],
            [
                'value' => 'bi bi-trash2',
                'label' => 'trash2',
                'attributes' => [
                    'data-icon' => 'bi bi-trash2',
                ],
            ],
            [
                'value' => 'bi bi-tree-fill',
                'label' => 'tree-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-tree-fill',
                ],
            ],
            [
                'value' => 'bi bi-tree',
                'label' => 'tree',
                'attributes' => [
                    'data-icon' => 'bi bi-tree',
                ],
            ],
            [
                'value' => 'bi bi-triangle-fill',
                'label' => 'triangle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-triangle-fill',
                ],
            ],
            [
                'value' => 'bi bi-triangle-half',
                'label' => 'triangle-half',
                'attributes' => [
                    'data-icon' => 'bi bi-triangle-half',
                ],
            ],
            [
                'value' => 'bi bi-triangle',
                'label' => 'triangle',
                'attributes' => [
                    'data-icon' => 'bi bi-triangle',
                ],
            ],
            [
                'value' => 'bi bi-trophy-fill',
                'label' => 'trophy-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-trophy-fill',
                ],
            ],
            [
                'value' => 'bi bi-trophy',
                'label' => 'trophy',
                'attributes' => [
                    'data-icon' => 'bi bi-trophy',
                ],
            ],
            [
                'value' => 'bi bi-tropical-storm',
                'label' => 'tropical-storm',
                'attributes' => [
                    'data-icon' => 'bi bi-tropical-storm',
                ],
            ],
            [
                'value' => 'bi bi-truck-flatbed',
                'label' => 'truck-flatbed',
                'attributes' => [
                    'data-icon' => 'bi bi-truck-flatbed',
                ],
            ],
            [
                'value' => 'bi bi-truck',
                'label' => 'truck',
                'attributes' => [
                    'data-icon' => 'bi bi-truck',
                ],
            ],
            [
                'value' => 'bi bi-tsunami',
                'label' => 'tsunami',
                'attributes' => [
                    'data-icon' => 'bi bi-tsunami',
                ],
            ],
            [
                'value' => 'bi bi-tv-fill',
                'label' => 'tv-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-tv-fill',
                ],
            ],
            [
                'value' => 'bi bi-tv',
                'label' => 'tv',
                'attributes' => [
                    'data-icon' => 'bi bi-tv',
                ],
            ],
            [
                'value' => 'bi bi-twitch',
                'label' => 'twitch',
                'attributes' => [
                    'data-icon' => 'bi bi-twitch',
                ],
            ],
            [
                'value' => 'bi bi-twitter',
                'label' => 'twitter',
                'attributes' => [
                    'data-icon' => 'bi bi-twitter',
                ],
            ],
            [
                'value' => 'bi bi-type-bold',
                'label' => 'type-bold',
                'attributes' => [
                    'data-icon' => 'bi bi-type-bold',
                ],
            ],
            [
                'value' => 'bi bi-type-h1',
                'label' => 'type-h1',
                'attributes' => [
                    'data-icon' => 'bi bi-type-h1',
                ],
            ],
            [
                'value' => 'bi bi-type-h2',
                'label' => 'type-h2',
                'attributes' => [
                    'data-icon' => 'bi bi-type-h2',
                ],
            ],
            [
                'value' => 'bi bi-type-h3',
                'label' => 'type-h3',
                'attributes' => [
                    'data-icon' => 'bi bi-type-h3',
                ],
            ],
            [
                'value' => 'bi bi-type-italic',
                'label' => 'type-italic',
                'attributes' => [
                    'data-icon' => 'bi bi-type-italic',
                ],
            ],
            [
                'value' => 'bi bi-type-strikethrough',
                'label' => 'type-strikethrough',
                'attributes' => [
                    'data-icon' => 'bi bi-type-strikethrough',
                ],
            ],
            [
                'value' => 'bi bi-type-underline',
                'label' => 'type-underline',
                'attributes' => [
                    'data-icon' => 'bi bi-type-underline',
                ],
            ],
            [
                'value' => 'bi bi-type',
                'label' => 'type',
                'attributes' => [
                    'data-icon' => 'bi bi-type',
                ],
            ],
            [
                'value' => 'bi bi-ui-checks-grid',
                'label' => 'ui-checks-grid',
                'attributes' => [
                    'data-icon' => 'bi bi-ui-checks-grid',
                ],
            ],
            [
                'value' => 'bi bi-ui-checks',
                'label' => 'ui-checks',
                'attributes' => [
                    'data-icon' => 'bi bi-ui-checks',
                ],
            ],
            [
                'value' => 'bi bi-ui-radios-grid',
                'label' => 'ui-radios-grid',
                'attributes' => [
                    'data-icon' => 'bi bi-ui-radios-grid',
                ],
            ],
            [
                'value' => 'bi bi-ui-radios',
                'label' => 'ui-radios',
                'attributes' => [
                    'data-icon' => 'bi bi-ui-radios',
                ],
            ],
            [
                'value' => 'bi bi-umbrella-fill',
                'label' => 'umbrella-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-umbrella-fill',
                ],
            ],
            [
                'value' => 'bi bi-umbrella',
                'label' => 'umbrella',
                'attributes' => [
                    'data-icon' => 'bi bi-umbrella',
                ],
            ],
            [
                'value' => 'bi bi-union',
                'label' => 'union',
                'attributes' => [
                    'data-icon' => 'bi bi-union',
                ],
            ],
            [
                'value' => 'bi bi-unlock-fill',
                'label' => 'unlock-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-unlock-fill',
                ],
            ],
            [
                'value' => 'bi bi-unlock',
                'label' => 'unlock',
                'attributes' => [
                    'data-icon' => 'bi bi-unlock',
                ],
            ],
            [
                'value' => 'bi bi-upc-scan',
                'label' => 'upc-scan',
                'attributes' => [
                    'data-icon' => 'bi bi-upc-scan',
                ],
            ],
            [
                'value' => 'bi bi-upc',
                'label' => 'upc',
                'attributes' => [
                    'data-icon' => 'bi bi-upc',
                ],
            ],
            [
                'value' => 'bi bi-upload',
                'label' => 'upload',
                'attributes' => [
                    'data-icon' => 'bi bi-upload',
                ],
            ],
            [
                'value' => 'bi bi-vector-pen',
                'label' => 'vector-pen',
                'attributes' => [
                    'data-icon' => 'bi bi-vector-pen',
                ],
            ],
            [
                'value' => 'bi bi-view-list',
                'label' => 'view-list',
                'attributes' => [
                    'data-icon' => 'bi bi-view-list',
                ],
            ],
            [
                'value' => 'bi bi-view-stacked',
                'label' => 'view-stacked',
                'attributes' => [
                    'data-icon' => 'bi bi-view-stacked',
                ],
            ],
            [
                'value' => 'bi bi-vinyl-fill',
                'label' => 'vinyl-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-vinyl-fill',
                ],
            ],
            [
                'value' => 'bi bi-vinyl',
                'label' => 'vinyl',
                'attributes' => [
                    'data-icon' => 'bi bi-vinyl',
                ],
            ],
            [
                'value' => 'bi bi-voicemail',
                'label' => 'voicemail',
                'attributes' => [
                    'data-icon' => 'bi bi-voicemail',
                ],
            ],
            [
                'value' => 'bi bi-volume-down-fill',
                'label' => 'volume-down-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-volume-down-fill',
                ],
            ],
            [
                'value' => 'bi bi-volume-down',
                'label' => 'volume-down',
                'attributes' => [
                    'data-icon' => 'bi bi-volume-down',
                ],
            ],
            [
                'value' => 'bi bi-volume-mute-fill',
                'label' => 'volume-mute-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-volume-mute-fill',
                ],
            ],
            [
                'value' => 'bi bi-volume-mute',
                'label' => 'volume-mute',
                'attributes' => [
                    'data-icon' => 'bi bi-volume-mute',
                ],
            ],
            [
                'value' => 'bi bi-volume-off-fill',
                'label' => 'volume-off-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-volume-off-fill',
                ],
            ],
            [
                'value' => 'bi bi-volume-off',
                'label' => 'volume-off',
                'attributes' => [
                    'data-icon' => 'bi bi-volume-off',
                ],
            ],
            [
                'value' => 'bi bi-volume-up-fill',
                'label' => 'volume-up-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-volume-up-fill',
                ],
            ],
            [
                'value' => 'bi bi-volume-up',
                'label' => 'volume-up',
                'attributes' => [
                    'data-icon' => 'bi bi-volume-up',
                ],
            ],
            [
                'value' => 'bi bi-vr',
                'label' => 'vr',
                'attributes' => [
                    'data-icon' => 'bi bi-vr',
                ],
            ],
            [
                'value' => 'bi bi-wallet-fill',
                'label' => 'wallet-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-wallet-fill',
                ],
            ],
            [
                'value' => 'bi bi-wallet',
                'label' => 'wallet',
                'attributes' => [
                    'data-icon' => 'bi bi-wallet',
                ],
            ],
            [
                'value' => 'bi bi-wallet2',
                'label' => 'wallet2',
                'attributes' => [
                    'data-icon' => 'bi bi-wallet2',
                ],
            ],
            [
                'value' => 'bi bi-watch',
                'label' => 'watch',
                'attributes' => [
                    'data-icon' => 'bi bi-watch',
                ],
            ],
            [
                'value' => 'bi bi-water',
                'label' => 'water',
                'attributes' => [
                    'data-icon' => 'bi bi-water',
                ],
            ],
            [
                'value' => 'bi bi-whatsapp',
                'label' => 'whatsapp',
                'attributes' => [
                    'data-icon' => 'bi bi-whatsapp',
                ],
            ],
            [
                'value' => 'bi bi-wifi-1',
                'label' => 'wifi-1',
                'attributes' => [
                    'data-icon' => 'bi bi-wifi-1',
                ],
            ],
            [
                'value' => 'bi bi-wifi-2',
                'label' => 'wifi-2',
                'attributes' => [
                    'data-icon' => 'bi bi-wifi-2',
                ],
            ],
            [
                'value' => 'bi bi-wifi-off',
                'label' => 'wifi-off',
                'attributes' => [
                    'data-icon' => 'bi bi-wifi-off',
                ],
            ],
            [
                'value' => 'bi bi-wifi',
                'label' => 'wifi',
                'attributes' => [
                    'data-icon' => 'bi bi-wifi',
                ],
            ],
            [
                'value' => 'bi bi-wind',
                'label' => 'wind',
                'attributes' => [
                    'data-icon' => 'bi bi-wind',
                ],
            ],
            [
                'value' => 'bi bi-window-dock',
                'label' => 'window-dock',
                'attributes' => [
                    'data-icon' => 'bi bi-window-dock',
                ],
            ],
            [
                'value' => 'bi bi-window-sidebar',
                'label' => 'window-sidebar',
                'attributes' => [
                    'data-icon' => 'bi bi-window-sidebar',
                ],
            ],
            [
                'value' => 'bi bi-window',
                'label' => 'window',
                'attributes' => [
                    'data-icon' => 'bi bi-window',
                ],
            ],
            [
                'value' => 'bi bi-wrench',
                'label' => 'wrench',
                'attributes' => [
                    'data-icon' => 'bi bi-wrench',
                ],
            ],
            [
                'value' => 'bi bi-x-circle-fill',
                'label' => 'x-circle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-x-circle-fill',
                ],
            ],
            [
                'value' => 'bi bi-x-circle',
                'label' => 'x-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-x-circle',
                ],
            ],
            [
                'value' => 'bi bi-x-diamond-fill',
                'label' => 'x-diamond-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-x-diamond-fill',
                ],
            ],
            [
                'value' => 'bi bi-x-diamond',
                'label' => 'x-diamond',
                'attributes' => [
                    'data-icon' => 'bi bi-x-diamond',
                ],
            ],
            [
                'value' => 'bi bi-x-octagon-fill',
                'label' => 'x-octagon-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-x-octagon-fill',
                ],
            ],
            [
                'value' => 'bi bi-x-octagon',
                'label' => 'x-octagon',
                'attributes' => [
                    'data-icon' => 'bi bi-x-octagon',
                ],
            ],
            [
                'value' => 'bi bi-x-square-fill',
                'label' => 'x-square-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-x-square-fill',
                ],
            ],
            [
                'value' => 'bi bi-x-square',
                'label' => 'x-square',
                'attributes' => [
                    'data-icon' => 'bi bi-x-square',
                ],
            ],
            [
                'value' => 'bi bi-x',
                'label' => 'x',
                'attributes' => [
                    'data-icon' => 'bi bi-x',
                ],
            ],
            [
                'value' => 'bi bi-youtube',
                'label' => 'youtube',
                'attributes' => [
                    'data-icon' => 'bi bi-youtube',
                ],
            ],
            [
                'value' => 'bi bi-zoom-in',
                'label' => 'zoom-in',
                'attributes' => [
                    'data-icon' => 'bi bi-zoom-in',
                ],
            ],
            [
                'value' => 'bi bi-zoom-out',
                'label' => 'zoom-out',
                'attributes' => [
                    'data-icon' => 'bi bi-zoom-out',
                ],
            ],
            [
                'value' => 'bi bi-bank',
                'label' => 'bank',
                'attributes' => [
                    'data-icon' => 'bi bi-bank',
                ],
            ],
            [
                'value' => 'bi bi-bank2',
                'label' => 'bank2',
                'attributes' => [
                    'data-icon' => 'bi bi-bank2',
                ],
            ],
            [
                'value' => 'bi bi-bell-slash-fill',
                'label' => 'bell-slash-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-bell-slash-fill',
                ],
            ],
            [
                'value' => 'bi bi-bell-slash',
                'label' => 'bell-slash',
                'attributes' => [
                    'data-icon' => 'bi bi-bell-slash',
                ],
            ],
            [
                'value' => 'bi bi-cash-coin',
                'label' => 'cash-coin',
                'attributes' => [
                    'data-icon' => 'bi bi-cash-coin',
                ],
            ],
            [
                'value' => 'bi bi-check-lg',
                'label' => 'check-lg',
                'attributes' => [
                    'data-icon' => 'bi bi-check-lg',
                ],
            ],
            [
                'value' => 'bi bi-coin',
                'label' => 'coin',
                'attributes' => [
                    'data-icon' => 'bi bi-coin',
                ],
            ],
            [
                'value' => 'bi bi-currency-bitcoin',
                'label' => 'currency-bitcoin',
                'attributes' => [
                    'data-icon' => 'bi bi-currency-bitcoin',
                ],
            ],
            [
                'value' => 'bi bi-currency-dollar',
                'label' => 'currency-dollar',
                'attributes' => [
                    'data-icon' => 'bi bi-currency-dollar',
                ],
            ],
            [
                'value' => 'bi bi-currency-euro',
                'label' => 'currency-euro',
                'attributes' => [
                    'data-icon' => 'bi bi-currency-euro',
                ],
            ],
            [
                'value' => 'bi bi-currency-exchange',
                'label' => 'currency-exchange',
                'attributes' => [
                    'data-icon' => 'bi bi-currency-exchange',
                ],
            ],
            [
                'value' => 'bi bi-currency-pound',
                'label' => 'currency-pound',
                'attributes' => [
                    'data-icon' => 'bi bi-currency-pound',
                ],
            ],
            [
                'value' => 'bi bi-currency-yen',
                'label' => 'currency-yen',
                'attributes' => [
                    'data-icon' => 'bi bi-currency-yen',
                ],
            ],
            [
                'value' => 'bi bi-dash-lg',
                'label' => 'dash-lg',
                'attributes' => [
                    'data-icon' => 'bi bi-dash-lg',
                ],
            ],
            [
                'value' => 'bi bi-exclamation-lg',
                'label' => 'exclamation-lg',
                'attributes' => [
                    'data-icon' => 'bi bi-exclamation-lg',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-pdf-fill',
                'label' => 'file-earmark-pdf-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-pdf-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-earmark-pdf',
                'label' => 'file-earmark-pdf',
                'attributes' => [
                    'data-icon' => 'bi bi-file-earmark-pdf',
                ],
            ],
            [
                'value' => 'bi bi-file-pdf-fill',
                'label' => 'file-pdf-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-file-pdf-fill',
                ],
            ],
            [
                'value' => 'bi bi-file-pdf',
                'label' => 'file-pdf',
                'attributes' => [
                    'data-icon' => 'bi bi-file-pdf',
                ],
            ],
            [
                'value' => 'bi bi-gender-ambiguous',
                'label' => 'gender-ambiguous',
                'attributes' => [
                    'data-icon' => 'bi bi-gender-ambiguous',
                ],
            ],
            [
                'value' => 'bi bi-gender-female',
                'label' => 'gender-female',
                'attributes' => [
                    'data-icon' => 'bi bi-gender-female',
                ],
            ],
            [
                'value' => 'bi bi-gender-male',
                'label' => 'gender-male',
                'attributes' => [
                    'data-icon' => 'bi bi-gender-male',
                ],
            ],
            [
                'value' => 'bi bi-gender-trans',
                'label' => 'gender-trans',
                'attributes' => [
                    'data-icon' => 'bi bi-gender-trans',
                ],
            ],
            [
                'value' => 'bi bi-headset-vr',
                'label' => 'headset-vr',
                'attributes' => [
                    'data-icon' => 'bi bi-headset-vr',
                ],
            ],
            [
                'value' => 'bi bi-info-lg',
                'label' => 'info-lg',
                'attributes' => [
                    'data-icon' => 'bi bi-info-lg',
                ],
            ],
            [
                'value' => 'bi bi-mastodon',
                'label' => 'mastodon',
                'attributes' => [
                    'data-icon' => 'bi bi-mastodon',
                ],
            ],
            [
                'value' => 'bi bi-messenger',
                'label' => 'messenger',
                'attributes' => [
                    'data-icon' => 'bi bi-messenger',
                ],
            ],
            [
                'value' => 'bi bi-piggy-bank-fill',
                'label' => 'piggy-bank-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-piggy-bank-fill',
                ],
            ],
            [
                'value' => 'bi bi-piggy-bank',
                'label' => 'piggy-bank',
                'attributes' => [
                    'data-icon' => 'bi bi-piggy-bank',
                ],
            ],
            [
                'value' => 'bi bi-pin-map-fill',
                'label' => 'pin-map-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-pin-map-fill',
                ],
            ],
            [
                'value' => 'bi bi-pin-map',
                'label' => 'pin-map',
                'attributes' => [
                    'data-icon' => 'bi bi-pin-map',
                ],
            ],
            [
                'value' => 'bi bi-plus-lg',
                'label' => 'plus-lg',
                'attributes' => [
                    'data-icon' => 'bi bi-plus-lg',
                ],
            ],
            [
                'value' => 'bi bi-question-lg',
                'label' => 'question-lg',
                'attributes' => [
                    'data-icon' => 'bi bi-question-lg',
                ],
            ],
            [
                'value' => 'bi bi-recycle',
                'label' => 'recycle',
                'attributes' => [
                    'data-icon' => 'bi bi-recycle',
                ],
            ],
            [
                'value' => 'bi bi-reddit',
                'label' => 'reddit',
                'attributes' => [
                    'data-icon' => 'bi bi-reddit',
                ],
            ],
            [
                'value' => 'bi bi-safe-fill',
                'label' => 'safe-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-safe-fill',
                ],
            ],
            [
                'value' => 'bi bi-safe2-fill',
                'label' => 'safe2-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-safe2-fill',
                ],
            ],
            [
                'value' => 'bi bi-safe2',
                'label' => 'safe2',
                'attributes' => [
                    'data-icon' => 'bi bi-safe2',
                ],
            ],
            [
                'value' => 'bi bi-sd-card-fill',
                'label' => 'sd-card-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-sd-card-fill',
                ],
            ],
            [
                'value' => 'bi bi-sd-card',
                'label' => 'sd-card',
                'attributes' => [
                    'data-icon' => 'bi bi-sd-card',
                ],
            ],
            [
                'value' => 'bi bi-skype',
                'label' => 'skype',
                'attributes' => [
                    'data-icon' => 'bi bi-skype',
                ],
            ],
            [
                'value' => 'bi bi-slash-lg',
                'label' => 'slash-lg',
                'attributes' => [
                    'data-icon' => 'bi bi-slash-lg',
                ],
            ],
            [
                'value' => 'bi bi-translate',
                'label' => 'translate',
                'attributes' => [
                    'data-icon' => 'bi bi-translate',
                ],
            ],
            [
                'value' => 'bi bi-x-lg',
                'label' => 'x-lg',
                'attributes' => [
                    'data-icon' => 'bi bi-x-lg',
                ],
            ],
            [
                'value' => 'bi bi-safe',
                'label' => 'safe',
                'attributes' => [
                    'data-icon' => 'bi bi-safe',
                ],
            ],
            [
                'value' => 'bi bi-apple',
                'label' => 'apple',
                'attributes' => [
                    'data-icon' => 'bi bi-apple',
                ],
            ],
            [
                'value' => 'bi bi-microsoft',
                'label' => 'microsoft',
                'attributes' => [
                    'data-icon' => 'bi bi-microsoft',
                ],
            ],
            [
                'value' => 'bi bi-windows',
                'label' => 'windows',
                'attributes' => [
                    'data-icon' => 'bi bi-windows',
                ],
            ],
            [
                'value' => 'bi bi-behance',
                'label' => 'behance',
                'attributes' => [
                    'data-icon' => 'bi bi-behance',
                ],
            ],
            [
                'value' => 'bi bi-dribbble',
                'label' => 'dribbble',
                'attributes' => [
                    'data-icon' => 'bi bi-dribbble',
                ],
            ],
            [
                'value' => 'bi bi-line',
                'label' => 'line',
                'attributes' => [
                    'data-icon' => 'bi bi-line',
                ],
            ],
            [
                'value' => 'bi bi-medium',
                'label' => 'medium',
                'attributes' => [
                    'data-icon' => 'bi bi-medium',
                ],
            ],
            [
                'value' => 'bi bi-paypal',
                'label' => 'paypal',
                'attributes' => [
                    'data-icon' => 'bi bi-paypal',
                ],
            ],
            [
                'value' => 'bi bi-pinterest',
                'label' => 'pinterest',
                'attributes' => [
                    'data-icon' => 'bi bi-pinterest',
                ],
            ],
            [
                'value' => 'bi bi-signal',
                'label' => 'signal',
                'attributes' => [
                    'data-icon' => 'bi bi-signal',
                ],
            ],
            [
                'value' => 'bi bi-snapchat',
                'label' => 'snapchat',
                'attributes' => [
                    'data-icon' => 'bi bi-snapchat',
                ],
            ],
            [
                'value' => 'bi bi-spotify',
                'label' => 'spotify',
                'attributes' => [
                    'data-icon' => 'bi bi-spotify',
                ],
            ],
            [
                'value' => 'bi bi-stack-overflow',
                'label' => 'stack-overflow',
                'attributes' => [
                    'data-icon' => 'bi bi-stack-overflow',
                ],
            ],
            [
                'value' => 'bi bi-strava',
                'label' => 'strava',
                'attributes' => [
                    'data-icon' => 'bi bi-strava',
                ],
            ],
            [
                'value' => 'bi bi-wordpress',
                'label' => 'wordpress',
                'attributes' => [
                    'data-icon' => 'bi bi-wordpress',
                ],
            ],
            [
                'value' => 'bi bi-vimeo',
                'label' => 'vimeo',
                'attributes' => [
                    'data-icon' => 'bi bi-vimeo',
                ],
            ],
            [
                'value' => 'bi bi-activity',
                'label' => 'activity',
                'attributes' => [
                    'data-icon' => 'bi bi-activity',
                ],
            ],
            [
                'value' => 'bi bi-easel2-fill',
                'label' => 'easel2-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-easel2-fill',
                ],
            ],
            [
                'value' => 'bi bi-easel2',
                'label' => 'easel2',
                'attributes' => [
                    'data-icon' => 'bi bi-easel2',
                ],
            ],
            [
                'value' => 'bi bi-easel3-fill',
                'label' => 'easel3-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-easel3-fill',
                ],
            ],
            [
                'value' => 'bi bi-easel3',
                'label' => 'easel3',
                'attributes' => [
                    'data-icon' => 'bi bi-easel3',
                ],
            ],
            [
                'value' => 'bi bi-fan',
                'label' => 'fan',
                'attributes' => [
                    'data-icon' => 'bi bi-fan',
                ],
            ],
            [
                'value' => 'bi bi-fingerprint',
                'label' => 'fingerprint',
                'attributes' => [
                    'data-icon' => 'bi bi-fingerprint',
                ],
            ],
            [
                'value' => 'bi bi-graph-down-arrow',
                'label' => 'graph-down-arrow',
                'attributes' => [
                    'data-icon' => 'bi bi-graph-down-arrow',
                ],
            ],
            [
                'value' => 'bi bi-graph-up-arrow',
                'label' => 'graph-up-arrow',
                'attributes' => [
                    'data-icon' => 'bi bi-graph-up-arrow',
                ],
            ],
            [
                'value' => 'bi bi-hypnotize',
                'label' => 'hypnotize',
                'attributes' => [
                    'data-icon' => 'bi bi-hypnotize',
                ],
            ],
            [
                'value' => 'bi bi-magic',
                'label' => 'magic',
                'attributes' => [
                    'data-icon' => 'bi bi-magic',
                ],
            ],
            [
                'value' => 'bi bi-person-rolodex',
                'label' => 'person-rolodex',
                'attributes' => [
                    'data-icon' => 'bi bi-person-rolodex',
                ],
            ],
            [
                'value' => 'bi bi-person-video',
                'label' => 'person-video',
                'attributes' => [
                    'data-icon' => 'bi bi-person-video',
                ],
            ],
            [
                'value' => 'bi bi-person-video2',
                'label' => 'person-video2',
                'attributes' => [
                    'data-icon' => 'bi bi-person-video2',
                ],
            ],
            [
                'value' => 'bi bi-person-video3',
                'label' => 'person-video3',
                'attributes' => [
                    'data-icon' => 'bi bi-person-video3',
                ],
            ],
            [
                'value' => 'bi bi-person-workspace',
                'label' => 'person-workspace',
                'attributes' => [
                    'data-icon' => 'bi bi-person-workspace',
                ],
            ],
            [
                'value' => 'bi bi-radioactive',
                'label' => 'radioactive',
                'attributes' => [
                    'data-icon' => 'bi bi-radioactive',
                ],
            ],
            [
                'value' => 'bi bi-webcam-fill',
                'label' => 'webcam-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-webcam-fill',
                ],
            ],
            [
                'value' => 'bi bi-webcam',
                'label' => 'webcam',
                'attributes' => [
                    'data-icon' => 'bi bi-webcam',
                ],
            ],
            [
                'value' => 'bi bi-yin-yang',
                'label' => 'yin-yang',
                'attributes' => [
                    'data-icon' => 'bi bi-yin-yang',
                ],
            ],
            [
                'value' => 'bi bi-bandaid-fill',
                'label' => 'bandaid-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-bandaid-fill',
                ],
            ],
            [
                'value' => 'bi bi-bandaid',
                'label' => 'bandaid',
                'attributes' => [
                    'data-icon' => 'bi bi-bandaid',
                ],
            ],
            [
                'value' => 'bi bi-bluetooth',
                'label' => 'bluetooth',
                'attributes' => [
                    'data-icon' => 'bi bi-bluetooth',
                ],
            ],
            [
                'value' => 'bi bi-body-text',
                'label' => 'body-text',
                'attributes' => [
                    'data-icon' => 'bi bi-body-text',
                ],
            ],
            [
                'value' => 'bi bi-boombox',
                'label' => 'boombox',
                'attributes' => [
                    'data-icon' => 'bi bi-boombox',
                ],
            ],
            [
                'value' => 'bi bi-boxes',
                'label' => 'boxes',
                'attributes' => [
                    'data-icon' => 'bi bi-boxes',
                ],
            ],
            [
                'value' => 'bi bi-dpad-fill',
                'label' => 'dpad-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-dpad-fill',
                ],
            ],
            [
                'value' => 'bi bi-dpad',
                'label' => 'dpad',
                'attributes' => [
                    'data-icon' => 'bi bi-dpad',
                ],
            ],
            [
                'value' => 'bi bi-ear-fill',
                'label' => 'ear-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-ear-fill',
                ],
            ],
            [
                'value' => 'bi bi-ear',
                'label' => 'ear',
                'attributes' => [
                    'data-icon' => 'bi bi-ear',
                ],
            ],
            [
                'value' => 'bi bi-envelope-check-1',
                'label' => 'envelope-check-1',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-check-1',
                ],
            ],
            [
                'value' => 'bi bi-envelope-check-fill',
                'label' => 'envelope-check-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-check-fill',
                ],
            ],
            [
                'value' => 'bi bi-envelope-check',
                'label' => 'envelope-check',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-check',
                ],
            ],
            [
                'value' => 'bi bi-envelope-dash-1',
                'label' => 'envelope-dash-1',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-dash-1',
                ],
            ],
            [
                'value' => 'bi bi-envelope-dash-fill',
                'label' => 'envelope-dash-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-dash-fill',
                ],
            ],
            [
                'value' => 'bi bi-envelope-dash',
                'label' => 'envelope-dash',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-dash',
                ],
            ],
            [
                'value' => 'bi bi-envelope-exclamation-1',
                'label' => 'envelope-exclamation-1',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-exclamation-1',
                ],
            ],
            [
                'value' => 'bi bi-envelope-exclamation-fill',
                'label' => 'envelope-exclamation-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-exclamation-fill',
                ],
            ],
            [
                'value' => 'bi bi-envelope-exclamation',
                'label' => 'envelope-exclamation',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-exclamation',
                ],
            ],
            [
                'value' => 'bi bi-envelope-plus-fill',
                'label' => 'envelope-plus-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-plus-fill',
                ],
            ],
            [
                'value' => 'bi bi-envelope-plus',
                'label' => 'envelope-plus',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-plus',
                ],
            ],
            [
                'value' => 'bi bi-envelope-slash-1',
                'label' => 'envelope-slash-1',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-slash-1',
                ],
            ],
            [
                'value' => 'bi bi-envelope-slash-fill',
                'label' => 'envelope-slash-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-slash-fill',
                ],
            ],
            [
                'value' => 'bi bi-envelope-slash',
                'label' => 'envelope-slash',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-slash',
                ],
            ],
            [
                'value' => 'bi bi-envelope-x-1',
                'label' => 'envelope-x-1',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-x-1',
                ],
            ],
            [
                'value' => 'bi bi-envelope-x-fill',
                'label' => 'envelope-x-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-x-fill',
                ],
            ],
            [
                'value' => 'bi bi-envelope-x',
                'label' => 'envelope-x',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-x',
                ],
            ],
            [
                'value' => 'bi bi-explicit-fill',
                'label' => 'explicit-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-explicit-fill',
                ],
            ],
            [
                'value' => 'bi bi-explicit',
                'label' => 'explicit',
                'attributes' => [
                    'data-icon' => 'bi bi-explicit',
                ],
            ],
            [
                'value' => 'bi bi-git',
                'label' => 'git',
                'attributes' => [
                    'data-icon' => 'bi bi-git',
                ],
            ],
            [
                'value' => 'bi bi-infinity',
                'label' => 'infinity',
                'attributes' => [
                    'data-icon' => 'bi bi-infinity',
                ],
            ],
            [
                'value' => 'bi bi-list-columns-reverse',
                'label' => 'list-columns-reverse',
                'attributes' => [
                    'data-icon' => 'bi bi-list-columns-reverse',
                ],
            ],
            [
                'value' => 'bi bi-list-columns',
                'label' => 'list-columns',
                'attributes' => [
                    'data-icon' => 'bi bi-list-columns',
                ],
            ],
            [
                'value' => 'bi bi-meta',
                'label' => 'meta',
                'attributes' => [
                    'data-icon' => 'bi bi-meta',
                ],
            ],
            [
                'value' => 'bi bi-mortorboard-fill',
                'label' => 'mortorboard-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-mortorboard-fill',
                ],
            ],
            [
                'value' => 'bi bi-mortorboard',
                'label' => 'mortorboard',
                'attributes' => [
                    'data-icon' => 'bi bi-mortorboard',
                ],
            ],
            [
                'value' => 'bi bi-nintendo-switch',
                'label' => 'nintendo-switch',
                'attributes' => [
                    'data-icon' => 'bi bi-nintendo-switch',
                ],
            ],
            [
                'value' => 'bi bi-pc-display-horizontal',
                'label' => 'pc-display-horizontal',
                'attributes' => [
                    'data-icon' => 'bi bi-pc-display-horizontal',
                ],
            ],
            [
                'value' => 'bi bi-pc-display',
                'label' => 'pc-display',
                'attributes' => [
                    'data-icon' => 'bi bi-pc-display',
                ],
            ],
            [
                'value' => 'bi bi-pc-horizontal',
                'label' => 'pc-horizontal',
                'attributes' => [
                    'data-icon' => 'bi bi-pc-horizontal',
                ],
            ],
            [
                'value' => 'bi bi-pc',
                'label' => 'pc',
                'attributes' => [
                    'data-icon' => 'bi bi-pc',
                ],
            ],
            [
                'value' => 'bi bi-playstation',
                'label' => 'playstation',
                'attributes' => [
                    'data-icon' => 'bi bi-playstation',
                ],
            ],
            [
                'value' => 'bi bi-plus-slash-minus',
                'label' => 'plus-slash-minus',
                'attributes' => [
                    'data-icon' => 'bi bi-plus-slash-minus',
                ],
            ],
            [
                'value' => 'bi bi-projector-fill',
                'label' => 'projector-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-projector-fill',
                ],
            ],
            [
                'value' => 'bi bi-projector',
                'label' => 'projector',
                'attributes' => [
                    'data-icon' => 'bi bi-projector',
                ],
            ],
            [
                'value' => 'bi bi-qr-code-scan',
                'label' => 'qr-code-scan',
                'attributes' => [
                    'data-icon' => 'bi bi-qr-code-scan',
                ],
            ],
            [
                'value' => 'bi bi-qr-code',
                'label' => 'qr-code',
                'attributes' => [
                    'data-icon' => 'bi bi-qr-code',
                ],
            ],
            [
                'value' => 'bi bi-quora',
                'label' => 'quora',
                'attributes' => [
                    'data-icon' => 'bi bi-quora',
                ],
            ],
            [
                'value' => 'bi bi-quote',
                'label' => 'quote',
                'attributes' => [
                    'data-icon' => 'bi bi-quote',
                ],
            ],
            [
                'value' => 'bi bi-robot',
                'label' => 'robot',
                'attributes' => [
                    'data-icon' => 'bi bi-robot',
                ],
            ],
            [
                'value' => 'bi bi-send-check-fill',
                'label' => 'send-check-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-send-check-fill',
                ],
            ],
            [
                'value' => 'bi bi-send-check',
                'label' => 'send-check',
                'attributes' => [
                    'data-icon' => 'bi bi-send-check',
                ],
            ],
            [
                'value' => 'bi bi-send-dash-fill',
                'label' => 'send-dash-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-send-dash-fill',
                ],
            ],
            [
                'value' => 'bi bi-send-dash',
                'label' => 'send-dash',
                'attributes' => [
                    'data-icon' => 'bi bi-send-dash',
                ],
            ],
            [
                'value' => 'bi bi-send-exclamation-1',
                'label' => 'send-exclamation-1',
                'attributes' => [
                    'data-icon' => 'bi bi-send-exclamation-1',
                ],
            ],
            [
                'value' => 'bi bi-send-exclamation-fill',
                'label' => 'send-exclamation-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-send-exclamation-fill',
                ],
            ],
            [
                'value' => 'bi bi-send-exclamation',
                'label' => 'send-exclamation',
                'attributes' => [
                    'data-icon' => 'bi bi-send-exclamation',
                ],
            ],
            [
                'value' => 'bi bi-send-fill',
                'label' => 'send-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-send-fill',
                ],
            ],
            [
                'value' => 'bi bi-send-plus-fill',
                'label' => 'send-plus-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-send-plus-fill',
                ],
            ],
            [
                'value' => 'bi bi-send-plus',
                'label' => 'send-plus',
                'attributes' => [
                    'data-icon' => 'bi bi-send-plus',
                ],
            ],
            [
                'value' => 'bi bi-send-slash-fill',
                'label' => 'send-slash-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-send-slash-fill',
                ],
            ],
            [
                'value' => 'bi bi-send-slash',
                'label' => 'send-slash',
                'attributes' => [
                    'data-icon' => 'bi bi-send-slash',
                ],
            ],
            [
                'value' => 'bi bi-send-x-fill',
                'label' => 'send-x-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-send-x-fill',
                ],
            ],
            [
                'value' => 'bi bi-send-x',
                'label' => 'send-x',
                'attributes' => [
                    'data-icon' => 'bi bi-send-x',
                ],
            ],
            [
                'value' => 'bi bi-send',
                'label' => 'send',
                'attributes' => [
                    'data-icon' => 'bi bi-send',
                ],
            ],
            [
                'value' => 'bi bi-steam',
                'label' => 'steam',
                'attributes' => [
                    'data-icon' => 'bi bi-steam',
                ],
            ],
            [
                'value' => 'bi bi-terminal-dash-1',
                'label' => 'terminal-dash-1',
                'attributes' => [
                    'data-icon' => 'bi bi-terminal-dash-1',
                ],
            ],
            [
                'value' => 'bi bi-terminal-dash',
                'label' => 'terminal-dash',
                'attributes' => [
                    'data-icon' => 'bi bi-terminal-dash',
                ],
            ],
            [
                'value' => 'bi bi-terminal-plus',
                'label' => 'terminal-plus',
                'attributes' => [
                    'data-icon' => 'bi bi-terminal-plus',
                ],
            ],
            [
                'value' => 'bi bi-terminal-split',
                'label' => 'terminal-split',
                'attributes' => [
                    'data-icon' => 'bi bi-terminal-split',
                ],
            ],
            [
                'value' => 'bi bi-ticket-detailed-fill',
                'label' => 'ticket-detailed-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-ticket-detailed-fill',
                ],
            ],
            [
                'value' => 'bi bi-ticket-detailed',
                'label' => 'ticket-detailed',
                'attributes' => [
                    'data-icon' => 'bi bi-ticket-detailed',
                ],
            ],
            [
                'value' => 'bi bi-ticket-fill',
                'label' => 'ticket-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-ticket-fill',
                ],
            ],
            [
                'value' => 'bi bi-ticket-perforated-fill',
                'label' => 'ticket-perforated-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-ticket-perforated-fill',
                ],
            ],
            [
                'value' => 'bi bi-ticket-perforated',
                'label' => 'ticket-perforated',
                'attributes' => [
                    'data-icon' => 'bi bi-ticket-perforated',
                ],
            ],
            [
                'value' => 'bi bi-ticket',
                'label' => 'ticket',
                'attributes' => [
                    'data-icon' => 'bi bi-ticket',
                ],
            ],
            [
                'value' => 'bi bi-tiktok',
                'label' => 'tiktok',
                'attributes' => [
                    'data-icon' => 'bi bi-tiktok',
                ],
            ],
            [
                'value' => 'bi bi-window-dash',
                'label' => 'window-dash',
                'attributes' => [
                    'data-icon' => 'bi bi-window-dash',
                ],
            ],
            [
                'value' => 'bi bi-window-desktop',
                'label' => 'window-desktop',
                'attributes' => [
                    'data-icon' => 'bi bi-window-desktop',
                ],
            ],
            [
                'value' => 'bi bi-window-fullscreen',
                'label' => 'window-fullscreen',
                'attributes' => [
                    'data-icon' => 'bi bi-window-fullscreen',
                ],
            ],
            [
                'value' => 'bi bi-window-plus',
                'label' => 'window-plus',
                'attributes' => [
                    'data-icon' => 'bi bi-window-plus',
                ],
            ],
            [
                'value' => 'bi bi-window-split',
                'label' => 'window-split',
                'attributes' => [
                    'data-icon' => 'bi bi-window-split',
                ],
            ],
            [
                'value' => 'bi bi-window-stack',
                'label' => 'window-stack',
                'attributes' => [
                    'data-icon' => 'bi bi-window-stack',
                ],
            ],
            [
                'value' => 'bi bi-window-x',
                'label' => 'window-x',
                'attributes' => [
                    'data-icon' => 'bi bi-window-x',
                ],
            ],
            [
                'value' => 'bi bi-xbox',
                'label' => 'xbox',
                'attributes' => [
                    'data-icon' => 'bi bi-xbox',
                ],
            ],
            [
                'value' => 'bi bi-ethernet',
                'label' => 'ethernet',
                'attributes' => [
                    'data-icon' => 'bi bi-ethernet',
                ],
            ],
            [
                'value' => 'bi bi-hdmi-fill',
                'label' => 'hdmi-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-hdmi-fill',
                ],
            ],
            [
                'value' => 'bi bi-hdmi',
                'label' => 'hdmi',
                'attributes' => [
                    'data-icon' => 'bi bi-hdmi',
                ],
            ],
            [
                'value' => 'bi bi-usb-c-fill',
                'label' => 'usb-c-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-usb-c-fill',
                ],
            ],
            [
                'value' => 'bi bi-usb-c',
                'label' => 'usb-c',
                'attributes' => [
                    'data-icon' => 'bi bi-usb-c',
                ],
            ],
            [
                'value' => 'bi bi-usb-fill',
                'label' => 'usb-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-usb-fill',
                ],
            ],
            [
                'value' => 'bi bi-usb-plug-fill',
                'label' => 'usb-plug-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-usb-plug-fill',
                ],
            ],
            [
                'value' => 'bi bi-usb-plug',
                'label' => 'usb-plug',
                'attributes' => [
                    'data-icon' => 'bi bi-usb-plug',
                ],
            ],
            [
                'value' => 'bi bi-usb-symbol',
                'label' => 'usb-symbol',
                'attributes' => [
                    'data-icon' => 'bi bi-usb-symbol',
                ],
            ],
            [
                'value' => 'bi bi-usb',
                'label' => 'usb',
                'attributes' => [
                    'data-icon' => 'bi bi-usb',
                ],
            ],
            [
                'value' => 'bi bi-boombox-fill',
                'label' => 'boombox-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-boombox-fill',
                ],
            ],
            [
                'value' => 'bi bi-displayport-1',
                'label' => 'displayport-1',
                'attributes' => [
                    'data-icon' => 'bi bi-displayport-1',
                ],
            ],
            [
                'value' => 'bi bi-displayport',
                'label' => 'displayport',
                'attributes' => [
                    'data-icon' => 'bi bi-displayport',
                ],
            ],
            [
                'value' => 'bi bi-gpu-card',
                'label' => 'gpu-card',
                'attributes' => [
                    'data-icon' => 'bi bi-gpu-card',
                ],
            ],
            [
                'value' => 'bi bi-memory',
                'label' => 'memory',
                'attributes' => [
                    'data-icon' => 'bi bi-memory',
                ],
            ],
            [
                'value' => 'bi bi-modem-fill',
                'label' => 'modem-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-modem-fill',
                ],
            ],
            [
                'value' => 'bi bi-modem',
                'label' => 'modem',
                'attributes' => [
                    'data-icon' => 'bi bi-modem',
                ],
            ],
            [
                'value' => 'bi bi-motherboard-fill',
                'label' => 'motherboard-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-motherboard-fill',
                ],
            ],
            [
                'value' => 'bi bi-motherboard',
                'label' => 'motherboard',
                'attributes' => [
                    'data-icon' => 'bi bi-motherboard',
                ],
            ],
            [
                'value' => 'bi bi-optical-audio-fill',
                'label' => 'optical-audio-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-optical-audio-fill',
                ],
            ],
            [
                'value' => 'bi bi-optical-audio',
                'label' => 'optical-audio',
                'attributes' => [
                    'data-icon' => 'bi bi-optical-audio',
                ],
            ],
            [
                'value' => 'bi bi-pci-card',
                'label' => 'pci-card',
                'attributes' => [
                    'data-icon' => 'bi bi-pci-card',
                ],
            ],
            [
                'value' => 'bi bi-router-fill',
                'label' => 'router-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-router-fill',
                ],
            ],
            [
                'value' => 'bi bi-router',
                'label' => 'router',
                'attributes' => [
                    'data-icon' => 'bi bi-router',
                ],
            ],
            [
                'value' => 'bi bi-ssd-fill',
                'label' => 'ssd-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-ssd-fill',
                ],
            ],
            [
                'value' => 'bi bi-ssd',
                'label' => 'ssd',
                'attributes' => [
                    'data-icon' => 'bi bi-ssd',
                ],
            ],
            [
                'value' => 'bi bi-thunderbolt-fill',
                'label' => 'thunderbolt-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-thunderbolt-fill',
                ],
            ],
            [
                'value' => 'bi bi-thunderbolt',
                'label' => 'thunderbolt',
                'attributes' => [
                    'data-icon' => 'bi bi-thunderbolt',
                ],
            ],
            [
                'value' => 'bi bi-usb-drive-fill',
                'label' => 'usb-drive-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-usb-drive-fill',
                ],
            ],
            [
                'value' => 'bi bi-usb-drive',
                'label' => 'usb-drive',
                'attributes' => [
                    'data-icon' => 'bi bi-usb-drive',
                ],
            ],
            [
                'value' => 'bi bi-usb-micro-fill',
                'label' => 'usb-micro-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-usb-micro-fill',
                ],
            ],
            [
                'value' => 'bi bi-usb-micro',
                'label' => 'usb-micro',
                'attributes' => [
                    'data-icon' => 'bi bi-usb-micro',
                ],
            ],
            [
                'value' => 'bi bi-usb-mini-fill',
                'label' => 'usb-mini-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-usb-mini-fill',
                ],
            ],
            [
                'value' => 'bi bi-usb-mini',
                'label' => 'usb-mini',
                'attributes' => [
                    'data-icon' => 'bi bi-usb-mini',
                ],
            ],
            [
                'value' => 'bi bi-cloud-haze2',
                'label' => 'cloud-haze2',
                'attributes' => [
                    'data-icon' => 'bi bi-cloud-haze2',
                ],
            ],
            [
                'value' => 'bi bi-device-hdd-fill',
                'label' => 'device-hdd-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-device-hdd-fill',
                ],
            ],
            [
                'value' => 'bi bi-device-hdd',
                'label' => 'device-hdd',
                'attributes' => [
                    'data-icon' => 'bi bi-device-hdd',
                ],
            ],
            [
                'value' => 'bi bi-device-ssd-fill',
                'label' => 'device-ssd-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-device-ssd-fill',
                ],
            ],
            [
                'value' => 'bi bi-device-ssd',
                'label' => 'device-ssd',
                'attributes' => [
                    'data-icon' => 'bi bi-device-ssd',
                ],
            ],
            [
                'value' => 'bi bi-displayport-fill',
                'label' => 'displayport-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-displayport-fill',
                ],
            ],
            [
                'value' => 'bi bi-mortarboard-fill',
                'label' => 'mortarboard-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-mortarboard-fill',
                ],
            ],
            [
                'value' => 'bi bi-mortarboard',
                'label' => 'mortarboard',
                'attributes' => [
                    'data-icon' => 'bi bi-mortarboard',
                ],
            ],
            [
                'value' => 'bi bi-terminal-x',
                'label' => 'terminal-x',
                'attributes' => [
                    'data-icon' => 'bi bi-terminal-x',
                ],
            ],
            [
                'value' => 'bi bi-arrow-through-heart-fill',
                'label' => 'arrow-through-heart-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-through-heart-fill',
                ],
            ],
            [
                'value' => 'bi bi-arrow-through-heart',
                'label' => 'arrow-through-heart',
                'attributes' => [
                    'data-icon' => 'bi bi-arrow-through-heart',
                ],
            ],
            [
                'value' => 'bi bi-badge-sd-fill',
                'label' => 'badge-sd-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-badge-sd-fill',
                ],
            ],
            [
                'value' => 'bi bi-badge-sd',
                'label' => 'badge-sd',
                'attributes' => [
                    'data-icon' => 'bi bi-badge-sd',
                ],
            ],
            [
                'value' => 'bi bi-bag-heart-fill',
                'label' => 'bag-heart-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-bag-heart-fill',
                ],
            ],
            [
                'value' => 'bi bi-bag-heart',
                'label' => 'bag-heart',
                'attributes' => [
                    'data-icon' => 'bi bi-bag-heart',
                ],
            ],
            [
                'value' => 'bi bi-balloon-fill',
                'label' => 'balloon-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-balloon-fill',
                ],
            ],
            [
                'value' => 'bi bi-balloon-heart-fill',
                'label' => 'balloon-heart-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-balloon-heart-fill',
                ],
            ],
            [
                'value' => 'bi bi-balloon-heart',
                'label' => 'balloon-heart',
                'attributes' => [
                    'data-icon' => 'bi bi-balloon-heart',
                ],
            ],
            [
                'value' => 'bi bi-balloon',
                'label' => 'balloon',
                'attributes' => [
                    'data-icon' => 'bi bi-balloon',
                ],
            ],
            [
                'value' => 'bi bi-box2-fill',
                'label' => 'box2-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-box2-fill',
                ],
            ],
            [
                'value' => 'bi bi-box2-heart-fill',
                'label' => 'box2-heart-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-box2-heart-fill',
                ],
            ],
            [
                'value' => 'bi bi-box2-heart',
                'label' => 'box2-heart',
                'attributes' => [
                    'data-icon' => 'bi bi-box2-heart',
                ],
            ],
            [
                'value' => 'bi bi-box2',
                'label' => 'box2',
                'attributes' => [
                    'data-icon' => 'bi bi-box2',
                ],
            ],
            [
                'value' => 'bi bi-braces-asterisk',
                'label' => 'braces-asterisk',
                'attributes' => [
                    'data-icon' => 'bi bi-braces-asterisk',
                ],
            ],
            [
                'value' => 'bi bi-calendar-heart-fill',
                'label' => 'calendar-heart-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar-heart-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar-heart',
                'label' => 'calendar-heart',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar-heart',
                ],
            ],
            [
                'value' => 'bi bi-calendar2-heart-fill',
                'label' => 'calendar2-heart-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar2-heart-fill',
                ],
            ],
            [
                'value' => 'bi bi-calendar2-heart',
                'label' => 'calendar2-heart',
                'attributes' => [
                    'data-icon' => 'bi bi-calendar2-heart',
                ],
            ],
            [
                'value' => 'bi bi-chat-heart-fill',
                'label' => 'chat-heart-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-heart-fill',
                ],
            ],
            [
                'value' => 'bi bi-chat-heart',
                'label' => 'chat-heart',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-heart',
                ],
            ],
            [
                'value' => 'bi bi-chat-left-heart-fill',
                'label' => 'chat-left-heart-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-left-heart-fill',
                ],
            ],
            [
                'value' => 'bi bi-chat-left-heart',
                'label' => 'chat-left-heart',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-left-heart',
                ],
            ],
            [
                'value' => 'bi bi-chat-right-heart-fill',
                'label' => 'chat-right-heart-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-right-heart-fill',
                ],
            ],
            [
                'value' => 'bi bi-chat-right-heart',
                'label' => 'chat-right-heart',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-right-heart',
                ],
            ],
            [
                'value' => 'bi bi-chat-square-heart-fill',
                'label' => 'chat-square-heart-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-square-heart-fill',
                ],
            ],
            [
                'value' => 'bi bi-chat-square-heart',
                'label' => 'chat-square-heart',
                'attributes' => [
                    'data-icon' => 'bi bi-chat-square-heart',
                ],
            ],
            [
                'value' => 'bi bi-clipboard-check-fill',
                'label' => 'clipboard-check-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard-check-fill',
                ],
            ],
            [
                'value' => 'bi bi-clipboard-data-fill',
                'label' => 'clipboard-data-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard-data-fill',
                ],
            ],
            [
                'value' => 'bi bi-clipboard-fill',
                'label' => 'clipboard-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard-fill',
                ],
            ],
            [
                'value' => 'bi bi-clipboard-heart-fill',
                'label' => 'clipboard-heart-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard-heart-fill',
                ],
            ],
            [
                'value' => 'bi bi-clipboard-heart',
                'label' => 'clipboard-heart',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard-heart',
                ],
            ],
            [
                'value' => 'bi bi-clipboard-minus-fill',
                'label' => 'clipboard-minus-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard-minus-fill',
                ],
            ],
            [
                'value' => 'bi bi-clipboard-plus-fill',
                'label' => 'clipboard-plus-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard-plus-fill',
                ],
            ],
            [
                'value' => 'bi bi-clipboard-pulse',
                'label' => 'clipboard-pulse',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard-pulse',
                ],
            ],
            [
                'value' => 'bi bi-clipboard-x-fill',
                'label' => 'clipboard-x-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard-x-fill',
                ],
            ],
            [
                'value' => 'bi bi-clipboard2-check-fill',
                'label' => 'clipboard2-check-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard2-check-fill',
                ],
            ],
            [
                'value' => 'bi bi-clipboard2-check',
                'label' => 'clipboard2-check',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard2-check',
                ],
            ],
            [
                'value' => 'bi bi-clipboard2-data-fill',
                'label' => 'clipboard2-data-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard2-data-fill',
                ],
            ],
            [
                'value' => 'bi bi-clipboard2-data',
                'label' => 'clipboard2-data',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard2-data',
                ],
            ],
            [
                'value' => 'bi bi-clipboard2-fill',
                'label' => 'clipboard2-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard2-fill',
                ],
            ],
            [
                'value' => 'bi bi-clipboard2-heart-fill',
                'label' => 'clipboard2-heart-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard2-heart-fill',
                ],
            ],
            [
                'value' => 'bi bi-clipboard2-heart',
                'label' => 'clipboard2-heart',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard2-heart',
                ],
            ],
            [
                'value' => 'bi bi-clipboard2-minus-fill',
                'label' => 'clipboard2-minus-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard2-minus-fill',
                ],
            ],
            [
                'value' => 'bi bi-clipboard2-minus',
                'label' => 'clipboard2-minus',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard2-minus',
                ],
            ],
            [
                'value' => 'bi bi-clipboard2-plus-fill',
                'label' => 'clipboard2-plus-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard2-plus-fill',
                ],
            ],
            [
                'value' => 'bi bi-clipboard2-plus',
                'label' => 'clipboard2-plus',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard2-plus',
                ],
            ],
            [
                'value' => 'bi bi-clipboard2-pulse-fill',
                'label' => 'clipboard2-pulse-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard2-pulse-fill',
                ],
            ],
            [
                'value' => 'bi bi-clipboard2-pulse',
                'label' => 'clipboard2-pulse',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard2-pulse',
                ],
            ],
            [
                'value' => 'bi bi-clipboard2-x-fill',
                'label' => 'clipboard2-x-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard2-x-fill',
                ],
            ],
            [
                'value' => 'bi bi-clipboard2-x',
                'label' => 'clipboard2-x',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard2-x',
                ],
            ],
            [
                'value' => 'bi bi-clipboard2',
                'label' => 'clipboard2',
                'attributes' => [
                    'data-icon' => 'bi bi-clipboard2',
                ],
            ],
            [
                'value' => 'bi bi-emoji-kiss-fill',
                'label' => 'emoji-kiss-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-emoji-kiss-fill',
                ],
            ],
            [
                'value' => 'bi bi-emoji-kiss',
                'label' => 'emoji-kiss',
                'attributes' => [
                    'data-icon' => 'bi bi-emoji-kiss',
                ],
            ],
            [
                'value' => 'bi bi-envelope-heart-fill',
                'label' => 'envelope-heart-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-heart-fill',
                ],
            ],
            [
                'value' => 'bi bi-envelope-heart',
                'label' => 'envelope-heart',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-heart',
                ],
            ],
            [
                'value' => 'bi bi-envelope-open-heart-fill',
                'label' => 'envelope-open-heart-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-open-heart-fill',
                ],
            ],
            [
                'value' => 'bi bi-envelope-open-heart',
                'label' => 'envelope-open-heart',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-open-heart',
                ],
            ],
            [
                'value' => 'bi bi-envelope-paper-fill',
                'label' => 'envelope-paper-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-paper-fill',
                ],
            ],
            [
                'value' => 'bi bi-envelope-paper-heart-fill',
                'label' => 'envelope-paper-heart-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-paper-heart-fill',
                ],
            ],
            [
                'value' => 'bi bi-envelope-paper-heart',
                'label' => 'envelope-paper-heart',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-paper-heart',
                ],
            ],
            [
                'value' => 'bi bi-envelope-paper',
                'label' => 'envelope-paper',
                'attributes' => [
                    'data-icon' => 'bi bi-envelope-paper',
                ],
            ],
            [
                'value' => 'bi bi-filetype-aac',
                'label' => 'filetype-aac',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-aac',
                ],
            ],
            [
                'value' => 'bi bi-filetype-ai',
                'label' => 'filetype-ai',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-ai',
                ],
            ],
            [
                'value' => 'bi bi-filetype-bmp',
                'label' => 'filetype-bmp',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-bmp',
                ],
            ],
            [
                'value' => 'bi bi-filetype-cs',
                'label' => 'filetype-cs',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-cs',
                ],
            ],
            [
                'value' => 'bi bi-filetype-css',
                'label' => 'filetype-css',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-css',
                ],
            ],
            [
                'value' => 'bi bi-filetype-csv',
                'label' => 'filetype-csv',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-csv',
                ],
            ],
            [
                'value' => 'bi bi-filetype-doc',
                'label' => 'filetype-doc',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-doc',
                ],
            ],
            [
                'value' => 'bi bi-filetype-docx',
                'label' => 'filetype-docx',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-docx',
                ],
            ],
            [
                'value' => 'bi bi-filetype-exe',
                'label' => 'filetype-exe',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-exe',
                ],
            ],
            [
                'value' => 'bi bi-filetype-gif',
                'label' => 'filetype-gif',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-gif',
                ],
            ],
            [
                'value' => 'bi bi-filetype-heic',
                'label' => 'filetype-heic',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-heic',
                ],
            ],
            [
                'value' => 'bi bi-filetype-html',
                'label' => 'filetype-html',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-html',
                ],
            ],
            [
                'value' => 'bi bi-filetype-java',
                'label' => 'filetype-java',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-java',
                ],
            ],
            [
                'value' => 'bi bi-filetype-jpg',
                'label' => 'filetype-jpg',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-jpg',
                ],
            ],
            [
                'value' => 'bi bi-filetype-js',
                'label' => 'filetype-js',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-js',
                ],
            ],
            [
                'value' => 'bi bi-filetype-jsx',
                'label' => 'filetype-jsx',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-jsx',
                ],
            ],
            [
                'value' => 'bi bi-filetype-key',
                'label' => 'filetype-key',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-key',
                ],
            ],
            [
                'value' => 'bi bi-filetype-m4p',
                'label' => 'filetype-m4p',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-m4p',
                ],
            ],
            [
                'value' => 'bi bi-filetype-md',
                'label' => 'filetype-md',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-md',
                ],
            ],
            [
                'value' => 'bi bi-filetype-mdx',
                'label' => 'filetype-mdx',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-mdx',
                ],
            ],
            [
                'value' => 'bi bi-filetype-mov',
                'label' => 'filetype-mov',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-mov',
                ],
            ],
            [
                'value' => 'bi bi-filetype-mp3',
                'label' => 'filetype-mp3',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-mp3',
                ],
            ],
            [
                'value' => 'bi bi-filetype-mp4',
                'label' => 'filetype-mp4',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-mp4',
                ],
            ],
            [
                'value' => 'bi bi-filetype-otf',
                'label' => 'filetype-otf',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-otf',
                ],
            ],
            [
                'value' => 'bi bi-filetype-pdf',
                'label' => 'filetype-pdf',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-pdf',
                ],
            ],
            [
                'value' => 'bi bi-filetype-php',
                'label' => 'filetype-php',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-php',
                ],
            ],
            [
                'value' => 'bi bi-filetype-png',
                'label' => 'filetype-png',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-png',
                ],
            ],
            [
                'value' => 'bi bi-filetype-ppt-1',
                'label' => 'filetype-ppt-1',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-ppt-1',
                ],
            ],
            [
                'value' => 'bi bi-filetype-ppt',
                'label' => 'filetype-ppt',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-ppt',
                ],
            ],
            [
                'value' => 'bi bi-filetype-psd',
                'label' => 'filetype-psd',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-psd',
                ],
            ],
            [
                'value' => 'bi bi-filetype-py',
                'label' => 'filetype-py',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-py',
                ],
            ],
            [
                'value' => 'bi bi-filetype-raw',
                'label' => 'filetype-raw',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-raw',
                ],
            ],
            [
                'value' => 'bi bi-filetype-rb',
                'label' => 'filetype-rb',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-rb',
                ],
            ],
            [
                'value' => 'bi bi-filetype-sass',
                'label' => 'filetype-sass',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-sass',
                ],
            ],
            [
                'value' => 'bi bi-filetype-scss',
                'label' => 'filetype-scss',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-scss',
                ],
            ],
            [
                'value' => 'bi bi-filetype-sh',
                'label' => 'filetype-sh',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-sh',
                ],
            ],
            [
                'value' => 'bi bi-filetype-svg',
                'label' => 'filetype-svg',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-svg',
                ],
            ],
            [
                'value' => 'bi bi-filetype-tiff',
                'label' => 'filetype-tiff',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-tiff',
                ],
            ],
            [
                'value' => 'bi bi-filetype-tsx',
                'label' => 'filetype-tsx',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-tsx',
                ],
            ],
            [
                'value' => 'bi bi-filetype-ttf',
                'label' => 'filetype-ttf',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-ttf',
                ],
            ],
            [
                'value' => 'bi bi-filetype-txt',
                'label' => 'filetype-txt',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-txt',
                ],
            ],
            [
                'value' => 'bi bi-filetype-wav',
                'label' => 'filetype-wav',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-wav',
                ],
            ],
            [
                'value' => 'bi bi-filetype-woff',
                'label' => 'filetype-woff',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-woff',
                ],
            ],
            [
                'value' => 'bi bi-filetype-xls-1',
                'label' => 'filetype-xls-1',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-xls-1',
                ],
            ],
            [
                'value' => 'bi bi-filetype-xls',
                'label' => 'filetype-xls',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-xls',
                ],
            ],
            [
                'value' => 'bi bi-filetype-xml',
                'label' => 'filetype-xml',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-xml',
                ],
            ],
            [
                'value' => 'bi bi-filetype-yml',
                'label' => 'filetype-yml',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-yml',
                ],
            ],
            [
                'value' => 'bi bi-heart-arrow',
                'label' => 'heart-arrow',
                'attributes' => [
                    'data-icon' => 'bi bi-heart-arrow',
                ],
            ],
            [
                'value' => 'bi bi-heart-pulse-fill',
                'label' => 'heart-pulse-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-heart-pulse-fill',
                ],
            ],
            [
                'value' => 'bi bi-heart-pulse',
                'label' => 'heart-pulse',
                'attributes' => [
                    'data-icon' => 'bi bi-heart-pulse',
                ],
            ],
            [
                'value' => 'bi bi-heartbreak-fill',
                'label' => 'heartbreak-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-heartbreak-fill',
                ],
            ],
            [
                'value' => 'bi bi-heartbreak',
                'label' => 'heartbreak',
                'attributes' => [
                    'data-icon' => 'bi bi-heartbreak',
                ],
            ],
            [
                'value' => 'bi bi-hearts',
                'label' => 'hearts',
                'attributes' => [
                    'data-icon' => 'bi bi-hearts',
                ],
            ],
            [
                'value' => 'bi bi-hospital-fill',
                'label' => 'hospital-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-hospital-fill',
                ],
            ],
            [
                'value' => 'bi bi-hospital',
                'label' => 'hospital',
                'attributes' => [
                    'data-icon' => 'bi bi-hospital',
                ],
            ],
            [
                'value' => 'bi bi-house-heart-fill',
                'label' => 'house-heart-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-house-heart-fill',
                ],
            ],
            [
                'value' => 'bi bi-house-heart',
                'label' => 'house-heart',
                'attributes' => [
                    'data-icon' => 'bi bi-house-heart',
                ],
            ],
            [
                'value' => 'bi bi-incognito',
                'label' => 'incognito',
                'attributes' => [
                    'data-icon' => 'bi bi-incognito',
                ],
            ],
            [
                'value' => 'bi bi-magnet-fill',
                'label' => 'magnet-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-magnet-fill',
                ],
            ],
            [
                'value' => 'bi bi-magnet',
                'label' => 'magnet',
                'attributes' => [
                    'data-icon' => 'bi bi-magnet',
                ],
            ],
            [
                'value' => 'bi bi-person-heart',
                'label' => 'person-heart',
                'attributes' => [
                    'data-icon' => 'bi bi-person-heart',
                ],
            ],
            [
                'value' => 'bi bi-person-hearts',
                'label' => 'person-hearts',
                'attributes' => [
                    'data-icon' => 'bi bi-person-hearts',
                ],
            ],
            [
                'value' => 'bi bi-phone-flip',
                'label' => 'phone-flip',
                'attributes' => [
                    'data-icon' => 'bi bi-phone-flip',
                ],
            ],
            [
                'value' => 'bi bi-plugin',
                'label' => 'plugin',
                'attributes' => [
                    'data-icon' => 'bi bi-plugin',
                ],
            ],
            [
                'value' => 'bi bi-postage-fill',
                'label' => 'postage-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-postage-fill',
                ],
            ],
            [
                'value' => 'bi bi-postage-heart-fill',
                'label' => 'postage-heart-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-postage-heart-fill',
                ],
            ],
            [
                'value' => 'bi bi-postage-heart',
                'label' => 'postage-heart',
                'attributes' => [
                    'data-icon' => 'bi bi-postage-heart',
                ],
            ],
            [
                'value' => 'bi bi-postage',
                'label' => 'postage',
                'attributes' => [
                    'data-icon' => 'bi bi-postage',
                ],
            ],
            [
                'value' => 'bi bi-postcard-fill',
                'label' => 'postcard-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-postcard-fill',
                ],
            ],
            [
                'value' => 'bi bi-postcard-heart-fill',
                'label' => 'postcard-heart-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-postcard-heart-fill',
                ],
            ],
            [
                'value' => 'bi bi-postcard-heart',
                'label' => 'postcard-heart',
                'attributes' => [
                    'data-icon' => 'bi bi-postcard-heart',
                ],
            ],
            [
                'value' => 'bi bi-postcard',
                'label' => 'postcard',
                'attributes' => [
                    'data-icon' => 'bi bi-postcard',
                ],
            ],
            [
                'value' => 'bi bi-search-heart-fill',
                'label' => 'search-heart-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-search-heart-fill',
                ],
            ],
            [
                'value' => 'bi bi-search-heart',
                'label' => 'search-heart',
                'attributes' => [
                    'data-icon' => 'bi bi-search-heart',
                ],
            ],
            [
                'value' => 'bi bi-sliders2-vertical',
                'label' => 'sliders2-vertical',
                'attributes' => [
                    'data-icon' => 'bi bi-sliders2-vertical',
                ],
            ],
            [
                'value' => 'bi bi-sliders2',
                'label' => 'sliders2',
                'attributes' => [
                    'data-icon' => 'bi bi-sliders2',
                ],
            ],
            [
                'value' => 'bi bi-trash3-fill',
                'label' => 'trash3-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-trash3-fill',
                ],
            ],
            [
                'value' => 'bi bi-trash3',
                'label' => 'trash3',
                'attributes' => [
                    'data-icon' => 'bi bi-trash3',
                ],
            ],
            [
                'value' => 'bi bi-valentine',
                'label' => 'valentine',
                'attributes' => [
                    'data-icon' => 'bi bi-valentine',
                ],
            ],
            [
                'value' => 'bi bi-valentine2',
                'label' => 'valentine2',
                'attributes' => [
                    'data-icon' => 'bi bi-valentine2',
                ],
            ],
            [
                'value' => 'bi bi-wrench-adjustable-circle-fill',
                'label' => 'wrench-adjustable-circle-fill',
                'attributes' => [
                    'data-icon' => 'bi bi-wrench-adjustable-circle-fill',
                ],
            ],
            [
                'value' => 'bi bi-wrench-adjustable-circle',
                'label' => 'wrench-adjustable-circle',
                'attributes' => [
                    'data-icon' => 'bi bi-wrench-adjustable-circle',
                ],
            ],
            [
                'value' => 'bi bi-wrench-adjustable',
                'label' => 'wrench-adjustable',
                'attributes' => [
                    'data-icon' => 'bi bi-wrench-adjustable',
                ],
            ],
            [
                'value' => 'bi bi-filetype-json',
                'label' => 'filetype-json',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-json',
                ],
            ],
            [
                'value' => 'bi bi-filetype-pptx',
                'label' => 'filetype-pptx',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-pptx',
                ],
            ],
            [
                'value' => 'bi bi-filetype-xlsx',
                'label' => 'filetype-xlsx',
                'attributes' => [
                    'data-icon' => 'bi bi-filetype-xlsx',
                ],
            ],
        ];

        return $laIcons;
    }
}

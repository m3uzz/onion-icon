<?php

/**
 * This file is part of Onion Library
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionIcon
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-icon
 */

declare(strict_types=1);

namespace OnionIcon;

class awesome
{
    /**
     *
     * @return array
     */
    public static function icons(): array
    {
        $laIcons = [
            [
                'value' => 'fas fa-0',
                'label' => '0',
                'attributes' => [
                    'data-icon' => 'fas fa-0',
                ],
            ],
            [
                'value' => 'fas fa-1',
                'label' => '1',
                'attributes' => [
                    'data-icon' => 'fas fa-1',
                ],
            ],
            [
                'value' => 'fas fa-2',
                'label' => '2',
                'attributes' => [
                    'data-icon' => 'fas fa-2',
                ],
            ],
            [
                'value' => 'fas fa-3',
                'label' => '3',
                'attributes' => [
                    'data-icon' => 'fas fa-3',
                ],
            ],
            [
                'value' => 'fas fa-4',
                'label' => '4',
                'attributes' => [
                    'data-icon' => 'fas fa-4',
                ],
            ],
            [
                'value' => 'fas fa-5',
                'label' => '5',
                'attributes' => [
                    'data-icon' => 'fas fa-5',
                ],
            ],
            [
                'value' => 'fas fa-6',
                'label' => '6',
                'attributes' => [
                    'data-icon' => 'fas fa-6',
                ],
            ],
            [
                'value' => 'fas fa-7',
                'label' => '7',
                'attributes' => [
                    'data-icon' => 'fas fa-7',
                ],
            ],
            [
                'value' => 'fas fa-8',
                'label' => '8',
                'attributes' => [
                    'data-icon' => 'fas fa-8',
                ],
            ],
            [
                'value' => 'fas fa-9',
                'label' => '9',
                'attributes' => [
                    'data-icon' => 'fas fa-9',
                ],
            ],
            [
                'value' => 'fab fa-42-group',
                'label' => '42.group',
                'attributes' => [
                    'data-icon' => 'fab fa-42-group',
                ],
            ],
            [
                'value' => 'fab fa-500px',
                'label' => '500px',
                'attributes' => [
                    'data-icon' => 'fab fa-500px',
                ],
            ],
            [
                'value' => 'fas fa-a',
                'label' => 'A',
                'attributes' => [
                    'data-icon' => 'fas fa-a',
                ],
            ],
            [
                'value' => 'fab fa-accessible-icon',
                'label' => 'Accessible Icon',
                'attributes' => [
                    'data-icon' => 'fab fa-accessible-icon',
                ],
            ],
            [
                'value' => 'fab fa-accusoft',
                'label' => 'Accusoft',
                'attributes' => [
                    'data-icon' => 'fab fa-accusoft',
                ],
            ],
            [
                'value' => 'fas fa-address-book',
                'label' => 'Address Book',
                'attributes' => [
                    'data-icon' => 'fas fa-address-book',
                ],
            ],
            [
                'value' => 'far fa-address-book',
                'label' => 'Address Book',
                'attributes' => [
                    'data-icon' => 'far fa-address-book',
                ],
            ],
            [
                'value' => 'fas fa-address-card',
                'label' => 'Address Card',
                'attributes' => [
                    'data-icon' => 'fas fa-address-card',
                ],
            ],
            [
                'value' => 'far fa-address-card',
                'label' => 'Address Card',
                'attributes' => [
                    'data-icon' => 'far fa-address-card',
                ],
            ],
            [
                'value' => 'fab fa-adn',
                'label' => 'App.net',
                'attributes' => [
                    'data-icon' => 'fab fa-adn',
                ],
            ],
            [
                'value' => 'fab fa-adversal',
                'label' => 'Adversal',
                'attributes' => [
                    'data-icon' => 'fab fa-adversal',
                ],
            ],
            [
                'value' => 'fab fa-affiliatetheme',
                'label' => 'affiliatetheme',
                'attributes' => [
                    'data-icon' => 'fab fa-affiliatetheme',
                ],
            ],
            [
                'value' => 'fab fa-airbnb',
                'label' => 'Airbnb',
                'attributes' => [
                    'data-icon' => 'fab fa-airbnb',
                ],
            ],
            [
                'value' => 'fab fa-algolia',
                'label' => 'Algolia',
                'attributes' => [
                    'data-icon' => 'fab fa-algolia',
                ],
            ],
            [
                'value' => 'fas fa-align-center',
                'label' => 'align-center',
                'attributes' => [
                    'data-icon' => 'fas fa-align-center',
                ],
            ],
            [
                'value' => 'fas fa-align-justify',
                'label' => 'align-justify',
                'attributes' => [
                    'data-icon' => 'fas fa-align-justify',
                ],
            ],
            [
                'value' => 'fas fa-align-left',
                'label' => 'align-left',
                'attributes' => [
                    'data-icon' => 'fas fa-align-left',
                ],
            ],
            [
                'value' => 'fas fa-align-right',
                'label' => 'align-right',
                'attributes' => [
                    'data-icon' => 'fas fa-align-right',
                ],
            ],
            [
                'value' => 'fab fa-alipay',
                'label' => 'Alipay',
                'attributes' => [
                    'data-icon' => 'fab fa-alipay',
                ],
            ],
            [
                'value' => 'fab fa-amazon',
                'label' => 'Amazon',
                'attributes' => [
                    'data-icon' => 'fab fa-amazon',
                ],
            ],
            [
                'value' => 'fab fa-amazon-pay',
                'label' => 'Amazon Pay',
                'attributes' => [
                    'data-icon' => 'fab fa-amazon-pay',
                ],
            ],
            [
                'value' => 'fab fa-amilia',
                'label' => 'Amilia',
                'attributes' => [
                    'data-icon' => 'fab fa-amilia',
                ],
            ],
            [
                'value' => 'fas fa-anchor',
                'label' => 'Anchor',
                'attributes' => [
                    'data-icon' => 'fas fa-anchor',
                ],
            ],
            [
                'value' => 'fas fa-anchor-circle-check',
                'label' => 'Anchor Circle-check',
                'attributes' => [
                    'data-icon' => 'fas fa-anchor-circle-check',
                ],
            ],
            [
                'value' => 'fas fa-anchor-circle-exclamation',
                'label' => 'Anchor Circle-exclamation',
                'attributes' => [
                    'data-icon' => 'fas fa-anchor-circle-exclamation',
                ],
            ],
            [
                'value' => 'fas fa-anchor-circle-xmark',
                'label' => 'Anchor Circle-xmark',
                'attributes' => [
                    'data-icon' => 'fas fa-anchor-circle-xmark',
                ],
            ],
            [
                'value' => 'fas fa-anchor-lock',
                'label' => 'Anchor Lock',
                'attributes' => [
                    'data-icon' => 'fas fa-anchor-lock',
                ],
            ],
            [
                'value' => 'fab fa-android',
                'label' => 'Android',
                'attributes' => [
                    'data-icon' => 'fab fa-android',
                ],
            ],
            [
                'value' => 'fab fa-angellist',
                'label' => 'AngelList',
                'attributes' => [
                    'data-icon' => 'fab fa-angellist',
                ],
            ],
            [
                'value' => 'fas fa-angle-down',
                'label' => 'angle-down',
                'attributes' => [
                    'data-icon' => 'fas fa-angle-down',
                ],
            ],
            [
                'value' => 'fas fa-angle-left',
                'label' => 'angle-left',
                'attributes' => [
                    'data-icon' => 'fas fa-angle-left',
                ],
            ],
            [
                'value' => 'fas fa-angle-right',
                'label' => 'angle-right',
                'attributes' => [
                    'data-icon' => 'fas fa-angle-right',
                ],
            ],
            [
                'value' => 'fas fa-angle-up',
                'label' => 'angle-up',
                'attributes' => [
                    'data-icon' => 'fas fa-angle-up',
                ],
            ],
            [
                'value' => 'fas fa-angles-down',
                'label' => 'Angles down',
                'attributes' => [
                    'data-icon' => 'fas fa-angles-down',
                ],
            ],
            [
                'value' => 'fas fa-angles-left',
                'label' => 'Angles left',
                'attributes' => [
                    'data-icon' => 'fas fa-angles-left',
                ],
            ],
            [
                'value' => 'fas fa-angles-right',
                'label' => 'Angles right',
                'attributes' => [
                    'data-icon' => 'fas fa-angles-right',
                ],
            ],
            [
                'value' => 'fas fa-angles-up',
                'label' => 'Angles up',
                'attributes' => [
                    'data-icon' => 'fas fa-angles-up',
                ],
            ],
            [
                'value' => 'fab fa-angrycreative',
                'label' => 'Angry Creative',
                'attributes' => [
                    'data-icon' => 'fab fa-angrycreative',
                ],
            ],
            [
                'value' => 'fab fa-angular',
                'label' => 'Angular',
                'attributes' => [
                    'data-icon' => 'fab fa-angular',
                ],
            ],
            [
                'value' => 'fas fa-ankh',
                'label' => 'Ankh',
                'attributes' => [
                    'data-icon' => 'fas fa-ankh',
                ],
            ],
            [
                'value' => 'fab fa-app-store',
                'label' => 'App Store',
                'attributes' => [
                    'data-icon' => 'fab fa-app-store',
                ],
            ],
            [
                'value' => 'fab fa-app-store-ios',
                'label' => 'iOS App Store',
                'attributes' => [
                    'data-icon' => 'fab fa-app-store-ios',
                ],
            ],
            [
                'value' => 'fab fa-apper',
                'label' => 'Apper Systems AB',
                'attributes' => [
                    'data-icon' => 'fab fa-apper',
                ],
            ],
            [
                'value' => 'fab fa-apple',
                'label' => 'Apple',
                'attributes' => [
                    'data-icon' => 'fab fa-apple',
                ],
            ],
            [
                'value' => 'fab fa-apple-pay',
                'label' => 'Apple Pay',
                'attributes' => [
                    'data-icon' => 'fab fa-apple-pay',
                ],
            ],
            [
                'value' => 'fas fa-apple-whole',
                'label' => 'Apple whole',
                'attributes' => [
                    'data-icon' => 'fas fa-apple-whole',
                ],
            ],
            [
                'value' => 'fas fa-archway',
                'label' => 'Archway',
                'attributes' => [
                    'data-icon' => 'fas fa-archway',
                ],
            ],
            [
                'value' => 'fas fa-arrow-down',
                'label' => 'Arrow down',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-down',
                ],
            ],
            [
                'value' => 'fas fa-arrow-down-1-9',
                'label' => 'Arrow down 1 9',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-down-1-9',
                ],
            ],
            [
                'value' => 'fas fa-arrow-down-9-1',
                'label' => 'Arrow down 9 1',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-down-9-1',
                ],
            ],
            [
                'value' => 'fas fa-arrow-down-a-z',
                'label' => 'Arrow down a z',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-down-a-z',
                ],
            ],
            [
                'value' => 'fas fa-arrow-down-long',
                'label' => 'Arrow down long',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-down-long',
                ],
            ],
            [
                'value' => 'fas fa-arrow-down-short-wide',
                'label' => 'Arrow down short wide',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-down-short-wide',
                ],
            ],
            [
                'value' => 'fas fa-arrow-down-up-across-line',
                'label' => 'Arrow Down-up-across-line',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-down-up-across-line',
                ],
            ],
            [
                'value' => 'fas fa-arrow-down-up-lock',
                'label' => 'Arrow Down-up-lock',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-down-up-lock',
                ],
            ],
            [
                'value' => 'fas fa-arrow-down-wide-short',
                'label' => 'Arrow down wide short',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-down-wide-short',
                ],
            ],
            [
                'value' => 'fas fa-arrow-down-z-a',
                'label' => 'Arrow down z a',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-down-z-a',
                ],
            ],
            [
                'value' => 'fas fa-arrow-left',
                'label' => 'arrow-left',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-left',
                ],
            ],
            [
                'value' => 'fas fa-arrow-left-long',
                'label' => 'Arrow left long',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-left-long',
                ],
            ],
            [
                'value' => 'fas fa-arrow-pointer',
                'label' => 'Arrow pointer',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-pointer',
                ],
            ],
            [
                'value' => 'fas fa-arrow-right',
                'label' => 'arrow right',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-right',
                ],
            ],
            [
                'value' => 'fas fa-arrow-right-arrow-left',
                'label' => 'Arrow right arrow left',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-right-arrow-left',
                ],
            ],
            [
                'value' => 'fas fa-arrow-right-from-bracket',
                'label' => 'Arrow right from bracket',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-right-from-bracket',
                ],
            ],
            [
                'value' => 'fas fa-arrow-right-long',
                'label' => 'Arrow right long',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-right-long',
                ],
            ],
            [
                'value' => 'fas fa-arrow-right-to-bracket',
                'label' => 'Arrow right to bracket',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-right-to-bracket',
                ],
            ],
            [
                'value' => 'fas fa-arrow-right-to-city',
                'label' => 'Arrow Right-to-city',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-right-to-city',
                ],
            ],
            [
                'value' => 'fas fa-arrow-rotate-left',
                'label' => 'Arrow Rotate Left',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-rotate-left',
                ],
            ],
            [
                'value' => 'fas fa-arrow-rotate-right',
                'label' => 'Arrow Rotate Right',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-rotate-right',
                ],
            ],
            [
                'value' => 'fas fa-arrow-trend-down',
                'label' => 'Arrow trend down',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-trend-down',
                ],
            ],
            [
                'value' => 'fas fa-arrow-trend-up',
                'label' => 'Arrow trend up',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-trend-up',
                ],
            ],
            [
                'value' => 'fas fa-arrow-turn-down',
                'label' => 'Arrow turn down',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-turn-down',
                ],
            ],
            [
                'value' => 'fas fa-arrow-turn-up',
                'label' => 'Arrow turn up',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-turn-up',
                ],
            ],
            [
                'value' => 'fas fa-arrow-up',
                'label' => 'Arrow up',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-up',
                ],
            ],
            [
                'value' => 'fas fa-arrow-up-1-9',
                'label' => 'Arrow up 1 9',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-up-1-9',
                ],
            ],
            [
                'value' => 'fas fa-arrow-up-9-1',
                'label' => 'Arrow up 9 1',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-up-9-1',
                ],
            ],
            [
                'value' => 'fas fa-arrow-up-a-z',
                'label' => 'Arrow up a z',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-up-a-z',
                ],
            ],
            [
                'value' => 'fas fa-arrow-up-from-bracket',
                'label' => 'Arrow up from bracket',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-up-from-bracket',
                ],
            ],
            [
                'value' => 'fas fa-arrow-up-from-ground-water',
                'label' => 'Arrow Up-from-ground-water',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-up-from-ground-water',
                ],
            ],
            [
                'value' => 'fas fa-arrow-up-from-water-pump',
                'label' => 'Arrow Up-from-water-pump',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-up-from-water-pump',
                ],
            ],
            [
                'value' => 'fas fa-arrow-up-long',
                'label' => 'Arrow up long',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-up-long',
                ],
            ],
            [
                'value' => 'fas fa-arrow-up-right-dots',
                'label' => 'Arrow Up-right-dots',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-up-right-dots',
                ],
            ],
            [
                'value' => 'fas fa-arrow-up-right-from-square',
                'label' => 'Arrow up right from square',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-up-right-from-square',
                ],
            ],
            [
                'value' => 'fas fa-arrow-up-short-wide',
                'label' => 'Arrow up short wide',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-up-short-wide',
                ],
            ],
            [
                'value' => 'fas fa-arrow-up-wide-short',
                'label' => 'Arrow up wide short',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-up-wide-short',
                ],
            ],
            [
                'value' => 'fas fa-arrow-up-z-a',
                'label' => 'Arrow up z a',
                'attributes' => [
                    'data-icon' => 'fas fa-arrow-up-z-a',
                ],
            ],
            [
                'value' => 'fas fa-arrows-down-to-line',
                'label' => 'Arrows Down-to-line',
                'attributes' => [
                    'data-icon' => 'fas fa-arrows-down-to-line',
                ],
            ],
            [
                'value' => 'fas fa-arrows-down-to-people',
                'label' => 'Arrows Down-to-people',
                'attributes' => [
                    'data-icon' => 'fas fa-arrows-down-to-people',
                ],
            ],
            [
                'value' => 'fas fa-arrows-left-right',
                'label' => 'Arrows left right',
                'attributes' => [
                    'data-icon' => 'fas fa-arrows-left-right',
                ],
            ],
            [
                'value' => 'fas fa-arrows-left-right-to-line',
                'label' => 'Arrows Left-right-to-line',
                'attributes' => [
                    'data-icon' => 'fas fa-arrows-left-right-to-line',
                ],
            ],
            [
                'value' => 'fas fa-arrows-rotate',
                'label' => 'Arrows rotate',
                'attributes' => [
                    'data-icon' => 'fas fa-arrows-rotate',
                ],
            ],
            [
                'value' => 'fas fa-arrows-spin',
                'label' => 'Arrows Spin',
                'attributes' => [
                    'data-icon' => 'fas fa-arrows-spin',
                ],
            ],
            [
                'value' => 'fas fa-arrows-split-up-and-left',
                'label' => 'Arrows Split-up-and-left',
                'attributes' => [
                    'data-icon' => 'fas fa-arrows-split-up-and-left',
                ],
            ],
            [
                'value' => 'fas fa-arrows-to-circle',
                'label' => 'Arrows To-circle',
                'attributes' => [
                    'data-icon' => 'fas fa-arrows-to-circle',
                ],
            ],
            [
                'value' => 'fas fa-arrows-to-dot',
                'label' => 'Arrows To-dot',
                'attributes' => [
                    'data-icon' => 'fas fa-arrows-to-dot',
                ],
            ],
            [
                'value' => 'fas fa-arrows-to-eye',
                'label' => 'Arrows To-eye',
                'attributes' => [
                    'data-icon' => 'fas fa-arrows-to-eye',
                ],
            ],
            [
                'value' => 'fas fa-arrows-turn-right',
                'label' => 'Arrows Turn-right',
                'attributes' => [
                    'data-icon' => 'fas fa-arrows-turn-right',
                ],
            ],
            [
                'value' => 'fas fa-arrows-turn-to-dots',
                'label' => 'Arrows Turn-to-dots',
                'attributes' => [
                    'data-icon' => 'fas fa-arrows-turn-to-dots',
                ],
            ],
            [
                'value' => 'fas fa-arrows-up-down',
                'label' => 'Arrows up down',
                'attributes' => [
                    'data-icon' => 'fas fa-arrows-up-down',
                ],
            ],
            [
                'value' => 'fas fa-arrows-up-down-left-right',
                'label' => 'Arrows up down left right',
                'attributes' => [
                    'data-icon' => 'fas fa-arrows-up-down-left-right',
                ],
            ],
            [
                'value' => 'fas fa-arrows-up-to-line',
                'label' => 'Arrows Up-to-line',
                'attributes' => [
                    'data-icon' => 'fas fa-arrows-up-to-line',
                ],
            ],
            [
                'value' => 'fab fa-artstation',
                'label' => 'Artstation',
                'attributes' => [
                    'data-icon' => 'fab fa-artstation',
                ],
            ],
            [
                'value' => 'fas fa-asterisk',
                'label' => 'asterisk',
                'attributes' => [
                    'data-icon' => 'fas fa-asterisk',
                ],
            ],
            [
                'value' => 'fab fa-asymmetrik',
                'label' => 'Asymmetrik, Ltd.',
                'attributes' => [
                    'data-icon' => 'fab fa-asymmetrik',
                ],
            ],
            [
                'value' => 'fas fa-at',
                'label' => 'At',
                'attributes' => [
                    'data-icon' => 'fas fa-at',
                ],
            ],
            [
                'value' => 'fab fa-atlassian',
                'label' => 'Atlassian',
                'attributes' => [
                    'data-icon' => 'fab fa-atlassian',
                ],
            ],
            [
                'value' => 'fas fa-atom',
                'label' => 'Atom',
                'attributes' => [
                    'data-icon' => 'fas fa-atom',
                ],
            ],
            [
                'value' => 'fab fa-audible',
                'label' => 'Audible',
                'attributes' => [
                    'data-icon' => 'fab fa-audible',
                ],
            ],
            [
                'value' => 'fas fa-audio-description',
                'label' => 'Rectangle audio description',
                'attributes' => [
                    'data-icon' => 'fas fa-audio-description',
                ],
            ],
            [
                'value' => 'fas fa-austral-sign',
                'label' => 'Austral Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-austral-sign',
                ],
            ],
            [
                'value' => 'fab fa-autoprefixer',
                'label' => 'Autoprefixer',
                'attributes' => [
                    'data-icon' => 'fab fa-autoprefixer',
                ],
            ],
            [
                'value' => 'fab fa-avianex',
                'label' => 'avianex',
                'attributes' => [
                    'data-icon' => 'fab fa-avianex',
                ],
            ],
            [
                'value' => 'fab fa-aviato',
                'label' => 'Aviato',
                'attributes' => [
                    'data-icon' => 'fab fa-aviato',
                ],
            ],
            [
                'value' => 'fas fa-award',
                'label' => 'Award',
                'attributes' => [
                    'data-icon' => 'fas fa-award',
                ],
            ],
            [
                'value' => 'fab fa-aws',
                'label' => 'Amazon Web Services (AWS)',
                'attributes' => [
                    'data-icon' => 'fab fa-aws',
                ],
            ],
            [
                'value' => 'fas fa-b',
                'label' => 'B',
                'attributes' => [
                    'data-icon' => 'fas fa-b',
                ],
            ],
            [
                'value' => 'fas fa-baby',
                'label' => 'Baby',
                'attributes' => [
                    'data-icon' => 'fas fa-baby',
                ],
            ],
            [
                'value' => 'fas fa-baby-carriage',
                'label' => 'Baby Carriage',
                'attributes' => [
                    'data-icon' => 'fas fa-baby-carriage',
                ],
            ],
            [
                'value' => 'fas fa-backward',
                'label' => 'backward',
                'attributes' => [
                    'data-icon' => 'fas fa-backward',
                ],
            ],
            [
                'value' => 'fas fa-backward-fast',
                'label' => 'Backward fast',
                'attributes' => [
                    'data-icon' => 'fas fa-backward-fast',
                ],
            ],
            [
                'value' => 'fas fa-backward-step',
                'label' => 'Backward step',
                'attributes' => [
                    'data-icon' => 'fas fa-backward-step',
                ],
            ],
            [
                'value' => 'fas fa-bacon',
                'label' => 'Bacon',
                'attributes' => [
                    'data-icon' => 'fas fa-bacon',
                ],
            ],
            [
                'value' => 'fas fa-bacteria',
                'label' => 'Bacteria',
                'attributes' => [
                    'data-icon' => 'fas fa-bacteria',
                ],
            ],
            [
                'value' => 'fas fa-bacterium',
                'label' => 'Bacterium',
                'attributes' => [
                    'data-icon' => 'fas fa-bacterium',
                ],
            ],
            [
                'value' => 'fas fa-bag-shopping',
                'label' => 'Bag shopping',
                'attributes' => [
                    'data-icon' => 'fas fa-bag-shopping',
                ],
            ],
            [
                'value' => 'fas fa-bahai',
                'label' => 'Bahá í',
                'attributes' => [
                    'data-icon' => 'fas fa-bahai',
                ],
            ],
            [
                'value' => 'fas fa-baht-sign',
                'label' => 'Baht Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-baht-sign',
                ],
            ],
            [
                'value' => 'fas fa-ban',
                'label' => 'ban',
                'attributes' => [
                    'data-icon' => 'fas fa-ban',
                ],
            ],
            [
                'value' => 'fas fa-ban-smoking',
                'label' => 'Ban smoking',
                'attributes' => [
                    'data-icon' => 'fas fa-ban-smoking',
                ],
            ],
            [
                'value' => 'fas fa-bandage',
                'label' => 'Bandage',
                'attributes' => [
                    'data-icon' => 'fas fa-bandage',
                ],
            ],
            [
                'value' => 'fab fa-bandcamp',
                'label' => 'Bandcamp',
                'attributes' => [
                    'data-icon' => 'fab fa-bandcamp',
                ],
            ],
            [
                'value' => 'fas fa-barcode',
                'label' => 'barcode',
                'attributes' => [
                    'data-icon' => 'fas fa-barcode',
                ],
            ],
            [
                'value' => 'fas fa-bars',
                'label' => 'Bars',
                'attributes' => [
                    'data-icon' => 'fas fa-bars',
                ],
            ],
            [
                'value' => 'fas fa-bars-progress',
                'label' => 'Bars progress',
                'attributes' => [
                    'data-icon' => 'fas fa-bars-progress',
                ],
            ],
            [
                'value' => 'fas fa-bars-staggered',
                'label' => 'Bars staggered',
                'attributes' => [
                    'data-icon' => 'fas fa-bars-staggered',
                ],
            ],
            [
                'value' => 'fas fa-baseball',
                'label' => 'Baseball Ball',
                'attributes' => [
                    'data-icon' => 'fas fa-baseball',
                ],
            ],
            [
                'value' => 'fas fa-baseball-bat-ball',
                'label' => 'Baseball bat ball',
                'attributes' => [
                    'data-icon' => 'fas fa-baseball-bat-ball',
                ],
            ],
            [
                'value' => 'fas fa-basket-shopping',
                'label' => 'Basket shopping',
                'attributes' => [
                    'data-icon' => 'fas fa-basket-shopping',
                ],
            ],
            [
                'value' => 'fas fa-basketball',
                'label' => 'Basketball Ball',
                'attributes' => [
                    'data-icon' => 'fas fa-basketball',
                ],
            ],
            [
                'value' => 'fas fa-bath',
                'label' => 'Bath',
                'attributes' => [
                    'data-icon' => 'fas fa-bath',
                ],
            ],
            [
                'value' => 'fas fa-battery-empty',
                'label' => 'Battery Empty',
                'attributes' => [
                    'data-icon' => 'fas fa-battery-empty',
                ],
            ],
            [
                'value' => 'fas fa-battery-full',
                'label' => 'Battery Full',
                'attributes' => [
                    'data-icon' => 'fas fa-battery-full',
                ],
            ],
            [
                'value' => 'fas fa-battery-half',
                'label' => 'Battery 1/2 Full',
                'attributes' => [
                    'data-icon' => 'fas fa-battery-half',
                ],
            ],
            [
                'value' => 'fas fa-battery-quarter',
                'label' => 'Battery 1/4 Full',
                'attributes' => [
                    'data-icon' => 'fas fa-battery-quarter',
                ],
            ],
            [
                'value' => 'fas fa-battery-three-quarters',
                'label' => 'Battery 3/4 Full',
                'attributes' => [
                    'data-icon' => 'fas fa-battery-three-quarters',
                ],
            ],
            [
                'value' => 'fab fa-battle-net',
                'label' => 'Battle.net',
                'attributes' => [
                    'data-icon' => 'fab fa-battle-net',
                ],
            ],
            [
                'value' => 'fas fa-bed',
                'label' => 'Bed',
                'attributes' => [
                    'data-icon' => 'fas fa-bed',
                ],
            ],
            [
                'value' => 'fas fa-bed-pulse',
                'label' => 'Bed pulse',
                'attributes' => [
                    'data-icon' => 'fas fa-bed-pulse',
                ],
            ],
            [
                'value' => 'fas fa-beer-mug-empty',
                'label' => 'Beer mug empty',
                'attributes' => [
                    'data-icon' => 'fas fa-beer-mug-empty',
                ],
            ],
            [
                'value' => 'fab fa-behance',
                'label' => 'Behance',
                'attributes' => [
                    'data-icon' => 'fab fa-behance',
                ],
            ],
            [
                'value' => 'fab fa-behance-square',
                'label' => 'Behance Square',
                'attributes' => [
                    'data-icon' => 'fab fa-behance-square',
                ],
            ],
            [
                'value' => 'fas fa-bell',
                'label' => 'bell',
                'attributes' => [
                    'data-icon' => 'fas fa-bell',
                ],
            ],
            [
                'value' => 'far fa-bell',
                'label' => 'bell',
                'attributes' => [
                    'data-icon' => 'far fa-bell',
                ],
            ],
            [
                'value' => 'fas fa-bell-concierge',
                'label' => 'Bell concierge',
                'attributes' => [
                    'data-icon' => 'fas fa-bell-concierge',
                ],
            ],
            [
                'value' => 'fas fa-bell-slash',
                'label' => 'Bell Slash',
                'attributes' => [
                    'data-icon' => 'fas fa-bell-slash',
                ],
            ],
            [
                'value' => 'far fa-bell-slash',
                'label' => 'Bell Slash',
                'attributes' => [
                    'data-icon' => 'far fa-bell-slash',
                ],
            ],
            [
                'value' => 'fas fa-bezier-curve',
                'label' => 'Bezier Curve',
                'attributes' => [
                    'data-icon' => 'fas fa-bezier-curve',
                ],
            ],
            [
                'value' => 'fas fa-bicycle',
                'label' => 'Bicycle',
                'attributes' => [
                    'data-icon' => 'fas fa-bicycle',
                ],
            ],
            [
                'value' => 'fab fa-bilibili',
                'label' => 'Bilibili',
                'attributes' => [
                    'data-icon' => 'fab fa-bilibili',
                ],
            ],
            [
                'value' => 'fab fa-bimobject',
                'label' => 'BIMobject',
                'attributes' => [
                    'data-icon' => 'fab fa-bimobject',
                ],
            ],
            [
                'value' => 'fas fa-binoculars',
                'label' => 'Binoculars',
                'attributes' => [
                    'data-icon' => 'fas fa-binoculars',
                ],
            ],
            [
                'value' => 'fas fa-biohazard',
                'label' => 'Biohazard',
                'attributes' => [
                    'data-icon' => 'fas fa-biohazard',
                ],
            ],
            [
                'value' => 'fab fa-bitbucket',
                'label' => 'Bitbucket',
                'attributes' => [
                    'data-icon' => 'fab fa-bitbucket',
                ],
            ],
            [
                'value' => 'fab fa-bitcoin',
                'label' => 'Bitcoin',
                'attributes' => [
                    'data-icon' => 'fab fa-bitcoin',
                ],
            ],
            [
                'value' => 'fas fa-bitcoin-sign',
                'label' => 'Bitcoin Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-bitcoin-sign',
                ],
            ],
            [
                'value' => 'fab fa-bity',
                'label' => 'Bity',
                'attributes' => [
                    'data-icon' => 'fab fa-bity',
                ],
            ],
            [
                'value' => 'fab fa-black-tie',
                'label' => 'Font Awesome Black Tie',
                'attributes' => [
                    'data-icon' => 'fab fa-black-tie',
                ],
            ],
            [
                'value' => 'fab fa-blackberry',
                'label' => 'BlackBerry',
                'attributes' => [
                    'data-icon' => 'fab fa-blackberry',
                ],
            ],
            [
                'value' => 'fas fa-blender',
                'label' => 'Blender',
                'attributes' => [
                    'data-icon' => 'fas fa-blender',
                ],
            ],
            [
                'value' => 'fas fa-blender-phone',
                'label' => 'Blender Phone',
                'attributes' => [
                    'data-icon' => 'fas fa-blender-phone',
                ],
            ],
            [
                'value' => 'fas fa-blog',
                'label' => 'Blog',
                'attributes' => [
                    'data-icon' => 'fas fa-blog',
                ],
            ],
            [
                'value' => 'fab fa-blogger',
                'label' => 'Blogger',
                'attributes' => [
                    'data-icon' => 'fab fa-blogger',
                ],
            ],
            [
                'value' => 'fab fa-blogger-b',
                'label' => 'Blogger B',
                'attributes' => [
                    'data-icon' => 'fab fa-blogger-b',
                ],
            ],
            [
                'value' => 'fab fa-bluetooth',
                'label' => 'Bluetooth',
                'attributes' => [
                    'data-icon' => 'fab fa-bluetooth',
                ],
            ],
            [
                'value' => 'fab fa-bluetooth-b',
                'label' => 'Bluetooth',
                'attributes' => [
                    'data-icon' => 'fab fa-bluetooth-b',
                ],
            ],
            [
                'value' => 'fas fa-bold',
                'label' => 'bold',
                'attributes' => [
                    'data-icon' => 'fas fa-bold',
                ],
            ],
            [
                'value' => 'fas fa-bolt',
                'label' => 'Bolt',
                'attributes' => [
                    'data-icon' => 'fas fa-bolt',
                ],
            ],
            [
                'value' => 'fas fa-bolt-lightning',
                'label' => 'Lightning Bolt',
                'attributes' => [
                    'data-icon' => 'fas fa-bolt-lightning',
                ],
            ],
            [
                'value' => 'fas fa-bomb',
                'label' => 'Bomb',
                'attributes' => [
                    'data-icon' => 'fas fa-bomb',
                ],
            ],
            [
                'value' => 'fas fa-bone',
                'label' => 'Bone',
                'attributes' => [
                    'data-icon' => 'fas fa-bone',
                ],
            ],
            [
                'value' => 'fas fa-bong',
                'label' => 'Bong',
                'attributes' => [
                    'data-icon' => 'fas fa-bong',
                ],
            ],
            [
                'value' => 'fas fa-book',
                'label' => 'book',
                'attributes' => [
                    'data-icon' => 'fas fa-book',
                ],
            ],
            [
                'value' => 'fas fa-book-atlas',
                'label' => 'Book atlas',
                'attributes' => [
                    'data-icon' => 'fas fa-book-atlas',
                ],
            ],
            [
                'value' => 'fas fa-book-bible',
                'label' => 'Book bible',
                'attributes' => [
                    'data-icon' => 'fas fa-book-bible',
                ],
            ],
            [
                'value' => 'fas fa-book-bookmark',
                'label' => 'Book Bookmark',
                'attributes' => [
                    'data-icon' => 'fas fa-book-bookmark',
                ],
            ],
            [
                'value' => 'fas fa-book-journal-whills',
                'label' => 'Book journal whills',
                'attributes' => [
                    'data-icon' => 'fas fa-book-journal-whills',
                ],
            ],
            [
                'value' => 'fas fa-book-medical',
                'label' => 'Medical Book',
                'attributes' => [
                    'data-icon' => 'fas fa-book-medical',
                ],
            ],
            [
                'value' => 'fas fa-book-open',
                'label' => 'Book Open',
                'attributes' => [
                    'data-icon' => 'fas fa-book-open',
                ],
            ],
            [
                'value' => 'fas fa-book-open-reader',
                'label' => 'Book open reader',
                'attributes' => [
                    'data-icon' => 'fas fa-book-open-reader',
                ],
            ],
            [
                'value' => 'fas fa-book-quran',
                'label' => 'Book quran',
                'attributes' => [
                    'data-icon' => 'fas fa-book-quran',
                ],
            ],
            [
                'value' => 'fas fa-book-skull',
                'label' => 'Book skull',
                'attributes' => [
                    'data-icon' => 'fas fa-book-skull',
                ],
            ],
            [
                'value' => 'fas fa-bookmark',
                'label' => 'bookmark',
                'attributes' => [
                    'data-icon' => 'fas fa-bookmark',
                ],
            ],
            [
                'value' => 'far fa-bookmark',
                'label' => 'bookmark',
                'attributes' => [
                    'data-icon' => 'far fa-bookmark',
                ],
            ],
            [
                'value' => 'fab fa-bootstrap',
                'label' => 'Bootstrap',
                'attributes' => [
                    'data-icon' => 'fab fa-bootstrap',
                ],
            ],
            [
                'value' => 'fas fa-border-all',
                'label' => 'Border All',
                'attributes' => [
                    'data-icon' => 'fas fa-border-all',
                ],
            ],
            [
                'value' => 'fas fa-border-none',
                'label' => 'Border None',
                'attributes' => [
                    'data-icon' => 'fas fa-border-none',
                ],
            ],
            [
                'value' => 'fas fa-border-top-left',
                'label' => 'Border top left',
                'attributes' => [
                    'data-icon' => 'fas fa-border-top-left',
                ],
            ],
            [
                'value' => 'fas fa-bore-hole',
                'label' => 'Bore Hole',
                'attributes' => [
                    'data-icon' => 'fas fa-bore-hole',
                ],
            ],
            [
                'value' => 'fab fa-bots',
                'label' => 'Bots',
                'attributes' => [
                    'data-icon' => 'fab fa-bots',
                ],
            ],
            [
                'value' => 'fas fa-bottle-droplet',
                'label' => 'Bottle Droplet',
                'attributes' => [
                    'data-icon' => 'fas fa-bottle-droplet',
                ],
            ],
            [
                'value' => 'fas fa-bottle-water',
                'label' => 'Bottle Water',
                'attributes' => [
                    'data-icon' => 'fas fa-bottle-water',
                ],
            ],
            [
                'value' => 'fas fa-bowl-food',
                'label' => 'Bowl Food',
                'attributes' => [
                    'data-icon' => 'fas fa-bowl-food',
                ],
            ],
            [
                'value' => 'fas fa-bowl-rice',
                'label' => 'Bowl Rice',
                'attributes' => [
                    'data-icon' => 'fas fa-bowl-rice',
                ],
            ],
            [
                'value' => 'fas fa-bowling-ball',
                'label' => 'Bowling Ball',
                'attributes' => [
                    'data-icon' => 'fas fa-bowling-ball',
                ],
            ],
            [
                'value' => 'fas fa-box',
                'label' => 'Box',
                'attributes' => [
                    'data-icon' => 'fas fa-box',
                ],
            ],
            [
                'value' => 'fas fa-box-archive',
                'label' => 'Box archive',
                'attributes' => [
                    'data-icon' => 'fas fa-box-archive',
                ],
            ],
            [
                'value' => 'fas fa-box-open',
                'label' => 'Box Open',
                'attributes' => [
                    'data-icon' => 'fas fa-box-open',
                ],
            ],
            [
                'value' => 'fas fa-box-tissue',
                'label' => 'Tissue Box',
                'attributes' => [
                    'data-icon' => 'fas fa-box-tissue',
                ],
            ],
            [
                'value' => 'fas fa-boxes-packing',
                'label' => 'Boxes Packing',
                'attributes' => [
                    'data-icon' => 'fas fa-boxes-packing',
                ],
            ],
            [
                'value' => 'fas fa-boxes-stacked',
                'label' => 'Boxes stacked',
                'attributes' => [
                    'data-icon' => 'fas fa-boxes-stacked',
                ],
            ],
            [
                'value' => 'fas fa-braille',
                'label' => 'Braille',
                'attributes' => [
                    'data-icon' => 'fas fa-braille',
                ],
            ],
            [
                'value' => 'fas fa-brain',
                'label' => 'Brain',
                'attributes' => [
                    'data-icon' => 'fas fa-brain',
                ],
            ],
            [
                'value' => 'fas fa-brazilian-real-sign',
                'label' => 'Brazilian Real Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-brazilian-real-sign',
                ],
            ],
            [
                'value' => 'fas fa-bread-slice',
                'label' => 'Bread Slice',
                'attributes' => [
                    'data-icon' => 'fas fa-bread-slice',
                ],
            ],
            [
                'value' => 'fas fa-bridge',
                'label' => 'Bridge',
                'attributes' => [
                    'data-icon' => 'fas fa-bridge',
                ],
            ],
            [
                'value' => 'fas fa-bridge-circle-check',
                'label' => 'Bridge Circle-check',
                'attributes' => [
                    'data-icon' => 'fas fa-bridge-circle-check',
                ],
            ],
            [
                'value' => 'fas fa-bridge-circle-exclamation',
                'label' => 'Bridge Circle-exclamation',
                'attributes' => [
                    'data-icon' => 'fas fa-bridge-circle-exclamation',
                ],
            ],
            [
                'value' => 'fas fa-bridge-circle-xmark',
                'label' => 'Bridge Circle-xmark',
                'attributes' => [
                    'data-icon' => 'fas fa-bridge-circle-xmark',
                ],
            ],
            [
                'value' => 'fas fa-bridge-lock',
                'label' => 'Bridge Lock',
                'attributes' => [
                    'data-icon' => 'fas fa-bridge-lock',
                ],
            ],
            [
                'value' => 'fas fa-bridge-water',
                'label' => 'Bridge Water',
                'attributes' => [
                    'data-icon' => 'fas fa-bridge-water',
                ],
            ],
            [
                'value' => 'fas fa-briefcase',
                'label' => 'Briefcase',
                'attributes' => [
                    'data-icon' => 'fas fa-briefcase',
                ],
            ],
            [
                'value' => 'fas fa-briefcase-medical',
                'label' => 'Medical Briefcase',
                'attributes' => [
                    'data-icon' => 'fas fa-briefcase-medical',
                ],
            ],
            [
                'value' => 'fas fa-broom',
                'label' => 'Broom',
                'attributes' => [
                    'data-icon' => 'fas fa-broom',
                ],
            ],
            [
                'value' => 'fas fa-broom-ball',
                'label' => 'Broom and Ball',
                'attributes' => [
                    'data-icon' => 'fas fa-broom-ball',
                ],
            ],
            [
                'value' => 'fas fa-brush',
                'label' => 'Brush',
                'attributes' => [
                    'data-icon' => 'fas fa-brush',
                ],
            ],
            [
                'value' => 'fab fa-btc',
                'label' => 'BTC',
                'attributes' => [
                    'data-icon' => 'fab fa-btc',
                ],
            ],
            [
                'value' => 'fas fa-bucket',
                'label' => 'Bucket',
                'attributes' => [
                    'data-icon' => 'fas fa-bucket',
                ],
            ],
            [
                'value' => 'fab fa-buffer',
                'label' => 'Buffer',
                'attributes' => [
                    'data-icon' => 'fab fa-buffer',
                ],
            ],
            [
                'value' => 'fas fa-bug',
                'label' => 'Bug',
                'attributes' => [
                    'data-icon' => 'fas fa-bug',
                ],
            ],
            [
                'value' => 'fas fa-bug-slash',
                'label' => 'Bug Slash',
                'attributes' => [
                    'data-icon' => 'fas fa-bug-slash',
                ],
            ],
            [
                'value' => 'fas fa-bugs',
                'label' => 'Bugs',
                'attributes' => [
                    'data-icon' => 'fas fa-bugs',
                ],
            ],
            [
                'value' => 'fas fa-building',
                'label' => 'Building',
                'attributes' => [
                    'data-icon' => 'fas fa-building',
                ],
            ],
            [
                'value' => 'far fa-building',
                'label' => 'Building',
                'attributes' => [
                    'data-icon' => 'far fa-building',
                ],
            ],
            [
                'value' => 'fas fa-building-circle-arrow-right',
                'label' => 'Building Circle-arrow-right',
                'attributes' => [
                    'data-icon' => 'fas fa-building-circle-arrow-right',
                ],
            ],
            [
                'value' => 'fas fa-building-circle-check',
                'label' => 'Building Circle-check',
                'attributes' => [
                    'data-icon' => 'fas fa-building-circle-check',
                ],
            ],
            [
                'value' => 'fas fa-building-circle-exclamation',
                'label' => 'Building Circle-exclamation',
                'attributes' => [
                    'data-icon' => 'fas fa-building-circle-exclamation',
                ],
            ],
            [
                'value' => 'fas fa-building-circle-xmark',
                'label' => 'Building Circle-xmark',
                'attributes' => [
                    'data-icon' => 'fas fa-building-circle-xmark',
                ],
            ],
            [
                'value' => 'fas fa-building-columns',
                'label' => 'Building with Columns',
                'attributes' => [
                    'data-icon' => 'fas fa-building-columns',
                ],
            ],
            [
                'value' => 'fas fa-building-flag',
                'label' => 'Building Flag',
                'attributes' => [
                    'data-icon' => 'fas fa-building-flag',
                ],
            ],
            [
                'value' => 'fas fa-building-lock',
                'label' => 'Building Lock',
                'attributes' => [
                    'data-icon' => 'fas fa-building-lock',
                ],
            ],
            [
                'value' => 'fas fa-building-ngo',
                'label' => 'Building Ngo',
                'attributes' => [
                    'data-icon' => 'fas fa-building-ngo',
                ],
            ],
            [
                'value' => 'fas fa-building-shield',
                'label' => 'Building Shield',
                'attributes' => [
                    'data-icon' => 'fas fa-building-shield',
                ],
            ],
            [
                'value' => 'fas fa-building-un',
                'label' => 'Building Un',
                'attributes' => [
                    'data-icon' => 'fas fa-building-un',
                ],
            ],
            [
                'value' => 'fas fa-building-user',
                'label' => 'Building User',
                'attributes' => [
                    'data-icon' => 'fas fa-building-user',
                ],
            ],
            [
                'value' => 'fas fa-building-wheat',
                'label' => 'Building Wheat',
                'attributes' => [
                    'data-icon' => 'fas fa-building-wheat',
                ],
            ],
            [
                'value' => 'fas fa-bullhorn',
                'label' => 'bullhorn',
                'attributes' => [
                    'data-icon' => 'fas fa-bullhorn',
                ],
            ],
            [
                'value' => 'fas fa-bullseye',
                'label' => 'Bullseye',
                'attributes' => [
                    'data-icon' => 'fas fa-bullseye',
                ],
            ],
            [
                'value' => 'fas fa-burger',
                'label' => 'Burger',
                'attributes' => [
                    'data-icon' => 'fas fa-burger',
                ],
            ],
            [
                'value' => 'fab fa-buromobelexperte',
                'label' => 'Büromöbel-Experte GmbH & Co. KG.',
                'attributes' => [
                    'data-icon' => 'fab fa-buromobelexperte',
                ],
            ],
            [
                'value' => 'fas fa-burst',
                'label' => 'Burst',
                'attributes' => [
                    'data-icon' => 'fas fa-burst',
                ],
            ],
            [
                'value' => 'fas fa-bus',
                'label' => 'Bus',
                'attributes' => [
                    'data-icon' => 'fas fa-bus',
                ],
            ],
            [
                'value' => 'fas fa-bus-simple',
                'label' => 'Bus simple',
                'attributes' => [
                    'data-icon' => 'fas fa-bus-simple',
                ],
            ],
            [
                'value' => 'fas fa-business-time',
                'label' => 'Briefcase clock',
                'attributes' => [
                    'data-icon' => 'fas fa-business-time',
                ],
            ],
            [
                'value' => 'fab fa-buy-n-large',
                'label' => 'Buy n Large',
                'attributes' => [
                    'data-icon' => 'fab fa-buy-n-large',
                ],
            ],
            [
                'value' => 'fab fa-buysellads',
                'label' => 'BuySellAds',
                'attributes' => [
                    'data-icon' => 'fab fa-buysellads',
                ],
            ],
            [
                'value' => 'fas fa-c',
                'label' => 'C',
                'attributes' => [
                    'data-icon' => 'fas fa-c',
                ],
            ],
            [
                'value' => 'fas fa-cake-candles',
                'label' => 'Cake candles',
                'attributes' => [
                    'data-icon' => 'fas fa-cake-candles',
                ],
            ],
            [
                'value' => 'fas fa-calculator',
                'label' => 'Calculator',
                'attributes' => [
                    'data-icon' => 'fas fa-calculator',
                ],
            ],
            [
                'value' => 'fas fa-calendar',
                'label' => 'Calendar',
                'attributes' => [
                    'data-icon' => 'fas fa-calendar',
                ],
            ],
            [
                'value' => 'far fa-calendar',
                'label' => 'Calendar',
                'attributes' => [
                    'data-icon' => 'far fa-calendar',
                ],
            ],
            [
                'value' => 'fas fa-calendar-check',
                'label' => 'Calendar Check',
                'attributes' => [
                    'data-icon' => 'fas fa-calendar-check',
                ],
            ],
            [
                'value' => 'far fa-calendar-check',
                'label' => 'Calendar Check',
                'attributes' => [
                    'data-icon' => 'far fa-calendar-check',
                ],
            ],
            [
                'value' => 'fas fa-calendar-day',
                'label' => 'Calendar with Day Focus',
                'attributes' => [
                    'data-icon' => 'fas fa-calendar-day',
                ],
            ],
            [
                'value' => 'fas fa-calendar-days',
                'label' => 'Calendar Days',
                'attributes' => [
                    'data-icon' => 'fas fa-calendar-days',
                ],
            ],
            [
                'value' => 'far fa-calendar-days',
                'label' => 'Calendar Days',
                'attributes' => [
                    'data-icon' => 'far fa-calendar-days',
                ],
            ],
            [
                'value' => 'fas fa-calendar-minus',
                'label' => 'Calendar Minus',
                'attributes' => [
                    'data-icon' => 'fas fa-calendar-minus',
                ],
            ],
            [
                'value' => 'far fa-calendar-minus',
                'label' => 'Calendar Minus',
                'attributes' => [
                    'data-icon' => 'far fa-calendar-minus',
                ],
            ],
            [
                'value' => 'fas fa-calendar-plus',
                'label' => 'Calendar Plus',
                'attributes' => [
                    'data-icon' => 'fas fa-calendar-plus',
                ],
            ],
            [
                'value' => 'far fa-calendar-plus',
                'label' => 'Calendar Plus',
                'attributes' => [
                    'data-icon' => 'far fa-calendar-plus',
                ],
            ],
            [
                'value' => 'fas fa-calendar-week',
                'label' => 'Calendar with Week Focus',
                'attributes' => [
                    'data-icon' => 'fas fa-calendar-week',
                ],
            ],
            [
                'value' => 'fas fa-calendar-xmark',
                'label' => 'Calendar X Mark',
                'attributes' => [
                    'data-icon' => 'fas fa-calendar-xmark',
                ],
            ],
            [
                'value' => 'far fa-calendar-xmark',
                'label' => 'Calendar X Mark',
                'attributes' => [
                    'data-icon' => 'far fa-calendar-xmark',
                ],
            ],
            [
                'value' => 'fas fa-camera',
                'label' => 'camera',
                'attributes' => [
                    'data-icon' => 'fas fa-camera',
                ],
            ],
            [
                'value' => 'fas fa-camera-retro',
                'label' => 'Retro Camera',
                'attributes' => [
                    'data-icon' => 'fas fa-camera-retro',
                ],
            ],
            [
                'value' => 'fas fa-camera-rotate',
                'label' => 'Camera Rotate',
                'attributes' => [
                    'data-icon' => 'fas fa-camera-rotate',
                ],
            ],
            [
                'value' => 'fas fa-campground',
                'label' => 'Campground',
                'attributes' => [
                    'data-icon' => 'fas fa-campground',
                ],
            ],
            [
                'value' => 'fab fa-canadian-maple-leaf',
                'label' => 'Canadian Maple Leaf',
                'attributes' => [
                    'data-icon' => 'fab fa-canadian-maple-leaf',
                ],
            ],
            [
                'value' => 'fas fa-candy-cane',
                'label' => 'Candy Cane',
                'attributes' => [
                    'data-icon' => 'fas fa-candy-cane',
                ],
            ],
            [
                'value' => 'fas fa-cannabis',
                'label' => 'Cannabis',
                'attributes' => [
                    'data-icon' => 'fas fa-cannabis',
                ],
            ],
            [
                'value' => 'fas fa-capsules',
                'label' => 'Capsules',
                'attributes' => [
                    'data-icon' => 'fas fa-capsules',
                ],
            ],
            [
                'value' => 'fas fa-car',
                'label' => 'Car',
                'attributes' => [
                    'data-icon' => 'fas fa-car',
                ],
            ],
            [
                'value' => 'fas fa-car-battery',
                'label' => 'Car Battery',
                'attributes' => [
                    'data-icon' => 'fas fa-car-battery',
                ],
            ],
            [
                'value' => 'fas fa-car-burst',
                'label' => 'Car Crash',
                'attributes' => [
                    'data-icon' => 'fas fa-car-burst',
                ],
            ],
            [
                'value' => 'fas fa-car-on',
                'label' => 'Car On',
                'attributes' => [
                    'data-icon' => 'fas fa-car-on',
                ],
            ],
            [
                'value' => 'fas fa-car-rear',
                'label' => 'Car rear',
                'attributes' => [
                    'data-icon' => 'fas fa-car-rear',
                ],
            ],
            [
                'value' => 'fas fa-car-side',
                'label' => 'Car Side',
                'attributes' => [
                    'data-icon' => 'fas fa-car-side',
                ],
            ],
            [
                'value' => 'fas fa-car-tunnel',
                'label' => 'Car Tunnel',
                'attributes' => [
                    'data-icon' => 'fas fa-car-tunnel',
                ],
            ],
            [
                'value' => 'fas fa-caravan',
                'label' => 'Caravan',
                'attributes' => [
                    'data-icon' => 'fas fa-caravan',
                ],
            ],
            [
                'value' => 'fas fa-caret-down',
                'label' => 'Caret Down',
                'attributes' => [
                    'data-icon' => 'fas fa-caret-down',
                ],
            ],
            [
                'value' => 'fas fa-caret-left',
                'label' => 'Caret Left',
                'attributes' => [
                    'data-icon' => 'fas fa-caret-left',
                ],
            ],
            [
                'value' => 'fas fa-caret-right',
                'label' => 'Caret Right',
                'attributes' => [
                    'data-icon' => 'fas fa-caret-right',
                ],
            ],
            [
                'value' => 'fas fa-caret-up',
                'label' => 'Caret Up',
                'attributes' => [
                    'data-icon' => 'fas fa-caret-up',
                ],
            ],
            [
                'value' => 'fas fa-carrot',
                'label' => 'Carrot',
                'attributes' => [
                    'data-icon' => 'fas fa-carrot',
                ],
            ],
            [
                'value' => 'fas fa-cart-arrow-down',
                'label' => 'Shopping Cart Arrow Down',
                'attributes' => [
                    'data-icon' => 'fas fa-cart-arrow-down',
                ],
            ],
            [
                'value' => 'fas fa-cart-flatbed',
                'label' => 'Cart flatbed',
                'attributes' => [
                    'data-icon' => 'fas fa-cart-flatbed',
                ],
            ],
            [
                'value' => 'fas fa-cart-flatbed-suitcase',
                'label' => 'Cart flatbed suitcase',
                'attributes' => [
                    'data-icon' => 'fas fa-cart-flatbed-suitcase',
                ],
            ],
            [
                'value' => 'fas fa-cart-plus',
                'label' => 'Add to Shopping Cart',
                'attributes' => [
                    'data-icon' => 'fas fa-cart-plus',
                ],
            ],
            [
                'value' => 'fas fa-cart-shopping',
                'label' => 'Cart shopping',
                'attributes' => [
                    'data-icon' => 'fas fa-cart-shopping',
                ],
            ],
            [
                'value' => 'fas fa-cash-register',
                'label' => 'Cash Register',
                'attributes' => [
                    'data-icon' => 'fas fa-cash-register',
                ],
            ],
            [
                'value' => 'fas fa-cat',
                'label' => 'Cat',
                'attributes' => [
                    'data-icon' => 'fas fa-cat',
                ],
            ],
            [
                'value' => 'fab fa-cc-amazon-pay',
                'label' => 'Amazon Pay Credit Card',
                'attributes' => [
                    'data-icon' => 'fab fa-cc-amazon-pay',
                ],
            ],
            [
                'value' => 'fab fa-cc-amex',
                'label' => 'American Express Credit Card',
                'attributes' => [
                    'data-icon' => 'fab fa-cc-amex',
                ],
            ],
            [
                'value' => 'fab fa-cc-apple-pay',
                'label' => 'Apple Pay Credit Card',
                'attributes' => [
                    'data-icon' => 'fab fa-cc-apple-pay',
                ],
            ],
            [
                'value' => 'fab fa-cc-diners-club',
                'label' => 'Diners Club Credit Card',
                'attributes' => [
                    'data-icon' => 'fab fa-cc-diners-club',
                ],
            ],
            [
                'value' => 'fab fa-cc-discover',
                'label' => 'Discover Credit Card',
                'attributes' => [
                    'data-icon' => 'fab fa-cc-discover',
                ],
            ],
            [
                'value' => 'fab fa-cc-jcb',
                'label' => 'JCB Credit Card',
                'attributes' => [
                    'data-icon' => 'fab fa-cc-jcb',
                ],
            ],
            [
                'value' => 'fab fa-cc-mastercard',
                'label' => 'MasterCard Credit Card',
                'attributes' => [
                    'data-icon' => 'fab fa-cc-mastercard',
                ],
            ],
            [
                'value' => 'fab fa-cc-paypal',
                'label' => 'Paypal Credit Card',
                'attributes' => [
                    'data-icon' => 'fab fa-cc-paypal',
                ],
            ],
            [
                'value' => 'fab fa-cc-stripe',
                'label' => 'Stripe Credit Card',
                'attributes' => [
                    'data-icon' => 'fab fa-cc-stripe',
                ],
            ],
            [
                'value' => 'fab fa-cc-visa',
                'label' => 'Visa Credit Card',
                'attributes' => [
                    'data-icon' => 'fab fa-cc-visa',
                ],
            ],
            [
                'value' => 'fas fa-cedi-sign',
                'label' => 'Cedi Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-cedi-sign',
                ],
            ],
            [
                'value' => 'fas fa-cent-sign',
                'label' => 'Cent Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-cent-sign',
                ],
            ],
            [
                'value' => 'fab fa-centercode',
                'label' => 'Centercode',
                'attributes' => [
                    'data-icon' => 'fab fa-centercode',
                ],
            ],
            [
                'value' => 'fab fa-centos',
                'label' => 'Centos',
                'attributes' => [
                    'data-icon' => 'fab fa-centos',
                ],
            ],
            [
                'value' => 'fas fa-certificate',
                'label' => 'certificate',
                'attributes' => [
                    'data-icon' => 'fas fa-certificate',
                ],
            ],
            [
                'value' => 'fas fa-chair',
                'label' => 'Chair',
                'attributes' => [
                    'data-icon' => 'fas fa-chair',
                ],
            ],
            [
                'value' => 'fas fa-chalkboard',
                'label' => 'Chalkboard',
                'attributes' => [
                    'data-icon' => 'fas fa-chalkboard',
                ],
            ],
            [
                'value' => 'fas fa-chalkboard-user',
                'label' => 'Chalkboard user',
                'attributes' => [
                    'data-icon' => 'fas fa-chalkboard-user',
                ],
            ],
            [
                'value' => 'fas fa-champagne-glasses',
                'label' => 'Champagne glasses',
                'attributes' => [
                    'data-icon' => 'fas fa-champagne-glasses',
                ],
            ],
            [
                'value' => 'fas fa-charging-station',
                'label' => 'Charging Station',
                'attributes' => [
                    'data-icon' => 'fas fa-charging-station',
                ],
            ],
            [
                'value' => 'fas fa-chart-area',
                'label' => 'Area Chart',
                'attributes' => [
                    'data-icon' => 'fas fa-chart-area',
                ],
            ],
            [
                'value' => 'fas fa-chart-bar',
                'label' => 'Bar Chart',
                'attributes' => [
                    'data-icon' => 'fas fa-chart-bar',
                ],
            ],
            [
                'value' => 'far fa-chart-bar',
                'label' => 'Bar Chart',
                'attributes' => [
                    'data-icon' => 'far fa-chart-bar',
                ],
            ],
            [
                'value' => 'fas fa-chart-column',
                'label' => 'Chart Column',
                'attributes' => [
                    'data-icon' => 'fas fa-chart-column',
                ],
            ],
            [
                'value' => 'fas fa-chart-gantt',
                'label' => 'Chart Gantt',
                'attributes' => [
                    'data-icon' => 'fas fa-chart-gantt',
                ],
            ],
            [
                'value' => 'fas fa-chart-line',
                'label' => 'Line Chart',
                'attributes' => [
                    'data-icon' => 'fas fa-chart-line',
                ],
            ],
            [
                'value' => 'fas fa-chart-pie',
                'label' => 'Pie Chart',
                'attributes' => [
                    'data-icon' => 'fas fa-chart-pie',
                ],
            ],
            [
                'value' => 'fas fa-chart-simple',
                'label' => 'Chart Simple',
                'attributes' => [
                    'data-icon' => 'fas fa-chart-simple',
                ],
            ],
            [
                'value' => 'fas fa-check',
                'label' => 'Check',
                'attributes' => [
                    'data-icon' => 'fas fa-check',
                ],
            ],
            [
                'value' => 'fas fa-check-double',
                'label' => 'Double Check',
                'attributes' => [
                    'data-icon' => 'fas fa-check-double',
                ],
            ],
            [
                'value' => 'fas fa-check-to-slot',
                'label' => 'Check to Slot',
                'attributes' => [
                    'data-icon' => 'fas fa-check-to-slot',
                ],
            ],
            [
                'value' => 'fas fa-cheese',
                'label' => 'Cheese',
                'attributes' => [
                    'data-icon' => 'fas fa-cheese',
                ],
            ],
            [
                'value' => 'fas fa-chess',
                'label' => 'Chess',
                'attributes' => [
                    'data-icon' => 'fas fa-chess',
                ],
            ],
            [
                'value' => 'fas fa-chess-bishop',
                'label' => 'Chess Bishop',
                'attributes' => [
                    'data-icon' => 'fas fa-chess-bishop',
                ],
            ],
            [
                'value' => 'far fa-chess-bishop',
                'label' => 'Chess Bishop',
                'attributes' => [
                    'data-icon' => 'far fa-chess-bishop',
                ],
            ],
            [
                'value' => 'fas fa-chess-board',
                'label' => 'Chess Board',
                'attributes' => [
                    'data-icon' => 'fas fa-chess-board',
                ],
            ],
            [
                'value' => 'fas fa-chess-king',
                'label' => 'Chess King',
                'attributes' => [
                    'data-icon' => 'fas fa-chess-king',
                ],
            ],
            [
                'value' => 'far fa-chess-king',
                'label' => 'Chess King',
                'attributes' => [
                    'data-icon' => 'far fa-chess-king',
                ],
            ],
            [
                'value' => 'fas fa-chess-knight',
                'label' => 'Chess Knight',
                'attributes' => [
                    'data-icon' => 'fas fa-chess-knight',
                ],
            ],
            [
                'value' => 'far fa-chess-knight',
                'label' => 'Chess Knight',
                'attributes' => [
                    'data-icon' => 'far fa-chess-knight',
                ],
            ],
            [
                'value' => 'fas fa-chess-pawn',
                'label' => 'Chess Pawn',
                'attributes' => [
                    'data-icon' => 'fas fa-chess-pawn',
                ],
            ],
            [
                'value' => 'far fa-chess-pawn',
                'label' => 'Chess Pawn',
                'attributes' => [
                    'data-icon' => 'far fa-chess-pawn',
                ],
            ],
            [
                'value' => 'fas fa-chess-queen',
                'label' => 'Chess Queen',
                'attributes' => [
                    'data-icon' => 'fas fa-chess-queen',
                ],
            ],
            [
                'value' => 'far fa-chess-queen',
                'label' => 'Chess Queen',
                'attributes' => [
                    'data-icon' => 'far fa-chess-queen',
                ],
            ],
            [
                'value' => 'fas fa-chess-rook',
                'label' => 'Chess Rook',
                'attributes' => [
                    'data-icon' => 'fas fa-chess-rook',
                ],
            ],
            [
                'value' => 'far fa-chess-rook',
                'label' => 'Chess Rook',
                'attributes' => [
                    'data-icon' => 'far fa-chess-rook',
                ],
            ],
            [
                'value' => 'fas fa-chevron-down',
                'label' => 'chevron-down',
                'attributes' => [
                    'data-icon' => 'fas fa-chevron-down',
                ],
            ],
            [
                'value' => 'fas fa-chevron-left',
                'label' => 'chevron-left',
                'attributes' => [
                    'data-icon' => 'fas fa-chevron-left',
                ],
            ],
            [
                'value' => 'fas fa-chevron-right',
                'label' => 'chevron-right',
                'attributes' => [
                    'data-icon' => 'fas fa-chevron-right',
                ],
            ],
            [
                'value' => 'fas fa-chevron-up',
                'label' => 'chevron-up',
                'attributes' => [
                    'data-icon' => 'fas fa-chevron-up',
                ],
            ],
            [
                'value' => 'fas fa-child',
                'label' => 'Child',
                'attributes' => [
                    'data-icon' => 'fas fa-child',
                ],
            ],
            [
                'value' => 'fas fa-child-dress',
                'label' => 'Child Dress',
                'attributes' => [
                    'data-icon' => 'fas fa-child-dress',
                ],
            ],
            [
                'value' => 'fas fa-child-reaching',
                'label' => 'Child Reaching',
                'attributes' => [
                    'data-icon' => 'fas fa-child-reaching',
                ],
            ],
            [
                'value' => 'fas fa-child-rifle',
                'label' => 'Child Rifle',
                'attributes' => [
                    'data-icon' => 'fas fa-child-rifle',
                ],
            ],
            [
                'value' => 'fas fa-children',
                'label' => 'Children',
                'attributes' => [
                    'data-icon' => 'fas fa-children',
                ],
            ],
            [
                'value' => 'fab fa-chrome',
                'label' => 'Chrome',
                'attributes' => [
                    'data-icon' => 'fab fa-chrome',
                ],
            ],
            [
                'value' => 'fab fa-chromecast',
                'label' => 'Chromecast',
                'attributes' => [
                    'data-icon' => 'fab fa-chromecast',
                ],
            ],
            [
                'value' => 'fas fa-church',
                'label' => 'Church',
                'attributes' => [
                    'data-icon' => 'fas fa-church',
                ],
            ],
            [
                'value' => 'fas fa-circle',
                'label' => 'Circle',
                'attributes' => [
                    'data-icon' => 'fas fa-circle',
                ],
            ],
            [
                'value' => 'far fa-circle',
                'label' => 'Circle',
                'attributes' => [
                    'data-icon' => 'far fa-circle',
                ],
            ],
            [
                'value' => 'fas fa-circle-arrow-down',
                'label' => 'Circle arrow down',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-arrow-down',
                ],
            ],
            [
                'value' => 'fas fa-circle-arrow-left',
                'label' => 'Circle arrow left',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-arrow-left',
                ],
            ],
            [
                'value' => 'fas fa-circle-arrow-right',
                'label' => 'Circle arrow right',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-arrow-right',
                ],
            ],
            [
                'value' => 'fas fa-circle-arrow-up',
                'label' => 'Circle arrow up',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-arrow-up',
                ],
            ],
            [
                'value' => 'fas fa-circle-check',
                'label' => 'Circle check',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-check',
                ],
            ],
            [
                'value' => 'far fa-circle-check',
                'label' => 'Circle check',
                'attributes' => [
                    'data-icon' => 'far fa-circle-check',
                ],
            ],
            [
                'value' => 'fas fa-circle-chevron-down',
                'label' => 'Circle chevron down',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-chevron-down',
                ],
            ],
            [
                'value' => 'fas fa-circle-chevron-left',
                'label' => 'Circle chevron left',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-chevron-left',
                ],
            ],
            [
                'value' => 'fas fa-circle-chevron-right',
                'label' => 'Circle chevron right',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-chevron-right',
                ],
            ],
            [
                'value' => 'fas fa-circle-chevron-up',
                'label' => 'Circle chevron up',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-chevron-up',
                ],
            ],
            [
                'value' => 'fas fa-circle-dollar-to-slot',
                'label' => 'Circle dollar to slot',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-dollar-to-slot',
                ],
            ],
            [
                'value' => 'fas fa-circle-dot',
                'label' => 'Circle dot',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-dot',
                ],
            ],
            [
                'value' => 'far fa-circle-dot',
                'label' => 'Circle dot',
                'attributes' => [
                    'data-icon' => 'far fa-circle-dot',
                ],
            ],
            [
                'value' => 'fas fa-circle-down',
                'label' => 'Circle down',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-down',
                ],
            ],
            [
                'value' => 'far fa-circle-down',
                'label' => 'Circle down',
                'attributes' => [
                    'data-icon' => 'far fa-circle-down',
                ],
            ],
            [
                'value' => 'fas fa-circle-exclamation',
                'label' => 'Circle exclamation',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-exclamation',
                ],
            ],
            [
                'value' => 'fas fa-circle-h',
                'label' => 'Circle h',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-h',
                ],
            ],
            [
                'value' => 'fas fa-circle-half-stroke',
                'label' => 'Circle half stroke',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-half-stroke',
                ],
            ],
            [
                'value' => 'fas fa-circle-info',
                'label' => 'Circle info',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-info',
                ],
            ],
            [
                'value' => 'fas fa-circle-left',
                'label' => 'Circle left',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-left',
                ],
            ],
            [
                'value' => 'far fa-circle-left',
                'label' => 'Circle left',
                'attributes' => [
                    'data-icon' => 'far fa-circle-left',
                ],
            ],
            [
                'value' => 'fas fa-circle-minus',
                'label' => 'Circle minus',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-minus',
                ],
            ],
            [
                'value' => 'fas fa-circle-nodes',
                'label' => 'Circle Nodes',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-nodes',
                ],
            ],
            [
                'value' => 'fas fa-circle-notch',
                'label' => 'Circle Notched',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-notch',
                ],
            ],
            [
                'value' => 'fas fa-circle-pause',
                'label' => 'Circle pause',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-pause',
                ],
            ],
            [
                'value' => 'far fa-circle-pause',
                'label' => 'Circle pause',
                'attributes' => [
                    'data-icon' => 'far fa-circle-pause',
                ],
            ],
            [
                'value' => 'fas fa-circle-play',
                'label' => 'Circle play',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-play',
                ],
            ],
            [
                'value' => 'far fa-circle-play',
                'label' => 'Circle play',
                'attributes' => [
                    'data-icon' => 'far fa-circle-play',
                ],
            ],
            [
                'value' => 'fas fa-circle-plus',
                'label' => 'Circle plus',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-plus',
                ],
            ],
            [
                'value' => 'fas fa-circle-question',
                'label' => 'Circle question',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-question',
                ],
            ],
            [
                'value' => 'far fa-circle-question',
                'label' => 'Circle question',
                'attributes' => [
                    'data-icon' => 'far fa-circle-question',
                ],
            ],
            [
                'value' => 'fas fa-circle-radiation',
                'label' => 'Circle radiation',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-radiation',
                ],
            ],
            [
                'value' => 'fas fa-circle-right',
                'label' => 'Circle right',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-right',
                ],
            ],
            [
                'value' => 'far fa-circle-right',
                'label' => 'Circle right',
                'attributes' => [
                    'data-icon' => 'far fa-circle-right',
                ],
            ],
            [
                'value' => 'fas fa-circle-stop',
                'label' => 'Circle stop',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-stop',
                ],
            ],
            [
                'value' => 'far fa-circle-stop',
                'label' => 'Circle stop',
                'attributes' => [
                    'data-icon' => 'far fa-circle-stop',
                ],
            ],
            [
                'value' => 'fas fa-circle-up',
                'label' => 'Circle up',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-up',
                ],
            ],
            [
                'value' => 'far fa-circle-up',
                'label' => 'Circle up',
                'attributes' => [
                    'data-icon' => 'far fa-circle-up',
                ],
            ],
            [
                'value' => 'fas fa-circle-user',
                'label' => 'Circle user',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-user',
                ],
            ],
            [
                'value' => 'far fa-circle-user',
                'label' => 'Circle user',
                'attributes' => [
                    'data-icon' => 'far fa-circle-user',
                ],
            ],
            [
                'value' => 'fas fa-circle-xmark',
                'label' => 'Circle X Mark',
                'attributes' => [
                    'data-icon' => 'fas fa-circle-xmark',
                ],
            ],
            [
                'value' => 'far fa-circle-xmark',
                'label' => 'Circle X Mark',
                'attributes' => [
                    'data-icon' => 'far fa-circle-xmark',
                ],
            ],
            [
                'value' => 'fas fa-city',
                'label' => 'City',
                'attributes' => [
                    'data-icon' => 'fas fa-city',
                ],
            ],
            [
                'value' => 'fas fa-clapperboard',
                'label' => 'Clapperboard',
                'attributes' => [
                    'data-icon' => 'fas fa-clapperboard',
                ],
            ],
            [
                'value' => 'fas fa-clipboard',
                'label' => 'Clipboard',
                'attributes' => [
                    'data-icon' => 'fas fa-clipboard',
                ],
            ],
            [
                'value' => 'far fa-clipboard',
                'label' => 'Clipboard',
                'attributes' => [
                    'data-icon' => 'far fa-clipboard',
                ],
            ],
            [
                'value' => 'fas fa-clipboard-check',
                'label' => 'Clipboard with Check',
                'attributes' => [
                    'data-icon' => 'fas fa-clipboard-check',
                ],
            ],
            [
                'value' => 'fas fa-clipboard-list',
                'label' => 'Clipboard List',
                'attributes' => [
                    'data-icon' => 'fas fa-clipboard-list',
                ],
            ],
            [
                'value' => 'fas fa-clipboard-question',
                'label' => 'Clipboard Question',
                'attributes' => [
                    'data-icon' => 'fas fa-clipboard-question',
                ],
            ],
            [
                'value' => 'fas fa-clipboard-user',
                'label' => 'Clipboard with User',
                'attributes' => [
                    'data-icon' => 'fas fa-clipboard-user',
                ],
            ],
            [
                'value' => 'fas fa-clock',
                'label' => 'Clock',
                'attributes' => [
                    'data-icon' => 'fas fa-clock',
                ],
            ],
            [
                'value' => 'far fa-clock',
                'label' => 'Clock',
                'attributes' => [
                    'data-icon' => 'far fa-clock',
                ],
            ],
            [
                'value' => 'fas fa-clock-rotate-left',
                'label' => 'Clock Rotate Left',
                'attributes' => [
                    'data-icon' => 'fas fa-clock-rotate-left',
                ],
            ],
            [
                'value' => 'fas fa-clone',
                'label' => 'Clone',
                'attributes' => [
                    'data-icon' => 'fas fa-clone',
                ],
            ],
            [
                'value' => 'far fa-clone',
                'label' => 'Clone',
                'attributes' => [
                    'data-icon' => 'far fa-clone',
                ],
            ],
            [
                'value' => 'fas fa-closed-captioning',
                'label' => 'Closed Captioning',
                'attributes' => [
                    'data-icon' => 'fas fa-closed-captioning',
                ],
            ],
            [
                'value' => 'far fa-closed-captioning',
                'label' => 'Closed Captioning',
                'attributes' => [
                    'data-icon' => 'far fa-closed-captioning',
                ],
            ],
            [
                'value' => 'fas fa-cloud',
                'label' => 'Cloud',
                'attributes' => [
                    'data-icon' => 'fas fa-cloud',
                ],
            ],
            [
                'value' => 'fas fa-cloud-arrow-down',
                'label' => 'Cloud arrow down',
                'attributes' => [
                    'data-icon' => 'fas fa-cloud-arrow-down',
                ],
            ],
            [
                'value' => 'fas fa-cloud-arrow-up',
                'label' => 'Cloud arrow up',
                'attributes' => [
                    'data-icon' => 'fas fa-cloud-arrow-up',
                ],
            ],
            [
                'value' => 'fas fa-cloud-bolt',
                'label' => 'Cloud bolt',
                'attributes' => [
                    'data-icon' => 'fas fa-cloud-bolt',
                ],
            ],
            [
                'value' => 'fas fa-cloud-meatball',
                'label' => 'Cloud with (a chance of) Meatball',
                'attributes' => [
                    'data-icon' => 'fas fa-cloud-meatball',
                ],
            ],
            [
                'value' => 'fas fa-cloud-moon',
                'label' => 'Cloud with Moon',
                'attributes' => [
                    'data-icon' => 'fas fa-cloud-moon',
                ],
            ],
            [
                'value' => 'fas fa-cloud-moon-rain',
                'label' => 'Cloud with Moon and Rain',
                'attributes' => [
                    'data-icon' => 'fas fa-cloud-moon-rain',
                ],
            ],
            [
                'value' => 'fas fa-cloud-rain',
                'label' => 'Cloud with Rain',
                'attributes' => [
                    'data-icon' => 'fas fa-cloud-rain',
                ],
            ],
            [
                'value' => 'fas fa-cloud-showers-heavy',
                'label' => 'Cloud with Heavy Showers',
                'attributes' => [
                    'data-icon' => 'fas fa-cloud-showers-heavy',
                ],
            ],
            [
                'value' => 'fas fa-cloud-showers-water',
                'label' => 'Cloud Showers-water',
                'attributes' => [
                    'data-icon' => 'fas fa-cloud-showers-water',
                ],
            ],
            [
                'value' => 'fas fa-cloud-sun',
                'label' => 'Cloud with Sun',
                'attributes' => [
                    'data-icon' => 'fas fa-cloud-sun',
                ],
            ],
            [
                'value' => 'fas fa-cloud-sun-rain',
                'label' => 'Cloud with Sun and Rain',
                'attributes' => [
                    'data-icon' => 'fas fa-cloud-sun-rain',
                ],
            ],
            [
                'value' => 'fab fa-cloudflare',
                'label' => 'Cloudflare',
                'attributes' => [
                    'data-icon' => 'fab fa-cloudflare',
                ],
            ],
            [
                'value' => 'fab fa-cloudscale',
                'label' => 'cloudscale.ch',
                'attributes' => [
                    'data-icon' => 'fab fa-cloudscale',
                ],
            ],
            [
                'value' => 'fab fa-cloudsmith',
                'label' => 'Cloudsmith',
                'attributes' => [
                    'data-icon' => 'fab fa-cloudsmith',
                ],
            ],
            [
                'value' => 'fab fa-cloudversify',
                'label' => 'cloudversify',
                'attributes' => [
                    'data-icon' => 'fab fa-cloudversify',
                ],
            ],
            [
                'value' => 'fas fa-clover',
                'label' => 'Clover',
                'attributes' => [
                    'data-icon' => 'fas fa-clover',
                ],
            ],
            [
                'value' => 'fab fa-cmplid',
                'label' => 'Cmplid',
                'attributes' => [
                    'data-icon' => 'fab fa-cmplid',
                ],
            ],
            [
                'value' => 'fas fa-code',
                'label' => 'Code',
                'attributes' => [
                    'data-icon' => 'fas fa-code',
                ],
            ],
            [
                'value' => 'fas fa-code-branch',
                'label' => 'Code Branch',
                'attributes' => [
                    'data-icon' => 'fas fa-code-branch',
                ],
            ],
            [
                'value' => 'fas fa-code-commit',
                'label' => 'Code Commit',
                'attributes' => [
                    'data-icon' => 'fas fa-code-commit',
                ],
            ],
            [
                'value' => 'fas fa-code-compare',
                'label' => 'Code Compare',
                'attributes' => [
                    'data-icon' => 'fas fa-code-compare',
                ],
            ],
            [
                'value' => 'fas fa-code-fork',
                'label' => 'Code Fork',
                'attributes' => [
                    'data-icon' => 'fas fa-code-fork',
                ],
            ],
            [
                'value' => 'fas fa-code-merge',
                'label' => 'Code Merge',
                'attributes' => [
                    'data-icon' => 'fas fa-code-merge',
                ],
            ],
            [
                'value' => 'fas fa-code-pull-request',
                'label' => 'Code Pull Request',
                'attributes' => [
                    'data-icon' => 'fas fa-code-pull-request',
                ],
            ],
            [
                'value' => 'fab fa-codepen',
                'label' => 'Codepen',
                'attributes' => [
                    'data-icon' => 'fab fa-codepen',
                ],
            ],
            [
                'value' => 'fab fa-codiepie',
                'label' => 'Codie Pie',
                'attributes' => [
                    'data-icon' => 'fab fa-codiepie',
                ],
            ],
            [
                'value' => 'fas fa-coins',
                'label' => 'Coins',
                'attributes' => [
                    'data-icon' => 'fas fa-coins',
                ],
            ],
            [
                'value' => 'fas fa-colon-sign',
                'label' => 'Colon Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-colon-sign',
                ],
            ],
            [
                'value' => 'fas fa-comment',
                'label' => 'comment',
                'attributes' => [
                    'data-icon' => 'fas fa-comment',
                ],
            ],
            [
                'value' => 'far fa-comment',
                'label' => 'comment',
                'attributes' => [
                    'data-icon' => 'far fa-comment',
                ],
            ],
            [
                'value' => 'fas fa-comment-dollar',
                'label' => 'Comment Dollar',
                'attributes' => [
                    'data-icon' => 'fas fa-comment-dollar',
                ],
            ],
            [
                'value' => 'fas fa-comment-dots',
                'label' => 'Comment Dots',
                'attributes' => [
                    'data-icon' => 'fas fa-comment-dots',
                ],
            ],
            [
                'value' => 'far fa-comment-dots',
                'label' => 'Comment Dots',
                'attributes' => [
                    'data-icon' => 'far fa-comment-dots',
                ],
            ],
            [
                'value' => 'fas fa-comment-medical',
                'label' => 'Alternate Medical Chat',
                'attributes' => [
                    'data-icon' => 'fas fa-comment-medical',
                ],
            ],
            [
                'value' => 'fas fa-comment-slash',
                'label' => 'Comment Slash',
                'attributes' => [
                    'data-icon' => 'fas fa-comment-slash',
                ],
            ],
            [
                'value' => 'fas fa-comment-sms',
                'label' => 'Comment sms',
                'attributes' => [
                    'data-icon' => 'fas fa-comment-sms',
                ],
            ],
            [
                'value' => 'fas fa-comments',
                'label' => 'comments',
                'attributes' => [
                    'data-icon' => 'fas fa-comments',
                ],
            ],
            [
                'value' => 'far fa-comments',
                'label' => 'comments',
                'attributes' => [
                    'data-icon' => 'far fa-comments',
                ],
            ],
            [
                'value' => 'fas fa-comments-dollar',
                'label' => 'Comments Dollar',
                'attributes' => [
                    'data-icon' => 'fas fa-comments-dollar',
                ],
            ],
            [
                'value' => 'fas fa-compact-disc',
                'label' => 'Compact Disc',
                'attributes' => [
                    'data-icon' => 'fas fa-compact-disc',
                ],
            ],
            [
                'value' => 'fas fa-compass',
                'label' => 'Compass',
                'attributes' => [
                    'data-icon' => 'fas fa-compass',
                ],
            ],
            [
                'value' => 'far fa-compass',
                'label' => 'Compass',
                'attributes' => [
                    'data-icon' => 'far fa-compass',
                ],
            ],
            [
                'value' => 'fas fa-compass-drafting',
                'label' => 'Compass drafting',
                'attributes' => [
                    'data-icon' => 'fas fa-compass-drafting',
                ],
            ],
            [
                'value' => 'fas fa-compress',
                'label' => 'Compress',
                'attributes' => [
                    'data-icon' => 'fas fa-compress',
                ],
            ],
            [
                'value' => 'fas fa-computer',
                'label' => 'Computer',
                'attributes' => [
                    'data-icon' => 'fas fa-computer',
                ],
            ],
            [
                'value' => 'fas fa-computer-mouse',
                'label' => 'Computer mouse',
                'attributes' => [
                    'data-icon' => 'fas fa-computer-mouse',
                ],
            ],
            [
                'value' => 'fab fa-confluence',
                'label' => 'Confluence',
                'attributes' => [
                    'data-icon' => 'fab fa-confluence',
                ],
            ],
            [
                'value' => 'fab fa-connectdevelop',
                'label' => 'Connect Develop',
                'attributes' => [
                    'data-icon' => 'fab fa-connectdevelop',
                ],
            ],
            [
                'value' => 'fab fa-contao',
                'label' => 'Contao',
                'attributes' => [
                    'data-icon' => 'fab fa-contao',
                ],
            ],
            [
                'value' => 'fas fa-cookie',
                'label' => 'Cookie',
                'attributes' => [
                    'data-icon' => 'fas fa-cookie',
                ],
            ],
            [
                'value' => 'fas fa-cookie-bite',
                'label' => 'Cookie Bite',
                'attributes' => [
                    'data-icon' => 'fas fa-cookie-bite',
                ],
            ],
            [
                'value' => 'fas fa-copy',
                'label' => 'Copy',
                'attributes' => [
                    'data-icon' => 'fas fa-copy',
                ],
            ],
            [
                'value' => 'far fa-copy',
                'label' => 'Copy',
                'attributes' => [
                    'data-icon' => 'far fa-copy',
                ],
            ],
            [
                'value' => 'fas fa-copyright',
                'label' => 'Copyright',
                'attributes' => [
                    'data-icon' => 'fas fa-copyright',
                ],
            ],
            [
                'value' => 'far fa-copyright',
                'label' => 'Copyright',
                'attributes' => [
                    'data-icon' => 'far fa-copyright',
                ],
            ],
            [
                'value' => 'fab fa-cotton-bureau',
                'label' => 'Cotton Bureau',
                'attributes' => [
                    'data-icon' => 'fab fa-cotton-bureau',
                ],
            ],
            [
                'value' => 'fas fa-couch',
                'label' => 'Couch',
                'attributes' => [
                    'data-icon' => 'fas fa-couch',
                ],
            ],
            [
                'value' => 'fas fa-cow',
                'label' => 'Cow',
                'attributes' => [
                    'data-icon' => 'fas fa-cow',
                ],
            ],
            [
                'value' => 'fab fa-cpanel',
                'label' => 'cPanel',
                'attributes' => [
                    'data-icon' => 'fab fa-cpanel',
                ],
            ],
            [
                'value' => 'fab fa-creative-commons',
                'label' => 'Creative Commons',
                'attributes' => [
                    'data-icon' => 'fab fa-creative-commons',
                ],
            ],
            [
                'value' => 'fab fa-creative-commons-by',
                'label' => 'Creative Commons Attribution',
                'attributes' => [
                    'data-icon' => 'fab fa-creative-commons-by',
                ],
            ],
            [
                'value' => 'fab fa-creative-commons-nc',
                'label' => 'Creative Commons Noncommercial',
                'attributes' => [
                    'data-icon' => 'fab fa-creative-commons-nc',
                ],
            ],
            [
                'value' => 'fab fa-creative-commons-nc-eu',
                'label' => 'Creative Commons Noncommercial (Euro Sign)',
                'attributes' => [
                    'data-icon' => 'fab fa-creative-commons-nc-eu',
                ],
            ],
            [
                'value' => 'fab fa-creative-commons-nc-jp',
                'label' => 'Creative Commons Noncommercial (Yen Sign)',
                'attributes' => [
                    'data-icon' => 'fab fa-creative-commons-nc-jp',
                ],
            ],
            [
                'value' => 'fab fa-creative-commons-nd',
                'label' => 'Creative Commons No Derivative Works',
                'attributes' => [
                    'data-icon' => 'fab fa-creative-commons-nd',
                ],
            ],
            [
                'value' => 'fab fa-creative-commons-pd',
                'label' => 'Creative Commons Public Domain',
                'attributes' => [
                    'data-icon' => 'fab fa-creative-commons-pd',
                ],
            ],
            [
                'value' => 'fab fa-creative-commons-pd-alt',
                'label' => 'Alternate Creative Commons Public Domain',
                'attributes' => [
                    'data-icon' => 'fab fa-creative-commons-pd-alt',
                ],
            ],
            [
                'value' => 'fab fa-creative-commons-remix',
                'label' => 'Creative Commons Remix',
                'attributes' => [
                    'data-icon' => 'fab fa-creative-commons-remix',
                ],
            ],
            [
                'value' => 'fab fa-creative-commons-sa',
                'label' => 'Creative Commons Share Alike',
                'attributes' => [
                    'data-icon' => 'fab fa-creative-commons-sa',
                ],
            ],
            [
                'value' => 'fab fa-creative-commons-sampling',
                'label' => 'Creative Commons Sampling',
                'attributes' => [
                    'data-icon' => 'fab fa-creative-commons-sampling',
                ],
            ],
            [
                'value' => 'fab fa-creative-commons-sampling-plus',
                'label' => 'Creative Commons Sampling +',
                'attributes' => [
                    'data-icon' => 'fab fa-creative-commons-sampling-plus',
                ],
            ],
            [
                'value' => 'fab fa-creative-commons-share',
                'label' => 'Creative Commons Share',
                'attributes' => [
                    'data-icon' => 'fab fa-creative-commons-share',
                ],
            ],
            [
                'value' => 'fab fa-creative-commons-zero',
                'label' => 'Creative Commons CC0',
                'attributes' => [
                    'data-icon' => 'fab fa-creative-commons-zero',
                ],
            ],
            [
                'value' => 'fas fa-credit-card',
                'label' => 'Credit Card',
                'attributes' => [
                    'data-icon' => 'fas fa-credit-card',
                ],
            ],
            [
                'value' => 'far fa-credit-card',
                'label' => 'Credit Card',
                'attributes' => [
                    'data-icon' => 'far fa-credit-card',
                ],
            ],
            [
                'value' => 'fab fa-critical-role',
                'label' => 'Critical Role',
                'attributes' => [
                    'data-icon' => 'fab fa-critical-role',
                ],
            ],
            [
                'value' => 'fas fa-crop',
                'label' => 'crop',
                'attributes' => [
                    'data-icon' => 'fas fa-crop',
                ],
            ],
            [
                'value' => 'fas fa-crop-simple',
                'label' => 'Crop simple',
                'attributes' => [
                    'data-icon' => 'fas fa-crop-simple',
                ],
            ],
            [
                'value' => 'fas fa-cross',
                'label' => 'Cross',
                'attributes' => [
                    'data-icon' => 'fas fa-cross',
                ],
            ],
            [
                'value' => 'fas fa-crosshairs',
                'label' => 'Crosshairs',
                'attributes' => [
                    'data-icon' => 'fas fa-crosshairs',
                ],
            ],
            [
                'value' => 'fas fa-crow',
                'label' => 'Crow',
                'attributes' => [
                    'data-icon' => 'fas fa-crow',
                ],
            ],
            [
                'value' => 'fas fa-crown',
                'label' => 'Crown',
                'attributes' => [
                    'data-icon' => 'fas fa-crown',
                ],
            ],
            [
                'value' => 'fas fa-crutch',
                'label' => 'Crutch',
                'attributes' => [
                    'data-icon' => 'fas fa-crutch',
                ],
            ],
            [
                'value' => 'fas fa-cruzeiro-sign',
                'label' => 'Cruzeiro Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-cruzeiro-sign',
                ],
            ],
            [
                'value' => 'fab fa-css3',
                'label' => 'CSS 3 Logo',
                'attributes' => [
                    'data-icon' => 'fab fa-css3',
                ],
            ],
            [
                'value' => 'fab fa-css3-alt',
                'label' => 'Alternate CSS3 Logo',
                'attributes' => [
                    'data-icon' => 'fab fa-css3-alt',
                ],
            ],
            [
                'value' => 'fas fa-cube',
                'label' => 'Cube',
                'attributes' => [
                    'data-icon' => 'fas fa-cube',
                ],
            ],
            [
                'value' => 'fas fa-cubes',
                'label' => 'Cubes',
                'attributes' => [
                    'data-icon' => 'fas fa-cubes',
                ],
            ],
            [
                'value' => 'fas fa-cubes-stacked',
                'label' => 'Cubes Stacked',
                'attributes' => [
                    'data-icon' => 'fas fa-cubes-stacked',
                ],
            ],
            [
                'value' => 'fab fa-cuttlefish',
                'label' => 'Cuttlefish',
                'attributes' => [
                    'data-icon' => 'fab fa-cuttlefish',
                ],
            ],
            [
                'value' => 'fas fa-d',
                'label' => 'D',
                'attributes' => [
                    'data-icon' => 'fas fa-d',
                ],
            ],
            [
                'value' => 'fab fa-d-and-d',
                'label' => 'Dungeons & Dragons',
                'attributes' => [
                    'data-icon' => 'fab fa-d-and-d',
                ],
            ],
            [
                'value' => 'fab fa-d-and-d-beyond',
                'label' => 'D&D Beyond',
                'attributes' => [
                    'data-icon' => 'fab fa-d-and-d-beyond',
                ],
            ],
            [
                'value' => 'fab fa-dailymotion',
                'label' => 'dailymotion',
                'attributes' => [
                    'data-icon' => 'fab fa-dailymotion',
                ],
            ],
            [
                'value' => 'fab fa-dashcube',
                'label' => 'DashCube',
                'attributes' => [
                    'data-icon' => 'fab fa-dashcube',
                ],
            ],
            [
                'value' => 'fas fa-database',
                'label' => 'Database',
                'attributes' => [
                    'data-icon' => 'fas fa-database',
                ],
            ],
            [
                'value' => 'fab fa-deezer',
                'label' => 'Deezer',
                'attributes' => [
                    'data-icon' => 'fab fa-deezer',
                ],
            ],
            [
                'value' => 'fas fa-delete-left',
                'label' => 'Delete left',
                'attributes' => [
                    'data-icon' => 'fas fa-delete-left',
                ],
            ],
            [
                'value' => 'fab fa-delicious',
                'label' => 'Delicious',
                'attributes' => [
                    'data-icon' => 'fab fa-delicious',
                ],
            ],
            [
                'value' => 'fas fa-democrat',
                'label' => 'Democrat',
                'attributes' => [
                    'data-icon' => 'fas fa-democrat',
                ],
            ],
            [
                'value' => 'fab fa-deploydog',
                'label' => 'deploy.dog',
                'attributes' => [
                    'data-icon' => 'fab fa-deploydog',
                ],
            ],
            [
                'value' => 'fab fa-deskpro',
                'label' => 'Deskpro',
                'attributes' => [
                    'data-icon' => 'fab fa-deskpro',
                ],
            ],
            [
                'value' => 'fas fa-desktop',
                'label' => 'Desktop',
                'attributes' => [
                    'data-icon' => 'fas fa-desktop',
                ],
            ],
            [
                'value' => 'fab fa-dev',
                'label' => 'DEV',
                'attributes' => [
                    'data-icon' => 'fab fa-dev',
                ],
            ],
            [
                'value' => 'fab fa-deviantart',
                'label' => 'deviantART',
                'attributes' => [
                    'data-icon' => 'fab fa-deviantart',
                ],
            ],
            [
                'value' => 'fas fa-dharmachakra',
                'label' => 'Dharmachakra',
                'attributes' => [
                    'data-icon' => 'fas fa-dharmachakra',
                ],
            ],
            [
                'value' => 'fab fa-dhl',
                'label' => 'DHL',
                'attributes' => [
                    'data-icon' => 'fab fa-dhl',
                ],
            ],
            [
                'value' => 'fas fa-diagram-next',
                'label' => 'Diagram Next',
                'attributes' => [
                    'data-icon' => 'fas fa-diagram-next',
                ],
            ],
            [
                'value' => 'fas fa-diagram-predecessor',
                'label' => 'Diagram Predecessor',
                'attributes' => [
                    'data-icon' => 'fas fa-diagram-predecessor',
                ],
            ],
            [
                'value' => 'fas fa-diagram-project',
                'label' => 'Project Diagram',
                'attributes' => [
                    'data-icon' => 'fas fa-diagram-project',
                ],
            ],
            [
                'value' => 'fas fa-diagram-successor',
                'label' => 'Diagram Successor',
                'attributes' => [
                    'data-icon' => 'fas fa-diagram-successor',
                ],
            ],
            [
                'value' => 'fas fa-diamond',
                'label' => 'Diamond',
                'attributes' => [
                    'data-icon' => 'fas fa-diamond',
                ],
            ],
            [
                'value' => 'fas fa-diamond-turn-right',
                'label' => 'Diamond turn right',
                'attributes' => [
                    'data-icon' => 'fas fa-diamond-turn-right',
                ],
            ],
            [
                'value' => 'fab fa-diaspora',
                'label' => 'Diaspora',
                'attributes' => [
                    'data-icon' => 'fab fa-diaspora',
                ],
            ],
            [
                'value' => 'fas fa-dice',
                'label' => 'Dice',
                'attributes' => [
                    'data-icon' => 'fas fa-dice',
                ],
            ],
            [
                'value' => 'fas fa-dice-d20',
                'label' => 'Dice D20',
                'attributes' => [
                    'data-icon' => 'fas fa-dice-d20',
                ],
            ],
            [
                'value' => 'fas fa-dice-d6',
                'label' => 'Dice D6',
                'attributes' => [
                    'data-icon' => 'fas fa-dice-d6',
                ],
            ],
            [
                'value' => 'fas fa-dice-five',
                'label' => 'Dice Five',
                'attributes' => [
                    'data-icon' => 'fas fa-dice-five',
                ],
            ],
            [
                'value' => 'fas fa-dice-four',
                'label' => 'Dice Four',
                'attributes' => [
                    'data-icon' => 'fas fa-dice-four',
                ],
            ],
            [
                'value' => 'fas fa-dice-one',
                'label' => 'Dice One',
                'attributes' => [
                    'data-icon' => 'fas fa-dice-one',
                ],
            ],
            [
                'value' => 'fas fa-dice-six',
                'label' => 'Dice Six',
                'attributes' => [
                    'data-icon' => 'fas fa-dice-six',
                ],
            ],
            [
                'value' => 'fas fa-dice-three',
                'label' => 'Dice Three',
                'attributes' => [
                    'data-icon' => 'fas fa-dice-three',
                ],
            ],
            [
                'value' => 'fas fa-dice-two',
                'label' => 'Dice Two',
                'attributes' => [
                    'data-icon' => 'fas fa-dice-two',
                ],
            ],
            [
                'value' => 'fab fa-digg',
                'label' => 'Digg Logo',
                'attributes' => [
                    'data-icon' => 'fab fa-digg',
                ],
            ],
            [
                'value' => 'fab fa-digital-ocean',
                'label' => 'Digital Ocean',
                'attributes' => [
                    'data-icon' => 'fab fa-digital-ocean',
                ],
            ],
            [
                'value' => 'fab fa-discord',
                'label' => 'Discord',
                'attributes' => [
                    'data-icon' => 'fab fa-discord',
                ],
            ],
            [
                'value' => 'fab fa-discourse',
                'label' => 'Discourse',
                'attributes' => [
                    'data-icon' => 'fab fa-discourse',
                ],
            ],
            [
                'value' => 'fas fa-disease',
                'label' => 'Disease',
                'attributes' => [
                    'data-icon' => 'fas fa-disease',
                ],
            ],
            [
                'value' => 'fas fa-display',
                'label' => 'Display',
                'attributes' => [
                    'data-icon' => 'fas fa-display',
                ],
            ],
            [
                'value' => 'fas fa-divide',
                'label' => 'Divide',
                'attributes' => [
                    'data-icon' => 'fas fa-divide',
                ],
            ],
            [
                'value' => 'fas fa-dna',
                'label' => 'DNA',
                'attributes' => [
                    'data-icon' => 'fas fa-dna',
                ],
            ],
            [
                'value' => 'fab fa-dochub',
                'label' => 'DocHub',
                'attributes' => [
                    'data-icon' => 'fab fa-dochub',
                ],
            ],
            [
                'value' => 'fab fa-docker',
                'label' => 'Docker',
                'attributes' => [
                    'data-icon' => 'fab fa-docker',
                ],
            ],
            [
                'value' => 'fas fa-dog',
                'label' => 'Dog',
                'attributes' => [
                    'data-icon' => 'fas fa-dog',
                ],
            ],
            [
                'value' => 'fas fa-dollar-sign',
                'label' => 'Dollar Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-dollar-sign',
                ],
            ],
            [
                'value' => 'fas fa-dolly',
                'label' => 'Dolly',
                'attributes' => [
                    'data-icon' => 'fas fa-dolly',
                ],
            ],
            [
                'value' => 'fas fa-dong-sign',
                'label' => 'Dong Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-dong-sign',
                ],
            ],
            [
                'value' => 'fas fa-door-closed',
                'label' => 'Door Closed',
                'attributes' => [
                    'data-icon' => 'fas fa-door-closed',
                ],
            ],
            [
                'value' => 'fas fa-door-open',
                'label' => 'Door Open',
                'attributes' => [
                    'data-icon' => 'fas fa-door-open',
                ],
            ],
            [
                'value' => 'fas fa-dove',
                'label' => 'Dove',
                'attributes' => [
                    'data-icon' => 'fas fa-dove',
                ],
            ],
            [
                'value' => 'fas fa-down-left-and-up-right-to-center',
                'label' => 'Down left and up right to center',
                'attributes' => [
                    'data-icon' => 'fas fa-down-left-and-up-right-to-center',
                ],
            ],
            [
                'value' => 'fas fa-down-long',
                'label' => 'Down long',
                'attributes' => [
                    'data-icon' => 'fas fa-down-long',
                ],
            ],
            [
                'value' => 'fas fa-download',
                'label' => 'Download',
                'attributes' => [
                    'data-icon' => 'fas fa-download',
                ],
            ],
            [
                'value' => 'fab fa-draft2digital',
                'label' => 'Draft2digital',
                'attributes' => [
                    'data-icon' => 'fab fa-draft2digital',
                ],
            ],
            [
                'value' => 'fas fa-dragon',
                'label' => 'Dragon',
                'attributes' => [
                    'data-icon' => 'fas fa-dragon',
                ],
            ],
            [
                'value' => 'fas fa-draw-polygon',
                'label' => 'Draw Polygon',
                'attributes' => [
                    'data-icon' => 'fas fa-draw-polygon',
                ],
            ],
            [
                'value' => 'fab fa-dribbble',
                'label' => 'Dribbble',
                'attributes' => [
                    'data-icon' => 'fab fa-dribbble',
                ],
            ],
            [
                'value' => 'fab fa-dribbble-square',
                'label' => 'Dribbble Square',
                'attributes' => [
                    'data-icon' => 'fab fa-dribbble-square',
                ],
            ],
            [
                'value' => 'fab fa-dropbox',
                'label' => 'Dropbox',
                'attributes' => [
                    'data-icon' => 'fab fa-dropbox',
                ],
            ],
            [
                'value' => 'fas fa-droplet',
                'label' => 'Droplet',
                'attributes' => [
                    'data-icon' => 'fas fa-droplet',
                ],
            ],
            [
                'value' => 'fas fa-droplet-slash',
                'label' => 'Droplet slash',
                'attributes' => [
                    'data-icon' => 'fas fa-droplet-slash',
                ],
            ],
            [
                'value' => 'fas fa-drum',
                'label' => 'Drum',
                'attributes' => [
                    'data-icon' => 'fas fa-drum',
                ],
            ],
            [
                'value' => 'fas fa-drum-steelpan',
                'label' => 'Drum Steelpan',
                'attributes' => [
                    'data-icon' => 'fas fa-drum-steelpan',
                ],
            ],
            [
                'value' => 'fas fa-drumstick-bite',
                'label' => 'Drumstick with Bite Taken Out',
                'attributes' => [
                    'data-icon' => 'fas fa-drumstick-bite',
                ],
            ],
            [
                'value' => 'fab fa-drupal',
                'label' => 'Drupal Logo',
                'attributes' => [
                    'data-icon' => 'fab fa-drupal',
                ],
            ],
            [
                'value' => 'fas fa-dumbbell',
                'label' => 'Dumbbell',
                'attributes' => [
                    'data-icon' => 'fas fa-dumbbell',
                ],
            ],
            [
                'value' => 'fas fa-dumpster',
                'label' => 'Dumpster',
                'attributes' => [
                    'data-icon' => 'fas fa-dumpster',
                ],
            ],
            [
                'value' => 'fas fa-dumpster-fire',
                'label' => 'Dumpster Fire',
                'attributes' => [
                    'data-icon' => 'fas fa-dumpster-fire',
                ],
            ],
            [
                'value' => 'fas fa-dungeon',
                'label' => 'Dungeon',
                'attributes' => [
                    'data-icon' => 'fas fa-dungeon',
                ],
            ],
            [
                'value' => 'fab fa-dyalog',
                'label' => 'Dyalog',
                'attributes' => [
                    'data-icon' => 'fab fa-dyalog',
                ],
            ],
            [
                'value' => 'fas fa-e',
                'label' => 'E',
                'attributes' => [
                    'data-icon' => 'fas fa-e',
                ],
            ],
            [
                'value' => 'fas fa-ear-deaf',
                'label' => 'Ear deaf',
                'attributes' => [
                    'data-icon' => 'fas fa-ear-deaf',
                ],
            ],
            [
                'value' => 'fas fa-ear-listen',
                'label' => 'Ear listen',
                'attributes' => [
                    'data-icon' => 'fas fa-ear-listen',
                ],
            ],
            [
                'value' => 'fab fa-earlybirds',
                'label' => 'Earlybirds',
                'attributes' => [
                    'data-icon' => 'fab fa-earlybirds',
                ],
            ],
            [
                'value' => 'fas fa-earth-africa',
                'label' => 'Earth Africa',
                'attributes' => [
                    'data-icon' => 'fas fa-earth-africa',
                ],
            ],
            [
                'value' => 'fas fa-earth-americas',
                'label' => 'Earth americas',
                'attributes' => [
                    'data-icon' => 'fas fa-earth-americas',
                ],
            ],
            [
                'value' => 'fas fa-earth-asia',
                'label' => 'Earth Asia',
                'attributes' => [
                    'data-icon' => 'fas fa-earth-asia',
                ],
            ],
            [
                'value' => 'fas fa-earth-europe',
                'label' => 'Earth Europe',
                'attributes' => [
                    'data-icon' => 'fas fa-earth-europe',
                ],
            ],
            [
                'value' => 'fas fa-earth-oceania',
                'label' => 'Earth Oceania',
                'attributes' => [
                    'data-icon' => 'fas fa-earth-oceania',
                ],
            ],
            [
                'value' => 'fab fa-ebay',
                'label' => 'eBay',
                'attributes' => [
                    'data-icon' => 'fab fa-ebay',
                ],
            ],
            [
                'value' => 'fab fa-edge',
                'label' => 'Edge Browser',
                'attributes' => [
                    'data-icon' => 'fab fa-edge',
                ],
            ],
            [
                'value' => 'fab fa-edge-legacy',
                'label' => 'Edge Legacy Browser',
                'attributes' => [
                    'data-icon' => 'fab fa-edge-legacy',
                ],
            ],
            [
                'value' => 'fas fa-egg',
                'label' => 'Egg',
                'attributes' => [
                    'data-icon' => 'fas fa-egg',
                ],
            ],
            [
                'value' => 'fas fa-eject',
                'label' => 'eject',
                'attributes' => [
                    'data-icon' => 'fas fa-eject',
                ],
            ],
            [
                'value' => 'fab fa-elementor',
                'label' => 'Elementor',
                'attributes' => [
                    'data-icon' => 'fab fa-elementor',
                ],
            ],
            [
                'value' => 'fas fa-elevator',
                'label' => 'Elevator',
                'attributes' => [
                    'data-icon' => 'fas fa-elevator',
                ],
            ],
            [
                'value' => 'fas fa-ellipsis',
                'label' => 'Ellipsis',
                'attributes' => [
                    'data-icon' => 'fas fa-ellipsis',
                ],
            ],
            [
                'value' => 'fas fa-ellipsis-vertical',
                'label' => 'Ellipsis vertical',
                'attributes' => [
                    'data-icon' => 'fas fa-ellipsis-vertical',
                ],
            ],
            [
                'value' => 'fab fa-ello',
                'label' => 'Ello',
                'attributes' => [
                    'data-icon' => 'fab fa-ello',
                ],
            ],
            [
                'value' => 'fab fa-ember',
                'label' => 'Ember',
                'attributes' => [
                    'data-icon' => 'fab fa-ember',
                ],
            ],
            [
                'value' => 'fab fa-empire',
                'label' => 'Galactic Empire',
                'attributes' => [
                    'data-icon' => 'fab fa-empire',
                ],
            ],
            [
                'value' => 'fas fa-envelope',
                'label' => 'Envelope',
                'attributes' => [
                    'data-icon' => 'fas fa-envelope',
                ],
            ],
            [
                'value' => 'far fa-envelope',
                'label' => 'Envelope',
                'attributes' => [
                    'data-icon' => 'far fa-envelope',
                ],
            ],
            [
                'value' => 'fas fa-envelope-circle-check',
                'label' => 'Envelope Circle-check',
                'attributes' => [
                    'data-icon' => 'fas fa-envelope-circle-check',
                ],
            ],
            [
                'value' => 'fas fa-envelope-open',
                'label' => 'Envelope Open',
                'attributes' => [
                    'data-icon' => 'fas fa-envelope-open',
                ],
            ],
            [
                'value' => 'far fa-envelope-open',
                'label' => 'Envelope Open',
                'attributes' => [
                    'data-icon' => 'far fa-envelope-open',
                ],
            ],
            [
                'value' => 'fas fa-envelope-open-text',
                'label' => 'Envelope Open-text',
                'attributes' => [
                    'data-icon' => 'fas fa-envelope-open-text',
                ],
            ],
            [
                'value' => 'fas fa-envelopes-bulk',
                'label' => 'Envelopes bulk',
                'attributes' => [
                    'data-icon' => 'fas fa-envelopes-bulk',
                ],
            ],
            [
                'value' => 'fab fa-envira',
                'label' => 'Envira Gallery',
                'attributes' => [
                    'data-icon' => 'fab fa-envira',
                ],
            ],
            [
                'value' => 'fas fa-equals',
                'label' => 'Equals',
                'attributes' => [
                    'data-icon' => 'fas fa-equals',
                ],
            ],
            [
                'value' => 'fas fa-eraser',
                'label' => 'eraser',
                'attributes' => [
                    'data-icon' => 'fas fa-eraser',
                ],
            ],
            [
                'value' => 'fab fa-erlang',
                'label' => 'Erlang',
                'attributes' => [
                    'data-icon' => 'fab fa-erlang',
                ],
            ],
            [
                'value' => 'fab fa-ethereum',
                'label' => 'Ethereum',
                'attributes' => [
                    'data-icon' => 'fab fa-ethereum',
                ],
            ],
            [
                'value' => 'fas fa-ethernet',
                'label' => 'Ethernet',
                'attributes' => [
                    'data-icon' => 'fas fa-ethernet',
                ],
            ],
            [
                'value' => 'fab fa-etsy',
                'label' => 'Etsy',
                'attributes' => [
                    'data-icon' => 'fab fa-etsy',
                ],
            ],
            [
                'value' => 'fas fa-euro-sign',
                'label' => 'Euro Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-euro-sign',
                ],
            ],
            [
                'value' => 'fab fa-evernote',
                'label' => 'Evernote',
                'attributes' => [
                    'data-icon' => 'fab fa-evernote',
                ],
            ],
            [
                'value' => 'fas fa-exclamation',
                'label' => 'exclamation',
                'attributes' => [
                    'data-icon' => 'fas fa-exclamation',
                ],
            ],
            [
                'value' => 'fas fa-expand',
                'label' => 'Expand',
                'attributes' => [
                    'data-icon' => 'fas fa-expand',
                ],
            ],
            [
                'value' => 'fab fa-expeditedssl',
                'label' => 'ExpeditedSSL',
                'attributes' => [
                    'data-icon' => 'fab fa-expeditedssl',
                ],
            ],
            [
                'value' => 'fas fa-explosion',
                'label' => 'Explosion',
                'attributes' => [
                    'data-icon' => 'fas fa-explosion',
                ],
            ],
            [
                'value' => 'fas fa-eye',
                'label' => 'Eye',
                'attributes' => [
                    'data-icon' => 'fas fa-eye',
                ],
            ],
            [
                'value' => 'far fa-eye',
                'label' => 'Eye',
                'attributes' => [
                    'data-icon' => 'far fa-eye',
                ],
            ],
            [
                'value' => 'fas fa-eye-dropper',
                'label' => 'Eye Dropper',
                'attributes' => [
                    'data-icon' => 'fas fa-eye-dropper',
                ],
            ],
            [
                'value' => 'fas fa-eye-low-vision',
                'label' => 'Eye low vision',
                'attributes' => [
                    'data-icon' => 'fas fa-eye-low-vision',
                ],
            ],
            [
                'value' => 'fas fa-eye-slash',
                'label' => 'Eye Slash',
                'attributes' => [
                    'data-icon' => 'fas fa-eye-slash',
                ],
            ],
            [
                'value' => 'far fa-eye-slash',
                'label' => 'Eye Slash',
                'attributes' => [
                    'data-icon' => 'far fa-eye-slash',
                ],
            ],
            [
                'value' => 'fas fa-f',
                'label' => 'F',
                'attributes' => [
                    'data-icon' => 'fas fa-f',
                ],
            ],
            [
                'value' => 'fas fa-face-angry',
                'label' => 'Face angry',
                'attributes' => [
                    'data-icon' => 'fas fa-face-angry',
                ],
            ],
            [
                'value' => 'far fa-face-angry',
                'label' => 'Face angry',
                'attributes' => [
                    'data-icon' => 'far fa-face-angry',
                ],
            ],
            [
                'value' => 'fas fa-face-dizzy',
                'label' => 'Face dizzy',
                'attributes' => [
                    'data-icon' => 'fas fa-face-dizzy',
                ],
            ],
            [
                'value' => 'far fa-face-dizzy',
                'label' => 'Face dizzy',
                'attributes' => [
                    'data-icon' => 'far fa-face-dizzy',
                ],
            ],
            [
                'value' => 'fas fa-face-flushed',
                'label' => 'Face flushed',
                'attributes' => [
                    'data-icon' => 'fas fa-face-flushed',
                ],
            ],
            [
                'value' => 'far fa-face-flushed',
                'label' => 'Face flushed',
                'attributes' => [
                    'data-icon' => 'far fa-face-flushed',
                ],
            ],
            [
                'value' => 'fas fa-face-frown',
                'label' => 'Face frown',
                'attributes' => [
                    'data-icon' => 'fas fa-face-frown',
                ],
            ],
            [
                'value' => 'far fa-face-frown',
                'label' => 'Face frown',
                'attributes' => [
                    'data-icon' => 'far fa-face-frown',
                ],
            ],
            [
                'value' => 'fas fa-face-frown-open',
                'label' => 'Face frown open',
                'attributes' => [
                    'data-icon' => 'fas fa-face-frown-open',
                ],
            ],
            [
                'value' => 'far fa-face-frown-open',
                'label' => 'Face frown open',
                'attributes' => [
                    'data-icon' => 'far fa-face-frown-open',
                ],
            ],
            [
                'value' => 'fas fa-face-grimace',
                'label' => 'Face grimace',
                'attributes' => [
                    'data-icon' => 'fas fa-face-grimace',
                ],
            ],
            [
                'value' => 'far fa-face-grimace',
                'label' => 'Face grimace',
                'attributes' => [
                    'data-icon' => 'far fa-face-grimace',
                ],
            ],
            [
                'value' => 'fas fa-face-grin',
                'label' => 'Face grin',
                'attributes' => [
                    'data-icon' => 'fas fa-face-grin',
                ],
            ],
            [
                'value' => 'far fa-face-grin',
                'label' => 'Face grin',
                'attributes' => [
                    'data-icon' => 'far fa-face-grin',
                ],
            ],
            [
                'value' => 'fas fa-face-grin-beam',
                'label' => 'Face grin beam',
                'attributes' => [
                    'data-icon' => 'fas fa-face-grin-beam',
                ],
            ],
            [
                'value' => 'far fa-face-grin-beam',
                'label' => 'Face grin beam',
                'attributes' => [
                    'data-icon' => 'far fa-face-grin-beam',
                ],
            ],
            [
                'value' => 'fas fa-face-grin-beam-sweat',
                'label' => 'Face grin beam sweat',
                'attributes' => [
                    'data-icon' => 'fas fa-face-grin-beam-sweat',
                ],
            ],
            [
                'value' => 'far fa-face-grin-beam-sweat',
                'label' => 'Face grin beam sweat',
                'attributes' => [
                    'data-icon' => 'far fa-face-grin-beam-sweat',
                ],
            ],
            [
                'value' => 'fas fa-face-grin-hearts',
                'label' => 'Face grin hearts',
                'attributes' => [
                    'data-icon' => 'fas fa-face-grin-hearts',
                ],
            ],
            [
                'value' => 'far fa-face-grin-hearts',
                'label' => 'Face grin hearts',
                'attributes' => [
                    'data-icon' => 'far fa-face-grin-hearts',
                ],
            ],
            [
                'value' => 'fas fa-face-grin-squint',
                'label' => 'Face grin squint',
                'attributes' => [
                    'data-icon' => 'fas fa-face-grin-squint',
                ],
            ],
            [
                'value' => 'far fa-face-grin-squint',
                'label' => 'Face grin squint',
                'attributes' => [
                    'data-icon' => 'far fa-face-grin-squint',
                ],
            ],
            [
                'value' => 'fas fa-face-grin-squint-tears',
                'label' => 'Face grin squint tears',
                'attributes' => [
                    'data-icon' => 'fas fa-face-grin-squint-tears',
                ],
            ],
            [
                'value' => 'far fa-face-grin-squint-tears',
                'label' => 'Face grin squint tears',
                'attributes' => [
                    'data-icon' => 'far fa-face-grin-squint-tears',
                ],
            ],
            [
                'value' => 'fas fa-face-grin-stars',
                'label' => 'Face grin stars',
                'attributes' => [
                    'data-icon' => 'fas fa-face-grin-stars',
                ],
            ],
            [
                'value' => 'far fa-face-grin-stars',
                'label' => 'Face grin stars',
                'attributes' => [
                    'data-icon' => 'far fa-face-grin-stars',
                ],
            ],
            [
                'value' => 'fas fa-face-grin-tears',
                'label' => 'Face grin tears',
                'attributes' => [
                    'data-icon' => 'fas fa-face-grin-tears',
                ],
            ],
            [
                'value' => 'far fa-face-grin-tears',
                'label' => 'Face grin tears',
                'attributes' => [
                    'data-icon' => 'far fa-face-grin-tears',
                ],
            ],
            [
                'value' => 'fas fa-face-grin-tongue',
                'label' => 'Face grin tongue',
                'attributes' => [
                    'data-icon' => 'fas fa-face-grin-tongue',
                ],
            ],
            [
                'value' => 'far fa-face-grin-tongue',
                'label' => 'Face grin tongue',
                'attributes' => [
                    'data-icon' => 'far fa-face-grin-tongue',
                ],
            ],
            [
                'value' => 'fas fa-face-grin-tongue-squint',
                'label' => 'Face grin tongue squint',
                'attributes' => [
                    'data-icon' => 'fas fa-face-grin-tongue-squint',
                ],
            ],
            [
                'value' => 'far fa-face-grin-tongue-squint',
                'label' => 'Face grin tongue squint',
                'attributes' => [
                    'data-icon' => 'far fa-face-grin-tongue-squint',
                ],
            ],
            [
                'value' => 'fas fa-face-grin-tongue-wink',
                'label' => 'Face grin tongue wink',
                'attributes' => [
                    'data-icon' => 'fas fa-face-grin-tongue-wink',
                ],
            ],
            [
                'value' => 'far fa-face-grin-tongue-wink',
                'label' => 'Face grin tongue wink',
                'attributes' => [
                    'data-icon' => 'far fa-face-grin-tongue-wink',
                ],
            ],
            [
                'value' => 'fas fa-face-grin-wide',
                'label' => 'Face grin wide',
                'attributes' => [
                    'data-icon' => 'fas fa-face-grin-wide',
                ],
            ],
            [
                'value' => 'far fa-face-grin-wide',
                'label' => 'Face grin wide',
                'attributes' => [
                    'data-icon' => 'far fa-face-grin-wide',
                ],
            ],
            [
                'value' => 'fas fa-face-grin-wink',
                'label' => 'Face grin wink',
                'attributes' => [
                    'data-icon' => 'fas fa-face-grin-wink',
                ],
            ],
            [
                'value' => 'far fa-face-grin-wink',
                'label' => 'Face grin wink',
                'attributes' => [
                    'data-icon' => 'far fa-face-grin-wink',
                ],
            ],
            [
                'value' => 'fas fa-face-kiss',
                'label' => 'Face kiss',
                'attributes' => [
                    'data-icon' => 'fas fa-face-kiss',
                ],
            ],
            [
                'value' => 'far fa-face-kiss',
                'label' => 'Face kiss',
                'attributes' => [
                    'data-icon' => 'far fa-face-kiss',
                ],
            ],
            [
                'value' => 'fas fa-face-kiss-beam',
                'label' => 'Face Kiss Beam',
                'attributes' => [
                    'data-icon' => 'fas fa-face-kiss-beam',
                ],
            ],
            [
                'value' => 'far fa-face-kiss-beam',
                'label' => 'Face Kiss Beam',
                'attributes' => [
                    'data-icon' => 'far fa-face-kiss-beam',
                ],
            ],
            [
                'value' => 'fas fa-face-kiss-wink-heart',
                'label' => 'Face Kiss Wink Heart',
                'attributes' => [
                    'data-icon' => 'fas fa-face-kiss-wink-heart',
                ],
            ],
            [
                'value' => 'far fa-face-kiss-wink-heart',
                'label' => 'Face Kiss Wink Heart',
                'attributes' => [
                    'data-icon' => 'far fa-face-kiss-wink-heart',
                ],
            ],
            [
                'value' => 'fas fa-face-laugh',
                'label' => 'Face Laugh',
                'attributes' => [
                    'data-icon' => 'fas fa-face-laugh',
                ],
            ],
            [
                'value' => 'far fa-face-laugh',
                'label' => 'Face Laugh',
                'attributes' => [
                    'data-icon' => 'far fa-face-laugh',
                ],
            ],
            [
                'value' => 'fas fa-face-laugh-beam',
                'label' => 'Face Laugh Beam',
                'attributes' => [
                    'data-icon' => 'fas fa-face-laugh-beam',
                ],
            ],
            [
                'value' => 'far fa-face-laugh-beam',
                'label' => 'Face Laugh Beam',
                'attributes' => [
                    'data-icon' => 'far fa-face-laugh-beam',
                ],
            ],
            [
                'value' => 'fas fa-face-laugh-squint',
                'label' => 'Face Laugh Squint',
                'attributes' => [
                    'data-icon' => 'fas fa-face-laugh-squint',
                ],
            ],
            [
                'value' => 'far fa-face-laugh-squint',
                'label' => 'Face Laugh Squint',
                'attributes' => [
                    'data-icon' => 'far fa-face-laugh-squint',
                ],
            ],
            [
                'value' => 'fas fa-face-laugh-wink',
                'label' => 'Face Laugh Wink',
                'attributes' => [
                    'data-icon' => 'fas fa-face-laugh-wink',
                ],
            ],
            [
                'value' => 'far fa-face-laugh-wink',
                'label' => 'Face Laugh Wink',
                'attributes' => [
                    'data-icon' => 'far fa-face-laugh-wink',
                ],
            ],
            [
                'value' => 'fas fa-face-meh',
                'label' => 'Face meh',
                'attributes' => [
                    'data-icon' => 'fas fa-face-meh',
                ],
            ],
            [
                'value' => 'far fa-face-meh',
                'label' => 'Face meh',
                'attributes' => [
                    'data-icon' => 'far fa-face-meh',
                ],
            ],
            [
                'value' => 'fas fa-face-meh-blank',
                'label' => 'Face Meh Blank',
                'attributes' => [
                    'data-icon' => 'fas fa-face-meh-blank',
                ],
            ],
            [
                'value' => 'far fa-face-meh-blank',
                'label' => 'Face Meh Blank',
                'attributes' => [
                    'data-icon' => 'far fa-face-meh-blank',
                ],
            ],
            [
                'value' => 'fas fa-face-rolling-eyes',
                'label' => 'Face Rolling Eyes',
                'attributes' => [
                    'data-icon' => 'fas fa-face-rolling-eyes',
                ],
            ],
            [
                'value' => 'far fa-face-rolling-eyes',
                'label' => 'Face Rolling Eyes',
                'attributes' => [
                    'data-icon' => 'far fa-face-rolling-eyes',
                ],
            ],
            [
                'value' => 'fas fa-face-sad-cry',
                'label' => 'Face Sad Cry',
                'attributes' => [
                    'data-icon' => 'fas fa-face-sad-cry',
                ],
            ],
            [
                'value' => 'far fa-face-sad-cry',
                'label' => 'Face Sad Cry',
                'attributes' => [
                    'data-icon' => 'far fa-face-sad-cry',
                ],
            ],
            [
                'value' => 'fas fa-face-sad-tear',
                'label' => 'Face Sad Tear',
                'attributes' => [
                    'data-icon' => 'fas fa-face-sad-tear',
                ],
            ],
            [
                'value' => 'far fa-face-sad-tear',
                'label' => 'Face Sad Tear',
                'attributes' => [
                    'data-icon' => 'far fa-face-sad-tear',
                ],
            ],
            [
                'value' => 'fas fa-face-smile',
                'label' => 'Face Smile',
                'attributes' => [
                    'data-icon' => 'fas fa-face-smile',
                ],
            ],
            [
                'value' => 'far fa-face-smile',
                'label' => 'Face Smile',
                'attributes' => [
                    'data-icon' => 'far fa-face-smile',
                ],
            ],
            [
                'value' => 'fas fa-face-smile-beam',
                'label' => 'Face Smile Beam',
                'attributes' => [
                    'data-icon' => 'fas fa-face-smile-beam',
                ],
            ],
            [
                'value' => 'far fa-face-smile-beam',
                'label' => 'Face Smile Beam',
                'attributes' => [
                    'data-icon' => 'far fa-face-smile-beam',
                ],
            ],
            [
                'value' => 'fas fa-face-smile-wink',
                'label' => 'Face Smile Wink',
                'attributes' => [
                    'data-icon' => 'fas fa-face-smile-wink',
                ],
            ],
            [
                'value' => 'far fa-face-smile-wink',
                'label' => 'Face Smile Wink',
                'attributes' => [
                    'data-icon' => 'far fa-face-smile-wink',
                ],
            ],
            [
                'value' => 'fas fa-face-surprise',
                'label' => 'Face Surprise',
                'attributes' => [
                    'data-icon' => 'fas fa-face-surprise',
                ],
            ],
            [
                'value' => 'far fa-face-surprise',
                'label' => 'Face Surprise',
                'attributes' => [
                    'data-icon' => 'far fa-face-surprise',
                ],
            ],
            [
                'value' => 'fas fa-face-tired',
                'label' => 'Face Tired',
                'attributes' => [
                    'data-icon' => 'fas fa-face-tired',
                ],
            ],
            [
                'value' => 'far fa-face-tired',
                'label' => 'Face Tired',
                'attributes' => [
                    'data-icon' => 'far fa-face-tired',
                ],
            ],
            [
                'value' => 'fab fa-facebook',
                'label' => 'Facebook',
                'attributes' => [
                    'data-icon' => 'fab fa-facebook',
                ],
            ],
            [
                'value' => 'fab fa-facebook-f',
                'label' => 'Facebook F',
                'attributes' => [
                    'data-icon' => 'fab fa-facebook-f',
                ],
            ],
            [
                'value' => 'fab fa-facebook-messenger',
                'label' => 'Facebook Messenger',
                'attributes' => [
                    'data-icon' => 'fab fa-facebook-messenger',
                ],
            ],
            [
                'value' => 'fab fa-facebook-square',
                'label' => 'Facebook Square',
                'attributes' => [
                    'data-icon' => 'fab fa-facebook-square',
                ],
            ],
            [
                'value' => 'fas fa-fan',
                'label' => 'Fan',
                'attributes' => [
                    'data-icon' => 'fas fa-fan',
                ],
            ],
            [
                'value' => 'fab fa-fantasy-flight-games',
                'label' => 'Fantasy Flight-games',
                'attributes' => [
                    'data-icon' => 'fab fa-fantasy-flight-games',
                ],
            ],
            [
                'value' => 'fas fa-faucet',
                'label' => 'Faucet',
                'attributes' => [
                    'data-icon' => 'fas fa-faucet',
                ],
            ],
            [
                'value' => 'fas fa-faucet-drip',
                'label' => 'Faucet Drip',
                'attributes' => [
                    'data-icon' => 'fas fa-faucet-drip',
                ],
            ],
            [
                'value' => 'fas fa-fax',
                'label' => 'Fax',
                'attributes' => [
                    'data-icon' => 'fas fa-fax',
                ],
            ],
            [
                'value' => 'fas fa-feather',
                'label' => 'Feather',
                'attributes' => [
                    'data-icon' => 'fas fa-feather',
                ],
            ],
            [
                'value' => 'fas fa-feather-pointed',
                'label' => 'Feather pointed',
                'attributes' => [
                    'data-icon' => 'fas fa-feather-pointed',
                ],
            ],
            [
                'value' => 'fab fa-fedex',
                'label' => 'FedEx',
                'attributes' => [
                    'data-icon' => 'fab fa-fedex',
                ],
            ],
            [
                'value' => 'fab fa-fedora',
                'label' => 'Fedora',
                'attributes' => [
                    'data-icon' => 'fab fa-fedora',
                ],
            ],
            [
                'value' => 'fas fa-ferry',
                'label' => 'Ferry',
                'attributes' => [
                    'data-icon' => 'fas fa-ferry',
                ],
            ],
            [
                'value' => 'fab fa-figma',
                'label' => 'Figma',
                'attributes' => [
                    'data-icon' => 'fab fa-figma',
                ],
            ],
            [
                'value' => 'fas fa-file',
                'label' => 'File',
                'attributes' => [
                    'data-icon' => 'fas fa-file',
                ],
            ],
            [
                'value' => 'far fa-file',
                'label' => 'File',
                'attributes' => [
                    'data-icon' => 'far fa-file',
                ],
            ],
            [
                'value' => 'fas fa-file-arrow-down',
                'label' => 'File arrow down',
                'attributes' => [
                    'data-icon' => 'fas fa-file-arrow-down',
                ],
            ],
            [
                'value' => 'fas fa-file-arrow-up',
                'label' => 'File arrow up',
                'attributes' => [
                    'data-icon' => 'fas fa-file-arrow-up',
                ],
            ],
            [
                'value' => 'fas fa-file-audio',
                'label' => 'Audio File',
                'attributes' => [
                    'data-icon' => 'fas fa-file-audio',
                ],
            ],
            [
                'value' => 'far fa-file-audio',
                'label' => 'Audio File',
                'attributes' => [
                    'data-icon' => 'far fa-file-audio',
                ],
            ],
            [
                'value' => 'fas fa-file-circle-check',
                'label' => 'File Circle-Check',
                'attributes' => [
                    'data-icon' => 'fas fa-file-circle-check',
                ],
            ],
            [
                'value' => 'fas fa-file-circle-exclamation',
                'label' => 'File Circle-exclamation',
                'attributes' => [
                    'data-icon' => 'fas fa-file-circle-exclamation',
                ],
            ],
            [
                'value' => 'fas fa-file-circle-minus',
                'label' => 'File Circle-minus',
                'attributes' => [
                    'data-icon' => 'fas fa-file-circle-minus',
                ],
            ],
            [
                'value' => 'fas fa-file-circle-plus',
                'label' => 'File Circle-plus',
                'attributes' => [
                    'data-icon' => 'fas fa-file-circle-plus',
                ],
            ],
            [
                'value' => 'fas fa-file-circle-question',
                'label' => 'File Circle-question',
                'attributes' => [
                    'data-icon' => 'fas fa-file-circle-question',
                ],
            ],
            [
                'value' => 'fas fa-file-circle-xmark',
                'label' => 'File Circle-xmark',
                'attributes' => [
                    'data-icon' => 'fas fa-file-circle-xmark',
                ],
            ],
            [
                'value' => 'fas fa-file-code',
                'label' => 'Code File',
                'attributes' => [
                    'data-icon' => 'fas fa-file-code',
                ],
            ],
            [
                'value' => 'far fa-file-code',
                'label' => 'Code File',
                'attributes' => [
                    'data-icon' => 'far fa-file-code',
                ],
            ],
            [
                'value' => 'fas fa-file-contract',
                'label' => 'File Contract',
                'attributes' => [
                    'data-icon' => 'fas fa-file-contract',
                ],
            ],
            [
                'value' => 'fas fa-file-csv',
                'label' => 'File CSV',
                'attributes' => [
                    'data-icon' => 'fas fa-file-csv',
                ],
            ],
            [
                'value' => 'fas fa-file-excel',
                'label' => 'Excel File',
                'attributes' => [
                    'data-icon' => 'fas fa-file-excel',
                ],
            ],
            [
                'value' => 'far fa-file-excel',
                'label' => 'Excel File',
                'attributes' => [
                    'data-icon' => 'far fa-file-excel',
                ],
            ],
            [
                'value' => 'fas fa-file-export',
                'label' => 'File Export',
                'attributes' => [
                    'data-icon' => 'fas fa-file-export',
                ],
            ],
            [
                'value' => 'fas fa-file-image',
                'label' => 'Image File',
                'attributes' => [
                    'data-icon' => 'fas fa-file-image',
                ],
            ],
            [
                'value' => 'far fa-file-image',
                'label' => 'Image File',
                'attributes' => [
                    'data-icon' => 'far fa-file-image',
                ],
            ],
            [
                'value' => 'fas fa-file-import',
                'label' => 'File Import',
                'attributes' => [
                    'data-icon' => 'fas fa-file-import',
                ],
            ],
            [
                'value' => 'fas fa-file-invoice',
                'label' => 'File Invoice',
                'attributes' => [
                    'data-icon' => 'fas fa-file-invoice',
                ],
            ],
            [
                'value' => 'fas fa-file-invoice-dollar',
                'label' => 'File Invoice with US Dollar',
                'attributes' => [
                    'data-icon' => 'fas fa-file-invoice-dollar',
                ],
            ],
            [
                'value' => 'fas fa-file-lines',
                'label' => 'File lines',
                'attributes' => [
                    'data-icon' => 'fas fa-file-lines',
                ],
            ],
            [
                'value' => 'far fa-file-lines',
                'label' => 'File lines',
                'attributes' => [
                    'data-icon' => 'far fa-file-lines',
                ],
            ],
            [
                'value' => 'fas fa-file-medical',
                'label' => 'Medical File',
                'attributes' => [
                    'data-icon' => 'fas fa-file-medical',
                ],
            ],
            [
                'value' => 'fas fa-file-pdf',
                'label' => 'PDF File',
                'attributes' => [
                    'data-icon' => 'fas fa-file-pdf',
                ],
            ],
            [
                'value' => 'far fa-file-pdf',
                'label' => 'PDF File',
                'attributes' => [
                    'data-icon' => 'far fa-file-pdf',
                ],
            ],
            [
                'value' => 'fas fa-file-pen',
                'label' => 'File pen',
                'attributes' => [
                    'data-icon' => 'fas fa-file-pen',
                ],
            ],
            [
                'value' => 'fas fa-file-powerpoint',
                'label' => 'Powerpoint File',
                'attributes' => [
                    'data-icon' => 'fas fa-file-powerpoint',
                ],
            ],
            [
                'value' => 'far fa-file-powerpoint',
                'label' => 'Powerpoint File',
                'attributes' => [
                    'data-icon' => 'far fa-file-powerpoint',
                ],
            ],
            [
                'value' => 'fas fa-file-prescription',
                'label' => 'File Prescription',
                'attributes' => [
                    'data-icon' => 'fas fa-file-prescription',
                ],
            ],
            [
                'value' => 'fas fa-file-shield',
                'label' => 'File Shield',
                'attributes' => [
                    'data-icon' => 'fas fa-file-shield',
                ],
            ],
            [
                'value' => 'fas fa-file-signature',
                'label' => 'File Signature',
                'attributes' => [
                    'data-icon' => 'fas fa-file-signature',
                ],
            ],
            [
                'value' => 'fas fa-file-video',
                'label' => 'Video File',
                'attributes' => [
                    'data-icon' => 'fas fa-file-video',
                ],
            ],
            [
                'value' => 'far fa-file-video',
                'label' => 'Video File',
                'attributes' => [
                    'data-icon' => 'far fa-file-video',
                ],
            ],
            [
                'value' => 'fas fa-file-waveform',
                'label' => 'File waveform',
                'attributes' => [
                    'data-icon' => 'fas fa-file-waveform',
                ],
            ],
            [
                'value' => 'fas fa-file-word',
                'label' => 'Word File',
                'attributes' => [
                    'data-icon' => 'fas fa-file-word',
                ],
            ],
            [
                'value' => 'far fa-file-word',
                'label' => 'Word File',
                'attributes' => [
                    'data-icon' => 'far fa-file-word',
                ],
            ],
            [
                'value' => 'fas fa-file-zipper',
                'label' => 'File zipper',
                'attributes' => [
                    'data-icon' => 'fas fa-file-zipper',
                ],
            ],
            [
                'value' => 'far fa-file-zipper',
                'label' => 'File zipper',
                'attributes' => [
                    'data-icon' => 'far fa-file-zipper',
                ],
            ],
            [
                'value' => 'fas fa-fill',
                'label' => 'Fill',
                'attributes' => [
                    'data-icon' => 'fas fa-fill',
                ],
            ],
            [
                'value' => 'fas fa-fill-drip',
                'label' => 'Fill Drip',
                'attributes' => [
                    'data-icon' => 'fas fa-fill-drip',
                ],
            ],
            [
                'value' => 'fas fa-film',
                'label' => 'Film',
                'attributes' => [
                    'data-icon' => 'fas fa-film',
                ],
            ],
            [
                'value' => 'fas fa-filter',
                'label' => 'Filter',
                'attributes' => [
                    'data-icon' => 'fas fa-filter',
                ],
            ],
            [
                'value' => 'fas fa-filter-circle-dollar',
                'label' => 'Filter Circle Dollar',
                'attributes' => [
                    'data-icon' => 'fas fa-filter-circle-dollar',
                ],
            ],
            [
                'value' => 'fas fa-filter-circle-xmark',
                'label' => 'Filter Circle X Mark',
                'attributes' => [
                    'data-icon' => 'fas fa-filter-circle-xmark',
                ],
            ],
            [
                'value' => 'fas fa-fingerprint',
                'label' => 'Fingerprint',
                'attributes' => [
                    'data-icon' => 'fas fa-fingerprint',
                ],
            ],
            [
                'value' => 'fas fa-fire',
                'label' => 'fire',
                'attributes' => [
                    'data-icon' => 'fas fa-fire',
                ],
            ],
            [
                'value' => 'fas fa-fire-burner',
                'label' => 'Fire Burner',
                'attributes' => [
                    'data-icon' => 'fas fa-fire-burner',
                ],
            ],
            [
                'value' => 'fas fa-fire-extinguisher',
                'label' => 'fire-extinguisher',
                'attributes' => [
                    'data-icon' => 'fas fa-fire-extinguisher',
                ],
            ],
            [
                'value' => 'fas fa-fire-flame-curved',
                'label' => 'Fire flame curved',
                'attributes' => [
                    'data-icon' => 'fas fa-fire-flame-curved',
                ],
            ],
            [
                'value' => 'fas fa-fire-flame-simple',
                'label' => 'Fire flame simple',
                'attributes' => [
                    'data-icon' => 'fas fa-fire-flame-simple',
                ],
            ],
            [
                'value' => 'fab fa-firefox',
                'label' => 'Firefox',
                'attributes' => [
                    'data-icon' => 'fab fa-firefox',
                ],
            ],
            [
                'value' => 'fab fa-firefox-browser',
                'label' => 'Firefox Browser',
                'attributes' => [
                    'data-icon' => 'fab fa-firefox-browser',
                ],
            ],
            [
                'value' => 'fab fa-first-order',
                'label' => 'First Order',
                'attributes' => [
                    'data-icon' => 'fab fa-first-order',
                ],
            ],
            [
                'value' => 'fab fa-first-order-alt',
                'label' => 'Alternate First Order',
                'attributes' => [
                    'data-icon' => 'fab fa-first-order-alt',
                ],
            ],
            [
                'value' => 'fab fa-firstdraft',
                'label' => 'firstdraft',
                'attributes' => [
                    'data-icon' => 'fab fa-firstdraft',
                ],
            ],
            [
                'value' => 'fas fa-fish',
                'label' => 'Fish',
                'attributes' => [
                    'data-icon' => 'fas fa-fish',
                ],
            ],
            [
                'value' => 'fas fa-fish-fins',
                'label' => 'Fish Fins',
                'attributes' => [
                    'data-icon' => 'fas fa-fish-fins',
                ],
            ],
            [
                'value' => 'fas fa-flag',
                'label' => 'flag',
                'attributes' => [
                    'data-icon' => 'fas fa-flag',
                ],
            ],
            [
                'value' => 'far fa-flag',
                'label' => 'flag',
                'attributes' => [
                    'data-icon' => 'far fa-flag',
                ],
            ],
            [
                'value' => 'fas fa-flag-checkered',
                'label' => 'flag-checkered',
                'attributes' => [
                    'data-icon' => 'fas fa-flag-checkered',
                ],
            ],
            [
                'value' => 'fas fa-flag-usa',
                'label' => 'United States of America Flag',
                'attributes' => [
                    'data-icon' => 'fas fa-flag-usa',
                ],
            ],
            [
                'value' => 'fas fa-flask',
                'label' => 'Flask',
                'attributes' => [
                    'data-icon' => 'fas fa-flask',
                ],
            ],
            [
                'value' => 'fas fa-flask-vial',
                'label' => 'Flask and Vial',
                'attributes' => [
                    'data-icon' => 'fas fa-flask-vial',
                ],
            ],
            [
                'value' => 'fab fa-flickr',
                'label' => 'Flickr',
                'attributes' => [
                    'data-icon' => 'fab fa-flickr',
                ],
            ],
            [
                'value' => 'fab fa-flipboard',
                'label' => 'Flipboard',
                'attributes' => [
                    'data-icon' => 'fab fa-flipboard',
                ],
            ],
            [
                'value' => 'fas fa-floppy-disk',
                'label' => 'Floppy Disk',
                'attributes' => [
                    'data-icon' => 'fas fa-floppy-disk',
                ],
            ],
            [
                'value' => 'far fa-floppy-disk',
                'label' => 'Floppy Disk',
                'attributes' => [
                    'data-icon' => 'far fa-floppy-disk',
                ],
            ],
            [
                'value' => 'fas fa-florin-sign',
                'label' => 'Florin Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-florin-sign',
                ],
            ],
            [
                'value' => 'fab fa-fly',
                'label' => 'Fly',
                'attributes' => [
                    'data-icon' => 'fab fa-fly',
                ],
            ],
            [
                'value' => 'fas fa-folder',
                'label' => 'Folder',
                'attributes' => [
                    'data-icon' => 'fas fa-folder',
                ],
            ],
            [
                'value' => 'far fa-folder',
                'label' => 'Folder',
                'attributes' => [
                    'data-icon' => 'far fa-folder',
                ],
            ],
            [
                'value' => 'fas fa-folder-closed',
                'label' => 'Folder Closed',
                'attributes' => [
                    'data-icon' => 'fas fa-folder-closed',
                ],
            ],
            [
                'value' => 'far fa-folder-closed',
                'label' => 'Folder Closed',
                'attributes' => [
                    'data-icon' => 'far fa-folder-closed',
                ],
            ],
            [
                'value' => 'fas fa-folder-minus',
                'label' => 'Folder Minus',
                'attributes' => [
                    'data-icon' => 'fas fa-folder-minus',
                ],
            ],
            [
                'value' => 'fas fa-folder-open',
                'label' => 'Folder Open',
                'attributes' => [
                    'data-icon' => 'fas fa-folder-open',
                ],
            ],
            [
                'value' => 'far fa-folder-open',
                'label' => 'Folder Open',
                'attributes' => [
                    'data-icon' => 'far fa-folder-open',
                ],
            ],
            [
                'value' => 'fas fa-folder-plus',
                'label' => 'Folder Plus',
                'attributes' => [
                    'data-icon' => 'fas fa-folder-plus',
                ],
            ],
            [
                'value' => 'fas fa-folder-tree',
                'label' => 'Folder Tree',
                'attributes' => [
                    'data-icon' => 'fas fa-folder-tree',
                ],
            ],
            [
                'value' => 'fas fa-font',
                'label' => 'font',
                'attributes' => [
                    'data-icon' => 'fas fa-font',
                ],
            ],
            [
                'value' => 'fas fa-font-awesome',
                'label' => 'Font Awesome',
                'attributes' => [
                    'data-icon' => 'fas fa-font-awesome',
                ],
            ],
            [
                'value' => 'far fa-font-awesome',
                'label' => 'Font Awesome',
                'attributes' => [
                    'data-icon' => 'far fa-font-awesome',
                ],
            ],
            [
                'value' => 'fab fa-font-awesome',
                'label' => 'Font Awesome',
                'attributes' => [
                    'data-icon' => 'fab fa-font-awesome',
                ],
            ],
            [
                'value' => 'fab fa-fonticons',
                'label' => 'Fonticons',
                'attributes' => [
                    'data-icon' => 'fab fa-fonticons',
                ],
            ],
            [
                'value' => 'fab fa-fonticons-fi',
                'label' => 'Fonticons Fi',
                'attributes' => [
                    'data-icon' => 'fab fa-fonticons-fi',
                ],
            ],
            [
                'value' => 'fas fa-football',
                'label' => 'Football Ball',
                'attributes' => [
                    'data-icon' => 'fas fa-football',
                ],
            ],
            [
                'value' => 'fab fa-fort-awesome',
                'label' => 'Fort Awesome',
                'attributes' => [
                    'data-icon' => 'fab fa-fort-awesome',
                ],
            ],
            [
                'value' => 'fab fa-fort-awesome-alt',
                'label' => 'Alternate Fort Awesome',
                'attributes' => [
                    'data-icon' => 'fab fa-fort-awesome-alt',
                ],
            ],
            [
                'value' => 'fab fa-forumbee',
                'label' => 'Forumbee',
                'attributes' => [
                    'data-icon' => 'fab fa-forumbee',
                ],
            ],
            [
                'value' => 'fas fa-forward',
                'label' => 'forward',
                'attributes' => [
                    'data-icon' => 'fas fa-forward',
                ],
            ],
            [
                'value' => 'fas fa-forward-fast',
                'label' => 'Forward fast',
                'attributes' => [
                    'data-icon' => 'fas fa-forward-fast',
                ],
            ],
            [
                'value' => 'fas fa-forward-step',
                'label' => 'Forward step',
                'attributes' => [
                    'data-icon' => 'fas fa-forward-step',
                ],
            ],
            [
                'value' => 'fab fa-foursquare',
                'label' => 'Foursquare',
                'attributes' => [
                    'data-icon' => 'fab fa-foursquare',
                ],
            ],
            [
                'value' => 'fas fa-franc-sign',
                'label' => 'Franc Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-franc-sign',
                ],
            ],
            [
                'value' => 'fab fa-free-code-camp',
                'label' => 'freeCodeCamp',
                'attributes' => [
                    'data-icon' => 'fab fa-free-code-camp',
                ],
            ],
            [
                'value' => 'fab fa-freebsd',
                'label' => 'FreeBSD',
                'attributes' => [
                    'data-icon' => 'fab fa-freebsd',
                ],
            ],
            [
                'value' => 'fas fa-frog',
                'label' => 'Frog',
                'attributes' => [
                    'data-icon' => 'fas fa-frog',
                ],
            ],
            [
                'value' => 'fab fa-fulcrum',
                'label' => 'Fulcrum',
                'attributes' => [
                    'data-icon' => 'fab fa-fulcrum',
                ],
            ],
            [
                'value' => 'fas fa-futbol',
                'label' => 'Futbol ball',
                'attributes' => [
                    'data-icon' => 'fas fa-futbol',
                ],
            ],
            [
                'value' => 'far fa-futbol',
                'label' => 'Futbol ball',
                'attributes' => [
                    'data-icon' => 'far fa-futbol',
                ],
            ],
            [
                'value' => 'fas fa-g',
                'label' => 'G',
                'attributes' => [
                    'data-icon' => 'fas fa-g',
                ],
            ],
            [
                'value' => 'fab fa-galactic-republic',
                'label' => 'Galactic Republic',
                'attributes' => [
                    'data-icon' => 'fab fa-galactic-republic',
                ],
            ],
            [
                'value' => 'fab fa-galactic-senate',
                'label' => 'Galactic Senate',
                'attributes' => [
                    'data-icon' => 'fab fa-galactic-senate',
                ],
            ],
            [
                'value' => 'fas fa-gamepad',
                'label' => 'Gamepad',
                'attributes' => [
                    'data-icon' => 'fas fa-gamepad',
                ],
            ],
            [
                'value' => 'fas fa-gas-pump',
                'label' => 'Gas Pump',
                'attributes' => [
                    'data-icon' => 'fas fa-gas-pump',
                ],
            ],
            [
                'value' => 'fas fa-gauge',
                'label' => 'Gauge med',
                'attributes' => [
                    'data-icon' => 'fas fa-gauge',
                ],
            ],
            [
                'value' => 'fas fa-gauge-high',
                'label' => 'Gauge',
                'attributes' => [
                    'data-icon' => 'fas fa-gauge-high',
                ],
            ],
            [
                'value' => 'fas fa-gauge-simple',
                'label' => 'Gauge simple med',
                'attributes' => [
                    'data-icon' => 'fas fa-gauge-simple',
                ],
            ],
            [
                'value' => 'fas fa-gauge-simple-high',
                'label' => 'Gauge simple',
                'attributes' => [
                    'data-icon' => 'fas fa-gauge-simple-high',
                ],
            ],
            [
                'value' => 'fas fa-gavel',
                'label' => 'Gavel',
                'attributes' => [
                    'data-icon' => 'fas fa-gavel',
                ],
            ],
            [
                'value' => 'fas fa-gear',
                'label' => 'Gear',
                'attributes' => [
                    'data-icon' => 'fas fa-gear',
                ],
            ],
            [
                'value' => 'fas fa-gears',
                'label' => 'Gears',
                'attributes' => [
                    'data-icon' => 'fas fa-gears',
                ],
            ],
            [
                'value' => 'fas fa-gem',
                'label' => 'Gem',
                'attributes' => [
                    'data-icon' => 'fas fa-gem',
                ],
            ],
            [
                'value' => 'far fa-gem',
                'label' => 'Gem',
                'attributes' => [
                    'data-icon' => 'far fa-gem',
                ],
            ],
            [
                'value' => 'fas fa-genderless',
                'label' => 'Genderless',
                'attributes' => [
                    'data-icon' => 'fas fa-genderless',
                ],
            ],
            [
                'value' => 'fab fa-get-pocket',
                'label' => 'Get Pocket',
                'attributes' => [
                    'data-icon' => 'fab fa-get-pocket',
                ],
            ],
            [
                'value' => 'fab fa-gg',
                'label' => 'GG Currency',
                'attributes' => [
                    'data-icon' => 'fab fa-gg',
                ],
            ],
            [
                'value' => 'fab fa-gg-circle',
                'label' => 'GG Currency Circle',
                'attributes' => [
                    'data-icon' => 'fab fa-gg-circle',
                ],
            ],
            [
                'value' => 'fas fa-ghost',
                'label' => 'Ghost',
                'attributes' => [
                    'data-icon' => 'fas fa-ghost',
                ],
            ],
            [
                'value' => 'fas fa-gift',
                'label' => 'gift',
                'attributes' => [
                    'data-icon' => 'fas fa-gift',
                ],
            ],
            [
                'value' => 'fas fa-gifts',
                'label' => 'Gifts',
                'attributes' => [
                    'data-icon' => 'fas fa-gifts',
                ],
            ],
            [
                'value' => 'fab fa-git',
                'label' => 'Git',
                'attributes' => [
                    'data-icon' => 'fab fa-git',
                ],
            ],
            [
                'value' => 'fab fa-git-alt',
                'label' => 'Git Alt',
                'attributes' => [
                    'data-icon' => 'fab fa-git-alt',
                ],
            ],
            [
                'value' => 'fab fa-git-square',
                'label' => 'Git Square',
                'attributes' => [
                    'data-icon' => 'fab fa-git-square',
                ],
            ],
            [
                'value' => 'fab fa-github',
                'label' => 'GitHub',
                'attributes' => [
                    'data-icon' => 'fab fa-github',
                ],
            ],
            [
                'value' => 'fab fa-github-alt',
                'label' => 'Alternate GitHub',
                'attributes' => [
                    'data-icon' => 'fab fa-github-alt',
                ],
            ],
            [
                'value' => 'fab fa-github-square',
                'label' => 'GitHub Square',
                'attributes' => [
                    'data-icon' => 'fab fa-github-square',
                ],
            ],
            [
                'value' => 'fab fa-gitkraken',
                'label' => 'GitKraken',
                'attributes' => [
                    'data-icon' => 'fab fa-gitkraken',
                ],
            ],
            [
                'value' => 'fab fa-gitlab',
                'label' => 'GitLab',
                'attributes' => [
                    'data-icon' => 'fab fa-gitlab',
                ],
            ],
            [
                'value' => 'fab fa-gitter',
                'label' => 'Gitter',
                'attributes' => [
                    'data-icon' => 'fab fa-gitter',
                ],
            ],
            [
                'value' => 'fas fa-glass-water',
                'label' => 'Glass Water',
                'attributes' => [
                    'data-icon' => 'fas fa-glass-water',
                ],
            ],
            [
                'value' => 'fas fa-glass-water-droplet',
                'label' => 'Glass Water-droplet',
                'attributes' => [
                    'data-icon' => 'fas fa-glass-water-droplet',
                ],
            ],
            [
                'value' => 'fas fa-glasses',
                'label' => 'Glasses',
                'attributes' => [
                    'data-icon' => 'fas fa-glasses',
                ],
            ],
            [
                'value' => 'fab fa-glide',
                'label' => 'Glide',
                'attributes' => [
                    'data-icon' => 'fab fa-glide',
                ],
            ],
            [
                'value' => 'fab fa-glide-g',
                'label' => 'Glide G',
                'attributes' => [
                    'data-icon' => 'fab fa-glide-g',
                ],
            ],
            [
                'value' => 'fas fa-globe',
                'label' => 'Globe',
                'attributes' => [
                    'data-icon' => 'fas fa-globe',
                ],
            ],
            [
                'value' => 'fab fa-gofore',
                'label' => 'Gofore',
                'attributes' => [
                    'data-icon' => 'fab fa-gofore',
                ],
            ],
            [
                'value' => 'fab fa-golang',
                'label' => 'Go',
                'attributes' => [
                    'data-icon' => 'fab fa-golang',
                ],
            ],
            [
                'value' => 'fas fa-golf-ball-tee',
                'label' => 'Golf ball tee',
                'attributes' => [
                    'data-icon' => 'fas fa-golf-ball-tee',
                ],
            ],
            [
                'value' => 'fab fa-goodreads',
                'label' => 'Goodreads',
                'attributes' => [
                    'data-icon' => 'fab fa-goodreads',
                ],
            ],
            [
                'value' => 'fab fa-goodreads-g',
                'label' => 'Goodreads G',
                'attributes' => [
                    'data-icon' => 'fab fa-goodreads-g',
                ],
            ],
            [
                'value' => 'fab fa-google',
                'label' => 'Google Logo',
                'attributes' => [
                    'data-icon' => 'fab fa-google',
                ],
            ],
            [
                'value' => 'fab fa-google-drive',
                'label' => 'Google Drive',
                'attributes' => [
                    'data-icon' => 'fab fa-google-drive',
                ],
            ],
            [
                'value' => 'fab fa-google-pay',
                'label' => 'Google Pay',
                'attributes' => [
                    'data-icon' => 'fab fa-google-pay',
                ],
            ],
            [
                'value' => 'fab fa-google-play',
                'label' => 'Google Play',
                'attributes' => [
                    'data-icon' => 'fab fa-google-play',
                ],
            ],
            [
                'value' => 'fab fa-google-plus',
                'label' => 'Google Plus',
                'attributes' => [
                    'data-icon' => 'fab fa-google-plus',
                ],
            ],
            [
                'value' => 'fab fa-google-plus-g',
                'label' => 'Google Plus G',
                'attributes' => [
                    'data-icon' => 'fab fa-google-plus-g',
                ],
            ],
            [
                'value' => 'fab fa-google-plus-square',
                'label' => 'Google Plus Square',
                'attributes' => [
                    'data-icon' => 'fab fa-google-plus-square',
                ],
            ],
            [
                'value' => 'fab fa-google-wallet',
                'label' => 'Google Wallet',
                'attributes' => [
                    'data-icon' => 'fab fa-google-wallet',
                ],
            ],
            [
                'value' => 'fas fa-gopuram',
                'label' => 'Gopuram',
                'attributes' => [
                    'data-icon' => 'fas fa-gopuram',
                ],
            ],
            [
                'value' => 'fas fa-graduation-cap',
                'label' => 'Graduation Cap',
                'attributes' => [
                    'data-icon' => 'fas fa-graduation-cap',
                ],
            ],
            [
                'value' => 'fab fa-gratipay',
                'label' => 'Gratipay (Gittip)',
                'attributes' => [
                    'data-icon' => 'fab fa-gratipay',
                ],
            ],
            [
                'value' => 'fab fa-grav',
                'label' => 'Grav',
                'attributes' => [
                    'data-icon' => 'fab fa-grav',
                ],
            ],
            [
                'value' => 'fas fa-greater-than',
                'label' => 'Greater Than',
                'attributes' => [
                    'data-icon' => 'fas fa-greater-than',
                ],
            ],
            [
                'value' => 'fas fa-greater-than-equal',
                'label' => 'Greater Than Equal To',
                'attributes' => [
                    'data-icon' => 'fas fa-greater-than-equal',
                ],
            ],
            [
                'value' => 'fas fa-grip',
                'label' => 'Grip',
                'attributes' => [
                    'data-icon' => 'fas fa-grip',
                ],
            ],
            [
                'value' => 'fas fa-grip-lines',
                'label' => 'Grip Lines',
                'attributes' => [
                    'data-icon' => 'fas fa-grip-lines',
                ],
            ],
            [
                'value' => 'fas fa-grip-lines-vertical',
                'label' => 'Grip Lines Vertical',
                'attributes' => [
                    'data-icon' => 'fas fa-grip-lines-vertical',
                ],
            ],
            [
                'value' => 'fas fa-grip-vertical',
                'label' => 'Grip Vertical',
                'attributes' => [
                    'data-icon' => 'fas fa-grip-vertical',
                ],
            ],
            [
                'value' => 'fab fa-gripfire',
                'label' => 'Gripfire, Inc.',
                'attributes' => [
                    'data-icon' => 'fab fa-gripfire',
                ],
            ],
            [
                'value' => 'fas fa-group-arrows-rotate',
                'label' => 'Group Arrows-rotate',
                'attributes' => [
                    'data-icon' => 'fas fa-group-arrows-rotate',
                ],
            ],
            [
                'value' => 'fab fa-grunt',
                'label' => 'Grunt',
                'attributes' => [
                    'data-icon' => 'fab fa-grunt',
                ],
            ],
            [
                'value' => 'fas fa-guarani-sign',
                'label' => 'Guarani Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-guarani-sign',
                ],
            ],
            [
                'value' => 'fab fa-guilded',
                'label' => 'Guilded',
                'attributes' => [
                    'data-icon' => 'fab fa-guilded',
                ],
            ],
            [
                'value' => 'fas fa-guitar',
                'label' => 'Guitar',
                'attributes' => [
                    'data-icon' => 'fas fa-guitar',
                ],
            ],
            [
                'value' => 'fab fa-gulp',
                'label' => 'Gulp',
                'attributes' => [
                    'data-icon' => 'fab fa-gulp',
                ],
            ],
            [
                'value' => 'fas fa-gun',
                'label' => 'Gun',
                'attributes' => [
                    'data-icon' => 'fas fa-gun',
                ],
            ],
            [
                'value' => 'fas fa-h',
                'label' => 'H',
                'attributes' => [
                    'data-icon' => 'fas fa-h',
                ],
            ],
            [
                'value' => 'fab fa-hacker-news',
                'label' => 'Hacker News',
                'attributes' => [
                    'data-icon' => 'fab fa-hacker-news',
                ],
            ],
            [
                'value' => 'fab fa-hacker-news-square',
                'label' => 'Hacker News Square',
                'attributes' => [
                    'data-icon' => 'fab fa-hacker-news-square',
                ],
            ],
            [
                'value' => 'fab fa-hackerrank',
                'label' => 'Hackerrank',
                'attributes' => [
                    'data-icon' => 'fab fa-hackerrank',
                ],
            ],
            [
                'value' => 'fas fa-hammer',
                'label' => 'Hammer',
                'attributes' => [
                    'data-icon' => 'fas fa-hammer',
                ],
            ],
            [
                'value' => 'fas fa-hamsa',
                'label' => 'Hamsa',
                'attributes' => [
                    'data-icon' => 'fas fa-hamsa',
                ],
            ],
            [
                'value' => 'fas fa-hand',
                'label' => 'Paper (Hand)',
                'attributes' => [
                    'data-icon' => 'fas fa-hand',
                ],
            ],
            [
                'value' => 'far fa-hand',
                'label' => 'Paper (Hand)',
                'attributes' => [
                    'data-icon' => 'far fa-hand',
                ],
            ],
            [
                'value' => 'fas fa-hand-back-fist',
                'label' => 'Rock (Hand)',
                'attributes' => [
                    'data-icon' => 'fas fa-hand-back-fist',
                ],
            ],
            [
                'value' => 'far fa-hand-back-fist',
                'label' => 'Rock (Hand)',
                'attributes' => [
                    'data-icon' => 'far fa-hand-back-fist',
                ],
            ],
            [
                'value' => 'fas fa-hand-dots',
                'label' => 'Hand dots',
                'attributes' => [
                    'data-icon' => 'fas fa-hand-dots',
                ],
            ],
            [
                'value' => 'fas fa-hand-fist',
                'label' => 'Raised Fist',
                'attributes' => [
                    'data-icon' => 'fas fa-hand-fist',
                ],
            ],
            [
                'value' => 'fas fa-hand-holding',
                'label' => 'Hand Holding',
                'attributes' => [
                    'data-icon' => 'fas fa-hand-holding',
                ],
            ],
            [
                'value' => 'fas fa-hand-holding-dollar',
                'label' => 'Hand holding dollar',
                'attributes' => [
                    'data-icon' => 'fas fa-hand-holding-dollar',
                ],
            ],
            [
                'value' => 'fas fa-hand-holding-droplet',
                'label' => 'Hand holding droplet',
                'attributes' => [
                    'data-icon' => 'fas fa-hand-holding-droplet',
                ],
            ],
            [
                'value' => 'fas fa-hand-holding-hand',
                'label' => 'Hand Holding-hand',
                'attributes' => [
                    'data-icon' => 'fas fa-hand-holding-hand',
                ],
            ],
            [
                'value' => 'fas fa-hand-holding-heart',
                'label' => 'Hand Holding Heart',
                'attributes' => [
                    'data-icon' => 'fas fa-hand-holding-heart',
                ],
            ],
            [
                'value' => 'fas fa-hand-holding-medical',
                'label' => 'Hand Holding Medical Cross',
                'attributes' => [
                    'data-icon' => 'fas fa-hand-holding-medical',
                ],
            ],
            [
                'value' => 'fas fa-hand-lizard',
                'label' => 'Lizard (Hand)',
                'attributes' => [
                    'data-icon' => 'fas fa-hand-lizard',
                ],
            ],
            [
                'value' => 'far fa-hand-lizard',
                'label' => 'Lizard (Hand)',
                'attributes' => [
                    'data-icon' => 'far fa-hand-lizard',
                ],
            ],
            [
                'value' => 'fas fa-hand-middle-finger',
                'label' => 'Hand with Middle Finger Raised',
                'attributes' => [
                    'data-icon' => 'fas fa-hand-middle-finger',
                ],
            ],
            [
                'value' => 'fas fa-hand-peace',
                'label' => 'Peace (Hand)',
                'attributes' => [
                    'data-icon' => 'fas fa-hand-peace',
                ],
            ],
            [
                'value' => 'far fa-hand-peace',
                'label' => 'Peace (Hand)',
                'attributes' => [
                    'data-icon' => 'far fa-hand-peace',
                ],
            ],
            [
                'value' => 'fas fa-hand-point-down',
                'label' => 'Hand Pointing Down',
                'attributes' => [
                    'data-icon' => 'fas fa-hand-point-down',
                ],
            ],
            [
                'value' => 'far fa-hand-point-down',
                'label' => 'Hand Pointing Down',
                'attributes' => [
                    'data-icon' => 'far fa-hand-point-down',
                ],
            ],
            [
                'value' => 'fas fa-hand-point-left',
                'label' => 'Hand Pointing Left',
                'attributes' => [
                    'data-icon' => 'fas fa-hand-point-left',
                ],
            ],
            [
                'value' => 'far fa-hand-point-left',
                'label' => 'Hand Pointing Left',
                'attributes' => [
                    'data-icon' => 'far fa-hand-point-left',
                ],
            ],
            [
                'value' => 'fas fa-hand-point-right',
                'label' => 'Hand Pointing Right',
                'attributes' => [
                    'data-icon' => 'fas fa-hand-point-right',
                ],
            ],
            [
                'value' => 'far fa-hand-point-right',
                'label' => 'Hand Pointing Right',
                'attributes' => [
                    'data-icon' => 'far fa-hand-point-right',
                ],
            ],
            [
                'value' => 'fas fa-hand-point-up',
                'label' => 'Hand Pointing Up',
                'attributes' => [
                    'data-icon' => 'fas fa-hand-point-up',
                ],
            ],
            [
                'value' => 'far fa-hand-point-up',
                'label' => 'Hand Pointing Up',
                'attributes' => [
                    'data-icon' => 'far fa-hand-point-up',
                ],
            ],
            [
                'value' => 'fas fa-hand-pointer',
                'label' => 'Pointer (Hand)',
                'attributes' => [
                    'data-icon' => 'fas fa-hand-pointer',
                ],
            ],
            [
                'value' => 'far fa-hand-pointer',
                'label' => 'Pointer (Hand)',
                'attributes' => [
                    'data-icon' => 'far fa-hand-pointer',
                ],
            ],
            [
                'value' => 'fas fa-hand-scissors',
                'label' => 'Scissors (Hand)',
                'attributes' => [
                    'data-icon' => 'fas fa-hand-scissors',
                ],
            ],
            [
                'value' => 'far fa-hand-scissors',
                'label' => 'Scissors (Hand)',
                'attributes' => [
                    'data-icon' => 'far fa-hand-scissors',
                ],
            ],
            [
                'value' => 'fas fa-hand-sparkles',
                'label' => 'Hand Sparkles',
                'attributes' => [
                    'data-icon' => 'fas fa-hand-sparkles',
                ],
            ],
            [
                'value' => 'fas fa-hand-spock',
                'label' => 'Spock (Hand)',
                'attributes' => [
                    'data-icon' => 'fas fa-hand-spock',
                ],
            ],
            [
                'value' => 'far fa-hand-spock',
                'label' => 'Spock (Hand)',
                'attributes' => [
                    'data-icon' => 'far fa-hand-spock',
                ],
            ],
            [
                'value' => 'fas fa-handcuffs',
                'label' => 'Handcuffs',
                'attributes' => [
                    'data-icon' => 'fas fa-handcuffs',
                ],
            ],
            [
                'value' => 'fas fa-hands',
                'label' => 'Hands',
                'attributes' => [
                    'data-icon' => 'fas fa-hands',
                ],
            ],
            [
                'value' => 'fas fa-hands-asl-interpreting',
                'label' => 'Hands american sign language interpreting',
                'attributes' => [
                    'data-icon' => 'fas fa-hands-asl-interpreting',
                ],
            ],
            [
                'value' => 'fas fa-hands-bound',
                'label' => 'Hands Bound',
                'attributes' => [
                    'data-icon' => 'fas fa-hands-bound',
                ],
            ],
            [
                'value' => 'fas fa-hands-bubbles',
                'label' => 'Hands bubbles',
                'attributes' => [
                    'data-icon' => 'fas fa-hands-bubbles',
                ],
            ],
            [
                'value' => 'fas fa-hands-clapping',
                'label' => 'Hands Clapping',
                'attributes' => [
                    'data-icon' => 'fas fa-hands-clapping',
                ],
            ],
            [
                'value' => 'fas fa-hands-holding',
                'label' => 'Hands holding',
                'attributes' => [
                    'data-icon' => 'fas fa-hands-holding',
                ],
            ],
            [
                'value' => 'fas fa-hands-holding-child',
                'label' => 'Hands Holding-child',
                'attributes' => [
                    'data-icon' => 'fas fa-hands-holding-child',
                ],
            ],
            [
                'value' => 'fas fa-hands-holding-circle',
                'label' => 'Hands Holding-circle',
                'attributes' => [
                    'data-icon' => 'fas fa-hands-holding-circle',
                ],
            ],
            [
                'value' => 'fas fa-hands-praying',
                'label' => 'Hands praying',
                'attributes' => [
                    'data-icon' => 'fas fa-hands-praying',
                ],
            ],
            [
                'value' => 'fas fa-handshake',
                'label' => 'Handshake',
                'attributes' => [
                    'data-icon' => 'fas fa-handshake',
                ],
            ],
            [
                'value' => 'far fa-handshake',
                'label' => 'Handshake',
                'attributes' => [
                    'data-icon' => 'far fa-handshake',
                ],
            ],
            [
                'value' => 'fas fa-handshake-angle',
                'label' => 'Handshake angle',
                'attributes' => [
                    'data-icon' => 'fas fa-handshake-angle',
                ],
            ],
            [
                'value' => 'fas fa-handshake-simple',
                'label' => 'Handshake simple',
                'attributes' => [
                    'data-icon' => 'fas fa-handshake-simple',
                ],
            ],
            [
                'value' => 'fas fa-handshake-simple-slash',
                'label' => 'Handshake simple slash',
                'attributes' => [
                    'data-icon' => 'fas fa-handshake-simple-slash',
                ],
            ],
            [
                'value' => 'fas fa-handshake-slash',
                'label' => 'Handshake Slash',
                'attributes' => [
                    'data-icon' => 'fas fa-handshake-slash',
                ],
            ],
            [
                'value' => 'fas fa-hanukiah',
                'label' => 'Hanukiah',
                'attributes' => [
                    'data-icon' => 'fas fa-hanukiah',
                ],
            ],
            [
                'value' => 'fas fa-hard-drive',
                'label' => 'Hard drive',
                'attributes' => [
                    'data-icon' => 'fas fa-hard-drive',
                ],
            ],
            [
                'value' => 'far fa-hard-drive',
                'label' => 'Hard drive',
                'attributes' => [
                    'data-icon' => 'far fa-hard-drive',
                ],
            ],
            [
                'value' => 'fab fa-hashnode',
                'label' => 'Hashnode',
                'attributes' => [
                    'data-icon' => 'fab fa-hashnode',
                ],
            ],
            [
                'value' => 'fas fa-hashtag',
                'label' => 'Hashtag',
                'attributes' => [
                    'data-icon' => 'fas fa-hashtag',
                ],
            ],
            [
                'value' => 'fas fa-hat-cowboy',
                'label' => 'Cowboy Hat',
                'attributes' => [
                    'data-icon' => 'fas fa-hat-cowboy',
                ],
            ],
            [
                'value' => 'fas fa-hat-cowboy-side',
                'label' => 'Cowboy Hat Side',
                'attributes' => [
                    'data-icon' => 'fas fa-hat-cowboy-side',
                ],
            ],
            [
                'value' => 'fas fa-hat-wizard',
                'label' => 'Wizards Hat',
                'attributes' => [
                    'data-icon' => 'fas fa-hat-wizard',
                ],
            ],
            [
                'value' => 'fas fa-head-side-cough',
                'label' => 'Head Side Cough',
                'attributes' => [
                    'data-icon' => 'fas fa-head-side-cough',
                ],
            ],
            [
                'value' => 'fas fa-head-side-cough-slash',
                'label' => 'Head Side-cough-slash',
                'attributes' => [
                    'data-icon' => 'fas fa-head-side-cough-slash',
                ],
            ],
            [
                'value' => 'fas fa-head-side-mask',
                'label' => 'Head Side Mask',
                'attributes' => [
                    'data-icon' => 'fas fa-head-side-mask',
                ],
            ],
            [
                'value' => 'fas fa-head-side-virus',
                'label' => 'Head Side Virus',
                'attributes' => [
                    'data-icon' => 'fas fa-head-side-virus',
                ],
            ],
            [
                'value' => 'fas fa-heading',
                'label' => 'heading',
                'attributes' => [
                    'data-icon' => 'fas fa-heading',
                ],
            ],
            [
                'value' => 'fas fa-headphones',
                'label' => 'headphones',
                'attributes' => [
                    'data-icon' => 'fas fa-headphones',
                ],
            ],
            [
                'value' => 'fas fa-headphones-simple',
                'label' => 'Headphones simple',
                'attributes' => [
                    'data-icon' => 'fas fa-headphones-simple',
                ],
            ],
            [
                'value' => 'fas fa-headset',
                'label' => 'Headset',
                'attributes' => [
                    'data-icon' => 'fas fa-headset',
                ],
            ],
            [
                'value' => 'fas fa-heart',
                'label' => 'Heart',
                'attributes' => [
                    'data-icon' => 'fas fa-heart',
                ],
            ],
            [
                'value' => 'far fa-heart',
                'label' => 'Heart',
                'attributes' => [
                    'data-icon' => 'far fa-heart',
                ],
            ],
            [
                'value' => 'fas fa-heart-circle-bolt',
                'label' => 'Heart Circle-bolt',
                'attributes' => [
                    'data-icon' => 'fas fa-heart-circle-bolt',
                ],
            ],
            [
                'value' => 'fas fa-heart-circle-check',
                'label' => 'Heart Circle-check',
                'attributes' => [
                    'data-icon' => 'fas fa-heart-circle-check',
                ],
            ],
            [
                'value' => 'fas fa-heart-circle-exclamation',
                'label' => 'Heart Circle-exclamation',
                'attributes' => [
                    'data-icon' => 'fas fa-heart-circle-exclamation',
                ],
            ],
            [
                'value' => 'fas fa-heart-circle-minus',
                'label' => 'Heart Circle-minus',
                'attributes' => [
                    'data-icon' => 'fas fa-heart-circle-minus',
                ],
            ],
            [
                'value' => 'fas fa-heart-circle-plus',
                'label' => 'Heart Circle-plus',
                'attributes' => [
                    'data-icon' => 'fas fa-heart-circle-plus',
                ],
            ],
            [
                'value' => 'fas fa-heart-circle-xmark',
                'label' => 'Heart Circle-xmark',
                'attributes' => [
                    'data-icon' => 'fas fa-heart-circle-xmark',
                ],
            ],
            [
                'value' => 'fas fa-heart-crack',
                'label' => 'Heart crack',
                'attributes' => [
                    'data-icon' => 'fas fa-heart-crack',
                ],
            ],
            [
                'value' => 'fas fa-heart-pulse',
                'label' => 'Heart pulse',
                'attributes' => [
                    'data-icon' => 'fas fa-heart-pulse',
                ],
            ],
            [
                'value' => 'fas fa-helicopter',
                'label' => 'Helicopter',
                'attributes' => [
                    'data-icon' => 'fas fa-helicopter',
                ],
            ],
            [
                'value' => 'fas fa-helicopter-symbol',
                'label' => 'Helicopter Symbol',
                'attributes' => [
                    'data-icon' => 'fas fa-helicopter-symbol',
                ],
            ],
            [
                'value' => 'fas fa-helmet-safety',
                'label' => 'Helmet safety',
                'attributes' => [
                    'data-icon' => 'fas fa-helmet-safety',
                ],
            ],
            [
                'value' => 'fas fa-helmet-un',
                'label' => 'Helmet Un',
                'attributes' => [
                    'data-icon' => 'fas fa-helmet-un',
                ],
            ],
            [
                'value' => 'fas fa-highlighter',
                'label' => 'Highlighter',
                'attributes' => [
                    'data-icon' => 'fas fa-highlighter',
                ],
            ],
            [
                'value' => 'fas fa-hill-avalanche',
                'label' => 'Hill Avalanche',
                'attributes' => [
                    'data-icon' => 'fas fa-hill-avalanche',
                ],
            ],
            [
                'value' => 'fas fa-hill-rockslide',
                'label' => 'Hill Rockslide',
                'attributes' => [
                    'data-icon' => 'fas fa-hill-rockslide',
                ],
            ],
            [
                'value' => 'fas fa-hippo',
                'label' => 'Hippo',
                'attributes' => [
                    'data-icon' => 'fas fa-hippo',
                ],
            ],
            [
                'value' => 'fab fa-hips',
                'label' => 'Hips',
                'attributes' => [
                    'data-icon' => 'fab fa-hips',
                ],
            ],
            [
                'value' => 'fab fa-hire-a-helper',
                'label' => 'HireAHelper',
                'attributes' => [
                    'data-icon' => 'fab fa-hire-a-helper',
                ],
            ],
            [
                'value' => 'fab fa-hive',
                'label' => 'Hive Blockchain Network',
                'attributes' => [
                    'data-icon' => 'fab fa-hive',
                ],
            ],
            [
                'value' => 'fas fa-hockey-puck',
                'label' => 'Hockey Puck',
                'attributes' => [
                    'data-icon' => 'fas fa-hockey-puck',
                ],
            ],
            [
                'value' => 'fas fa-holly-berry',
                'label' => 'Holly Berry',
                'attributes' => [
                    'data-icon' => 'fas fa-holly-berry',
                ],
            ],
            [
                'value' => 'fab fa-hooli',
                'label' => 'Hooli',
                'attributes' => [
                    'data-icon' => 'fab fa-hooli',
                ],
            ],
            [
                'value' => 'fab fa-hornbill',
                'label' => 'Hornbill',
                'attributes' => [
                    'data-icon' => 'fab fa-hornbill',
                ],
            ],
            [
                'value' => 'fas fa-horse',
                'label' => 'Horse',
                'attributes' => [
                    'data-icon' => 'fas fa-horse',
                ],
            ],
            [
                'value' => 'fas fa-horse-head',
                'label' => 'Horse Head',
                'attributes' => [
                    'data-icon' => 'fas fa-horse-head',
                ],
            ],
            [
                'value' => 'fas fa-hospital',
                'label' => 'hospital',
                'attributes' => [
                    'data-icon' => 'fas fa-hospital',
                ],
            ],
            [
                'value' => 'far fa-hospital',
                'label' => 'hospital',
                'attributes' => [
                    'data-icon' => 'far fa-hospital',
                ],
            ],
            [
                'value' => 'fas fa-hospital-user',
                'label' => 'Hospital with User',
                'attributes' => [
                    'data-icon' => 'fas fa-hospital-user',
                ],
            ],
            [
                'value' => 'fas fa-hot-tub-person',
                'label' => 'Hot tub person',
                'attributes' => [
                    'data-icon' => 'fas fa-hot-tub-person',
                ],
            ],
            [
                'value' => 'fas fa-hotdog',
                'label' => 'Hot Dog',
                'attributes' => [
                    'data-icon' => 'fas fa-hotdog',
                ],
            ],
            [
                'value' => 'fas fa-hotel',
                'label' => 'Hotel',
                'attributes' => [
                    'data-icon' => 'fas fa-hotel',
                ],
            ],
            [
                'value' => 'fab fa-hotjar',
                'label' => 'Hotjar',
                'attributes' => [
                    'data-icon' => 'fab fa-hotjar',
                ],
            ],
            [
                'value' => 'fas fa-hourglass',
                'label' => 'Hourglass',
                'attributes' => [
                    'data-icon' => 'fas fa-hourglass',
                ],
            ],
            [
                'value' => 'far fa-hourglass',
                'label' => 'Hourglass',
                'attributes' => [
                    'data-icon' => 'far fa-hourglass',
                ],
            ],
            [
                'value' => 'fas fa-hourglass-empty',
                'label' => 'Hourglass empty',
                'attributes' => [
                    'data-icon' => 'fas fa-hourglass-empty',
                ],
            ],
            [
                'value' => 'fas fa-hourglass-end',
                'label' => 'Hourglass End',
                'attributes' => [
                    'data-icon' => 'fas fa-hourglass-end',
                ],
            ],
            [
                'value' => 'fas fa-hourglass-start',
                'label' => 'Hourglass Start',
                'attributes' => [
                    'data-icon' => 'fas fa-hourglass-start',
                ],
            ],
            [
                'value' => 'fas fa-house',
                'label' => 'House',
                'attributes' => [
                    'data-icon' => 'fas fa-house',
                ],
            ],
            [
                'value' => 'fas fa-house-chimney',
                'label' => 'House Chimney',
                'attributes' => [
                    'data-icon' => 'fas fa-house-chimney',
                ],
            ],
            [
                'value' => 'fas fa-house-chimney-crack',
                'label' => 'House crack',
                'attributes' => [
                    'data-icon' => 'fas fa-house-chimney-crack',
                ],
            ],
            [
                'value' => 'fas fa-house-chimney-medical',
                'label' => 'House medical',
                'attributes' => [
                    'data-icon' => 'fas fa-house-chimney-medical',
                ],
            ],
            [
                'value' => 'fas fa-house-chimney-user',
                'label' => 'House User',
                'attributes' => [
                    'data-icon' => 'fas fa-house-chimney-user',
                ],
            ],
            [
                'value' => 'fas fa-house-chimney-window',
                'label' => 'House with Window + Chimney',
                'attributes' => [
                    'data-icon' => 'fas fa-house-chimney-window',
                ],
            ],
            [
                'value' => 'fas fa-house-circle-check',
                'label' => 'House Circle-check',
                'attributes' => [
                    'data-icon' => 'fas fa-house-circle-check',
                ],
            ],
            [
                'value' => 'fas fa-house-circle-exclamation',
                'label' => 'House Circle-exclamation',
                'attributes' => [
                    'data-icon' => 'fas fa-house-circle-exclamation',
                ],
            ],
            [
                'value' => 'fas fa-house-circle-xmark',
                'label' => 'House Circle-xmark',
                'attributes' => [
                    'data-icon' => 'fas fa-house-circle-xmark',
                ],
            ],
            [
                'value' => 'fas fa-house-crack',
                'label' => 'House Simple Crack',
                'attributes' => [
                    'data-icon' => 'fas fa-house-crack',
                ],
            ],
            [
                'value' => 'fas fa-house-fire',
                'label' => 'House Fire',
                'attributes' => [
                    'data-icon' => 'fas fa-house-fire',
                ],
            ],
            [
                'value' => 'fas fa-house-flag',
                'label' => 'House Flag',
                'attributes' => [
                    'data-icon' => 'fas fa-house-flag',
                ],
            ],
            [
                'value' => 'fas fa-house-flood-water',
                'label' => 'House Flood',
                'attributes' => [
                    'data-icon' => 'fas fa-house-flood-water',
                ],
            ],
            [
                'value' => 'fas fa-house-flood-water-circle-arrow-right',
                'label' => 'House Flood-circle-arrow-right',
                'attributes' => [
                    'data-icon' => 'fas fa-house-flood-water-circle-arrow-right',
                ],
            ],
            [
                'value' => 'fas fa-house-laptop',
                'label' => 'House laptop',
                'attributes' => [
                    'data-icon' => 'fas fa-house-laptop',
                ],
            ],
            [
                'value' => 'fas fa-house-lock',
                'label' => 'House Lock',
                'attributes' => [
                    'data-icon' => 'fas fa-house-lock',
                ],
            ],
            [
                'value' => 'fas fa-house-medical',
                'label' => 'House Simple Medical',
                'attributes' => [
                    'data-icon' => 'fas fa-house-medical',
                ],
            ],
            [
                'value' => 'fas fa-house-medical-circle-check',
                'label' => 'House Medical-circle-check',
                'attributes' => [
                    'data-icon' => 'fas fa-house-medical-circle-check',
                ],
            ],
            [
                'value' => 'fas fa-house-medical-circle-exclamation',
                'label' => 'House Medical-circle-exclamation',
                'attributes' => [
                    'data-icon' => 'fas fa-house-medical-circle-exclamation',
                ],
            ],
            [
                'value' => 'fas fa-house-medical-circle-xmark',
                'label' => 'House Medical-circle-xmark',
                'attributes' => [
                    'data-icon' => 'fas fa-house-medical-circle-xmark',
                ],
            ],
            [
                'value' => 'fas fa-house-medical-flag',
                'label' => 'House Medical-flag',
                'attributes' => [
                    'data-icon' => 'fas fa-house-medical-flag',
                ],
            ],
            [
                'value' => 'fas fa-house-signal',
                'label' => 'House Signal',
                'attributes' => [
                    'data-icon' => 'fas fa-house-signal',
                ],
            ],
            [
                'value' => 'fas fa-house-tsunami',
                'label' => 'House Tsunami',
                'attributes' => [
                    'data-icon' => 'fas fa-house-tsunami',
                ],
            ],
            [
                'value' => 'fas fa-house-user',
                'label' => 'Home User',
                'attributes' => [
                    'data-icon' => 'fas fa-house-user',
                ],
            ],
            [
                'value' => 'fab fa-houzz',
                'label' => 'Houzz',
                'attributes' => [
                    'data-icon' => 'fab fa-houzz',
                ],
            ],
            [
                'value' => 'fas fa-hryvnia-sign',
                'label' => 'Hryvnia sign',
                'attributes' => [
                    'data-icon' => 'fas fa-hryvnia-sign',
                ],
            ],
            [
                'value' => 'fab fa-html5',
                'label' => 'HTML 5 Logo',
                'attributes' => [
                    'data-icon' => 'fab fa-html5',
                ],
            ],
            [
                'value' => 'fab fa-hubspot',
                'label' => 'HubSpot',
                'attributes' => [
                    'data-icon' => 'fab fa-hubspot',
                ],
            ],
            [
                'value' => 'fas fa-hurricane',
                'label' => 'Hurricane',
                'attributes' => [
                    'data-icon' => 'fas fa-hurricane',
                ],
            ],
            [
                'value' => 'fas fa-i',
                'label' => 'I',
                'attributes' => [
                    'data-icon' => 'fas fa-i',
                ],
            ],
            [
                'value' => 'fas fa-i-cursor',
                'label' => 'I Beam Cursor',
                'attributes' => [
                    'data-icon' => 'fas fa-i-cursor',
                ],
            ],
            [
                'value' => 'fas fa-ice-cream',
                'label' => 'Ice Cream',
                'attributes' => [
                    'data-icon' => 'fas fa-ice-cream',
                ],
            ],
            [
                'value' => 'fas fa-icicles',
                'label' => 'Icicles',
                'attributes' => [
                    'data-icon' => 'fas fa-icicles',
                ],
            ],
            [
                'value' => 'fas fa-icons',
                'label' => 'Icons',
                'attributes' => [
                    'data-icon' => 'fas fa-icons',
                ],
            ],
            [
                'value' => 'fas fa-id-badge',
                'label' => 'Identification Badge',
                'attributes' => [
                    'data-icon' => 'fas fa-id-badge',
                ],
            ],
            [
                'value' => 'far fa-id-badge',
                'label' => 'Identification Badge',
                'attributes' => [
                    'data-icon' => 'far fa-id-badge',
                ],
            ],
            [
                'value' => 'fas fa-id-card',
                'label' => 'Identification Card',
                'attributes' => [
                    'data-icon' => 'fas fa-id-card',
                ],
            ],
            [
                'value' => 'far fa-id-card',
                'label' => 'Identification Card',
                'attributes' => [
                    'data-icon' => 'far fa-id-card',
                ],
            ],
            [
                'value' => 'fas fa-id-card-clip',
                'label' => 'Id card clip',
                'attributes' => [
                    'data-icon' => 'fas fa-id-card-clip',
                ],
            ],
            [
                'value' => 'fab fa-ideal',
                'label' => 'iDeal',
                'attributes' => [
                    'data-icon' => 'fab fa-ideal',
                ],
            ],
            [
                'value' => 'fas fa-igloo',
                'label' => 'Igloo',
                'attributes' => [
                    'data-icon' => 'fas fa-igloo',
                ],
            ],
            [
                'value' => 'fas fa-image',
                'label' => 'Image',
                'attributes' => [
                    'data-icon' => 'fas fa-image',
                ],
            ],
            [
                'value' => 'far fa-image',
                'label' => 'Image',
                'attributes' => [
                    'data-icon' => 'far fa-image',
                ],
            ],
            [
                'value' => 'fas fa-image-portrait',
                'label' => 'Image portrait',
                'attributes' => [
                    'data-icon' => 'fas fa-image-portrait',
                ],
            ],
            [
                'value' => 'fas fa-images',
                'label' => 'Images',
                'attributes' => [
                    'data-icon' => 'fas fa-images',
                ],
            ],
            [
                'value' => 'far fa-images',
                'label' => 'Images',
                'attributes' => [
                    'data-icon' => 'far fa-images',
                ],
            ],
            [
                'value' => 'fab fa-imdb',
                'label' => 'IMDB',
                'attributes' => [
                    'data-icon' => 'fab fa-imdb',
                ],
            ],
            [
                'value' => 'fas fa-inbox',
                'label' => 'inbox',
                'attributes' => [
                    'data-icon' => 'fas fa-inbox',
                ],
            ],
            [
                'value' => 'fas fa-indent',
                'label' => 'Indent',
                'attributes' => [
                    'data-icon' => 'fas fa-indent',
                ],
            ],
            [
                'value' => 'fas fa-indian-rupee-sign',
                'label' => 'Indian Rupee-sign',
                'attributes' => [
                    'data-icon' => 'fas fa-indian-rupee-sign',
                ],
            ],
            [
                'value' => 'fas fa-industry',
                'label' => 'Industry',
                'attributes' => [
                    'data-icon' => 'fas fa-industry',
                ],
            ],
            [
                'value' => 'fas fa-infinity',
                'label' => 'Infinity',
                'attributes' => [
                    'data-icon' => 'fas fa-infinity',
                ],
            ],
            [
                'value' => 'fas fa-info',
                'label' => 'Info',
                'attributes' => [
                    'data-icon' => 'fas fa-info',
                ],
            ],
            [
                'value' => 'fab fa-instagram',
                'label' => 'Instagram',
                'attributes' => [
                    'data-icon' => 'fab fa-instagram',
                ],
            ],
            [
                'value' => 'fab fa-instagram-square',
                'label' => 'Instagram Square',
                'attributes' => [
                    'data-icon' => 'fab fa-instagram-square',
                ],
            ],
            [
                'value' => 'fab fa-instalod',
                'label' => 'InstaLOD',
                'attributes' => [
                    'data-icon' => 'fab fa-instalod',
                ],
            ],
            [
                'value' => 'fab fa-intercom',
                'label' => 'Intercom',
                'attributes' => [
                    'data-icon' => 'fab fa-intercom',
                ],
            ],
            [
                'value' => 'fab fa-internet-explorer',
                'label' => 'Internet-explorer',
                'attributes' => [
                    'data-icon' => 'fab fa-internet-explorer',
                ],
            ],
            [
                'value' => 'fab fa-invision',
                'label' => 'InVision',
                'attributes' => [
                    'data-icon' => 'fab fa-invision',
                ],
            ],
            [
                'value' => 'fab fa-ioxhost',
                'label' => 'ioxhost',
                'attributes' => [
                    'data-icon' => 'fab fa-ioxhost',
                ],
            ],
            [
                'value' => 'fas fa-italic',
                'label' => 'italic',
                'attributes' => [
                    'data-icon' => 'fas fa-italic',
                ],
            ],
            [
                'value' => 'fab fa-itch-io',
                'label' => 'itch.io',
                'attributes' => [
                    'data-icon' => 'fab fa-itch-io',
                ],
            ],
            [
                'value' => 'fab fa-itunes',
                'label' => 'iTunes',
                'attributes' => [
                    'data-icon' => 'fab fa-itunes',
                ],
            ],
            [
                'value' => 'fab fa-itunes-note',
                'label' => 'Itunes Note',
                'attributes' => [
                    'data-icon' => 'fab fa-itunes-note',
                ],
            ],
            [
                'value' => 'fas fa-j',
                'label' => 'J',
                'attributes' => [
                    'data-icon' => 'fas fa-j',
                ],
            ],
            [
                'value' => 'fas fa-jar',
                'label' => 'Jar',
                'attributes' => [
                    'data-icon' => 'fas fa-jar',
                ],
            ],
            [
                'value' => 'fas fa-jar-wheat',
                'label' => 'Jar Wheat',
                'attributes' => [
                    'data-icon' => 'fas fa-jar-wheat',
                ],
            ],
            [
                'value' => 'fab fa-java',
                'label' => 'Java',
                'attributes' => [
                    'data-icon' => 'fab fa-java',
                ],
            ],
            [
                'value' => 'fas fa-jedi',
                'label' => 'Jedi',
                'attributes' => [
                    'data-icon' => 'fas fa-jedi',
                ],
            ],
            [
                'value' => 'fab fa-jedi-order',
                'label' => 'Jedi Order',
                'attributes' => [
                    'data-icon' => 'fab fa-jedi-order',
                ],
            ],
            [
                'value' => 'fab fa-jenkins',
                'label' => 'Jenkis',
                'attributes' => [
                    'data-icon' => 'fab fa-jenkins',
                ],
            ],
            [
                'value' => 'fas fa-jet-fighter',
                'label' => 'Jet fighter',
                'attributes' => [
                    'data-icon' => 'fas fa-jet-fighter',
                ],
            ],
            [
                'value' => 'fas fa-jet-fighter-up',
                'label' => 'Jet Fighter Up',
                'attributes' => [
                    'data-icon' => 'fas fa-jet-fighter-up',
                ],
            ],
            [
                'value' => 'fab fa-jira',
                'label' => 'Jira',
                'attributes' => [
                    'data-icon' => 'fab fa-jira',
                ],
            ],
            [
                'value' => 'fab fa-joget',
                'label' => 'Joget',
                'attributes' => [
                    'data-icon' => 'fab fa-joget',
                ],
            ],
            [
                'value' => 'fas fa-joint',
                'label' => 'Joint',
                'attributes' => [
                    'data-icon' => 'fas fa-joint',
                ],
            ],
            [
                'value' => 'fab fa-joomla',
                'label' => 'Joomla Logo',
                'attributes' => [
                    'data-icon' => 'fab fa-joomla',
                ],
            ],
            [
                'value' => 'fab fa-js',
                'label' => 'JavaScript (JS)',
                'attributes' => [
                    'data-icon' => 'fab fa-js',
                ],
            ],
            [
                'value' => 'fab fa-js-square',
                'label' => 'JavaScript (JS) Square',
                'attributes' => [
                    'data-icon' => 'fab fa-js-square',
                ],
            ],
            [
                'value' => 'fab fa-jsfiddle',
                'label' => 'jsFiddle',
                'attributes' => [
                    'data-icon' => 'fab fa-jsfiddle',
                ],
            ],
            [
                'value' => 'fas fa-jug-detergent',
                'label' => 'Jug Detergent',
                'attributes' => [
                    'data-icon' => 'fas fa-jug-detergent',
                ],
            ],
            [
                'value' => 'fas fa-k',
                'label' => 'K',
                'attributes' => [
                    'data-icon' => 'fas fa-k',
                ],
            ],
            [
                'value' => 'fas fa-kaaba',
                'label' => 'Kaaba',
                'attributes' => [
                    'data-icon' => 'fas fa-kaaba',
                ],
            ],
            [
                'value' => 'fab fa-kaggle',
                'label' => 'Kaggle',
                'attributes' => [
                    'data-icon' => 'fab fa-kaggle',
                ],
            ],
            [
                'value' => 'fas fa-key',
                'label' => 'key',
                'attributes' => [
                    'data-icon' => 'fas fa-key',
                ],
            ],
            [
                'value' => 'fab fa-keybase',
                'label' => 'Keybase',
                'attributes' => [
                    'data-icon' => 'fab fa-keybase',
                ],
            ],
            [
                'value' => 'fas fa-keyboard',
                'label' => 'Keyboard',
                'attributes' => [
                    'data-icon' => 'fas fa-keyboard',
                ],
            ],
            [
                'value' => 'far fa-keyboard',
                'label' => 'Keyboard',
                'attributes' => [
                    'data-icon' => 'far fa-keyboard',
                ],
            ],
            [
                'value' => 'fab fa-keycdn',
                'label' => 'KeyCDN',
                'attributes' => [
                    'data-icon' => 'fab fa-keycdn',
                ],
            ],
            [
                'value' => 'fas fa-khanda',
                'label' => 'Khanda',
                'attributes' => [
                    'data-icon' => 'fas fa-khanda',
                ],
            ],
            [
                'value' => 'fab fa-kickstarter',
                'label' => 'Kickstarter',
                'attributes' => [
                    'data-icon' => 'fab fa-kickstarter',
                ],
            ],
            [
                'value' => 'fab fa-kickstarter-k',
                'label' => 'Kickstarter K',
                'attributes' => [
                    'data-icon' => 'fab fa-kickstarter-k',
                ],
            ],
            [
                'value' => 'fas fa-kip-sign',
                'label' => 'Kip Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-kip-sign',
                ],
            ],
            [
                'value' => 'fas fa-kit-medical',
                'label' => 'Kit medical',
                'attributes' => [
                    'data-icon' => 'fas fa-kit-medical',
                ],
            ],
            [
                'value' => 'fas fa-kitchen-set',
                'label' => 'Kitchen Set',
                'attributes' => [
                    'data-icon' => 'fas fa-kitchen-set',
                ],
            ],
            [
                'value' => 'fas fa-kiwi-bird',
                'label' => 'Kiwi Bird',
                'attributes' => [
                    'data-icon' => 'fas fa-kiwi-bird',
                ],
            ],
            [
                'value' => 'fab fa-korvue',
                'label' => 'KORVUE',
                'attributes' => [
                    'data-icon' => 'fab fa-korvue',
                ],
            ],
            [
                'value' => 'fas fa-l',
                'label' => 'L',
                'attributes' => [
                    'data-icon' => 'fas fa-l',
                ],
            ],
            [
                'value' => 'fas fa-land-mine-on',
                'label' => 'Land Mine-on',
                'attributes' => [
                    'data-icon' => 'fas fa-land-mine-on',
                ],
            ],
            [
                'value' => 'fas fa-landmark',
                'label' => 'Landmark',
                'attributes' => [
                    'data-icon' => 'fas fa-landmark',
                ],
            ],
            [
                'value' => 'fas fa-landmark-dome',
                'label' => 'Landmark dome',
                'attributes' => [
                    'data-icon' => 'fas fa-landmark-dome',
                ],
            ],
            [
                'value' => 'fas fa-landmark-flag',
                'label' => 'Landmark Flag',
                'attributes' => [
                    'data-icon' => 'fas fa-landmark-flag',
                ],
            ],
            [
                'value' => 'fas fa-language',
                'label' => 'Language',
                'attributes' => [
                    'data-icon' => 'fas fa-language',
                ],
            ],
            [
                'value' => 'fas fa-laptop',
                'label' => 'Laptop',
                'attributes' => [
                    'data-icon' => 'fas fa-laptop',
                ],
            ],
            [
                'value' => 'fas fa-laptop-code',
                'label' => 'Laptop Code',
                'attributes' => [
                    'data-icon' => 'fas fa-laptop-code',
                ],
            ],
            [
                'value' => 'fas fa-laptop-file',
                'label' => 'Laptop File',
                'attributes' => [
                    'data-icon' => 'fas fa-laptop-file',
                ],
            ],
            [
                'value' => 'fas fa-laptop-medical',
                'label' => 'Laptop Medical',
                'attributes' => [
                    'data-icon' => 'fas fa-laptop-medical',
                ],
            ],
            [
                'value' => 'fab fa-laravel',
                'label' => 'Laravel',
                'attributes' => [
                    'data-icon' => 'fab fa-laravel',
                ],
            ],
            [
                'value' => 'fas fa-lari-sign',
                'label' => 'Lari Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-lari-sign',
                ],
            ],
            [
                'value' => 'fab fa-lastfm',
                'label' => 'last.fm',
                'attributes' => [
                    'data-icon' => 'fab fa-lastfm',
                ],
            ],
            [
                'value' => 'fab fa-lastfm-square',
                'label' => 'last.fm Square',
                'attributes' => [
                    'data-icon' => 'fab fa-lastfm-square',
                ],
            ],
            [
                'value' => 'fas fa-layer-group',
                'label' => 'Layer Group',
                'attributes' => [
                    'data-icon' => 'fas fa-layer-group',
                ],
            ],
            [
                'value' => 'fas fa-leaf',
                'label' => 'leaf',
                'attributes' => [
                    'data-icon' => 'fas fa-leaf',
                ],
            ],
            [
                'value' => 'fab fa-leanpub',
                'label' => 'Leanpub',
                'attributes' => [
                    'data-icon' => 'fab fa-leanpub',
                ],
            ],
            [
                'value' => 'fas fa-left-long',
                'label' => 'Left long',
                'attributes' => [
                    'data-icon' => 'fas fa-left-long',
                ],
            ],
            [
                'value' => 'fas fa-left-right',
                'label' => 'Left right',
                'attributes' => [
                    'data-icon' => 'fas fa-left-right',
                ],
            ],
            [
                'value' => 'fas fa-lemon',
                'label' => 'Lemon',
                'attributes' => [
                    'data-icon' => 'fas fa-lemon',
                ],
            ],
            [
                'value' => 'far fa-lemon',
                'label' => 'Lemon',
                'attributes' => [
                    'data-icon' => 'far fa-lemon',
                ],
            ],
            [
                'value' => 'fab fa-less',
                'label' => 'Less',
                'attributes' => [
                    'data-icon' => 'fab fa-less',
                ],
            ],
            [
                'value' => 'fas fa-less-than',
                'label' => 'Less Than',
                'attributes' => [
                    'data-icon' => 'fas fa-less-than',
                ],
            ],
            [
                'value' => 'fas fa-less-than-equal',
                'label' => 'Less Than Equal To',
                'attributes' => [
                    'data-icon' => 'fas fa-less-than-equal',
                ],
            ],
            [
                'value' => 'fas fa-life-ring',
                'label' => 'Life Ring',
                'attributes' => [
                    'data-icon' => 'fas fa-life-ring',
                ],
            ],
            [
                'value' => 'far fa-life-ring',
                'label' => 'Life Ring',
                'attributes' => [
                    'data-icon' => 'far fa-life-ring',
                ],
            ],
            [
                'value' => 'fas fa-lightbulb',
                'label' => 'Lightbulb',
                'attributes' => [
                    'data-icon' => 'fas fa-lightbulb',
                ],
            ],
            [
                'value' => 'far fa-lightbulb',
                'label' => 'Lightbulb',
                'attributes' => [
                    'data-icon' => 'far fa-lightbulb',
                ],
            ],
            [
                'value' => 'fab fa-line',
                'label' => 'Line',
                'attributes' => [
                    'data-icon' => 'fab fa-line',
                ],
            ],
            [
                'value' => 'fas fa-lines-leaning',
                'label' => 'Lines Leaning',
                'attributes' => [
                    'data-icon' => 'fas fa-lines-leaning',
                ],
            ],
            [
                'value' => 'fas fa-link',
                'label' => 'Link',
                'attributes' => [
                    'data-icon' => 'fas fa-link',
                ],
            ],
            [
                'value' => 'fas fa-link-slash',
                'label' => 'Link Slash',
                'attributes' => [
                    'data-icon' => 'fas fa-link-slash',
                ],
            ],
            [
                'value' => 'fab fa-linkedin',
                'label' => 'LinkedIn',
                'attributes' => [
                    'data-icon' => 'fab fa-linkedin',
                ],
            ],
            [
                'value' => 'fab fa-linkedin-in',
                'label' => 'LinkedIn In',
                'attributes' => [
                    'data-icon' => 'fab fa-linkedin-in',
                ],
            ],
            [
                'value' => 'fab fa-linode',
                'label' => 'Linode',
                'attributes' => [
                    'data-icon' => 'fab fa-linode',
                ],
            ],
            [
                'value' => 'fab fa-linux',
                'label' => 'Linux',
                'attributes' => [
                    'data-icon' => 'fab fa-linux',
                ],
            ],
            [
                'value' => 'fas fa-lira-sign',
                'label' => 'Turkish Lira Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-lira-sign',
                ],
            ],
            [
                'value' => 'fas fa-list',
                'label' => 'List',
                'attributes' => [
                    'data-icon' => 'fas fa-list',
                ],
            ],
            [
                'value' => 'fas fa-list-check',
                'label' => 'List check',
                'attributes' => [
                    'data-icon' => 'fas fa-list-check',
                ],
            ],
            [
                'value' => 'fas fa-list-ol',
                'label' => 'list-ol',
                'attributes' => [
                    'data-icon' => 'fas fa-list-ol',
                ],
            ],
            [
                'value' => 'fas fa-list-ul',
                'label' => 'list-ul',
                'attributes' => [
                    'data-icon' => 'fas fa-list-ul',
                ],
            ],
            [
                'value' => 'fas fa-litecoin-sign',
                'label' => 'Litecoin Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-litecoin-sign',
                ],
            ],
            [
                'value' => 'fas fa-location-arrow',
                'label' => 'location-arrow',
                'attributes' => [
                    'data-icon' => 'fas fa-location-arrow',
                ],
            ],
            [
                'value' => 'fas fa-location-crosshairs',
                'label' => 'Location Crosshairs',
                'attributes' => [
                    'data-icon' => 'fas fa-location-crosshairs',
                ],
            ],
            [
                'value' => 'fas fa-location-dot',
                'label' => 'Location dot',
                'attributes' => [
                    'data-icon' => 'fas fa-location-dot',
                ],
            ],
            [
                'value' => 'fas fa-location-pin',
                'label' => 'Location',
                'attributes' => [
                    'data-icon' => 'fas fa-location-pin',
                ],
            ],
            [
                'value' => 'fas fa-location-pin-lock',
                'label' => 'Location Pin-lock',
                'attributes' => [
                    'data-icon' => 'fas fa-location-pin-lock',
                ],
            ],
            [
                'value' => 'fas fa-lock',
                'label' => 'lock',
                'attributes' => [
                    'data-icon' => 'fas fa-lock',
                ],
            ],
            [
                'value' => 'fas fa-lock-open',
                'label' => 'Lock Open',
                'attributes' => [
                    'data-icon' => 'fas fa-lock-open',
                ],
            ],
            [
                'value' => 'fas fa-locust',
                'label' => 'Locust',
                'attributes' => [
                    'data-icon' => 'fas fa-locust',
                ],
            ],
            [
                'value' => 'fas fa-lungs',
                'label' => 'Lungs',
                'attributes' => [
                    'data-icon' => 'fas fa-lungs',
                ],
            ],
            [
                'value' => 'fas fa-lungs-virus',
                'label' => 'Lungs Virus',
                'attributes' => [
                    'data-icon' => 'fas fa-lungs-virus',
                ],
            ],
            [
                'value' => 'fab fa-lyft',
                'label' => 'lyft',
                'attributes' => [
                    'data-icon' => 'fab fa-lyft',
                ],
            ],
            [
                'value' => 'fas fa-m',
                'label' => 'M',
                'attributes' => [
                    'data-icon' => 'fas fa-m',
                ],
            ],
            [
                'value' => 'fab fa-magento',
                'label' => 'Magento',
                'attributes' => [
                    'data-icon' => 'fab fa-magento',
                ],
            ],
            [
                'value' => 'fas fa-magnet',
                'label' => 'magnet',
                'attributes' => [
                    'data-icon' => 'fas fa-magnet',
                ],
            ],
            [
                'value' => 'fas fa-magnifying-glass',
                'label' => 'Magnifying glass',
                'attributes' => [
                    'data-icon' => 'fas fa-magnifying-glass',
                ],
            ],
            [
                'value' => 'fas fa-magnifying-glass-arrow-right',
                'label' => 'Magnifying Glass-arrow-right',
                'attributes' => [
                    'data-icon' => 'fas fa-magnifying-glass-arrow-right',
                ],
            ],
            [
                'value' => 'fas fa-magnifying-glass-chart',
                'label' => 'Magnifying Glass-chart',
                'attributes' => [
                    'data-icon' => 'fas fa-magnifying-glass-chart',
                ],
            ],
            [
                'value' => 'fas fa-magnifying-glass-dollar',
                'label' => 'Magnifying glass dollar',
                'attributes' => [
                    'data-icon' => 'fas fa-magnifying-glass-dollar',
                ],
            ],
            [
                'value' => 'fas fa-magnifying-glass-location',
                'label' => 'Magnifying glass location',
                'attributes' => [
                    'data-icon' => 'fas fa-magnifying-glass-location',
                ],
            ],
            [
                'value' => 'fas fa-magnifying-glass-minus',
                'label' => 'Magnifying glass minus',
                'attributes' => [
                    'data-icon' => 'fas fa-magnifying-glass-minus',
                ],
            ],
            [
                'value' => 'fas fa-magnifying-glass-plus',
                'label' => 'Magnifying glass plus',
                'attributes' => [
                    'data-icon' => 'fas fa-magnifying-glass-plus',
                ],
            ],
            [
                'value' => 'fab fa-mailchimp',
                'label' => 'Mailchimp',
                'attributes' => [
                    'data-icon' => 'fab fa-mailchimp',
                ],
            ],
            [
                'value' => 'fas fa-manat-sign',
                'label' => 'Manat Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-manat-sign',
                ],
            ],
            [
                'value' => 'fab fa-mandalorian',
                'label' => 'Mandalorian',
                'attributes' => [
                    'data-icon' => 'fab fa-mandalorian',
                ],
            ],
            [
                'value' => 'fas fa-map',
                'label' => 'Map',
                'attributes' => [
                    'data-icon' => 'fas fa-map',
                ],
            ],
            [
                'value' => 'far fa-map',
                'label' => 'Map',
                'attributes' => [
                    'data-icon' => 'far fa-map',
                ],
            ],
            [
                'value' => 'fas fa-map-location',
                'label' => 'Map location',
                'attributes' => [
                    'data-icon' => 'fas fa-map-location',
                ],
            ],
            [
                'value' => 'fas fa-map-location-dot',
                'label' => 'Map location dot',
                'attributes' => [
                    'data-icon' => 'fas fa-map-location-dot',
                ],
            ],
            [
                'value' => 'fas fa-map-pin',
                'label' => 'Map Pin',
                'attributes' => [
                    'data-icon' => 'fas fa-map-pin',
                ],
            ],
            [
                'value' => 'fab fa-markdown',
                'label' => 'Markdown',
                'attributes' => [
                    'data-icon' => 'fab fa-markdown',
                ],
            ],
            [
                'value' => 'fas fa-marker',
                'label' => 'Marker',
                'attributes' => [
                    'data-icon' => 'fas fa-marker',
                ],
            ],
            [
                'value' => 'fas fa-mars',
                'label' => 'Mars',
                'attributes' => [
                    'data-icon' => 'fas fa-mars',
                ],
            ],
            [
                'value' => 'fas fa-mars-and-venus',
                'label' => 'Mars and Venus',
                'attributes' => [
                    'data-icon' => 'fas fa-mars-and-venus',
                ],
            ],
            [
                'value' => 'fas fa-mars-and-venus-burst',
                'label' => 'Mars and Venus Burst',
                'attributes' => [
                    'data-icon' => 'fas fa-mars-and-venus-burst',
                ],
            ],
            [
                'value' => 'fas fa-mars-double',
                'label' => 'Mars Double',
                'attributes' => [
                    'data-icon' => 'fas fa-mars-double',
                ],
            ],
            [
                'value' => 'fas fa-mars-stroke',
                'label' => 'Mars Stroke',
                'attributes' => [
                    'data-icon' => 'fas fa-mars-stroke',
                ],
            ],
            [
                'value' => 'fas fa-mars-stroke-right',
                'label' => 'Mars stroke right',
                'attributes' => [
                    'data-icon' => 'fas fa-mars-stroke-right',
                ],
            ],
            [
                'value' => 'fas fa-mars-stroke-up',
                'label' => 'Mars stroke up',
                'attributes' => [
                    'data-icon' => 'fas fa-mars-stroke-up',
                ],
            ],
            [
                'value' => 'fas fa-martini-glass',
                'label' => 'Martini glass',
                'attributes' => [
                    'data-icon' => 'fas fa-martini-glass',
                ],
            ],
            [
                'value' => 'fas fa-martini-glass-citrus',
                'label' => 'Martini glass citrus',
                'attributes' => [
                    'data-icon' => 'fas fa-martini-glass-citrus',
                ],
            ],
            [
                'value' => 'fas fa-martini-glass-empty',
                'label' => 'Martini glass empty',
                'attributes' => [
                    'data-icon' => 'fas fa-martini-glass-empty',
                ],
            ],
            [
                'value' => 'fas fa-mask',
                'label' => 'Mask',
                'attributes' => [
                    'data-icon' => 'fas fa-mask',
                ],
            ],
            [
                'value' => 'fas fa-mask-face',
                'label' => 'Face Mask',
                'attributes' => [
                    'data-icon' => 'fas fa-mask-face',
                ],
            ],
            [
                'value' => 'fas fa-mask-ventilator',
                'label' => 'Mask Ventilator',
                'attributes' => [
                    'data-icon' => 'fas fa-mask-ventilator',
                ],
            ],
            [
                'value' => 'fas fa-masks-theater',
                'label' => 'Masks theater',
                'attributes' => [
                    'data-icon' => 'fas fa-masks-theater',
                ],
            ],
            [
                'value' => 'fab fa-mastodon',
                'label' => 'Mastodon',
                'attributes' => [
                    'data-icon' => 'fab fa-mastodon',
                ],
            ],
            [
                'value' => 'fas fa-mattress-pillow',
                'label' => 'Mattress Pillow',
                'attributes' => [
                    'data-icon' => 'fas fa-mattress-pillow',
                ],
            ],
            [
                'value' => 'fab fa-maxcdn',
                'label' => 'MaxCDN',
                'attributes' => [
                    'data-icon' => 'fab fa-maxcdn',
                ],
            ],
            [
                'value' => 'fas fa-maximize',
                'label' => 'Maximize',
                'attributes' => [
                    'data-icon' => 'fas fa-maximize',
                ],
            ],
            [
                'value' => 'fab fa-mdb',
                'label' => 'Material Design for Bootstrap',
                'attributes' => [
                    'data-icon' => 'fab fa-mdb',
                ],
            ],
            [
                'value' => 'fas fa-medal',
                'label' => 'Medal',
                'attributes' => [
                    'data-icon' => 'fas fa-medal',
                ],
            ],
            [
                'value' => 'fab fa-medapps',
                'label' => 'MedApps',
                'attributes' => [
                    'data-icon' => 'fab fa-medapps',
                ],
            ],
            [
                'value' => 'fab fa-medium',
                'label' => 'Medium',
                'attributes' => [
                    'data-icon' => 'fab fa-medium',
                ],
            ],
            [
                'value' => 'fab fa-medrt',
                'label' => 'MRT',
                'attributes' => [
                    'data-icon' => 'fab fa-medrt',
                ],
            ],
            [
                'value' => 'fab fa-meetup',
                'label' => 'Meetup',
                'attributes' => [
                    'data-icon' => 'fab fa-meetup',
                ],
            ],
            [
                'value' => 'fab fa-megaport',
                'label' => 'Megaport',
                'attributes' => [
                    'data-icon' => 'fab fa-megaport',
                ],
            ],
            [
                'value' => 'fas fa-memory',
                'label' => 'Memory',
                'attributes' => [
                    'data-icon' => 'fas fa-memory',
                ],
            ],
            [
                'value' => 'fab fa-mendeley',
                'label' => 'Mendeley',
                'attributes' => [
                    'data-icon' => 'fab fa-mendeley',
                ],
            ],
            [
                'value' => 'fas fa-menorah',
                'label' => 'Menorah',
                'attributes' => [
                    'data-icon' => 'fas fa-menorah',
                ],
            ],
            [
                'value' => 'fas fa-mercury',
                'label' => 'Mercury',
                'attributes' => [
                    'data-icon' => 'fas fa-mercury',
                ],
            ],
            [
                'value' => 'fas fa-message',
                'label' => 'Message',
                'attributes' => [
                    'data-icon' => 'fas fa-message',
                ],
            ],
            [
                'value' => 'far fa-message',
                'label' => 'Message',
                'attributes' => [
                    'data-icon' => 'far fa-message',
                ],
            ],
            [
                'value' => 'fas fa-meteor',
                'label' => 'Meteor',
                'attributes' => [
                    'data-icon' => 'fas fa-meteor',
                ],
            ],
            [
                'value' => 'fab fa-microblog',
                'label' => 'Micro.blog',
                'attributes' => [
                    'data-icon' => 'fab fa-microblog',
                ],
            ],
            [
                'value' => 'fas fa-microchip',
                'label' => 'Microchip',
                'attributes' => [
                    'data-icon' => 'fas fa-microchip',
                ],
            ],
            [
                'value' => 'fas fa-microphone',
                'label' => 'microphone',
                'attributes' => [
                    'data-icon' => 'fas fa-microphone',
                ],
            ],
            [
                'value' => 'fas fa-microphone-lines',
                'label' => 'Microphone lines',
                'attributes' => [
                    'data-icon' => 'fas fa-microphone-lines',
                ],
            ],
            [
                'value' => 'fas fa-microphone-lines-slash',
                'label' => 'Microphone lines slash',
                'attributes' => [
                    'data-icon' => 'fas fa-microphone-lines-slash',
                ],
            ],
            [
                'value' => 'fas fa-microphone-slash',
                'label' => 'Microphone Slash',
                'attributes' => [
                    'data-icon' => 'fas fa-microphone-slash',
                ],
            ],
            [
                'value' => 'fas fa-microscope',
                'label' => 'Microscope',
                'attributes' => [
                    'data-icon' => 'fas fa-microscope',
                ],
            ],
            [
                'value' => 'fab fa-microsoft',
                'label' => 'Microsoft',
                'attributes' => [
                    'data-icon' => 'fab fa-microsoft',
                ],
            ],
            [
                'value' => 'fas fa-mill-sign',
                'label' => 'Mill Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-mill-sign',
                ],
            ],
            [
                'value' => 'fas fa-minimize',
                'label' => 'Minimize',
                'attributes' => [
                    'data-icon' => 'fas fa-minimize',
                ],
            ],
            [
                'value' => 'fas fa-minus',
                'label' => 'minus',
                'attributes' => [
                    'data-icon' => 'fas fa-minus',
                ],
            ],
            [
                'value' => 'fas fa-mitten',
                'label' => 'Mitten',
                'attributes' => [
                    'data-icon' => 'fas fa-mitten',
                ],
            ],
            [
                'value' => 'fab fa-mix',
                'label' => 'Mix',
                'attributes' => [
                    'data-icon' => 'fab fa-mix',
                ],
            ],
            [
                'value' => 'fab fa-mixcloud',
                'label' => 'Mixcloud',
                'attributes' => [
                    'data-icon' => 'fab fa-mixcloud',
                ],
            ],
            [
                'value' => 'fab fa-mixer',
                'label' => 'Mixer',
                'attributes' => [
                    'data-icon' => 'fab fa-mixer',
                ],
            ],
            [
                'value' => 'fab fa-mizuni',
                'label' => 'Mizuni',
                'attributes' => [
                    'data-icon' => 'fab fa-mizuni',
                ],
            ],
            [
                'value' => 'fas fa-mobile',
                'label' => 'Mobile',
                'attributes' => [
                    'data-icon' => 'fas fa-mobile',
                ],
            ],
            [
                'value' => 'fas fa-mobile-button',
                'label' => 'Mobile button',
                'attributes' => [
                    'data-icon' => 'fas fa-mobile-button',
                ],
            ],
            [
                'value' => 'fas fa-mobile-retro',
                'label' => 'Mobile Retro',
                'attributes' => [
                    'data-icon' => 'fas fa-mobile-retro',
                ],
            ],
            [
                'value' => 'fas fa-mobile-screen',
                'label' => 'Mobile screen',
                'attributes' => [
                    'data-icon' => 'fas fa-mobile-screen',
                ],
            ],
            [
                'value' => 'fas fa-mobile-screen-button',
                'label' => 'Mobile screen button',
                'attributes' => [
                    'data-icon' => 'fas fa-mobile-screen-button',
                ],
            ],
            [
                'value' => 'fab fa-modx',
                'label' => 'MODX',
                'attributes' => [
                    'data-icon' => 'fab fa-modx',
                ],
            ],
            [
                'value' => 'fab fa-monero',
                'label' => 'Monero',
                'attributes' => [
                    'data-icon' => 'fab fa-monero',
                ],
            ],
            [
                'value' => 'fas fa-money-bill',
                'label' => 'Money Bill',
                'attributes' => [
                    'data-icon' => 'fas fa-money-bill',
                ],
            ],
            [
                'value' => 'fas fa-money-bill-1',
                'label' => 'Money bill 1',
                'attributes' => [
                    'data-icon' => 'fas fa-money-bill-1',
                ],
            ],
            [
                'value' => 'far fa-money-bill-1',
                'label' => 'Money bill 1',
                'attributes' => [
                    'data-icon' => 'far fa-money-bill-1',
                ],
            ],
            [
                'value' => 'fas fa-money-bill-1-wave',
                'label' => 'Money bill 1 wave',
                'attributes' => [
                    'data-icon' => 'fas fa-money-bill-1-wave',
                ],
            ],
            [
                'value' => 'fas fa-money-bill-transfer',
                'label' => 'Money Bill-transfer',
                'attributes' => [
                    'data-icon' => 'fas fa-money-bill-transfer',
                ],
            ],
            [
                'value' => 'fas fa-money-bill-trend-up',
                'label' => 'Money Bill-trend-up',
                'attributes' => [
                    'data-icon' => 'fas fa-money-bill-trend-up',
                ],
            ],
            [
                'value' => 'fas fa-money-bill-wave',
                'label' => 'Wavy Money Bill',
                'attributes' => [
                    'data-icon' => 'fas fa-money-bill-wave',
                ],
            ],
            [
                'value' => 'fas fa-money-bill-wheat',
                'label' => 'Money Bill-wheat',
                'attributes' => [
                    'data-icon' => 'fas fa-money-bill-wheat',
                ],
            ],
            [
                'value' => 'fas fa-money-bills',
                'label' => 'Money Bills',
                'attributes' => [
                    'data-icon' => 'fas fa-money-bills',
                ],
            ],
            [
                'value' => 'fas fa-money-check',
                'label' => 'Money Check',
                'attributes' => [
                    'data-icon' => 'fas fa-money-check',
                ],
            ],
            [
                'value' => 'fas fa-money-check-dollar',
                'label' => 'Money check dollar',
                'attributes' => [
                    'data-icon' => 'fas fa-money-check-dollar',
                ],
            ],
            [
                'value' => 'fas fa-monument',
                'label' => 'Monument',
                'attributes' => [
                    'data-icon' => 'fas fa-monument',
                ],
            ],
            [
                'value' => 'fas fa-moon',
                'label' => 'Moon',
                'attributes' => [
                    'data-icon' => 'fas fa-moon',
                ],
            ],
            [
                'value' => 'far fa-moon',
                'label' => 'Moon',
                'attributes' => [
                    'data-icon' => 'far fa-moon',
                ],
            ],
            [
                'value' => 'fas fa-mortar-pestle',
                'label' => 'Mortar Pestle',
                'attributes' => [
                    'data-icon' => 'fas fa-mortar-pestle',
                ],
            ],
            [
                'value' => 'fas fa-mosque',
                'label' => 'Mosque',
                'attributes' => [
                    'data-icon' => 'fas fa-mosque',
                ],
            ],
            [
                'value' => 'fas fa-mosquito',
                'label' => 'Mosquito',
                'attributes' => [
                    'data-icon' => 'fas fa-mosquito',
                ],
            ],
            [
                'value' => 'fas fa-mosquito-net',
                'label' => 'Mosquito Net',
                'attributes' => [
                    'data-icon' => 'fas fa-mosquito-net',
                ],
            ],
            [
                'value' => 'fas fa-motorcycle',
                'label' => 'Motorcycle',
                'attributes' => [
                    'data-icon' => 'fas fa-motorcycle',
                ],
            ],
            [
                'value' => 'fas fa-mound',
                'label' => 'Mound',
                'attributes' => [
                    'data-icon' => 'fas fa-mound',
                ],
            ],
            [
                'value' => 'fas fa-mountain',
                'label' => 'Mountain',
                'attributes' => [
                    'data-icon' => 'fas fa-mountain',
                ],
            ],
            [
                'value' => 'fas fa-mountain-city',
                'label' => 'Mountain City',
                'attributes' => [
                    'data-icon' => 'fas fa-mountain-city',
                ],
            ],
            [
                'value' => 'fas fa-mountain-sun',
                'label' => 'Mountain Sun',
                'attributes' => [
                    'data-icon' => 'fas fa-mountain-sun',
                ],
            ],
            [
                'value' => 'fas fa-mug-hot',
                'label' => 'Mug Hot',
                'attributes' => [
                    'data-icon' => 'fas fa-mug-hot',
                ],
            ],
            [
                'value' => 'fas fa-mug-saucer',
                'label' => 'Mug saucer',
                'attributes' => [
                    'data-icon' => 'fas fa-mug-saucer',
                ],
            ],
            [
                'value' => 'fas fa-music',
                'label' => 'Music',
                'attributes' => [
                    'data-icon' => 'fas fa-music',
                ],
            ],
            [
                'value' => 'fas fa-n',
                'label' => 'N',
                'attributes' => [
                    'data-icon' => 'fas fa-n',
                ],
            ],
            [
                'value' => 'fas fa-naira-sign',
                'label' => 'Naira Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-naira-sign',
                ],
            ],
            [
                'value' => 'fab fa-napster',
                'label' => 'Napster',
                'attributes' => [
                    'data-icon' => 'fab fa-napster',
                ],
            ],
            [
                'value' => 'fab fa-neos',
                'label' => 'Neos',
                'attributes' => [
                    'data-icon' => 'fab fa-neos',
                ],
            ],
            [
                'value' => 'fas fa-network-wired',
                'label' => 'Wired Network',
                'attributes' => [
                    'data-icon' => 'fas fa-network-wired',
                ],
            ],
            [
                'value' => 'fas fa-neuter',
                'label' => 'Neuter',
                'attributes' => [
                    'data-icon' => 'fas fa-neuter',
                ],
            ],
            [
                'value' => 'fas fa-newspaper',
                'label' => 'Newspaper',
                'attributes' => [
                    'data-icon' => 'fas fa-newspaper',
                ],
            ],
            [
                'value' => 'far fa-newspaper',
                'label' => 'Newspaper',
                'attributes' => [
                    'data-icon' => 'far fa-newspaper',
                ],
            ],
            [
                'value' => 'fab fa-nfc-directional',
                'label' => 'NFC Directional',
                'attributes' => [
                    'data-icon' => 'fab fa-nfc-directional',
                ],
            ],
            [
                'value' => 'fab fa-nfc-symbol',
                'label' => 'NFC Simplified',
                'attributes' => [
                    'data-icon' => 'fab fa-nfc-symbol',
                ],
            ],
            [
                'value' => 'fab fa-nimblr',
                'label' => 'Nimblr',
                'attributes' => [
                    'data-icon' => 'fab fa-nimblr',
                ],
            ],
            [
                'value' => 'fab fa-node',
                'label' => 'Node.js',
                'attributes' => [
                    'data-icon' => 'fab fa-node',
                ],
            ],
            [
                'value' => 'fab fa-node-js',
                'label' => 'Node.js JS',
                'attributes' => [
                    'data-icon' => 'fab fa-node-js',
                ],
            ],
            [
                'value' => 'fas fa-not-equal',
                'label' => 'Not Equal',
                'attributes' => [
                    'data-icon' => 'fas fa-not-equal',
                ],
            ],
            [
                'value' => 'fas fa-note-sticky',
                'label' => 'Note sticky',
                'attributes' => [
                    'data-icon' => 'fas fa-note-sticky',
                ],
            ],
            [
                'value' => 'far fa-note-sticky',
                'label' => 'Note sticky',
                'attributes' => [
                    'data-icon' => 'far fa-note-sticky',
                ],
            ],
            [
                'value' => 'fas fa-notes-medical',
                'label' => 'Medical Notes',
                'attributes' => [
                    'data-icon' => 'fas fa-notes-medical',
                ],
            ],
            [
                'value' => 'fab fa-npm',
                'label' => 'npm',
                'attributes' => [
                    'data-icon' => 'fab fa-npm',
                ],
            ],
            [
                'value' => 'fab fa-ns8',
                'label' => 'NS8',
                'attributes' => [
                    'data-icon' => 'fab fa-ns8',
                ],
            ],
            [
                'value' => 'fab fa-nutritionix',
                'label' => 'Nutritionix',
                'attributes' => [
                    'data-icon' => 'fab fa-nutritionix',
                ],
            ],
            [
                'value' => 'fas fa-o',
                'label' => 'O',
                'attributes' => [
                    'data-icon' => 'fas fa-o',
                ],
            ],
            [
                'value' => 'fas fa-object-group',
                'label' => 'Object Group',
                'attributes' => [
                    'data-icon' => 'fas fa-object-group',
                ],
            ],
            [
                'value' => 'far fa-object-group',
                'label' => 'Object Group',
                'attributes' => [
                    'data-icon' => 'far fa-object-group',
                ],
            ],
            [
                'value' => 'fas fa-object-ungroup',
                'label' => 'Object Ungroup',
                'attributes' => [
                    'data-icon' => 'fas fa-object-ungroup',
                ],
            ],
            [
                'value' => 'far fa-object-ungroup',
                'label' => 'Object Ungroup',
                'attributes' => [
                    'data-icon' => 'far fa-object-ungroup',
                ],
            ],
            [
                'value' => 'fab fa-octopus-deploy',
                'label' => 'Octopus Deploy',
                'attributes' => [
                    'data-icon' => 'fab fa-octopus-deploy',
                ],
            ],
            [
                'value' => 'fab fa-odnoklassniki',
                'label' => 'Odnoklassniki',
                'attributes' => [
                    'data-icon' => 'fab fa-odnoklassniki',
                ],
            ],
            [
                'value' => 'fab fa-odnoklassniki-square',
                'label' => 'Odnoklassniki Square',
                'attributes' => [
                    'data-icon' => 'fab fa-odnoklassniki-square',
                ],
            ],
            [
                'value' => 'fas fa-oil-can',
                'label' => 'Oil Can',
                'attributes' => [
                    'data-icon' => 'fas fa-oil-can',
                ],
            ],
            [
                'value' => 'fas fa-oil-well',
                'label' => 'Oil Well',
                'attributes' => [
                    'data-icon' => 'fas fa-oil-well',
                ],
            ],
            [
                'value' => 'fab fa-old-republic',
                'label' => 'Old Republic',
                'attributes' => [
                    'data-icon' => 'fab fa-old-republic',
                ],
            ],
            [
                'value' => 'fas fa-om',
                'label' => 'Om',
                'attributes' => [
                    'data-icon' => 'fas fa-om',
                ],
            ],
            [
                'value' => 'fab fa-opencart',
                'label' => 'OpenCart',
                'attributes' => [
                    'data-icon' => 'fab fa-opencart',
                ],
            ],
            [
                'value' => 'fab fa-openid',
                'label' => 'OpenID',
                'attributes' => [
                    'data-icon' => 'fab fa-openid',
                ],
            ],
            [
                'value' => 'fab fa-opera',
                'label' => 'Opera',
                'attributes' => [
                    'data-icon' => 'fab fa-opera',
                ],
            ],
            [
                'value' => 'fab fa-optin-monster',
                'label' => 'Optin Monster',
                'attributes' => [
                    'data-icon' => 'fab fa-optin-monster',
                ],
            ],
            [
                'value' => 'fab fa-orcid',
                'label' => 'ORCID',
                'attributes' => [
                    'data-icon' => 'fab fa-orcid',
                ],
            ],
            [
                'value' => 'fab fa-osi',
                'label' => 'Open Source Initiative',
                'attributes' => [
                    'data-icon' => 'fab fa-osi',
                ],
            ],
            [
                'value' => 'fas fa-otter',
                'label' => 'Otter',
                'attributes' => [
                    'data-icon' => 'fas fa-otter',
                ],
            ],
            [
                'value' => 'fas fa-outdent',
                'label' => 'Outdent',
                'attributes' => [
                    'data-icon' => 'fas fa-outdent',
                ],
            ],
            [
                'value' => 'fas fa-p',
                'label' => 'P',
                'attributes' => [
                    'data-icon' => 'fas fa-p',
                ],
            ],
            [
                'value' => 'fab fa-padlet',
                'label' => 'Padlet',
                'attributes' => [
                    'data-icon' => 'fab fa-padlet',
                ],
            ],
            [
                'value' => 'fab fa-page4',
                'label' => 'page4 Corporation',
                'attributes' => [
                    'data-icon' => 'fab fa-page4',
                ],
            ],
            [
                'value' => 'fab fa-pagelines',
                'label' => 'Pagelines',
                'attributes' => [
                    'data-icon' => 'fab fa-pagelines',
                ],
            ],
            [
                'value' => 'fas fa-pager',
                'label' => 'Pager',
                'attributes' => [
                    'data-icon' => 'fas fa-pager',
                ],
            ],
            [
                'value' => 'fas fa-paint-roller',
                'label' => 'Paint Roller',
                'attributes' => [
                    'data-icon' => 'fas fa-paint-roller',
                ],
            ],
            [
                'value' => 'fas fa-paintbrush',
                'label' => 'Paint Brush',
                'attributes' => [
                    'data-icon' => 'fas fa-paintbrush',
                ],
            ],
            [
                'value' => 'fas fa-palette',
                'label' => 'Palette',
                'attributes' => [
                    'data-icon' => 'fas fa-palette',
                ],
            ],
            [
                'value' => 'fab fa-palfed',
                'label' => 'Palfed',
                'attributes' => [
                    'data-icon' => 'fab fa-palfed',
                ],
            ],
            [
                'value' => 'fas fa-pallet',
                'label' => 'Pallet',
                'attributes' => [
                    'data-icon' => 'fas fa-pallet',
                ],
            ],
            [
                'value' => 'fas fa-panorama',
                'label' => 'Panorama',
                'attributes' => [
                    'data-icon' => 'fas fa-panorama',
                ],
            ],
            [
                'value' => 'fas fa-paper-plane',
                'label' => 'Paper Plane',
                'attributes' => [
                    'data-icon' => 'fas fa-paper-plane',
                ],
            ],
            [
                'value' => 'far fa-paper-plane',
                'label' => 'Paper Plane',
                'attributes' => [
                    'data-icon' => 'far fa-paper-plane',
                ],
            ],
            [
                'value' => 'fas fa-paperclip',
                'label' => 'Paperclip',
                'attributes' => [
                    'data-icon' => 'fas fa-paperclip',
                ],
            ],
            [
                'value' => 'fas fa-parachute-box',
                'label' => 'Parachute Box',
                'attributes' => [
                    'data-icon' => 'fas fa-parachute-box',
                ],
            ],
            [
                'value' => 'fas fa-paragraph',
                'label' => 'paragraph',
                'attributes' => [
                    'data-icon' => 'fas fa-paragraph',
                ],
            ],
            [
                'value' => 'fas fa-passport',
                'label' => 'Passport',
                'attributes' => [
                    'data-icon' => 'fas fa-passport',
                ],
            ],
            [
                'value' => 'fas fa-paste',
                'label' => 'Paste',
                'attributes' => [
                    'data-icon' => 'fas fa-paste',
                ],
            ],
            [
                'value' => 'far fa-paste',
                'label' => 'Paste',
                'attributes' => [
                    'data-icon' => 'far fa-paste',
                ],
            ],
            [
                'value' => 'fab fa-patreon',
                'label' => 'Patreon',
                'attributes' => [
                    'data-icon' => 'fab fa-patreon',
                ],
            ],
            [
                'value' => 'fas fa-pause',
                'label' => 'pause',
                'attributes' => [
                    'data-icon' => 'fas fa-pause',
                ],
            ],
            [
                'value' => 'fas fa-paw',
                'label' => 'Paw',
                'attributes' => [
                    'data-icon' => 'fas fa-paw',
                ],
            ],
            [
                'value' => 'fab fa-paypal',
                'label' => 'Paypal',
                'attributes' => [
                    'data-icon' => 'fab fa-paypal',
                ],
            ],
            [
                'value' => 'fas fa-peace',
                'label' => 'Peace',
                'attributes' => [
                    'data-icon' => 'fas fa-peace',
                ],
            ],
            [
                'value' => 'fas fa-pen',
                'label' => 'Pen',
                'attributes' => [
                    'data-icon' => 'fas fa-pen',
                ],
            ],
            [
                'value' => 'fas fa-pen-clip',
                'label' => 'Pen clip',
                'attributes' => [
                    'data-icon' => 'fas fa-pen-clip',
                ],
            ],
            [
                'value' => 'fas fa-pen-fancy',
                'label' => 'Pen Fancy',
                'attributes' => [
                    'data-icon' => 'fas fa-pen-fancy',
                ],
            ],
            [
                'value' => 'fas fa-pen-nib',
                'label' => 'Pen Nib',
                'attributes' => [
                    'data-icon' => 'fas fa-pen-nib',
                ],
            ],
            [
                'value' => 'fas fa-pen-ruler',
                'label' => 'Pen ruler',
                'attributes' => [
                    'data-icon' => 'fas fa-pen-ruler',
                ],
            ],
            [
                'value' => 'fas fa-pen-to-square',
                'label' => 'Pen to square',
                'attributes' => [
                    'data-icon' => 'fas fa-pen-to-square',
                ],
            ],
            [
                'value' => 'far fa-pen-to-square',
                'label' => 'Pen to square',
                'attributes' => [
                    'data-icon' => 'far fa-pen-to-square',
                ],
            ],
            [
                'value' => 'fas fa-pencil',
                'label' => 'pencil',
                'attributes' => [
                    'data-icon' => 'fas fa-pencil',
                ],
            ],
            [
                'value' => 'fas fa-people-arrows-left-right',
                'label' => 'People arrows left right',
                'attributes' => [
                    'data-icon' => 'fas fa-people-arrows-left-right',
                ],
            ],
            [
                'value' => 'fas fa-people-carry-box',
                'label' => 'People carry box',
                'attributes' => [
                    'data-icon' => 'fas fa-people-carry-box',
                ],
            ],
            [
                'value' => 'fas fa-people-group',
                'label' => 'People Group',
                'attributes' => [
                    'data-icon' => 'fas fa-people-group',
                ],
            ],
            [
                'value' => 'fas fa-people-line',
                'label' => 'People Line',
                'attributes' => [
                    'data-icon' => 'fas fa-people-line',
                ],
            ],
            [
                'value' => 'fas fa-people-pulling',
                'label' => 'People Pulling',
                'attributes' => [
                    'data-icon' => 'fas fa-people-pulling',
                ],
            ],
            [
                'value' => 'fas fa-people-robbery',
                'label' => 'People Robbery',
                'attributes' => [
                    'data-icon' => 'fas fa-people-robbery',
                ],
            ],
            [
                'value' => 'fas fa-people-roof',
                'label' => 'People Roof',
                'attributes' => [
                    'data-icon' => 'fas fa-people-roof',
                ],
            ],
            [
                'value' => 'fas fa-pepper-hot',
                'label' => 'Hot Pepper',
                'attributes' => [
                    'data-icon' => 'fas fa-pepper-hot',
                ],
            ],
            [
                'value' => 'fab fa-perbyte',
                'label' => 'PerByte',
                'attributes' => [
                    'data-icon' => 'fab fa-perbyte',
                ],
            ],
            [
                'value' => 'fas fa-percent',
                'label' => 'Percent',
                'attributes' => [
                    'data-icon' => 'fas fa-percent',
                ],
            ],
            [
                'value' => 'fab fa-periscope',
                'label' => 'Periscope',
                'attributes' => [
                    'data-icon' => 'fab fa-periscope',
                ],
            ],
            [
                'value' => 'fas fa-person',
                'label' => 'Person',
                'attributes' => [
                    'data-icon' => 'fas fa-person',
                ],
            ],
            [
                'value' => 'fas fa-person-arrow-down-to-line',
                'label' => 'Person Arrow-down-to-line',
                'attributes' => [
                    'data-icon' => 'fas fa-person-arrow-down-to-line',
                ],
            ],
            [
                'value' => 'fas fa-person-arrow-up-from-line',
                'label' => 'Person Arrow-up-from-line',
                'attributes' => [
                    'data-icon' => 'fas fa-person-arrow-up-from-line',
                ],
            ],
            [
                'value' => 'fas fa-person-biking',
                'label' => 'Person biking',
                'attributes' => [
                    'data-icon' => 'fas fa-person-biking',
                ],
            ],
            [
                'value' => 'fas fa-person-booth',
                'label' => 'Person Entering Booth',
                'attributes' => [
                    'data-icon' => 'fas fa-person-booth',
                ],
            ],
            [
                'value' => 'fas fa-person-breastfeeding',
                'label' => 'Person Breastfeeding',
                'attributes' => [
                    'data-icon' => 'fas fa-person-breastfeeding',
                ],
            ],
            [
                'value' => 'fas fa-person-burst',
                'label' => 'Person Burst',
                'attributes' => [
                    'data-icon' => 'fas fa-person-burst',
                ],
            ],
            [
                'value' => 'fas fa-person-cane',
                'label' => 'Person Cane',
                'attributes' => [
                    'data-icon' => 'fas fa-person-cane',
                ],
            ],
            [
                'value' => 'fas fa-person-chalkboard',
                'label' => 'Person Chalkboard',
                'attributes' => [
                    'data-icon' => 'fas fa-person-chalkboard',
                ],
            ],
            [
                'value' => 'fas fa-person-circle-check',
                'label' => 'Person Circle-check',
                'attributes' => [
                    'data-icon' => 'fas fa-person-circle-check',
                ],
            ],
            [
                'value' => 'fas fa-person-circle-exclamation',
                'label' => 'Person Circle-exclamation',
                'attributes' => [
                    'data-icon' => 'fas fa-person-circle-exclamation',
                ],
            ],
            [
                'value' => 'fas fa-person-circle-minus',
                'label' => 'Person Circle-minus',
                'attributes' => [
                    'data-icon' => 'fas fa-person-circle-minus',
                ],
            ],
            [
                'value' => 'fas fa-person-circle-plus',
                'label' => 'Person Circle-plus',
                'attributes' => [
                    'data-icon' => 'fas fa-person-circle-plus',
                ],
            ],
            [
                'value' => 'fas fa-person-circle-question',
                'label' => 'Person Circle-question',
                'attributes' => [
                    'data-icon' => 'fas fa-person-circle-question',
                ],
            ],
            [
                'value' => 'fas fa-person-circle-xmark',
                'label' => 'Person Circle-xmark',
                'attributes' => [
                    'data-icon' => 'fas fa-person-circle-xmark',
                ],
            ],
            [
                'value' => 'fas fa-person-digging',
                'label' => 'Person digging',
                'attributes' => [
                    'data-icon' => 'fas fa-person-digging',
                ],
            ],
            [
                'value' => 'fas fa-person-dots-from-line',
                'label' => 'Person dots from line',
                'attributes' => [
                    'data-icon' => 'fas fa-person-dots-from-line',
                ],
            ],
            [
                'value' => 'fas fa-person-dress',
                'label' => 'Person dress',
                'attributes' => [
                    'data-icon' => 'fas fa-person-dress',
                ],
            ],
            [
                'value' => 'fas fa-person-dress-burst',
                'label' => 'Person Dress BUrst',
                'attributes' => [
                    'data-icon' => 'fas fa-person-dress-burst',
                ],
            ],
            [
                'value' => 'fas fa-person-drowning',
                'label' => 'Person Drowning',
                'attributes' => [
                    'data-icon' => 'fas fa-person-drowning',
                ],
            ],
            [
                'value' => 'fas fa-person-falling',
                'label' => 'Person Falling',
                'attributes' => [
                    'data-icon' => 'fas fa-person-falling',
                ],
            ],
            [
                'value' => 'fas fa-person-falling-burst',
                'label' => 'Person Falling Burst',
                'attributes' => [
                    'data-icon' => 'fas fa-person-falling-burst',
                ],
            ],
            [
                'value' => 'fas fa-person-half-dress',
                'label' => 'Person Half-dress',
                'attributes' => [
                    'data-icon' => 'fas fa-person-half-dress',
                ],
            ],
            [
                'value' => 'fas fa-person-harassing',
                'label' => 'Person Harassing',
                'attributes' => [
                    'data-icon' => 'fas fa-person-harassing',
                ],
            ],
            [
                'value' => 'fas fa-person-hiking',
                'label' => 'Person hiking',
                'attributes' => [
                    'data-icon' => 'fas fa-person-hiking',
                ],
            ],
            [
                'value' => 'fas fa-person-military-pointing',
                'label' => 'Person Military-pointing',
                'attributes' => [
                    'data-icon' => 'fas fa-person-military-pointing',
                ],
            ],
            [
                'value' => 'fas fa-person-military-rifle',
                'label' => 'Person Military-rifle',
                'attributes' => [
                    'data-icon' => 'fas fa-person-military-rifle',
                ],
            ],
            [
                'value' => 'fas fa-person-military-to-person',
                'label' => 'Person Military-to-person',
                'attributes' => [
                    'data-icon' => 'fas fa-person-military-to-person',
                ],
            ],
            [
                'value' => 'fas fa-person-praying',
                'label' => 'Person praying',
                'attributes' => [
                    'data-icon' => 'fas fa-person-praying',
                ],
            ],
            [
                'value' => 'fas fa-person-pregnant',
                'label' => 'Person Pregnant',
                'attributes' => [
                    'data-icon' => 'fas fa-person-pregnant',
                ],
            ],
            [
                'value' => 'fas fa-person-rays',
                'label' => 'Person Rays',
                'attributes' => [
                    'data-icon' => 'fas fa-person-rays',
                ],
            ],
            [
                'value' => 'fas fa-person-rifle',
                'label' => 'Person Rifle',
                'attributes' => [
                    'data-icon' => 'fas fa-person-rifle',
                ],
            ],
            [
                'value' => 'fas fa-person-running',
                'label' => 'Person running',
                'attributes' => [
                    'data-icon' => 'fas fa-person-running',
                ],
            ],
            [
                'value' => 'fas fa-person-shelter',
                'label' => 'Person Shelter',
                'attributes' => [
                    'data-icon' => 'fas fa-person-shelter',
                ],
            ],
            [
                'value' => 'fas fa-person-skating',
                'label' => 'Person skating',
                'attributes' => [
                    'data-icon' => 'fas fa-person-skating',
                ],
            ],
            [
                'value' => 'fas fa-person-skiing',
                'label' => 'Person skiing',
                'attributes' => [
                    'data-icon' => 'fas fa-person-skiing',
                ],
            ],
            [
                'value' => 'fas fa-person-skiing-nordic',
                'label' => 'Person skiing nordic',
                'attributes' => [
                    'data-icon' => 'fas fa-person-skiing-nordic',
                ],
            ],
            [
                'value' => 'fas fa-person-snowboarding',
                'label' => 'Person snowboarding',
                'attributes' => [
                    'data-icon' => 'fas fa-person-snowboarding',
                ],
            ],
            [
                'value' => 'fas fa-person-swimming',
                'label' => 'Person swimming',
                'attributes' => [
                    'data-icon' => 'fas fa-person-swimming',
                ],
            ],
            [
                'value' => 'fas fa-person-through-window',
                'label' => 'Person Through-window',
                'attributes' => [
                    'data-icon' => 'fas fa-person-through-window',
                ],
            ],
            [
                'value' => 'fas fa-person-walking',
                'label' => 'Person walking',
                'attributes' => [
                    'data-icon' => 'fas fa-person-walking',
                ],
            ],
            [
                'value' => 'fas fa-person-walking-arrow-loop-left',
                'label' => 'Person Walking-arrow-loop-left',
                'attributes' => [
                    'data-icon' => 'fas fa-person-walking-arrow-loop-left',
                ],
            ],
            [
                'value' => 'fas fa-person-walking-arrow-right',
                'label' => 'Person Walking-arrow-right',
                'attributes' => [
                    'data-icon' => 'fas fa-person-walking-arrow-right',
                ],
            ],
            [
                'value' => 'fas fa-person-walking-dashed-line-arrow-right',
                'label' => 'Person Walking-dashed-line-arrow-right',
                'attributes' => [
                    'data-icon' => 'fas fa-person-walking-dashed-line-arrow-right',
                ],
            ],
            [
                'value' => 'fas fa-person-walking-luggage',
                'label' => 'Person Walking-luggage',
                'attributes' => [
                    'data-icon' => 'fas fa-person-walking-luggage',
                ],
            ],
            [
                'value' => 'fas fa-person-walking-with-cane',
                'label' => 'Person walking with cane',
                'attributes' => [
                    'data-icon' => 'fas fa-person-walking-with-cane',
                ],
            ],
            [
                'value' => 'fas fa-peseta-sign',
                'label' => 'Peseta Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-peseta-sign',
                ],
            ],
            [
                'value' => 'fas fa-peso-sign',
                'label' => 'Peso Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-peso-sign',
                ],
            ],
            [
                'value' => 'fab fa-phabricator',
                'label' => 'Phabricator',
                'attributes' => [
                    'data-icon' => 'fab fa-phabricator',
                ],
            ],
            [
                'value' => 'fab fa-phoenix-framework',
                'label' => 'Phoenix Framework',
                'attributes' => [
                    'data-icon' => 'fab fa-phoenix-framework',
                ],
            ],
            [
                'value' => 'fab fa-phoenix-squadron',
                'label' => 'Phoenix Squadron',
                'attributes' => [
                    'data-icon' => 'fab fa-phoenix-squadron',
                ],
            ],
            [
                'value' => 'fas fa-phone',
                'label' => 'Phone',
                'attributes' => [
                    'data-icon' => 'fas fa-phone',
                ],
            ],
            [
                'value' => 'fas fa-phone-flip',
                'label' => 'Phone flip',
                'attributes' => [
                    'data-icon' => 'fas fa-phone-flip',
                ],
            ],
            [
                'value' => 'fas fa-phone-slash',
                'label' => 'Phone Slash',
                'attributes' => [
                    'data-icon' => 'fas fa-phone-slash',
                ],
            ],
            [
                'value' => 'fas fa-phone-volume',
                'label' => 'Phone Volume',
                'attributes' => [
                    'data-icon' => 'fas fa-phone-volume',
                ],
            ],
            [
                'value' => 'fas fa-photo-film',
                'label' => 'Photo film',
                'attributes' => [
                    'data-icon' => 'fas fa-photo-film',
                ],
            ],
            [
                'value' => 'fab fa-php',
                'label' => 'PHP',
                'attributes' => [
                    'data-icon' => 'fab fa-php',
                ],
            ],
            [
                'value' => 'fab fa-pied-piper',
                'label' => 'Pied Piper Logo',
                'attributes' => [
                    'data-icon' => 'fab fa-pied-piper',
                ],
            ],
            [
                'value' => 'fab fa-pied-piper-alt',
                'label' => 'Alternate Pied Piper Logo (Old)',
                'attributes' => [
                    'data-icon' => 'fab fa-pied-piper-alt',
                ],
            ],
            [
                'value' => 'fab fa-pied-piper-hat',
                'label' => 'Pied Piper Hat (Old)',
                'attributes' => [
                    'data-icon' => 'fab fa-pied-piper-hat',
                ],
            ],
            [
                'value' => 'fab fa-pied-piper-pp',
                'label' => 'Pied Piper PP Logo (Old)',
                'attributes' => [
                    'data-icon' => 'fab fa-pied-piper-pp',
                ],
            ],
            [
                'value' => 'fab fa-pied-piper-square',
                'label' => 'Pied Piper Square Logo (Old)',
                'attributes' => [
                    'data-icon' => 'fab fa-pied-piper-square',
                ],
            ],
            [
                'value' => 'fas fa-piggy-bank',
                'label' => 'Piggy Bank',
                'attributes' => [
                    'data-icon' => 'fas fa-piggy-bank',
                ],
            ],
            [
                'value' => 'fas fa-pills',
                'label' => 'Pills',
                'attributes' => [
                    'data-icon' => 'fas fa-pills',
                ],
            ],
            [
                'value' => 'fab fa-pinterest',
                'label' => 'Pinterest',
                'attributes' => [
                    'data-icon' => 'fab fa-pinterest',
                ],
            ],
            [
                'value' => 'fab fa-pinterest-p',
                'label' => 'Pinterest P',
                'attributes' => [
                    'data-icon' => 'fab fa-pinterest-p',
                ],
            ],
            [
                'value' => 'fab fa-pinterest-square',
                'label' => 'Pinterest Square',
                'attributes' => [
                    'data-icon' => 'fab fa-pinterest-square',
                ],
            ],
            [
                'value' => 'fab fa-pix',
                'label' => 'Pix',
                'attributes' => [
                    'data-icon' => 'fab fa-pix',
                ],
            ],
            [
                'value' => 'fas fa-pizza-slice',
                'label' => 'Pizza Slice',
                'attributes' => [
                    'data-icon' => 'fas fa-pizza-slice',
                ],
            ],
            [
                'value' => 'fas fa-place-of-worship',
                'label' => 'Place of Worship',
                'attributes' => [
                    'data-icon' => 'fas fa-place-of-worship',
                ],
            ],
            [
                'value' => 'fas fa-plane',
                'label' => 'plane',
                'attributes' => [
                    'data-icon' => 'fas fa-plane',
                ],
            ],
            [
                'value' => 'fas fa-plane-arrival',
                'label' => 'Plane Arrival',
                'attributes' => [
                    'data-icon' => 'fas fa-plane-arrival',
                ],
            ],
            [
                'value' => 'fas fa-plane-circle-check',
                'label' => 'Plane Circle-check',
                'attributes' => [
                    'data-icon' => 'fas fa-plane-circle-check',
                ],
            ],
            [
                'value' => 'fas fa-plane-circle-exclamation',
                'label' => 'Plane Circle-exclamation',
                'attributes' => [
                    'data-icon' => 'fas fa-plane-circle-exclamation',
                ],
            ],
            [
                'value' => 'fas fa-plane-circle-xmark',
                'label' => 'Plane Circle-xmark',
                'attributes' => [
                    'data-icon' => 'fas fa-plane-circle-xmark',
                ],
            ],
            [
                'value' => 'fas fa-plane-departure',
                'label' => 'Plane Departure',
                'attributes' => [
                    'data-icon' => 'fas fa-plane-departure',
                ],
            ],
            [
                'value' => 'fas fa-plane-lock',
                'label' => 'Plane Lock',
                'attributes' => [
                    'data-icon' => 'fas fa-plane-lock',
                ],
            ],
            [
                'value' => 'fas fa-plane-slash',
                'label' => 'Plane Slash',
                'attributes' => [
                    'data-icon' => 'fas fa-plane-slash',
                ],
            ],
            [
                'value' => 'fas fa-plane-up',
                'label' => 'Plane Up',
                'attributes' => [
                    'data-icon' => 'fas fa-plane-up',
                ],
            ],
            [
                'value' => 'fas fa-plant-wilt',
                'label' => 'Plant Wilt',
                'attributes' => [
                    'data-icon' => 'fas fa-plant-wilt',
                ],
            ],
            [
                'value' => 'fas fa-plate-wheat',
                'label' => 'Plate Wheat',
                'attributes' => [
                    'data-icon' => 'fas fa-plate-wheat',
                ],
            ],
            [
                'value' => 'fas fa-play',
                'label' => 'play',
                'attributes' => [
                    'data-icon' => 'fas fa-play',
                ],
            ],
            [
                'value' => 'fab fa-playstation',
                'label' => 'PlayStation',
                'attributes' => [
                    'data-icon' => 'fab fa-playstation',
                ],
            ],
            [
                'value' => 'fas fa-plug',
                'label' => 'Plug',
                'attributes' => [
                    'data-icon' => 'fas fa-plug',
                ],
            ],
            [
                'value' => 'fas fa-plug-circle-bolt',
                'label' => 'Plug Circle-bolt',
                'attributes' => [
                    'data-icon' => 'fas fa-plug-circle-bolt',
                ],
            ],
            [
                'value' => 'fas fa-plug-circle-check',
                'label' => 'Plug Circle-check',
                'attributes' => [
                    'data-icon' => 'fas fa-plug-circle-check',
                ],
            ],
            [
                'value' => 'fas fa-plug-circle-exclamation',
                'label' => 'Plug Circle-exclamation',
                'attributes' => [
                    'data-icon' => 'fas fa-plug-circle-exclamation',
                ],
            ],
            [
                'value' => 'fas fa-plug-circle-minus',
                'label' => 'Plug Circle-minus',
                'attributes' => [
                    'data-icon' => 'fas fa-plug-circle-minus',
                ],
            ],
            [
                'value' => 'fas fa-plug-circle-plus',
                'label' => 'Plug Circle-plus',
                'attributes' => [
                    'data-icon' => 'fas fa-plug-circle-plus',
                ],
            ],
            [
                'value' => 'fas fa-plug-circle-xmark',
                'label' => 'Plug Circle-xmark',
                'attributes' => [
                    'data-icon' => 'fas fa-plug-circle-xmark',
                ],
            ],
            [
                'value' => 'fas fa-plus',
                'label' => 'plus',
                'attributes' => [
                    'data-icon' => 'fas fa-plus',
                ],
            ],
            [
                'value' => 'fas fa-plus-minus',
                'label' => 'Plus Minus',
                'attributes' => [
                    'data-icon' => 'fas fa-plus-minus',
                ],
            ],
            [
                'value' => 'fas fa-podcast',
                'label' => 'Podcast',
                'attributes' => [
                    'data-icon' => 'fas fa-podcast',
                ],
            ],
            [
                'value' => 'fas fa-poo',
                'label' => 'Poo',
                'attributes' => [
                    'data-icon' => 'fas fa-poo',
                ],
            ],
            [
                'value' => 'fas fa-poo-storm',
                'label' => 'Poo bolt',
                'attributes' => [
                    'data-icon' => 'fas fa-poo-storm',
                ],
            ],
            [
                'value' => 'fas fa-poop',
                'label' => 'Poop',
                'attributes' => [
                    'data-icon' => 'fas fa-poop',
                ],
            ],
            [
                'value' => 'fas fa-power-off',
                'label' => 'Power Off',
                'attributes' => [
                    'data-icon' => 'fas fa-power-off',
                ],
            ],
            [
                'value' => 'fas fa-prescription',
                'label' => 'Prescription',
                'attributes' => [
                    'data-icon' => 'fas fa-prescription',
                ],
            ],
            [
                'value' => 'fas fa-prescription-bottle',
                'label' => 'Prescription Bottle',
                'attributes' => [
                    'data-icon' => 'fas fa-prescription-bottle',
                ],
            ],
            [
                'value' => 'fas fa-prescription-bottle-medical',
                'label' => 'Prescription bottle medical',
                'attributes' => [
                    'data-icon' => 'fas fa-prescription-bottle-medical',
                ],
            ],
            [
                'value' => 'fas fa-print',
                'label' => 'print',
                'attributes' => [
                    'data-icon' => 'fas fa-print',
                ],
            ],
            [
                'value' => 'fab fa-product-hunt',
                'label' => 'Product Hunt',
                'attributes' => [
                    'data-icon' => 'fab fa-product-hunt',
                ],
            ],
            [
                'value' => 'fas fa-pump-medical',
                'label' => 'Pump Medical',
                'attributes' => [
                    'data-icon' => 'fas fa-pump-medical',
                ],
            ],
            [
                'value' => 'fas fa-pump-soap',
                'label' => 'Pump Soap',
                'attributes' => [
                    'data-icon' => 'fas fa-pump-soap',
                ],
            ],
            [
                'value' => 'fab fa-pushed',
                'label' => 'Pushed',
                'attributes' => [
                    'data-icon' => 'fab fa-pushed',
                ],
            ],
            [
                'value' => 'fas fa-puzzle-piece',
                'label' => 'Puzzle Piece',
                'attributes' => [
                    'data-icon' => 'fas fa-puzzle-piece',
                ],
            ],
            [
                'value' => 'fab fa-python',
                'label' => 'Python',
                'attributes' => [
                    'data-icon' => 'fab fa-python',
                ],
            ],
            [
                'value' => 'fas fa-q',
                'label' => 'Q',
                'attributes' => [
                    'data-icon' => 'fas fa-q',
                ],
            ],
            [
                'value' => 'fab fa-qq',
                'label' => 'QQ',
                'attributes' => [
                    'data-icon' => 'fab fa-qq',
                ],
            ],
            [
                'value' => 'fas fa-qrcode',
                'label' => 'qrcode',
                'attributes' => [
                    'data-icon' => 'fas fa-qrcode',
                ],
            ],
            [
                'value' => 'fas fa-question',
                'label' => 'Question',
                'attributes' => [
                    'data-icon' => 'fas fa-question',
                ],
            ],
            [
                'value' => 'fab fa-quinscape',
                'label' => 'QuinScape',
                'attributes' => [
                    'data-icon' => 'fab fa-quinscape',
                ],
            ],
            [
                'value' => 'fab fa-quora',
                'label' => 'Quora',
                'attributes' => [
                    'data-icon' => 'fab fa-quora',
                ],
            ],
            [
                'value' => 'fas fa-quote-left',
                'label' => 'quote-left',
                'attributes' => [
                    'data-icon' => 'fas fa-quote-left',
                ],
            ],
            [
                'value' => 'fas fa-quote-right',
                'label' => 'quote-right',
                'attributes' => [
                    'data-icon' => 'fas fa-quote-right',
                ],
            ],
            [
                'value' => 'fas fa-r',
                'label' => 'R',
                'attributes' => [
                    'data-icon' => 'fas fa-r',
                ],
            ],
            [
                'value' => 'fab fa-r-project',
                'label' => 'R Project',
                'attributes' => [
                    'data-icon' => 'fab fa-r-project',
                ],
            ],
            [
                'value' => 'fas fa-radiation',
                'label' => 'Radiation',
                'attributes' => [
                    'data-icon' => 'fas fa-radiation',
                ],
            ],
            [
                'value' => 'fas fa-radio',
                'label' => 'Radio',
                'attributes' => [
                    'data-icon' => 'fas fa-radio',
                ],
            ],
            [
                'value' => 'fas fa-rainbow',
                'label' => 'Rainbow',
                'attributes' => [
                    'data-icon' => 'fas fa-rainbow',
                ],
            ],
            [
                'value' => 'fas fa-ranking-star',
                'label' => 'Ranking Star',
                'attributes' => [
                    'data-icon' => 'fas fa-ranking-star',
                ],
            ],
            [
                'value' => 'fab fa-raspberry-pi',
                'label' => 'Raspberry Pi',
                'attributes' => [
                    'data-icon' => 'fab fa-raspberry-pi',
                ],
            ],
            [
                'value' => 'fab fa-ravelry',
                'label' => 'Ravelry',
                'attributes' => [
                    'data-icon' => 'fab fa-ravelry',
                ],
            ],
            [
                'value' => 'fab fa-react',
                'label' => 'React',
                'attributes' => [
                    'data-icon' => 'fab fa-react',
                ],
            ],
            [
                'value' => 'fab fa-reacteurope',
                'label' => 'ReactEurope',
                'attributes' => [
                    'data-icon' => 'fab fa-reacteurope',
                ],
            ],
            [
                'value' => 'fab fa-readme',
                'label' => 'ReadMe',
                'attributes' => [
                    'data-icon' => 'fab fa-readme',
                ],
            ],
            [
                'value' => 'fab fa-rebel',
                'label' => 'Rebel Alliance',
                'attributes' => [
                    'data-icon' => 'fab fa-rebel',
                ],
            ],
            [
                'value' => 'fas fa-receipt',
                'label' => 'Receipt',
                'attributes' => [
                    'data-icon' => 'fas fa-receipt',
                ],
            ],
            [
                'value' => 'fas fa-record-vinyl',
                'label' => 'Record Vinyl',
                'attributes' => [
                    'data-icon' => 'fas fa-record-vinyl',
                ],
            ],
            [
                'value' => 'fas fa-rectangle-ad',
                'label' => 'Rectangle ad',
                'attributes' => [
                    'data-icon' => 'fas fa-rectangle-ad',
                ],
            ],
            [
                'value' => 'fas fa-rectangle-list',
                'label' => 'Rectangle list',
                'attributes' => [
                    'data-icon' => 'fas fa-rectangle-list',
                ],
            ],
            [
                'value' => 'far fa-rectangle-list',
                'label' => 'Rectangle list',
                'attributes' => [
                    'data-icon' => 'far fa-rectangle-list',
                ],
            ],
            [
                'value' => 'fas fa-rectangle-xmark',
                'label' => 'Rectangle X Mark',
                'attributes' => [
                    'data-icon' => 'fas fa-rectangle-xmark',
                ],
            ],
            [
                'value' => 'far fa-rectangle-xmark',
                'label' => 'Rectangle X Mark',
                'attributes' => [
                    'data-icon' => 'far fa-rectangle-xmark',
                ],
            ],
            [
                'value' => 'fas fa-recycle',
                'label' => 'Recycle',
                'attributes' => [
                    'data-icon' => 'fas fa-recycle',
                ],
            ],
            [
                'value' => 'fab fa-red-river',
                'label' => 'red river',
                'attributes' => [
                    'data-icon' => 'fab fa-red-river',
                ],
            ],
            [
                'value' => 'fab fa-reddit',
                'label' => 'reddit Logo',
                'attributes' => [
                    'data-icon' => 'fab fa-reddit',
                ],
            ],
            [
                'value' => 'fab fa-reddit-alien',
                'label' => 'reddit Alien',
                'attributes' => [
                    'data-icon' => 'fab fa-reddit-alien',
                ],
            ],
            [
                'value' => 'fab fa-reddit-square',
                'label' => 'reddit Square',
                'attributes' => [
                    'data-icon' => 'fab fa-reddit-square',
                ],
            ],
            [
                'value' => 'fab fa-redhat',
                'label' => 'Redhat',
                'attributes' => [
                    'data-icon' => 'fab fa-redhat',
                ],
            ],
            [
                'value' => 'fas fa-registered',
                'label' => 'Registered Trademark',
                'attributes' => [
                    'data-icon' => 'fas fa-registered',
                ],
            ],
            [
                'value' => 'far fa-registered',
                'label' => 'Registered Trademark',
                'attributes' => [
                    'data-icon' => 'far fa-registered',
                ],
            ],
            [
                'value' => 'fab fa-renren',
                'label' => 'Renren',
                'attributes' => [
                    'data-icon' => 'fab fa-renren',
                ],
            ],
            [
                'value' => 'fas fa-repeat',
                'label' => 'Repeat',
                'attributes' => [
                    'data-icon' => 'fas fa-repeat',
                ],
            ],
            [
                'value' => 'fas fa-reply',
                'label' => 'Reply',
                'attributes' => [
                    'data-icon' => 'fas fa-reply',
                ],
            ],
            [
                'value' => 'fas fa-reply-all',
                'label' => 'reply-all',
                'attributes' => [
                    'data-icon' => 'fas fa-reply-all',
                ],
            ],
            [
                'value' => 'fab fa-replyd',
                'label' => 'replyd',
                'attributes' => [
                    'data-icon' => 'fab fa-replyd',
                ],
            ],
            [
                'value' => 'fas fa-republican',
                'label' => 'Republican',
                'attributes' => [
                    'data-icon' => 'fas fa-republican',
                ],
            ],
            [
                'value' => 'fab fa-researchgate',
                'label' => 'Researchgate',
                'attributes' => [
                    'data-icon' => 'fab fa-researchgate',
                ],
            ],
            [
                'value' => 'fab fa-resolving',
                'label' => 'Resolving',
                'attributes' => [
                    'data-icon' => 'fab fa-resolving',
                ],
            ],
            [
                'value' => 'fas fa-restroom',
                'label' => 'Restroom',
                'attributes' => [
                    'data-icon' => 'fas fa-restroom',
                ],
            ],
            [
                'value' => 'fas fa-retweet',
                'label' => 'Retweet',
                'attributes' => [
                    'data-icon' => 'fas fa-retweet',
                ],
            ],
            [
                'value' => 'fab fa-rev',
                'label' => 'Rev.io',
                'attributes' => [
                    'data-icon' => 'fab fa-rev',
                ],
            ],
            [
                'value' => 'fas fa-ribbon',
                'label' => 'Ribbon',
                'attributes' => [
                    'data-icon' => 'fas fa-ribbon',
                ],
            ],
            [
                'value' => 'fas fa-right-from-bracket',
                'label' => 'Right from bracket',
                'attributes' => [
                    'data-icon' => 'fas fa-right-from-bracket',
                ],
            ],
            [
                'value' => 'fas fa-right-left',
                'label' => 'Right left',
                'attributes' => [
                    'data-icon' => 'fas fa-right-left',
                ],
            ],
            [
                'value' => 'fas fa-right-long',
                'label' => 'Right long',
                'attributes' => [
                    'data-icon' => 'fas fa-right-long',
                ],
            ],
            [
                'value' => 'fas fa-right-to-bracket',
                'label' => 'Right to bracket',
                'attributes' => [
                    'data-icon' => 'fas fa-right-to-bracket',
                ],
            ],
            [
                'value' => 'fas fa-ring',
                'label' => 'Ring',
                'attributes' => [
                    'data-icon' => 'fas fa-ring',
                ],
            ],
            [
                'value' => 'fas fa-road',
                'label' => 'road',
                'attributes' => [
                    'data-icon' => 'fas fa-road',
                ],
            ],
            [
                'value' => 'fas fa-road-barrier',
                'label' => 'Road Barrier',
                'attributes' => [
                    'data-icon' => 'fas fa-road-barrier',
                ],
            ],
            [
                'value' => 'fas fa-road-bridge',
                'label' => 'Road Bridge',
                'attributes' => [
                    'data-icon' => 'fas fa-road-bridge',
                ],
            ],
            [
                'value' => 'fas fa-road-circle-check',
                'label' => 'Road Circle-check',
                'attributes' => [
                    'data-icon' => 'fas fa-road-circle-check',
                ],
            ],
            [
                'value' => 'fas fa-road-circle-exclamation',
                'label' => 'Road Circle-exclamation',
                'attributes' => [
                    'data-icon' => 'fas fa-road-circle-exclamation',
                ],
            ],
            [
                'value' => 'fas fa-road-circle-xmark',
                'label' => 'Road Circle-xmark',
                'attributes' => [
                    'data-icon' => 'fas fa-road-circle-xmark',
                ],
            ],
            [
                'value' => 'fas fa-road-lock',
                'label' => 'Road Lock',
                'attributes' => [
                    'data-icon' => 'fas fa-road-lock',
                ],
            ],
            [
                'value' => 'fas fa-road-spikes',
                'label' => 'Road Spikes',
                'attributes' => [
                    'data-icon' => 'fas fa-road-spikes',
                ],
            ],
            [
                'value' => 'fas fa-robot',
                'label' => 'Robot',
                'attributes' => [
                    'data-icon' => 'fas fa-robot',
                ],
            ],
            [
                'value' => 'fas fa-rocket',
                'label' => 'rocket',
                'attributes' => [
                    'data-icon' => 'fas fa-rocket',
                ],
            ],
            [
                'value' => 'fab fa-rocketchat',
                'label' => 'Rocket.Chat',
                'attributes' => [
                    'data-icon' => 'fab fa-rocketchat',
                ],
            ],
            [
                'value' => 'fab fa-rockrms',
                'label' => 'Rockrms',
                'attributes' => [
                    'data-icon' => 'fab fa-rockrms',
                ],
            ],
            [
                'value' => 'fas fa-rotate',
                'label' => 'Rotate',
                'attributes' => [
                    'data-icon' => 'fas fa-rotate',
                ],
            ],
            [
                'value' => 'fas fa-rotate-left',
                'label' => 'Rotate Left',
                'attributes' => [
                    'data-icon' => 'fas fa-rotate-left',
                ],
            ],
            [
                'value' => 'fas fa-rotate-right',
                'label' => 'Rotate Right',
                'attributes' => [
                    'data-icon' => 'fas fa-rotate-right',
                ],
            ],
            [
                'value' => 'fas fa-route',
                'label' => 'Route',
                'attributes' => [
                    'data-icon' => 'fas fa-route',
                ],
            ],
            [
                'value' => 'fas fa-rss',
                'label' => 'rss',
                'attributes' => [
                    'data-icon' => 'fas fa-rss',
                ],
            ],
            [
                'value' => 'fas fa-ruble-sign',
                'label' => 'Ruble Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-ruble-sign',
                ],
            ],
            [
                'value' => 'fas fa-rug',
                'label' => 'Rug',
                'attributes' => [
                    'data-icon' => 'fas fa-rug',
                ],
            ],
            [
                'value' => 'fas fa-ruler',
                'label' => 'Ruler',
                'attributes' => [
                    'data-icon' => 'fas fa-ruler',
                ],
            ],
            [
                'value' => 'fas fa-ruler-combined',
                'label' => 'Ruler Combined',
                'attributes' => [
                    'data-icon' => 'fas fa-ruler-combined',
                ],
            ],
            [
                'value' => 'fas fa-ruler-horizontal',
                'label' => 'Ruler Horizontal',
                'attributes' => [
                    'data-icon' => 'fas fa-ruler-horizontal',
                ],
            ],
            [
                'value' => 'fas fa-ruler-vertical',
                'label' => 'Ruler Vertical',
                'attributes' => [
                    'data-icon' => 'fas fa-ruler-vertical',
                ],
            ],
            [
                'value' => 'fas fa-rupee-sign',
                'label' => 'Indian Rupee Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-rupee-sign',
                ],
            ],
            [
                'value' => 'fas fa-rupiah-sign',
                'label' => 'Rupiah Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-rupiah-sign',
                ],
            ],
            [
                'value' => 'fab fa-rust',
                'label' => 'Rust',
                'attributes' => [
                    'data-icon' => 'fab fa-rust',
                ],
            ],
            [
                'value' => 'fas fa-s',
                'label' => 'S',
                'attributes' => [
                    'data-icon' => 'fas fa-s',
                ],
            ],
            [
                'value' => 'fas fa-sack-dollar',
                'label' => 'Sack of Money',
                'attributes' => [
                    'data-icon' => 'fas fa-sack-dollar',
                ],
            ],
            [
                'value' => 'fas fa-sack-xmark',
                'label' => 'Sack Xmark',
                'attributes' => [
                    'data-icon' => 'fas fa-sack-xmark',
                ],
            ],
            [
                'value' => 'fab fa-safari',
                'label' => 'Safari',
                'attributes' => [
                    'data-icon' => 'fab fa-safari',
                ],
            ],
            [
                'value' => 'fas fa-sailboat',
                'label' => 'Sailboat',
                'attributes' => [
                    'data-icon' => 'fas fa-sailboat',
                ],
            ],
            [
                'value' => 'fab fa-salesforce',
                'label' => 'Salesforce',
                'attributes' => [
                    'data-icon' => 'fab fa-salesforce',
                ],
            ],
            [
                'value' => 'fab fa-sass',
                'label' => 'Sass',
                'attributes' => [
                    'data-icon' => 'fab fa-sass',
                ],
            ],
            [
                'value' => 'fas fa-satellite',
                'label' => 'Satellite',
                'attributes' => [
                    'data-icon' => 'fas fa-satellite',
                ],
            ],
            [
                'value' => 'fas fa-satellite-dish',
                'label' => 'Satellite Dish',
                'attributes' => [
                    'data-icon' => 'fas fa-satellite-dish',
                ],
            ],
            [
                'value' => 'fas fa-scale-balanced',
                'label' => 'Scale balanced',
                'attributes' => [
                    'data-icon' => 'fas fa-scale-balanced',
                ],
            ],
            [
                'value' => 'fas fa-scale-unbalanced',
                'label' => 'Scale unbalanced',
                'attributes' => [
                    'data-icon' => 'fas fa-scale-unbalanced',
                ],
            ],
            [
                'value' => 'fas fa-scale-unbalanced-flip',
                'label' => 'Scale unbalanced flip',
                'attributes' => [
                    'data-icon' => 'fas fa-scale-unbalanced-flip',
                ],
            ],
            [
                'value' => 'fab fa-schlix',
                'label' => 'SCHLIX',
                'attributes' => [
                    'data-icon' => 'fab fa-schlix',
                ],
            ],
            [
                'value' => 'fas fa-school',
                'label' => 'School',
                'attributes' => [
                    'data-icon' => 'fas fa-school',
                ],
            ],
            [
                'value' => 'fas fa-school-circle-check',
                'label' => 'School Circle-check',
                'attributes' => [
                    'data-icon' => 'fas fa-school-circle-check',
                ],
            ],
            [
                'value' => 'fas fa-school-circle-exclamation',
                'label' => 'School Circle-exclamation',
                'attributes' => [
                    'data-icon' => 'fas fa-school-circle-exclamation',
                ],
            ],
            [
                'value' => 'fas fa-school-circle-xmark',
                'label' => 'School Circle-xmark',
                'attributes' => [
                    'data-icon' => 'fas fa-school-circle-xmark',
                ],
            ],
            [
                'value' => 'fas fa-school-flag',
                'label' => 'School Flag',
                'attributes' => [
                    'data-icon' => 'fas fa-school-flag',
                ],
            ],
            [
                'value' => 'fas fa-school-lock',
                'label' => 'School Lock',
                'attributes' => [
                    'data-icon' => 'fas fa-school-lock',
                ],
            ],
            [
                'value' => 'fas fa-scissors',
                'label' => 'Scissors',
                'attributes' => [
                    'data-icon' => 'fas fa-scissors',
                ],
            ],
            [
                'value' => 'fab fa-screenpal',
                'label' => 'Screenpal',
                'attributes' => [
                    'data-icon' => 'fab fa-screenpal',
                ],
            ],
            [
                'value' => 'fas fa-screwdriver',
                'label' => 'Screwdriver',
                'attributes' => [
                    'data-icon' => 'fas fa-screwdriver',
                ],
            ],
            [
                'value' => 'fas fa-screwdriver-wrench',
                'label' => 'Screwdriver wrench',
                'attributes' => [
                    'data-icon' => 'fas fa-screwdriver-wrench',
                ],
            ],
            [
                'value' => 'fab fa-scribd',
                'label' => 'Scribd',
                'attributes' => [
                    'data-icon' => 'fab fa-scribd',
                ],
            ],
            [
                'value' => 'fas fa-scroll',
                'label' => 'Scroll',
                'attributes' => [
                    'data-icon' => 'fas fa-scroll',
                ],
            ],
            [
                'value' => 'fas fa-scroll-torah',
                'label' => 'Scroll torah',
                'attributes' => [
                    'data-icon' => 'fas fa-scroll-torah',
                ],
            ],
            [
                'value' => 'fas fa-sd-card',
                'label' => 'Sd Card',
                'attributes' => [
                    'data-icon' => 'fas fa-sd-card',
                ],
            ],
            [
                'value' => 'fab fa-searchengin',
                'label' => 'Searchengin',
                'attributes' => [
                    'data-icon' => 'fab fa-searchengin',
                ],
            ],
            [
                'value' => 'fas fa-section',
                'label' => 'Section',
                'attributes' => [
                    'data-icon' => 'fas fa-section',
                ],
            ],
            [
                'value' => 'fas fa-seedling',
                'label' => 'Seedling',
                'attributes' => [
                    'data-icon' => 'fas fa-seedling',
                ],
            ],
            [
                'value' => 'fab fa-sellcast',
                'label' => 'Sellcast',
                'attributes' => [
                    'data-icon' => 'fab fa-sellcast',
                ],
            ],
            [
                'value' => 'fab fa-sellsy',
                'label' => 'Sellsy',
                'attributes' => [
                    'data-icon' => 'fab fa-sellsy',
                ],
            ],
            [
                'value' => 'fas fa-server',
                'label' => 'Server',
                'attributes' => [
                    'data-icon' => 'fas fa-server',
                ],
            ],
            [
                'value' => 'fab fa-servicestack',
                'label' => 'Servicestack',
                'attributes' => [
                    'data-icon' => 'fab fa-servicestack',
                ],
            ],
            [
                'value' => 'fas fa-shapes',
                'label' => 'Shapes',
                'attributes' => [
                    'data-icon' => 'fas fa-shapes',
                ],
            ],
            [
                'value' => 'fas fa-share',
                'label' => 'Share',
                'attributes' => [
                    'data-icon' => 'fas fa-share',
                ],
            ],
            [
                'value' => 'fas fa-share-from-square',
                'label' => 'Share from square',
                'attributes' => [
                    'data-icon' => 'fas fa-share-from-square',
                ],
            ],
            [
                'value' => 'far fa-share-from-square',
                'label' => 'Share from square',
                'attributes' => [
                    'data-icon' => 'far fa-share-from-square',
                ],
            ],
            [
                'value' => 'fas fa-share-nodes',
                'label' => 'Share nodes',
                'attributes' => [
                    'data-icon' => 'fas fa-share-nodes',
                ],
            ],
            [
                'value' => 'fas fa-sheet-plastic',
                'label' => 'Sheet Plastic',
                'attributes' => [
                    'data-icon' => 'fas fa-sheet-plastic',
                ],
            ],
            [
                'value' => 'fas fa-shekel-sign',
                'label' => 'Shekel Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-shekel-sign',
                ],
            ],
            [
                'value' => 'fas fa-shield',
                'label' => 'shield',
                'attributes' => [
                    'data-icon' => 'fas fa-shield',
                ],
            ],
            [
                'value' => 'fas fa-shield-cat',
                'label' => 'Shield Cat',
                'attributes' => [
                    'data-icon' => 'fas fa-shield-cat',
                ],
            ],
            [
                'value' => 'fas fa-shield-dog',
                'label' => 'Shield Dog',
                'attributes' => [
                    'data-icon' => 'fas fa-shield-dog',
                ],
            ],
            [
                'value' => 'fas fa-shield-halved',
                'label' => 'Shield Halved',
                'attributes' => [
                    'data-icon' => 'fas fa-shield-halved',
                ],
            ],
            [
                'value' => 'fas fa-shield-heart',
                'label' => 'Shield Heart',
                'attributes' => [
                    'data-icon' => 'fas fa-shield-heart',
                ],
            ],
            [
                'value' => 'fas fa-shield-virus',
                'label' => 'Shield Virus',
                'attributes' => [
                    'data-icon' => 'fas fa-shield-virus',
                ],
            ],
            [
                'value' => 'fas fa-ship',
                'label' => 'Ship',
                'attributes' => [
                    'data-icon' => 'fas fa-ship',
                ],
            ],
            [
                'value' => 'fas fa-shirt',
                'label' => 'T-Shirt',
                'attributes' => [
                    'data-icon' => 'fas fa-shirt',
                ],
            ],
            [
                'value' => 'fab fa-shirtsinbulk',
                'label' => 'Shirts in Bulk',
                'attributes' => [
                    'data-icon' => 'fab fa-shirtsinbulk',
                ],
            ],
            [
                'value' => 'fas fa-shoe-prints',
                'label' => 'Shoe Prints',
                'attributes' => [
                    'data-icon' => 'fas fa-shoe-prints',
                ],
            ],
            [
                'value' => 'fas fa-shop',
                'label' => 'Shop',
                'attributes' => [
                    'data-icon' => 'fas fa-shop',
                ],
            ],
            [
                'value' => 'fas fa-shop-lock',
                'label' => 'Shop Lock',
                'attributes' => [
                    'data-icon' => 'fas fa-shop-lock',
                ],
            ],
            [
                'value' => 'fas fa-shop-slash',
                'label' => 'Shop slash',
                'attributes' => [
                    'data-icon' => 'fas fa-shop-slash',
                ],
            ],
            [
                'value' => 'fab fa-shopify',
                'label' => 'Shopify',
                'attributes' => [
                    'data-icon' => 'fab fa-shopify',
                ],
            ],
            [
                'value' => 'fab fa-shopware',
                'label' => 'Shopware',
                'attributes' => [
                    'data-icon' => 'fab fa-shopware',
                ],
            ],
            [
                'value' => 'fas fa-shower',
                'label' => 'Shower',
                'attributes' => [
                    'data-icon' => 'fas fa-shower',
                ],
            ],
            [
                'value' => 'fas fa-shrimp',
                'label' => 'Shrimp',
                'attributes' => [
                    'data-icon' => 'fas fa-shrimp',
                ],
            ],
            [
                'value' => 'fas fa-shuffle',
                'label' => 'Shuffle',
                'attributes' => [
                    'data-icon' => 'fas fa-shuffle',
                ],
            ],
            [
                'value' => 'fas fa-shuttle-space',
                'label' => 'Shuttle space',
                'attributes' => [
                    'data-icon' => 'fas fa-shuttle-space',
                ],
            ],
            [
                'value' => 'fas fa-sign-hanging',
                'label' => 'Sign hanging',
                'attributes' => [
                    'data-icon' => 'fas fa-sign-hanging',
                ],
            ],
            [
                'value' => 'fas fa-signal',
                'label' => 'signal',
                'attributes' => [
                    'data-icon' => 'fas fa-signal',
                ],
            ],
            [
                'value' => 'fas fa-signature',
                'label' => 'Signature',
                'attributes' => [
                    'data-icon' => 'fas fa-signature',
                ],
            ],
            [
                'value' => 'fas fa-signs-post',
                'label' => 'Signs post',
                'attributes' => [
                    'data-icon' => 'fas fa-signs-post',
                ],
            ],
            [
                'value' => 'fas fa-sim-card',
                'label' => 'SIM Card',
                'attributes' => [
                    'data-icon' => 'fas fa-sim-card',
                ],
            ],
            [
                'value' => 'fab fa-simplybuilt',
                'label' => 'SimplyBuilt',
                'attributes' => [
                    'data-icon' => 'fab fa-simplybuilt',
                ],
            ],
            [
                'value' => 'fas fa-sink',
                'label' => 'Sink',
                'attributes' => [
                    'data-icon' => 'fas fa-sink',
                ],
            ],
            [
                'value' => 'fab fa-sistrix',
                'label' => 'SISTRIX',
                'attributes' => [
                    'data-icon' => 'fab fa-sistrix',
                ],
            ],
            [
                'value' => 'fas fa-sitemap',
                'label' => 'Sitemap',
                'attributes' => [
                    'data-icon' => 'fas fa-sitemap',
                ],
            ],
            [
                'value' => 'fab fa-sith',
                'label' => 'Sith',
                'attributes' => [
                    'data-icon' => 'fab fa-sith',
                ],
            ],
            [
                'value' => 'fab fa-sitrox',
                'label' => 'Sitrox',
                'attributes' => [
                    'data-icon' => 'fab fa-sitrox',
                ],
            ],
            [
                'value' => 'fab fa-sketch',
                'label' => 'Sketch',
                'attributes' => [
                    'data-icon' => 'fab fa-sketch',
                ],
            ],
            [
                'value' => 'fas fa-skull',
                'label' => 'Skull',
                'attributes' => [
                    'data-icon' => 'fas fa-skull',
                ],
            ],
            [
                'value' => 'fas fa-skull-crossbones',
                'label' => 'Skull & Crossbones',
                'attributes' => [
                    'data-icon' => 'fas fa-skull-crossbones',
                ],
            ],
            [
                'value' => 'fab fa-skyatlas',
                'label' => 'skyatlas',
                'attributes' => [
                    'data-icon' => 'fab fa-skyatlas',
                ],
            ],
            [
                'value' => 'fab fa-skype',
                'label' => 'Skype',
                'attributes' => [
                    'data-icon' => 'fab fa-skype',
                ],
            ],
            [
                'value' => 'fab fa-slack',
                'label' => 'Slack Logo',
                'attributes' => [
                    'data-icon' => 'fab fa-slack',
                ],
            ],
            [
                'value' => 'fas fa-slash',
                'label' => 'Slash',
                'attributes' => [
                    'data-icon' => 'fas fa-slash',
                ],
            ],
            [
                'value' => 'fas fa-sleigh',
                'label' => 'Sleigh',
                'attributes' => [
                    'data-icon' => 'fas fa-sleigh',
                ],
            ],
            [
                'value' => 'fas fa-sliders',
                'label' => 'Sliders',
                'attributes' => [
                    'data-icon' => 'fas fa-sliders',
                ],
            ],
            [
                'value' => 'fab fa-slideshare',
                'label' => 'Slideshare',
                'attributes' => [
                    'data-icon' => 'fab fa-slideshare',
                ],
            ],
            [
                'value' => 'fas fa-smog',
                'label' => 'Smog',
                'attributes' => [
                    'data-icon' => 'fas fa-smog',
                ],
            ],
            [
                'value' => 'fas fa-smoking',
                'label' => 'Smoking',
                'attributes' => [
                    'data-icon' => 'fas fa-smoking',
                ],
            ],
            [
                'value' => 'fab fa-snapchat',
                'label' => 'Snapchat',
                'attributes' => [
                    'data-icon' => 'fab fa-snapchat',
                ],
            ],
            [
                'value' => 'fab fa-snapchat-square',
                'label' => 'Snapchat Square',
                'attributes' => [
                    'data-icon' => 'fab fa-snapchat-square',
                ],
            ],
            [
                'value' => 'fas fa-snowflake',
                'label' => 'Snowflake',
                'attributes' => [
                    'data-icon' => 'fas fa-snowflake',
                ],
            ],
            [
                'value' => 'far fa-snowflake',
                'label' => 'Snowflake',
                'attributes' => [
                    'data-icon' => 'far fa-snowflake',
                ],
            ],
            [
                'value' => 'fas fa-snowman',
                'label' => 'Snowman',
                'attributes' => [
                    'data-icon' => 'fas fa-snowman',
                ],
            ],
            [
                'value' => 'fas fa-snowplow',
                'label' => 'Snowplow',
                'attributes' => [
                    'data-icon' => 'fas fa-snowplow',
                ],
            ],
            [
                'value' => 'fas fa-soap',
                'label' => 'Soap',
                'attributes' => [
                    'data-icon' => 'fas fa-soap',
                ],
            ],
            [
                'value' => 'fas fa-socks',
                'label' => 'Socks',
                'attributes' => [
                    'data-icon' => 'fas fa-socks',
                ],
            ],
            [
                'value' => 'fas fa-solar-panel',
                'label' => 'Solar Panel',
                'attributes' => [
                    'data-icon' => 'fas fa-solar-panel',
                ],
            ],
            [
                'value' => 'fas fa-sort',
                'label' => 'Sort',
                'attributes' => [
                    'data-icon' => 'fas fa-sort',
                ],
            ],
            [
                'value' => 'fas fa-sort-down',
                'label' => 'Sort Down (Descending)',
                'attributes' => [
                    'data-icon' => 'fas fa-sort-down',
                ],
            ],
            [
                'value' => 'fas fa-sort-up',
                'label' => 'Sort Up (Ascending)',
                'attributes' => [
                    'data-icon' => 'fas fa-sort-up',
                ],
            ],
            [
                'value' => 'fab fa-soundcloud',
                'label' => 'SoundCloud',
                'attributes' => [
                    'data-icon' => 'fab fa-soundcloud',
                ],
            ],
            [
                'value' => 'fab fa-sourcetree',
                'label' => 'Sourcetree',
                'attributes' => [
                    'data-icon' => 'fab fa-sourcetree',
                ],
            ],
            [
                'value' => 'fas fa-spa',
                'label' => 'Spa',
                'attributes' => [
                    'data-icon' => 'fas fa-spa',
                ],
            ],
            [
                'value' => 'fas fa-spaghetti-monster-flying',
                'label' => 'Spaghetti monster flying',
                'attributes' => [
                    'data-icon' => 'fas fa-spaghetti-monster-flying',
                ],
            ],
            [
                'value' => 'fab fa-speakap',
                'label' => 'Speakap',
                'attributes' => [
                    'data-icon' => 'fab fa-speakap',
                ],
            ],
            [
                'value' => 'fab fa-speaker-deck',
                'label' => 'Speaker Deck',
                'attributes' => [
                    'data-icon' => 'fab fa-speaker-deck',
                ],
            ],
            [
                'value' => 'fas fa-spell-check',
                'label' => 'Spell Check',
                'attributes' => [
                    'data-icon' => 'fas fa-spell-check',
                ],
            ],
            [
                'value' => 'fas fa-spider',
                'label' => 'Spider',
                'attributes' => [
                    'data-icon' => 'fas fa-spider',
                ],
            ],
            [
                'value' => 'fas fa-spinner',
                'label' => 'Spinner',
                'attributes' => [
                    'data-icon' => 'fas fa-spinner',
                ],
            ],
            [
                'value' => 'fas fa-splotch',
                'label' => 'Splotch',
                'attributes' => [
                    'data-icon' => 'fas fa-splotch',
                ],
            ],
            [
                'value' => 'fas fa-spoon',
                'label' => 'Spoon',
                'attributes' => [
                    'data-icon' => 'fas fa-spoon',
                ],
            ],
            [
                'value' => 'fab fa-spotify',
                'label' => 'Spotify',
                'attributes' => [
                    'data-icon' => 'fab fa-spotify',
                ],
            ],
            [
                'value' => 'fas fa-spray-can',
                'label' => 'Spray Can',
                'attributes' => [
                    'data-icon' => 'fas fa-spray-can',
                ],
            ],
            [
                'value' => 'fas fa-spray-can-sparkles',
                'label' => 'Spray Can Sparkles',
                'attributes' => [
                    'data-icon' => 'fas fa-spray-can-sparkles',
                ],
            ],
            [
                'value' => 'fas fa-square',
                'label' => 'Square',
                'attributes' => [
                    'data-icon' => 'fas fa-square',
                ],
            ],
            [
                'value' => 'far fa-square',
                'label' => 'Square',
                'attributes' => [
                    'data-icon' => 'far fa-square',
                ],
            ],
            [
                'value' => 'fas fa-square-arrow-up-right',
                'label' => 'Square arrow up right',
                'attributes' => [
                    'data-icon' => 'fas fa-square-arrow-up-right',
                ],
            ],
            [
                'value' => 'fas fa-square-caret-down',
                'label' => 'Square caret down',
                'attributes' => [
                    'data-icon' => 'fas fa-square-caret-down',
                ],
            ],
            [
                'value' => 'far fa-square-caret-down',
                'label' => 'Square caret down',
                'attributes' => [
                    'data-icon' => 'far fa-square-caret-down',
                ],
            ],
            [
                'value' => 'fas fa-square-caret-left',
                'label' => 'Square caret left',
                'attributes' => [
                    'data-icon' => 'fas fa-square-caret-left',
                ],
            ],
            [
                'value' => 'far fa-square-caret-left',
                'label' => 'Square caret left',
                'attributes' => [
                    'data-icon' => 'far fa-square-caret-left',
                ],
            ],
            [
                'value' => 'fas fa-square-caret-right',
                'label' => 'Square caret right',
                'attributes' => [
                    'data-icon' => 'fas fa-square-caret-right',
                ],
            ],
            [
                'value' => 'far fa-square-caret-right',
                'label' => 'Square caret right',
                'attributes' => [
                    'data-icon' => 'far fa-square-caret-right',
                ],
            ],
            [
                'value' => 'fas fa-square-caret-up',
                'label' => 'Square caret up',
                'attributes' => [
                    'data-icon' => 'fas fa-square-caret-up',
                ],
            ],
            [
                'value' => 'far fa-square-caret-up',
                'label' => 'Square caret up',
                'attributes' => [
                    'data-icon' => 'far fa-square-caret-up',
                ],
            ],
            [
                'value' => 'fas fa-square-check',
                'label' => 'Square check',
                'attributes' => [
                    'data-icon' => 'fas fa-square-check',
                ],
            ],
            [
                'value' => 'far fa-square-check',
                'label' => 'Square check',
                'attributes' => [
                    'data-icon' => 'far fa-square-check',
                ],
            ],
            [
                'value' => 'fas fa-square-envelope',
                'label' => 'Square envelope',
                'attributes' => [
                    'data-icon' => 'fas fa-square-envelope',
                ],
            ],
            [
                'value' => 'fab fa-square-font-awesome',
                'label' => 'Font Awesome in Square',
                'attributes' => [
                    'data-icon' => 'fab fa-square-font-awesome',
                ],
            ],
            [
                'value' => 'fab fa-square-font-awesome-stroke',
                'label' => 'Font Awesome in Square with Stroke Outline',
                'attributes' => [
                    'data-icon' => 'fab fa-square-font-awesome-stroke',
                ],
            ],
            [
                'value' => 'fas fa-square-full',
                'label' => 'Square Full',
                'attributes' => [
                    'data-icon' => 'fas fa-square-full',
                ],
            ],
            [
                'value' => 'far fa-square-full',
                'label' => 'Square Full',
                'attributes' => [
                    'data-icon' => 'far fa-square-full',
                ],
            ],
            [
                'value' => 'fas fa-square-h',
                'label' => 'Square h',
                'attributes' => [
                    'data-icon' => 'fas fa-square-h',
                ],
            ],
            [
                'value' => 'fas fa-square-minus',
                'label' => 'Square minus',
                'attributes' => [
                    'data-icon' => 'fas fa-square-minus',
                ],
            ],
            [
                'value' => 'far fa-square-minus',
                'label' => 'Square minus',
                'attributes' => [
                    'data-icon' => 'far fa-square-minus',
                ],
            ],
            [
                'value' => 'fas fa-square-nfi',
                'label' => 'Square Nfi',
                'attributes' => [
                    'data-icon' => 'fas fa-square-nfi',
                ],
            ],
            [
                'value' => 'fas fa-square-parking',
                'label' => 'Square parking',
                'attributes' => [
                    'data-icon' => 'fas fa-square-parking',
                ],
            ],
            [
                'value' => 'fas fa-square-pen',
                'label' => 'Square pen',
                'attributes' => [
                    'data-icon' => 'fas fa-square-pen',
                ],
            ],
            [
                'value' => 'fas fa-square-person-confined',
                'label' => 'Square Person-confined',
                'attributes' => [
                    'data-icon' => 'fas fa-square-person-confined',
                ],
            ],
            [
                'value' => 'fas fa-square-phone',
                'label' => 'Square phone',
                'attributes' => [
                    'data-icon' => 'fas fa-square-phone',
                ],
            ],
            [
                'value' => 'fas fa-square-phone-flip',
                'label' => 'Square phone flip',
                'attributes' => [
                    'data-icon' => 'fas fa-square-phone-flip',
                ],
            ],
            [
                'value' => 'fas fa-square-plus',
                'label' => 'Square plus',
                'attributes' => [
                    'data-icon' => 'fas fa-square-plus',
                ],
            ],
            [
                'value' => 'far fa-square-plus',
                'label' => 'Square plus',
                'attributes' => [
                    'data-icon' => 'far fa-square-plus',
                ],
            ],
            [
                'value' => 'fas fa-square-poll-horizontal',
                'label' => 'Square poll horizontal',
                'attributes' => [
                    'data-icon' => 'fas fa-square-poll-horizontal',
                ],
            ],
            [
                'value' => 'fas fa-square-poll-vertical',
                'label' => 'Square poll vertical',
                'attributes' => [
                    'data-icon' => 'fas fa-square-poll-vertical',
                ],
            ],
            [
                'value' => 'fas fa-square-root-variable',
                'label' => 'Square root variable',
                'attributes' => [
                    'data-icon' => 'fas fa-square-root-variable',
                ],
            ],
            [
                'value' => 'fas fa-square-rss',
                'label' => 'Square rss',
                'attributes' => [
                    'data-icon' => 'fas fa-square-rss',
                ],
            ],
            [
                'value' => 'fas fa-square-share-nodes',
                'label' => 'Square share nodes',
                'attributes' => [
                    'data-icon' => 'fas fa-square-share-nodes',
                ],
            ],
            [
                'value' => 'fas fa-square-up-right',
                'label' => 'Square up right',
                'attributes' => [
                    'data-icon' => 'fas fa-square-up-right',
                ],
            ],
            [
                'value' => 'fas fa-square-virus',
                'label' => 'Square Virus',
                'attributes' => [
                    'data-icon' => 'fas fa-square-virus',
                ],
            ],
            [
                'value' => 'fas fa-square-xmark',
                'label' => 'Square X Mark',
                'attributes' => [
                    'data-icon' => 'fas fa-square-xmark',
                ],
            ],
            [
                'value' => 'fab fa-squarespace',
                'label' => 'Squarespace',
                'attributes' => [
                    'data-icon' => 'fab fa-squarespace',
                ],
            ],
            [
                'value' => 'fab fa-stack-exchange',
                'label' => 'Stack Exchange',
                'attributes' => [
                    'data-icon' => 'fab fa-stack-exchange',
                ],
            ],
            [
                'value' => 'fab fa-stack-overflow',
                'label' => 'Stack Overflow',
                'attributes' => [
                    'data-icon' => 'fab fa-stack-overflow',
                ],
            ],
            [
                'value' => 'fab fa-stackpath',
                'label' => 'Stackpath',
                'attributes' => [
                    'data-icon' => 'fab fa-stackpath',
                ],
            ],
            [
                'value' => 'fas fa-staff-aesculapius',
                'label' => 'Staff Aesculapius',
                'attributes' => [
                    'data-icon' => 'fas fa-staff-aesculapius',
                ],
            ],
            [
                'value' => 'fas fa-stairs',
                'label' => 'Stairs',
                'attributes' => [
                    'data-icon' => 'fas fa-stairs',
                ],
            ],
            [
                'value' => 'fas fa-stamp',
                'label' => 'Stamp',
                'attributes' => [
                    'data-icon' => 'fas fa-stamp',
                ],
            ],
            [
                'value' => 'fas fa-star',
                'label' => 'Star',
                'attributes' => [
                    'data-icon' => 'fas fa-star',
                ],
            ],
            [
                'value' => 'far fa-star',
                'label' => 'Star',
                'attributes' => [
                    'data-icon' => 'far fa-star',
                ],
            ],
            [
                'value' => 'fas fa-star-and-crescent',
                'label' => 'Star and Crescent',
                'attributes' => [
                    'data-icon' => 'fas fa-star-and-crescent',
                ],
            ],
            [
                'value' => 'fas fa-star-half',
                'label' => 'star-half',
                'attributes' => [
                    'data-icon' => 'fas fa-star-half',
                ],
            ],
            [
                'value' => 'far fa-star-half',
                'label' => 'star-half',
                'attributes' => [
                    'data-icon' => 'far fa-star-half',
                ],
            ],
            [
                'value' => 'fas fa-star-half-stroke',
                'label' => 'Star half stroke',
                'attributes' => [
                    'data-icon' => 'fas fa-star-half-stroke',
                ],
            ],
            [
                'value' => 'far fa-star-half-stroke',
                'label' => 'Star half stroke',
                'attributes' => [
                    'data-icon' => 'far fa-star-half-stroke',
                ],
            ],
            [
                'value' => 'fas fa-star-of-david',
                'label' => 'Star of David',
                'attributes' => [
                    'data-icon' => 'fas fa-star-of-david',
                ],
            ],
            [
                'value' => 'fas fa-star-of-life',
                'label' => 'Star of Life',
                'attributes' => [
                    'data-icon' => 'fas fa-star-of-life',
                ],
            ],
            [
                'value' => 'fab fa-staylinked',
                'label' => 'StayLinked',
                'attributes' => [
                    'data-icon' => 'fab fa-staylinked',
                ],
            ],
            [
                'value' => 'fab fa-steam',
                'label' => 'Steam',
                'attributes' => [
                    'data-icon' => 'fab fa-steam',
                ],
            ],
            [
                'value' => 'fab fa-steam-square',
                'label' => 'Steam Square',
                'attributes' => [
                    'data-icon' => 'fab fa-steam-square',
                ],
            ],
            [
                'value' => 'fab fa-steam-symbol',
                'label' => 'Steam Symbol',
                'attributes' => [
                    'data-icon' => 'fab fa-steam-symbol',
                ],
            ],
            [
                'value' => 'fas fa-sterling-sign',
                'label' => 'Sterling sign',
                'attributes' => [
                    'data-icon' => 'fas fa-sterling-sign',
                ],
            ],
            [
                'value' => 'fas fa-stethoscope',
                'label' => 'Stethoscope',
                'attributes' => [
                    'data-icon' => 'fas fa-stethoscope',
                ],
            ],
            [
                'value' => 'fab fa-sticker-mule',
                'label' => 'Sticker Mule',
                'attributes' => [
                    'data-icon' => 'fab fa-sticker-mule',
                ],
            ],
            [
                'value' => 'fas fa-stop',
                'label' => 'stop',
                'attributes' => [
                    'data-icon' => 'fas fa-stop',
                ],
            ],
            [
                'value' => 'fas fa-stopwatch',
                'label' => 'Stopwatch',
                'attributes' => [
                    'data-icon' => 'fas fa-stopwatch',
                ],
            ],
            [
                'value' => 'fas fa-stopwatch-20',
                'label' => 'Stopwatch 20',
                'attributes' => [
                    'data-icon' => 'fas fa-stopwatch-20',
                ],
            ],
            [
                'value' => 'fas fa-store',
                'label' => 'Store',
                'attributes' => [
                    'data-icon' => 'fas fa-store',
                ],
            ],
            [
                'value' => 'fas fa-store-slash',
                'label' => 'Store Slash',
                'attributes' => [
                    'data-icon' => 'fas fa-store-slash',
                ],
            ],
            [
                'value' => 'fab fa-strava',
                'label' => 'Strava',
                'attributes' => [
                    'data-icon' => 'fab fa-strava',
                ],
            ],
            [
                'value' => 'fas fa-street-view',
                'label' => 'Street View',
                'attributes' => [
                    'data-icon' => 'fas fa-street-view',
                ],
            ],
            [
                'value' => 'fas fa-strikethrough',
                'label' => 'Strikethrough',
                'attributes' => [
                    'data-icon' => 'fas fa-strikethrough',
                ],
            ],
            [
                'value' => 'fab fa-stripe',
                'label' => 'Stripe',
                'attributes' => [
                    'data-icon' => 'fab fa-stripe',
                ],
            ],
            [
                'value' => 'fab fa-stripe-s',
                'label' => 'Stripe S',
                'attributes' => [
                    'data-icon' => 'fab fa-stripe-s',
                ],
            ],
            [
                'value' => 'fas fa-stroopwafel',
                'label' => 'Stroopwafel',
                'attributes' => [
                    'data-icon' => 'fas fa-stroopwafel',
                ],
            ],
            [
                'value' => 'fab fa-studiovinari',
                'label' => 'Studio Vinari',
                'attributes' => [
                    'data-icon' => 'fab fa-studiovinari',
                ],
            ],
            [
                'value' => 'fab fa-stumbleupon',
                'label' => 'StumbleUpon Logo',
                'attributes' => [
                    'data-icon' => 'fab fa-stumbleupon',
                ],
            ],
            [
                'value' => 'fab fa-stumbleupon-circle',
                'label' => 'StumbleUpon Circle',
                'attributes' => [
                    'data-icon' => 'fab fa-stumbleupon-circle',
                ],
            ],
            [
                'value' => 'fas fa-subscript',
                'label' => 'subscript',
                'attributes' => [
                    'data-icon' => 'fas fa-subscript',
                ],
            ],
            [
                'value' => 'fas fa-suitcase',
                'label' => 'Suitcase',
                'attributes' => [
                    'data-icon' => 'fas fa-suitcase',
                ],
            ],
            [
                'value' => 'fas fa-suitcase-medical',
                'label' => 'Suitcase medical',
                'attributes' => [
                    'data-icon' => 'fas fa-suitcase-medical',
                ],
            ],
            [
                'value' => 'fas fa-suitcase-rolling',
                'label' => 'Suitcase Rolling',
                'attributes' => [
                    'data-icon' => 'fas fa-suitcase-rolling',
                ],
            ],
            [
                'value' => 'fas fa-sun',
                'label' => 'Sun',
                'attributes' => [
                    'data-icon' => 'fas fa-sun',
                ],
            ],
            [
                'value' => 'far fa-sun',
                'label' => 'Sun',
                'attributes' => [
                    'data-icon' => 'far fa-sun',
                ],
            ],
            [
                'value' => 'fas fa-sun-plant-wilt',
                'label' => 'Sun Plant-wilt',
                'attributes' => [
                    'data-icon' => 'fas fa-sun-plant-wilt',
                ],
            ],
            [
                'value' => 'fab fa-superpowers',
                'label' => 'Superpowers',
                'attributes' => [
                    'data-icon' => 'fab fa-superpowers',
                ],
            ],
            [
                'value' => 'fas fa-superscript',
                'label' => 'superscript',
                'attributes' => [
                    'data-icon' => 'fas fa-superscript',
                ],
            ],
            [
                'value' => 'fab fa-supple',
                'label' => 'Supple',
                'attributes' => [
                    'data-icon' => 'fab fa-supple',
                ],
            ],
            [
                'value' => 'fab fa-suse',
                'label' => 'Suse',
                'attributes' => [
                    'data-icon' => 'fab fa-suse',
                ],
            ],
            [
                'value' => 'fas fa-swatchbook',
                'label' => 'Swatchbook',
                'attributes' => [
                    'data-icon' => 'fas fa-swatchbook',
                ],
            ],
            [
                'value' => 'fab fa-swift',
                'label' => 'Swift',
                'attributes' => [
                    'data-icon' => 'fab fa-swift',
                ],
            ],
            [
                'value' => 'fab fa-symfony',
                'label' => 'Symfony',
                'attributes' => [
                    'data-icon' => 'fab fa-symfony',
                ],
            ],
            [
                'value' => 'fas fa-synagogue',
                'label' => 'Synagogue',
                'attributes' => [
                    'data-icon' => 'fas fa-synagogue',
                ],
            ],
            [
                'value' => 'fas fa-syringe',
                'label' => 'Syringe',
                'attributes' => [
                    'data-icon' => 'fas fa-syringe',
                ],
            ],
            [
                'value' => 'fas fa-t',
                'label' => 'T',
                'attributes' => [
                    'data-icon' => 'fas fa-t',
                ],
            ],
            [
                'value' => 'fas fa-table',
                'label' => 'table',
                'attributes' => [
                    'data-icon' => 'fas fa-table',
                ],
            ],
            [
                'value' => 'fas fa-table-cells',
                'label' => 'Table cells',
                'attributes' => [
                    'data-icon' => 'fas fa-table-cells',
                ],
            ],
            [
                'value' => 'fas fa-table-cells-large',
                'label' => 'Table cells large',
                'attributes' => [
                    'data-icon' => 'fas fa-table-cells-large',
                ],
            ],
            [
                'value' => 'fas fa-table-columns',
                'label' => 'Table columns',
                'attributes' => [
                    'data-icon' => 'fas fa-table-columns',
                ],
            ],
            [
                'value' => 'fas fa-table-list',
                'label' => 'Table list',
                'attributes' => [
                    'data-icon' => 'fas fa-table-list',
                ],
            ],
            [
                'value' => 'fas fa-table-tennis-paddle-ball',
                'label' => 'Table tennis paddle ball',
                'attributes' => [
                    'data-icon' => 'fas fa-table-tennis-paddle-ball',
                ],
            ],
            [
                'value' => 'fas fa-tablet',
                'label' => 'Tablet',
                'attributes' => [
                    'data-icon' => 'fas fa-tablet',
                ],
            ],
            [
                'value' => 'fas fa-tablet-button',
                'label' => 'Tablet button',
                'attributes' => [
                    'data-icon' => 'fas fa-tablet-button',
                ],
            ],
            [
                'value' => 'fas fa-tablet-screen-button',
                'label' => 'Tablet screen button',
                'attributes' => [
                    'data-icon' => 'fas fa-tablet-screen-button',
                ],
            ],
            [
                'value' => 'fas fa-tablets',
                'label' => 'Tablets',
                'attributes' => [
                    'data-icon' => 'fas fa-tablets',
                ],
            ],
            [
                'value' => 'fas fa-tachograph-digital',
                'label' => 'Tachograph digital',
                'attributes' => [
                    'data-icon' => 'fas fa-tachograph-digital',
                ],
            ],
            [
                'value' => 'fas fa-tag',
                'label' => 'tag',
                'attributes' => [
                    'data-icon' => 'fas fa-tag',
                ],
            ],
            [
                'value' => 'fas fa-tags',
                'label' => 'tags',
                'attributes' => [
                    'data-icon' => 'fas fa-tags',
                ],
            ],
            [
                'value' => 'fas fa-tape',
                'label' => 'Tape',
                'attributes' => [
                    'data-icon' => 'fas fa-tape',
                ],
            ],
            [
                'value' => 'fas fa-tarp',
                'label' => 'Tarp',
                'attributes' => [
                    'data-icon' => 'fas fa-tarp',
                ],
            ],
            [
                'value' => 'fas fa-tarp-droplet',
                'label' => 'Tarp Droplet',
                'attributes' => [
                    'data-icon' => 'fas fa-tarp-droplet',
                ],
            ],
            [
                'value' => 'fas fa-taxi',
                'label' => 'Taxi',
                'attributes' => [
                    'data-icon' => 'fas fa-taxi',
                ],
            ],
            [
                'value' => 'fab fa-teamspeak',
                'label' => 'TeamSpeak',
                'attributes' => [
                    'data-icon' => 'fab fa-teamspeak',
                ],
            ],
            [
                'value' => 'fas fa-teeth',
                'label' => 'Teeth',
                'attributes' => [
                    'data-icon' => 'fas fa-teeth',
                ],
            ],
            [
                'value' => 'fas fa-teeth-open',
                'label' => 'Teeth Open',
                'attributes' => [
                    'data-icon' => 'fas fa-teeth-open',
                ],
            ],
            [
                'value' => 'fab fa-telegram',
                'label' => 'Telegram',
                'attributes' => [
                    'data-icon' => 'fab fa-telegram',
                ],
            ],
            [
                'value' => 'fas fa-temperature-arrow-down',
                'label' => 'Temperature arrow down',
                'attributes' => [
                    'data-icon' => 'fas fa-temperature-arrow-down',
                ],
            ],
            [
                'value' => 'fas fa-temperature-arrow-up',
                'label' => 'Temperature arrow up',
                'attributes' => [
                    'data-icon' => 'fas fa-temperature-arrow-up',
                ],
            ],
            [
                'value' => 'fas fa-temperature-empty',
                'label' => 'Temperature empty',
                'attributes' => [
                    'data-icon' => 'fas fa-temperature-empty',
                ],
            ],
            [
                'value' => 'fas fa-temperature-full',
                'label' => 'Temperature full',
                'attributes' => [
                    'data-icon' => 'fas fa-temperature-full',
                ],
            ],
            [
                'value' => 'fas fa-temperature-half',
                'label' => 'Temperature half',
                'attributes' => [
                    'data-icon' => 'fas fa-temperature-half',
                ],
            ],
            [
                'value' => 'fas fa-temperature-high',
                'label' => 'High Temperature',
                'attributes' => [
                    'data-icon' => 'fas fa-temperature-high',
                ],
            ],
            [
                'value' => 'fas fa-temperature-low',
                'label' => 'Low Temperature',
                'attributes' => [
                    'data-icon' => 'fas fa-temperature-low',
                ],
            ],
            [
                'value' => 'fas fa-temperature-quarter',
                'label' => 'Temperature quarter',
                'attributes' => [
                    'data-icon' => 'fas fa-temperature-quarter',
                ],
            ],
            [
                'value' => 'fas fa-temperature-three-quarters',
                'label' => 'Temperature three quarters',
                'attributes' => [
                    'data-icon' => 'fas fa-temperature-three-quarters',
                ],
            ],
            [
                'value' => 'fab fa-tencent-weibo',
                'label' => 'Tencent Weibo',
                'attributes' => [
                    'data-icon' => 'fab fa-tencent-weibo',
                ],
            ],
            [
                'value' => 'fas fa-tenge-sign',
                'label' => 'Tenge sign',
                'attributes' => [
                    'data-icon' => 'fas fa-tenge-sign',
                ],
            ],
            [
                'value' => 'fas fa-tent',
                'label' => 'Tent',
                'attributes' => [
                    'data-icon' => 'fas fa-tent',
                ],
            ],
            [
                'value' => 'fas fa-tent-arrow-down-to-line',
                'label' => 'Tent Arrow-down-to-line',
                'attributes' => [
                    'data-icon' => 'fas fa-tent-arrow-down-to-line',
                ],
            ],
            [
                'value' => 'fas fa-tent-arrow-left-right',
                'label' => 'Tent Arrow-left-right',
                'attributes' => [
                    'data-icon' => 'fas fa-tent-arrow-left-right',
                ],
            ],
            [
                'value' => 'fas fa-tent-arrow-turn-left',
                'label' => 'Tent Arrow-turn-left',
                'attributes' => [
                    'data-icon' => 'fas fa-tent-arrow-turn-left',
                ],
            ],
            [
                'value' => 'fas fa-tent-arrows-down',
                'label' => 'Tent Arrows-down',
                'attributes' => [
                    'data-icon' => 'fas fa-tent-arrows-down',
                ],
            ],
            [
                'value' => 'fas fa-tents',
                'label' => 'Tents',
                'attributes' => [
                    'data-icon' => 'fas fa-tents',
                ],
            ],
            [
                'value' => 'fas fa-terminal',
                'label' => 'Terminal',
                'attributes' => [
                    'data-icon' => 'fas fa-terminal',
                ],
            ],
            [
                'value' => 'fas fa-text-height',
                'label' => 'text-height',
                'attributes' => [
                    'data-icon' => 'fas fa-text-height',
                ],
            ],
            [
                'value' => 'fas fa-text-slash',
                'label' => 'Text slash',
                'attributes' => [
                    'data-icon' => 'fas fa-text-slash',
                ],
            ],
            [
                'value' => 'fas fa-text-width',
                'label' => 'Text Width',
                'attributes' => [
                    'data-icon' => 'fas fa-text-width',
                ],
            ],
            [
                'value' => 'fab fa-the-red-yeti',
                'label' => 'The Red Yeti',
                'attributes' => [
                    'data-icon' => 'fab fa-the-red-yeti',
                ],
            ],
            [
                'value' => 'fab fa-themeco',
                'label' => 'Themeco',
                'attributes' => [
                    'data-icon' => 'fab fa-themeco',
                ],
            ],
            [
                'value' => 'fab fa-themeisle',
                'label' => 'ThemeIsle',
                'attributes' => [
                    'data-icon' => 'fab fa-themeisle',
                ],
            ],
            [
                'value' => 'fas fa-thermometer',
                'label' => 'Thermometer',
                'attributes' => [
                    'data-icon' => 'fas fa-thermometer',
                ],
            ],
            [
                'value' => 'fab fa-think-peaks',
                'label' => 'Think Peaks',
                'attributes' => [
                    'data-icon' => 'fab fa-think-peaks',
                ],
            ],
            [
                'value' => 'fas fa-thumbs-down',
                'label' => 'thumbs-down',
                'attributes' => [
                    'data-icon' => 'fas fa-thumbs-down',
                ],
            ],
            [
                'value' => 'far fa-thumbs-down',
                'label' => 'thumbs-down',
                'attributes' => [
                    'data-icon' => 'far fa-thumbs-down',
                ],
            ],
            [
                'value' => 'fas fa-thumbs-up',
                'label' => 'thumbs-up',
                'attributes' => [
                    'data-icon' => 'fas fa-thumbs-up',
                ],
            ],
            [
                'value' => 'far fa-thumbs-up',
                'label' => 'thumbs-up',
                'attributes' => [
                    'data-icon' => 'far fa-thumbs-up',
                ],
            ],
            [
                'value' => 'fas fa-thumbtack',
                'label' => 'Thumbtack',
                'attributes' => [
                    'data-icon' => 'fas fa-thumbtack',
                ],
            ],
            [
                'value' => 'fas fa-ticket',
                'label' => 'Ticket',
                'attributes' => [
                    'data-icon' => 'fas fa-ticket',
                ],
            ],
            [
                'value' => 'fas fa-ticket-simple',
                'label' => 'Ticket simple',
                'attributes' => [
                    'data-icon' => 'fas fa-ticket-simple',
                ],
            ],
            [
                'value' => 'fab fa-tiktok',
                'label' => 'TikTok',
                'attributes' => [
                    'data-icon' => 'fab fa-tiktok',
                ],
            ],
            [
                'value' => 'fas fa-timeline',
                'label' => 'Timeline',
                'attributes' => [
                    'data-icon' => 'fas fa-timeline',
                ],
            ],
            [
                'value' => 'fas fa-toggle-off',
                'label' => 'Toggle Off',
                'attributes' => [
                    'data-icon' => 'fas fa-toggle-off',
                ],
            ],
            [
                'value' => 'fas fa-toggle-on',
                'label' => 'Toggle On',
                'attributes' => [
                    'data-icon' => 'fas fa-toggle-on',
                ],
            ],
            [
                'value' => 'fas fa-toilet',
                'label' => 'Toilet',
                'attributes' => [
                    'data-icon' => 'fas fa-toilet',
                ],
            ],
            [
                'value' => 'fas fa-toilet-paper',
                'label' => 'Toilet Paper',
                'attributes' => [
                    'data-icon' => 'fas fa-toilet-paper',
                ],
            ],
            [
                'value' => 'fas fa-toilet-paper-slash',
                'label' => 'Toilet Paper Slash',
                'attributes' => [
                    'data-icon' => 'fas fa-toilet-paper-slash',
                ],
            ],
            [
                'value' => 'fas fa-toilet-portable',
                'label' => 'Toilet Portable',
                'attributes' => [
                    'data-icon' => 'fas fa-toilet-portable',
                ],
            ],
            [
                'value' => 'fas fa-toilets-portable',
                'label' => 'Toilets Portable',
                'attributes' => [
                    'data-icon' => 'fas fa-toilets-portable',
                ],
            ],
            [
                'value' => 'fas fa-toolbox',
                'label' => 'Toolbox',
                'attributes' => [
                    'data-icon' => 'fas fa-toolbox',
                ],
            ],
            [
                'value' => 'fas fa-tooth',
                'label' => 'Tooth',
                'attributes' => [
                    'data-icon' => 'fas fa-tooth',
                ],
            ],
            [
                'value' => 'fas fa-torii-gate',
                'label' => 'Torii Gate',
                'attributes' => [
                    'data-icon' => 'fas fa-torii-gate',
                ],
            ],
            [
                'value' => 'fas fa-tornado',
                'label' => 'Tornado',
                'attributes' => [
                    'data-icon' => 'fas fa-tornado',
                ],
            ],
            [
                'value' => 'fas fa-tower-broadcast',
                'label' => 'Tower broadcast',
                'attributes' => [
                    'data-icon' => 'fas fa-tower-broadcast',
                ],
            ],
            [
                'value' => 'fas fa-tower-cell',
                'label' => 'Tower Cell',
                'attributes' => [
                    'data-icon' => 'fas fa-tower-cell',
                ],
            ],
            [
                'value' => 'fas fa-tower-observation',
                'label' => 'Tower Observation',
                'attributes' => [
                    'data-icon' => 'fas fa-tower-observation',
                ],
            ],
            [
                'value' => 'fas fa-tractor',
                'label' => 'Tractor',
                'attributes' => [
                    'data-icon' => 'fas fa-tractor',
                ],
            ],
            [
                'value' => 'fab fa-trade-federation',
                'label' => 'Trade Federation',
                'attributes' => [
                    'data-icon' => 'fab fa-trade-federation',
                ],
            ],
            [
                'value' => 'fas fa-trademark',
                'label' => 'Trademark',
                'attributes' => [
                    'data-icon' => 'fas fa-trademark',
                ],
            ],
            [
                'value' => 'fas fa-traffic-light',
                'label' => 'Traffic Light',
                'attributes' => [
                    'data-icon' => 'fas fa-traffic-light',
                ],
            ],
            [
                'value' => 'fas fa-trailer',
                'label' => 'Trailer',
                'attributes' => [
                    'data-icon' => 'fas fa-trailer',
                ],
            ],
            [
                'value' => 'fas fa-train',
                'label' => 'Train',
                'attributes' => [
                    'data-icon' => 'fas fa-train',
                ],
            ],
            [
                'value' => 'fas fa-train-subway',
                'label' => 'Train subway',
                'attributes' => [
                    'data-icon' => 'fas fa-train-subway',
                ],
            ],
            [
                'value' => 'fas fa-train-tram',
                'label' => 'Train tram',
                'attributes' => [
                    'data-icon' => 'fas fa-train-tram',
                ],
            ],
            [
                'value' => 'fas fa-transgender',
                'label' => 'Transgender',
                'attributes' => [
                    'data-icon' => 'fas fa-transgender',
                ],
            ],
            [
                'value' => 'fas fa-trash',
                'label' => 'Trash',
                'attributes' => [
                    'data-icon' => 'fas fa-trash',
                ],
            ],
            [
                'value' => 'fas fa-trash-arrow-up',
                'label' => 'Trash arrow up',
                'attributes' => [
                    'data-icon' => 'fas fa-trash-arrow-up',
                ],
            ],
            [
                'value' => 'fas fa-trash-can',
                'label' => 'Trash can',
                'attributes' => [
                    'data-icon' => 'fas fa-trash-can',
                ],
            ],
            [
                'value' => 'far fa-trash-can',
                'label' => 'Trash can',
                'attributes' => [
                    'data-icon' => 'far fa-trash-can',
                ],
            ],
            [
                'value' => 'fas fa-trash-can-arrow-up',
                'label' => 'Trash can arrow up',
                'attributes' => [
                    'data-icon' => 'fas fa-trash-can-arrow-up',
                ],
            ],
            [
                'value' => 'fas fa-tree',
                'label' => 'Tree',
                'attributes' => [
                    'data-icon' => 'fas fa-tree',
                ],
            ],
            [
                'value' => 'fas fa-tree-city',
                'label' => 'Tree City',
                'attributes' => [
                    'data-icon' => 'fas fa-tree-city',
                ],
            ],
            [
                'value' => 'fab fa-trello',
                'label' => 'Trello',
                'attributes' => [
                    'data-icon' => 'fab fa-trello',
                ],
            ],
            [
                'value' => 'fas fa-triangle-exclamation',
                'label' => 'Triangle exclamation',
                'attributes' => [
                    'data-icon' => 'fas fa-triangle-exclamation',
                ],
            ],
            [
                'value' => 'fas fa-trophy',
                'label' => 'trophy',
                'attributes' => [
                    'data-icon' => 'fas fa-trophy',
                ],
            ],
            [
                'value' => 'fas fa-trowel',
                'label' => 'Trowel',
                'attributes' => [
                    'data-icon' => 'fas fa-trowel',
                ],
            ],
            [
                'value' => 'fas fa-trowel-bricks',
                'label' => 'Trowel Bricks',
                'attributes' => [
                    'data-icon' => 'fas fa-trowel-bricks',
                ],
            ],
            [
                'value' => 'fas fa-truck',
                'label' => 'truck',
                'attributes' => [
                    'data-icon' => 'fas fa-truck',
                ],
            ],
            [
                'value' => 'fas fa-truck-arrow-right',
                'label' => 'Truck Arrow-right',
                'attributes' => [
                    'data-icon' => 'fas fa-truck-arrow-right',
                ],
            ],
            [
                'value' => 'fas fa-truck-droplet',
                'label' => 'Truck Droplet',
                'attributes' => [
                    'data-icon' => 'fas fa-truck-droplet',
                ],
            ],
            [
                'value' => 'fas fa-truck-fast',
                'label' => 'Truck fast',
                'attributes' => [
                    'data-icon' => 'fas fa-truck-fast',
                ],
            ],
            [
                'value' => 'fas fa-truck-field',
                'label' => 'Truck Field',
                'attributes' => [
                    'data-icon' => 'fas fa-truck-field',
                ],
            ],
            [
                'value' => 'fas fa-truck-field-un',
                'label' => 'Truck Field-un',
                'attributes' => [
                    'data-icon' => 'fas fa-truck-field-un',
                ],
            ],
            [
                'value' => 'fas fa-truck-front',
                'label' => 'Truck Front',
                'attributes' => [
                    'data-icon' => 'fas fa-truck-front',
                ],
            ],
            [
                'value' => 'fas fa-truck-medical',
                'label' => 'Truck medical',
                'attributes' => [
                    'data-icon' => 'fas fa-truck-medical',
                ],
            ],
            [
                'value' => 'fas fa-truck-monster',
                'label' => 'Truck Monster',
                'attributes' => [
                    'data-icon' => 'fas fa-truck-monster',
                ],
            ],
            [
                'value' => 'fas fa-truck-moving',
                'label' => 'Truck Moving',
                'attributes' => [
                    'data-icon' => 'fas fa-truck-moving',
                ],
            ],
            [
                'value' => 'fas fa-truck-pickup',
                'label' => 'Truck Side',
                'attributes' => [
                    'data-icon' => 'fas fa-truck-pickup',
                ],
            ],
            [
                'value' => 'fas fa-truck-plane',
                'label' => 'Truck Plane',
                'attributes' => [
                    'data-icon' => 'fas fa-truck-plane',
                ],
            ],
            [
                'value' => 'fas fa-truck-ramp-box',
                'label' => 'Truck ramp box',
                'attributes' => [
                    'data-icon' => 'fas fa-truck-ramp-box',
                ],
            ],
            [
                'value' => 'fas fa-tty',
                'label' => 'TTY',
                'attributes' => [
                    'data-icon' => 'fas fa-tty',
                ],
            ],
            [
                'value' => 'fab fa-tumblr',
                'label' => 'Tumblr',
                'attributes' => [
                    'data-icon' => 'fab fa-tumblr',
                ],
            ],
            [
                'value' => 'fab fa-tumblr-square',
                'label' => 'Tumblr Square',
                'attributes' => [
                    'data-icon' => 'fab fa-tumblr-square',
                ],
            ],
            [
                'value' => 'fas fa-turkish-lira-sign',
                'label' => 'Turkish Lira-sign',
                'attributes' => [
                    'data-icon' => 'fas fa-turkish-lira-sign',
                ],
            ],
            [
                'value' => 'fas fa-turn-down',
                'label' => 'Turn down',
                'attributes' => [
                    'data-icon' => 'fas fa-turn-down',
                ],
            ],
            [
                'value' => 'fas fa-turn-up',
                'label' => 'Turn up',
                'attributes' => [
                    'data-icon' => 'fas fa-turn-up',
                ],
            ],
            [
                'value' => 'fas fa-tv',
                'label' => 'Television',
                'attributes' => [
                    'data-icon' => 'fas fa-tv',
                ],
            ],
            [
                'value' => 'fab fa-twitch',
                'label' => 'Twitch',
                'attributes' => [
                    'data-icon' => 'fab fa-twitch',
                ],
            ],
            [
                'value' => 'fab fa-twitter',
                'label' => 'Twitter',
                'attributes' => [
                    'data-icon' => 'fab fa-twitter',
                ],
            ],
            [
                'value' => 'fab fa-twitter-square',
                'label' => 'Twitter Square',
                'attributes' => [
                    'data-icon' => 'fab fa-twitter-square',
                ],
            ],
            [
                'value' => 'fab fa-typo3',
                'label' => 'Typo3',
                'attributes' => [
                    'data-icon' => 'fab fa-typo3',
                ],
            ],
            [
                'value' => 'fas fa-u',
                'label' => 'U',
                'attributes' => [
                    'data-icon' => 'fas fa-u',
                ],
            ],
            [
                'value' => 'fab fa-uber',
                'label' => 'Uber',
                'attributes' => [
                    'data-icon' => 'fab fa-uber',
                ],
            ],
            [
                'value' => 'fab fa-ubuntu',
                'label' => 'Ubuntu',
                'attributes' => [
                    'data-icon' => 'fab fa-ubuntu',
                ],
            ],
            [
                'value' => 'fab fa-uikit',
                'label' => 'UIkit',
                'attributes' => [
                    'data-icon' => 'fab fa-uikit',
                ],
            ],
            [
                'value' => 'fab fa-umbraco',
                'label' => 'Umbraco',
                'attributes' => [
                    'data-icon' => 'fab fa-umbraco',
                ],
            ],
            [
                'value' => 'fas fa-umbrella',
                'label' => 'Umbrella',
                'attributes' => [
                    'data-icon' => 'fas fa-umbrella',
                ],
            ],
            [
                'value' => 'fas fa-umbrella-beach',
                'label' => 'Umbrella Beach',
                'attributes' => [
                    'data-icon' => 'fas fa-umbrella-beach',
                ],
            ],
            [
                'value' => 'fab fa-uncharted',
                'label' => 'Uncharted Software',
                'attributes' => [
                    'data-icon' => 'fab fa-uncharted',
                ],
            ],
            [
                'value' => 'fas fa-underline',
                'label' => 'Underline',
                'attributes' => [
                    'data-icon' => 'fas fa-underline',
                ],
            ],
            [
                'value' => 'fab fa-uniregistry',
                'label' => 'Uniregistry',
                'attributes' => [
                    'data-icon' => 'fab fa-uniregistry',
                ],
            ],
            [
                'value' => 'fab fa-unity',
                'label' => 'Unity 3D',
                'attributes' => [
                    'data-icon' => 'fab fa-unity',
                ],
            ],
            [
                'value' => 'fas fa-universal-access',
                'label' => 'Universal Access',
                'attributes' => [
                    'data-icon' => 'fas fa-universal-access',
                ],
            ],
            [
                'value' => 'fas fa-unlock',
                'label' => 'unlock',
                'attributes' => [
                    'data-icon' => 'fas fa-unlock',
                ],
            ],
            [
                'value' => 'fas fa-unlock-keyhole',
                'label' => 'Unlock keyhole',
                'attributes' => [
                    'data-icon' => 'fas fa-unlock-keyhole',
                ],
            ],
            [
                'value' => 'fab fa-unsplash',
                'label' => 'Unsplash',
                'attributes' => [
                    'data-icon' => 'fab fa-unsplash',
                ],
            ],
            [
                'value' => 'fab fa-untappd',
                'label' => 'Untappd',
                'attributes' => [
                    'data-icon' => 'fab fa-untappd',
                ],
            ],
            [
                'value' => 'fas fa-up-down',
                'label' => 'Up down',
                'attributes' => [
                    'data-icon' => 'fas fa-up-down',
                ],
            ],
            [
                'value' => 'fas fa-up-down-left-right',
                'label' => 'Up down left right',
                'attributes' => [
                    'data-icon' => 'fas fa-up-down-left-right',
                ],
            ],
            [
                'value' => 'fas fa-up-long',
                'label' => 'Up long',
                'attributes' => [
                    'data-icon' => 'fas fa-up-long',
                ],
            ],
            [
                'value' => 'fas fa-up-right-and-down-left-from-center',
                'label' => 'Up right and down left from center',
                'attributes' => [
                    'data-icon' => 'fas fa-up-right-and-down-left-from-center',
                ],
            ],
            [
                'value' => 'fas fa-up-right-from-square',
                'label' => 'Up right from square',
                'attributes' => [
                    'data-icon' => 'fas fa-up-right-from-square',
                ],
            ],
            [
                'value' => 'fas fa-upload',
                'label' => 'Upload',
                'attributes' => [
                    'data-icon' => 'fas fa-upload',
                ],
            ],
            [
                'value' => 'fab fa-ups',
                'label' => 'UPS',
                'attributes' => [
                    'data-icon' => 'fab fa-ups',
                ],
            ],
            [
                'value' => 'fab fa-usb',
                'label' => 'USB',
                'attributes' => [
                    'data-icon' => 'fab fa-usb',
                ],
            ],
            [
                'value' => 'fas fa-user',
                'label' => 'User',
                'attributes' => [
                    'data-icon' => 'fas fa-user',
                ],
            ],
            [
                'value' => 'far fa-user',
                'label' => 'User',
                'attributes' => [
                    'data-icon' => 'far fa-user',
                ],
            ],
            [
                'value' => 'fas fa-user-astronaut',
                'label' => 'User Astronaut',
                'attributes' => [
                    'data-icon' => 'fas fa-user-astronaut',
                ],
            ],
            [
                'value' => 'fas fa-user-check',
                'label' => 'User Check',
                'attributes' => [
                    'data-icon' => 'fas fa-user-check',
                ],
            ],
            [
                'value' => 'fas fa-user-clock',
                'label' => 'User Clock',
                'attributes' => [
                    'data-icon' => 'fas fa-user-clock',
                ],
            ],
            [
                'value' => 'fas fa-user-doctor',
                'label' => 'User doctor',
                'attributes' => [
                    'data-icon' => 'fas fa-user-doctor',
                ],
            ],
            [
                'value' => 'fas fa-user-gear',
                'label' => 'User gear',
                'attributes' => [
                    'data-icon' => 'fas fa-user-gear',
                ],
            ],
            [
                'value' => 'fas fa-user-graduate',
                'label' => 'User Graduate',
                'attributes' => [
                    'data-icon' => 'fas fa-user-graduate',
                ],
            ],
            [
                'value' => 'fas fa-user-group',
                'label' => 'User group',
                'attributes' => [
                    'data-icon' => 'fas fa-user-group',
                ],
            ],
            [
                'value' => 'fas fa-user-injured',
                'label' => 'User Injured',
                'attributes' => [
                    'data-icon' => 'fas fa-user-injured',
                ],
            ],
            [
                'value' => 'fas fa-user-large',
                'label' => 'User large',
                'attributes' => [
                    'data-icon' => 'fas fa-user-large',
                ],
            ],
            [
                'value' => 'fas fa-user-large-slash',
                'label' => 'User large slash',
                'attributes' => [
                    'data-icon' => 'fas fa-user-large-slash',
                ],
            ],
            [
                'value' => 'fas fa-user-lock',
                'label' => 'User Lock',
                'attributes' => [
                    'data-icon' => 'fas fa-user-lock',
                ],
            ],
            [
                'value' => 'fas fa-user-minus',
                'label' => 'User Minus',
                'attributes' => [
                    'data-icon' => 'fas fa-user-minus',
                ],
            ],
            [
                'value' => 'fas fa-user-ninja',
                'label' => 'User Ninja',
                'attributes' => [
                    'data-icon' => 'fas fa-user-ninja',
                ],
            ],
            [
                'value' => 'fas fa-user-nurse',
                'label' => 'Nurse',
                'attributes' => [
                    'data-icon' => 'fas fa-user-nurse',
                ],
            ],
            [
                'value' => 'fas fa-user-pen',
                'label' => 'User pen',
                'attributes' => [
                    'data-icon' => 'fas fa-user-pen',
                ],
            ],
            [
                'value' => 'fas fa-user-plus',
                'label' => 'User Plus',
                'attributes' => [
                    'data-icon' => 'fas fa-user-plus',
                ],
            ],
            [
                'value' => 'fas fa-user-secret',
                'label' => 'User Secret',
                'attributes' => [
                    'data-icon' => 'fas fa-user-secret',
                ],
            ],
            [
                'value' => 'fas fa-user-shield',
                'label' => 'User Shield',
                'attributes' => [
                    'data-icon' => 'fas fa-user-shield',
                ],
            ],
            [
                'value' => 'fas fa-user-slash',
                'label' => 'User Slash',
                'attributes' => [
                    'data-icon' => 'fas fa-user-slash',
                ],
            ],
            [
                'value' => 'fas fa-user-tag',
                'label' => 'User Tag',
                'attributes' => [
                    'data-icon' => 'fas fa-user-tag',
                ],
            ],
            [
                'value' => 'fas fa-user-tie',
                'label' => 'User Tie',
                'attributes' => [
                    'data-icon' => 'fas fa-user-tie',
                ],
            ],
            [
                'value' => 'fas fa-user-xmark',
                'label' => 'User X Mark',
                'attributes' => [
                    'data-icon' => 'fas fa-user-xmark',
                ],
            ],
            [
                'value' => 'fas fa-users',
                'label' => 'Users',
                'attributes' => [
                    'data-icon' => 'fas fa-users',
                ],
            ],
            [
                'value' => 'fas fa-users-between-lines',
                'label' => 'Users Between-lines',
                'attributes' => [
                    'data-icon' => 'fas fa-users-between-lines',
                ],
            ],
            [
                'value' => 'fas fa-users-gear',
                'label' => 'Users gear',
                'attributes' => [
                    'data-icon' => 'fas fa-users-gear',
                ],
            ],
            [
                'value' => 'fas fa-users-line',
                'label' => 'Users Line',
                'attributes' => [
                    'data-icon' => 'fas fa-users-line',
                ],
            ],
            [
                'value' => 'fas fa-users-rays',
                'label' => 'Users Rays',
                'attributes' => [
                    'data-icon' => 'fas fa-users-rays',
                ],
            ],
            [
                'value' => 'fas fa-users-rectangle',
                'label' => 'Users Rectangle',
                'attributes' => [
                    'data-icon' => 'fas fa-users-rectangle',
                ],
            ],
            [
                'value' => 'fas fa-users-slash',
                'label' => 'Users Slash',
                'attributes' => [
                    'data-icon' => 'fas fa-users-slash',
                ],
            ],
            [
                'value' => 'fas fa-users-viewfinder',
                'label' => 'Users Viewfinder',
                'attributes' => [
                    'data-icon' => 'fas fa-users-viewfinder',
                ],
            ],
            [
                'value' => 'fab fa-usps',
                'label' => 'United States Postal Service',
                'attributes' => [
                    'data-icon' => 'fab fa-usps',
                ],
            ],
            [
                'value' => 'fab fa-ussunnah',
                'label' => 'us-Sunnah Foundation',
                'attributes' => [
                    'data-icon' => 'fab fa-ussunnah',
                ],
            ],
            [
                'value' => 'fas fa-utensils',
                'label' => 'Utensils',
                'attributes' => [
                    'data-icon' => 'fas fa-utensils',
                ],
            ],
            [
                'value' => 'fas fa-v',
                'label' => 'V',
                'attributes' => [
                    'data-icon' => 'fas fa-v',
                ],
            ],
            [
                'value' => 'fab fa-vaadin',
                'label' => 'Vaadin',
                'attributes' => [
                    'data-icon' => 'fab fa-vaadin',
                ],
            ],
            [
                'value' => 'fas fa-van-shuttle',
                'label' => 'Van shuttle',
                'attributes' => [
                    'data-icon' => 'fas fa-van-shuttle',
                ],
            ],
            [
                'value' => 'fas fa-vault',
                'label' => 'Vault',
                'attributes' => [
                    'data-icon' => 'fas fa-vault',
                ],
            ],
            [
                'value' => 'fas fa-vector-square',
                'label' => 'Vector Square',
                'attributes' => [
                    'data-icon' => 'fas fa-vector-square',
                ],
            ],
            [
                'value' => 'fas fa-venus',
                'label' => 'Venus',
                'attributes' => [
                    'data-icon' => 'fas fa-venus',
                ],
            ],
            [
                'value' => 'fas fa-venus-double',
                'label' => 'Venus Double',
                'attributes' => [
                    'data-icon' => 'fas fa-venus-double',
                ],
            ],
            [
                'value' => 'fas fa-venus-mars',
                'label' => 'Venus Mars',
                'attributes' => [
                    'data-icon' => 'fas fa-venus-mars',
                ],
            ],
            [
                'value' => 'fas fa-vest',
                'label' => 'vest',
                'attributes' => [
                    'data-icon' => 'fas fa-vest',
                ],
            ],
            [
                'value' => 'fas fa-vest-patches',
                'label' => 'vest-patches',
                'attributes' => [
                    'data-icon' => 'fas fa-vest-patches',
                ],
            ],
            [
                'value' => 'fab fa-viacoin',
                'label' => 'Viacoin',
                'attributes' => [
                    'data-icon' => 'fab fa-viacoin',
                ],
            ],
            [
                'value' => 'fab fa-viadeo',
                'label' => 'Viadeo',
                'attributes' => [
                    'data-icon' => 'fab fa-viadeo',
                ],
            ],
            [
                'value' => 'fab fa-viadeo-square',
                'label' => 'Viadeo Square',
                'attributes' => [
                    'data-icon' => 'fab fa-viadeo-square',
                ],
            ],
            [
                'value' => 'fas fa-vial',
                'label' => 'Vial',
                'attributes' => [
                    'data-icon' => 'fas fa-vial',
                ],
            ],
            [
                'value' => 'fas fa-vial-circle-check',
                'label' => 'Vial Circle-check',
                'attributes' => [
                    'data-icon' => 'fas fa-vial-circle-check',
                ],
            ],
            [
                'value' => 'fas fa-vial-virus',
                'label' => 'Vial Virus',
                'attributes' => [
                    'data-icon' => 'fas fa-vial-virus',
                ],
            ],
            [
                'value' => 'fas fa-vials',
                'label' => 'Vials',
                'attributes' => [
                    'data-icon' => 'fas fa-vials',
                ],
            ],
            [
                'value' => 'fab fa-viber',
                'label' => 'Viber',
                'attributes' => [
                    'data-icon' => 'fab fa-viber',
                ],
            ],
            [
                'value' => 'fas fa-video',
                'label' => 'Video',
                'attributes' => [
                    'data-icon' => 'fas fa-video',
                ],
            ],
            [
                'value' => 'fas fa-video-slash',
                'label' => 'Video Slash',
                'attributes' => [
                    'data-icon' => 'fas fa-video-slash',
                ],
            ],
            [
                'value' => 'fas fa-vihara',
                'label' => 'Vihara',
                'attributes' => [
                    'data-icon' => 'fas fa-vihara',
                ],
            ],
            [
                'value' => 'fab fa-vimeo',
                'label' => 'Vimeo',
                'attributes' => [
                    'data-icon' => 'fab fa-vimeo',
                ],
            ],
            [
                'value' => 'fab fa-vimeo-square',
                'label' => 'Vimeo Square',
                'attributes' => [
                    'data-icon' => 'fab fa-vimeo-square',
                ],
            ],
            [
                'value' => 'fab fa-vimeo-v',
                'label' => 'Vimeo',
                'attributes' => [
                    'data-icon' => 'fab fa-vimeo-v',
                ],
            ],
            [
                'value' => 'fab fa-vine',
                'label' => 'Vine',
                'attributes' => [
                    'data-icon' => 'fab fa-vine',
                ],
            ],
            [
                'value' => 'fas fa-virus',
                'label' => 'Virus',
                'attributes' => [
                    'data-icon' => 'fas fa-virus',
                ],
            ],
            [
                'value' => 'fas fa-virus-covid',
                'label' => 'Virus Covid',
                'attributes' => [
                    'data-icon' => 'fas fa-virus-covid',
                ],
            ],
            [
                'value' => 'fas fa-virus-covid-slash',
                'label' => 'Virus Covid-slash',
                'attributes' => [
                    'data-icon' => 'fas fa-virus-covid-slash',
                ],
            ],
            [
                'value' => 'fas fa-virus-slash',
                'label' => 'Virus Slash',
                'attributes' => [
                    'data-icon' => 'fas fa-virus-slash',
                ],
            ],
            [
                'value' => 'fas fa-viruses',
                'label' => 'Viruses',
                'attributes' => [
                    'data-icon' => 'fas fa-viruses',
                ],
            ],
            [
                'value' => 'fab fa-vk',
                'label' => 'VK',
                'attributes' => [
                    'data-icon' => 'fab fa-vk',
                ],
            ],
            [
                'value' => 'fab fa-vnv',
                'label' => 'VNV',
                'attributes' => [
                    'data-icon' => 'fab fa-vnv',
                ],
            ],
            [
                'value' => 'fas fa-voicemail',
                'label' => 'Voicemail',
                'attributes' => [
                    'data-icon' => 'fas fa-voicemail',
                ],
            ],
            [
                'value' => 'fas fa-volcano',
                'label' => 'Volcano',
                'attributes' => [
                    'data-icon' => 'fas fa-volcano',
                ],
            ],
            [
                'value' => 'fas fa-volleyball',
                'label' => 'Volleyball Ball',
                'attributes' => [
                    'data-icon' => 'fas fa-volleyball',
                ],
            ],
            [
                'value' => 'fas fa-volume-high',
                'label' => 'Volume high',
                'attributes' => [
                    'data-icon' => 'fas fa-volume-high',
                ],
            ],
            [
                'value' => 'fas fa-volume-low',
                'label' => 'Volume low',
                'attributes' => [
                    'data-icon' => 'fas fa-volume-low',
                ],
            ],
            [
                'value' => 'fas fa-volume-off',
                'label' => 'Volume Off',
                'attributes' => [
                    'data-icon' => 'fas fa-volume-off',
                ],
            ],
            [
                'value' => 'fas fa-volume-xmark',
                'label' => 'Volume X Mark',
                'attributes' => [
                    'data-icon' => 'fas fa-volume-xmark',
                ],
            ],
            [
                'value' => 'fas fa-vr-cardboard',
                'label' => 'Cardboard VR',
                'attributes' => [
                    'data-icon' => 'fas fa-vr-cardboard',
                ],
            ],
            [
                'value' => 'fab fa-vuejs',
                'label' => 'Vue.js',
                'attributes' => [
                    'data-icon' => 'fab fa-vuejs',
                ],
            ],
            [
                'value' => 'fas fa-w',
                'label' => 'W',
                'attributes' => [
                    'data-icon' => 'fas fa-w',
                ],
            ],
            [
                'value' => 'fas fa-walkie-talkie',
                'label' => 'Walkie Talkie',
                'attributes' => [
                    'data-icon' => 'fas fa-walkie-talkie',
                ],
            ],
            [
                'value' => 'fas fa-wallet',
                'label' => 'Wallet',
                'attributes' => [
                    'data-icon' => 'fas fa-wallet',
                ],
            ],
            [
                'value' => 'fas fa-wand-magic',
                'label' => 'Wand magic',
                'attributes' => [
                    'data-icon' => 'fas fa-wand-magic',
                ],
            ],
            [
                'value' => 'fas fa-wand-magic-sparkles',
                'label' => 'Wand magic sparkles',
                'attributes' => [
                    'data-icon' => 'fas fa-wand-magic-sparkles',
                ],
            ],
            [
                'value' => 'fas fa-wand-sparkles',
                'label' => 'Wand sparkles',
                'attributes' => [
                    'data-icon' => 'fas fa-wand-sparkles',
                ],
            ],
            [
                'value' => 'fas fa-warehouse',
                'label' => 'Warehouse',
                'attributes' => [
                    'data-icon' => 'fas fa-warehouse',
                ],
            ],
            [
                'value' => 'fab fa-watchman-monitoring',
                'label' => 'Watchman Monitoring',
                'attributes' => [
                    'data-icon' => 'fab fa-watchman-monitoring',
                ],
            ],
            [
                'value' => 'fas fa-water',
                'label' => 'Water',
                'attributes' => [
                    'data-icon' => 'fas fa-water',
                ],
            ],
            [
                'value' => 'fas fa-water-ladder',
                'label' => 'Water ladder',
                'attributes' => [
                    'data-icon' => 'fas fa-water-ladder',
                ],
            ],
            [
                'value' => 'fas fa-wave-square',
                'label' => 'Square Wave',
                'attributes' => [
                    'data-icon' => 'fas fa-wave-square',
                ],
            ],
            [
                'value' => 'fab fa-waze',
                'label' => 'Waze',
                'attributes' => [
                    'data-icon' => 'fab fa-waze',
                ],
            ],
            [
                'value' => 'fab fa-weebly',
                'label' => 'Weebly',
                'attributes' => [
                    'data-icon' => 'fab fa-weebly',
                ],
            ],
            [
                'value' => 'fab fa-weibo',
                'label' => 'Weibo',
                'attributes' => [
                    'data-icon' => 'fab fa-weibo',
                ],
            ],
            [
                'value' => 'fas fa-weight-hanging',
                'label' => 'Hanging Weight',
                'attributes' => [
                    'data-icon' => 'fas fa-weight-hanging',
                ],
            ],
            [
                'value' => 'fas fa-weight-scale',
                'label' => 'Weight scale',
                'attributes' => [
                    'data-icon' => 'fas fa-weight-scale',
                ],
            ],
            [
                'value' => 'fab fa-weixin',
                'label' => 'Weixin (WeChat)',
                'attributes' => [
                    'data-icon' => 'fab fa-weixin',
                ],
            ],
            [
                'value' => 'fab fa-whatsapp',
                'label' => 'Whats App',
                'attributes' => [
                    'data-icon' => 'fab fa-whatsapp',
                ],
            ],
            [
                'value' => 'fab fa-whatsapp-square',
                'label' => 'Whats App Square',
                'attributes' => [
                    'data-icon' => 'fab fa-whatsapp-square',
                ],
            ],
            [
                'value' => 'fas fa-wheat-awn',
                'label' => 'Wheat awn',
                'attributes' => [
                    'data-icon' => 'fas fa-wheat-awn',
                ],
            ],
            [
                'value' => 'fas fa-wheat-awn-circle-exclamation',
                'label' => 'Wheat Awn-circle-exclamation',
                'attributes' => [
                    'data-icon' => 'fas fa-wheat-awn-circle-exclamation',
                ],
            ],
            [
                'value' => 'fas fa-wheelchair',
                'label' => 'Wheelchair',
                'attributes' => [
                    'data-icon' => 'fas fa-wheelchair',
                ],
            ],
            [
                'value' => 'fas fa-wheelchair-move',
                'label' => 'Wheelchair Move',
                'attributes' => [
                    'data-icon' => 'fas fa-wheelchair-move',
                ],
            ],
            [
                'value' => 'fas fa-whiskey-glass',
                'label' => 'Whiskey glass',
                'attributes' => [
                    'data-icon' => 'fas fa-whiskey-glass',
                ],
            ],
            [
                'value' => 'fab fa-whmcs',
                'label' => 'WHMCS',
                'attributes' => [
                    'data-icon' => 'fab fa-whmcs',
                ],
            ],
            [
                'value' => 'fas fa-wifi',
                'label' => 'WiFi',
                'attributes' => [
                    'data-icon' => 'fas fa-wifi',
                ],
            ],
            [
                'value' => 'fab fa-wikipedia-w',
                'label' => 'Wikipedia W',
                'attributes' => [
                    'data-icon' => 'fab fa-wikipedia-w',
                ],
            ],
            [
                'value' => 'fas fa-wind',
                'label' => 'Wind',
                'attributes' => [
                    'data-icon' => 'fas fa-wind',
                ],
            ],
            [
                'value' => 'fas fa-window-maximize',
                'label' => 'Window Maximize',
                'attributes' => [
                    'data-icon' => 'fas fa-window-maximize',
                ],
            ],
            [
                'value' => 'far fa-window-maximize',
                'label' => 'Window Maximize',
                'attributes' => [
                    'data-icon' => 'far fa-window-maximize',
                ],
            ],
            [
                'value' => 'fas fa-window-minimize',
                'label' => 'Window Minimize',
                'attributes' => [
                    'data-icon' => 'fas fa-window-minimize',
                ],
            ],
            [
                'value' => 'far fa-window-minimize',
                'label' => 'Window Minimize',
                'attributes' => [
                    'data-icon' => 'far fa-window-minimize',
                ],
            ],
            [
                'value' => 'fas fa-window-restore',
                'label' => 'Window Restore',
                'attributes' => [
                    'data-icon' => 'fas fa-window-restore',
                ],
            ],
            [
                'value' => 'far fa-window-restore',
                'label' => 'Window Restore',
                'attributes' => [
                    'data-icon' => 'far fa-window-restore',
                ],
            ],
            [
                'value' => 'fab fa-windows',
                'label' => 'Windows',
                'attributes' => [
                    'data-icon' => 'fab fa-windows',
                ],
            ],
            [
                'value' => 'fas fa-wine-bottle',
                'label' => 'Wine Bottle',
                'attributes' => [
                    'data-icon' => 'fas fa-wine-bottle',
                ],
            ],
            [
                'value' => 'fas fa-wine-glass',
                'label' => 'Wine Glass',
                'attributes' => [
                    'data-icon' => 'fas fa-wine-glass',
                ],
            ],
            [
                'value' => 'fas fa-wine-glass-empty',
                'label' => 'Wine glass empty',
                'attributes' => [
                    'data-icon' => 'fas fa-wine-glass-empty',
                ],
            ],
            [
                'value' => 'fab fa-wirsindhandwerk',
                'label' => 'wirsindhandwerk',
                'attributes' => [
                    'data-icon' => 'fab fa-wirsindhandwerk',
                ],
            ],
            [
                'value' => 'fab fa-wix',
                'label' => 'Wix',
                'attributes' => [
                    'data-icon' => 'fab fa-wix',
                ],
            ],
            [
                'value' => 'fab fa-wizards-of-the-coast',
                'label' => 'Wizards of the Coast',
                'attributes' => [
                    'data-icon' => 'fab fa-wizards-of-the-coast',
                ],
            ],
            [
                'value' => 'fab fa-wodu',
                'label' => 'Wodu',
                'attributes' => [
                    'data-icon' => 'fab fa-wodu',
                ],
            ],
            [
                'value' => 'fab fa-wolf-pack-battalion',
                'label' => 'Wolf Pack Battalion',
                'attributes' => [
                    'data-icon' => 'fab fa-wolf-pack-battalion',
                ],
            ],
            [
                'value' => 'fas fa-won-sign',
                'label' => 'Won Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-won-sign',
                ],
            ],
            [
                'value' => 'fab fa-wordpress',
                'label' => 'WordPress Logo',
                'attributes' => [
                    'data-icon' => 'fab fa-wordpress',
                ],
            ],
            [
                'value' => 'fab fa-wordpress-simple',
                'label' => 'Wordpress Simple',
                'attributes' => [
                    'data-icon' => 'fab fa-wordpress-simple',
                ],
            ],
            [
                'value' => 'fas fa-worm',
                'label' => 'Worm',
                'attributes' => [
                    'data-icon' => 'fas fa-worm',
                ],
            ],
            [
                'value' => 'fab fa-wpbeginner',
                'label' => 'WPBeginner',
                'attributes' => [
                    'data-icon' => 'fab fa-wpbeginner',
                ],
            ],
            [
                'value' => 'fab fa-wpexplorer',
                'label' => 'WPExplorer',
                'attributes' => [
                    'data-icon' => 'fab fa-wpexplorer',
                ],
            ],
            [
                'value' => 'fab fa-wpforms',
                'label' => 'WPForms',
                'attributes' => [
                    'data-icon' => 'fab fa-wpforms',
                ],
            ],
            [
                'value' => 'fab fa-wpressr',
                'label' => 'wpressr',
                'attributes' => [
                    'data-icon' => 'fab fa-wpressr',
                ],
            ],
            [
                'value' => 'fas fa-wrench',
                'label' => 'Wrench',
                'attributes' => [
                    'data-icon' => 'fas fa-wrench',
                ],
            ],
            [
                'value' => 'fas fa-x',
                'label' => 'X',
                'attributes' => [
                    'data-icon' => 'fas fa-x',
                ],
            ],
            [
                'value' => 'fas fa-x-ray',
                'label' => 'X-Ray',
                'attributes' => [
                    'data-icon' => 'fas fa-x-ray',
                ],
            ],
            [
                'value' => 'fab fa-xbox',
                'label' => 'Xbox',
                'attributes' => [
                    'data-icon' => 'fab fa-xbox',
                ],
            ],
            [
                'value' => 'fab fa-xing',
                'label' => 'Xing',
                'attributes' => [
                    'data-icon' => 'fab fa-xing',
                ],
            ],
            [
                'value' => 'fab fa-xing-square',
                'label' => 'Xing Square',
                'attributes' => [
                    'data-icon' => 'fab fa-xing-square',
                ],
            ],
            [
                'value' => 'fas fa-xmark',
                'label' => 'X Mark',
                'attributes' => [
                    'data-icon' => 'fas fa-xmark',
                ],
            ],
            [
                'value' => 'fas fa-xmarks-lines',
                'label' => 'Xmarks Lines',
                'attributes' => [
                    'data-icon' => 'fas fa-xmarks-lines',
                ],
            ],
            [
                'value' => 'fas fa-y',
                'label' => 'Y',
                'attributes' => [
                    'data-icon' => 'fas fa-y',
                ],
            ],
            [
                'value' => 'fab fa-y-combinator',
                'label' => 'Y Combinator',
                'attributes' => [
                    'data-icon' => 'fab fa-y-combinator',
                ],
            ],
            [
                'value' => 'fab fa-yahoo',
                'label' => 'Yahoo Logo',
                'attributes' => [
                    'data-icon' => 'fab fa-yahoo',
                ],
            ],
            [
                'value' => 'fab fa-yammer',
                'label' => 'Yammer',
                'attributes' => [
                    'data-icon' => 'fab fa-yammer',
                ],
            ],
            [
                'value' => 'fab fa-yandex',
                'label' => 'Yandex',
                'attributes' => [
                    'data-icon' => 'fab fa-yandex',
                ],
            ],
            [
                'value' => 'fab fa-yandex-international',
                'label' => 'Yandex International',
                'attributes' => [
                    'data-icon' => 'fab fa-yandex-international',
                ],
            ],
            [
                'value' => 'fab fa-yarn',
                'label' => 'Yarn',
                'attributes' => [
                    'data-icon' => 'fab fa-yarn',
                ],
            ],
            [
                'value' => 'fab fa-yelp',
                'label' => 'Yelp',
                'attributes' => [
                    'data-icon' => 'fab fa-yelp',
                ],
            ],
            [
                'value' => 'fas fa-yen-sign',
                'label' => 'Yen Sign',
                'attributes' => [
                    'data-icon' => 'fas fa-yen-sign',
                ],
            ],
            [
                'value' => 'fas fa-yin-yang',
                'label' => 'Yin Yang',
                'attributes' => [
                    'data-icon' => 'fas fa-yin-yang',
                ],
            ],
            [
                'value' => 'fab fa-yoast',
                'label' => 'Yoast',
                'attributes' => [
                    'data-icon' => 'fab fa-yoast',
                ],
            ],
            [
                'value' => 'fab fa-youtube',
                'label' => 'YouTube',
                'attributes' => [
                    'data-icon' => 'fab fa-youtube',
                ],
            ],
            [
                'value' => 'fab fa-youtube-square',
                'label' => 'YouTube Square',
                'attributes' => [
                    'data-icon' => 'fab fa-youtube-square',
                ],
            ],
            [
                'value' => 'fas fa-z',
                'label' => 'Z',
                'attributes' => [
                    'data-icon' => 'fas fa-z',
                ],
            ],
            [
                'value' => 'fab fa-zhihu',
                'label' => 'Zhihu',
                'attributes' => [
                    'data-icon' => 'fab fa-zhihu',
                ],
            ],
        ];

        return $laIcons;
    }
}
